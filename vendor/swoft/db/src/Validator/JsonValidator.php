<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2019/4/23
 * Time: 18:06
 */

namespace Swoft\Db\Validator;


use Swoft\Bean\Annotation\Bean;
use Swoft\Exception\ValidatorException;

/**
 * @Bean("DbJsonValidator")
 */
class JsonValidator implements ValidatorInterface
{
    /**
     * @param string $column    Column name
     * @param mixed  $value     Column value
     * @param array  ...$params Other parameters
     * @throws ValidatorException When validation failures, will throw an Exception
     * @return bool When validation successful
     */
    public function validate(string $column, $value, ...$params): bool
    {
        json_decode($value);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}