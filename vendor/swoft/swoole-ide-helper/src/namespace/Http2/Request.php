<?php
namespace Swoole\Http2;

/**
 * @since 4.3.0
 */
class Request
{

    public $path;
    public $method;
    public $headers;
    public $cookies;
    public $data;
    public $pipeline;
    public $files;


}
