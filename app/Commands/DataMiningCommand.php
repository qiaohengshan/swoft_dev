<?php

/**
 * This file is part of Swoft.
 *
 * @link https://swoft.org
 * @document https://doc.swoft.org
 * @contact group@swoft.org
 * @license https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Commands;

use App\Models\Entity\StockTrade;
use App\Models\Logic\StockTradeLogic;
use App\Services\StockService;
use Swoft\Console\Bean\Annotation\Command;
use Swoft\Console\Bean\Annotation\Mapping;
use Swoft\Console\Input\Input;
use Swoft\Console\Output\Output;
use traits\controller\Jump;

/**
 * This is a example command class
 * @Command(coroutine=false)
 * @package App\Commands
 */
class DataMiningCommand
{
    /**
     * 持续调整
     * @Usage {command} [arguments ...] [--options ...]
     * @Arguments
     *   first STRING        The first argument value
     *   second STRING       The second argument value
     * @Options
     *   --opt STRING        This is a long option
     *   -s BOOL             This is a short option(<comment>use color</comment>)
     * @Example {command} FIRST SECOND --opt VALUE -s
     * @param Input $input
     * @param Output $output
     * @Mapping("adjust")
     * @return int
     */
    public function adjust(Input $input, Output $output)
    {
        $stocks = StockService::all();
        if($stocks)
        {
            foreach ($stocks as $stock)
            {
                $result  = StockTradeLogic::adjust($stock);
                if($result === true)
                {
                    dump($stock);
                }
            }
        }
    }

    /**
     * 持续放量拉升
     * @Usage {command} [arguments ...] [--options ...]
     * @Arguments
     *   first STRING        The first argument value
     *   second STRING       The second argument value
     * @Options
     *   --opt STRING        This is a long option
     *   -s BOOL             This is a short option(<comment>use color</comment>)
     * @Example {command} FIRST SECOND --opt VALUE -s
     * @param Input $input
     * @param Output $output
     * @Mapping("lifting")
     * @return int
     */
    public function lifting(Input $input, Output $output)
    {
        $stocks = StockService::all();
        if($stocks)
        {
            foreach ($stocks as $stock)
            {
                $result  = StockTradeLogic::adjust($stock,">=");
                if($result === true)
                {
                    dump($stock);
                }
            }
        }
    }
}
