<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2018/12/17
 * Time: 16:25
 */

namespace App\Lib;
use Swoft\Core\ResultInterface;
/**
 * The interface of stock service
 *
 * @method ResultInterface deferLists(array $params)
 * @method ResultInterface deferDetail(string $id)
 */
interface StockInterface
{
    /**
     * @param array $params
     * @return array
     */
    public function lists(array  $params);

    /**
     * @param $id
     * @return mixed
     */
    public function detail(int $id);
}