<?php

/**
 * This file is part of Swoft.
 *
 * @link https://swoft.org
 * @document https://doc.swoft.org
 * @contact group@swoft.org
 * @license https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Process;

use App\Services\SNSService;
use App\Services\StockService;
use Swoft\App;
use Swoft\Core\Coroutine;
use Swoft\Process\Bean\Annotation\Process;
use Swoft\Process\Process as SwoftProcess;
use Swoft\Process\ProcessInterface;
use Swoft\Task\Task;

/**
 *
 * Class StockRealTradeProcess - Custom process
 *
 * @Process(name="StockRealTrade", coroutine=true,boot=false,inout=false,pipe=true,num=10)
 * @package App\Process
 */
class StockRealTradeProcess implements ProcessInterface
{
    public function run(SwoftProcess $process)
    {
        $service = new SNSService();
        $stocks = StockService::all();
        if($stocks)
        {
            foreach ($stocks as $stock)
            {
                $response = $service->minute($stock);
                Task::deliverByProcess('stock-trade','saveMinuteData',[$stock,$response['detail'],$response['items']]);
            }
        }
    }

    public function check(): bool
    {
        return true;
    }
}
