<?php

/**
 * This file is part of Swoft.
 *
 * @link https://swoft.org
 * @document https://doc.swoft.org
 * @contact group@swoft.org
 * @license https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Process;

use App\Services\MailService;
use App\Services\SNSStatisticService;
use Swoft\App;
use Swoft\Core\Coroutine;
use Swoft\Process\Bean\Annotation\Process;
use Swoft\Process\Process as SwoftProcess;
use Swoft\Process\ProcessInterface;
use Swoole\Exception;

/**
 *
 * Class TopsProcess - Custom process
 *
 * @Process(name="Tops", coroutine=true,boot=false)
 * @package App\Process
 */
class TopsProcess implements ProcessInterface
{
    private $overview=null;

    public function run(SwoftProcess $process)
    {
        try{
            $overview = $this->overview;
            if($overview)
            {

                $subject="[今日热点概念]:[".$overview["name"]."]";
                $body =<<< EOD
                    今日热点：{$overview["name"]},入选原因:{$overview['hype_remark']} <br />     
EOD;
                $stocks = isset($overview['stocks'])?$overview['stocks']:[];

                if($stocks)
                {
                    $body = $body.PHP_EOL;
                    $body = $body."当前人气龙头:";
                    foreach ($stocks as $stock)
                    {
                        $body = $body.$stock['name']."({$stock['symbol']})".PHP_EOL;
                    }
                }
                $recipients=["joy_qhs@163.com","458977025@qq.com"];
                $response = MailService::send($subject,$body,$recipients);
                return $response;
            }
        }catch (Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),$exp->getMessage()));
        }
    }

    public function check(): bool
    {
        $service = new SNSStatisticService();
        $response = $service->tops();
        if(isset($response['overview']) && $response['overview'])
        {
            $this->overview = $response['overview'];
            return true;
        }
        return false;
    }
}
