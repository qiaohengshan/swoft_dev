<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2019/4/24
 * Time: 12:21
 */
namespace App\Process;

use App\Models\Logic\TradeLogic;
use Swoft\App;
use Swoft\Core\Coroutine;
use Swoft\Process\Bean\Annotation\Process;
use Swoft\Process\Process as SwoftProcess;
use Swoft\Process\ProcessInterface;
use App\Services\RedisService;
/**
 * Custom process
 *
 * @Process(name="hot-block", coroutine=true,boot=false,inout=false,pipe=true,num=6)
 */
class HotBlockRecommendStockProcess implements ProcessInterface
{
    private $stocks;
    private $service;
    const KEY='hot-tops:stock';
    public function run(SwoftProcess $process)
    {
        $logic = new TradeLogic();
        if($this->stocks)
        {
            foreach ($this->stocks as $stock)
            {
                $result =$logic->volume($stock);

            }
        }
    }

    public function check(): bool
    {
        $this->service = new RedisService();
        $this->stocks = $this->service->hGetAll(self::KEY);
        if($this->stocks)
        {
            return true;
        }
        return false;
    }
}