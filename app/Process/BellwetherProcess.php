<?php

/**
 * This file is part of Swoft.
 *
 * @link https://swoft.org
 * @document https://doc.swoft.org
 * @contact group@swoft.org
 * @license https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Process;

use App\Models\Logic\ConceptLogic;
use App\Services\RedisService;
use App\Services\SNSBlockService;
use Swoft\Core\Coroutine;
use Swoft\Process\Bean\Annotation\Process;
use Swoft\Process\Process as SwoftProcess;
use Swoft\Process\ProcessInterface;
use Swoft\Task\Task;

/**
 *
 * Class BellwetherProcess - Custom process
 *
 * @Process(name="Bellwether", coroutine=true,boot=false,inout=false,pipe=true,num=8)
 * @package App\Process
 */
class BellwetherProcess implements ProcessInterface
{
    private $blocks;
    private $service;
    public function run(SwoftProcess $process)
    {
        if($this->blocks)
        {
            $blocks = $this->blocks;
            foreach ($blocks as $block)
            {
                $bkCode = $block['code'];
                $response = $this->service->stocks($bkCode);
                if(isset($response['items']) && $items = $response['items'])
                {
                    foreach ($items as $item)
                    {
                        dump($item);
                        if(  !empty($item['tag']) || !empty($item['tags']))
                        {
                            Task::deliverByProcess('block','storeBellwether',[$bkCode,$item]);
                        }
                    }
                }
            }
        }
    }

    public function check(): bool
    {
        $redisService = new RedisService();
        $blocks =  $redisService->hGetAll(ConceptLogic::NAME);
        if($blocks)
        {
            $this->blocks = $blocks;
            $this->service = new SNSBlockService();
            return true;
        }
        return false;
    }
}
