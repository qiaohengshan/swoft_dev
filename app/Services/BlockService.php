<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2018/12/11
 * Time: 18:02
 */

namespace App\Services;

use App\Models\Entity\Concept;
use Swoft\App;
use Swoft\Bean\Annotation\Inject;
use Swoft\Db\Db;
use Swoft\Rpc\Client\Bean\Annotation\Reference;
use Swoft\Bean\Annotation\Bean;
use Swoft\HttpClient\Client;
use Swoole\Exception;

/**
 * 版块概念服务类
 * @version   2018年12月11日
 * @author    joy <joy_qhs@163.com>
 * @copyright Copyright 2010-2016 swoft software
 * @license   PHP Version 7.x {@link http://www.php.net/license/3_0.txt}
 */
class BlockService
{
    /**
     * @Inject()
     * @var \Swoft\Redis\Redis
     */
    private $redis;

    private static $url = "http://stock.snssdk.com/v1/block/statistic/";

    const KEY = 'stock:block';

    public function list($type)
    {
        $url = self::$url.__FUNCTION__.'?'."index=0&order=0&rank_type=0&class_type=$type";
        $client = new Client();
        $response = $client->get($url)->getResult();
        $result = $this->format($response);
        if ($result)
        {
            $this->create($type,$result);
        }
    }
    private function create($type,$items)
    {
        foreach ($items as $item)
        {
            $data = [
                'name'=>$item['name'],
                'code'=>$item['bk_code'],
                'type'=>$type,
                'tags'=>json_encode($item['tags'])
            ];
            $model = new Concept();
            $has  =$model->findOne(['code' => 1], ['fields' => ['id']])->getResult();
            Db::beginTransaction();
            try{
                if($has)
                {
                    $data['update_time'] = time();
                    $model->updateOne($data,['id'=>$has['id']])->getResult();
                    $id = $has['id'];
                }else{
                    $data['create_time'] = time();
                    $model->fill($data)->save()->getResult();
                    $id = $model->getId();
                }
                Db::commit();
                $this->redis->hSet(self::KEY,$id,json_encode($data));
            }catch (Exception $exp){
                Db::rollback();
                App::error($exp->getMessage());
            }
        }
    }
    private function format($response)
    {
        $array = json_decode($response,true);
        $result = [];
        if(isset($array['code']) && $array['code'] == 0)
        {
            $data =isset($array['data'])?$array['data']:[];
            $blocks = isset($data['blocks'])?$data['blocks']:[];
            $result = $blocks;
        }
        return $result;
    }
}