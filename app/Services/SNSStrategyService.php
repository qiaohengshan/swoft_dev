<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2019/5/20
 * Time: 15:36
 */

namespace App\Services;
use Swoft\HttpClient\Client;
use Swoft\Task\Task;

class SNSStrategyService
{
    const URL = "http://stock.snssdk.com/v1/strategy/";
    private $client;
    public function __construct()
    {
        $config['headers']['Upgrade-Insecure-Requests']=1;
        $config['headers']['User-Agent']="Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Mobile Safari/537.36";
        $this->client = new Client($config);

    }

    /**
     * 关注信息
     * @param int $period 周期,1天，2天,...
     * @param int $offset
     * @param int $limit
     * @param string $field
     * @param int $order
     */
    public function attentionstocks($period=1,$offset=0,$limit=30,$field="change_rate",$order=0)
    {
        $url = self::URL.__FUNCTION__."?order=$order&field=$field&offset=$offset&limit=$limit&period=$period";
        $response = $this->client->get($url)->getResult();
        $array = json_decode($response,true);
        $result['items'] = isset($array['data'])?$array['data']:[];
        if(isset($result['items']) && !empty($result['items']))
        {
            Task::deliverByProcess('strategy','saveAttentionStocks',[$result['items']]);
            $offset = $offset + $limit ;
            self::attentionstocks($period,$offset,$limit,$field,$order);
        }
        return true;
    }
}