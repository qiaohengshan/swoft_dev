<?php


namespace App\Services;

use App\Models\Entity\StockTradeDetailMinute;

class StockAnalysis
{
    const START_TIME="1445";
    const END_TIME = "1500";
    const TOTAL = 4*60;
    /**
     * 尾盘分析
     * @param $stock
     * @param $time
     */
    public static function tail($stock,$date)
    {
        $items = StockTradeDetailMinute::findAll(
            [
                'stock_id' => $stock['id'],
                'date'=>$date,
            ]
        )->whereBetween('time',[self::START_TIME,self::END_TIME])
       ->getResult();
        $service = new RedisService();
        // (close - open) / high
        if($items && count($items) == 16)
        {
            $first = array_shift($items);
            $last = array_pop($items);
            $detail =$service->hGet('stock:detail',$stock['code']);
            if($detail)
            {
                $rate  = ($last['price'] - $first['price']) / $detail['limit_up'];
                return $rate;
            }
        }
        return null;
    }
}