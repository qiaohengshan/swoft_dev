<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2019/4/19
 * Time: 18:47
 */

namespace App\Services;

use Swoft\HttpClient\Client;
class HttpService
{
    public $client;
    public function __construct()
    {
        $config['headers']['Upgrade-Insecure-Requests']=1;
        $config['headers']['User-Agent']="Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Mobile Safari/537.36";
        $this->client = new Client($config);
    }

    public static function get()
    {
        $self = new self();
        return $self->client;
    }
}