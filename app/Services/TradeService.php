<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2019/4/15
 * Time: 17:39
 */

namespace App\Services;


use App\Models\Entity\StockTradeDetailMinute;
use Swoft\App;
use Swoft\Db\Db;

class TradeService
{
    public function __construct()
    {

    }

    public function storeMinuteData($data)
    {
        $model = new StockTradeDetailMinute();
        $where=[
            'stock_id'=>$data['stock_id'],
            'time'=>$data['time'],
            'date'=>$data['date'],
        ];
        $has  = $model->findOne($where, ['fields' => ['id', 'stock_id']])->getResult();

        Db::beginTransaction();

        try{
            if($has)
            {
                $result = $model->updateOne($data, ['id' => $has->getId()])->getResult();
            }else{
                $result = $model->fill($data)->save()->getResult();
            }
            Db::commit();
        }catch (\Exception $exp){
            Db::rollback();
            App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));

        }
    }
}