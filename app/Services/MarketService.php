<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2019/5/17
 * Time: 14:18
 */

namespace App\Services;

use Swoft\App;
use Swoft\HttpClient\Client;
class MarketService
{
    const URL = "http://stock.snssdk.com/v1/market/";

    private $client;

    public function __construct()
    {
        $config['headers']['Upgrade-Insecure-Requests']=1;
        $config['headers']['User-Agent']="Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Mobile Safari/537.36";
        $this->client = new Client($config);
    }

    public function time($market='sh')
    {
        try{
            $response = self::URL.__FUNCTION__;
            $array = json_decode($response,true);
            $data  = isset($array['data'])?$array['data']:[];
            if(isset($data[$market]))
            {
                return $data[$market];
            }
            return $data;
        }catch (\Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),$exp->getMessage()));
        }
    }

}