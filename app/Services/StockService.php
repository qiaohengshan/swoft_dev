<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2018/12/17
 * Time: 16:24
 */

namespace App\Services;
use App\Lib\StockInterface;
use App\Models\Entity\Stock;
use App\Models\Entity\StockConcept;
use App\Models\Entity\StockDetail;
use App\Models\Entity\StockProfile;
use Swoft\Bean\Annotation\Enum;
use Swoft\Bean\Annotation\Floats;
use Swoft\Bean\Annotation\Number;
use Swoft\Bean\Annotation\Strings;
use Swoft\Db\Query;
use Swoft\Rpc\Server\Bean\Annotation\Service;
use Swoft\Core\ResultInterface;
/**
 * Stock Service
 *
 * @method ResultInterface deferLists(array $params)
 * @method ResultInterface deferDetail(string $id)
 *
 * @Service()
 */
class StockService implements StockInterface
{
    private $entity  = null;
    public function __construct()
    {
        $this->entity = new Stock();
    }

    /**
     * @param array $params
     * @param int $page
     * @param int $pageSize
     * @param string $sortBy
     * @param string $descending
     * @return array
     */
    public function lists(array $params,int $page=1,int $pageSize=30,string  $sortBy='',string $descending='')
    {
        $where=[];
        $offset=  ($page - 1 ) * $pageSize;
        $descending = $descending?'DESC':'ASC';
        $columns=['distinct(a.id)','a.*','c.industry_name','c.industry_enable','d.*'];
        $rows = Query::table(Stock::class,'a')
            ->leftJoin(StockConcept::class,'a.id=b.stock_id','b')
            ->leftJoin(StockProfile::class,'a.id=c.stock_id','c')
            ->leftJoin(StockDetail::class,'a.id=d.stock_id','d')
            ->orderBy($sortBy,$descending)
            ->limit($pageSize,$offset)->get($columns)->getResult();
        $total = Query::table(Stock::class,'a')
            ->leftJoin(StockConcept::class,'a.id=b.stock_id','b')
            ->leftJoin(StockProfile::class,'a.id=c.stock_id','c')
            ->leftJoin(StockDetail::class,'a.id=d.stock_id','d')
            ->count('distinct(a.id)')->getResult();

        foreach ($rows as &$row)
        {
            $concepts = StockConcept::findAll(['stock_id'=>$row['id']])->getResult();
            $row['concept']=$concepts;
        }

        return ['data'=>$rows,'total'=>$total];
    }

    public function detail(int $id)
    {

    }

    public static function all()
    {
        $service = new RedisService();
        $stocks = $service->hGetAll('stock');
        return $stocks;
    }
}