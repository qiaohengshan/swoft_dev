<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2018/12/10
 * Time: 10:16
 */

namespace App\Services;


class FormatData
{
    /**
     * 筹码数据
     * @param $item
     */
    public static function chips($item)
    {
        $data = $item;
        $distributionFirst=array_shift($item);
        $distributionEnd  =array_pop($item);
        $data['chip_focus_90'] = ext_json_decode($data['chip_focus_90']);
        $data['chip_focus_70'] = ext_json_decode($data['chip_focus_70']);
        $data['profit_level'] = ext_json_decode($data['profit_level']);
        $data['chip_desc'] = ext_json_decode($data['chip_desc']);
        $data['distribution_min']=$distributionFirst[0];
        $data['distribution_max']=$distributionEnd[0];
        unset($data['main_cost_str']);
        unset($data['distribution_range']);
        unset($data['avg_cost_str']);
        unset($data['profit_ratio_str']);
        unset($data['loss_ratio_str']);
        unset($data['avg_profit_str']);
        unset($data['dense_line_str']);
        return $data;
    }
    public static function article($item):array
    {
        $data['title']=$item['title'];
        $data['abstract']=$item['abstract'];
        $data['article_type']=$item['article_type'];
        $data['type']=$item['type'];
        $data['importance']=$item['importance'];
        $data['sentiment']=$item['sentiment'];
        $data['source']=$item['source'];
        $data['publish_time']=$item['publish_time'];
        return $data;
    }
    public static function tops($item)
    {
        $result['bk_code']=$item['bk_code'];
        $result['name']=$item['name'];
        $result['symbol']=$item['symbol'];
        $result['consistency']=$item['consistency'];
        $result['hot']=$item['hot'];
        $result['type']=$item['type'];
        $result['hype_remark']=$item['hype_remark'];
        $result['created_at']=date('Y-m-d',strtotime($item['created_at']));
        $result['stocks']=json_encode($item['stocks']);
        return $result;
    }
    /**
     * 格式化详情数据
     * @param $item
     * @return array
     */
    public static function formatDetailData($item):array
    {
        $row=[];

        $row['open'] = isset($item['open'])?$item['open']:0;
        $row['cur_price'] = isset($item['cur_price'])?$item['cur_price']:0;
        $row['high'] = isset($item['high'])?$item['high']:0;
        $row['low'] = isset($item['low'])?$item['low']:0;
        $row['pre_close'] = isset($item['pre_close'])?$item['pre_close']:0;
        $row['limit_up'] = isset($item['limit_up'])?$item['limit_up']:0;
        $row['limit_down'] = isset($item['limit_down'])?$item['limit_down']:0;

        $row['amplitude'] = isset($item['amplitude'])?$item['amplitude']:0;
        $row['amplitude'] = str_replace('%','',$row['amplitude']);

        $row['bid_ratio'] = isset($item['bid_ratio'])?$item['bid_ratio']:0;
        $row['bid_ratio'] = str_replace('%','',$row['bid_ratio']);

        $row['change'] = isset($item['change'])?$item['change']:0;

        $row['change_rate'] = isset($item['change_rate'])?$item['change_rate']:'';
        $row['change_rate'] = str_replace('%','',$row['change_rate']);

        $row['turnover_rate'] = isset($item['turnover_rate'])?$item['turnover_rate']:0;
        $row['turnover_rate'] = str_replace('%','',$row['turnover_rate']);

        $row['turnover_num'] = isset($item['turnover_num'])?$item['turnover_num']:0;
        $row['volume_num'] = isset($item['volume_num'])?$item['volume_num']:0;
        $row['state'] = isset($item['state'])?$item['state']:'';


        $row['eps'] = isset($item['eps'])?$item['eps']:0;
        $row['eps_key'] = isset($item['eps_key'])?$item['eps_key']:0;
        $row['pb'] = isset($item['pb'])?$item['pb']:0;
        $row['pe'] = isset($item['pe'])?$item['pe']:0;
        $row['pe_num'] = isset($item['pe_num'])?$item['pe_num']:0;

        $row['pe_sta'] = isset($item['pe_sta'])?$item['pe_sta']:0;
        preg_match('/[\d.\d]+/', $row['pe_sta'],$match);
        if($match)
        {
            $row['pe_sta'] = $match[0];
        }else{
            $row['pe_sta'] = 0;
        }

        $row['pe_ttm'] = isset($item['pe_ttm'])?$item['pe_ttm']:0;

        $row['circulation_market_value'] = isset($item['circulation_market_value'])?$item['circulation_market_value']:0;
        preg_match('/[\d.\d]+/', $row['circulation_market_value'],$match);
        if($match)
        {
            $row['circulation_market_value'] = $match[0];
        }else{
            $row['circulation_market_value'] = 0;
        }

        $row['market_value'] = isset($item['market_value'])?$item['market_value']:0;
        preg_match('/[\d.\d]+/', $row['market_value'],$match);
        if($match)
        {
            $row['market_value'] = $match[0];
        }else{
            $row['market_value'] = 0;
        }

        $row['total_shares'] = isset($item['total_shares'])?$item['total_shares']:0;
        preg_match('/[\d.\d]+/', $row['total_shares'],$match);
        if($match)
        {
            $row['total_shares'] = $match[0];
        }else{
            $row['total_shares'] = 0;
        }

        $row['float_shares'] = isset($item['float_shares'])?$item['float_shares']:0;
        preg_match('/[\d.\d]+/', $row['float_shares'],$match);
        if($match)
        {
            $row['float_shares'] = $match[0];
        }else{
            $row['float_shares'] = 0;
        }
        $row['time'] = strtotime(date('Y-m-d',strtotime($item['time'])));
        return $row;
    }
    /**
     * 格式化简介数据
     * @param $item
     * @return array
     */
    public static function formatProfileData($item)
    {
        $row=[];
        $row['company_code'] = isset($item['company_code'])?$item['company_code']:'';
        $row['company_name'] = isset($item['company_name'])?$item['company_name']:'';
        $row['indu_csrc'] = isset($item['indu_csrc'])?$item['indu_csrc']:0;
        $row['state'] = isset($item['state'])?$item['state']:'';
        $row['business_major'] = isset($item['business_major'])?$item['business_major']:'';
        $row['industry_name'] = isset($item['industry_name'])?$item['industry_name']:'';
        $row['industry_code'] = isset($item['industry_code'])?$item['industry_code']:'';
        $row['industry_enable'] = isset($item['industry_enable'])?$item['industry_enable']:0;
        $row['listing_date'] = isset($item['listing_date'])?$item['listing_date']:'';
        $row['issue_price'] = isset($item['issue_price'])?$item['issue_price']:0;
        $row['issue_vol'] = isset($item['issue_vol'])?$item['issue_vol']:0;

        preg_match('/[\d.\d]+/', $row['issue_vol'],$match1);
        preg_match('/[\x{4e00}-\x{9fff}]+/u', $row['issue_vol'],$match2);

        if($match1 && $match2)
        {
            $number = $match1[0];
            $unit = $match2[0];
            $row['issue_vol'] = $unit=='万'?$number * 10000:$number*100000000;
        }else{
            $row['issue_vol'] =  0;
        }

        $row['info_pub_date'] = isset($item['info_pub_date'])?$item['info_pub_date']:'';
        $row['total_shares'] = isset($item['total_shares'])?$item['total_shares']:0;
        preg_match('/[\d.\d]+/', $row['total_shares'],$match1);
        preg_match('/[\x{4e00}-\x{9fff}]+/u', $row['total_shares'],$match2);
        if($match1 && $match2)
        {
            $number = $match1[0];
            $unit = $match2[0];
            $row['total_shares'] = $unit=='万'?$number * 10000:$number*100000000;
        }else{
            $row['total_shares'] =  0;
        }

        $row['first_holder'] = isset($item['first_holder'])?$item['first_holder']:'';
        $row['pct_main_holders'] = isset($item['pct_main_holders'])?$item['pct_main_holders']:'';
        $row['pct_institutions'] = isset($item['pct_institutions'])?$item['pct_institutions']:'';
        $row['share_pub_date'] = isset($item['share_pub_date'])?$item['share_pub_date']:'';
        $row['share_holder_num'] = isset($item['share_holder_num'])?$item['share_holder_num']:'';
        $row['average_hold_sum'] = isset($item['average_hold_sum'])?$item['average_hold_sum']:'';
        $row['income_sum'] = isset($item['income_sum'])?$item['income_sum']:0;
        preg_match('/[\d.\d]+/', $row['income_sum'],$match1);
        preg_match('/[\x{4e00}-\x{9fff}]+/u', $row['income_sum'],$match2);

        if($match1 && $match2)
        {
            $number = $match1[0];
            $unit = $match2[0];
            $row['income_sum'] = $unit=='万'?$number * 10000:$number*100000000;
        }else{
            $row['income_sum'] =  0;
        }

        $row['income_date'] = isset($item['income_date'])?$item['income_date']:'';
        $row['leader_date'] = isset($item['leader_date'])?$item['leader_date']:'';

        return $row;
    }

    /**
     * 格式化交易数据
     * @param $item
     * @return mixed
     */
    public static function formatTradeData($item)
    {
        $row['date']=$item[0];
        $row['open']=$item[1];
        $row['close']=$item[2];
        $row['high']=$item[3];
        $row['low']=$item[4];
        $row['volume']=$item[5];
        $row['turnover']=$item[6];
        $row['turnover_rate']=$item[7];
        $row['volume_ratio']=$item[8];
        $row['price_inc']=number_format($item[10],4);
        $row['price_pre'] =  $row['close'] + $row['price_inc'] ;
        return $row;
    }

    public static function formatMinuteData($item):array
    {
        $array = explode(" ",$item);
        $data['time'] = $array[0];
        $data['price'] = $array[1];
        $data['volume'] = $array[2];
        $data['turnover'] = $array[3];
        return $data;
    }

    public static function formatAttentionStockData($item):array
    {
        $data['code'] = $item['code'];
        $data['symbol'] = $item['symbol'];
        $data['type'] = $item['type'];
        $data['date'] = $item['date']=='今天'?date('Y-m-d',time()):$item['date'];
        $data['change_rate'] = str_replace('%','',$item['change_rate']);
        $data['profit'] = str_replace('%','',$item['profit']);
        $data['max_change_rate'] = str_replace('%','',$item['max_change_rate']);
        return $data;
    }
}