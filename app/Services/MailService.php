<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2019/4/27
 * Time: 15:23
 */

namespace App\Services;


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/**
 * 邮件服务类
 * Class MailService
 * @package App\Services
 */
class MailService
{
    public static function send($subject,$body,$recipients=[])
    {
        $mail = new PHPMailer(true);
        try {
            //Server settings
            $mail->CharSet = 'utf-8';
            $mail->SMTPDebug = 2;                                       // Enable verbose debug output
            $mail->isSMTP();                                            // Set mailer to use SMTP
            $mail->Host       = 'smtp.163.com';  // Specify main and backup SMTP servers
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   =env('MAIL','gupiao568168@163.com');                  // SMTP username
            $mail->Password   = env('MAIL_PASSWORD','joy568');                             // SMTP password
            $mail->SMTPSecure = 'ssl';                                  // Enable TLS encryption, `ssl` also accepted
            $mail->Port       = 994;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom(env('MAIL','gupiao568168@163.com'), env('MAIL_FROM','Qiao'));

            if($recipients)
            {
                foreach ($recipients as $recipient)
                {
                    $mail->addAddress($recipient);
                }
            }


//            $mail->addReplyTo('info@example.com', 'Information');
//            $mail->addCC('cc@example.com');
//            $mail->addBCC('bcc@example.com');

            // Attachments
            //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

            // Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject =$subject;
            $mail->Body    = $body;
            //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
            $mail->send();
            return true;
        } catch (Exception $e) {
            throw new Exception($mail->ErrorInfo);
        }
    }
}