<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2019/4/22
 * Time: 12:08
 */

namespace App\Services;

use Swoft\HttpClient\Client;
use App\Models\Logic\StockLogic;
class SNSStatisticService
{
    const URL =  "http://stock.snssdk.com/v1/block/statistic/";
    public function __construct()
    {
        $config['headers']['Upgrade-Insecure-Requests']=1;

        $config['headers']['User-Agent']="Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Mobile Safari/537.36";

        $this->client = new Client($config);

        $this->logic = new StockLogic();
    }
    public function list()
    {
        try{
            $url = self::URL.__FUNCTION__;
            $response = $this->client->get($url)->getResult();
            $data = isset($response['data'])?$response['data']:[];
            $blocks = isset($data['blocks'])?$data['blocks']:[];
            return $blocks;
        }catch (Exception $exp) {
            App::error(sprintf('%s has error,error:%s', get_class($this),$exp->getMessage()));
        }
    }

    /**
     * 热点
     * @return array
     */
    public function tops()
    {
        try{
            $url = self::URL.__FUNCTION__;
            $response = $this->client->get($url)->getResult();
            $response = json_decode($response,true);
            $data = isset($response['data'])?$response['data']:[];
            return $data;
        }catch (Exception $exp) {
            App::error(sprintf('%s has error,error:%s', get_class($this),$exp->getMessage()));
        }
    }
}