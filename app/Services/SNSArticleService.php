<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2019/5/22
 * Time: 17:37
 */

namespace App\Services;


use QL\QueryList;
use Swoft\HttpClient\Client;

class SNSArticleService
{
    const URL="http://stock.snssdk.com/v1/article/";
    private $client;
    public function __construct()
    {
        $config['headers']['Upgrade-Insecure-Requests']=1;
        $config['headers']['User-Agent']="Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Mobile Safari/537.36";
        $this->client = new Client($config);
    }

    /**
     * 文章列表
     * @param $stock
     * @param int $end 结束时间
     * @return array
     */
    public function list($stock,$end=0)
    {
        $url = self::URL.__FUNCTION__."?end=$end&limit=100&code={$stock['code']}";
        $response = $this->client->get($url)->getResult();
        $array = json_decode($response,true);
        $data = isset($array['data'])?$array['data']:[];
        return $data;
    }

    /**
     * 根据链接地址获取HTML页面内容
     * @param $url
     * @return Array
     */
    public static function content($url)
    {
        $data = QueryList::get($url)
            ->rules([
                'content'=>array('section#the-article-content>article','text'),
            ])->queryData();
        return $data;
    }
}