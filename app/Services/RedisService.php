<?php
/**
 * Created by PhpStorm.
 * User: joy
 * Date: 18-12-8
 * Time: 上午10:42
 */

namespace App\Services;
use Swoft\App;
use Swoft\Redis\Redis;
class RedisService
{

    private $redis;

    public function __construct()
    {
        $this->redis =new Redis();
    }

    /**
     * 保存hash缓存
     * @param $key
     * @param $hashKey
     * @param $value
     */
    public  function hSet($key, $hashKey, $value)
    {
        return $this->redis->hSet($key,$hashKey,is_array($value)?json_encode($value):$value);
    }

    /**
     * 获取hash缓存
     * @param $key
     * @param $hashKey
     * @return string
     */
    public  function hGet($key,$hashKey)
    {
        $cache =  $this->redis->hGet($key,$hashKey);

        return json_decode($cache,true);
    }

    /**
     * 获取所有缓存
     * @param $key
     * @return array
     */
    public  function hGetAll($key)
    {
        $rows = $this->redis->hGetAll($key);
        if($rows)
        {
            $items = [];
            foreach ($rows as $key=>$row)
            {
                $row = !is_array($row)?json_decode($row,true):$row;
                $items[$key] = $row;
            }
            return $items;
        }else{
            return $rows;
        }
    }

}