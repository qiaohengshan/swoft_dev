<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2018/12/7
 * Time: 13:43
 */

namespace App\Services;

use App\Models\Logic\StockLogic;
use Swoft\App;
use Swoft\HttpClient\Client;
use Swoft\Task\Task;
use Swoole\Exception;
use Swoft\Bean\Annotation\Inject;
use Swoft\Bean\Annotation\Bean;
use Swoft\Db\Db;
use Swoft\Task\Bean\Annotation\Scheduled;

class SNSService
{
    const URL = "http://stock.snssdk.com/v1/quotes/";
    const STOCK = "http://stock.snssdk.com/v1/stocks/detail/";

    const STOCK_URL = "http://stock.snssdk.com/v1/stocks/";

    const STOCK_FS = "https://financial-stock-web.snssdk.com/mc/stock/";

    const STATISTIC_URL =  "http://stock.snssdk.com/v1/block/statistic/";

    private $client;

    private $logic;

    public function __construct()
    {
        $config['headers']['Upgrade-Insecure-Requests']=1;
        $config['headers']['User-Agent']="Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Mobile Safari/537.36";
        $this->client = new Client($config);

        $this->logic = new StockLogic();
    }

    /**
     * 筹码
     * @param $stock
     * @return array
     * @throws \Exception
     */
    public function chips($stock)
    {
        try{
            $url = self::URL.'chips'.'?'."code={$stock['code']}";
            $response = $this->client->get($url)->getResult();
            $detail = $this->formatReturnData($response,__FUNCTION__);
            return $detail;
        }catch (\Exception $exp){
            throw $exp;
        }
    }


    /**
     * @param $code 代码
     * @param string $start 开始时间,0940 1300
     * @param $deal_size
     * @param int $deal 1:交易列表,其他:detail
     * @param int $deal_last
     * @param int $deal_direction
     */
    public function minute($stock,$start="",$deal_size="",$deal=1,$deal_last=-1,$deal_direction=1)
    {
        try{
            $code = $stock['code'];
            $url = self::URL.'stockdetail'.'?'."start=$start&deal_size=$deal_size&deal=$deal&deal_last=$deal_last&deal_direction=$deal_direction&code=$code&version=2200";
            $response = $this->client->get($url)->getResult();
            $detail = $this->formatReturnData($response,'minute');
            return $detail;
        }catch (\Exception $exp){
            throw $exp;
        }
    }

    /**
     * @param $order 排序:0降序，1升序,
     * @param $market 市场
     * @param int $field 排序规则，1涨幅,6换手率,3成交额,5振幅,7涨速
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public  function quoteranklist($field=1,$order=0,$market=1,$offset=0,$limit=30)
    {
        try{
            $url = self::URL.__FUNCTION__.'?'."order={$order}&field={$field}&offset={$offset}&limit={$limit}&market={$market}";
            $response = $this->client->get($url)->getResult();
            $result =  $this->formatReturnData($response,__FUNCTION__);
            if(isset($result['items']) && !empty($result['items']))
            {
                Task::deliverByProcess('stock-sync', 'createStock', [$result['items']]);
                $offset = $offset + $limit ;
                self::quoteranklist($field,$order,$market,$offset,$limit);
            }
            return true;
        }catch (Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),$exp->getMessage()));
        }

    }

    /**
     * 获取日k数据
     * @param $code
     * @param $start
     * @param $end
     * @return bool
     */
    public function fqkline($stock,$end,$start='',$return=false)
    {
        try{
            $url = self::URL.__FUNCTION__.'?'."count=1024&start=$start&end=$end&ktype=day&autype=qfq&code={$stock['code']}";
            $response = $this->client->get($url)->getResult();
            $result =  $this->formatReturnData($response,__FUNCTION__);
            if($return)
            {
                if(isset($result['items']) && !empty($result['items']))
                {
                    return $result['items'];
                }else{
                    return $result;
                }
            }else{
                if(isset($result['items']) && !empty($result['items']))
                {
                    $items = $result['items'];
                    $item = reset($items);
                    $end  = $item[0];
                    Task::deliverByProcess('stock-sync', 'saveStockTradeData', [$stock,$result['items']]);
                    if (count($result['items']) > 1)
                    {
                        self::fqkline($stock,$end,$start);
                    }
                }
            }

        }catch (Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),$exp->getMessage()));
        }
    }

    public function fqklineAll($stock,$end,$count=1024)
    {
        try{
            $url = self::URL.'fqkline'.'?'."count=$count&end=$end&ktype=day&autype=qfq&code={$stock['code']}";
            $response = $this->client->get($url)->getResult();
            $result =  $this->formatReturnData($response,'fqkline');
            if(isset($result['items']) && $result['items'] )
            {
                $items = $result['items'];
                return $items;
            }else{
                return $result;
            }
        }catch (Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),$exp->getMessage()));
        }
    }


    /**
     * 获取个股简介
     * @param $code 股票编码
     * @return array
     */
    public function profile($code)
    {
        try{
            $url = self::STOCK.__FUNCTION__.'?'."code=$code";
            $response = $this->client->get($url)->getResult();
            $result =  $this->formatReturnData($response,__FUNCTION__);
            return $result;
        }catch (Exception $exp) {
            App::error(sprintf('%s has error,error:%s', get_class($this),$exp->getMessage()));
        }
    }

    public function getFSData($code)
    {
        try{
            $url = self::STOCK_FS.__FUNCTION__.'?'."code=$code";
            $response = $this->client->get($url)->getResult();
            $result =  $this->formatReturnData($response,__FUNCTION__);
            return $result;
        }catch (Exception $exp) {
            App::error(sprintf('%s has error,error:%s', get_class($this),$exp->getMessage()));
        }
    }

    /**
     * 格式化返回的数据
     * @param $response
     * @param $method
     * @return array
     */
    public function formatReturnData($response,$method)
    {
        $array = json_decode($response,true);
        $result = [];
        if(isset($array['code']) && $array['code'] == 0)
        {
            switch ($method)
            {
                case 'getFSData':
                    $data = isset($array['data'])?$array['data']:[];
                    $result = $data;
                    break;
                case 'quoteranklist':
                    $data = isset($array['data'])?$array['data']:[];
                    $rankList = isset($data['rank_list'])?$data['rank_list']:[]; //排名列表
                    $total = isset($data['total'])?$data['total']:0; //总数
                    $result['items'] = $rankList;
                    $result['total'] = $total;
                    break;
                case 'profile':
                    $data = isset($array['data'])?$array['data']:[];
                    $result = $data;
                    break;
                case 'fqkline':
                    $data = isset($array['data'])?$array['data']:[];
                    $result['items'] = isset($data['day'])?$data['day']:[];
                    break;
                case 'block':
                    $data = isset($array['data'])?$array['data']:[];
                    $result['items'] = isset($data['blocks'])?$data['blocks']:[];
                    break;
                case 'minute':
                    $data = isset($array['data'])?$array['data']:[];
                    $result['items'] = isset($data['minute'])?$data['minute']:[];
                    $result['detail'] = isset($data['detail'])?$data['detail']:[];
                    break;
                case 'detail':
                    $data = isset($array['data'])?$array['data']:[];
                    $result = isset($data['detail'])?$data['detail']:[];
                    break;
                case 'chips':
                    $data = isset($array['data'])?$array['data']:[];
                    $result = isset($data['list'])?$data['list']:[];
                    break;
                default:
                    break;
            }
        }
        return $result;
    }

    /**
     * 获取股票概念
     * @param $stock
     * @return array
     */
    public function block($code)
    {

        try{
            $url = self::STOCK_URL.__FUNCTION__.'?'."code=$code";
            $response = $this->client->get($url)->getResult();
            $result =  $this->formatReturnData($response,__FUNCTION__);
            return isset($result['items'])?$result['items']:[];
        }catch (Exception $exp) {
            App::error(sprintf('%s has error,error:%s', get_class($this),$exp->getMessage()));
        }
    }

    /**
     * @param $code 代码
     * @param string $start 开始时间,0940 1300
     * @param $deal_size
     * @param int $deal 1:交易列表,其他:detail
     * @param int $deal_last
     * @param int $deal_direction
     */
    public function stockdetail($stock,$start="",$deal_size="",$deal=1,$deal_last=-1,$deal_direction=1)
    {
        try{
            $code = $stock['code'];
            $url = self::URL.__FUNCTION__.'?'."start=$start&deal_size=$deal_size&deal=$deal&deal_last=$deal_last&deal_direction=$deal_direction&code=$code";
            $response = $this->client->get($url)->getResult();
            $detail = $this->formatReturnData($response,'detail');
            return $detail;
        }catch (Exception $exp){
            throw $exp;
        }
    }

}