<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2018/12/10
 * Time: 18:20
 */

namespace App\Services;


class Algorithm
{
  public static function continuousIncrement($items,$name):bool {
      foreach ($items as $key=>$item){
          if($key>0 && $item[$name] > $items[$key-1][$name])
          {
              return true;
          }else{
              return false;
          }
      }
  }

    /**
     * 求数组的平均值
     * @param $rows
     * @param $key
     * @return array
     */
  public static function minuteAverage($rows,$key)
  {
      $tmp = [];
      $result=[];
      foreach ($rows as $row)
      {
          array_push($tmp,$row[$key]);
          $total = count($tmp);
          $sum = array_sum($tmp);
          $avg = $sum / $total;
          array_push($result,$avg);
      }
      return $result;
  }
}