<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2019/4/25
 * Time: 11:44
 */

namespace App\Services;


use Swoft\HttpClient\Client;
use Swoft\Task\Task;

class SNSBlockService
{
    const URL="http://stock.snssdk.com/v1/block/";
    private $client;
    public function __construct()
    {
        $config['headers']['Upgrade-Insecure-Requests']=1;
        $config['headers']['User-Agent']="Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Mobile Safari/537.36";
        $this->client = new Client($config);

    }

    /**
     * 版块/概念列表
     * @param $type 类型
     * @return array
     */
    public function list($type)
    {
        $url = self::URL."statistic".DIRECTORY_SEPARATOR.__FUNCTION__.'?'."index=0&rank_type=0&class_type={$type}&order=0";
        $response = $this->client->get($url)->getResult();
        $detail = $this->formatReturnData($response,'blocks');
        return $detail;
    }

    /**
     * 获取对应概念/版块股票
     * @param $code 概念/版块code
     * @return array
     */
    public function stocks($code)
    {
        $url = self::URL.__FUNCTION__.'?'."bkcode=$code";
        $response = $this->client->get($url)->getResult();
        $detail = $this->formatReturnData($response,'stocks');
        return $detail;
    }

    /**
     * 格式化返回的数据
     * @param $response
     * @param $method
     * @return array
     */
    public function formatReturnData($response,$method)
    {
        $array = json_decode($response,true);
        $result = [];
        if(isset($array['code']) && $array['code'] == 0)
        {
            switch ($method)
            {
                case "blocks":
                    $data = isset($array['data'])?$array['data']:[];
                    $result['items'] = isset($data['blocks'])?$data['blocks']:[];
                    $result['total'] = isset($data['total'])?$data['total']:0;
                    break;
                case 'stocks':
                    $data = isset($array['data'])?$array['data']:[];
                    $result['items'] = isset($data['stocks'])?$data['stocks']:[];
                    break;
                default:
                    break;
            }
        }
        return $result;
    }
}