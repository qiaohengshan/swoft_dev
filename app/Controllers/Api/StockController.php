<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2018/12/17
 * Time: 14:07
 */

namespace App\Controllers\Api;


use App\Services\StockService;
use Swoft\Http\Message\Bean\Annotation\Middleware;
use Swoft\Http\Message\Server\Request;
use Swoft\Http\Server\Bean\Annotation\Controller;
use Swoft\Http\Server\Bean\Annotation\RequestMapping;
use Swoft\Http\Server\Bean\Annotation\RequestMethod;
use Swoft\Http\Server\Exception\HttpException;
use Swoole\Exception;
use App\Exception\SwoftExceptionHandler;
use App\Middlewares\ControllerSubMiddleware;
/**
 * Stock接口
 *
 * @Controller(prefix="/api/stock")
 */
class StockController
{
    private $service;
    public function __construct()
    {
        $this->service = new StockService();
    }

    /**
     * stock查询列表接口
     * 地址:/api/stock/.
     *
     * @RequestMapping(route="/api/stock/list", method={RequestMethod::GET})
     * @Middleware(class=ControllerSubMiddleware::class )
     */
    public function list(Request $request)
    {
       try{
           $params = $request->query();
           $page = $request->query('page',1);
           $pageSize = $request->query('rowsPerPage',30);
           $sortBy = $request->query('sortBy','change_rate');
           $descending = $request->query('descending','DESC');
           $response = $this->service->lists($params,$page,$pageSize,$sortBy,$descending);
           return $response;
       }catch (Exception $exp ){
           throw new Exception($exp->getMessage());
       }
    }

}