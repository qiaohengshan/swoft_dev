<?php
/**
 * This file is part of Swoft.
 *
 * @link https://swoft.org
 * @document https://doc.swoft.org
 * @contact group@swoft.org
 * @license https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Controllers;

use App\Services\SNSService;
use Swoft\HttpClient\Client;
use Swoft\Http\Server\Bean\Annotation\Controller;
use Swoft\Bean\Annotation\Inject;

/**
 * @Controller(prefix="/httpClient")
 */
class HttpClientController
{
    /**
     * @Inject()
     * @var \App\Models\Logic\IndexLogic
     */
    private $logic;

    /**
     * @return array
     * @throws \Swoft\HttpClient\Exception\RuntimeException
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     */
    public function request(): array
    {
        $client = new Client();
        $result = $client->get('http://www.swoft.org')->getResult();
        $result2 = $client->get('http://www.swoft.org')->getResponse()->getBody()->getContents();
        return compact('result', 'result2');
    }

    public function stock()
    {
        $client = new Client();
        $response = $client->get("http://stock.snssdk.com/v1/quotes/quoteranklist?order=0&field=1&offset=0&limit=3&market=1")->getResult();

        $result = (new SNSService())->formatReturnData($response,'quoteranklist');
        if(isset($result['items']) && !empty($result['items']))
        {
            $items = $result['items'];
            foreach ($items as $item) {
                return $this->logic->add($item);
            }
        }
    }
}