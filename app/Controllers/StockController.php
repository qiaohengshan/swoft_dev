<?php
/**
 * This file is part of Swoft.
 *
 * @link https://swoft.org
 * @document https://doc.swoft.org
 * @contact group@swoft.org
 * @license https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Controllers;

use App\Services\FormatData;
use App\Services\SNSService;
use App\Tasks\StockListTask;
use Swoft\App;
use Swoft\Http\Server\Bean\Annotation\Controller;
use Swoft\Http\Server\Bean\Annotation\RequestMapping;
use Swoft\Http\Server\Bean\Annotation\RequestMethod;
// use Swoft\View\Bean\Annotation\View;
// use Swoft\Http\Message\Server\Response;
use Swoft\Bean\Annotation\Inject;
use Swoft\Task\Task;

/**
 * Class StockController
 * @Controller()
 * @package App\Controllers
 */
class StockController{
    /**
     * @Inject()
     * @var \Swoft\Redis\Redis
     */
    private $redis;

    const NAME='cache:stock';
    /**
     * this is a example action. access uri path: /Stock
     * @RequestMapping("/profile")
     */
    public function profile()
    {
        try{
            $stocks = $this->redis->hGetAll(self::NAME);
            if($stocks){
                foreach ($stocks as $stock) {
                    $stock = json_decode($stock,true);
                    $service = new SNSService();
                    $result = $service->profile($stock['code']);
                    if ($result)
                    {
                        $result = FormatData::formatProfileData($result);
                        return [$result];
                        //$response = $this->logic->saveProfile($stock,$result);
                    }

                }
            }
        }catch (\Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),$exp->getMessage()));
        }
    }
    /**
     * this is a example action. access uri path: /Stock
     * @RequestMapping("/test")
     * @return array
     */
    public function index(): array
    {

        $stocks = $this->redis->hGetAll(self::NAME);
        if($stocks)
        {
            foreach ($stocks as $stock)
            {
                $stock = json_decode($stock,true);
                $result = Task::deliver('stock','trade',[$stock],Task::TYPE_CO);
            }
        }
        return ['stocks'=>$stocks];
    }
}
