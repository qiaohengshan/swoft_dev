<?php
namespace App\Models\Entity;

use Swoft\Db\Model;
use Swoft\Db\Bean\Annotation\Column;
use Swoft\Db\Bean\Annotation\Entity;
use Swoft\Db\Bean\Annotation\Id;
use Swoft\Db\Bean\Annotation\Required;
use Swoft\Db\Bean\Annotation\Table;
use Swoft\Db\Types;

/**
 * 异动

 * @Entity()
 * @Table(name="stock_tops_vary")
 * @uses      StockTopsVary
 */
class StockTopsVary extends Model
{
    /**
     * @var int $id 
     * @Id()
     * @Column(name="id", type="bigint")
     */
    private $id;

    /**
     * @var string $bkCode 版块代码
     * @Column(name="bk_code", type="string", length=12, default="")
     */
    private $bkCode;

    /**
     * @var string $name 版块名称
     * @Column(name="name", type="string", length=32, default="")
     */
    private $name;

    /**
     * @var string $time 时间
     * @Column(name="time", type="string", length=12, default="")
     */
    private $time;

    /**
     * @var string $wording 描述
     * @Column(name="wording", type="string", length=32, default="")
     */
    private $wording;

    /**
     * @var string $stocks 
     * @Column(name="stocks", type="json")
     */
    private $stocks;

    /**
     * @var string $date 日期
     * @Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var string $createTime 创建时间
     * @Column(name="create_time", type="timestamp", default="CURRENT_TIMESTAMP")
     */
    private $createTime;

    /**
     * @var string $updateTime 更新时间
     * @Column(name="update_time", type="timestamp", default="CURRENT_TIMESTAMP")
     */
    private $updateTime;

    /**
     * @param int $value
     * @return $this
     */
    public function setId(int $value)
    {
        $this->id = $value;

        return $this;
    }

    /**
     * 版块代码
     * @param string $value
     * @return $this
     */
    public function setBkCode(string $value): self
    {
        $this->bkCode = $value;

        return $this;
    }

    /**
     * 版块名称
     * @param string $value
     * @return $this
     */
    public function setName(string $value): self
    {
        $this->name = $value;

        return $this;
    }

    /**
     * 时间
     * @param string $value
     * @return $this
     */
    public function setTime(string $value): self
    {
        $this->time = $value;

        return $this;
    }

    /**
     * 描述
     * @param string $value
     * @return $this
     */
    public function setWording(string $value): self
    {
        $this->wording = $value;

        return $this;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setStocks(string $value): self
    {
        $this->stocks = $value;

        return $this;
    }

    /**
     * 日期
     * @param string $value
     * @return $this
     */
    public function setDate(string $value): self
    {
        $this->date = $value;

        return $this;
    }

    /**
     * 创建时间
     * @param string $value
     * @return $this
     */
    public function setCreateTime(string $value): self
    {
        $this->createTime = $value;

        return $this;
    }

    /**
     * 更新时间
     * @param string $value
     * @return $this
     */
    public function setUpdateTime(string $value): self
    {
        $this->updateTime = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * 版块代码
     * @return string
     */
    public function getBkCode()
    {
        return $this->bkCode;
    }

    /**
     * 版块名称
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * 时间
     * @return string
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * 描述
     * @return string
     */
    public function getWording()
    {
        return $this->wording;
    }

    /**
     * @return string
     */
    public function getStocks()
    {
        return $this->stocks;
    }

    /**
     * 日期
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * 创建时间
     * @return mixed
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }

    /**
     * 更新时间
     * @return mixed
     */
    public function getUpdateTime()
    {
        return $this->updateTime;
    }

}
