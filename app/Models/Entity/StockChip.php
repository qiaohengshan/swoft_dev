<?php
namespace App\Models\Entity;

use Swoft\Db\Model;
use Swoft\Db\Bean\Annotation\Column;
use Swoft\Db\Bean\Annotation\Entity;
use Swoft\Db\Bean\Annotation\Id;
use Swoft\Db\Bean\Annotation\Required;
use Swoft\Db\Bean\Annotation\Table;
use Swoft\Db\Types;

/**
 * 筹码

 * @Entity()
 * @Table(name="stock_chip")
 * @uses      StockChip
 */
class StockChip extends Model
{
    /**
     * @var int $id 
     * @Id()
     * @Column(name="id", type="bigint")
     */
    private $id;

    /**
     * @var int $stockId stock id
     * @Column(name="stock_id", type="integer", default=0)
     */
    private $stockId;

    /**
     * @var string $date 日期
     * @Column(name="date", type="date")
     */
    private $date;

    /**
     * @var float $closePrice 当前价
     * @Column(name="close_price", type="decimal", default=0)
     */
    private $closePrice;

    /**
     * @var string $distribution 筹码分布
     * @Column(name="distribution", type="json")
     */
    private $distribution;

    /**
     * @var float $mainCost 主力成本
     * @Column(name="main_cost", type="decimal", default=0)
     */
    private $mainCost;

    /**
     * @var float $avgCost 平均成本
     * @Column(name="avg_cost", type="decimal", default=0)
     */
    private $avgCost;

    /**
     * @var float $profitRatio 获利比例
     * @Column(name="profit_ratio", type="decimal", default=0)
     */
    private $profitRatio;

    /**
     * @var float $avgProfit 平均获利
     * @Column(name="avg_profit", type="decimal", default=0)
     */
    private $avgProfit;

    /**
     * @var float $distributionMin 筹码最低价
     * @Column(name="distribution_min", type="decimal", default=0)
     */
    private $distributionMin;

    /**
     * @var float $distributionMax 筹码最高价
     * @Column(name="distribution_max", type="decimal", default=0)
     */
    private $distributionMax;

    /**
     * @var string $chipFocus90 90%筹码
     * @Column(name="chip_focus_90", type="json")
     */
    private $chipFocus90;

    /**
     * @var string $chipFocus70 70%筹码
     * @Column(name="chip_focus_70", type="json")
     */
    private $chipFocus70;

    /**
     * @var string $profitLevel 获利情况,mild_profit轻度获利,heavy_profit重度获利,mild_loss轻度套牢,heavy_loss重度套牢
     * @Column(name="profit_level", type="json")
     */
    private $profitLevel;

    /**
     * @var float $denseLine 筹码密集线
     * @Column(name="dense_line", type="decimal", default=0)
     */
    private $denseLine;

    /**
     * @var float $chipDescLine 筹码密集线
     * @Column(name="chip_desc_line", type="decimal", default=0)
     */
    private $chipDescLine;

    /**
     * @var int $chipDescLineRate 筹码密集线
     * @Column(name="chip_desc_line_rate", type="integer", default=0)
     */
    private $chipDescLineRate;

    /**
     * @var string $denseAreas 密集区域
     * @Column(name="dense_areas", type="json")
     */
    private $denseAreas;

    /**
     * @var string $maxDenseArea 最大密集区域
     * @Column(name="max_dense_area", type="json")
     */
    private $maxDenseArea;

    /**
     * @var string $createTime 创建时间
     * @Column(name="create_time", type="timestamp", default="CURRENT_TIMESTAMP")
     */
    private $createTime;

    /**
     * @var string $updateTime 
     * @Column(name="update_time", type="timestamp", default="CURRENT_TIMESTAMP")
     */
    private $updateTime;

    /**
     * @var string $chipDesc 
     * @Column(name="chip_desc", type="string", length=128, default="")
     */
    private $chipDesc;

    /**
     * @param int $value
     * @return $this
     */
    public function setId(int $value)
    {
        $this->id = $value;

        return $this;
    }

    /**
     * stock id
     * @param int $value
     * @return $this
     */
    public function setStockId(int $value): self
    {
        $this->stockId = $value;

        return $this;
    }

    /**
     * 日期
     * @param string $value
     * @return $this
     */
    public function setDate(string $value): self
    {
        $this->date = $value;

        return $this;
    }

    /**
     * 当前价
     * @param float $value
     * @return $this
     */
    public function setClosePrice(float $value): self
    {
        $this->closePrice = $value;

        return $this;
    }

    /**
     * 筹码分布
     * @param string $value
     * @return $this
     */
    public function setDistribution(string $value): self
    {
        $this->distribution = $value;

        return $this;
    }

    /**
     * 主力成本
     * @param float $value
     * @return $this
     */
    public function setMainCost(float $value): self
    {
        $this->mainCost = $value;

        return $this;
    }

    /**
     * 平均成本
     * @param float $value
     * @return $this
     */
    public function setAvgCost(float $value): self
    {
        $this->avgCost = $value;

        return $this;
    }

    /**
     * 获利比例
     * @param float $value
     * @return $this
     */
    public function setProfitRatio(float $value): self
    {
        $this->profitRatio = $value;

        return $this;
    }

    /**
     * 平均获利
     * @param float $value
     * @return $this
     */
    public function setAvgProfit(float $value): self
    {
        $this->avgProfit = $value;

        return $this;
    }

    /**
     * 筹码最低价
     * @param float $value
     * @return $this
     */
    public function setDistributionMin(float $value): self
    {
        $this->distributionMin = $value;

        return $this;
    }

    /**
     * 筹码最高价
     * @param float $value
     * @return $this
     */
    public function setDistributionMax(float $value): self
    {
        $this->distributionMax = $value;

        return $this;
    }

    /**
     * 90%筹码
     * @param string $value
     * @return $this
     */
    public function setChipFocus90(string $value): self
    {
        $this->chipFocus90 = $value;

        return $this;
    }

    /**
     * 70%筹码
     * @param string $value
     * @return $this
     */
    public function setChipFocus70(string $value): self
    {
        $this->chipFocus70 = $value;

        return $this;
    }

    /**
     * 获利情况,mild_profit轻度获利,heavy_profit重度获利,mild_loss轻度套牢,heavy_loss重度套牢
     * @param string $value
     * @return $this
     */
    public function setProfitLevel(string $value): self
    {
        $this->profitLevel = $value;

        return $this;
    }

    /**
     * 筹码密集线
     * @param float $value
     * @return $this
     */
    public function setDenseLine(float $value): self
    {
        $this->denseLine = $value;

        return $this;
    }

    /**
     * 筹码密集线
     * @param float $value
     * @return $this
     */
    public function setChipDescLine(float $value): self
    {
        $this->chipDescLine = $value;

        return $this;
    }

    /**
     * 筹码密集线
     * @param int $value
     * @return $this
     */
    public function setChipDescLineRate(int $value): self
    {
        $this->chipDescLineRate = $value;

        return $this;
    }

    /**
     * 密集区域
     * @param string $value
     * @return $this
     */
    public function setDenseAreas(string $value): self
    {
        $this->denseAreas = $value;

        return $this;
    }

    /**
     * 最大密集区域
     * @param string $value
     * @return $this
     */
    public function setMaxDenseArea(string $value): self
    {
        $this->maxDenseArea = $value;

        return $this;
    }

    /**
     * 创建时间
     * @param string $value
     * @return $this
     */
    public function setCreateTime(string $value): self
    {
        $this->createTime = $value;

        return $this;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setUpdateTime(string $value): self
    {
        $this->updateTime = $value;

        return $this;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setChipDesc(string $value): self
    {
        $this->chipDesc = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * stock id
     * @return int
     */
    public function getStockId()
    {
        return $this->stockId;
    }

    /**
     * 日期
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * 当前价
     * @return mixed
     */
    public function getClosePrice()
    {
        return $this->closePrice;
    }

    /**
     * 筹码分布
     * @return string
     */
    public function getDistribution()
    {
        return $this->distribution;
    }

    /**
     * 主力成本
     * @return mixed
     */
    public function getMainCost()
    {
        return $this->mainCost;
    }

    /**
     * 平均成本
     * @return mixed
     */
    public function getAvgCost()
    {
        return $this->avgCost;
    }

    /**
     * 获利比例
     * @return mixed
     */
    public function getProfitRatio()
    {
        return $this->profitRatio;
    }

    /**
     * 平均获利
     * @return mixed
     */
    public function getAvgProfit()
    {
        return $this->avgProfit;
    }

    /**
     * 筹码最低价
     * @return mixed
     */
    public function getDistributionMin()
    {
        return $this->distributionMin;
    }

    /**
     * 筹码最高价
     * @return mixed
     */
    public function getDistributionMax()
    {
        return $this->distributionMax;
    }

    /**
     * 90%筹码
     * @return string
     */
    public function getChipFocus90()
    {
        return $this->chipFocus90;
    }

    /**
     * 70%筹码
     * @return string
     */
    public function getChipFocus70()
    {
        return $this->chipFocus70;
    }

    /**
     * 获利情况,mild_profit轻度获利,heavy_profit重度获利,mild_loss轻度套牢,heavy_loss重度套牢
     * @return string
     */
    public function getProfitLevel()
    {
        return $this->profitLevel;
    }

    /**
     * 筹码密集线
     * @return mixed
     */
    public function getDenseLine()
    {
        return $this->denseLine;
    }

    /**
     * 筹码密集线
     * @return mixed
     */
    public function getChipDescLine()
    {
        return $this->chipDescLine;
    }

    /**
     * 筹码密集线
     * @return int
     */
    public function getChipDescLineRate()
    {
        return $this->chipDescLineRate;
    }

    /**
     * 密集区域
     * @return string
     */
    public function getDenseAreas()
    {
        return $this->denseAreas;
    }

    /**
     * 最大密集区域
     * @return string
     */
    public function getMaxDenseArea()
    {
        return $this->maxDenseArea;
    }

    /**
     * 创建时间
     * @return mixed
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }

    /**
     * @return mixed
     */
    public function getUpdateTime()
    {
        return $this->updateTime;
    }

    /**
     * @return string
     */
    public function getChipDesc()
    {
        return $this->chipDesc;
    }

}
