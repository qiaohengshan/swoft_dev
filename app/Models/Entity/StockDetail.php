<?php
namespace App\Models\Entity;

use Swoft\Db\Model;
use Swoft\Db\Bean\Annotation\Column;
use Swoft\Db\Bean\Annotation\Entity;
use Swoft\Db\Bean\Annotation\Id;
use Swoft\Db\Bean\Annotation\Required;
use Swoft\Db\Bean\Annotation\Table;
use Swoft\Db\Types;

/**
 * @Entity()
 * @Table(name="stock_detail")
 * @uses      StockDetail
 */
class StockDetail extends Model
{
    /**
     * @var int $stockId 
     * @Id()
     * @Column(name="stock_id", type="integer")
     */
    private $stockId;

    /**
     * @var float $open 
     * @Column(name="open", type="decimal", default=0)
     */
    private $open;

    /**
     * @var float $curPrice 
     * @Column(name="cur_price", type="decimal", default=0)
     */
    private $curPrice;

    /**
     * @var float $high 
     * @Column(name="high", type="decimal", default=0)
     */
    private $high;

    /**
     * @var float $low 
     * @Column(name="low", type="decimal", default=0)
     */
    private $low;

    /**
     * @var float $preClose 
     * @Column(name="pre_close", type="decimal", default=0)
     */
    private $preClose;

    /**
     * @var float $limitUp 
     * @Column(name="limit_up", type="decimal", default=0)
     */
    private $limitUp;

    /**
     * @var float $limitDown 
     * @Column(name="limit_down", type="decimal", default=0)
     */
    private $limitDown;

    /**
     * @var float $amplitude 
     * @Column(name="amplitude", type="decimal", default=0)
     */
    private $amplitude;

    /**
     * @var float $bidRatio 
     * @Column(name="bid_ratio", type="decimal", default=0)
     */
    private $bidRatio;

    /**
     * @var float $turnoverNum 
     * @Column(name="turnover_num", type="decimal", default=0)
     */
    private $turnoverNum;

    /**
     * @var int $volumeNum 
     * @Column(name="volume_num", type="bigint", default=0)
     */
    private $volumeNum;

    /**
     * @var string $state 
     * @Column(name="state", type="string", length=8, default="")
     */
    private $state;

    /**
     * @var float $change 
     * @Column(name="change", type="decimal", default=0)
     */
    private $change;

    /**
     * @var float $changeRate 
     * @Column(name="change_rate", type="decimal", default=0)
     */
    private $changeRate;

    /**
     * @var float $eps 
     * @Column(name="eps", type="decimal", default=0)
     */
    private $eps;

    /**
     * @var string $epsKey key
     * @Column(name="eps_key", type="string", length=16, default="")
     */
    private $epsKey;

    /**
     * @var float $pb 
     * @Column(name="pb", type="decimal", default=0)
     */
    private $pb;

    /**
     * @var float $pe ,
     * @Column(name="pe", type="decimal", default=0)
     */
    private $pe;

    /**
     * @var float $peNum ,
     * @Column(name="pe_num", type="decimal", default=0)
     */
    private $peNum;

    /**
     * @var float $peSta ()
     * @Column(name="pe_sta", type="decimal", default=0)
     */
    private $peSta;

    /**
     * @var string $peTtm 
     * @Column(name="pe_ttm", type="string", length=16, default="")
     */
    private $peTtm;

    /**
     * @var float $circulationMarketValue ,
     * @Column(name="circulation_market_value", type="decimal", default=0)
     */
    private $circulationMarketValue;

    /**
     * @var float $marketValue ,
     * @Column(name="market_value", type="decimal", default=0)
     */
    private $marketValue;

    /**
     * @var float $floatShares ,
     * @Column(name="float_shares", type="decimal", default=0)
     */
    private $floatShares;

    /**
     * @var float $totalShares ,
     * @Column(name="total_shares", type="decimal", default=0)
     */
    private $totalShares;

    /**
     * @var int $createTime 
     * @Column(name="create_time", type="integer", default=0)
     */
    private $createTime;

    /**
     * @var int $updateTime 
     * @Column(name="update_time", type="integer", default=0)
     */
    private $updateTime;

    /**
     * @param int $value
     * @return $this
     */
    public function setStockId(int $value)
    {
        $this->stockId = $value;

        return $this;
    }

    /**
     * @param float $value
     * @return $this
     */
    public function setOpen(float $value): self
    {
        $this->open = $value;

        return $this;
    }

    /**
     * @param float $value
     * @return $this
     */
    public function setCurPrice(float $value): self
    {
        $this->curPrice = $value;

        return $this;
    }

    /**
     * @param float $value
     * @return $this
     */
    public function setHigh(float $value): self
    {
        $this->high = $value;

        return $this;
    }

    /**
     * @param float $value
     * @return $this
     */
    public function setLow(float $value): self
    {
        $this->low = $value;

        return $this;
    }

    /**
     * @param float $value
     * @return $this
     */
    public function setPreClose(float $value): self
    {
        $this->preClose = $value;

        return $this;
    }

    /**
     * @param float $value
     * @return $this
     */
    public function setLimitUp(float $value): self
    {
        $this->limitUp = $value;

        return $this;
    }

    /**
     * @param float $value
     * @return $this
     */
    public function setLimitDown(float $value): self
    {
        $this->limitDown = $value;

        return $this;
    }

    /**
     * @param float $value
     * @return $this
     */
    public function setAmplitude(float $value): self
    {
        $this->amplitude = $value;

        return $this;
    }

    /**
     * @param float $value
     * @return $this
     */
    public function setBidRatio(float $value): self
    {
        $this->bidRatio = $value;

        return $this;
    }

    /**
     * @param float $value
     * @return $this
     */
    public function setTurnoverNum(float $value): self
    {
        $this->turnoverNum = $value;

        return $this;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setVolumeNum(int $value): self
    {
        $this->volumeNum = $value;

        return $this;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setState(string $value): self
    {
        $this->state = $value;

        return $this;
    }

    /**
     * @param float $value
     * @return $this
     */
    public function setChange(float $value): self
    {
        $this->change = $value;

        return $this;
    }

    /**
     * @param float $value
     * @return $this
     */
    public function setChangeRate(float $value): self
    {
        $this->changeRate = $value;

        return $this;
    }

    /**
     * @param float $value
     * @return $this
     */
    public function setEps(float $value): self
    {
        $this->eps = $value;

        return $this;
    }

    /**
     * key
     * @param string $value
     * @return $this
     */
    public function setEpsKey(string $value): self
    {
        $this->epsKey = $value;

        return $this;
    }

    /**
     * @param float $value
     * @return $this
     */
    public function setPb(float $value): self
    {
        $this->pb = $value;

        return $this;
    }

    /**
     * ,
     * @param float $value
     * @return $this
     */
    public function setPe(float $value): self
    {
        $this->pe = $value;

        return $this;
    }

    /**
     * ,
     * @param float $value
     * @return $this
     */
    public function setPeNum(float $value): self
    {
        $this->peNum = $value;

        return $this;
    }

    /**
     * ()
     * @param float $value
     * @return $this
     */
    public function setPeSta(float $value): self
    {
        $this->peSta = $value;

        return $this;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setPeTtm(string $value): self
    {
        $this->peTtm = $value;

        return $this;
    }

    /**
     * ,
     * @param float $value
     * @return $this
     */
    public function setCirculationMarketValue(float $value): self
    {
        $this->circulationMarketValue = $value;

        return $this;
    }

    /**
     * ,
     * @param float $value
     * @return $this
     */
    public function setMarketValue(float $value): self
    {
        $this->marketValue = $value;

        return $this;
    }

    /**
     * ,
     * @param float $value
     * @return $this
     */
    public function setFloatShares(float $value): self
    {
        $this->floatShares = $value;

        return $this;
    }

    /**
     * ,
     * @param float $value
     * @return $this
     */
    public function setTotalShares(float $value): self
    {
        $this->totalShares = $value;

        return $this;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setCreateTime(int $value): self
    {
        $this->createTime = $value;

        return $this;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setUpdateTime(int $value): self
    {
        $this->updateTime = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStockId()
    {
        return $this->stockId;
    }

    /**
     * @return mixed
     */
    public function getOpen()
    {
        return $this->open;
    }

    /**
     * @return mixed
     */
    public function getCurPrice()
    {
        return $this->curPrice;
    }

    /**
     * @return mixed
     */
    public function getHigh()
    {
        return $this->high;
    }

    /**
     * @return mixed
     */
    public function getLow()
    {
        return $this->low;
    }

    /**
     * @return mixed
     */
    public function getPreClose()
    {
        return $this->preClose;
    }

    /**
     * @return mixed
     */
    public function getLimitUp()
    {
        return $this->limitUp;
    }

    /**
     * @return mixed
     */
    public function getLimitDown()
    {
        return $this->limitDown;
    }

    /**
     * @return mixed
     */
    public function getAmplitude()
    {
        return $this->amplitude;
    }

    /**
     * @return mixed
     */
    public function getBidRatio()
    {
        return $this->bidRatio;
    }

    /**
     * @return mixed
     */
    public function getTurnoverNum()
    {
        return $this->turnoverNum;
    }

    /**
     * @return int
     */
    public function getVolumeNum()
    {
        return $this->volumeNum;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return mixed
     */
    public function getChange()
    {
        return $this->change;
    }

    /**
     * @return mixed
     */
    public function getChangeRate()
    {
        return $this->changeRate;
    }

    /**
     * @return mixed
     */
    public function getEps()
    {
        return $this->eps;
    }

    /**
     * key
     * @return string
     */
    public function getEpsKey()
    {
        return $this->epsKey;
    }

    /**
     * @return mixed
     */
    public function getPb()
    {
        return $this->pb;
    }

    /**
     * ,
     * @return mixed
     */
    public function getPe()
    {
        return $this->pe;
    }

    /**
     * ,
     * @return mixed
     */
    public function getPeNum()
    {
        return $this->peNum;
    }

    /**
     * ()
     * @return mixed
     */
    public function getPeSta()
    {
        return $this->peSta;
    }

    /**
     * @return string
     */
    public function getPeTtm()
    {
        return $this->peTtm;
    }

    /**
     * ,
     * @return mixed
     */
    public function getCirculationMarketValue()
    {
        return $this->circulationMarketValue;
    }

    /**
     * ,
     * @return mixed
     */
    public function getMarketValue()
    {
        return $this->marketValue;
    }

    /**
     * ,
     * @return mixed
     */
    public function getFloatShares()
    {
        return $this->floatShares;
    }

    /**
     * ,
     * @return mixed
     */
    public function getTotalShares()
    {
        return $this->totalShares;
    }

    /**
     * @return int
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }

    /**
     * @return int
     */
    public function getUpdateTime()
    {
        return $this->updateTime;
    }

}
