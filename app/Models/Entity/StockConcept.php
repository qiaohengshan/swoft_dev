<?php
namespace App\Models\Entity;

use Swoft\Db\Model;
use Swoft\Db\Bean\Annotation\Column;
use Swoft\Db\Bean\Annotation\Entity;
use Swoft\Db\Bean\Annotation\Id;
use Swoft\Db\Bean\Annotation\Required;
use Swoft\Db\Bean\Annotation\Table;
use Swoft\Db\Types;

/**
 * 股票所属概念

 * @Entity()
 * @Table(name="stock_concept")
 * @uses      StockConcept
 */
class StockConcept extends Model
{
    /**
     * @var int $id 
     * @Id()
     * @Column(name="id", type="integer")
     */
    private $id;

    /**
     * @var int $stockId 股票id
     * @Column(name="stock_id", type="integer", default=0)
     */
    private $stockId;

    /**
     * @var string $name 名称
     * @Column(name="name", type="string", length=32, default="")
     */
    private $name;

    /**
     * @var string $symbol 符号
     * @Column(name="symbol", type="string", length=16)
     */
    private $symbol;

    /**
     * @var string $code 代码
     * @Column(name="code", type="string", length=32, default="")
     */
    private $code;

    /**
     * @var int $type 
     * @Column(name="type", type="integer", default=0)
     */
    private $type;

    /**
     * @var int $isHot 是否热门,1是,0不是
     * @Column(name="is_hot", type="tinyint", default=1)
     */
    private $isHot;

    /**
     * @var int $createTime 创建时间
     * @Column(name="create_time", type="integer", default=0)
     */
    private $createTime;

    /**
     * @var int $updateTime 更新时间
     * @Column(name="update_time", type="integer", default=0)
     */
    private $updateTime;

    /**
     * @param int $value
     * @return $this
     */
    public function setId(int $value)
    {
        $this->id = $value;

        return $this;
    }

    /**
     * 股票id
     * @param int $value
     * @return $this
     */
    public function setStockId(int $value): self
    {
        $this->stockId = $value;

        return $this;
    }

    /**
     * 名称
     * @param string $value
     * @return $this
     */
    public function setName(string $value): self
    {
        $this->name = $value;

        return $this;
    }

    /**
     * 符号
     * @param string $value
     * @return $this
     */
    public function setSymbol(string $value): self
    {
        $this->symbol = $value;

        return $this;
    }

    /**
     * 代码
     * @param string $value
     * @return $this
     */
    public function setCode(string $value): self
    {
        $this->code = $value;

        return $this;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setType(int $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * 是否热门,1是,0不是
     * @param int $value
     * @return $this
     */
    public function setIsHot(int $value): self
    {
        $this->isHot = $value;

        return $this;
    }

    /**
     * 创建时间
     * @param int $value
     * @return $this
     */
    public function setCreateTime(int $value): self
    {
        $this->createTime = $value;

        return $this;
    }

    /**
     * 更新时间
     * @param int $value
     * @return $this
     */
    public function setUpdateTime(int $value): self
    {
        $this->updateTime = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * 股票id
     * @return int
     */
    public function getStockId()
    {
        return $this->stockId;
    }

    /**
     * 名称
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * 符号
     * @return string
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * 代码
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * 是否热门,1是,0不是
     * @return mixed
     */
    public function getIsHot()
    {
        return $this->isHot;
    }

    /**
     * 创建时间
     * @return int
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }

    /**
     * 更新时间
     * @return int
     */
    public function getUpdateTime()
    {
        return $this->updateTime;
    }

}
