<?php
namespace App\Models\Entity;

use Swoft\Db\Model;
use Swoft\Db\Bean\Annotation\Column;
use Swoft\Db\Bean\Annotation\Entity;
use Swoft\Db\Bean\Annotation\Id;
use Swoft\Db\Bean\Annotation\Required;
use Swoft\Db\Bean\Annotation\Table;
use Swoft\Db\Types;

/**
 * 每分钟成交数据

 * @Entity()
 * @Table(name="stock_trade_detail_minute")
 * @uses      StockTradeDetailMinute
 */
class StockTradeDetailMinute extends Model
{
    /**
     * @var int $id
     * @Id()
     * @Column(name="id", type="integer")
     */
    private $id;

    /**
     * @var int $stockId 股票id
     * @Column(name="stock_id", type="integer", default=0)
     */
    private $stockId;

    /**
     * @var string $code 股票编码
     * @Column(name="code", type="string", length=16, default="")
     */
    private $code;

    /**
     * @var string $symbol 标示
     * @Column(name="symbol", type="string", length=16, default="")
     */
    private $symbol;

    /**
     * @var float $price 成交价
     * @Column(name="price", type="decimal", default=0)
     */
    private $price;

    /**
     * @var float $turnover 成交额/元
     * @Column(name="turnover", type="decimal", default=0)
     */
    private $turnover;

    /**
     * @var int $volume 成交量/手
     * @Column(name="volume", type="bigint", default=0)
     */
    private $volume;

    /**
     * @var string $time 时间
     * @Column(name="time", type="char", length=8, default="")
     */
    private $time;

    /**
     * @var int $date 日期
     * @Column(name="date", type="integer", default=0)
     */
    private $date;

    /**
     * @param int $value
     * @return $this
     */
    public function setId(int $value)
    {
        $this->id = $value;

        return $this;
    }

    /**
     * 股票id
     * @param int $value
     * @return $this
     */
    public function setStockId(int $value): self
    {
        $this->stockId = $value;

        return $this;
    }

    /**
     * 股票编码
     * @param string $value
     * @return $this
     */
    public function setCode(string $value): self
    {
        $this->code = $value;

        return $this;
    }

    /**
     * 标示
     * @param string $value
     * @return $this
     */
    public function setSymbol(string $value): self
    {
        $this->symbol = $value;

        return $this;
    }

    /**
     * 成交价
     * @param float $value
     * @return $this
     */
    public function setPrice(float $value): self
    {
        $this->price = $value;

        return $this;
    }

    /**
     * 成交额/元
     * @param float $value
     * @return $this
     */
    public function setTurnover(float $value): self
    {
        $this->turnover = $value;

        return $this;
    }

    /**
     * 成交量/手
     * @param int $value
     * @return $this
     */
    public function setVolume(int $value): self
    {
        $this->volume = $value;

        return $this;
    }

    /**
     * 时间
     * @param string $value
     * @return $this
     */
    public function setTime(string $value): self
    {
        $this->time = $value;

        return $this;
    }

    /**
     * 日期
     * @param int $value
     * @return $this
     */
    public function setDate(int $value): self
    {
        $this->date = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * 股票id
     * @return int
     */
    public function getStockId()
    {
        return $this->stockId;
    }

    /**
     * 股票编码
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * 标示
     * @return string
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * 成交价
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * 成交额/元
     * @return mixed
     */
    public function getTurnover()
    {
        return $this->turnover;
    }

    /**
     * 成交量/手
     * @return int
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * 时间
     * @return string
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * 日期
     * @return int
     */
    public function getDate()
    {
        return $this->date;
    }

}