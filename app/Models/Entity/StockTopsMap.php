<?php
namespace App\Models\Entity;

use Swoft\Db\Model;
use Swoft\Db\Bean\Annotation\Column;
use Swoft\Db\Bean\Annotation\Entity;
use Swoft\Db\Bean\Annotation\Id;
use Swoft\Db\Bean\Annotation\Required;
use Swoft\Db\Bean\Annotation\Table;
use Swoft\Db\Types;

/**
 * 热门概念推荐股票

 * @Entity()
 * @Table(name="stock_tops_map")
 * @uses      StockTopsMap
 */
class StockTopsMap extends Model
{
    /**
     * @var int $id
     * @Id()
     * @Column(name="id", type="bigint")
     */
    private $id;

    /**
     * @var string $code 概念代码
     * @Column(name="code", type="string", length=32, default="")
     */
    private $code;

    /**
     * @var string $name 概念名称
     * @Column(name="name", type="string", length=32, default="")
     */
    private $name;

    /**
     * @var int $stockId 股票id
     * @Column(name="stock_id", type="integer", default=0)
     */
    private $stockId;

    /**
     * @var string $createTime 创建时间
     * @Column(name="create_time", type="date")
     */
    private $createTime;

    /**
     * @var string $updateTime 更新时间
     * @Column(name="update_time", type="date")
     */
    private $updateTime;

    /**
     * @param int $value
     * @return $this
     */
    public function setId(int $value)
    {
        $this->id = $value;

        return $this;
    }

    /**
     * 概念代码
     * @param string $value
     * @return $this
     */
    public function setCode(string $value): self
    {
        $this->code = $value;

        return $this;
    }

    /**
     * 概念名称
     * @param string $value
     * @return $this
     */
    public function setName(string $value): self
    {
        $this->name = $value;

        return $this;
    }

    /**
     * 股票id
     * @param int $value
     * @return $this
     */
    public function setStockId(int $value): self
    {
        $this->stockId = $value;

        return $this;
    }

    /**
     * 创建时间
     * @param string $value
     * @return $this
     */
    public function setCreateTime(string $value): self
    {
        $this->createTime = $value;

        return $this;
    }

    /**
     * 创建时间
     * @param string $value
     * @return $this
     */
    public function setUpdateTime(string $value): self
    {
        $this->updateTime = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * 概念代码
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * 概念名称
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * 股票id
     * @return int
     */
    public function getStockId()
    {
        return $this->stockId;
    }

    /**
     * 创建时间
     * @return string
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }

    /**
     * 更新时间
     * @return string
     */
    public function getUpdateTime()
    {
        return $this->updateTime;
    }

}
