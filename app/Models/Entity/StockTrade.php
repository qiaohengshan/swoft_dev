<?php
namespace App\Models\Entity;

use Swoft\Db\Model;
use Swoft\Db\Bean\Annotation\Column;
use Swoft\Db\Bean\Annotation\Entity;
use Swoft\Db\Bean\Annotation\Id;
use Swoft\Db\Bean\Annotation\Required;
use Swoft\Db\Bean\Annotation\Table;
use Swoft\Db\Types;

/**
 * 每天交易数据

 * @Entity()
 * @Table(name="stock_trade")
 * @uses      StockTrade
 */
class StockTrade extends Model
{
    /**
     * @var int $id
     * @Id()
     * @Column(name="id", type="integer")
     */
    private $id;

    /**
     * @var int $stockId 股票id
     * @Column(name="stock_id", type="integer", default=0)
     */
    private $stockId;

    /**
     * @var string $code 股票编码
     * @Column(name="code", type="string", length=16, default="")
     */
    private $code;

    /**
     * @var string $symbol 标示
     * @Column(name="symbol", type="string", length=16, default="")
     */
    private $symbol;

    /**
     * @var float $open 开盘价
     * @Column(name="open", type="decimal")
     */
    private $open;

    /**
     * @var float $close 成交价
     * @Column(name="close", type="decimal", default=0)
     */
    private $close;

    /**
     * @var float $priceInc 价格波动
     * @Column(name="price_inc", type="decimal", default=0)
     */
    private $priceInc;

    /**
     * @var float $pricePre 成交前价格
     * @Column(name="price_pre", type="decimal", default=0)
     */
    private $pricePre;

    /**
     * @var float $low 最低
     * @Column(name="low", type="decimal")
     */
    private $low;

    /**
     * @var float $high 最高
     * @Column(name="high", type="decimal")
     */
    private $high;

    /**
     * @var float $turnover 成交额/元
     * @Column(name="turnover", type="decimal", default=0)
     */
    private $turnover;

    /**
     * @var int $volume 成交量/手
     * @Column(name="volume", type="integer", default=0)
     */
    private $volume;

    /**
     * @var float $volumeRatio 量比
     * @Column(name="volume_ratio", type="decimal")
     */
    private $volumeRatio;

    /**
     * @var float $turnoverRate 换手率
     * @Column(name="turnover_rate", type="decimal")
     */
    private $turnoverRate;

    /**
     * @var string $date 日期
     * @Column(name="date", type="date")
     */
    private $date;

    /**
     * @var string $createTime 创建时间
     * @Column(name="create_time", type="datetime")
     */
    private $createTime;

    /**
     * @var string $updateTime 更新时间
     * @Column(name="update_time", type="datetime")
     */
    private $updateTime;

    /**
     * @param int $value
     * @return $this
     */
    public function setId(int $value)
    {
        $this->id = $value;

        return $this;
    }

    /**
     * 股票id
     * @param int $value
     * @return $this
     */
    public function setStockId(int $value): self
    {
        $this->stockId = $value;

        return $this;
    }

    /**
     * 股票编码
     * @param string $value
     * @return $this
     */
    public function setCode(string $value): self
    {
        $this->code = $value;

        return $this;
    }

    /**
     * 标示
     * @param string $value
     * @return $this
     */
    public function setSymbol(string $value): self
    {
        $this->symbol = $value;

        return $this;
    }

    /**
     * 开盘价
     * @param float $value
     * @return $this
     */
    public function setOpen(float $value): self
    {
        $this->open = $value;

        return $this;
    }

    /**
     * 成交价
     * @param float $value
     * @return $this
     */
    public function setClose(float $value): self
    {
        $this->close = $value;

        return $this;
    }

    /**
     * 价格波动
     * @param float $value
     * @return $this
     */
    public function setPriceInc(float $value): self
    {
        $this->priceInc = $value;

        return $this;
    }

    /**
     * 成交前价格
     * @param float $value
     * @return $this
     */
    public function setPricePre(float $value): self
    {
        $this->pricePre = $value;

        return $this;
    }

    /**
     * 最低
     * @param float $value
     * @return $this
     */
    public function setLow(float $value): self
    {
        $this->low = $value;

        return $this;
    }

    /**
     * 最高
     * @param float $value
     * @return $this
     */
    public function setHigh(float $value): self
    {
        $this->high = $value;

        return $this;
    }

    /**
     * 成交额/元
     * @param float $value
     * @return $this
     */
    public function setTurnover(float $value): self
    {
        $this->turnover = $value;

        return $this;
    }

    /**
     * 成交量/手
     * @param int $value
     * @return $this
     */
    public function setVolume(int $value): self
    {
        $this->volume = $value;

        return $this;
    }

    /**
     * 量比
     * @param float $value
     * @return $this
     */
    public function setVolumeRatio(float $value): self
    {
        $this->volumeRatio = $value;

        return $this;
    }

    /**
     * 换手率
     * @param float $value
     * @return $this
     */
    public function setTurnoverRate(float $value): self
    {
        $this->turnoverRate = $value;

        return $this;
    }

    /**
     * 日期
     * @param string $value
     * @return $this
     */
    public function setDate(string $value): self
    {
        $this->date = $value;

        return $this;
    }

    /**
     * 创建时间
     * @param string $value
     * @return $this
     */
    public function setCreateTime(string $value): self
    {
        $this->createTime = $value;

        return $this;
    }

    /**
     * 更新时间
     * @param string $value
     * @return $this
     */
    public function setUpdateTime(string $value): self
    {
        $this->updateTime = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * 股票id
     * @return int
     */
    public function getStockId()
    {
        return $this->stockId;
    }

    /**
     * 股票编码
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * 标示
     * @return string
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * 开盘价
     * @return float
     */
    public function getOpen()
    {
        return $this->open;
    }

    /**
     * 成交价
     * @return mixed
     */
    public function getClose()
    {
        return $this->close;
    }

    /**
     * 价格波动
     * @return mixed
     */
    public function getPriceInc()
    {
        return $this->priceInc;
    }

    /**
     * 成交前价格
     * @return mixed
     */
    public function getPricePre()
    {
        return $this->pricePre;
    }

    /**
     * 最低
     * @return float
     */
    public function getLow()
    {
        return $this->low;
    }

    /**
     * 最高
     * @return float
     */
    public function getHigh()
    {
        return $this->high;
    }

    /**
     * 成交额/元
     * @return mixed
     */
    public function getTurnover()
    {
        return $this->turnover;
    }

    /**
     * 成交量/手
     * @return int
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * 量比
     * @return float
     */
    public function getVolumeRatio()
    {
        return $this->volumeRatio;
    }

    /**
     * 换手率
     * @return float
     */
    public function getTurnoverRate()
    {
        return $this->turnoverRate;
    }

    /**
     * 日期
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * 创建时间
     * @return string
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }

    /**
     * 更新时间
     * @return string
     */
    public function getUpdateTime()
    {
        return $this->updateTime;
    }

}
