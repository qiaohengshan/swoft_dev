<?php
namespace App\Models\Entity;

use Swoft\Db\Model;
use Swoft\Db\Bean\Annotation\Column;
use Swoft\Db\Bean\Annotation\Entity;
use Swoft\Db\Bean\Annotation\Id;
use Swoft\Db\Bean\Annotation\Required;
use Swoft\Db\Bean\Annotation\Table;
use Swoft\Db\Types;

/**
 * 持续增量

 * @Entity()
 * @Table(name="stock_increment")
 * @uses      StockIncrement
 */
class StockIncrement extends Model
{
    /**
     * @var int $id 
     * @Id()
     * @Column(name="id", type="integer")
     */
    private $id;

    /**
     * @var int $stockId stockID
     * @Column(name="stock_id", type="integer")
     * @Required()
     */
    private $stockId;

    /**
     * @var int $date 入选时间
     * @Column(name="date", type="integer", default=0)
     */
    private $date;

    /**
     * @param int $value
     * @return $this
     */
    public function setId(int $value)
    {
        $this->id = $value;

        return $this;
    }

    /**
     * stockID
     * @param int $value
     * @return $this
     */
    public function setStockId(int $value): self
    {
        $this->stockId = $value;

        return $this;
    }

    /**
     * 入选时间
     * @param int $value
     * @return $this
     */
    public function setDate(int $value): self
    {
        $this->date = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * stockID
     * @return int
     */
    public function getStockId()
    {
        return $this->stockId;
    }

    /**
     * 入选时间
     * @return int
     */
    public function getDate()
    {
        return $this->date;
    }

}
