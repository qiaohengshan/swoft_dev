<?php
namespace App\Models\Entity;

use Swoft\Db\Model;
use Swoft\Db\Bean\Annotation\Column;
use Swoft\Db\Bean\Annotation\Entity;
use Swoft\Db\Bean\Annotation\Id;
use Swoft\Db\Bean\Annotation\Required;
use Swoft\Db\Bean\Annotation\Table;
use Swoft\Db\Types;

/**
 * 今日热点

 * @Entity()
 * @Table(name="stock_tops")
 * @uses      StockTops
 */
class StockTops extends Model
{
    /**
     * @var int $id
     * @Id()
     * @Column(name="id", type="bigint")
     */
    private $id;

    /**
     * @var string $bkCode 版块代码
     * @Column(name="bk_code", type="string", length=16, default="")
     */
    private $bkCode;

    /**
     * @var string $name 版块名称
     * @Column(name="name", type="string", length=32, default="")
     */
    private $name;

    /**
     * @var string $symbol 版块符号
     * @Column(name="symbol", type="string", length=16, default="")
     */
    private $symbol;

    /**
     * @var int $consistency 持续度
     * @Column(name="consistency", type="tinyint", default=0)
     */
    private $consistency;

    /**
     * @var int $hot 热度
     * @Column(name="hot", type="tinyint", default=0)
     */
    private $hot;

    /**
     * @var int $type
     * @Column(name="type", type="integer", default=0)
     */
    private $type;

    /**
     * @var string $hypeRemark 描述
     * @Column(name="hype_remark", type="string", length=256, default="")
     */
    private $hypeRemark;

    /**
     * @var string $createdAt 创建时间
     * @Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var string $wording 当前状态描述
     * @Column(name="wording", type="string", length=64, default="")
     */
    private $wording;

    /**
     * @var string $time 时间
     * @Column(name="time", type="string", length=8)
     */
    private $time;

    /**
     * @var string $stocks 领涨股票
     * @Column(name="stocks", type="json")
     */
    private $stocks;

    /**
     * @param int $value
     * @return $this
     */
    public function setId(int $value)
    {
        $this->id = $value;

        return $this;
    }

    /**
     * 版块代码
     * @param string $value
     * @return $this
     */
    public function setBkCode(string $value): self
    {
        $this->bkCode = $value;

        return $this;
    }

    /**
     * 版块名称
     * @param string $value
     * @return $this
     */
    public function setName(string $value): self
    {
        $this->name = $value;

        return $this;
    }

    /**
     * 版块符号
     * @param string $value
     * @return $this
     */
    public function setSymbol(string $value): self
    {
        $this->symbol = $value;

        return $this;
    }

    /**
     * 持续度
     * @param int $value
     * @return $this
     */
    public function setConsistency(int $value): self
    {
        $this->consistency = $value;

        return $this;
    }

    /**
     * 热度
     * @param int $value
     * @return $this
     */
    public function setHot(int $value): self
    {
        $this->hot = $value;

        return $this;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setType(int $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * 描述
     * @param string $value
     * @return $this
     */
    public function setHypeRemark(string $value): self
    {
        $this->hypeRemark = $value;

        return $this;
    }

    /**
     * 创建时间
     * @param string $value
     * @return $this
     */
    public function setCreatedAt(string $value): self
    {
        $this->createdAt = $value;

        return $this;
    }

    /**
     * 当前状态描述
     * @param string $value
     * @return $this
     */
    public function setWording(string $value): self
    {
        $this->wording = $value;

        return $this;
    }

    /**
     * 时间
     * @param string $value
     * @return $this
     */
    public function setTime(string $value): self
    {
        $this->time = $value;

        return $this;
    }

    /**
     * 领涨股票
     * @param string $value
     * @return $this
     */
    public function setStocks(string $value): self
    {
        $this->stocks = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * 版块代码
     * @return string
     */
    public function getBkCode()
    {
        return $this->bkCode;
    }

    /**
     * 版块名称
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * 版块符号
     * @return string
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * 持续度
     * @return int
     */
    public function getConsistency()
    {
        return $this->consistency;
    }

    /**
     * 热度
     * @return int
     */
    public function getHot()
    {
        return $this->hot;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * 描述
     * @return string
     */
    public function getHypeRemark()
    {
        return $this->hypeRemark;
    }

    /**
     * 创建时间
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * 当前状态描述
     * @return string
     */
    public function getWording()
    {
        return $this->wording;
    }

    /**
     * 时间
     * @return string
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * 领涨股票
     * @return string
     */
    public function getStocks()
    {
        return $this->stocks;
    }

}
