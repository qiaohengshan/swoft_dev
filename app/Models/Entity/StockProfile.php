<?php
namespace App\Models\Entity;

use Swoft\Db\Model;
use Swoft\Db\Bean\Annotation\Column;
use Swoft\Db\Bean\Annotation\Entity;
use Swoft\Db\Bean\Annotation\Id;
use Swoft\Db\Bean\Annotation\Required;
use Swoft\Db\Bean\Annotation\Table;
use Swoft\Db\Types;

/**
 * 公司简介

 * @Entity()
 * @Table(name="stock_profile")
 * @uses      StockProfile
 */
class StockProfile extends Model
{
    /**
     * @var int $stockId 
     * @Id()
     * @Column(name="stock_id", type="integer")
     */
    private $stockId;

    /**
     * @var int $companyCode 公司代码
     * @Column(name="company_code", type="integer", default=0)
     */
    private $companyCode;

    /**
     * @var string $companyName 公司名称
     * @Column(name="company_name", type="string", length=256, default="")
     */
    private $companyName;

    /**
     * @var int $induCsrc 
     * @Column(name="indu_csrc", type="integer", default=0)
     */
    private $induCsrc;

    /**
     * @var string $state 所在省份
     * @Column(name="state", type="string", length=32, default="")
     */
    private $state;

    /**
     * @var string $businessMajor 主营业务
     * @Column(name="business_major", type="text", length=65535)
     */
    private $businessMajor;

    /**
     * @var string $industryName 所属行业
     * @Column(name="industry_name", type="string", length=32, default="")
     */
    private $industryName;

    /**
     * @var string $industryCode 行业编码
     * @Column(name="industry_code", type="string", length=32, default="")
     */
    private $industryCode;

    /**
     * @var int $industryEnable 
     * @Column(name="industry_enable", type="tinyint", default=0)
     */
    private $industryEnable;

    /**
     * @var string $listingDate 上市时间
     * @Column(name="listing_date", type="timestamp")
     */
    private $listingDate;

    /**
     * @var float $issuePrice 发行价格
     * @Column(name="issue_price", type="decimal", default=0)
     */
    private $issuePrice;

    /**
     * @var int $issueVol 发行数量
     * @Column(name="issue_vol", type="bigint", default=0)
     */
    private $issueVol;

    /**
     * @var string $infoPubDate 股东股本发布时间
     * @Column(name="info_pub_date", type="timestamp")
     */
    private $infoPubDate;

    /**
     * @var int $totalShares 总股本/万
     * @Column(name="total_shares", type="bigint", default=0)
     */
    private $totalShares;

    /**
     * @var string $firstHolder 第一大股东
     * @Column(name="first_holder", type="string", length=128, default="")
     */
    private $firstHolder;

    /**
     * @var string $pctMainHolders 前十大股东占比
     * @Column(name="pct_main_holders", type="string", length=8, default="")
     */
    private $pctMainHolders;

    /**
     * @var string $pctInstitutions 机构投资者占比
     * @Column(name="pct_institutions", type="string", length=16, default="")
     */
    private $pctInstitutions;

    /**
     * @var string $sharePubDate 
     * @Column(name="share_pub_date", type="timestamp")
     */
    private $sharePubDate;

    /**
     * @var string $shareHolderNum 股东人数
     * @Column(name="share_holder_num", type="string", length=64, default="")
     */
    private $shareHolderNum;

    /**
     * @var string $averageHoldSum 平均持股
     * @Column(name="average_hold_sum", type="string", length=64, default="")
     */
    private $averageHoldSum;

    /**
     * @var float $incomeSum 总收入
     * @Column(name="income_sum", type="decimal", default=0)
     */
    private $incomeSum;

    /**
     * @var string $incomeDate 收入统计时间
     * @Column(name="income_date", type="timestamp")
     */
    private $incomeDate;

    /**
     * @var string $leaderDate 领导统计时间
     * @Column(name="leader_date", type="timestamp")
     */
    private $leaderDate;

    /**
     * @param int $value
     * @return $this
     */
    public function setStockId(int $value)
    {
        $this->stockId = $value;

        return $this;
    }

    /**
     * 公司代码
     * @param int $value
     * @return $this
     */
    public function setCompanyCode(int $value): self
    {
        $this->companyCode = $value;

        return $this;
    }

    /**
     * 公司名称
     * @param string $value
     * @return $this
     */
    public function setCompanyName(string $value): self
    {
        $this->companyName = $value;

        return $this;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setInduCsrc(int $value): self
    {
        $this->induCsrc = $value;

        return $this;
    }

    /**
     * 所在省份
     * @param string $value
     * @return $this
     */
    public function setState(string $value): self
    {
        $this->state = $value;

        return $this;
    }

    /**
     * 主营业务
     * @param string $value
     * @return $this
     */
    public function setBusinessMajor(string $value): self
    {
        $this->businessMajor = $value;

        return $this;
    }

    /**
     * 所属行业
     * @param string $value
     * @return $this
     */
    public function setIndustryName(string $value): self
    {
        $this->industryName = $value;

        return $this;
    }

    /**
     * 行业编码
     * @param string $value
     * @return $this
     */
    public function setIndustryCode(string $value): self
    {
        $this->industryCode = $value;

        return $this;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setIndustryEnable(int $value): self
    {
        $this->industryEnable = $value;

        return $this;
    }

    /**
     * 上市时间
     * @param string $value
     * @return $this
     */
    public function setListingDate(string $value): self
    {
        $this->listingDate = $value;

        return $this;
    }

    /**
     * 发行价格
     * @param float $value
     * @return $this
     */
    public function setIssuePrice(float $value): self
    {
        $this->issuePrice = $value;

        return $this;
    }

    /**
     * 发行数量
     * @param int $value
     * @return $this
     */
    public function setIssueVol(int $value): self
    {
        $this->issueVol = $value;

        return $this;
    }

    /**
     * 股东股本发布时间
     * @param string $value
     * @return $this
     */
    public function setInfoPubDate(string $value): self
    {
        $this->infoPubDate = $value;

        return $this;
    }

    /**
     * 总股本/万
     * @param int $value
     * @return $this
     */
    public function setTotalShares(int $value): self
    {
        $this->totalShares = $value;

        return $this;
    }

    /**
     * 第一大股东
     * @param string $value
     * @return $this
     */
    public function setFirstHolder(string $value): self
    {
        $this->firstHolder = $value;

        return $this;
    }

    /**
     * 前十大股东占比
     * @param string $value
     * @return $this
     */
    public function setPctMainHolders(string $value): self
    {
        $this->pctMainHolders = $value;

        return $this;
    }

    /**
     * 机构投资者占比
     * @param string $value
     * @return $this
     */
    public function setPctInstitutions(string $value): self
    {
        $this->pctInstitutions = $value;

        return $this;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setSharePubDate(string $value): self
    {
        $this->sharePubDate = $value;

        return $this;
    }

    /**
     * 股东人数
     * @param string $value
     * @return $this
     */
    public function setShareHolderNum(string $value): self
    {
        $this->shareHolderNum = $value;

        return $this;
    }

    /**
     * 平均持股
     * @param string $value
     * @return $this
     */
    public function setAverageHoldSum(string $value): self
    {
        $this->averageHoldSum = $value;

        return $this;
    }

    /**
     * 总收入
     * @param float $value
     * @return $this
     */
    public function setIncomeSum(float $value): self
    {
        $this->incomeSum = $value;

        return $this;
    }

    /**
     * 收入统计时间
     * @param string $value
     * @return $this
     */
    public function setIncomeDate(string $value): self
    {
        $this->incomeDate = $value;

        return $this;
    }

    /**
     * 领导统计时间
     * @param string $value
     * @return $this
     */
    public function setLeaderDate(string $value): self
    {
        $this->leaderDate = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStockId()
    {
        return $this->stockId;
    }

    /**
     * 公司代码
     * @return int
     */
    public function getCompanyCode()
    {
        return $this->companyCode;
    }

    /**
     * 公司名称
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @return int
     */
    public function getInduCsrc()
    {
        return $this->induCsrc;
    }

    /**
     * 所在省份
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * 主营业务
     * @return string
     */
    public function getBusinessMajor()
    {
        return $this->businessMajor;
    }

    /**
     * 所属行业
     * @return string
     */
    public function getIndustryName()
    {
        return $this->industryName;
    }

    /**
     * 行业编码
     * @return string
     */
    public function getIndustryCode()
    {
        return $this->industryCode;
    }

    /**
     * @return int
     */
    public function getIndustryEnable()
    {
        return $this->industryEnable;
    }

    /**
     * 上市时间
     * @return string
     */
    public function getListingDate()
    {
        return $this->listingDate;
    }

    /**
     * 发行价格
     * @return mixed
     */
    public function getIssuePrice()
    {
        return $this->issuePrice;
    }

    /**
     * 发行数量
     * @return int
     */
    public function getIssueVol()
    {
        return $this->issueVol;
    }

    /**
     * 股东股本发布时间
     * @return string
     */
    public function getInfoPubDate()
    {
        return $this->infoPubDate;
    }

    /**
     * 总股本/万
     * @return int
     */
    public function getTotalShares()
    {
        return $this->totalShares;
    }

    /**
     * 第一大股东
     * @return string
     */
    public function getFirstHolder()
    {
        return $this->firstHolder;
    }

    /**
     * 前十大股东占比
     * @return string
     */
    public function getPctMainHolders()
    {
        return $this->pctMainHolders;
    }

    /**
     * 机构投资者占比
     * @return string
     */
    public function getPctInstitutions()
    {
        return $this->pctInstitutions;
    }

    /**
     * @return string
     */
    public function getSharePubDate()
    {
        return $this->sharePubDate;
    }

    /**
     * 股东人数
     * @return string
     */
    public function getShareHolderNum()
    {
        return $this->shareHolderNum;
    }

    /**
     * 平均持股
     * @return string
     */
    public function getAverageHoldSum()
    {
        return $this->averageHoldSum;
    }

    /**
     * 总收入
     * @return mixed
     */
    public function getIncomeSum()
    {
        return $this->incomeSum;
    }

    /**
     * 收入统计时间
     * @return string
     */
    public function getIncomeDate()
    {
        return $this->incomeDate;
    }

    /**
     * 领导统计时间
     * @return string
     */
    public function getLeaderDate()
    {
        return $this->leaderDate;
    }

}
