<?php
namespace App\Models\Entity;

use Swoft\Db\Model;
use Swoft\Db\Bean\Annotation\Column;
use Swoft\Db\Bean\Annotation\Entity;
use Swoft\Db\Bean\Annotation\Id;
use Swoft\Db\Bean\Annotation\Required;
use Swoft\Db\Bean\Annotation\Table;
use Swoft\Db\Types;

/**
 * 股票

 * @Entity()
 * @Table(name="stock")
 * @uses      Stock
 */
class Stock extends Model
{
    /**
     * @var int $id 
     * @Id()
     * @Column(name="id", type="integer")
     */
    private $id;

    /**
     * @var int $market 股票类型，0:A股；1:港股;2:美股
     * @Column(name="market", type="tinyint", default=0)
     */
    private $market;

    /**
     * @var int $type 0:沪A;1:沪B;2:深A;3:深B
     * @Column(name="type", type="tinyint", default=0)
     */
    private $type;

    /**
     * @var string $code 代码
     * @Column(name="code", type="string", length=16, default="")
     */
    private $code;

    /**
     * @var string $symbol 标记
     * @Column(name="symbol", type="string", length=16, default="")
     */
    private $symbol;

    /**
     * @var string $name 股票名称
     * @Column(name="name", type="string", length=32, default="")
     */
    private $name;

    /**
     * @var int $createTime 创建时间
     * @Column(name="create_time", type="integer", default=0)
     */
    private $createTime;

    /**
     * @var int $updateTime 更新时间
     * @Column(name="update_time", type="integer", default=0)
     */
    private $updateTime;

    /**
     * @var int $status 
     * @Column(name="status", type="tinyint", default=1)
     */
    private $status;

    /**
     * @param int $value
     * @return $this
     */
    public function setId(int $value)
    {
        $this->id = $value;

        return $this;
    }

    /**
     * 股票类型，0:A股；1:港股;2:美股
     * @param int $value
     * @return $this
     */
    public function setMarket(int $value): self
    {
        $this->market = $value;

        return $this;
    }

    /**
     * 0:沪A;1:沪B;2:深A;3:深B
     * @param int $value
     * @return $this
     */
    public function setType(int $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * 代码
     * @param string $value
     * @return $this
     */
    public function setCode(string $value): self
    {
        $this->code = $value;

        return $this;
    }

    /**
     * 标记
     * @param string $value
     * @return $this
     */
    public function setSymbol(string $value): self
    {
        $this->symbol = $value;

        return $this;
    }

    /**
     * 股票名称
     * @param string $value
     * @return $this
     */
    public function setName(string $value): self
    {
        $this->name = $value;

        return $this;
    }

    /**
     * 创建时间
     * @param int $value
     * @return $this
     */
    public function setCreateTime(int $value): self
    {
        $this->createTime = $value;

        return $this;
    }

    /**
     * 更新时间
     * @param int $value
     * @return $this
     */
    public function setUpdateTime(int $value): self
    {
        $this->updateTime = $value;

        return $this;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setStatus(int $value): self
    {
        $this->status = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * 股票类型，0:A股；1:港股;2:美股
     * @return int
     */
    public function getMarket()
    {
        return $this->market;
    }

    /**
     * 0:沪A;1:沪B;2:深A;3:深B
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * 代码
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * 标记
     * @return string
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * 股票名称
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * 创建时间
     * @return int
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }

    /**
     * 更新时间
     * @return int
     */
    public function getUpdateTime()
    {
        return $this->updateTime;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

}
