<?php
namespace App\Models\Entity;

use Swoft\Db\Model;
use Swoft\Db\Bean\Annotation\Column;
use Swoft\Db\Bean\Annotation\Entity;
use Swoft\Db\Bean\Annotation\Id;
use Swoft\Db\Bean\Annotation\Required;
use Swoft\Db\Bean\Annotation\Table;
use Swoft\Db\Types;

/**
 * 龙头股

 * @Entity()
 * @Table(name="stock_bellwether")
 * @uses      StockBellwether
 */
class StockBellwether extends Model
{
    /**
     * @var int $id 
     * @Id()
     * @Column(name="id", type="bigint")
     */
    private $id;

    /**
     * @var string $stockCode stock id
     * @Column(name="stock_code", type="char", length=12, default="0")
     */
    private $stockCode;

    /**
     * @var string $bkCode 版块/概念代码
     * @Column(name="bk_code", type="char", length=12, default="")
     */
    private $bkCode;

    /**
     * @var string $tag 标签
     * @Column(name="tag", type="string", length=64, default="")
     */
    private $tag;

    /**
     * @var string $tags 
     * @Column(name="tags", type="json")
     */
    private $tags;

    /**
     * @var string $createTime 创建时间
     * @Column(name="create_time", type="datetime")
     */
    private $createTime;

    /**
     * @var string $updateTime 更新时间
     * @Column(name="update_time", type="datetime")
     */
    private $updateTime;

    /**
     * @param int $value
     * @return $this
     */
    public function setId(int $value)
    {
        $this->id = $value;

        return $this;
    }

    /**
     * stock id
     * @param string $value
     * @return $this
     */
    public function setStockCode(string $value): self
    {
        $this->stockCode = $value;

        return $this;
    }

    /**
     * 版块/概念代码
     * @param string $value
     * @return $this
     */
    public function setBkCode(string $value): self
    {
        $this->bkCode = $value;

        return $this;
    }

    /**
     * 标签
     * @param string $value
     * @return $this
     */
    public function setTag(string $value): self
    {
        $this->tag = $value;

        return $this;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setTags(string $value): self
    {
        $this->tags = $value;

        return $this;
    }

    /**
     * 创建时间
     * @param string $value
     * @return $this
     */
    public function setCreateTime(string $value): self
    {
        $this->createTime = $value;

        return $this;
    }

    /**
     * 更新时间
     * @param string $value
     * @return $this
     */
    public function setUpdateTime(string $value): self
    {
        $this->updateTime = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * stock id
     * @return string
     */
    public function getStockCode()
    {
        return $this->stockCode;
    }

    /**
     * 版块/概念代码
     * @return string
     */
    public function getBkCode()
    {
        return $this->bkCode;
    }

    /**
     * 标签
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @return string
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * 创建时间
     * @return string
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }

    /**
     * 更新时间
     * @return string
     */
    public function getUpdateTime()
    {
        return $this->updateTime;
    }

}
