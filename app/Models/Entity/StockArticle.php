<?php
namespace App\Models\Entity;

use Swoft\Db\Model;
use Swoft\Db\Bean\Annotation\Column;
use Swoft\Db\Bean\Annotation\Entity;
use Swoft\Db\Bean\Annotation\Id;
use Swoft\Db\Bean\Annotation\Required;
use Swoft\Db\Bean\Annotation\Table;
use Swoft\Db\Types;

/**
 * 文章/新闻

 * @Entity()
 * @Table(name="stock_article")
 * @uses      StockArticle
 */
class StockArticle extends Model
{
    /**
     * @var int $id 
     * @Id()
     * @Column(name="id", type="bigint")
     */
    private $id;

    /**
     * @var int $stockId stock id
     * @Column(name="stock_id", type="integer", default=0)
     */
    private $stockId;

    /**
     * @var string $title 标题
     * @Column(name="title", type="string", length=128, default="")
     */
    private $title;

    /**
     * @var string $abstract 摘要
     * @Column(name="abstract", type="string", length=128, default="")
     */
    private $abstract;

    /**
     * @var int $articleType 文章类型
     * @Column(name="article_type", type="tinyint", default=0)
     */
    private $articleType;

    /**
     * @var int $type 类型
     * @Column(name="type", type="tinyint", default=0)
     */
    private $type;

    /**
     * @var int $importance 重要度
     * @Column(name="importance", type="tinyint", default=0)
     */
    private $importance;

    /**
     * @var int $sentiment 热度
     * @Column(name="sentiment", type="tinyint", default=0)
     */
    private $sentiment;

    /**
     * @var string $source 文章来源
     * @Column(name="source", type="string", length=32, default="")
     */
    private $source;

    /**
     * @var string $content 文章内容
     * @Column(name="content", type="text", length=4294967295)
     */
    private $content;

    /**
     * @var string $publishTime 发布时间
     * @Column(name="publish_time", type="datetime")
     */
    private $publishTime;

    /**
     * @var string $createTime 创建时间
     * @Column(name="create_time", type="datetime")
     */
    private $createTime;

    /**
     * @var string $updateTime 更新时间
     * @Column(name="update_time", type="datetime")
     */
    private $updateTime;

    /**
     * @param int $value
     * @return $this
     */
    public function setId(int $value)
    {
        $this->id = $value;

        return $this;
    }

    /**
     * stock id
     * @param int $value
     * @return $this
     */
    public function setStockId(int $value): self
    {
        $this->stockId = $value;

        return $this;
    }

    /**
     * 标题
     * @param string $value
     * @return $this
     */
    public function setTitle(string $value): self
    {
        $this->title = $value;

        return $this;
    }

    /**
     * 摘要
     * @param string $value
     * @return $this
     */
    public function setAbstract(string $value): self
    {
        $this->abstract = $value;

        return $this;
    }

    /**
     * 文章类型
     * @param int $value
     * @return $this
     */
    public function setArticleType(int $value): self
    {
        $this->articleType = $value;

        return $this;
    }

    /**
     * 类型
     * @param int $value
     * @return $this
     */
    public function setType(int $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * 重要度
     * @param int $value
     * @return $this
     */
    public function setImportance(int $value): self
    {
        $this->importance = $value;

        return $this;
    }

    /**
     * 热度
     * @param int $value
     * @return $this
     */
    public function setSentiment(int $value): self
    {
        $this->sentiment = $value;

        return $this;
    }

    /**
     * 文章来源
     * @param string $value
     * @return $this
     */
    public function setSource(string $value): self
    {
        $this->source = $value;

        return $this;
    }

    /**
     * 文章内容
     * @param string $value
     * @return $this
     */
    public function setContent(string $value): self
    {
        $this->content = $value;

        return $this;
    }

    /**
     * 发布时间
     * @param string $value
     * @return $this
     */
    public function setPublishTime(string $value): self
    {
        $this->publishTime = $value;

        return $this;
    }

    /**
     * 创建时间
     * @param string $value
     * @return $this
     */
    public function setCreateTime(string $value): self
    {
        $this->createTime = $value;

        return $this;
    }

    /**
     * 更新时间
     * @param string $value
     * @return $this
     */
    public function setUpdateTime(string $value): self
    {
        $this->updateTime = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * stock id
     * @return int
     */
    public function getStockId()
    {
        return $this->stockId;
    }

    /**
     * 标题
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * 摘要
     * @return string
     */
    public function getAbstract()
    {
        return $this->abstract;
    }

    /**
     * 文章类型
     * @return int
     */
    public function getArticleType()
    {
        return $this->articleType;
    }

    /**
     * 类型
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * 重要度
     * @return int
     */
    public function getImportance()
    {
        return $this->importance;
    }

    /**
     * 热度
     * @return int
     */
    public function getSentiment()
    {
        return $this->sentiment;
    }

    /**
     * 文章来源
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * 文章内容
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * 发布时间
     * @return string
     */
    public function getPublishTime()
    {
        return $this->publishTime;
    }

    /**
     * 创建时间
     * @return string
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }

    /**
     * 更新时间
     * @return string
     */
    public function getUpdateTime()
    {
        return $this->updateTime;
    }

}
