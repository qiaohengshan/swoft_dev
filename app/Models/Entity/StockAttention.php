<?php
namespace App\Models\Entity;

use Swoft\Db\Model;
use Swoft\Db\Bean\Annotation\Column;
use Swoft\Db\Bean\Annotation\Entity;
use Swoft\Db\Bean\Annotation\Id;
use Swoft\Db\Bean\Annotation\Required;
use Swoft\Db\Bean\Annotation\Table;
use Swoft\Db\Types;

/**
 * 关注信号

 * @Entity()
 * @Table(name="stock_attention")
 * @uses      StockAttention
 */
class StockAttention extends Model
{
    /**
     * @var int $id 
     * @Id()
     * @Column(name="id", type="bigint")
     */
    private $id;

    /**
     * @var string $code 代码
     * @Column(name="code", type="char", length=10, default="")
     */
    private $code;

    /**
     * @var string $symbol 符号
     * @Column(name="symbol", type="char", length=8, default="")
     */
    private $symbol;

    /**
     * @var int $type 
     * @Column(name="type", type="tinyint", default=0)
     */
    private $type;

    /**
     * @var string $date 时间
     * @Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var float $changeRate 变化率
     * @Column(name="change_rate", type="decimal", default=0)
     */
    private $changeRate;

    /**
     * @var float $profit 利润率
     * @Column(name="profit", type="decimal", default=0)
     */
    private $profit;

    /**
     * @var float $maxChangeRate 最大变化率
     * @Column(name="max_change_rate", type="decimal", default=0)
     */
    private $maxChangeRate;

    /**
     * @param int $value
     * @return $this
     */
    public function setId(int $value)
    {
        $this->id = $value;

        return $this;
    }

    /**
     * 代码
     * @param string $value
     * @return $this
     */
    public function setCode(string $value): self
    {
        $this->code = $value;

        return $this;
    }

    /**
     * 符号
     * @param string $value
     * @return $this
     */
    public function setSymbol(string $value): self
    {
        $this->symbol = $value;

        return $this;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setType(int $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * 时间
     * @param string $value
     * @return $this
     */
    public function setDate(string $value): self
    {
        $this->date = $value;

        return $this;
    }

    /**
     * 变化率
     * @param float $value
     * @return $this
     */
    public function setChangeRate(float $value): self
    {
        $this->changeRate = $value;

        return $this;
    }

    /**
     * 利润率
     * @param float $value
     * @return $this
     */
    public function setProfit(float $value): self
    {
        $this->profit = $value;

        return $this;
    }

    /**
     * 最大变化率
     * @param float $value
     * @return $this
     */
    public function setMaxChangeRate(float $value): self
    {
        $this->maxChangeRate = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * 代码
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * 符号
     * @return string
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * 时间
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * 变化率
     * @return mixed
     */
    public function getChangeRate()
    {
        return $this->changeRate;
    }

    /**
     * 利润率
     * @return mixed
     */
    public function getProfit()
    {
        return $this->profit;
    }

    /**
     * 最大变化率
     * @return mixed
     */
    public function getMaxChangeRate()
    {
        return $this->maxChangeRate;
    }

}
