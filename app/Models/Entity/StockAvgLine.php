<?php
namespace App\Models\Entity;

use Swoft\Db\Model;
use Swoft\Db\Bean\Annotation\Column;
use Swoft\Db\Bean\Annotation\Entity;
use Swoft\Db\Bean\Annotation\Id;
use Swoft\Db\Bean\Annotation\Required;
use Swoft\Db\Bean\Annotation\Table;
use Swoft\Db\Types;

/**
 * @Entity()
 * @Table(name="stock_avg_line")
 * @uses      StockAvgLine
 */
class StockAvgLine extends Model
{
    /**
     * @var int $id 
     * @Id()
     * @Column(name="id", type="bigint")
     */
    private $id;

    /**
     * @var int $stockId stock id
     * @Column(name="stock_id", type="integer", default=0)
     */
    private $stockId;

    /**
     * @var int $type 类型:1:日均价,2:K线
     * @Column(name="type", type="tinyint", default=0)
     */
    private $type;

    /**
     * @var string $date 日期
     * @Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @param int $value
     * @return $this
     */
    public function setId(int $value)
    {
        $this->id = $value;

        return $this;
    }

    /**
     * stock id
     * @param int $value
     * @return $this
     */
    public function setStockId(int $value): self
    {
        $this->stockId = $value;

        return $this;
    }

    /**
     * 类型:1:日均价,2:K线
     * @param int $value
     * @return $this
     */
    public function setType(int $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * 日期
     * @param string $value
     * @return $this
     */
    public function setDate(string $value): self
    {
        $this->date = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * stock id
     * @return int
     */
    public function getStockId()
    {
        return $this->stockId;
    }

    /**
     * 类型:1:日均价,2:K线
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * 日期
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

}
