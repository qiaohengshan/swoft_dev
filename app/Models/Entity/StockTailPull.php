<?php
namespace App\Models\Entity;

use Swoft\Db\Model;
use Swoft\Db\Bean\Annotation\Column;
use Swoft\Db\Bean\Annotation\Entity;
use Swoft\Db\Bean\Annotation\Id;
use Swoft\Db\Bean\Annotation\Required;
use Swoft\Db\Bean\Annotation\Table;
use Swoft\Db\Types;

/**
 * 尾盘拉升

 * @Entity()
 * @Table(name="stock_tail_pull")
 * @uses      StockTailPull
 */
class StockTailPull extends Model
{
    /**
     * @var int $id 
     * @Id()
     * @Column(name="id", type="bigint")
     */
    private $id;

    /**
     * @var int $stockId id
     * @Column(name="stock_id", type="integer", default=0)
     */
    private $stockId;

    /**
     * @var string $code 代码
     * @Column(name="code", type="string", length=16, default="")
     */
    private $code;

    /**
     * @var string $date 日期
     * @Column(name="date", type="timestamp")
     */
    private $date;

    /**
     * @var float $rate (close-open)/limit_up
     * @Column(name="rate", type="decimal", default=0)
     */
    private $rate;

    /**
     * @param int $value
     * @return $this
     */
    public function setId(int $value)
    {
        $this->id = $value;

        return $this;
    }

    /**
     * id
     * @param int $value
     * @return $this
     */
    public function setStockId(int $value): self
    {
        $this->stockId = $value;

        return $this;
    }

    /**
     * 代码
     * @param string $value
     * @return $this
     */
    public function setCode(string $value): self
    {
        $this->code = $value;

        return $this;
    }

    /**
     * 日期
     * @param string $value
     * @return $this
     */
    public function setDate(string $value): self
    {
        $this->date = $value;

        return $this;
    }

    /**
     * (close-open)/limit_up
     * @param float $value
     * @return $this
     */
    public function setRate(float $value): self
    {
        $this->rate = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * id
     * @return int
     */
    public function getStockId()
    {
        return $this->stockId;
    }

    /**
     * 代码
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * 日期
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * (close-open)/limit_up
     * @return mixed
     */
    public function getRate()
    {
        return $this->rate;
    }

}
