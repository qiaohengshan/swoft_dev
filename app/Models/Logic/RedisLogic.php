<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2019/4/18
 * Time: 17:42
 */

namespace App\Models\Logic;

use Swoft\Bean\Annotation\Inject;
use Swoft\Bean\Annotation\Bean;


/**
 * 用户逻辑层
 * 同时可以被controller server task使用
 *
 * @Bean()
 * @uses      RedisLogic
 * @version   2017年10月15日
 * @author    stelin <phpcrazy@126.com>
 * @copyright Copyright 2010-2016 swoft software
 * @license   PHP Version 7.x {@link http://www.php.net/license/3_0.txt}
 */

class RedisLogic
{
    /**
     * @Inject()
     * @var \Swoft\Redis\Redis
     */
    private $redis;

    /**
     * 保存hash缓存
     * @param $key
     * @param $hashKey
     * @param $value
     */
    public  function hSet($key, $hashKey, $value)
    {
        return $this->redis->hSet($key,$hashKey,is_array($value)?json_encode($value):$value);
    }

    /**
     * 获取hash缓存
     * @param $key
     * @param $hashKey
     * @return string
     */
    public  function hGet($key,$hashKey)
    {
        $cache =  $this->redis->hGet($key,$hashKey);

        return json_decode($cache,true);
    }

    /**
     * 获取所有缓存
     * @param $key
     * @return array
     */
    public  function hGetAll($key)
    {
        $rows = $this->redis->hGetAll($key);
        if($rows)
        {
            $items = [];
            foreach ($rows as $key=>$row)
            {
                $row = !is_array($row)?json_decode($row,true):$row;
                $items[$key] = $row;
            }
            return $items;
        }else{
            return $rows;
        }
    }

}