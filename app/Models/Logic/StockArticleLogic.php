<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2019/5/22
 * Time: 17:30
 */

namespace App\Models\Logic;

use App\Models\Entity\StockArticle;
use Swoft\Bean\Annotation\Bean;
use Swoft\Db\Db;

/**
 * @Bean("StockArticleLogic")
 * Class StockArticleLogic
 * @package App\Models\Logic
 */
class StockArticleLogic
{
    public  function save($data)
    {
        $has = StockArticle::findOne(['stock_id'=>$data['stock_id'],'title'=>$data['title']])->getResult();
        Db::beginTransaction();
        $timestamp = date('Y-m-d H:i:s',time());
        $data['update_time'] = $timestamp;
        try{
            if($has)
            {

                StockArticle::updateOne($data,['id'=>$has['id']])->getResult();
            }else{
                $data['create_time'] = $timestamp;
                StockArticle::batchInsert([$data])->getResult();
            }

            Db::commit();
        }catch (\Exception $exp){
            Db::rollback();
            throw $exp;
        }
    }
}