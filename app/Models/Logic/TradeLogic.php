<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2019/4/24
 * Time: 15:28
 */

namespace App\Models\Logic;

use App\Models\Entity\StockTrade;
use App\Models\Entity\StockTradeDetailMinute;

/**
 * 交易逻辑类
 * Class TradeLogic
 * @package App\Models\Logic
 */
class TradeLogic
{
    /**
     * 获取持续放量的
     * @param $stock
     * @return bool
     */
    public function volume($stock):bool
    {
        $rows = StockTrade::findAll(
            [
                    'stock_id'=> $stock['id'],
            ],
            [
                'orderby' => ['date' => 'DESC'],
                'limit' => 3,
                'fields'=>['volume'],
            ])->getResult();
        $first=0;

        if(count($rows) == 3)
        {
            if($rows[0]['volume'] > $rows[1]['volume'] &&  $rows[1]['volume'] > $rows[2]['volume'] )
            {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取分时成交数据
     * @param array $where
     */
    public static function minute(array $where)
    {
        $rows = StockTradeDetailMinute::findAll($where,[
            'orderby' => ['time' => 'ASC'],
        ])->getResult();
        return $rows;
    }
}