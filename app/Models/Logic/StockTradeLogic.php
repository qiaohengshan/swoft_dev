<?php


namespace App\Models\Logic;


use App\Models\Entity\StockTrade;

class StockTradeLogic
{
    public static function adjust($stock,$operate='<=')
    {
        $result = false;
        $items = StockTrade::findAll(
            [
                'stock_id'=>$stock['id'],
            ],
            ['orderby' => ['date' => 'DESC'], 'limit' => 5])->getResult();

        if($items)
        {
            $items = $items->toArray();
            $items = array_reverse($items);
            foreach ($items as $key=>$item)
            {
                switch ($operate)
                {
                    case ">=":
                        if($items[$key+1]['close'] >= $item['close'])
                        {
                            $result = true;
                            continue;
                        }else{
                            $result = false;
                            break;
                        }
                        break;
                    case "<=":
                        if($items[$key+1]['close'] <= $item['close'])
                        {
                            $result = true;
                            continue;
                        }else{
                            $result = false;
                            break;
                        }
                        break;
                }
            }
        }
        return $result;
    }
}