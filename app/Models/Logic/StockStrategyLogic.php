<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2019/5/20
 * Time: 15:48
 */

namespace App\Models\Logic;


use App\Models\Entity\StockAttention;
use Swoft\Db\Db;

class StockStrategyLogic
{
    /**
     * 创建关注信号
     * @param $data
     * @throws \Swoft\Db\Exception\DbException
     */
    public static function attention($data)
    {
        $has = StockAttention::findOne(['code'=>$data['code'],'date'=>$data['date']])->getResult();
        Db::beginTransaction();
        try{
            if($has)
            {
                StockAttention::updateOne($data,['id'=>$has['id']])->getResult();
            }else{
                StockAttention::batchInsert([$data])->getResult();
            }
            Db::commit();
        }catch (\Exception $exp){
            Db::rollback();
            throw $exp;
        }
    }
}