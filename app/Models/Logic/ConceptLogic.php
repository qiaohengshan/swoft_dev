<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2019/4/25
 * Time: 13:38
 */

namespace App\Models\Logic;


use App\Models\Entity\Concept;
use App\Services\RedisService;
use Swoft\App;
use Swoft\Db\Db;
use Swoft\Exception\Exception;

class ConceptLogic
{
    private $model = null;
    private $redisService;
    const NAME='concept';
    public function __construct()
    {
        $this->model = new Concept();
        $this->redisService = new RedisService();
    }

    public function add($data)
    {
        try{

            $has = $this->redisService->hGet(self::NAME,$data['code']);
            if(empty($has))
            {
                $has = Concept::findOne(['code'=>$data['code'],'type'=>$data['type']],['fields' => ['id']])->getResult();
            }

            $timestamp = date('Y-m-d H:i:s',time());
            Db::beginTransaction();
            try{

                $data['update_time'] = $timestamp;
                if($has)
                {
                    $id = $has['id'];
                   Concept::updateOne($data,['id'=>$id])->getResult();
                   $data['id'] = $id;
                }else{
                    $data['create_time'] = $timestamp;
                    $id = $this->model->fill($data)->save()->getResult();
                    $data['id']=$id;
                }
                Db::commit();
                $this->redisService->hSet(self::NAME,$data['code'],json_encode($data));
            }catch (Exception $exp){
                Db::rollback();
                App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));
            }
        }catch (\Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));
        }
    }
}