<?php
/**
 * This file is part of Swoft.
 *
 * @link https://swoft.org
 * @document https://doc.swoft.org
 * @contact group@swoft.org
 * @license https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Models\Logic;

use App\Models\Entity\Stock;

use App\Models\Entity\StockConcept;
use App\Models\Entity\StockDetail;
use App\Models\Entity\StockProfile;
use App\Models\Entity\StockTrade;
use App\Services\RedisService;
use Swoft\App;
use Swoft\Db\Db;
use Swoft\Exception\Exception;

class StockLogic
{

    private $redis;
    const NAME='stock';

    const STOCK_TRADE_TIME = 'stock:trade:time';

    public function __construct()
    {
        $this->redis = new RedisService();
    }

    public function add($param)
    {
        try{
            $model = new Stock();
            $code = $param['code'];
            $stock = $this->redis->hGet(self::NAME,$code);
            if(empty($stock))
            {
                $stock = $model->findOne(['code'=>$code])->getResult();
            }

            $data['code']  = $param['code'];
            $data['symbol'] = $param['symbol'];
            $data['name'] = $param['name'];
            $data['is_new'] = $param['is_new'];
            $timestamp = time();
            Db::beginTransaction();
            try{
                if ($stock)
                {
                    $data['update_time'] = $timestamp;
                    $model->updateOne($data,['id'=>$stock['id']])->getResult();
                    $insertId = $stock['id'];
                }else{
                    $data['create_time'] = $timestamp;
                    $data['update_time'] = $timestamp;
                    $insertId = $model->fill($data)->save()->getResult();
                }
                Db::commit();
                $data['id'] = $insertId;
                $this->redis->hSet(self::NAME,$code,json_encode($data));
                return $insertId;
            }catch (Exception $exp){
                Db::rollback();
                App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));
            }

        }catch (\Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));
        }
    }

    /**
     * 保存股票简介
     * @param $stock
     * @param $params
     */
    public function saveProfile($stock,$params)
    {
        try{
            $model = new StockProfile();
            $data = $params;
            $data['stock_id']  = $stock['id'];
            $has = $model->findById($data['stock_id'])->getResult();
            Db::beginTransaction();
            try{
                if ($has)
                {
                    $insertId = $model->updateOne($data,['stock_id'=>$data['stock_id']])->getResult();
                }else{
                    $insertId = $model->fill($data)->save()->getResult();
                }
                Db::commit();
            }catch (Exception $exp){
                Db::rollback();
                App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));
            }

        }catch (\Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));
        }
    }

    /**
     * 保存交易数据
     * @param $stock
     * @param $param
     */
    public function saveTrade($stock,$param)
    {
        try{
            $data = $param;
            $data['date'] = $data['date'];
            $data['stock_id'] = $stock['id'];
            $data['code'] = $stock['code'];
            $data['symbol'] = $stock['symbol'];
            $model = new StockTrade();
            $has = $model->findOne(['stock_id'=>$data['stock_id'],'date'=>$data['date']],['fields' => ['id']])->getResult();
            $timestamp = date('Y-m-d H:i:s',time());
            if (empty($has))
            {
                Db::beginTransaction();
                try{
                    $data['create_time'] = $timestamp;
                    $data['update_time'] = $timestamp;
                    $insertId = $model->fill($data)->save()->getResult();
                    Db::commit();
                    $this->redis->hSet(self::STOCK_TRADE_TIME,$stock['code'],$param['date']);
                }catch (Exception $exp){
                    Db::rollback();
                    App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));
                }
            }
        }catch (\Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));
        }
    }

    /**
     * 保存交易数据
     * @param $stock
     * @param $param
     */
    public function saveStockConcept($stock,$param)
    {
        try{

            $data['name'] = $param['name'];
            $data['stock_id'] = $stock['id'];
            $data['code'] = $param['code'];
            $data['symbol'] = $param['symbol'];
            $data['type'] = $param['type'];
            $data['is_hot'] = $param['is_hot']?1:0;
            $model = new StockConcept();
            $has = $model->findOne(['stock_id'=>$data['stock_id'],'code'=>$data['code']],['fields' => ['id']])->getResult();
            $timestamp = time();
            if($has)
            {
                $data['update_time'] = $timestamp;
            }else{
                $data['create_time'] = $timestamp;
            }

            Db::beginTransaction();
            try{
                if($has)
                {
                    $model->updateOne($data,['id'=>$has->getId()])->getResult();
                }else{
                    $model->fill($data)->save()->getResult();
                }

                Db::commit();
            }catch (Exception $exp){
                Db::rollback();
                App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));
            }
        }catch (\Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));
        }
    }

    /**
     * 保存股票详情
     * @param $stock
     * @param $params
     */
    public function saveStockDetail($stock,$params)
    {
        try{

            $data = $params;
            $data['stock_id'] = $stock['id'];
            $model = new StockDetail();
            $has = $model->findOne(['stock_id'=>$data['stock_id']],['fields' => ['stock_id']])->getResult();
            $timestamp = time();
            if($has)
            {
                $data['update_time'] = $timestamp;
            }else{
                $data['create_time'] = $timestamp;
            }

            Db::beginTransaction();
            try{
                if($has)
                {
                    $model->updateOne($data,['stock_id'=>$has->getStockId()])->getResult();
                }else{
                    $model->fill($data)->save()->getResult();
                }
                Db::commit();
                (new RedisService())->hSet('stock:detail',$data);
            }catch (Exception $exp){
                Db::rollback();
                App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));
            }
        }catch (\Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));
        }
    }


    public static function getOne($code)
    {
        $redis  = new RedisService();
        $stock  = $redis->hGet(self::NAME,$code);
        if (empty($stock))
        {
            $stock = Stock::findOne([['code'=>$code]])->getResult();
        }
        return $stock;
    }
    
}