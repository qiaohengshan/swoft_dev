<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2019/4/24
 * Time: 16:51
 */

namespace App\Models\Logic;


use App\Models\Entity\StockTopsMap;
use App\Models\Entity\StockTopsVary;
use Swoft\App;
use Swoft\Db\Db;

class TopsLogic
{
    public  function map($data)
    {
        $row = StockTopsMap::findOne([
            'code'=>$data['code'],
            'stock_id'=>$data['stock_id'],
            'create_time'=>$data['create_time'],
        ])->getResult();
        Db::beginTransaction();
        try{
            if($row)
            {
                StockTopsMap::updateOne($data,['id'=>$row->getId()])->getResult();
            }else{
                StockTopsMap::batchInsert([$data])->getResult();
            }
            Db::commit();
        }catch (Exception $exp){
            Db::rollback();
            App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));
        }
    }

    public static function vary($data)
    {
        $has = StockTopsVary::findOne(['bk_code'=>$data['bk_code'],'date'=>$data['date']])->getResult();

        Db::beginTransaction();
        try{
            if($has)
            {
                StockTopsVary::updateOne($data,['id'=>$has['id']])->getResult();
            }else{
                $data['create_time'] = date('Y-m-d H:i:s',time());
                StockTopsVary::batchInsert([$data])->getResult();
            }
            Db::commit();
        }catch (\Exception $exp){
            Db::rollback();
            throw $exp;
        }
    }
}