<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2019/4/25
 * Time: 15:51
 */

namespace App\Models\Logic;


use App\Models\Entity\StockBellwether;
use Swoft\App;
use Swoft\Db\Db;
use Swoft\Exception\Exception;

/**
 * 版块、概念龙头逻辑
 * Class BellwetherLogic
 * @package App\Models\Logic
 */
class BellwetherLogic
{
    private $model = null;
    public function __construct()
    {
        $this->model = new StockBellwether();
    }

    /**
     * 新增数据
     * @param $data
     * @throws \Swoft\Db\Exception\DbException
     */
    public function create($data)
    {
        $has = StockBellwether::findOne(['stock_code'=>$data['stock_code'],'bk_code'=>$data['bk_code']],['fields' => ['id']])->getResult();
        Db::beginTransaction();
        $timestamp = date('Y-m-d H:i:s',time());
        try{
            if($has)
            {
                $data['create_time'] = $timestamp;
                StockBellwether::updateOne($data,['id'=>$has['id']])->getResult();
                $id = $has['id'];
            }else{
                $data['update_time'] = $timestamp;
                $id = $this->model->fill($data)->save()->getResult();
            }
            Db::commit();
            return $id;
        }catch (Exception $exp){
            Db::rollback();
            App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));
        }
    }

    public static function recommend($bkCode)
    {
        try{
            $sql = "select * from stock_bellwether where bk_code='{$bkCode}' and JSON_SEARCH(tags,\"one\",\"龙头股\") is not null limit 2";

            $tops = Db::query($sql)->getResult();

            $sql = "select * from stock.stock_bellwether where  bk_code='{$bkCode}' and JSON_SEARCH(tags,\"one\",\"连续放量\") is not null  limit 2";

            $increments = Db::query($sql)->getResult();

            $result =[];

            $result = array_merge_recursive($result,$tops,$increments);

            return $result;
        }catch (Exception $exp){
            dump($exp->getMessage());
        }
    }
}