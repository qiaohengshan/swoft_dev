<?php


namespace App\Models\Logic;


use App\Models\Entity\StockChip;
use Swoft\Db\Db;

class StockChipLogic
{
    public static function save($stock,$data)
    {
        $has = StockChip::findOne([
            'stock_id'=>$stock['id'],
            'date'=>$data['date'],
        ])->getResult();
        Db::beginTransaction();
        try{
            if($has)
            {
                $result = StockChip::updateOne($data)->getResult();
            }else{
                $result = StockChip::batchInsert([$data])->getResult();
            }
            Db::commit();
            return $result;
        }catch (\Exception $exp){
            Db::rollback();
            throw $exp;
        }
    }
}