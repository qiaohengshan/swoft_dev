<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2019/4/23
 * Time: 12:14
 */

namespace App\Models\Logic;


use App\Models\Entity\Stock;
use App\Models\Entity\StockConcept;
use App\Models\Entity\StockTops;
use Swoft\App;
use Swoft\Db\Db;
use Swoft\Db\Query;
use Swoft\Exception\Exception;
class StatisticsLogic
{
    public function __construct()
    {

    }

    /**
     * 保存热点
     * @param $data
     * @return bool
     * @throws \Swoft\Db\Exception\DbException
     */
    public function storeTops($data)
    {
        $model =new StockTops();
        $top = $model->findOne(['bk_code'=>$data['bk_code'],'created_at'=>$data['created_at']])->getResult();
        Db::beginTransaction();
        try{
            if ($top)
            {
                $model->updateOne($data,['id'=>$top->getId()])->getResult();
            }else{
               $model->fill($data)->save()->getResult();
            }
            Db::commit();
            return true;
        }catch (Exception $exp){
            Db::rollback();
            App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));

        }
    }

    /**
     * 获取热门概念股票
     * @param $block
     * @return mixed
     * @throws \Swoft\Db\Exception\DbException
     */
    public function selectHotStocks($block)
    {
        $stocks = Query::table(Stock::class,'a')
            ->leftJoin(StockConcept::class,'a.id=b.stock_id','b')
            ->where('b.code',$block['bk_code'])
            ->get(['a.*'])->getResult();
        return $stocks;
    }


}