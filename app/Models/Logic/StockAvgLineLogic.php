<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2019/5/17
 * Time: 15:51
 */

namespace App\Models\Logic;


use App\Models\Entity\StockAvgLine;
use Swoft\App;
use Swoft\Db\Db;

class StockAvgLineLogic
{
    public static function add($data)
    {
       $where=[
           'stock_id'=>$data['stock_id'],
           'type'=>$data['type'],
           'date'=>$data['date'],
       ] ;
       $has = StockAvgLine::findOne($where)->getResult();

       Db::beginTransaction();
       try{
           if($has)
           {
               StockAvgLine::updateOne($data,['id'=>$has['id']])->getResult();
           }else{
               StockAvgLine::batchInsert([$data])->getResult();
           }
           Db::commit();
       }catch (\Exception $exp){
            Db::rollback();
           App::error(sprintf('%s has error,error:%s', get_class(self::class),$exp->getMessage()));
       }
    }
}