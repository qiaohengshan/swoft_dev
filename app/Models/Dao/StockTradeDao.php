<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2018/12/10
 * Time: 17:53
 */

namespace App\Models\Dao;


use App\Models\Entity\StockTrade;

class StockTradeDao
{
    private $model  = null ;
    public function __construct()
    {
        $this->model = new StockTrade();
    }

    /**
     * 获取前N条记录
     * @param $stock
     * @return mixed
     */
    public function getThreeRows($stock,$limit=3)
    {
        $rows = $this->model->findAll(['stock_id'=>$stock['id']], ['orderby' => ['date' => 'DESC'], 'limit' => $limit])->getResult();
        return $rows;
    }
}