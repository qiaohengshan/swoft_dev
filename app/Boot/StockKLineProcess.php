<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2018/12/19
 * Time: 17:04
 */

namespace App\Boot;


use App\Helper\Constant;
use App\Models\Logic\StockLogic;
use App\Services\FormatData;
use App\Services\SNSService;
use App\Services\StockService;
use Swoft\App;
use Swoft\Process\Process as SwoftProcess;
use Swoft\Process\ProcessInterface;
use Swoft\Redis\Redis;
use Swoft\Process\Bean\Annotation\Process;
use Swoft\Task\Task;

/**
 * @Process(name="StockKLineProcess", boot=false,inout=false,pipe=true,coroutine=true,num=6)
 */
class StockKLineProcess implements ProcessInterface
{
    private $service;
    public function run(SwoftProcess $process)
    {
        try{
            $stocks = StockService::all();
            $this->service = new SNSService();
            if($stocks)
            {
                foreach ($stocks as $stock)
                {
                    $this->getTradeItems($stock);
                }
            }
        }catch (\Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));
        }
    }
    public function getTradeItems($stock,$end='')
    {
        $count = 1024;
        if(empty($end))
        {
            $timestamp =time();
            $end = date('Y-m-d',$timestamp);
        }
        $items = $this->service->fqklineAll($stock,$end,$count);
        if($items)
        {
            foreach ($items as $key=>$item)
            {

                $item = FormatData::formatTradeData($item);
                if($key==0)
                {
                    $end = $item['date'];
                }
                Task::deliverByProcess("stock-sync","saveOneTradeData",[$stock,$item]);
            }
        }
        //如果返回的数据总数大于1，则
        if(count($items)>1)
        {
            $this->getTradeItems($stock,$end);
        }
    }
    public function check(): bool
    {
        return true;
    }
}