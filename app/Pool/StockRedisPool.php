<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2019/4/8
 * Time: 11:00
 */

namespace App\Pool;

use Swoft\Bean\Annotation\Inject;
use Swoft\Bean\Annotation\Pool;
use Swoft\Redis\Pool\RedisPool;
use App\Pool\Config\StockRedisPoolConfig;
/**
 * StockRedisPool
 *
 * @Pool("stockRedis")
 */
class StockRedisPool extends RedisPool
{
    /**
     * @Inject()
     * @var StockRedisPoolConfig
     */
    public $poolConfig;
}