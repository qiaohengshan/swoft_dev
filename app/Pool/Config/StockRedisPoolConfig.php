<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2019/4/8
 * Time: 11:02
 */

namespace App\Pool\Config;

use Swoft\Bean\Annotation\Bean;
use Swoft\Bean\Annotation\Value;
use Swoft\Redis\Pool\Config\RedisPoolConfig;

/**
 * StockRedisPoolConfig
 *
 * @Bean()
 */
class StockRedisPoolConfig extends RedisPoolConfig
{
    /**
     * @Value(name="${config.cache.stockRedis.db}", env="${REDIS_STOCK_REDIS_DB}")
     * @var int
     */
    protected $db = 0;

    /**
     * @Value(name="${config.cache.stockRedis.prefix}", env="${REDIS_STOCK_REDIS_PREFIX}")
     * @var string
     */
    protected $prefix = '';
}