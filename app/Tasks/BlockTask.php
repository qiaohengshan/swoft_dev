<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2019/4/25
 * Time: 11:58
 */

namespace App\Tasks;


use App\Models\Logic\BellwetherLogic;
use App\Models\Logic\ConceptLogic;
use App\Models\Logic\StockLogic;
use App\Services\MailService;
use App\Services\RedisService;
use App\Services\SNSBlockService;
use App\Services\SNSStatisticService;
use Swoft\App;
use Swoft\Process\ProcessBuilder;
use Swoft\Task\Bean\Annotation\Scheduled;
use Swoft\Exception\Exception;
use Swoft\Task\Bean\Annotation\Task;
use Swoft\Task\Task as SwoftTask;
/**
 * 版块/概念任务
 *
 * @Task("block")
 * @package App\Tasks
 */

class BlockTask
{
    /**
     * 版块列表
     * 每秒执行一次
     */
    public function block()
    {
        $type=2;
        $service = new SNSBlockService();
        try{
            $response = $service->list($type);
            $items = isset($response['items'])?$response['items']:[];
            if($items)
            {
                foreach ($items as $item)
                {
                    SwoftTask::deliverByProcess('block','store',[$type,$item]);
                }
                return 'block-success';
            }
            return 'block-fail';
        }catch (Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));
            return 'fail';
        }
    }

    /**
     * 概念列表
     * 每秒执行一次
     */
    public function concept()
    {
        $type=1;
        $service = new SNSBlockService();
        try{
            $response = $service->list($type);
            $items = isset($response['items'])?$response['items']:[];
            if($items)
            {
                foreach ($items as $item)
                {
                    SwoftTask::deliverByProcess('block','store',[$type,$item]);
                }
                return 'concept-success';
            }
            return 'concept-fail';
        }catch (Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));
            return 'fail';
        }
    }

    /**
     * 概念所属所对应的stock
     * 每秒执行一次
     */
    public function stocks()
    {
        $redisService = new RedisService();
        $blocks =  $redisService->hGetAll(ConceptLogic::NAME);
        $service = new SNSBlockService();
        if($blocks)
        {
            foreach ($blocks as $block)
            {
                $bkCode = $block['code'];
                $response = $service->stocks($bkCode);
                if(isset($response['items']) && $items = $response['items'])
                {
                    foreach ($items as $item)
                    {
                        if(  !empty($item['tag']) || !empty($item['tags']))
                        {
                            SwoftTask::deliverByProcess('block','storeBellwether',[$bkCode,$item]);
                        }
                    }
                }
            }
        }

        return 'block-stocks';
    }

    /**
     * 保存概念、版块数据
     * @param $type
     * @param $items
     */
    public function store($type,$item)
    {
        if($item)
        {
            $logic = new ConceptLogic();
            $data['name'] = $item['name'];
            $data['type'] = $type;
            $data['code'] = $item['bk_code'];
            $data['tags'] = json_encode($item['tags']);
            $logic->add($data);
        }
    }

    /**
     * 保存概念/版块标签
     * @param $code
     * @param $item
     * @throws \Swoft\Db\Exception\DbException
     */
    public function storeBellwether($code,$item)
    {
        if($item)
        {
            $logic = new BellwetherLogic();
            $data['stock_code'] = $item['code'];
            $data['bk_code'] = $code;
            $data['tag'] = $item['tag'];
            $data['tags'] = json_encode($item['tags']);
            $response = $logic->create($data);
        }
    }

    /**
     * 热点概念推送
     * 周一到周五推送
     */
    public function overview()
    {
        try{
            $service = new SNSStatisticService();
            $response = $service->tops();
            if(isset($response['overview']) && $response['overview'])
            {
                $overview = $response['overview'];

                $subject="[今日热点概念]:[".$overview["name"]."]";

                $body = "今日热点：<font color='red'>{$overview["name"]}</font>.入选原因:{$overview['hype_remark']}"."\r\n";

                $stocks = isset($overview['stocks'])?$overview['stocks']:[];

                if($stocks)
                {
                    $body = $body."\r\n";
                    $body = $body."当前人气龙头:";
                    foreach ($stocks as $stock)
                    {
                        $body = $body.$stock['name']."({$stock['symbol']})"."\r\n";
                    }
                }

                $incrementStocks =BellwetherLogic::recommend($overview['bk_code']);
                if($incrementStocks)
                {
                    $body = $body."行业龙头股和连续放量股票有:";
                    foreach ($incrementStocks as $item)
                    {
                        $stock = StockLogic::getOne($item['stock_code']);
                        if($stock)
                        {
                            $body = $body.$stock['name']."({$stock['symbol']})"."\r\n";
                        }

                    }
                }
                $recipients=["joy_qhs@163.com"];
                $response = MailService::send($subject,$body,$recipients);

                if($response == true)
                {

                }
            }
        }catch (\Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));
        }
    }

}