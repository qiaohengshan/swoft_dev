<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2018/12/7
 * Time: 17:25
 */

namespace App\Tasks;

use App\Models\Dao\StockTradeDao;
use App\Models\Entity\Stock;
use App\Models\Entity\StockIncrement;
use App\Models\Entity\StockProfile;
use App\Models\Logic\StockChipLogic;
use App\Models\Logic\StockLogic;
use App\Services\Algorithm;
use App\Services\BlockService;
use App\Services\FormatData;
use App\Services\SNSService;
use App\Services\StockService;
use Swoft\App;
use Swoft\Bean\Annotation\Inject;
use Swoft\Db\Db;
use Swoft\HttpClient\Client;
use Swoft\Rpc\Client\Bean\Annotation\Reference;
use Swoft\Exception\Exception;
use Swoft\Task\Bean\Annotation\Scheduled;
use Swoft\Task\Bean\Annotation\Task;
use Swoft\Bean\Annotation\Bean;
use Swoft\Task\Task as SwoftTask;
/**
 * Class StockSyncTask - define some tasks
 *
 * @Task("stock-sync")
 * @package App\Tasks
 */
class StockSyncTask
{

    /**
     * @Inject()
     * @var \Swoft\Redis\Redis
     */
    private $redis;

    const STOCK_TRADE_TIME = 'stock:trade:time';

    public function chips($stock,$chips)
    {
        foreach ($chips as $chip)
        {
            $item = FormatData::chips($chip);
            dump($item);
            $result = StockChipLogic::save($stock,$item);
            dump($result);
        }
    }
    /**
     * 新建stock信息
     * @param array $items
     */
    public function createStock(array $items)
    {
        $logic = new  StockLogic();
        $insertId=0;
        try{
            if($items)
            {
                foreach ($items as $item)
                {
                    if(isset($item['code']) && $item['code'])
                    {
                        $insertId = $logic->add($item);
                    }
                }
            }
            return $insertId;
        }catch (\Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),$exp->getMessage()));
        }
    }

    /**
     * 从当前时间获取，往前推，直到获取时间为上市时间
     * @param $stock
     */
    public function createStockTrade($stock)
    {
        try{
            $cache = $this->redis->hGet(self::STOCK_TRADE_TIME,$stock['code']);
            $profile = StockProfile::findOne(['stock_id'=>$stock['id']])->getResult();
            if(empty($cache)) //没有获取记录时间
            {
                $end = date('Y-m-d',time());
                $start=$end;
            }elseif($profile && strtotime($profile->getListingDate()) == strtotime($cache)){
                $end = date('Y-m-d',time());
                $start=$end;
            } else{
                $end = $cache;
                $start='';
            }
            $service = new SNSService();
            $service->fqkline($stock,$end,$start);
        }catch (\Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),$exp->getMessage()));
        }
    }

    /**
     * 保存交易数据
     * @param $stock
     * @param $items
     */
    public function saveStockTradeData($stock,$items)
    {

        foreach ($items as $item)
        {
            $item = FormatData::formatTradeData($item);
            $this->logic->saveTrade($stock,$item);
            //SwoftTask::deliverByProcess('stock-sync','syncCreateStock',[$stock,$item]);
        }
    }

    public function saveOneTradeData($stock,$item)
    {
        $logic = new StockLogic();
        $logic->saveTrade($stock,$item);
    }

    /**
     *
     * @param $stock
     * @param $item
     */
    public function syncCreateStock($stock,$item)
    {
        try{
            $this->logic->saveTrade($stock,$item);
        }catch (\Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),$exp->getMessage()));
        }


    }

    /**
     * 获取个股简介
     * @param $stock
     */
    public function CreateStockProfile($stock)
    {
        try {
            $service = new SNSService();
            $result = $service->profile($stock['code']);
            if ($result)
            {
                $result = FormatData::formatProfileData($result);
                $response = $this->logic->saveProfile($stock,$result);
            }
        }catch (\Throwable $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),$exp->getMessage()));
        }
    }

    /**
     * 保存详情
     * @param $stock 缓存数据
     */
    public function CreateStockDetail($stock)
    {
        try {
            $service = new SNSService();
            $result = $service->getFSData($stock['code']);
            if ($result)
            {
                $result = FormatData::formatDetailData($result['detail']);
                $response = $this->logic->saveStockDetail($stock,$result);
            }
        }catch (\Throwable $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),$exp->getMessage()));
        }
    }

    /**
     * 获取持续增量
     * @param $stock
     */
    public function getContinuousIncrement($stock)
    {
        $service = new StockTradeDao();
        $items = $service->getThreeRows($stock);
        $result = Algorithm::continuousIncrement($items,'Volume');
        if($result === true)
        {
            $data['date'] = strtotime(date('Y-m-d',time()));
            $data['stock_id'] = $stock['id'];
            $has = StockIncrement::findOne($data)->getResult();
            if(empty($has))
            {
                StockIncrement::fill($data)->save()->getResult();
            }
        }
    }

    /**
     * 获取行业/概念数据
     * @param $type 类型,1概念,2行业
     */
    public function getBlockData($type)
    {
        try{
            $service = new BlockService();
            $result = $service->list($type);
        }catch (\Throwable $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),$exp->getMessage()));
        }
    }

    /**
     * 获取所有的交易数据
     * @param $stock
     */
    public function syncGetAllTradeData($stock)
    {
        try{
            $cache = $this->redis->hGet(self::STOCK_TRADE_TIME,$stock['code']);
            $profile = StockProfile::findOne(['stock_id'=>$stock['id']])->getResult();
            if($profile)
            {
                $start = strtotime($profile->getListingDate());
                $timestamp = time();
                $end = date('Y-m-d',$timestamp);
                $count = ceil(($timestamp - $start) / 86400);
                $service = new SNSService();
                $rows = $service->fqklineAll($stock,$end,$count);
                if($rows)
                {
                   SwoftTask::deliverByProcess("stock-sync","saveStockTradeData",[$stock,$rows]);
                }
            }
        }catch (\Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),$exp->getMessage()));
        }
    }

    /**
     * 保存概念数据
     * @param $stock
     */
    public function stockConcept($stock,$concepts)
    {
        try {
            if($stock && $concepts)
            {
                $logic = new StockLogic();
                foreach ($concepts as $concept)
                {
                    $logic->saveStockConcept($stock,$concept);
                }
            }
            return 'Success';
        } catch (\Throwable $exp) {
            App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));
        }
    }
    public function getAllTrade($stock)
    {

    }

    /**
     * 保存详情
     * @param $stock
     * @param $item
     */
    public function saveStockDetail($stock,$item)
    {
        try {
            $result = FormatData::formatDetailData($item);
            $logic = new StockLogic();
            $logic->saveStockDetail($stock,$result);
        }catch (\Throwable $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),$exp->getMessage()));
        }
    }
}