<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2019/4/22
 * Time: 15:33
 */

namespace App\Tasks;


use App\Models\Logic\RedisLogic;
use App\Services\FormatData;
use App\Services\RedisService;
use App\Services\SNSService;
use App\Services\StockService;
use App\Services\TradeService;
use Swoft\App;
use Swoft\Bean\Annotation\Inject;
use Swoft\HttpClient\Client;
use Swoft\Rpc\Client\Bean\Annotation\Reference;
use Swoft\Exception\Exception;
use Swoft\Task\Bean\Annotation\Scheduled;
use Swoft\Task\Bean\Annotation\Task;
use Swoft\Task\Task as SwoftTask;
/**
 * stock交易任务
 *
 * @Task("stock-trade")
 * @package App\Tasks
 */
class StockTradeTask
{
    /**
     * 每分钟交易数据
     * 每秒执行一次
     */
    public function minute()
    {
        try{
            $service = new SNSService();
            $stocks = StockService::all();
            if($stocks)
            {
                foreach ($stocks as $stock)
                {
                    $response = $service->minute($stock);
                    SwoftTask::deliverByProcess('stock-trade','saveMinuteData',[$stock,$response['detail'],$response['items']]);
                }
            }
            return  "Success" ;
        }catch (Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),$exp->getMessage()));
        }
    }

    public function kline()
    {

    }

    /**
     * 保存每分钟交易数据
     * @param $stock
     * @param $detail
     * @param $items
     * @throws \Exception
     */
    public function saveMinuteData($stock,$detail,$items)
    {
        $service = new TradeService();
        if($items)
        {
            $detail = FormatData::formatDetailData($detail);
            foreach ($items as $item)
            {
                $data = FormatData::formatMinuteData($item);
                $data['date'] = $detail['time'];
                $data['stock_id']=$stock['id'];
                $data['code'] = $stock['code'];
                $data['symbol'] = $stock['symbol'];
                $service->storeMinuteData($data);
            }
        }
    }
}