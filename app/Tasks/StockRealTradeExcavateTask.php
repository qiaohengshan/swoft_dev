<?php

/**
 * This file is part of Swoft.
 *
 * @link https://swoft.org
 * @document https://doc.swoft.org
 * @contact group@swoft.org
 * @license https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Tasks;

use App\Models\Logic\StockAvgLineLogic;
use App\Models\Logic\TradeLogic;
use App\Services\Algorithm;
use App\Services\SNSService;
use App\Services\StockService;
use Swoft\App;
// use Swoft\Bean\Annotation\Inject;
// use Swoft\HttpClient\Client;
// use Swoft\Rpc\Client\Bean\Annotation\Reference;
use Swoft\Task\Bean\Annotation\Scheduled;
use Swoft\Task\Bean\Annotation\Task;
use Swoft\Task\Task as SwoftTask;
/**
 * Class StockRealTradeExcavateTask - define some tasks
 *
 * @Task("StockRealTradeExcavate")
 * @package App\Tasks
 */
class StockRealTradeExcavateTask
{

    /**
     * A cronTab task
     * 3-5 seconds per minute 每分钟第3-5秒执行
     */
    public function cronTask()
    {
        try
        {
            $date =strtotime(date('Y-m-d',time()));
            $service = new SNSService();
            $stocks  = StockService::all();
            if ($stocks)
            {
                foreach ($stocks as $stock)
                {
                    SwoftTask::deliverByProcess("StockRealTradeExcavate","excavateAverageLine",[$stock,$date]);
                }
            }
        }catch (\Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),$exp->getMessage()));
        }
    }

    public function excavateAverageLine($stock,$date)
    {
        $where= [
            'stock_id'=> $stock['id'],
            'date'=>$date,
        ];
        $rows = TradeLogic::minute($where);
        $avg = Algorithm::minuteAverage($rows,'price');
        $result = false;
        foreach ($rows as $key=>$row)
        {
            if($row['price']>= $avg[$key])
            {
                $result = true;
            }else{
                $result = false;
                break;
            }
        }
        if ($result === true)
        {
            $data = [
                'stock_id'=>$stock['id'],
                'type'=>1,
                'date'=>date('Y-m-d',$date),
            ];
            StockAvgLineLogic::add($data);
        }
    }
}
