<?php

/**
 * This file is part of Swoft.
 *
 * @link https://swoft.org
 * @document https://doc.swoft.org
 * @contact group@swoft.org
 * @license https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Tasks;

use App\Models\Logic\StockArticleLogic;
use App\Services\FormatData;
use App\Services\SNSArticleService;
use App\Services\StockService;
use Swoft\App;
// use Swoft\Bean\Annotation\Inject;
// use Swoft\HttpClient\Client;
// use Swoft\Rpc\Client\Bean\Annotation\Reference;
use Swoft\Task\Bean\Annotation\Scheduled;
use Swoft\Task\Bean\Annotation\Task;
use Swoft\Task\Task as SwoftTask;

/**
 * Class StockArticleTask - define some tasks
 *
 * @Task("StockArticle")
 * @package App\Tasks
 */
class StockArticleTask
{
    /**
     * @\Swoft\Bean\Annotation\Inject("StockArticleLogic")
     * @var StockArticleLogic
     */
    private $logic;
    /**
     * A cronTab task
     * 3-5 seconds per minute 每分钟第3-5秒执行
     *
     */
    public function article()
    {
        try{
            $service = new SNSArticleService();
            $stocks = StockService::all();
            if($stocks)
            {
                foreach ($stocks as $stock)
                {
                    $end = 0 ;
                    do{
                        $response = $service->list($stock,$end);
                        if(empty($response))
                        {
                            break;
                        }else{
                            $items = $response;
                            $last = array_pop($response);
                            $end = $last['publish_time'];
                            SwoftTask::deliverByProcess('StockArticle','save',[$stock,$items]);
                        }
                    }while(true);
                }
            }
            return  "article-success" ;
        }catch (Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),$exp->getMessage()));
        }
    }

    /**
     * 保存文章信息
     * @param $stock
     * @param $items
     */
    public function save($stock,$items)
    {
        if($items)
        {
            foreach ($items as $item)
            {
                $data = FormatData::article($item);
                $content =SNSArticleService::content($item['url']);
                if($content)
                {
                    $data['content'] = $content[0]['content'];
                }
                $data['stock_id'] = $stock['id'];
                $this->logic->save($data);
            }
        }
    }
}
