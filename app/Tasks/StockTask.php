<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2019/4/16
 * Time: 11:58
 */

namespace App\Tasks;


use App\Models\Logic\RedisLogic;
use App\Services\RedisService;
use App\Services\SNSService;
use App\Services\StockService;
use Swoft\App;
use Swoft\Bean\Annotation\Inject;
use Swoft\HttpClient\Client;
use Swoft\Rpc\Client\Bean\Annotation\Reference;
use Swoft\Exception\Exception;
use Swoft\Task\Bean\Annotation\Scheduled;
use Swoft\Task\Bean\Annotation\Task;
use Swoft\Task\Task as SwoftTask;

/**
 * stock任务
 *
 * @Task("stock")
 * @package App\Tasks
 */
class StockTask
{
    /**
     * stock列表
     * 每秒执行一次
     */
    public function lists()
    {
        try{
            $service = new SNSService();
            $response = $service->quoteranklist();
            return $response === true ?"Stock-Success":"Stock-Fail";
        }catch (Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));
        }
    }
    /**
     * stock概念
     * 每秒执行一次
     */
    public function block()
    {
        try{
            $stocks = StockService::all();
            if($stocks)
            {
                foreach ($stocks as $stock)
                {
                    $service = new SNSService();
                    $response = $service->block($stock['code']);
                    SwoftTask::deliverByProcess('stock-sync','stockConcept',[$stock,$response]);
                }
            }
            return  "Block-Success" ;
        }catch (Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));
        }
    }

    /**
     * stock详情
     * 每秒执行一次
     */
    public function detail()
    {
        try{
            $stocks = StockService::all();
            $service = new SNSService();
            if($stocks)
            {
                foreach ($stocks as $stock)
                {
                    $detail = $service->stockdetail($stock);
                    SwoftTask::deliverByProcess('stock-sync','saveStockDetail',[$stock,$detail]);
                }
            }
            return  "detail-Success" ;
        }catch (Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));
        }
    }
    /**
     * 筹码
     * 每秒执行一次
     * @Scheduled(cron="3-5 * * * * *")
     */
    public function chips()
    {
        try{
            $stocks = StockService::all();
            $service = new SNSService();
            if($stocks)
            {
                foreach ($stocks as $stock)
                {
                    $chips = $service->chips($stock);
                    SwoftTask::deliverByProcess('stock-sync','chips',[$stock,$chips]);
                }
            }
            return  "chips-Success" ;
        }catch (Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));
        }
    }
}