<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2019/5/20
 * Time: 15:36
 */

namespace App\Tasks;


use App\Models\Logic\StockStrategyLogic;
use App\Services\FormatData;
use App\Services\SNSStrategyService;
use Swoft\App;
use Swoft\Bean\Annotation\Inject;
use Swoft\HttpClient\Client;
use Swoft\Rpc\Client\Bean\Annotation\Reference;
use Swoft\Exception\Exception;
use Swoft\Task\Bean\Annotation\Scheduled;
use Swoft\Task\Bean\Annotation\Task;
use Swoft\Task\Task as SwoftTask;

/**
 * 策略任务
 *
 * @Task("strategy")
 * @package App\Tasks
 */
class StockStrategy
{
    /**
     * 关注信号
     * 每秒执行一次
     * @Scheduled(cron="3-5 * * * * *")
     */
    public function attentionStocks()
    {
        try{
            $service = new SNSStrategyService();
            $response = $service->attentionstocks();
            return $response;
        }catch (\Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));
        }
    }

    /**
     * 保存关注信号
     * @param $items
     * @throws \Swoft\Db\Exception\DbException
     */
    public function saveAttentionStocks($items)
    {
        if($items)
        {
            foreach ($items as $item)
            {
                $data = FormatData::formatAttentionStockData($item);
                StockStrategyLogic::attention($data);
            }
        }
    }
}