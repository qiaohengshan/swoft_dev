<?php
/**
 * Created by PhpStorm.
 * User: qiaohengshan
 * Date: 2019/4/22
 * Time: 12:02
 */

namespace App\Tasks;


use App\Models\Logic\StatisticsLogic;
use App\Models\Logic\TopsLogic;
use App\Models\Logic\TradeLogic;
use App\Services\FormatData;
use App\Services\RedisService;
use App\Services\SNSStatisticService;
use Swoft\App;
use Swoft\Exception\Exception;
use Swoft\Task\Bean\Annotation\Scheduled;
use Swoft\Task\Bean\Annotation\Task;
use Swoft\Task\Task as SwoftTask;

/**
 * stock任务
 *
 * @Task("statistic-task")
 * @package App\Tasks
 */
class StatisticTask
{
    const KEY='hot-tops:stock';
    /**
     * 今日热点
     * 每秒执行一次
     */

    public function tops()
    {
        try{
            $service = new SNSStatisticService();
            $response = $service->tops();
            if(isset($response['overview']) && $response['overview'])
            {
                SwoftTask::deliverByProcess('statistic-task','saveTopsData',[$response['overview']]);
                SwoftTask::deliverByProcess('statistic-task','recommend',[$response['overview']]);
            }
            if(isset($response['vary']) && $response['vary'])
            {
                SwoftTask::deliverByProcess('statistic-task','vary',[$response['vary']]);
            }
        }catch (Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));
        }
    }

    /**
     * 异动
     * @param $items
     * @return string
     * @throws \Exception
     */
    public function vary($items)
    {

        if($items)
        {
            foreach ($items as $item)
            {
                $item['stocks'] = json_encode($item['stocks']);
                $item['date'] = date('Y-m-d',time());
                TopsLogic::vary($item);
            }
        }
        return 'vary-success';
    }

    /**
     * 热点概念推荐，根据对应的版块code,获取最近持续放量，或者龙头股
     * @param $block
     */
    public function recommend($block)
    {
        try{
           $stocks  = (new StatisticsLogic())->selectHotStocks($block);
           $logic   = new TradeLogic();

           if($stocks)
           {
               $service = new TopsLogic();
                foreach ($stocks as $stock)
                {
                    $result = $logic->volume($stock);
                    if($result)
                    {
                        $data['code'] = $block['bk_code'];
                        $data['stock_id'] = $stock['id'];
                        $data['create_time'] = date('Y-m-d',time());
                        $service->map($data);
                    }
                }
           }
        }catch (Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));
        }
    }

    /**
     * 异步保存热点数据
     * @param $item
     * @throws \Swoft\Db\Exception\DbException
     */
    public function saveTopsData($item)
    {
        try{
            $service = new StatisticsLogic();
            $row = FormatData::tops($item);
            $response = $service->storeTops($row);
            return $response;
        }catch (Exception $exp){
            App::error(sprintf('%s has error,error:%s', get_class($this),"{$exp->getFile()};{$exp->getLine()};{$exp->getMessage()}"));
        }
    }
}