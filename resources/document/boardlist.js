!
    function(e) {
        function t(r) {
            if (n[r]) return n[r].exports;
            var a = n[r] = {
                i: r,
                l: !1,
                exports: {}
            };
            return e[r].call(a.exports, a, a.exports, t),
                a.l = !0,
                a.exports
        }
        var n = {};
        t.m = e,
            t.c = n,
            t.d = function(e, n, r) {
                t.o(e, n) || Object.defineProperty(e, n, {
                    configurable: !1,
                    enumerable: !0,
                    get: r
                })
            },
            t.n = function(e) {
                var n = e && e.__esModule ?
                    function() {
                        return e["default"]
                    }: function() {
                        return e
                    };
                return t.d(n, "a", n),
                    n
            },
            t.o = function(e, t) {
                return Object.prototype.hasOwnProperty.call(e, t)
            },
            t.p = "//hqres.eastmoney.com/EMQuote_Center3.0/",
            t(t.s = "./ClientApp/boardlist.js")
    } ({
        "./ClientApp/adapter.js": function(e, t, n) {
            n("./ClientApp/modules/jsonp.js");
            var r = n("./ClientApp/modules/uri/main.js"),
                a = n("./ClientApp/config/adapter.json"),
                o = n("./ClientApp/modules/utils.extend.js");
            e.exports = function(e) {
                var t = o(new
                    function() {
                        this.host = "quote.eastmoney.com",
                            this.router = a
                    },
                    e || {},
                    !0);
                this.redirect = function(e) {
                    var n = function(e) {
                            var t = e.indexOf("?"),
                                n = e.indexOf("#"),
                                r = t > 0 ? e.substring(t, n > t ? n: e.length) : "",
                                a = n > 0 ? (e.substr(n) || "").split("?")[0] : "";
                            if (t < 0 && n < 0) return e;
                            var o = e.substr(0, t < 0 ? n: n < 0 ? t: Math.min(t, n));
                            return r && (o += r),
                            a && (o += a),
                                o
                        } (e || window.location.href),
                        a = new r(n),
                        i = a.pathname(),
                        s = a.hash() || "",
                        l = t.host ? "//" + t.host: "",
                        c = t.router[(i + s).toLowerCase()];
                    if (!c) {
                        var u = /^#2800(\d)(\d{3})\w*/.exec(s);
                        if (u) {
                            var d = "BK0" + u[2] + "1";
                            u[1],
                                c = "/center/boardlist.html#boards-" + d
                        }
                    }
                    if (/^(http:\/\/|https:\/\/)/.test(c)) window.location.replace(c);
                    else if (c) {
                        var h = new r(c).absoluteTo(l);
                        h.query(function(e) {
                            o(e, a.query(!0) || {})
                        }),
                            window.location.replace(h.toString())
                    }
                }
            }
        },
        "./ClientApp/base.js": function(e, t, n) { (function(t) {
            n("./ClientApp/adapter.js");
            var r = n("./ClientApp/components/qoute-header/header.js"),
                a = n("./ClientApp/components/quote-sidemenu/sidemenu.js"),
                o = n("./ClientApp/components/quote-sidetoolbox/toolbox.js");
            n("./ClientApp/modules/jquery-plugins/jquery.postfixed.js");
            n("./ClientApp/css/common.css");
            var i = n("./ClientApp/modules/utils.js"),
                s = n("./ClientApp/modules/template-web.js");
            t.extend(s.defaults.imports, i),
                e.exports = function(e) {
                    var n = t.extend({
                            sidemenu: {
                                disable: !1,
                                args: {}
                            },
                            rightbox: {
                                disable: !1
                            }
                        },
                        e);
                    if (this.sidemenu = a, !n.sidemenu.disable) {
                        var i = t.extend({
                                current: this.currentmenu,
                                ajax: {
                                    url: "/center/sidemenu.json",
                                    dataType: "json",
                                    cache: !0
                                },
                                onloaded: function(e, n, r) {
                                    setTimeout(function() {
                                            t(e).posfixed({
                                                direction: "top-bottom",
                                                type: "while",
                                                distance: {
                                                    top: t(".top-nav-wrap").outerHeight(!0) || 0,
                                                    bottom: t(".page-footer").outerHeight(!0) || 0
                                                }
                                            })
                                        },
                                        1500)
                                }
                            },
                            n.sidemenu.args);
                        this.menu = new a(t("#sidemenu"), i)
                    }
                    this.beforeLoading = function(e) {
                        r.load(),
                        n.sidemenu.disable || this.menu.load(),
                        "function" == typeof e && e(this)
                    },
                        this.afterLoaded = function(e) {
                            "function" == typeof event && event(this),
                            n.rightbox && !n.rightbox.disable && (this.rightbox = new o({
                                gotop: {
                                    show: "while",
                                    scrollTop: t("#page-body").offset().top
                                }
                            }))
                        }
                }
        }).call(t, n("jquery"))},
        "./ClientApp/boardlist.js": function(e, t, n) {
            function r(e) {
                i.call(this);
                var t = this;
                this.gridview = null;
                var n = [],
                    r = [];
                this.load = function() {
                    var i = function(e) {
                        var t, n = p,
                            r = (e = e || "default").split("-"),
                            o = r[0],
                            i = r[1];
                        return t = n[o] ? a.extend(!0, {},
                            n["default"], n[o]) : n[o = "default"],
                            t.key = e,
                            t.id = i,
                            t
                    } (e || (window.location.hash || "#").substr(1));
                    this.menu.loaded && this.menu.draw(),
                        function(e, t) {
                            if (!e) return "function" == typeof t && t(!1),
                                !1;
                            a.ajax({
                                url: "//nufm.dfcfw.com/EM_Finance2014NumericApplication/JS.aspx?type=CT&sty=DCRRSPFBK&js=((x))&token=4f1862fc3b5e77c150a2b985b12db0fd",
                                data: {
                                    cmd: e
                                },
                                dataType: "jsonp",
                                jsonp: "cb",
                                success: function(e) {
                                    if ("string" == typeof e) {
                                        var n = e.split(",");
                                        if ("function" == typeof t) {
                                            var r = {
                                                Code: n[0],
                                                SimpleCode: n[0].replace("BK0", ""),
                                                JYS: n[1],
                                                Name: n[2],
                                                MarketType: n[3]
                                            };
                                            t(r)
                                        }
                                    }
                                }
                            })
                        }
                        (i.id,
                            function(e) { !
                                function(e, t) {
                                    var n = e.hotlinks;
                                    if (!n) return ! 1;
                                    var r = a(n.container),
                                        i = a(n.container + "-wrapper");
                                    n.show ? i.show() : i.hide();
                                    var s = o.render(d, a.extend({
                                            JYS: {
                                                region_board: "1",
                                                industry_board: "2",
                                                concept_board: "3"
                                            } [e.key]
                                        },
                                        t));
                                    r.html(s)
                                } (i, e),
                                function(e, t) {
                                    var i = e.digest;
                                    if (t) {
                                        var d = o.render(u, t);
                                        a(i.container).html(d).show();
                                        var h = i["capitalflow-rank"],
                                            f = i.capitalflowchart,
                                            p = i.quotechart,
                                            m = t.Code + t.MarketType;
                                        if (h.params = new s(h.params).setSearch("cmd", "C." + m).toString(), a(h.container).ListView(h.type, h), g) {
                                            var v = a.extend({},
                                                p.compatible, {
                                                    data: {
                                                        id: m
                                                    },
                                                    success: function(e) {
                                                        a(p.compatible.container).html(e)
                                                    },
                                                    cache: !(p.compatible.update > 0)
                                                }),
                                                b = a.extend({},
                                                    f.compatible, {
                                                        data: {
                                                            id: m
                                                        },
                                                        success: function(e) {
                                                            a(f.compatible.container).html(e)
                                                        },
                                                        cache: !(f.compatible.update > 0)
                                                    });
                                            c(v),
                                                c(b),
                                                n.push(setInterval(function() {
                                                        c(v)
                                                    },
                                                    p.compatible.update)),
                                                n.push(setInterval(function() {
                                                        c(b)
                                                    },
                                                    f.compatible.update))
                                        } else l.preload(function() {
                                            var e = new l(p.type, a.extend({
                                                    code: m
                                                },
                                                p.options));
                                            e.load();
                                            var n = new l(f.type, f.options),
                                                o = ["09:30", "11:30/13:00", "15:00"];
                                            n.dataloader = function(e) {
                                                a.ajax({
                                                    url: f.options.baseurl,
                                                    data: {
                                                        id: m
                                                    },
                                                    dataType: "jsonp",
                                                    jsonp: "cb",
                                                    success: function(n) {
                                                        if (e.stop(), !n) return ! 1;
                                                        var r = [],
                                                            a = [];
                                                        if ("string" == typeof n.xa) {
                                                            for (var i = n.xa.split(","), s = 0; s < i.length; s++)"11:30" === i[s] && (i[s] = "11:30/13:00"),
                                                            i[s] && r.push({
                                                                show: o.indexOf(i[s]) >= 0,
                                                                showline: o.indexOf(i[s]) >= 0,
                                                                time: i[s]
                                                            });
                                                            var l = r[0],
                                                                c = r[r.length - 1];
                                                            l && (l.show = !0, l.showline = !0),
                                                            c && (c.show = !0, c.showline = !0)
                                                        }
                                                        if (n.ya instanceof Array) for (s = 0; s < n.ya.length; s++) {
                                                            var u = n.ya[s].split(",");
                                                            u[0] && a.push(u[0])
                                                        }
                                                        e.setData({
                                                            title: t.Name + "资金流",
                                                            subfix: "万元",
                                                            xaxis: r,
                                                            yaxis: {
                                                                title: "资金净流入",
                                                                data: a
                                                            }
                                                        }),
                                                            e.draw()
                                                    }
                                                })
                                            },
                                                n.load(),
                                                r.push.apply(r, [e, n])
                                        })
                                    } else a(i.container).hide()
                                } (i, e),
                                t.gridview = function(e) {
                                    function t(o, i) {
                                        if ("data" === r && ["concept_board", "region_board", "industry_board"].indexOf(e.key) >= 0) {
                                            var s = new a.fn.dataTable.Api(this),
                                                l = s.order()[0],
                                                c = i.aoColumns[l[0]];
                                            if (c && "ChangePercent" === c.name) {
                                                var u = "desc" === l[1];
                                                s.column("BKCPLEADNAME:name").visible(u, !1).column("BKCPLEADCP:name").visible(u, !1),
                                                    s.column("BKCPLoseName:name").visible(!u, !1).column("BKCPLoseCP:name").visible(!u, !1),
                                                    s.columns.adjust().draw(!1)
                                            }
                                            a(n.container).one("order.dt", t)
                                        }
                                    }
                                    var n = e.gridtable,
                                        r = "data",
                                        i = new s(location).search(!0);
                                    i.mode && (r = i.mode.toLowerCase());
                                    var l = n.config;
                                    if ("pic" === r) l = n["config-pic"];
                                    else {
                                        if ("string" == typeof l.fields) {
                                            var c = (l.fields || "").replace("{{key}}", e.key);
                                            l.fields = f[c]
                                        }
                                        if (!l.fields || !l.params) return ! 1
                                    }
                                    if (e.id) {
                                        var u = e.id.indexOf(".") > 0 ? e.id: "C." + e.id;
                                        l.params = new s(l.params).setSearch("cmd", u).toString()
                                    }
                                    if (!l.mappings && l.mappingsKey) {
                                        var d = new s(l.params).search(!0),
                                            p = o.render(l.mappingsKey, d);
                                        h.hasOwnProperty(p) && (l.mappings = h[p])
                                    }
                                    return function(e) {
                                        if (location.search) {
                                            var t = new s(location).search(!0),
                                                n = {
                                                    A: "Code",
                                                    B: "Close",
                                                    C: "ChangePercent",
                                                    D: "Change",
                                                    E: "Amount",
                                                    F: "Volume",
                                                    G: "FiveMinuteChangePercent",
                                                    H: "VolumeRate",
                                                    I: "Peration",
                                                    J: "TurnoverRate",
                                                    K: "Amplitude",
                                                    L: "ListingDate"
                                                },
                                                r = {
                                                    1 : "asc",
                                                    "-1": "desc"
                                                },
                                                a = t.sortType || t.st,
                                                o = t.sortRule || t.sr || "-1",
                                                i = "pic" === t.mode;
                                            if (a) if (n.hasOwnProperty(a.toUpperCase()) && (a = n[a.toUpperCase()]), i) e["config-pic"].sort = [a, o];
                                            else for (var l = 0; l < e.config.fields.length; l++) {
                                                    var c = e.config.fields[l];
                                                    if (c.data === a || c.name === a) {
                                                        e.config.order = [[l, r[o]]];
                                                        break
                                                    }
                                                }
                                        }
                                    } (n),
                                        a(n.container).one("order.dt", t).ListView("pic" === r ? "pic": "full", l)
                                } (i)
                            })
                },
                    this.init = function() { (function() {
                        var e = this,
                            t = a('<a class="checkNew"><em></em>查看最新</a>').click(function(e) {
                                return window.location.reload(),
                                    !1
                            }),
                            n = a("<a></a>"),
                            r = new s(location);
                        "pic" === (r.search(!0) || {}).mode ? n.addClass("data-table-mode").html("<em></em>列表").click(function(e) {
                            return window.location.href = r.search(function(e) {
                                e.mode = "data"
                            }).toString(),
                                !1
                        }) : n.addClass("pic-table-mode").html("<em></em>多股同列").click(function(t) {
                            var n, a, o;
                            if (e.gridview && e.gridview.dataTable) {
                                n = e.gridview.dataTable.order()[0];
                                var i = e.gridview.dataTable.column(n[0]).init();
                                a = n instanceof Array ? i.columns[n[0]].name: "",
                                    o = n instanceof Array ? m[n[1]] || "-1": ""
                            }
                            return window.location.href = r.search(function(e) {
                                e.mode = "pic",
                                a && (e.sortType = a),
                                o && (e.sortRule = o)
                            }).toString(),
                                !1
                        });
                        a("#tools").html([n, t])
                    }).apply(this),
                        this.beforeLoading(),
                        this.load(),
                        this.afterLoaded()
                    },
                    this.destroy = function() {
                        this.gridview && this.gridview.destroy();
                        for (var e = 0; e < r.length; e++) {
                            var t = r[e];
                            "function" == typeof t.stop && t.stop()
                        }
                        for (e = 0; e < n.length; e++) clearInterval(n[e])
                    }
            }
            n("./ClientApp/css/boardlist.css");
            var a = n("jquery"),
                o = (n("./ClientApp/modules/utils.js"), n("./ClientApp/modules/template-web.js")),
                i = n("./ClientApp/base.js"),
                s = (n("./ClientApp/modules/listview/main.js"), n("./ClientApp/modules/uri/main.js")),
                l = (n("./ClientApp/modules/jquery-plugins/jquery.ba-hashchange.js"), n("./ClientApp/modules/quotecharts.js")),
                c = n("./ClientApp/modules/asyncloaders.js").imgLoader,
                u = n("./ClientApp/templates/boards.digest.art"),
                d = n("./ClientApp/templates/boards.hotlinks.art"),
                h = n("./ClientApp/config/gridlist.fields.map.json"),
                f = function(e) {
                    for (var t = e.keys(), n = {},
                             r = 0; r < t.length; r++) {
                        var o = t[r];
                        a.extend(!0, n, e(o))
                    }
                    return n
                } (n("./ClientApp/config recursive gridlist\\.fields\\.json$")),
                p = a.extend(n("./ClientApp/config/gridlist.cfg.json"), n("./ClientApp/config/borads.gridlist.cfg.json")),
                m = {
                    1 : "asc",
                    "-1": "desc"
                },
                g = !1,
                v = new r;
            v.init(),
                a(window).hashchange(function(e) {
                    v.destroy(),
                        v.load()
                }),
                e.exports = r
        },
        "./ClientApp/components/qoute-header/header.css": function(e, t) {},
        "./ClientApp/components/qoute-header/header.js": function(e, t, n) {
            function r(e) {
                var t, n, r = this,
                    a = this.options = s.extend({
                            url: f + "&cmd=C._UIFO&sty=sfcoo&st=z&token=4f1862fc3b5e77c150a2b985b12db0fd",
                            template: '{{each data val idx}}<a href="/gb/zs{{val.Code}}.html">{{val.Name}}</a> {{val.Close}} <span class="{{val.Change | getColor}}">{{val.Arrow}}{{val.Change}} {{val.Arrow}}{{val.ChangePercent}}</span>{{/each}}',
                            render: function(e) {
                                if (e instanceof Array) {
                                    for (var t = [], r = 0; r < e.length; r++) if ("string" == typeof e[r]) {
                                        var o = e[r].split(",");
                                        t.push({
                                            Market: o[0],
                                            Code: o[1],
                                            Name: o[2],
                                            Close: o[3],
                                            Change: o[4],
                                            ChangePercent: o[5],
                                            Arrow: parseFloat(o[4]) > 0 ? "↑": parseFloat(o[4]) < 0 ? "↓": ""
                                        })
                                    }
                                    var i = u.render(a.template, {
                                        data: t
                                    });
                                    s("#globalindex").html(i),
                                    n && n.cancel(),
                                        (n = new l(s("#globalindex")[0])).animate()
                                }
                            },
                            update: 12e4
                        },
                        e);
                this.load = function() {
                    clearTimeout(t),
                        s.ajax({
                            url: a.url,
                            dataType: "jsonp",
                            jsonp: "cb",
                            success: function(e) {
                                return a.render(e)
                            },
                            error: function(e, t, n) {
                                console.error(t, n)
                            },
                            complete: function() {
                                t = setTimeout(function() {
                                        r.load()
                                    },
                                    a.update)
                            }
                        })
                }
            }
            function a() {
                var e, t = this;
                this.load = function() {
                    clearTimeout(e),
                        s.ajax({
                            url: f + "&cmd=P.(x),(x)|0000011|3990012&sty=SHSTD|SZSTD&st=z&token=4f1862fc3b5e77c150a2b985b12db0fd",
                            dataType: "jsonp",
                            jsonp: "cb",
                            success: function(e) {
                                if (e instanceof Array && 2 === e.length) {
                                    if ("string" == typeof e[0]) { (n = e[0].split(","))[6] && s("#hgtzj").html(n[6]).removeClass("red green").addClass(c.getColor(n[6])),
                                        s("#hgtrun").html(t.getstatus(n[9])),
                                    n[0] && s("#ggthzj").html(n[0]).removeClass("red green").addClass(c.getColor(n[0])),
                                        s("#ggthrun").html(t.getstatus(n[3]))
                                    }
                                    if ("string" == typeof e[1]) {
                                        var n; (n = e[1].split(","))[6] && s("#sgtzj").html(n[6]).removeClass("red green").addClass(c.getColor(n[6])),
                                            s("#sgtrun").html(t.getstatus(n[9])),
                                        n[0] && s("#ggtszj").html(n[0]).removeClass("red green").addClass(c.getColor(n[0])),
                                            s("#ggtsrun").html(t.getstatus(n[3]))
                                    }
                                }
                            },
                            complete: function() {
                                e = setTimeout(t.load, 2e4)
                            }
                        })
                },
                    this.getstatus = function(e) {
                        var t = "",
                            n = parseFloat(e);
                        if ("NaN" == n || isNaN(n)) return t = "<b></b>正常";
                        switch (n) {
                            case 0:
                                t = '<b class="icon icon_stockopen"></b>有额度';
                                break;
                            case 2:
                                t = '<b class="icon icon_stockopen"></b>午休';
                                break;
                            case 4:
                                t = '<b class="icon icon_stockopen"></b>清空';
                                break;
                            case - 2 : t = '<b class="icon icon_stockopen"></b>无额度';
                                break;
                            case - 1 : t = '<b class="icon icon_stockclose"></b>停牌';
                                break;
                            case 1:
                                t = '<b class="icon icon_stockclose"></b>收盘';
                                break;
                            case 3:
                                t = '<b class="icon icon_stockclose"></b>休市';
                                break;
                            case 5:
                                t = '<b class="icon icon_stockclose"></b>限买';
                                break;
                            case 6:
                                t = '<b class="icon icon_stockclose"></b>限卖';
                                break;
                            case 7:
                                t = '<b class="icon icon_stockclose"></b>暂停';
                                break;
                            case 8:
                                t = '<b class="icon icon_stockclose"></b>5%熔断';
                                break;
                            case 9:
                                t = '<b class="icon icon_stockclose"></b>7%熔断';
                                break;
                            case 10:
                                t = '<b class="icon icon_stockclose"></b>-5%熔断';
                                break;
                            case 11:
                                t = '<b class="icon icon_stockclose"></b>-7%熔断'
                        }
                        return t
                    }
            }
            function o() {
                var e, t, n, r, a = this;
                this.load = function() {
                    s.ajax({
                        url: h + "/static/portfolio.json",
                        dataType: "jsonp",
                        jsonp: "cb",
                        cache: !0,
                        jsonpCallback: "portfolioLoader",
                        success: function(o) {
                            if (!o || !o.success) return ! 1;
                            var i = o.data;
                            e = i.ReturnRateDaily,
                                t = i.ReturnRateWeekly,
                                n = i.ReturnRateMonthly,
                                r = i.ReturnRateYearly,
                                a.render()
                        }
                    })
                },
                    this.template = '<ul><li><a href="http://group.eastmoney.com/index.html" target="_blank">高手日收益达<span>{{rateDay}}</span></a></li><li><a href="http://group.eastmoney.com/index.html" target="_blank">高手周收益达<span>{{rateWeek}}</span></a></li><li><a href="http://group.eastmoney.com/index.html" target="_blank">高手月收益达<span>{{rateMonth}}</span></a></li><li><a href="http://group.eastmoney.com/index.html" target="_blank">高手年收益达<span>{{rateYear}}</span></a></li></ul>',
                    this.render = function() {
                        if (! (e && t && n && r)) return ! 1;
                        var o = s("#zhrool"),
                            i = u.render(a.template, {
                                rateDay: e,
                                rateWeek: t,
                                rateMonth: n,
                                rateYear: r
                            });
                        if (0 === o.length) return ! 1;
                        o.html(i).vTicker({
                            showItems: 1,
                            height: 28
                        })
                    }
            }
            function i() {
                var e, t = this;
                this.load = function() {
                    clearTimeout(e),
                        s.ajax({
                            url: h + "/static/bulletin/752",
                            dataType: "jsonp",
                            jsonp: "cb",
                            success: function(e) {
                                t.render(e)
                            },
                            complete: function() {
                                e = setTimeout(t.load, 3e5)
                            }
                        })
                },
                    this.render = function(e) {
                        try {
                            var t = s(e),
                                n = s("li h3", t);
                            n.length > 0 && s("#headlines").html(n.first().text())
                        } catch(r) {
                            console.error(r)
                        }
                    }
            }
            n("./ClientApp/components/qoute-header/header.css");
            var s = n("jquery"),
                l = n("./ClientApp/modules/qcsroll.js"),
                c = n("./ClientApp/modules/utils.js"),
                u = n("./ClientApp/modules/template-web.js"),
                d = (n("./ClientApp/modules/jquery-plugins/jquery.vticker.js"), n("./ClientApp/modules/asyncloaders.js")),
                h = (d = n("./ClientApp/modules/asyncloaders.js"), "production" === environment ? "//quote.eastmoney.com/api": "//quotationtest.eastmoney.com/api"),
                f = "//nufm.dfcfw.com/EM_Finance2014NumericApplication/JS.aspx?type=CT";
            e.exports = {
                load: function() {
                    var e = {};
                    s('meta[name="quote-tag"]', "head")[0] && (e.set = s('meta[name="quote-tag"]', "head").attr("content")),
                        d.scriptLoader({
                            url: "//emcharts.dfcfw.com/suggest/stocksuggest2017.min.js",
                            success: function() {
                                new suggest2017({
                                    inputid: "search_box",
                                    width: 300,
                                    shownewtips: !0,
                                    newtipsoffset: {
                                        top: -2,
                                        left: 0
                                    }
                                })
                            }
                        }),
                        d.scriptLoader({
                            url: "//hqres.eastmoney.com/EMQuote_Lib/js/HisAcc.js",
                            charset: "gb2312",
                            success: function() {
                                new HistoryViews("historyest", {
                                    def: "",
                                    set: "",
                                    lns: 0
                                })
                            }
                        }),
                        (new r).load(),
                        (new a).load(),
                        (new i).load(),
                        (new o).load()
                },
                globalstocks: r,
                capitalflow: a,
                headlines: i,
                portfolio: o
            }
        },
        "./ClientApp/components/quote-sidemenu/sidemenu.art": function(e, t) {e.exports = '<div class="side-menu-inner">\r\n    <div class="head"></div>\r\n    <div class="level-list">\r\n        <ul class="level1-wrapper">\r\n            {{each menu val idx}}\r\n            <li class="level1-wrapper-li menu-{{val.key}}-wrapper" style="{{val.show===false?\'display:none\':\'\'}}">\r\n                <a id="menu_{{val.key}}" class="level1-ahref menu-{{val.key}} {{val.classtitle}}"\r\n                    data-key="{{val.key}}" data-path=""\r\n                    title="{{val.title}}" href="{{val.href}}" target="{{val.target||\'_self\'}}">\r\n                    <span class="level1-icon"></span>\r\n                    <span class="text">{{val.title}}</span>\r\n                    {{if val.hot}}<span class="icon-hot"></span>{{/if}}\r\n                    {{if val.next && val.next.length>0}}\r\n                    <span class="dot leftmenu-right-arrow"></span>\r\n                    {{/if}}\r\n                </a>\r\n                {{if val.next && val.next.length>0}}\r\n                <div class="level2-wrapper">\r\n                    <div class="level2-list menu-{{val.key}}-wrapper" data-count="{{val.next.length}}">\r\n                        <ul class="level2-items">\r\n                            {{each val.next val1 idx1}}\r\n                            <li class="level2-wrapper-li" style="{{val1.show===false?\'display:none\':\'\'}}">\r\n                                <a id="menu_{{val1.key}}" class="level2-ahref menu-{{val1.key}} {{val1.classtitle}}" \r\n                                    data-key="{{val1.key}}" data-path="{{val.key}}"\r\n                                    title="{{val1.title}}" href="{{val1.href}}" \r\n                                    target="{{val1.target||\'_self\'}}">\r\n                                    {{if val1.next && val1.next.length>0}}\r\n                                    <span class="leftmenu-right-arrow2 icon"></span>\r\n                                    {{/if}}\r\n                                    <span class="text">{{val1.title}}</span>\r\n                                    {{if val1.hot}}<span class="icon-hot"></span>{{/if}}\r\n                                </a>\r\n                                {{if val1.next && val1.next.length>0}}\r\n                                <div class="level3-wrapper menu-{{val1.key}}-wrapper" data-count="{{val1.next.length}}">\r\n                                    <div class="level3-list">\r\n                                        {{each val1.next val2 idx2}}\r\n                                        {{if idx2%25===0}}<ul class="ul-col">{{/if}}\r\n                                            <li class="level3-wrapper-li menu-{{val2.key}}-wrapper" style="{{val2.show===false?\'display:none\':\'\'}}">\r\n                                                <span class="letter">{{val2.groupKey}}</span>\r\n                                                <a id="menu_{{val2.key}}" href="{{val2.href}}" class="{{val2.className}} menu-{{val2.key}}" \r\n                                                    data-key="{{val2.key}}" data-path="{{val.key}};{{val1.key}}"\r\n                                                    title="{{val2.title}}" target="{{val2.target||\'_self\'}}">\r\n                                                    <span class="text">{{val2.title}}</span>\r\n                                                    {{if val2.hot}}<span class="icon-hot"></span>{{/if}}\r\n                                                </a>\r\n                                            </li>\r\n                                        {{if idx2%25===24}}</ul>{{/if}}\r\n                                        {{/each}}\r\n                                    </div>\r\n                                </div>\r\n                                {{/if}}\r\n                            </li>\r\n                            {{/each}}\r\n                        </ul>\r\n                    </div>\r\n                </div>\r\n                {{/if}}\r\n            </li>\r\n            {{/each}}\r\n        </ul>\r\n    </div>\r\n</div>'},
        "./ClientApp/components/quote-sidemenu/sidemenu.css": function(e, t) {},
        "./ClientApp/components/quote-sidemenu/sidemenu.js": function(e, t, n) {
            function r(e, t) {
                var n, r = i(e),
                    c = i.extend(new
                        function() {
                            this.current = location.hash.replace("#", "") || "",
                                this.ajax = !1,
                                this.crumbs = new o,
                                this.onloaded = null
                        },
                        t),
                    u = this;
                this.dom = r[0],
                    this.loaded = !1,
                    this.load = function() {
                        if (0 === r.length) return ! 1;
                        if (r.unbind("menu.draw"), c.ajax) {
                            "jsonp" === c.dataType && (c.ajax.jsonpCallback = "menuLoader");
                            c.ajax.success = function(e) {
                                if (n = e, u.draw(e), "function" == typeof c.onloaded) {
                                    var a = [r[0], e],
                                        o = i("#menu_" + c.current, r);
                                    o.length && a.push(o.data()),
                                        c.onloaded.apply(t, a)
                                }
                                u.loaded = !0
                            };
                            i.ajax(c.ajax)
                        } else if (c.data) {
                            if (n = c.data, this.draw(n), "function" == typeof c.onloaded) {
                                var e = [r[0], n],
                                    a = i("#menu_" + c.current, r);
                                a.length && e.push(a.data()),
                                    c.onloaded.apply(t, e)
                            }
                            this.loaded = !0
                        }
                        return this
                    },
                    this.draw = function(e, t) {
                        e = e || n,
                            c.current = t || location.hash.replace("#", "") || c.current,
                        e instanceof Array && (function(e, t) {
                            function n(e) {
                                if (e instanceof Array && e.length > 0) {
                                    e.sort(function(e, t) {
                                        return e.order - t.order
                                    });
                                    for (var t = 0; t < e.length; t++) {
                                        var r = e[t];
                                        r && n(r.next)
                                    }
                                }
                            }
                            var r = s.compile(l);
                            n(t);
                            var a = r({
                                menu: t
                            });
                            i(e).addClass("side-menu").html(a)
                        } (r, e),
                            function(e, t, n) {
                                function r(e, t) {
                                    var n = [],
                                        a = e[t];
                                    return a && (n.push(a), a.parentKey && Array.prototype.unshift.apply(n, r(e, a.parentKey))),
                                        n
                                }
                                var l = i.extend(new o, e),
                                    c = i(l.container);
                                if (c.length <= 0 || !(t instanceof Array) || !n) return ! 1;
                                var u = {
                                    list: r(a(t), n)
                                };
                                try {
                                    var d = s.render(l.template, u);
                                    c.html(d)
                                } catch(h) {
                                    console.error(h)
                                }
                            } (c.crumbs, e, c.current),
                            function(e, t) {
                                var n = i(e),
                                    r = {
                                        includeMargin: !0
                                    };
                                if (t) {
                                    var a = i("#menu_" + t, n).addClass("active"),
                                        o = a.data("path");
                                    o && i.each(o.split(";"),
                                        function(e, t) {
                                            i("#menu_" + t, n).addClass("active")
                                        })
                                }
                                i(".level3-wrapper", n).css("width",
                                    function(e, t) {
                                        var n = i(this),
                                            a = n.data("count"),
                                            o = i(".level3-wrapper-li", n).actual("outerWidth", r),
                                            s = n.actual("outerWidth", r);
                                        return a > 25 ? parseInt(a / 25 + (a % 25 == 0 ? 0 : 1)) * o: s
                                    }),
                                    i(".level1-wrapper-li", n).on("mouseenter",
                                        function(e) {
                                            i(".level1-wrapper-li", n);
                                            var t = i(".level2-wrapper", n),
                                                a = i(this).find(".level2-wrapper");
                                            if (i(this).find(".level1-icon").addClass("hover"), i(this).find(".level1-ahref").addClass("hover"), t.hide(), a.length > 0) {
                                                a.show();
                                                var o = a.offset(),
                                                    s = a.actual("outerHeight", r),
                                                    l = i(window).height() + i(window).scrollTop() - (o.top + s + 25);
                                                a.css("top", l < 0 ? l: "")
                                            }
                                        }),
                                    i(".level1-wrapper-li", n).on("mouseleave",
                                        function() {
                                            var e = i(".level2-wrapper", n);
                                            e.hide().css("top", ""),
                                                i(this).find(".level1-icon").removeClass("hover"),
                                                i(this).find(".level1-ahref").removeClass("hover")
                                        }),
                                    i(".level2-wrapper-li", n).on("mouseenter",
                                        function(e) {
                                            i(".level2-wrapper-li", n);
                                            var t = i(".level3-wrapper", n),
                                                a = i(this).find(".level3-wrapper");
                                            if (t.hide(), i(this).find(".level2-ahref").addClass("hover"), a.length > 0) {
                                                a.show();
                                                var o = a.offset(),
                                                    s = a.actual("outerHeight", r),
                                                    l = i(window).height() + i(window).scrollTop() - (o.top + s + 25);
                                                a.css("top", l < 0 ? l: "")
                                            }
                                        }),
                                    i(".level2-wrapper-li", n).on("mouseleave",
                                        function(e) {
                                            var t = i(".level3-wrapper", n);
                                            t.hide().css("top", ""),
                                                i(this).find(".level2-ahref").removeClass("hover")
                                        }),
                                    i(".level3-wrapper-li", n).on("mouseenter",
                                        function(e) {
                                            i(this).find("a").addClass("hover")
                                        }),
                                    i(".level3-wrapper-li", n).on("mouseleave",
                                        function(e) {
                                            i(this).find("a").removeClass("hover")
                                        })
                            } (r, c.current));
                        var u = i("#menu_" + c.current, r);
                        return r.trigger("menu.draw", u.length > 0 ? [e, u.data()] : e),
                            this
                    }
            }
            function a(e, t) {
                var n = {};
                if (e instanceof Array && e.length > 0) for (var r = 0; r < e.length; r++) {
                    var o = e[r]; (o || o.key) && (n[o.key] = o, t && (o.parentKey = t), o.next instanceof Array && i.extend(n, a(o.next, o.key)))
                }
                return n
            }
            function o() {
                this.container = document.getElementById("crumbs"),
                    this.template = '<a href="http://www.eastmoney.com" target="_blank">东方财富网</a> &gt; <a href="/center" target="_self">行情中心</a>{{each list}}{{if $value.href && $index!==list.length-1}} &gt; <a target="{{$value.target}}" href="{{$value.href}}">{{$value.title}}</a>{{else}} &gt; {{$value.title}}{{/if}}{{/each}}'
            }
            var i = n("jquery"),
                s = (n("./ClientApp/modules/jquery-plugins/jquery-actual.js"), n("./ClientApp/modules/template-web.js")),
                l = n("./ClientApp/components/quote-sidemenu/sidemenu.art");
            n("./ClientApp/components/quote-sidemenu/sidemenu.css"),
                e.exports = r,
                r.getmap = a
        },
        "./ClientApp/components/quote-sidetoolbox/toolbox.art": function(e, t) {e.exports = '<div class="tool-box">\r\n    <a id="tool-box-feedback" class="feedback" href="http://corp.eastmoney.com/liuyan.html" target="_blank">\r\n        <span>意见反馈</span>\r\n        <div class="feedback-left" style="display:none"></div>\r\n        <div class="feedback-right" style="display:none"></div>\r\n    </a>\r\n    <a id="tool-box-sreach" class="stock-search">\r\n        <div class="search-left" style="display:none">行情</div>\r\n        <div class="search-right" style="display:none">搜索</div>\r\n    </a>\r\n    <a id="tool-box-gotop" class="gotop icon-gotop" style="display:none"></a>\r\n</div>'},
        "./ClientApp/components/quote-sidetoolbox/toolbox.css": function(e, t) {},
        "./ClientApp/components/quote-sidetoolbox/toolbox.js": function(e, t, n) {
            function r(e) {
                var t = a.extend(!0, {},
                    r.defaults, e);
                this.load = function() {
                    var e = a(t.template);
                    return a("body").append(e),
                        this.bindevents(e),
                        this
                },
                    this.bindevents = function(e) {
                        return function(e) {
                            a("#tool-box-feedback", e).hover(function(e) {
                                    var t = this;
                                    a("span", t).hide(),
                                        a("div", t).show(),
                                        a(".feedback-left", t).animate({
                                                left: 12
                                            },
                                            {
                                                duration: 250,
                                                complete: function() {
                                                    a(".feedback-right", t).animate({
                                                            left: 24
                                                        },
                                                        250)
                                                }
                                            })
                                },
                                function(e) {
                                    a("span", this).show(),
                                        a("div", this).hide(),
                                        a(".feedback-left", this).css("left", -36),
                                        a(".feedback-right", this).css("left", 50)
                                }),
                            t.feedback.show || a("#tool-box-feedback", e).hide()
                        } (e),
                            function(e) {
                                a("#tool-box-sreach", e).hover(function() {
                                        a(this).removeClass("icon-fdj"),
                                            a("div", this).show(),
                                            a(".search-left", this).animate({
                                                left: 11
                                            }),
                                            a(".search-right", this).animate({
                                                left: 11
                                            })
                                    },
                                    function() {
                                        a("div", this).hide(),
                                            a(".search-left", this).css("left", -33),
                                            a(".search-right", this).css("left", 50),
                                            a(this).addClass("icon-fdj")
                                    }),
                                t.search.show || a("#tool-box-sreach", e).hide()
                            } (e),
                            function(e) {
                                var n = t.gotop.show;
                                if ("none" === n) a("#tool-box-gotop").hide();
                                else if ("always" === n) a("#tool-box-gotop").show();
                                else {
                                    var r = "while" === n && t.gotop.scrollTop ? t.gotop.scrollTop: a(window).height();
                                    a(window).resize(function(e) {
                                        r = a(window).height()
                                    }),
                                        a(window).scroll(o(function(e) {
                                                a(document).scrollTop() >= r ? a("#tool-box-gotop").show() : a("#tool-box-gotop").hide()
                                            },
                                            500))
                                }
                                a("#tool-box-gotop", e).hover(function(e) {
                                        a(this).removeClass("icon-gotop"),
                                            a(this).text("回到顶部")
                                    },
                                    function(e) {
                                        a(this).addClass("icon-gotop"),
                                            a(this).text("")
                                    }),
                                    a("#tool-box-gotop", e).click(function(e) {
                                        return a(document).scrollTop(0),
                                            !1
                                    })
                            } (e),
                            this
                    },
                    this.load()
            }
            n("./ClientApp/components/quote-sidetoolbox/toolbox.css");
            var a = n("jquery"),
                o = n("./node_modules/lodash.throttle/index.js"),
                i = n("./ClientApp/components/quote-sidetoolbox/toolbox.art");
            e.exports = r,
                r.defaults = {
                    template: i,
                    feedback: {
                        show: !0
                    },
                    search: {
                        show: !1
                    },
                    gotop: {
                        show: "nextscreen",
                        scrollTop: 0
                    }
                }
        },
        "./ClientApp/config recursive gridlist\\.fields\\.json$": function(e, t, n) {
            function r(e) {
                return n(a(e))
            }
            function a(e) {
                var t = o[e];
                if (! (t + 1)) throw new Error("Cannot find module '" + e + "'.");
                return t
            }
            var o = {
                "./gridlist.fields.json": "./ClientApp/config/gridlist.fields.json",
                "./tsq.gridlist.fields.json": "./ClientApp/config/tsq.gridlist.fields.json"
            };
            r.keys = function() {
                return Object.keys(o)
            },
                r.resolve = a,
                e.exports = r,
                r.id = "./ClientApp/config recursive gridlist\\.fields\\.json$"
        },
        "./ClientApp/config/adapter.json": function(e, t) {
            e.exports = {
                "/center/paihang.html#paihang_0_0": "/center/gridlist.html#hs_a_board",
                "/center/list.html#10_0_0_u": "/center/gridlist.html?st=ChangePercent#sh_a_board",
                "/center/list.html#10_0_0_d": "/center/gridlist.html?st=ChangePercent&sr=1#sh_a_board",
                "/center/list.html#20_0_0_u": "/center/gridlist.html?st=ChangePercent#sz_a_board",
                "/center/list.html#20_0_0_d": "/center/gridlist.html?st=ChangePercent&sr=1#sz_a_board",
                "/center/list.html#11_0_0_u": "/center/gridlist.html?st=ChangePercent#b_board",
                "/center/list.html#11_0_0_d": "/center/gridlist.html?st=ChangePercent&sr=1#b_board",
                "/center/list.html#21_0_0_u": "/center/gridlist.html?st=ChangePercent#b_board",
                "/center/list.html#21_0_0_d": "/center/gridlist.html?st=ChangePercent&sr=1#b_board",
                "/center/bklist.html#notion_0_0": "/center/boardlist.html?st=ChangePercent#concept_board",
                "/center/bklist.html#area_0_0": "/center/boardlist.html?st=ChangePercent#region_board",
                "/center/bklist.html#trade_0_0": "/center/boardlist.html?st=ChangePercent#industry_board",
                "/center/list.html#33": "/center/gridlist.html#hs_a_board",
                "/center/list.html#10": "/center/gridlist.html#sh_a_board",
                "/center/list.html#11": "/center/gridlist.html#b_board",
                "/center/list.html#20": "/center/gridlist.html#sz_a_board",
                "/center/list.html#21": "/center/gridlist.html#b_board",
                "/center/list.html#absh_0_4": "/center/gridlist.html#ab_comparison_sh",
                "/center/list.html#absz_0_4": "/center/gridlist.html#ab_comparison_sz",
                "/center/index.html#zyzs_0_1": "/centerv2/hszs",
                "/center/list.html#15_0_1": "/center/gridlist.html#index_sh",
                "/center/list.html#25_0_1": "/center/gridlist.html#index_sz",
                "/center/list.html#35_0_1": "/center/gridlist.html#index_components",
                "/center/bankuai.html#_0_2": "/centerv2/hsbk",
                "/center/list.html#27": "/center/gridlist.html#gem_board",
                "/center/list.html#26": "/center/gridlist.html#sme_board",
                "/center/list.html#285001_0": "/center/gridlist.html#newshares",
                "/center/list.html#28003707_12_2": "/center/gridlist.html#sh_hk_board",
                "/center/list.html#28013804_12_3": "/center/gridlist.html#sz_hk_board",
                "/center/list.html#2850022_0": "/center/gridlist.html#st_board",
                "/center/list.html#40_0_3": "/gridlist.html#staq_net_board",
                "/center/list.html#43_9": "/center/gridlist.html#neeq_stocks",
                "/center/list.html#44_9": "/center/gridlist.html#neeq_innovate",
                "/center/list.html#45_9": "/center/gridlist.html#neeq_basic",
                "/center/list.html#46_9": "/center/gridlist.html#neeq_agreement",
                "/center/list.html#47_9": "/center/gridlist.html#neeq_marketmaking",
                "/center/list.html#48_9": "/center/gridlist.html#neeq_bidding",
                "/center/qiquan.html#1,1,0_13_0": "/center/gridlist.html#options_sh50etf_all",
                "/center/qiquan.html#1,1,0_13_1": "/center/gridlist.html#options_sh50etf_call",
                "/center/qiquan.html#1,1,0_13_2": "/center/gridlist.html#options_sh50etf_put",
                "/center/qiquan.html#1,510050,0_13_0": "/center/gridlist.html#options_sh50etf_all",
                "/center/qiquan.html#1,510050,0_13_1": "/center/gridlist.html#options_sh50etf_call",
                "/center/qiquan.html#1,510050,0_13_2": "/center/gridlist.html#options_sh50etf_put",
                "/center/dssqiquan.html#hk_13_2": "/center/gridlist.html#options_beanpulp_all",
                "/center/zssqiquan.html#hk_13_3": "/center/gridlist.html#options_sugar_all",
                "/center/hkqiquan.html#hk_13_1": "/center/gridlist.html#options_uscny_all",
                "/center/hgtstock.html#_12": "/centerv2/hsgt",
                "/center/hgtstock.html#12": "/centerv2/hsgt",
                "/center/list.html#mk0144_12": "/center/gridlist.html#hk_sh_stocks",
                "/center/list.html#mk0146_12": "/center/gridlist.html#hk_sz_stocks",
                "/center/list.html#ah_12": "/center/gridlist.html#ah_comparison",
                "/center/hkstock.html#_1": "/centerv2/ggsc",
                "/center/list.html#50_1": "/center/gridlist.html#hk_stocks",
                "/center/list.html#28hsci_1": "/center/gridlist.html#hk_bluechips",
                "/center/list.html#28hscci_1": "/center/gridlist.html#hk_redchips",
                "/center/list.html#28hsciindex_1": "/center/gridlist.html#hk_redchips_components",
                "/center/list.html#28hscei_1": "/center/gridlist.html#hk_stateowned",
                "/center/list.html#28hsceiindex_1": "/center/gridlist.html#hk_stateowned_components",
                "/center/list.html#28gem_1": "/center/gridlist.html#hk_gem",
                "/center/list.html#mk0141_1": "/center/gridlist.html#hsi_large_components",
                "/center/list.html#mk0142_1": "/center/gridlist.html#hsi_medium_components",
                "/center/list.html#mk0144_1": "/center/gridlist.html#hk_components",
                "/center/list.html#52_1": "/center/gridlist.html#hk_warrants",
                "/center/list.html#32_1": "/center/gridlist.html#hk_index",
                "/center/list.html#ah_1": "/center/gridlist.html#ah_comparison",
                "/center/adrlist.html#adr_1": "/center/gridlist.html#hk_adr",
                "/center/list.html#zsqh_1": "/centerv2/qhsc/gjs/zsqh",
                "/center/list.html#yspgpqh_1": "/centerv2/qhsc/gjs/gpqh",
                "/center/list.html#hjqh_1": "/center/gridlist.html#hk_gold_futures",
                "/center/hkysp.html?code=hcf_cus#yspgpqh_1_0": "/centerv2/qhsc/gjs/HCF_CUS",
                "/center/hkysp.html?code=hcf_ucn#yspgpqh1_1_0": "/centerv2/qhsc/gjs/HCF_UCN",
                "/center/hkysp.html?code=hcf_cjp#yspgpqh2_1_0": "/centerv2/qhsc/gjs/HCF_CJP",
                "/center/hkysp.html?code=hcf_ceu#yspgpqh3_1_0": "/centerv2/qhsc/gjs/HCF_CEU",
                "/center/hkysp.html?code=hcf_cau#yspgpqh4_1_0": "/centerv2/qhsc/gjs/HCF_CAU",
                "/center/hkysp.html?code=hmfs_lrp#yspgpqh0_1_1": "/centerv2/qhsc/gjs/HMFS_LRP",
                "/center/hkysp.html?code=hmfs_lrc#yspgpqh1_1_1": "/centerv2/qhsc/gjs/HMFS_LRC",
                "/center/hkysp.html?code=hmfs_lrs#yspgpqh2_1_1": "/centerv2/qhsc/gjs/HMFS_LRS",
                "/center/hkysp.html?code=hmfs_lra#yspgpqh3_1_1": "/centerv2/qhsc/gjs/HMFS_LRA",
                "/center/hkysp.html?code=hmfs_lrz#yspgpqh4_1_1": "/centerv2/qhsc/gjs/HMFS_LRZ",
                "/center/hkysp.html?code=hmfs_lrn#yspgpqh5_1_1": "/centerv2/qhsc/gjs/HMFS_LRN",
                "/center/hkysp.html?code=hmfs_fem#yspgpqh6_1_1": "/centerv2/qhsc/gjs/HMFS_FEM",
                "/center/hkysp.html?code=hmfs_feq#yspgpqh7_1_1": "/centerv2/qhsc/gjs/HMFS_FEQ",
                "/center/usstock.html#_2": "/centerv2/mgsc",
                "/center/list.html#70_2": "/center/gridlist.html#us_stocks",
                "/center/list.html#28mk0001_2": "/center/gridlist.html#us_wellknown",
                "/center/list.html#28china_2": "/center/gridlist.html#us_chinese",
                "/center/list.html#28chinainternet_2": "/center/gridlist.html#us_chinese_internet",
                "/center/list.html#286_2": "/center/gridlist.html#us_technology",
                "/center/list.html#283_2": "/center/gridlist.html#us_financial",
                "/center/list.html#287_2": "/center/gridlist.html#us_medicine_food",
                "/center/list.html#284_2": "/center/gridlist.html#us_media",
                "/center/list.html#281_2": "/center/gridlist.html#us_automotive_energy",
                "/center/list.html#285_2": "/center/gridlist.html#us_manufacture_retail",
                "/center/global.html#global_3": "/centerv2/qqzs",
                "/center/asia.html#asia_3": "/center/gridlist.html#global_asia",
                "/center/australia.html#australia_3": "/center/gridlist.html#global_australia",
                "/center/america.html#america_3": "/center/gridlist.html#global_america",
                "/center/europe.html#europe_3": "/center/gridlist.html#global_euro",
                "/center/fund.html#_4": "/centerv2/jjsc",
                "/center/list.html#285002_4": "/center/gridlist.html#fund_close_end",
                "/center/list.html#2850013_4": "/center/gridlist.html#fund_etf",
                "/center/list.html#2850014_4": "/center/gridlist.html#fund_lof",
                "/center/fundlist.html#0,0_4_0": "http://fund.eastmoney.com/data/fundranking.html",
                "/center/fundlist.html#0,1_4_0": "http://fund.eastmoney.com/data/fundranking.html#tgp",
                "/center/fundlist.html#0,2_4_0": "http://fund.eastmoney.com/data/fundranking.html#thh",
                "/center/fundlist.html#0,3_4_0": "http://fund.eastmoney.com/data/fundranking.html#tzq",
                "/center/fundlist.html#0,4_4_0": "http://fund.eastmoney.com/data/fundranking.html#tlof",
                "/center/fundlist.html#0,5_4_0": "http://fund.eastmoney.com/data/fundranking.html#tzs",
                "/center/fundlist.html#0,6_4_0": "http://fund.eastmoney.com/data/fundranking.html#tqdii",
                "/center/fundlist.html#2,2_4_1": "http://fund.eastmoney.com/CXXFBS_bzdm.html",
                "/center/fundlist.html#1,0_4": "http://fund.eastmoney.com/HBJJ_dwsy.html",
                "/center/fundlist.html#3,_4": "http://fund.eastmoney.com/fund.html",
                "/center/futures.html#_5": "/centerv2/qhsc",
                "/center/futurelist.html#ine_5_5": "/center/gridlist.html#futures_ine",
                "/center/futurelist.html#ine,sc_5_5": "/center/gridlist.html#futures_ine-F_INE_SC",
                "/center/futurelist.html#11_5_0": "/center/gridlist.html#futures_shfe",
                "/center/futurelist.html#1,0,1_5_0": "/center/gridlist.html#futures_shfe-F_SHFE_RB",
                "/center/futurelist.html#1,1,1_5_0": "/center/gridlist.html#futures_shfe-F_SHFE_WR",
                "/center/futurelist.html#1,2,1_5_0": "/center/gridlist.html#futures_shfe-F_SHFE_CU",
                "/center/futurelist.html#1,3,1_5_0": "/center/gridlist.html#futures_shfe-F_SHFE_AL",
                "/center/futurelist.html#1,4,1_5_0": "/center/gridlist.html#futures_shfe-F_SHFE_RU",
                "/center/futurelist.html#1,5,1_5_0": "/center/gridlist.html#futures_shfe-F_SHFE_FU",
                "/center/futurelist.html#1,6,1_5_0": "/center/gridlist.html#futures_shfe-F_SHFE_ZN",
                "/center/futurelist.html#1,7,1_5_0": "/center/gridlist.html#futures_shfe-F_SHFE_AU",
                "/center/futurelist.html#1,8,1_5_0": "/center/gridlist.html#futures_shfe-F_SHFE_PB",
                "/center/futurelist.html#1,9,1_5_0": "/center/gridlist.html#futures_shfe-F_SHFE_AG",
                "/center/futurelist.html#1,10,1_5_0": "/center/gridlist.html#futures_shfe-F_SHFE_BU",
                "/center/futurelist.html#1,11,1_5_0": "/center/gridlist.html#futures_shfe-F_SHFE_HC",
                "/center/futurelist.html#1,12,1_5_0": "/center/gridlist.html#futures_shfe-F_SHFE_NI",
                "/center/futurelist.html#1,13,1_5_0": "/center/gridlist.html#futures_shfe-F_SHFE_SN",
                "/center/futurelist.html#3_5_1": "/center/gridlist.html#futures_dce",
                "/center/futurelist.html#3,0,2_5_1": "/center/gridlist.html#futures_dce-F_DCE_M",
                "/center/futurelist.html#3,1,2_5_1": "/center/gridlist.html#futures_dce-F_DCE_A",
                "/center/futurelist.html#3,2,2_5_1": "/center/gridlist.html#futures_dce-F_DCE_C",
                "/center/futurelist.html#3,3,2_5_1": "/center/gridlist.html#futures_dce-F_DCE_B",
                "/center/futurelist.html#3,4,2_5_1": "/center/gridlist.html#futures_dce-F_DCE_Y",
                "/center/futurelist.html#3,5,2_5_1": "/center/gridlist.html#futures_dce-F_DCE_L",
                "/center/futurelist.html#3,6,2_5_1": "/center/gridlist.html#futures_dce-F_DCE_P",
                "/center/futurelist.html#3,7,2_5_1": "/center/gridlist.html#futures_dce-F_DCE_V",
                "/center/futurelist.html#3,8,2_5_1": "/center/gridlist.html#futures_dce-F_DCE_J",
                "/center/futurelist.html#3,9,2_5_1": "/center/gridlist.html#futures_dce-F_DCE_JM",
                "/center/futurelist.html#3,10,2_5_1": "/center/gridlist.html#futures_dce-F_DCE_I",
                "/center/futurelist.html#3,11,2_5_1": "/center/gridlist.html#futures_dce-F_DCE_JD",
                "/center/futurelist.html#3,12,2_5_1": "/center/gridlist.html#futures_dce-F_DCE_BB",
                "/center/futurelist.html#3,13,2_5_1": "/center/gridlist.html#futures_dce-F_DCE_FB",
                "/center/futurelist.html#3,14,2_5_1": "/center/gridlist.html#futures_dce-F_DCE_PP",
                "/center/futurelist.html#3,15,2_5_1": "/center/gridlist.html#futures_dce-F_DCE_CS",
                "/center/futurelist.html#4_5_2": "/center/gridlist.html#futures_czce",
                "/center/futurelist.html#4,0,3_5_2": "/center/gridlist.html#futures_czce-F_CZCE_WT",
                "/center/futurelist.html#4,1,3_5_2": "/center/gridlist.html#futures_czce-F_CZCE_WH",
                "/center/futurelist.html#4,2,3_5_2": "/center/gridlist.html#futures_global-UF_NYBOT_CT",
                "/center/futurelist.html#4,3,3_5_2": "/center/gridlist.html#futures_czce-F_CZCE_SR",
                "/center/futurelist.html#4,4,3_5_2": "/center/gridlist.html#futures_czce-F_CZCE_TA",
                "/center/futurelist.html#4,5,3_5_2": "/center/gridlist.html#futures_czce-F_CZCE_OI",
                "/center/futurelist.html#4,6,3_5_2": "/center/gridlist.html#futures_czce-F_CZCE_ER",
                "/center/futurelist.html#4,7,3_5_2": "/center/gridlist.html#futures_czce-F_CZCE_MA",
                "/center/futurelist.html#4,8,3_5_2": "/center/gridlist.html#futures_czce-F_CZCE_FG",
                "/center/futurelist.html#4,9,3_5_2": "/center/gridlist.html#futures_czce-F_CZCE_RM",
                "/center/futurelist.html#4,10,3_5_2": "/center/gridlist.html#futures_czce-F_CZCE_RS",
                "/center/futurelist.html#4,11,3_5_2": "/center/gridlist.html#futures_czce-F_CZCE_ZC",
                "/center/futurelist.html#4,12,3_5_2": "/center/gridlist.html#futures_czce-F_CZCE_JR",
                "/center/futurelist.html#4,13,3_5_2": "/center/gridlist.html#futures_czce-F_CZCE_LR",
                "/center/futurelist.html#4,14,3_5_2": "/center/gridlist.html#futures_czce-F_CZCE_SF",
                "/center/futurelist.html#4,15,3_5_2": "/center/gridlist.html#futures_czce-F_CZCE_SM",
                "/center/futurelist.html#4,16,3_5_2": "/center/gridlist.html#futures_czce-F_CZCE_CY",
                "/center/futurelist.html#4,17,3_5_2": "/center/gridlist.html#futures_czce-F_CZCE_AP",
                "/center/list.html#12.1.1_5_3": "/center/gridlist.html#futures_cffex",
                "/center/list.html#12_5_3": "/center/gridlist.html#futures_cffex-_IF_FO",
                "/center/list.html#12.1_5_3": "/center/gridlist.html#futures_cffex-_IH_FO",
                "/center/list.html#12.2_5_3": "/center/gridlist.html#futures_cffex-_IC_FO",
                "/center/list.html#13_5_2": "/center/gridlist.html#futures_cffex-_TF_FO",
                "/center/list.html#13_5_3": "/center/gridlist.html#futures_cffex-_TF_FO",
                "/center/list.html#14_5_3": "/center/gridlist.html#futures_cffex-_T_FO",
                "/center/list.html#gjqh_5": "/center/gridlist.html#futures_global",
                "/center/list.html#gjqh_5_4": "/center/gridlist.html#futures_global",
                "/center/list.html#lme_5_4": "/center/gridlist.html#futures_global-lme",
                "/center/list.html#ipe_5_4": "/center/gridlist.html#futures_global-ipe",
                "/center/list.html#cobot_5_4": "/center/gridlist.html#futures_global-cobot",
                "/center/list.html#nybot_5_4": "/center/gridlist.html#futures_global-nybot",
                "/center/list.html#nymex_5_4": "/center/gridlist.html#futures_global-nymex",
                "/center/list.html#tocom_5_4": "/center/gridlist.html#futures_global-tocom",
                "/center/forex.html#_6": "/centerv2/whsc",
                "/center/list.html#forex_6": "/center/gridlist.html#forex_all",
                "/center/list.html#1_6": "/center/gridlist.html#forex_basic",
                "/center/list.html#2_6": "/center/gridlist.html#forex_cross",
                "/center/list.html#rmbzjj_6": "/center/gridlist.html#forex_cny",
                "/center/list.html#rmbzjj_6_2": "/center/gridlist.html#forex_cnyc",
                "/center/list.html#rmbxj_6_2": "/center/gridlist.html#forex_cnyi",
                "/center/list.html#rmbjj_6_2": "/center/gridlist.html#forex_cnyb",
                "/center/list.html#28rmbrate_6": "/center/gridlist.html#forex_cnyi",
                "/center/list.html#28rmbrate_6_2": "/center/gridlist.html#forex_cnyi",
                "/center/list.html#rmbforex_6_2": "/center/gridlist.html#forex_cnh",
                "/center/gold.html#_7": "/centerv2/hjsc",
                "/center/glodlist.html#gjgjsqh_7": "/center/gridlist.html#nobalmetal_futures",
                "/center/futurelist.html#1,7,1_7": "/center/gridlist.html#gold_sh_futures",
                "/center/glodlist.html#gjgjsxh_7": "/center/gridlist.html#nobalmetal_spotgoods",
                "/center/glodlist.html#shhjxh_7": "/center/gridlist.html#gold_sh_spotgoods",
                "/center/bond.html#_8": "/centerv2/zqsc",
                "/center/list.html#bondindex_8": "/center/gridlist.html#bond_index",
                "/center/list.html#14.1.1_8_0": "/center/gridlist.html#bond_national_sh",
                "/center/list.html#14.2.1_8_0": "/center/gridlist.html#bond_enterprise_sh",
                "/center/list.html#14.3_8_0": "/center/gridlist.html#bond_convertible_sh",
                "/center/list.html#24.1_8_1": "/center/gridlist.html#bond_national_sz",
                "/center/list.html#24.2_8_1": "/center/gridlist.html#bond_enterprise_sz",
                "/center/list.html#24.3_8_1": "/center/gridlist.html#bond_convertible_sz",
                "/center/list.html#2850020_8": "/center/gridlist.html#bond_sh_buyback",
                "/center/list.html#2850021_8": "/center/gridlist.html#bond_sz_buyback",
                "/center/fullsrceenlist.html#bp_8": "/center/fullscreenlist.html#convertible_comparison",
                "/center/list.html#btb_10": "/center/gridlist.html#virtual_money_all"
            }
        },
        "./ClientApp/config/borads.gridlist.cfg.json": function(e, t) {
            e.exports = {
                concept_board: {
                    _comment: "概念板块排行",
                    hotlinks: {
                        show: !0
                    },
                    digest: {
                        container: "#digest_box"
                    },
                    gridtable: {
                        config: {
                            params: "?cmd=C._BKGN&sty=FPGBKI",
                            fields: "Borads"
                        },
                        "config-pic": {
                            params: "?cmd=C._BKGN&sty=ESBFDTC"
                        }
                    }
                },
                industry_board: {
                    _comment: "行业板块排行",
                    hotlinks: {
                        show: !0
                    },
                    digest: {
                        container: "#digest_box"
                    },
                    gridtable: {
                        config: {
                            params: "?cmd=C._BKHY&sty=FPGBKI",
                            fields: "Borads"
                        },
                        "config-pic": {
                            params: "?cmd=C._BKHY&sty=ESBFDTC"
                        }
                    }
                },
                region_board: {
                    _comment: "地域板块排行",
                    hotlinks: {
                        show: !0
                    },
                    digest: {
                        container: "#digest_box"
                    },
                    gridtable: {
                        config: {
                            params: "?cmd=C._BKDY&sty=FPGBKI",
                            fields: "Borads"
                        },
                        "config-pic": {
                            params: "?cmd=C._BKDY&sty=ESBFDTC"
                        }
                    }
                },
                boards: {
                    _comment: "板块详情",
                    hotlinks: {
                        show: !0
                    },
                    digest: {
                        container: "#digest_box",
                        quotechart: {
                            type: "timemini",
                            options: {
                                container: "#quote_chart",
                                width: 324,
                                height: 170,
                                token: "4f1862fc3b5e77c150a2b985b12db0fd",
                                bigImg: {
                                    stauts: "show"
                                },
                                timeline: [{
                                    time: "11:30/13:00",
                                    position: .5
                                }],
                                update: 2e4
                            },
                            compatible: {
                                container: "#quote_chart",
                                url: "//pifm.eastmoney.com/EM_Finance2014PictureInterface/Index.aspx?imageType=FLP20RSI&token=4f1862fc3b5e77c150a2b985b12db0fd",
                                width: 304,
                                height: 150,
                                update: 2e4
                            }
                        },
                        capitalflowchart: {
                            type: "fundmini",
                            options: {
                                baseurl: "//ff.eastmoney.com/EM_CapitalFlowInterface/api/js?type=ff&check=MLBMS&js=({(x)})&rtntype=3&acces_token=4f1862fc3b5e77c150a2b985b12db0fd",
                                container: "#capitalflow_chart",
                                width: 324,
                                height: 170,
                                bigImg: {
                                    stauts: "show"
                                },
                                update: 2e4
                            },
                            compatible: {
                                container: "#capitalflow_chart",
                                url: "//pifm.eastmoney.com/EM_Finance2014PictureInterface/Index.aspx?imageType=FFFLP20R&type=FFR&token=4f1862fc3b5e77c150a2b985b12db0fd",
                                width: 304,
                                height: 150,
                                update: 2e4
                            }
                        },
                        "capitalflow-rank": {
                            container: "#capitalflow_rank",
                            type: "simple",
                            params: "?sty=DCFFPBFMS&st=(BalFlowMain)&sr=-1",
                            count: 5,
                            mappings: "csv:0=MarketType&1=Code&2=Name&3=BalFlowMainSub",
                            fields: [{
                                name: "number",
                                title: "排名",
                                data: "$num"
                            },
                                {
                                    name: "Name",
                                    title: "名称",
                                    template: "<a href='/{{marketMapping[MarketType]}}{{Code}}.html'>{{Name}}</a>"
                                },
                                {
                                    name: "Links",
                                    title: "相关链接",
                                    template: "<a href='//guba.eastmoney.com/list,{{Code}}.html'>股吧</a><a href='//data.eastmoney.com/zjlx/{{Code}}.html'>资金流</a><a href='//data.eastmoney.com/stockdata/{{Code}}.html'>数据</a>"
                                },
                                {
                                    data: "BalFlowMainSub",
                                    title: "主力净流入",
                                    color: !0
                                }],
                            update: 2e4
                        }
                    },
                    gridtable: {
                        config: {
                            params: "?sty=FCOIATC",
                            fields: "ABStocks"
                        },
                        "config-pic": {
                            params: "?sty=ESBFDTC"
                        }
                    }
                }
            }
        },
        "./ClientApp/config/gridlist.cfg.json": function(e, t) {
            e.exports = {
                "default": {
                    pageRenderer: {
                        disabled: !0,
                        baseurl: "/js",
                        template: "{{key}}.render.js"
                    },
                    toolbar: {
                        show: !0,
                        container: "#tools",
                        tools: ["refresh"]
                    },
                    navbar: {
                        show: !1,
                        division: "#division",
                        container: "#tab",
                        keys: [],
                        template: "<ul class='tab-list clearfix'>{{each menu}}<li id='nav_{{$index}}' class='fl {{cur===$index?\"cur\":null}}'><a target='{{$value.target}}' href='{{$value.href}}'>{{$value.name||$value.title}}</a></li>{{/each}}</ul>"
                    },
                    hotlinks: {
                        show: !1,
                        container: "#hot-links",
                        template: ""
                    },
                    customfields: {
                        show: !1,
                        wrapper: "#custom-fields-wrapper",
                        container: "#custom-fields",
                        template: "{{each fields}}<option value=\"{{$value[0]}}\" {{$value[2]?'selected':''}}>{{$value[1]}}</option>{{/each}}",
                        fields: [["PB", "市净率"], ["MarketValue", "总市值"], ["FlowCapitalValue", "流通市值"], ["ChangePercent60Day", "60日涨跌幅"], ["ChangePercent360Day", "年初至今涨跌幅"], ["Speed", "涨速"], ["FiveMinuteChangePercent", "五分钟涨跌"]]
                    },
                    gridtable: {
                        container: "#main-table",
                        config: {
                            params: "",
                            count: 20,
                            mappingsKey: "{{sty}}",
                            fields: "{{key}}",
                            update: 15e3
                        },
                        "config-pic": {
                            mappingsKey: "{{sty}}"
                        }
                    }
                }
            }
        },
        "./ClientApp/config/gridlist.fields.json": function(e, t) {e.exports = {
                ABStocks: [
                    {
                        name: "number",
                        data: "$num"
                    },
                    {
                        name: "Code",
                        title: "代码",
                        template: "<a href='/{{marketMapping[MarketType]}}{{Code}}.html'>{{Code}}</a>"
                    },
                    {
                        name: "Name",
                        title: "名称",
                        template: "<a href='/{{marketMapping[MarketType]}}{{Code}}.html'>{{Name}}</a>"
                    },
                    {
                        name: "Links",
                        title: "相关链接",
                        template: "<a href='http://guba.eastmoney.com/list,{{Code}}.html'>股吧</a><a href='http://data.eastmoney.com/zjlx/{{Code}}.html'>资金流</a><a href='http://data.eastmoney.com/stockdata/{{Code}}.html'>数据</a>",
                        orderable: !1
                    },
                    {
                        data: "Close",
                        title: "最新价",
                        color: "{{Change}}"
                    },
                    {
                        data: "ChangePercent",
                        title: "涨跌幅",
                        template: "{{ChangePercent | percentRender}}",
                        color: !0,
                        ordering: "desc"
                    },
                    {
                        data: "Change",
                        title: "涨跌额",
                        color: !0
                    },
                    {
                        data: "Volume",
                        title: "成交量(手)",
                        template: "{{Volume | numbericFormat}}"
                    },
                    {
                        data: "Amount",
                        title: "成交额",
                        template: "{{Amount | numbericFormat}}"
                    },
                    {
                        data: "Amplitude",
                        title: "振幅",
                        template: "{{Amplitude | percentRender}}"
                    },
                    {
                        data: "High",
                        title: "最高",
                        color: "{{High - PreviousClose}}"
                    },
                    {
                        data: "Low",
                        title: "最低",
                        color: "{{Low - PreviousClose}}"
                    },
                    {
                        data: "Open",
                        title: "今开",
                        color: "{{Open - PreviousClose}}"
                    },
                    {
                        data: "PreviousClose",
                        title: "昨收"
                    },
                    {
                        title: "量比",
                        data: "VolumeRate"
                    },
                    {
                        title: "换手率",
                        data: "TurnoverRate",
                        template: "{{TurnoverRate | percentRender}}"
                    },
                    {
                        title: "市盈率(动态)",
                        data: "PERation"
                    },
                    {
                        title: "市净率",
                        data: "PB",
                        visible: !1
                    },
                    {
                        title: "总市值",
                        name: "MarketValue",
                        template: "{{MarketValue | numbericFormat}}",
                        visible: !1
                    },
                    {
                        title: "流通市值",
                        name: "FlowCapitalValue",
                        template: "{{FlowCapitalValue | numbericFormat}}",
                        visible: !1
                    },
                    {
                        title: "60日涨跌幅",
                        name: "ChangePercent60Day",
                        template: "{{ChangePercent60Day | percentRender}}",
                        color: !0,
                        visible: !1
                    },
                    {
                        title: "年初至今涨跌幅",
                        name: "ChangePercent360Day",
                        template: "{{ChangePercent360Day | percentRender}}",
                        color: !0,
                        visible: !1
                    },
                    {
                        title: "涨速",
                        name: "Speed",
                        template: "{{Speed | percentRender}}",
                        color: !0,
                        visible: !1
                    },
                    {
                        title: "五分钟涨跌",
                        name: "FiveMinuteChangePercent",
                        template: "{{FiveMinuteChangePercent | percentRender}}",
                        color: !0,
                        visible: !1
                    },
                    {
                        data: "$favourite"
                    }],
                newshares: [
                    {
                        name: "number",
                        data: "$num"
                    },
                    {
                        name: "Code",
                        title: "代码",
                        template: "<a href='/{{marketMapping[MarketType]}}{{Code}}.html'>{{Code}}</a>"
                    },
                    {
                        name: "Name",
                        title: "名称",
                        template: "<a href='/{{marketMapping[MarketType]}}{{Code}}.html'>{{Name}}</a>"
                    },
                    {
                        name: "Links",
                        title: "相关链接",
                        template: "<a href='http://guba.eastmoney.com/list,{{Code}}.html'>股吧</a><a href='http://data.eastmoney.com/zjlx/{{Code}}.html'>资金流</a><a href='http://data.eastmoney.com/stockdata/{{Code}}.html'>数据</a>",
                        orderable: !1
                    },
                    {
                        data: "Close",
                        title: "最新价",
                        color: "{{Change}}"
                    },
                    {
                        data: "ChangePercent",
                        title: "涨跌幅",
                        template: "{{ChangePercent | percentRender}}",
                        color: !0
                    },
                    {
                        data: "Change",
                        title: "涨跌额",
                        color: !0
                    },
                    {
                        data: "Volume",
                        title: "成交量(手)",
                        template: "{{Volume | numbericFormat}}"
                    },
                    {
                        data: "Amount",
                        title: "成交额",
                        template: "{{Amount | numbericFormat}}"
                    },
                    {
                        data: "Amplitude",
                        title: "振幅",
                        template: "{{Amplitude | percentRender}}"
                    },
                    {
                        data: "High",
                        title: "最高",
                        color: "{{High - PreviousClose}}"
                    },
                    {
                        data: "Low",
                        title: "最低",
                        color: "{{Low - PreviousClose}}"
                    },
                    {
                        data: "Open",
                        title: "今开",
                        color: "{{Open - PreviousClose}}"
                    },
                    {
                        data: "PreviousClose",
                        title: "昨收"
                    },
                    {
                        title: "换手率",
                        data: "TurnoverRate",
                        template: "{{TurnoverRate | percentRender}}"
                    },
                    {
                        title: "市盈率(动态)",
                        data: "PERation"
                    },
                    {
                        title: "上市日期",
                        data: "ListingDate",
                        ordering: "desc"
                    },
                    {
                        title: "市净率",
                        data: "PB",
                        visible: !0
                    },
                    {
                        title: "总市值",
                        name: "MarketValue",
                        template: "{{MarketValue | numbericFormat}}",
                        visible: !1
                    },
                    {
                        title: "流通市值",
                        name: "FlowCapitalValue",
                        template: "{{FlowCapitalValue | numbericFormat}}",
                        visible: !1
                    },
                    {
                        title: "60日涨跌幅",
                        name: "ChangePercent60Day",
                        template: "{{ChangePercent60Day | percentRender}}",
                        color: !0,
                        visible: !1
                    },
                    {
                        title: "年初至今涨跌幅",
                        name: "ChangePercent360Day",
                        template: "{{ChangePercent360Day | percentRender}}",
                        color: !0,
                        visible: !1
                    },
                    {
                        title: "涨速",
                        name: "Speed",
                        template: "{{Speed | percentRender}}",
                        color: !0,
                        visible: !1
                    },
                    {
                        title: "五分钟涨跌",
                        name: "FiveMinuteChangePercent",
                        template: "{{FiveMinuteChangePercent | percentRender}}",
                        color: !0,
                        visible: !1
                    },
                    {
                        title: "量比",
                        name: "VolumeRate",
                        visible: !1
                    },
                    {
                        data: "$favourite"
                    }],
                CDRStocks: [
                    {
                        name: "number",
                        data: "$num"
                    },
                    {
                        name: "Code",
                        title: "代码",
                        template: "<a href='/{{marketMapping[MarketType]}}{{Code}}.html'>{{Code}}</a>"
                    },
                    {
                        name: "Name",
                        title: "名称",
                        template: "<a href='/{{marketMapping[MarketType]}}{{Code}}.html'>{{Name}}</a>"
                    },
                    {
                        name: "Links",
                        title: "相关链接",
                        template: "<a href='http://guba.eastmoney.com/list,{{Code}}.html'>股吧</a><a href='http://data.eastmoney.com/zjlx/{{Code}}.html'>资金流</a><a href='http://data.eastmoney.com/stockdata/{{Code}}.html'>数据</a>",
                        orderable: !1
                    },
                    {
                        data: "Close",
                        title: "最新价",
                        color: "{{Change}}"
                    },
                    {
                        data: "ChangePercent",
                        title: "涨跌幅",
                        template: "{{ChangePercent | percentRender}}",
                        color: !0,
                        ordering: "desc"
                    },
                    {
                        data: "Change",
                        title: "涨跌额",
                        color: !0
                    },
                    {
                        data: "Volume",
                        title: "成交量(手)",
                        template: "{{Volume | numbericFormat}}"
                    },
                    {
                        data: "Amount",
                        title: "成交额",
                        template: "{{Amount | numbericFormat}}"
                    },
                    {
                        data: "Amplitude",
                        title: "振幅",
                        template: "{{Amplitude | percentRender}}"
                    },
                    {
                        data: "High",
                        title: "最高",
                        color: "{{High - PreviousClose}}"
                    },
                    {
                        data: "Low",
                        title: "最低",
                        color: "{{Low - PreviousClose}}"
                    },
                    {
                        data: "Open",
                        title: "今开",
                        color: "{{Open - PreviousClose}}"
                    },
                    {
                        data: "PreviousClose",
                        title: "昨收"
                    },
                    {
                        data: "IsCDRProfit",
                        title: "是否盈利",
                        template: "{{$current === '1'?'盈利':'亏损'}}",
                        orderable: !1
                    },
                    {
                        data: "IsCDRVotable",
                        title: "是否有投票权",
                        template: "{{$current === '1'?'是':'否'}}",
                        orderable: !1
                    },
                    {
                        title: "量比",
                        data: "VolumeRate",
                        visible: !1
                    },
                    {
                        title: "换手率",
                        data: "TurnoverRate",
                        template: "{{TurnoverRate | percentRender}}",
                        visible: !1
                    },
                    {
                        title: "市盈率(动态)",
                        data: "PERation"
                    },
                    {
                        title: "市净率",
                        data: "PB",
                        visible: !1
                    },
                    {
                        title: "总市值",
                        name: "MarketValue",
                        template: "{{MarketValue | numbericFormat}}",
                        visible: !1
                    },
                    {
                        title: "流通市值",
                        name: "FlowCapitalValue",
                        template: "{{FlowCapitalValue | numbericFormat}}",
                        visible: !1
                    },
                    {
                        title: "60日涨跌幅",
                        name: "ChangePercent60Day",
                        template: "{{ChangePercent60Day | percentRender}}",
                        color: !0,
                        visible: !1
                    },
                    {
                        title: "年初至今涨跌幅",
                        name: "ChangePercent360Day",
                        template: "{{ChangePercent360Day | percentRender}}",
                        color: !0,
                        visible: !1
                    },
                    {
                        title: "涨速",
                        name: "Speed",
                        template: "{{Speed | percentRender}}",
                        color: !0,
                        visible: !1
                    },
                    {
                        title: "五分钟涨跌",
                        name: "FiveMinuteChangePercent",
                        template: "{{FiveMinuteChangePercent | percentRender}}",
                        color: !0,
                        visible: !1
                    },
                    {
                        data: "$favourite"
                    }],
                ABComparison_SH: [
                    {
                        name: "number",
                        data: "$num"
                    },
                    {
                        name: "Code",
                        title: "B股代码",
                        template: "<a href='/{{marketMapping[MarketType]}}{{Code}}.html'>{{Code}}</a>"
                    },
                    {
                        name: "Name",
                        title: "B股名称",
                        template: "<a href='/{{marketMapping[MarketType]}}{{Code}}.html'>{{Name}}</a>"
                    },
                    {
                        name: "Links",
                        title: "相关链接",
                        template: "<a href='http://guba.eastmoney.com/list,{{Code}}.html'>股吧</a><a href='http://data.eastmoney.com/zjlx/{{Code}}.html'>资金流</a><a href='http://data.eastmoney.com/stockdata/{{Code}}.html'>数据</a>",
                        orderable: !1
                    },
                    {
                        data: "Close",
                        title: "最新价（美元）",
                        color: "{{Change}}"
                    },
                    {
                        data: "ChangePercent",
                        title: "涨跌幅",
                        template: "{{ChangePercent | percentRender}}",
                        color: !0
                    },
                    {
                        data: "ABAHCode",
                        title: "A股代码",
                        template: "<a href='/{{marketMapping[ABAHMkt]}}{{ABAHCode}}.html'>{{ABAHCode}}</a>"
                    },
                    {
                        data: "ABAHName",
                        title: "A股名称",
                        template: "<a href='/{{marketMapping[ABAHMkt]}}{{ABAHCode}}.html'>{{ABAHName}}</a>"
                    },
                    {
                        data: "ABAHClose",
                        title: "最新价",
                        color: "{{ABAHCP}}"
                    },
                    {
                        data: "ABAHCP",
                        title: "涨跌幅",
                        template: "{{ABAHCP | percentRender}}",
                        color: !0
                    },
                    {
                        data: "AB/AH/USD",
                        title: "比价（A/B）",
                        ordering: "desc"
                    }],
                ABComparison_SZ: [
                    {
                        name: "number",
                        data: "$num"
                    },
                    {
                        name: "Code",
                        title: "B股代码",
                        template: "<a href='/{{marketMapping[MarketType]}}{{Code}}.html'>{{Code}}</a>"
                    },
                    {
                        name: "Name",
                        title: "B股名称",
                        template: "<a href='/{{marketMapping[MarketType]}}{{Code}}.html'>{{Name}}</a>"
                    },
                    {
                        name: "Links",
                        title: "相关链接",
                        template: "<a href='http://guba.eastmoney.com/list,{{Code}}.html'>股吧</a><a href='http://data.eastmoney.com/zjlx/{{Code}}.html'>资金流</a><a href='http://data.eastmoney.com/stockdata/{{Code}}.html'>数据</a>",
                        orderable: !1
                    },
                    {
                        data: "Close",
                        title: "最新价（港元）",
                        color: "{{Change}}"
                    },
                    {
                        data: "ChangePercent",
                        title: "涨跌幅",
                        template: "{{ChangePercent | percentRender}}",
                        color: !0
                    },
                    {
                        data: "ABAHCode",
                        title: "A股代码",
                        template: "<a href='/{{marketMapping[ABAHMkt]}}{{ABAHCode}}.html'>{{ABAHCode}}</a>"
                    },
                    {
                        data: "ABAHName",
                        title: "A股名称",
                        template: "<a href='/{{marketMapping[ABAHMkt]}}{{ABAHCode}}.html'>{{ABAHName}}</a>"
                    },
                    {
                        data: "ABAHClose",
                        title: "最新价",
                        color: "{{ABAHCP}}"
                    },
                    {
                        data: "ABAHCP",
                        title: "涨跌幅",
                        template: "{{ABAHCP | percentRender}}",
                        color: !0
                    },
                    {
                        data: "AB/AH/HKD",
                        title: "比价（A/B）",
                        ordering: "desc"
                    }],
                AHComparison: [
                    {
                        name: "number",
                        data: "$num"
                    },
                    {
                        name: "Code",
                        title: "H股代码",
                        template: "<a href='/hk/{{Code}}.html'>{{Code}}</a>"
                    },
                    {
                        name: "Name",
                        title: "H股名称",
                        template: "<a href='/hk/{{Code}}.html'>{{Name}}</a>"
                    },
                    {
                        data: "Close",
                        title: "最新价(HKD)",
                        color: "{{Change}}"
                    },
                    {
                        data: "ChangePercent",
                        title: "涨跌幅",
                        template: "{{ChangePercent | percentRender}}",
                        color: !0
                    },
                    {
                        name: "guba_hk",
                        title: "港股吧",
                        template: "<a href='//guba.eastmoney.com/list,hk{{Code}}.html'>港股吧</a>",
                        orderable: !1
                    },
                    {
                        data: "ABAHCode",
                        title: "A股代码",
                        template: "<a href='/{{marketMapping[ABAHMkt]}}{{ABAHCode}}.html'>{{ABAHCode}}</a>"
                    },
                    {
                        data: "ABAHName",
                        title: "A股名称",
                        template: "<a href='/{{marketMapping[ABAHMkt]}}{{ABAHCode}}.html'>{{ABAHName}}</a>"
                    },
                    {
                        data: "ABAHClose",
                        title: "最新价(RMB)",
                        color: "{{ABAHCP}}"
                    },
                    {
                        data: "ABAHCP",
                        title: "涨跌幅",
                        template: "{{ABAHCP | percentRender}}",
                        color: !0
                    },
                    {
                        name: "guba_a",
                        title: "A股吧",
                        template: "<a href='//guba.eastmoney.com/list,{{ABAHCode}}.html'>A股吧</a>",
                        orderable: !1
                    },
                    {
                        data: "AB/AH/HKD",
                        title: "比价(A/H)",
                        ordering: "desc"
                    },
                    {
                        data: "ABHPremium",
                        title: "溢价(A/H)"
                    }],
                Borads: [
                    {
                        name: "number",
                        title: "排名",
                        data: "$num"
                    },
                    {
                        name: "Name",
                        title: "板块名称",
                        template: "<a href='/web/{{Code}}{{MarketType}}.html'>{{Name}}</a>"
                    },
                    {
                        name: "Links",
                        title: "相关链接",
                        template: "<a href='//guba.eastmoney.com/list,{{Code}}.html'>股吧</a><a href='//data.eastmoney.com/bkzj/{{Code}}.html'>资金流</a><a href='//data.eastmoney.com/report/{{Code.replace('BK0','')}}yb.html'>研报</a>",
                        orderable: !1
                    },
                    {
                        data: "Close",
                        title: "最新价",
                        color: "{{Change}}"
                    },
                    {
                        data: "Change",
                        title: "涨跌额",
                        color: !0
                    },
                    {
                        data: "ChangePercent",
                        title: "涨跌幅",
                        template: "{{ChangePercent | percentRender}}",
                        color: "{{Change}}",
                        ordering: "desc"
                    },
                    {
                        title: "总市值",
                        data: "MarketValue",
                        template: "{{MarketValue | numbericFormat}}"
                    },
                    {
                        title: "换手率",
                        data: "TurnoverRate",
                        template: "{{TurnoverRate | percentRender}}"
                    },
                    {
                        name: "RaiseCount",
                        title: "上涨家数",
                        template: "{{RECORDSBK.split('|')[0]}}",
                        orderable: !1,
                        color: "1"
                    },
                    {
                        title: "下跌家数",
                        data: "FallCount",
                        template: "{{RECORDSBK.split('|')[2]}}",
                        orderable: !1,
                        color: "-1"
                    },
                    {
                        title: "领涨股票",
                        data: "BKCPLEADNAME",
                        template: "<a href='/{{marketMapping[BKCPLEADMKT]}}{{BKCPLEADCODE}}.html'>{{BKCPLEADNAME}}</a>"
                    },
                    {
                        title: "涨跌幅",
                        data: "BKCPLEADCP",
                        template: "{{BKCPLEADCP | percentRender}}",
                        color: !0
                    },
                    {
                        title: "领跌股票",
                        data: "BKCPLoseName",
                        template: "<a href='/{{marketMapping[BKCPLoseMkt]}}{{BKCPLoseCode}}.html'>{{BKCPLoseName}}</a>",
                        visible: !0
                    },
                    {
                        title: "涨跌幅",
                        data: "BKCPLoseCP",
                        template: "{{BKCPLoseCP | percentRender}}",
                        color: !0,
                        visible: !0
                    }],
                HKStocks: [
                    {
                        name: "number",
                        data: "$num"
                    },
                    {
                        name: "Code",
                        title: "代码",
                        template: "<a href='/hk/{{Code}}.html'>{{Code}}</a>"
                    },
                    {
                        name: "Name",
                        title: "名称",
                        template: "<a href='/hk/{{Code}}.html'>{{Name}}</a>"
                    },
                    {
                        name: "Links",
                        title: "相关链接",
                        template: "<a href='//guba.eastmoney.com/list,hk{{Code}}.html'>股吧</a><a href='//so.eastmoney.com/Web/s?keyword={{Name}}'>资讯</a><a href='//hkf10.eastmoney.com/html_HKStock/index.html?securitycode={{Code}}&name=companyIntro'>档案</a>",
                        orderable: !1
                    },
                    {
                        data: "Close",
                        title: "最新价（HKD）",
                        color: "{{Change}}"
                    },
                    {
                        data: "Change",
                        title: "涨跌额",
                        color: !0
                    },
                    {
                        data: "ChangePercent",
                        title: "涨跌幅",
                        template: "{{ChangePercent | percentRender}}",
                        color: "{{Change}}",
                        ordering: "desc"
                    },
                    {
                        data: "Open",
                        title: "今开",
                        color: "{{Open - PreviousClose}}"
                    },
                    {
                        data: "High",
                        title: "最高",
                        color: "{{High - PreviousClose}}"
                    },
                    {
                        data: "Low",
                        title: "最低",
                        color: "{{Low - PreviousClose}}"
                    },
                    {
                        data: "PreviousClose",
                        title: "昨收"
                    },
                    {
                        data: "Volume",
                        title: "成交量(股)",
                        template: "{{Volume | numbericFormat}}"
                    },
                    {
                        data: "Amount",
                        title: "成交额(港元)",
                        template: "{{Amount | numbericFormat}}"
                    },
                    {
                        data: '$favourite={"t":5}'
                    }],
                HKADR: [
                    {
                        name: "number",
                        data: "$num"
                    },
                    {
                        name: "ADRName",
                        title: "股份名称",
                        template: "<a href='//quote.eastmoney.com/hk/{{ADRCode}}.html'>{{ADRName}}</a>"
                    },
                    {
                        name: "ADRCode",
                        title: "港股代码",
                        template: "<a href='//quote.eastmoney.com/hk/{{ADRCode}}.html'>{{ADRCode}}</a>"
                    },
                    {
                        name: "Code",
                        title: "ADR代码",
                        template: "<a href='//quote.eastmoney.com/us/{{Code}}.html'>{{Code}}</a>"
                    },
                    {
                        data: "Close",
                        title: "ADR收市价(USD)",
                        color: "{{Change}}"
                    },
                    {
                        data: "Change",
                        title: "涨跌额",
                        color: !0
                    },
                    {
                        data: "ChangePercent",
                        title: "涨跌幅",
                        template: "{{ChangePercent | percentRender}}",
                        color: "{{Change}}",
                        ordering: "desc"
                    },
                    {
                        data: "HKD2USD",
                        title: "折合每股港元"
                    }],
                HKOthers: [
                    {
                    name: "number",
                    data: "$num"
                    },
                    {
                        name: "Code",
                        title: "代码",
                        template: "<a href='//quote.eastmoney.com/web/r/{{Code}}{{MarketType}}'>{{Code}}</a>"
                    },
                    {
                        name: "Name",
                        title: "名称",
                        template: "<a href='//quote.eastmoney.com/web/r/{{Code}}{{MarketType}}'>{{Name}}</a>"
                    },
                    {
                        data: "Close",
                        title: "最新价（HKD）",
                        color: "{{Change}}"
                    },
                    {
                        data: "Change",
                        title: "涨跌额",
                        color: !0
                    },
                    {
                        data: "ChangePercent",
                        title: "涨跌幅",
                        template: "{{ChangePercent | percentRender}}",
                        color: "{{Change}}",
                        ordering: "desc"
                    },
                    {
                        data: "Open",
                        title: "今开",
                        color: "{{Open - PreviousClose}}"
                    },
                    {
                        data: "High",
                        title: "最高",
                        color: "{{High - PreviousClose}}"
                    },
                    {
                        data: "Low",
                        title: "最低",
                        color: "{{Low - PreviousClose}}"
                    },
                    {
                        data: "PreviousClose",
                        title: "昨收"
                    },
                    {
                        data: "Volume",
                        title: "成交量(股)",
                        template: "{{Volume | numbericFormat}}"
                    },
                    {
                        data: "Amount",
                        title: "成交额(港元)",
                        template: "{{Amount | numbericFormat}}"
                    }],
                HSIndex: [
                    {
                        name: "number",
                        data: "$num"
                     },
                    {
                        name: "Code",
                        title: "代码",
                        template: "<a href='/zs{{Code}}.html'>{{Code}}</a>"
                    },
                    {
                        name: "Name",
                        title: "名称",
                        template: "<a href='/zs{{Code}}.html'>{{Name}}</a>"
                    },
                    {
                        data: "Close",
                        title: "最新价",
                        color: "{{Change}}"
                    },
                    {
                        data: "ChangePercent",
                        title: "涨跌幅",
                        template: "{{ChangePercent | percentRender}}",
                        color: !0,
                        ordering: "desc"
                    },
                    {
                        data: "Change",
                        title: "涨跌额",
                        color: !0
                    },
                    {
                        data: "Volume",
                        title: "成交量(股)",
                        template: "{{Volume | numbericFormat}}"
                    },
                    {
                        data: "Amount",
                        title: "成交额",
                        template: "{{Amount | numbericFormat}}"
                    },
                    {
                        data: "Amplitude",
                        title: "振幅",
                        template: "{{Amplitude | percentRender}}"
                    },
                    {
                        data: "High",
                        title: "最高",
                        color: "{{High - PreviousClose}}"
                    },
                    {
                        data: "Low",
                        title: "最低",
                        color: "{{Low - PreviousClose}}"
                    },
                    {
                        data: "Open",
                        title: "今开",
                        color: "{{Open - PreviousClose}}"
                    },
                    {
                        data: "PreviousClose",
                        title: "昨收"
                    },
                    {
                        title: "量比",
                        data: "VolumeRate"
                    }],
                NeeqStocks: [
                    {
                        name: "number",
                        data: "$num"
                    },
                    {
                        name: "Code",
                        title: "代码",
                        template: "<a href='//xinsanban.eastmoney.com/QuoteCenter/{{Code}}.html'>{{Code}}</a>"
                    },
                    {
                        name: "Name",
                        title: "名称",
                        template: "<a href='//xinsanban.eastmoney.com/QuoteCenter/{{Code}}.html'>{{Name}}</a>"
                    },
                    {
                        name: "Links",
                        title: "相关链接",
                        template: "<a href='//guba.eastmoney.com/topic,{{Code}}.html'>股吧</a><a href='//so.eastmoney.com/Web/s?keyword={{Name}}' target='_blank'>资讯</a>",
                        orderable: !1
                    },
                    {
                        data: "Close",
                        title: "最新价",
                        color: "{{Change}}"
                    },
                    {
                        data: "Change",
                        title: "涨跌额",
                        color: !0
                    },
                    {
                        data: "ChangePercent",
                        title: "涨跌幅",
                        template: "{{ChangePercent | percentRender}}",
                        color: !0,
                        ordering: "desc"
                    },
                    {
                        data: "Volume",
                        title: "成交量",
                        template: "{{Volume | numbericFormat}}"
                    },
                    {
                        data: "Amount",
                        title: "成交额",
                        template: "{{Amount | numbericFormat}}"
                    },
                    {
                        data: "PreviousClose",
                        title: "昨收"
                    },
                    {
                        data: "Open",
                        title: "今开",
                        color: "{{Open - PreviousClose}}"
                    },
                    {
                        data: "High",
                        title: "最高",
                        color: "{{High - PreviousClose}}"
                    },
                    {
                        data: "Low",
                        title: "最低",
                        color: "{{Low - PreviousClose}}"
                    },
                    {
                        title: "委比",
                        data: "CommissionRate",
                        color: !0
                    }],
                USStocks: [
                    {
                        name: "number",
                        data: "$num"
                    },
                    {
                        name: "Name",
                        title: "名称",
                        template: "<a href='/us/{{Code}}.html'>{{Name}}</a>"
                    },
                    {
                        data: "Close",
                        title: "最新价(美元)",
                        color: "{{Change}}"
                    },
                    {
                        data: "Change",
                        title: "涨跌额",
                        color: !0
                    },
                    {
                        data: "ChangePercent",
                        title: "涨跌幅",
                        template: "{{ChangePercent | percentRender}}",
                        color: !0,
                        ordering: "desc"
                    },
                    {
                        data: "Open",
                        title: "开盘价",
                        color: "{{Open - PreviousClose}}"
                    },
                    {
                        data: "High",
                        title: "最高价",
                        color: "{{High - PreviousClose}}"
                    },
                    {
                        data: "Low",
                        title: "最低价",
                        color: "{{Low - PreviousClose}}"
                    },
                    {
                        data: "PreviousClose",
                        title: "昨收价"
                    },
                    {
                        title: "总市值(美元)",
                        data: "MarketValue",
                        template: "{{MarketValue | numbericFormat}}"
                    },
                    {
                        title: "市盈率",
                        data: "PERation"
                    }],
                GlobalIndex: [
                    {
                        name: "number",
                        data: "$num"
                    },
                    {
                        name: "Name",
                        title: "名称",
                        className: "global-index-name",
                        template: "<em class='circle {{Tag=='0'?'trading':''}}' title='{{Tag=='0'?'交易中':'已收盘'}}'>●</em><a href='/web/r/{{Code}}{{MarketType}}'>{{Name}}</a>"
                    },
                    {
                        data: "Close",
                        title: "最新价",
                        color: "{{Change}}"
                    },
                    {
                        data: "Change",
                        title: "涨跌额",
                        color: !0
                    },
                    {
                        data: "ChangePercent",
                        title: "涨跌幅",
                        template: "{{ChangePercent | percentRender}}",
                        color: !0
                    },
                    {
                        data: "Open",
                        title: "开盘价",
                        color: "{{Open - PreviousClose}}"
                    },
                    {
                        data: "High",
                        title: "最高价",
                        color: "{{High - PreviousClose}}"
                    },
                    {
                        data: "Low",
                        title: "最低价",
                        color: "{{Low - PreviousClose}}"
                    },
                    {
                        data: "PreviousClose",
                        title: "昨收价"
                    },
                    {
                        title: "振幅",
                        data: "Amplitude",
                        template: "{{Amplitude | percentRender}}"
                    },
                    {
                        title: "最新行情时间",
                        data: "LastUpdate"
                    }],
                Futures: [
                    {
                        name: "number",
                        data: "$num"
                    },
                    {
                        name: "Code",
                        title: "代码",
                        template: "<a href='/{{MarketType=='_ZJ'||MarketType=='_ITFFO'?'gzqh':MarketType=='0'?'globalfuture':'qihuo'}}/{{Code}}.html'>{{Code}}</a>"
                    },
                    {
                        name: "Name",
                        title: "名称",
                        template: "<a href='/{{MarketType=='_ZJ'||MarketType=='_ITFFO'?'gzqh':MarketType=='0'?'globalfuture':'qihuo'}}/{{Code}}.html'>{{Name}}</a>"
                    },
                    {
                        data: "Close",
                        title: "最新价",
                        color: "{{Change}}"
                    },
                    {
                        data: "Change",
                        title: "涨跌额",
                        color: !0
                    },
                    {
                        data: "ChangePercent",
                        title: "涨跌幅",
                        template: "{{ChangePercent | percentRender}}",
                        color: !0,
                        ordering: "desc"
                    },
                    {
                        data: "Open",
                        title: "今开",
                        color: "{{Open - PreviousClose}}"
                    },
                    {
                        data: "High",
                        title: "最高",
                        color: "{{High - PreviousClose}}"
                    },
                    {
                        data: "Low",
                        title: "最低",
                        color: "{{Low - PreviousClose}}"
                    },
                    {
                        data: "PreviousSettlement",
                        title: "昨结"
                    },
                    {
                        data: "Volume",
                        title: "成交量",
                        template: "{{Volume | numbericFormat}}"
                    },
                    {
                        data: "Amount",
                        title: "成交额",
                        template: "{{Amount | numbericFormat}}"
                    },
                    {
                        title: "买盘(外盘)",
                        data: "BuyOrder"
                    },
                    {
                        title: "卖盘(内盘)",
                        data: "SellOrder"
                    },
                    {
                        data: "OpenInterest",
                        title: "持仓量"
                    }],
                GlobalFutures: [
                    {
                        name: "number",
                        data: "$num"
                    },
                    {
                        name: "Code",
                        title: "代码",
                        template: "<a href='/globalfuture/{{Code}}.html'>{{Code}}</a>"
                    },
                    {
                        name: "Name",
                        title: "名称",
                        template: "<a href='/globalfuture/{{Code}}.html'>{{Name}}</a>"
                    },
                    {
                        data: "Close",
                        title: "最新价",
                        color: "{{Change}}"
                    },
                    {
                        data: "Change",
                        title: "涨跌额",
                        color: !0
                    },
                    {
                        data: "ChangePercent",
                        title: "涨跌幅",
                        template: "{{ChangePercent | percentRender}}",
                        color: !0,
                        ordering: "desc"
                    },
                    {
                        data: "Open",
                        title: "今开",
                        color: "{{Open - PreviousClose}}"
                    },
                    {
                        data: "High",
                        title: "最高",
                        color: "{{High - PreviousClose}}"
                    },
                    {
                        data: "Low",
                        title: "最低",
                        color: "{{Low - PreviousClose}}"
                    },
                    {
                        data: "PreviousSettlement",
                        title: "昨结"
                    },
                    {
                        data: "Volume",
                        title: "成交量",
                        template: "{{Volume | numbericFormat}}"
                    },
                    {
                        title: "买盘(外盘)",
                        data: "BuyOrder"
                    },
                    {
                        title: "卖盘(内盘)",
                        data: "SellOrder"
                    },
                    {
                        data: "OpenInterest",
                        title: "持仓量"
                    }],
                Funds: [
                    {
                        name: "number",
                        data: "$num"
                    },
                    {
                        name: "Code",
                        title: "代码",
                        template: "<a href='//quote.eastmoney.com/web/r/{{Code}}{{MarketType}}'>{{Code}}</a>"
                    },
                    {
                        name: "Name",
                        title: "名称",
                        template: "<a href='//quote.eastmoney.com/web/r/{{Code}}{{MarketType}}'>{{Name}}</a>"
                    },
                    {
                        name: "Links",
                        title: "相关链接",
                        template: "<a href='//fund.eastmoney.com/{{Code}}.html'>估算图</a><a href='//guba.eastmoney.com/list,{{Code}}.html'>基金吧</a><a href='//fund.eastmoney.com/f10/{{Code}}.html'>档案</a>",
                        orderable: !1
                    },
                    {
                        data: "Close",
                        title: "最新价",
                        color: "{{Change}}"
                    },
                    {
                        data: "Change",
                        title: "涨跌额",
                        color: !0
                    },
                    {
                        data: "ChangePercent",
                        title: "涨跌幅",
                        template: "{{ChangePercent | percentRender}}",
                        color: !0,
                        ordering: "desc"
                    },
                    {
                        data: "Volume",
                        title: "成交量",
                        template: "{{Volume | numbericFormat}}"
                    },
                    {
                        data: "Amount",
                        title: "成交额",
                        template: "{{Amount | numbericFormat}}"
                    },
                    {
                        data: "Open",
                        title: "开盘价",
                        color: "{{Open - PreviousClose}}"
                    },
                    {
                        data: "High",
                        title: "最高价",
                        color: "{{High - PreviousClose}}"
                    },
                    {
                        data: "Low",
                        title: "最低价",
                        color: "{{Low - PreviousClose}}"
                    },
                    {
                        data: "PreviousClose",
                        title: "昨收"
                    }],
                Bonds: [
                    {
                        name: "number",
                        data: "$num"
                    },
                    {
                        name: "Code",
                        title: "代码",
                        template: "<a href='//quote.eastmoney.com/web/r/{{Code}}{{MarketType}}'>{{Code}}</a>"
                    },
                    {
                        name: "Name",
                        title: "名称",
                        template: "<a href='//quote.eastmoney.com/web/r/{{Code}}{{MarketType}}'>{{Name}}</a>"
                    },
                    {
                        data: "Close",
                        title: "最新价",
                        color: "{{Change}}"
                    },
                    {
                        data: "Change",
                        title: "涨跌额",
                        color: !0
                    },
                    {
                        data: "ChangePercent",
                        title: "涨跌幅",
                        template: "{{ChangePercent | percentRender}}",
                        color: !0,
                        ordering: "desc"
                    },
                    {
                        data: "Open",
                        title: "今开",
                        color: "{{Open - PreviousClose}}"
                    },
                    {
                        data: "High",
                        title: "最高",
                        color: "{{High - PreviousClose}}"
                    },
                    {
                        data: "Low",
                        title: "最低",
                        color: "{{Low - PreviousClose}}"
                    },
                    {
                        data: "PreviousClose",
                        title: "昨收"
                    },
                    {
                        data: "Volume",
                        title: "成交量(手)",
                        template: "{{Volume | numbericFormat}}"
                    },
                    {
                        data: "Amount",
                        title: "成交额",
                        template: "{{Amount | numbericFormat}}"
                    }],
                Forex: [
                    {
                        name: "number",
                        data: "$num"
                    },
                    {
                        name: "Code",
                        title: "代码",
                        template: "<a href='//quote.eastmoney.com/web/r/{{Code}}{{MarketType}}'>{{Code}}</a>"
                    },
                    {
                        name: "Name",
                        title: "名称",
                        template: "<a href='//quote.eastmoney.com/web/r/{{Code}}{{MarketType}}'>{{Name}}</a>"
                    },
                    {
                        data: "Close",
                        title: "最新价",
                        color: "{{Change}}"
                    },
                    {
                        data: "Change",
                        title: "涨跌额",
                        color: !0
                    },
                    {
                        data: "ChangePercent",
                        title: "涨跌幅",
                        template: "{{ChangePercent | percentRender}}",
                        color: !0,
                        ordering: "desc"
                    },
                    {
                        data: "Open",
                        title: "今开",
                        color: "{{Open - PreviousClose}}"
                    },
                    {
                        data: "High",
                        title: "最高",
                        color: "{{High - PreviousClose}}"
                    },
                    {
                        data: "Low",
                        title: "最低",
                        color: "{{Low - PreviousClose}}"
                    },
                    {
                        data: "PreviousClose",
                        title: "昨收"
                    }],
                StockOptions: [
                    {
                    name: "number",
                    data: "$num"
                    },
                    {
                        name: "Code",
                        title: "代码",
                        template: "<a href='//quote.eastmoney.com/web/r/{{Code}}{{MarketType}}'>{{Code}}</a>"
                    },
                    {
                        name: "Name",
                        title: "名称",
                        template: "<a href='//quote.eastmoney.com/web/r/{{Code}}{{MarketType}}'>{{Name}}</a>"
                    },
                    {
                        data: "Close",
                        title: "最新价",
                        color: "{{Change}}"
                    },
                    {
                        data: "Change",
                        title: "涨跌额",
                        color: !0
                    },
                    {
                        data: "ChangePercent",
                        title: "涨跌幅",
                        template: "{{ChangePercent | percentRender}}",
                        color: !0,
                        ordering: "desc"
                    },
                    {
                        data: "Volumn",
                        title: "成交量",
                        template: "{{Volume | numbericFormat}}"
                    },
                    {
                        data: "Amount",
                        title: "成交额",
                        template: "{{Amount | numbericFormat}}"
                    },
                    {
                        data: "OpenInterest",
                        title: "持仓量"
                    },
                    {
                        data: "ExercisePrice",
                        title: "行权价"
                    },
                    {
                        data: "ExerciseDateRemain",
                        title: "剩余日"
                    },
                    {
                        data: "OpenInterest",
                        title: "持仓量"
                    },
                    {
                        data: "OpenInterestAdd",
                        title: "日增"
                    },
                    {
                        data: "PreviousClose",
                        title: "昨收"
                    },
                    {
                        data: "Open",
                        title: "今开"
                    }],
                FuturesOptions: [
                    {
                    name: "number",
                    data: "$num"
                    },
                    {
                        name: "Code",
                        title: "代码",
                        template: "<a href='//quote.eastmoney.com/web/r/{{Code}}{{MarketType}}'>{{Code}}</a>"
                    },
                    {
                        name: "Name",
                        title: "名称",
                        template: "<a href='//quote.eastmoney.com/web/r/{{Code}}{{MarketType}}'>{{Name}}</a>"
                    },
                    {
                        data: "Close",
                        title: "最新价",
                        color: "{{Change}}"
                    },
                    {
                        data: "Change",
                        title: "涨跌额",
                        color: !0
                    },
                    {
                        data: "ChangePercent",
                        title: "涨跌幅",
                        template: "{{ChangePercent | percentRender}}",
                        color: !0,
                        ordering: "desc"
                    },
                    {
                        data: "Volumn",
                        title: "成交量",
                        template: "{{Volume | numbericFormat}}"
                    },
                    {
                        data: "Amount",
                        title: "成交额",
                        template: "{{Amount | numbericFormat}}"
                    },
                    {
                        data: "OpenInterest",
                        title: "持仓量"
                    },
                    {
                        data: "ExercisePrice",
                        title: "行权价"
                    },
                    {
                        data: "ExerciseDateRemain",
                        title: "剩余日"
                    },
                    {
                        data: "OpenInterest",
                        title: "持仓量"
                    },
                    {
                        data: "OpenInterestAdd",
                        title: "日增"
                    },
                    {
                        data: "PreviousSettlement",
                        title: "昨结"
                    },
                    {
                        data: "Open",
                        title: "今开"
                    }],
                GoldFutures: [
                    {
                        name: "number",
                        data: "$num"
                     },
                    {
                        name: "Code",
                        title: "代码",
                        template: "<a href='//quote.eastmoney.com/web/r/{{Code}}{{MarketType}}'>{{Code}}</a>"
                    },
                    {
                        name: "Name",
                        title: "品种",
                        template: "<a href='//quote.eastmoney.com/web/r/{{Code}}{{MarketType}}'>{{Name}}</a>",
                        orderable: !1
                    },
                    {
                        data: "Close",
                        title: "最新价",
                        color: "{{Change}}"
                    },
                    {
                        data: "Change",
                        title: "涨跌额",
                        color: !0
                    },
                    {
                        data: "ChangePercent",
                        title: "涨跌幅",
                        template: "{{ChangePercent | percentRender}}",
                        color: !0,
                        ordering: "desc"
                    },
                    {
                        data: "Open",
                        title: "今开",
                        color: "{{Open - PreviousClose}}"
                    },
                    {
                        data: "High",
                        title: "最高",
                        color: "{{High - PreviousClose}}"
                    },
                    {
                        data: "Low",
                        title: "最低",
                        color: "{{Low - PreviousClose}}"
                    },
                    {
                        data: "PreviousSettlement",
                        title: "昨结"
                    },
                    {
                        data: "LastUpdate",
                        title: "更新时间"
                    }],
                GoldGoods: [
                    {
                    name: "number",
                    data: "$num"
                    },
                    {
                        name: "Code",
                        title: "代码"
                    },
                    {
                        name: "Name",
                        title: "品种",
                        orderable: !1
                    },
                    {
                        data: "Close",
                        title: "最新价",
                        color: "{{Change}}"
                    },
                    {
                        data: "Change",
                        title: "涨跌额",
                        color: !0
                    },
                    {
                        data: "ChangePercent",
                        title: "涨跌幅",
                        template: "{{ChangePercent | percentRender}}",
                        color: !0,
                        ordering: "desc"
                    },
                    {
                        data: "Open",
                        title: "今开",
                        color: "{{Open - PreviousClose}}"
                    },
                    {
                        data: "High",
                        title: "最高",
                        color: "{{High - PreviousClose}}"
                    },
                    {
                        data: "Low",
                        title: "最低",
                        color: "{{Low - PreviousClose}}"
                    },
                    {
                        data: "PreviousSettlement",
                        title: "昨结"
                    },
                    {
                        data: "LastUpdate",
                        title: "更新时间"
                    }],
                Virtual: [
                    {
                        name: "number",
                        data: "$num"
                    },
                    {
                        name: "Code",
                        title: "代码"
                    },
                    {
                        name: "Name",
                        title: "名称"
                    },
                    {
                        data: "DescribeMarket",
                        title: "市场",
                        orderable: !1
                    },
                    {
                        name: "TradeCurrency",
                        title: "交易货币",
                        template: "{{TradeCurrency === 'USD'?'美元':'人民币'}}",
                        orderable: !1
                    },
                    {
                        name: "Close_CNY",
                        title: "人民币",
                        color: "{{Change}}",
                        className: "muti-row",
                        template: "￥{{TradeCurrency === 'CNY'?Close:USD2CNY}}",
                        orderable: !1
                    },
                    {
                        name: "Close_USD",
                        title: "美元",
                        color: "{{Change}}",
                        className: "muti-row",
                        template: "${{TradeCurrency==='USD'?Close:CNY2USD}}",
                        orderable: !1
                    },
                    {
                        name: "Change",
                        title: "涨跌额",
                        template: "{{TradeCurrency==='USD'?'$':'￥'}}{{Change}}",
                        color: !0
                    },
                    {
                        name: "ChangePercent",
                        title: "涨跌幅",
                        template: "{{ChangePercent | percentRender}}",
                        color: !0,
                        ordering: "desc"
                    },
                    {
                        name: "Open",
                        template: "{{TradeCurrency==='USD'?'$':'￥'}}{{Open}}",
                        title: "今开",
                        color: "{{Open - PreviousClose}}"
                    },
                    {
                        name: "High",
                        template: "{{TradeCurrency==='USD'?'$':'￥'}}{{High}}",
                        title: "最高",
                        color: "{{High - PreviousClose}}"
                    },
                    {
                        name: "Low",
                        template: "{{TradeCurrency==='USD'?'$':'￥'}}{{Low}}",
                        title: "最低",
                        color: "{{Low - PreviousClose}}"
                    },
                    {
                        name: "PreviousClose",
                        template: "{{TradeCurrency==='USD'?'$':'￥'}}{{PreviousClose}}",
                        title: "昨收"
                    },
                    {
                        data: "Volume",
                        title: "成交量"
                    },
                    {
                        data: "BuyOrder",
                        title: "买量"
                    },
                    {
                        data: "SellOrder",
                        title: "卖量"
                    }],
                ConvertibleBond: [
                    {
                    name: "number",
                    data: "$num"
                    },
                    {
                        title: "转债代码",
                        name: "Code",
                        template: "<a target=_blank href='/bond/{{marketMapping[MarketType]}}{{Code}}.html'>{{Code}}</a>",
                        orderable: !1
                    },
                    {
                        title: "转债名称",
                        name: "Name",
                        template: "<a target=_blank href='/bond/{{marketMapping[MarketType]}}{{Code}}.html'>{{Name}}</a>",
                        orderable: !1
                    },
                    {
                        title: "最新价",
                        data: "Close",
                        color: "{{ChangePercent}}"
                    },
                    {
                        title: "涨跌幅",
                        name: "ChangePercent",
                        template: "{{ChangePercent | percentRender}}",
                        color: !0
                    },
                    {
                        title: "相关链接",
                        name: "links",
                        template: '<a target=_blank href="http://guba.eastmoney.com/list,{{Code}}.html">股吧</a><a target=_blank href="http://data.eastmoney.com/kzz/detail/{{Code}}.html">详细</a>',
                        orderable: !1
                    },
                    {
                        title: "正股代码",
                        data: "UnderlyingStockCode",
                        template: "<a target=_blank href='/{{marketMapping[UnderlyingStockMkt]}}{{UnderlyingStockCode}}.html'>{{UnderlyingStockCode}}</a>",
                        orderable: !1
                    },
                    {
                        title: "正股名称",
                        name: "UnderlyingStockName",
                        template: "<a target=_blank href='/{{marketMapping[UnderlyingStockMkt]}}{{UnderlyingStockCode}}.html'>{{UnderlyingStockName}}</a>",
                        orderable: !1
                    },
                    {
                        title: "最新价",
                        data: "UnderlyingStockPrice",
                        color: "{{UnderlyingStockCP}}"
                    },
                    {
                        title: "涨跌幅",
                        name: "UnderlyingStockCP",
                        template: "{{UnderlyingStockCP | percentRender}}",
                        color: !0
                    },
                    {
                        title: "转股价",
                        data: "ConversionPrice"
                    },
                    {
                        title: '<span id="cv-tips" title="转股价值=正股价/转股价*100">转股价值<em class="help-icon"></em></span>',
                        data: "ConversionValue"
                    },
                    {
                        title: '<span id="cpr-tips" title="转股溢价率 = （转债最新价 – 转股价值）/ 转股价值">转股溢价率<em class="help-icon"></em></span>',
                        name: "ConvertiblePremiumRate",
                        template: "{{ConvertiblePremiumRate | percentRender}}",
                        color: !0
                    },
                    {
                        title: '<span id="bpr-tips" title="纯债溢价率 = （转债最新价 – 纯债价值）/ 纯债价值">纯债溢价率<em class="help-icon"></em></span>',
                        name: "BondPremiumRate",
                        template: "{{BondPremiumRate | percentRender}}",
                        color: !0
                    },
                    {
                        title: '<span id="tpsp-tips" title="满足回售触发条件时，可转债持有人有权将其持有的可转债全部或部分按债券面值加上当期应计利息的价格回售给公司">回售触发价<em class="help-icon"></em></span>',
                        data: "TriggerPriceOfSpecialPut"
                    },
                    {
                        title: '<span id="tpsr-tips" title="满足赎回触发条件时，公司有权按照债券面值加当期应计利息的价格赎回全部或部分未转股的可转债">强赎触发价<em class="help-icon"></em></span>',
                        data: "TriggerPriceOfSpecialRedemption"
                    },
                    {
                        title: '<span id="rp-tips" title="公司有权以债券发行说明书中规定的到期赎回价买回其发行在外债券">到期赎回价<em class="help-icon"></em></span>',
                        data: "Redemptionprice"
                    },
                    {
                        title: "纯债价值",
                        data: "StraightBondValue"
                    },
                    {
                        title: "开始转股日",
                        data: "ConversionDate"
                    },
                    {
                        title: "上市日期",
                        data: "BAdinLISTDATE"
                    },
                    {
                        title: "申购日期",
                        data: "BAdinSTARTDATE",
                        ordering: "desc-0"
                    },
                    {
                        data: "$favourite|UnderlyingStockCode,UnderlyingStockMkt"
                    }],
                FuturesDerivatives: [
                    {
                    name: "number",
                    data: "$num"
                    },
                    {
                        name: "Code",
                        title: "代码"
                    },
                    {
                        name: "Name",
                        title: "名称"
                    },
                    {
                        data: "Close",
                        title: "最新价",
                        color: "{{Change}}"
                    },
                    {
                        data: "Change",
                        title: "涨跌额",
                        color: !0
                    },
                    {
                        data: "ChangePercent",
                        title: "涨跌幅",
                        template: "{{ChangePercent | percentRender}}",
                        color: !0,
                        ordering: "desc"
                    },
                    {
                        data: "Open",
                        title: "今开",
                        color: "{{Open - PreviousClose}}"
                    },
                    {
                        data: "High",
                        title: "最高",
                        color: "{{High - PreviousClose}}"
                    },
                    {
                        data: "Low",
                        title: "最低",
                        color: "{{Low - PreviousClose}}"
                    },
                    {
                        data: "PreviousSettlement",
                        title: "昨结"
                    },
                    {
                        data: "Volume",
                        title: "成交量",
                        template: "{{Volume | numbericFormat}}"
                    },
                    {
                        data: "Amount",
                        title: "成交额",
                        template: "{{Amount | numbericFormat}}"
                    },
                    {
                        title: "买盘(外盘)",
                        data: "BuyOrder"
                    },
                    {
                        title: "卖盘(内盘)",
                        data: "SellOrder"
                    },
                    {
                        data: "OpenInterest",
                        title: "持仓量"
                    }]
            }},
        "./ClientApp/config/gridlist.fields.map.json": function(e, t) {e.exports = {
                ESBFDTC: "csv:0=MarketType&1=Code&2=Name&3=ID",
                CTF: "csv:0=MarketType&1=Code&2=Name&3=Close&4=Change&5=ChangePercent&9=Volume&10=Amount&11=Open&12=PreviousClose&13=High&14=Low&15=TurnoverRate&16=PERation",
                FCABHL: "csv:0=MarketType&1=Code&2=Name&3=Close&4=ChangePercent&5=ABAHCode&6=ABAHMkt&7=ABAHName&8=ABAHClose&9=ABAHCP&10=CloseCrossUSD&11=CloseCrossHKD&12=AB/AH/USD&13=AB/AH/HKD&14=ABDPremium&15=ABHPremium&16=Change",
                FCOIATC: "csv:0=MarketType&1=Code&2=Name&3=Close&4=Change&5=ChangePercent&6=Volume&7=Amount&8=Amplitude&9=High&10=Low&11=Open&12=PreviousClose&13=FiveMinuteChangePercent&14=VolumeRate&15=TurnoverRate&16=PERation&17=PB&18=MarketValue&19=FlowCapitalValue&20=ChangePercent60Day&21=ChangePercent360Day&22=Speed&23=ListingDate&24=LastUpdate",
                FCOIATD: "csv:0=MarketType&1=Code&2=Name&3=Close&4=Change&5=ChangePercent&6=Volume&7=Amount&8=Amplitude&9=High&10=Low&11=Open&12=PreviousClose&13=FiveMinuteChangePercent&14=VolumeRate&15=TurnoverRate&16=PERation&17=PB&18=MarketValue&19=FlowCapitalValue&20=ChangePercent60Day&21=ChangePercent360Day&22=Speed&23=ListingDate&24=LastUpdate&26=IsCDR&27=IsCDRVotable&28=IsCDRProfit",
                FCOIA: "csv:0=MarketType&1=Code&2=Name&3=Close&4=Change&5=ChangePercent&6=Volume&7=Amount&8=PreviousClose&9=Open&10=High&11=Low&12=CommissionRate&15=Amplitude&16=FiveMinuteChangePercent&17=TurnoverRate&18=PERation&19=VolumeRate",
                FCFL4O: "csv:0=MarketType&1=Code&2=Name&3=Close&4=ChangePercent&5=Change&6=BuyOrder&7=SellOrder&8=PreviousSettlement&9=OpenInterest&10=Volume&11=Open&12=PreviousClose&13=High&14=Low&15=Amount",
                FCRH: "csv:0=MarketType&1=Code&2=Name&3=Close&4=Change&5=ChangePercent&6=LastUpdate&7=Tag&8=Open&9=High&10=Low&11=PreviousClose&12=Amplitude",
                AMIC: "csv:0=Name&1=Code&2=Close&3=Change&4=ChangePercent&5=High&6=Low&7=Open&8=PreviousClose&9=Amplitude&13=LastUpdate&14=MarketType",
                FC2UCO: "csv:0=MarketType&1=Code&2=Name&3=Close&4=Change&5=ChangePercent&6=Volume&7=Amount&8=OpenInterest&9=ExercisePrice&10=ExerciseDateRemain&11=OpenInterestAdd&12=PreviousSettlement&13=Open&14=WarehouseBad",
                FCOL: "csv:0=MarketType&1=Code&2=Name&3=Close&4=Change&5=ChangePercent&6=Volume&7=Amount&8=OpenInterest&9=ExercisePrice&10=ExerciseDateRemain&11=OpenInterestAdd&12=PreviousClose&13=Open&14=High&15=Low",
                FCUFFO: "csv:0=MarketType&1=Code&2=Name&3=Open&4=Close&5=Change&6=ChangePercent&7=PreviousSettlement&8=High&9=Low&10=LastUpdate",
                FPGBKI: "csv:0=MarketType&1=Code&2=Name&3=ChangePercent&4=MarketValue&5=TurnoverRate&6=RECORDSBK&7=BKCPLEADCODE&8=BKCPLEADMKT&9=BKCPLEADNAME&10=BKCPLEADCLOSE&11=BKCPLEADCP&12=BKCPLoseCode&13=BKCPLoseMkt&14=BKCPLoseName&15=BKCPLoseClose&16=BKCPLoseCP&17=BKType&18=Close&19=Change",
                DCFFPBFMS: "csv:0=MarketType&1=Code&2=Name&3=BalFlowMainSub",
                FCFL4OTA: "csv:0=MarketType&1=Code&2=Name&3=Close&4=ChangePercent&5=Change&6=BuyOrder&7=SellOrder&8=PreviousSettlement&9=OpenInterest&10=Volume&11=Open&12=PreviousClose&13=High&14=Low&15=Amount&16=DescribeMarket&17=TradeCurrency&18=USD2CNY&19=CNY2USD",
                FC20DPADRL: "csv:0=MarketType&1=Code&2=Name&3=Close&4=ChangePercent&5=ADRCode&6=ADRMkt&7=ADRName&8=USD2HKD&9=HKD2USD&10=Change",
                FC20RA: "csv:0=MarketType&1=Code&2=Name&3=Close&4=ChangePercent&5=Change",
                FC20SSBTA: "csv:0=MarketType&1=Code&2=Name&3=ChangePercent&4=FiveMinuteChangePercent&5=TurnoverRate&6=VolumeRate&7=Amplitude&8=Amount&9=PERation&10=PB&11=FlowCapitalValue&12=MarketValue&13=ChangePercent60Day&14=ChangePercent360Day&15=Speed&16=Change&17=Close",
                FC20SSBTB: "csv:0=MarketType&1=Code&2=Name&3=ChangePercent&4=DayFlow&5=TurnoverRate&6=Speed&7=Amount&8=BalFlowMain&9=FFRank&10=Change&11=Close&12=BKCPLEADCODE&13=BKCPLEADMKT&14=BKCPLEADNAME&15=BKCPLEADCP&16=RankingChange&17=BalFlowNetRate&18=BKLEADER&19=BKLEADERNAME&20=BKCPLoseMkt&21=BKCPLoseCode&22=BKCPLoseName&23=BKCPLoseCP",
                FC20SSBTC: "csv:0=MarketType&1=Code&2=Name&3=ChangePercent&4=Volume&5=Amount&6=Change&7=Close",
                FC20RC: "csv:0=MarketType&1=Code&2=Name&3=Close&4=ChangePercent&5=BalFlowMain&6=BalFlowNetRate",
                FC20CDTA: "csv:0=MarketType&1=Code&2=Name&3=Close&4=ChangePercent&5=UnderlyingStockMkt&6=UnderlyingStockCode&7=UnderlyingStockName&8=UnderlyingStockPrice&9=UnderlyingStockCP&10=ConversionPrice&11=ConversionValue&12=ConvertiblePremiumRate&13=BondPremiumRate&14=TriggerPriceOfSpecialPut&15=TriggerPriceOfSpecialRedemption&16=Redemptionprice&17=StraightBondValue&18=ConversionDate&19=BAdinLISTDATE&20=BAdinSTARTDATE",
                TSQList: "json|diff:"
            }},
        "./ClientApp/config/tsq.gridlist.fields.json": function(e, t) {
            e.exports = {
                TSQABStocks: [{
                    name: "number",
                    data: "$num"
                },
                    {
                        name: "Code",
                        data: "f12",
                        title: "代码",
                        template: "<a href='/{{innerMktNumMap[f13]}}{{f12}}.html'>{{f12}}</a>"
                    },
                    {
                        name: "Name",
                        data: "f14",
                        title: "名称",
                        template: "<a href='/{{innerMktNumMap[f13]}}{{f12}}.html'>{{f14}}</a>"
                    },
                    {
                        name: "Links",
                        title: "相关链接",
                        template: "<a href='http://guba.eastmoney.com/list,{{f12}}.html'>股吧</a><a href='http://data.eastmoney.com/zjlx/{{f12}}.html'>资金流</a><a href='http://data.eastmoney.com/stockdata/{{f12}}.html'>数据</a>",
                        orderable: !1
                    },
                    {
                        name: "Close",
                        data: "f2",
                        title: "最新价",
                        color: "{{f4}}",
                        template: "{{$current | decimalHandler(f1)}}"
                    },
                    {
                        name: "ChangePercent",
                        data: "f3",
                        title: "涨跌幅",
                        template: "{{$current | decimalHandler(2) | percentRender}}",
                        color: "{{f4}}",
                        ordering: "desc"
                    },
                    {
                        name: "Change",
                        data: "f4",
                        title: "涨跌额",
                        color: !0,
                        template: "{{$current | decimalHandler(f1)}}"
                    },
                    {
                        name: "Volume",
                        data: "f5",
                        title: "成交量(手)",
                        template: "{{$current | numbericFormat}}"
                    },
                    {
                        name: "Amount",
                        data: "f6",
                        title: "成交额",
                        template: "{{$current | numbericFormat}}"
                    },
                    {
                        name: "Amplitude",
                        data: "f7",
                        title: "振幅",
                        template: "{{$current | decimalHandler(2) | percentRender}}"
                    },
                    {
                        name: "High",
                        data: "f15",
                        title: "最高",
                        color: "{{$current - f18}}",
                        template: "{{$current | decimalHandler(f1)}}"
                    },
                    {
                        name: "Low",
                        data: "f16",
                        title: "最低",
                        color: "{{$current - f18}}",
                        template: "{{$current | decimalHandler(f1)}}"
                    },
                    {
                        name: "Open",
                        data: "f17",
                        title: "今开",
                        color: "{{$current - f18}}",
                        template: "{{$current | decimalHandler(f1)}}"
                    },
                    {
                        name: "PreviousClose",
                        data: "f18",
                        title: "昨收",
                        template: "{{$current | decimalHandler(f1)}}"
                    },
                    {
                        name: "VolumeRate",
                        data: "f10",
                        title: "量比",
                        template: "{{$current | decimalHandler(2)}}"
                    },
                    {
                        name: "TurnoverRate",
                        data: "f8",
                        title: "换手率",
                        template: "{{$current | decimalHandler(2) | percentRender}}"
                    },
                    {
                        name: "PERation",
                        data: "f9",
                        title: "市盈率(动态)",
                        template: "{{$current | decimalHandler(2)}}"
                    },
                    {
                        name: "PB",
                        data: "f23",
                        title: "市净率",
                        visible: !1,
                        template: "{{$current | decimalHandler(2)}}"
                    },
                    {
                        name: "MarketValue",
                        data: "f20",
                        title: "总市值",
                        template: "{{$current | numbericFormat}}",
                        visible: !1
                    },
                    {
                        name: "FlowCapitalValue",
                        data: "f21",
                        title: "流通市值",
                        template: "{{$current | numbericFormat}}",
                        visible: !1
                    },
                    {
                        name: "ChangePercent60Day",
                        data: "f24",
                        title: "60日涨跌幅",
                        template: "{{$current | decimalHandler(2) | percentRender}}",
                        color: !0,
                        visible: !1
                    },
                    {
                        name: "ChangePercent360Day",
                        data: "f25",
                        title: "年初至今涨跌幅",
                        template: "{{$current | decimalHandler(2) | percentRender}}",
                        color: !0,
                        visible: !1
                    },
                    {
                        name: "Speed",
                        data: "f22",
                        title: "涨速",
                        template: "{{$current | decimalHandler(2) | percentRender}}",
                        color: !0,
                        visible: !1
                    },
                    {
                        name: "FiveMinuteChangePercent",
                        data: "f11",
                        title: "五分钟涨跌",
                        template: "{{$current | decimalHandler(2) | percentRender}}",
                        color: !0,
                        visible: !1
                    },
                    {
                        data: "$favourite"
                    }]
            }
        },
        "./ClientApp/css/boardlist.css": function(e, t) {},
        "./ClientApp/css/common.css": function(e, t) {},
        "./ClientApp/modules/asyncloaders.js": function(e, t, n) {
            function r(e) {
                r.cache || (r.cache = new s);
                var t = this,
                    n = o({
                            url: "",
                            id: "",
                            charset: "utf-8",
                            checkrepeat: !0,
                            data: null,
                            success: null,
                            error: function() {
                                console.error("load script error: " + n.url)
                            }
                        },
                        e);
                if (!n.url) return ! 1;
                var i = "function" == typeof n.success && n.success,
                    l = new a(n.url);
                n.data && l.setSearch(n.data);
                var c = this.url = l.toString(),
                    u = this.id = n.id || r.generateId(c);
                n.checkrepeat && (r.cache[c] || document.getElementById(u)) ? i && i.apply(this, [r.cache[c] || document.getElementById(u), null]) : (this.script = null, this.load = function() {
                    var e = this.script = document.createElement("script");
                    e.id = u,
                        e.className = "ns-script-loader",
                        e.charset = n.charset,
                        e.async = !0,
                        e.defer = !0,
                        e.src = c,
                    "function" == typeof n.error && (e.onerror = function(e) {
                        n.error.apply(t, [e])
                    }),
                        e.onload = e.onreadystatechange = function(n) {
                            e.readyState && !/loaded|complete/.test(e.readyState) || (e.onload = e.onreadystatechange = null, r.cache.set(c, e), i && i.apply(t, [e, n]))
                        },
                        document.getElementsByTagName("head")[0].appendChild(e)
                },
                    this.destory = function() {
                        document.getElementsByTagName("head")[0].removeChild(this.script)
                    },
                    this.load())
            }
            var a = n("./ClientApp/modules/uri/main.js"),
                o = n("./ClientApp/modules/utils.extend.js"),
                s = n("./ClientApp/modules/utils.cache.js");
            r.generateId = function(e) {
                return e ? "script-" +
                    function(e) {
                        var t = 0;
                        if (0 == e.length) return t;
                        for (i = 0; i < e.length; i++) t = (t << 5) - t + e.charCodeAt(i),
                            t &= t;
                        return t
                    } (e) : ""
            },
                r.remove = function(e) {
                    var t = document.getElementById(e);
                    t && document.getElementsByTagName("head")[0].removeChild(t)
                },
                e.exports = {
                    imgLoader: function(e) {
                        if ("object" != typeof e || !e.url) return ! 1;
                        var t = "function" == typeof e.success ? e.success: null,
                            n = new a(e.url);
                        e.data && n.setSearch(e.data),
                        e.cache || n.setSearch("_", +new Date);
                        var r = document.createElement("img");
                        return "number" == typeof e.height && e.height > 0 ? r.setAttribute("height", e.height + "px") : e.height && r.setAttribute("height", e.height),
                            "number" == typeof e.width && e.width > 0 ? r.setAttribute("width", e.width + "px") : e.width && r.setAttribute("width", e.width),
                            r.setAttribute("src", n.toString()),
                        "function" == typeof e.error && (r.onerror = function() {
                            e.error(r)
                        }),
                            r.onload = r.onreadystatechange = function(e) {
                                r.readyState && !/loaded|complete/.test(r.readyState) || (r.onload = r.onreadystatechange = null, t && t(r))
                            },
                            r
                    },
                    scriptLoader: r
                }
        },
        "./ClientApp/modules/favouriteclient.js": function(e, t, n) {
            function r(e) {
                function t() {
                    if (h.cookie && !h.cookie.disabled) {
                        for (var e = [], t = 0; t < (this.stocks ? this.stocks.length: 0); t++) {
                            var n = this.stocks[t],
                                r = this.stocks.get(n);
                            r && e.push(r.toString())
                        }
                        s(h.cookie.name, e.join("~"), h.cookie)
                    }
                }
                function n(e, t, n) {
                    var a = "";
                    a = u.checkLogin() ? new i(h.server.paths.login).absoluteTo(h.server.baseUrl).toString() : new i(h.server.paths.anonymous).absoluteTo(h.server.baseUrl).toString();
                    var o = n || h.onerror;
                    r(a, e,
                        function(e) {
                            e && 1 == e.result ? "function" == typeof t && t(e) : "function" === o && o(e.data.msg)
                        },
                        o)
                }
                function r(e, t, n, r) {
                    l(e, t, "cb", n, r)
                }
                var u = this,
                    h = c.extend(c.extend({},
                        d, !0), e || {}),
                    f = !1;
                this.stocks = new a;
                this.checkLogin = function() {
                    var e = s("ct"),
                        t = s("ut"),
                        n = s("uidal");
                    return !! e && !!t && !!n
                },
                    this.getlist = function(e, r) {
                        function a(e, t) {
                            if (t = t || this.stocks, e instanceof Array && e.length > 0) for (var n = 0; n < e.length; n++) {
                                var r = e[n],
                                    a = r ? r.split("-") : [];
                                a.length > 0 && t.add(new o(a[0], a[1]))
                            }
                        }
                        if (f) return "function" == typeof e && e(this.stocks),
                            this.stocks;
                        var i = s(h.cookie.name),
                            l = h.defaultstocks;
                        n({
                                f: "gsaandcheck",
                                t: h.t
                            },
                            function(n) {
                                if (n && n.data && "string" == typeof n.data.list && n.data.list) for (var r = n.data.list.split(","), i = 0; i < r.length; i++) {
                                    var s = r[i];
                                    if ("string" == typeof s) {
                                        var c = s.split("|");
                                        u.stocks.add(new o(c[0], parseInt(c[1]) || c[1]))
                                    }
                                }
                                try {
                                    if ("function" == typeof e) {
                                        var d = u.stocks.clone();
                                        d.length < h.displayCount && (a.apply(u, [l, d]), d.splice(h.displayCount, d.length - h.displayCount)),
                                            e.apply(u, [d])
                                    }
                                } catch(p) {
                                    console.error(p)
                                }
                                t.apply(u),
                                    f = !0
                            },
                            function() {
                                i && (l = i.split("~")),
                                    a(l),
                                "function" == typeof e && e.apply(this, [this.stocks])
                            })
                    },
                    this.add = function(e, r, a, i) {
                        var s;
                        return s = e instanceof o ? e: new o(e, r),
                            this.stocks.add(s),
                            n({
                                    f: "asz",
                                    sc: s.toFavorId(),
                                    t: h.t
                                },
                                a, i || h.onerror),
                            t.apply(u),
                            s
                    },
                    this.addbatch = function(e, r, a) {
                        for (var o = [], i = 0; i < e.length; i++) {
                            var s = e[i];
                            this.stocks.add(s) >= 0 && o.push(s.toFavorId())
                        }
                        t.apply(u),
                        o.length > 0 && n({
                                f: "aszlot",
                                sc: o.join(","),
                                t: h.t
                            },
                            r, a)
                    },
                    this.remove = function(e, r, a, i) {
                        var s;
                        if (s = e instanceof o ? e: new o(e, r), this.stocks.remove(s)) return t.apply(u),
                            n({
                                    f: "dsz",
                                    sc: s.toFavorId(),
                                    t: h.t
                                },
                                a, i),
                            s
                    },
                    this.checkfavor = function(e, t) {
                        var n;
                        return n = e instanceof o ? e: new o(e, t),
                            this.stocks.contains(n)
                    },
                    this.move = function(e, r) {
                        this.stocks.move(e, r),
                            t.apply(u);
                        var a = {
                                f: "ssz",
                                sc: e.toFavorId(),
                                t: h.t
                            },
                            o = this.stocks[r - 1],
                            i = this.stocks[r + 1];
                        o && (a.sc1 = o),
                        i && (a.sc2 = i),
                            n(a)
                    },
                    this.getQuote = function(e, t, n) {
                        function a() {
                            this.stocks.length <= 0 || r(o.baseUrl, o.data,
                                function(e) {
                                    if (e) {
                                        var n = e;
                                        "function" == typeof o.dataResolver && (n = o.dataResolver.apply(u, [e])),
                                        "function" == typeof t && t.apply(u, [n])
                                    }
                                },
                                n)
                        }
                        var o = c.extend({},
                            h.quote, !0),
                            i = e || this.stocks.length,
                            s = this.stocks.slice(0, i).join(",");
                        c.extend(o, {
                                data: {
                                    cmd: s
                                }
                            },
                            !0),
                            a(),
                        o.update > 0 && setInterval(a, o.update)
                    }
            }
            function a() {
                var e = this.cache = new c.ObjectCache;
                this.get = function(t) {
                    return e[t]
                },
                    this.add = function(t) {
                        return t instanceof o && !this.contains(t.id) ? (e[t.id] = t, this.push(t.id) - 1) : -1
                    },
                    this.remove = function(t) {
                        var n = t;
                        t instanceof o && (n = t.id);
                        var r = this.indexOf(n);
                        this[r];
                        return ! (r < 0) && (this.splice(r, 1), e.remove(n))
                    },
                    this.clear = function() {
                        e.clear(),
                            this.splice(0, this.length)
                    },
                    this.move = function(e, t) {
                        e instanceof o && e.id;
                        var n = this.indexOf(_key),
                            r = this[n];
                        if (n >= 0) return this.splice(n, 1),
                            this.splice(t, 0, r),
                            this
                    },
                    this.contains = function(t) {
                        var n = t;
                        return t instanceof o && (n = t.id),
                            e.hasOwnProperty(n)
                    },
                    this.clone = function() {
                        for (var e = new a,
                                 t = 0; t < this.length; t++) {
                            var n = this.get(this[t]);
                            e.add(n)
                        }
                        return e
                    }
            }
            function o(e, t) {
                this.id = e + t,
                    this.code = e || "",
                    this.market = t || "",
                    this.shortMarket = u[t],
                    this.toFavorId = function() {
                        var e = this.market;
                        return parseInt(e) && (e = "0" + e),
                        this.code + "|" + e + "|01"
                    },
                    this.toString = function() {
                        return this.code + "-" + this.market
                    }
            }
            var i = n("./ClientApp/modules/uri/main.js"),
                s = n("./ClientApp/modules/utils.cookie.js"),
                l = n("./ClientApp/modules/jsonp.js"),
                c = r.utils = n("./ClientApp/modules/utils.js");
            a.prototype = new Array;
            var u = o.shortMarketMapping = {
                1 : "sh",
                2 : "sz",
                _TB: "sz"
            };
            r.version = "2.0.0",
                r.stockinfo = o,
                r.stockcontainer = a;
            var d = r.defaults = {
                server: {
                    baseUrl: "https://myfavor.eastmoney.com",
                    paths: {
                        login: "/mystock",
                        anonymous: "/mystock_anonym"
                    }
                },
                quote: {
                    baseUrl: "//nufm.dfcfw.com/EM_Finance2014NumericApplication/JS.aspx?type=CT",
                    data: {
                        sty: "e1ii",
                        st: "z",
                        token: "4f1862fc3b5e77c150a2b985b12db0fd"
                    },
                    update: 15e3
                },
                cookie: {
                    disabled: !0,
                    name: "emhqfavor",
                    path: "/",
                    domain: "eastmoney.com",
                    expires: 365
                },
                localStorage: {
                    disabled: !1,
                    key: "myfavor",
                    serializer: JSON.stringify,
                    deserializer: JSON.parse
                },
                displayCount: 5,
                defaultstocks: ["000001-2", "600000-1", "300059-2", "000001-2", "000002-2", "000004-2", "000005-2", "000006-2", "000008-2", "000009-2", "000010-2", "000011-2", "000012-2", "000014-2", "000016-2", "000017-2", "000018-2"],
                onerror: function(e) {
                    console.error(e)
                }
            };
            e.exports = r
        },
        "./ClientApp/modules/jquery-plugins/jquery-actual.js": function(e, t, n) {
            var r = n("jquery");
            r.fn.addBack = r.fn.addBack || r.fn.andSelf,
                r.fn.extend({
                    actual: function(e, t) {
                        if (!this[e]) throw '$.actual => The jQuery method "' + e + '" you called does not exist';
                        var n, a, o = r.extend({
                                absolute: !1,
                                clone: !1,
                                includeMargin: !1,
                                display: "block"
                            },
                            t),
                            i = this.eq(0);
                        if (!0 === o.clone) n = function() {
                            i = i.clone().attr("style", "position: absolute !important; top: -1000 !important; ").appendTo("body")
                        },
                            a = function() {
                                i.remove()
                            };
                        else {
                            var s, l = [],
                                c = "";
                            n = function() {
                                s = i.parents().addBack().filter(":hidden"),
                                    c += "visibility: hidden !important; display: " + o.display + " !important; ",
                                !0 === o.absolute && (c += "position: absolute !important; "),
                                    s.each(function() {
                                        var e = r(this),
                                            t = e.attr("style");
                                        l.push(t),
                                            e.attr("style", t ? t + ";" + c: c)
                                    })
                            },
                                a = function() {
                                    s.each(function(e) {
                                        var t = r(this),
                                            n = l[e];
                                        n === undefined ? t.removeAttr("style") : t.attr("style", n)
                                    })
                                }
                        }
                        n();
                        var u = /(outer)/.test(e) ? i[e](o.includeMargin) : i[e]();
                        return a(),
                            u
                    }
                }),
                e.exports = r.fn.actual
        },
        "./ClientApp/modules/jquery-plugins/jquery.ba-hashchange.js": function(e, t, n) {
            e.exports = function(e, t) {
                function n(e) {
                    return "#" + (e = e || location.href).replace(/^[^#]*#?(.*)$/, "$1")
                }
                var r, a = "hashchange",
                    o = document,
                    i = e.event.special,
                    s = o.documentMode,
                    l = "on" + a in t && (s === undefined || s > 7);
                e.fn[a] = function(e) {
                    return e ? this.on(a, e) : this.trigger(a)
                },
                    e.fn[a].delay = 50,
                    i[a] = e.extend(i[a], {
                        setup: function() {
                            if (l) return ! 1;
                            e(r.start)
                        },
                        teardown: function() {
                            if (l) return ! 1;
                            e(r.stop)
                        }
                    }),
                    r = function() {
                        function r() {
                            var o = n(),
                                s = h(c);
                            o !== c ? (d(c = o, s), e(t).trigger(a)) : s !== c && (location.href = location.href.replace(/#.*/, "") + s),
                                i = setTimeout(r, e.fn[a].delay)
                        }
                        var i, s = {},
                            c = n(),
                            u = function(e) {
                                return e
                            },
                            d = u,
                            h = u;
                        return s.start = function() {
                            i || r()
                        },
                            s.stop = function() {
                                i && clearTimeout(i),
                                    i = undefined
                            },
                        !l &&
                        function() {
                            var t, i;
                            s.start = function() {
                                t || (i = (i = e.fn[a].src) && i + n(), t = e('<iframe tabindex="-1" title="empty"/>').hide().one("load",
                                    function() {
                                        i || d(n()),
                                            r()
                                    }).attr("src", i || "javascript:0").insertAfter("body")[0].contentWindow, o.onpropertychange = function() {
                                    try {
                                        "title" === event.propertyName && (t.document.title = o.title)
                                    } catch(e) {}
                                })
                            },
                                s.stop = u,
                                h = function() {
                                    return n(t.location.href)
                                },
                                d = function(n, r) {
                                    var i = t.document,
                                        s = e.fn[a].domain;
                                    n !== r && (i.title = o.title, i.open(), s && i.write('<script>document.domain="' + s + '"<\/script>'), i.close(), t.location.hash = n)
                                }
                        } (),
                            s
                    } ()
            } (n("jquery"), window)
        },
        "./ClientApp/modules/jquery-plugins/jquery.color.js": function(e, t, n) { (function(e) { !
            function(e, t) {
                function n(e, t, n) {
                    var r = u[t.type] || {};
                    return null == e ? n || !t.def ? null: t.def: (e = r.floor ? ~~e: parseFloat(e), isNaN(e) ? t.def: r.mod ? (e + r.mod) % r.mod: 0 > e ? 0 : r.max < e ? r.max: e)
                }
                function r(t) {
                    var n = l(),
                        r = n._rgba = [];
                    return t = t.toLowerCase(),
                        f(s,
                            function(e, a) {
                                var o, i = a.re.exec(t),
                                    s = i && a.parse(i),
                                    l = a.space || "rgba";
                                if (s) return o = n[l](s),
                                    n[c[l].cache] = o[c[l].cache],
                                    r = n._rgba = o._rgba,
                                    !1
                            }),
                        r.length ? ("0,0,0,0" === r.join() && e.extend(r, o.transparent), n) : o[t]
                }
                function a(e, t, n) {
                    return 6 * (n = (n + 1) % 1) < 1 ? e + (t - e) * n * 6 : 2 * n < 1 ? t: 3 * n < 2 ? e + (t - e) * (2 / 3 - n) * 6 : e
                }
                var o, i = /^([\-+])=\s*(\d+\.?\d*)/,
                    s = [{
                        re: /rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
                        parse: function(e) {
                            return [e[1], e[2], e[3], e[4]]
                        }
                    },
                        {
                            re: /rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
                            parse: function(e) {
                                return [2.55 * e[1], 2.55 * e[2], 2.55 * e[3], e[4]]
                            }
                        },
                        {
                            re: /#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/,
                            parse: function(e) {
                                return [parseInt(e[1], 16), parseInt(e[2], 16), parseInt(e[3], 16)]
                            }
                        },
                        {
                            re: /#([a-f0-9])([a-f0-9])([a-f0-9])/,
                            parse: function(e) {
                                return [parseInt(e[1] + e[1], 16), parseInt(e[2] + e[2], 16), parseInt(e[3] + e[3], 16)]
                            }
                        },
                        {
                            re: /hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
                            space: "hsla",
                            parse: function(e) {
                                return [e[1], e[2] / 100, e[3] / 100, e[4]]
                            }
                        }],
                    l = e.Color = function(t, n, r, a) {
                        return new e.Color.fn.parse(t, n, r, a)
                    },
                    c = {
                        rgba: {
                            props: {
                                red: {
                                    idx: 0,
                                    type: "byte"
                                },
                                green: {
                                    idx: 1,
                                    type: "byte"
                                },
                                blue: {
                                    idx: 2,
                                    type: "byte"
                                }
                            }
                        },
                        hsla: {
                            props: {
                                hue: {
                                    idx: 0,
                                    type: "degrees"
                                },
                                saturation: {
                                    idx: 1,
                                    type: "percent"
                                },
                                lightness: {
                                    idx: 2,
                                    type: "percent"
                                }
                            }
                        }
                    },
                    u = {
                        byte: {
                            floor: !0,
                            max: 255
                        },
                        percent: {
                            max: 1
                        },
                        degrees: {
                            mod: 360,
                            floor: !0
                        }
                    },
                    d = l.support = {},
                    h = e("<p>")[0],
                    f = e.each;
                h.style.cssText = "background-color:rgba(1,1,1,.5)",
                    d.rgba = h.style.backgroundColor.indexOf("rgba") > -1,
                    f(c,
                        function(e, t) {
                            t.cache = "_" + e,
                                t.props.alpha = {
                                    idx: 3,
                                    type: "percent",
                                    def: 1
                                }
                        }),
                    l.fn = e.extend(l.prototype, {
                        parse: function(t, a, i, s) {
                            if (void 0 === t) return this._rgba = [null, null, null, null],
                                this; (t.jquery || t.nodeType) && (t = e(t).css(a), a = void 0);
                            var u = this,
                                d = e.type(t),
                                h = this._rgba = [];
                            return void 0 !== a && (t = [t, a, i, s], d = "array"),
                                "string" === d ? this.parse(r(t) || o._default) : "array" === d ? (f(c.rgba.props,
                                    function(e, r) {
                                        h[r.idx] = n(t[r.idx], r)
                                    }), this) : "object" === d ? (f(c, t instanceof l ?
                                    function(e, n) {
                                        t[n.cache] && (u[n.cache] = t[n.cache].slice())
                                    }: function(r, a) {
                                        var o = a.cache;
                                        f(a.props,
                                            function(e, r) {
                                                if (!u[o] && a.to) {
                                                    if ("alpha" === e || null == t[e]) return;
                                                    u[o] = a.to(u._rgba)
                                                }
                                                u[o][r.idx] = n(t[e], r, !0)
                                            }),
                                        u[o] && e.inArray(null, u[o].slice(0, 3)) < 0 && (u[o][3] = 1, a.from && (u._rgba = a.from(u[o])))
                                    }), this) : void 0
                        },
                        is: function(e) {
                            var t = l(e),
                                n = !0,
                                r = this;
                            return f(c,
                                function(e, a) {
                                    var o, i = t[a.cache];
                                    return i && (o = r[a.cache] || a.to && a.to(r._rgba) || [], f(a.props,
                                        function(e, t) {
                                            if (null != i[t.idx]) return n = i[t.idx] === o[t.idx]
                                        })),
                                        n
                                }),
                                n
                        },
                        _space: function() {
                            var e = [],
                                t = this;
                            return f(c,
                                function(n, r) {
                                    t[r.cache] && e.push(n)
                                }),
                                e.pop()
                        },
                        transition: function(e, t) {
                            var r = l(e),
                                a = r._space(),
                                o = c[a],
                                i = 0 === this.alpha() ? l("transparent") : this,
                                s = i[o.cache] || o.to(i._rgba),
                                d = s.slice();
                            return r = r[o.cache],
                                f(o.props,
                                    function(e, a) {
                                        var o = a.idx,
                                            i = s[o],
                                            l = r[o],
                                            c = u[a.type] || {};
                                        null !== l && (null === i ? d[o] = l: (c.mod && (l - i > c.mod / 2 ? i += c.mod: i - l > c.mod / 2 && (i -= c.mod)), d[o] = n((l - i) * t + i, a)))
                                    }),
                                this[a](d)
                        },
                        blend: function(t) {
                            if (1 === this._rgba[3]) return this;
                            var n = this._rgba.slice(),
                                r = n.pop(),
                                a = l(t)._rgba;
                            return l(e.map(n,
                                function(e, t) {
                                    return (1 - r) * a[t] + r * e
                                }))
                        },
                        toRgbaString: function() {
                            var t = "rgba(",
                                n = e.map(this._rgba,
                                    function(e, t) {
                                        return null == e ? t > 2 ? 1 : 0 : e
                                    });
                            return 1 === n[3] && (n.pop(), t = "rgb("),
                            t + n.join() + ")"
                        },
                        toHslaString: function() {
                            var t = "hsla(",
                                n = e.map(this.hsla(),
                                    function(e, t) {
                                        return null == e && (e = t > 2 ? 1 : 0),
                                        t && t < 3 && (e = Math.round(100 * e) + "%"),
                                            e
                                    });
                            return 1 === n[3] && (n.pop(), t = "hsl("),
                            t + n.join() + ")"
                        },
                        toHexString: function(t) {
                            var n = this._rgba.slice(),
                                r = n.pop();
                            return t && n.push(~~ (255 * r)),
                            "#" + e.map(n,
                                function(e) {
                                    return 1 === (e = (e || 0).toString(16)).length ? "0" + e: e
                                }).join("")
                        },
                        toString: function() {
                            return 0 === this._rgba[3] ? "transparent": this.toRgbaString()
                        }
                    }),
                    l.fn.parse.prototype = l.fn,
                    c.hsla.to = function(e) {
                        if (null == e[0] || null == e[1] || null == e[2]) return [null, null, null, e[3]];
                        var t, n, r = e[0] / 255,
                            a = e[1] / 255,
                            o = e[2] / 255,
                            i = e[3],
                            s = Math.max(r, a, o),
                            l = Math.min(r, a, o),
                            c = s - l,
                            u = s + l,
                            d = .5 * u;
                        return t = l === s ? 0 : r === s ? 60 * (a - o) / c + 360 : a === s ? 60 * (o - r) / c + 120 : 60 * (r - a) / c + 240,
                            n = 0 === c ? 0 : d <= .5 ? c / u: c / (2 - u),
                            [Math.round(t) % 360, n, d, null == i ? 1 : i]
                    },
                    c.hsla.from = function(e) {
                        if (null == e[0] || null == e[1] || null == e[2]) return [null, null, null, e[3]];
                        var t = e[0] / 360,
                            n = e[1],
                            r = e[2],
                            o = e[3],
                            i = r <= .5 ? r * (1 + n) : r + n - r * n,
                            s = 2 * r - i;
                        return [Math.round(255 * a(s, i, t + 1 / 3)), Math.round(255 * a(s, i, t)), Math.round(255 * a(s, i, t - 1 / 3)), o]
                    },
                    f(c,
                        function(t, r) {
                            var a = r.props,
                                o = r.cache,
                                s = r.to,
                                c = r.from;
                            l.fn[t] = function(t) {
                                if (s && !this[o] && (this[o] = s(this._rgba)), void 0 === t) return this[o].slice();
                                var r, i = e.type(t),
                                    u = "array" === i || "object" === i ? t: arguments,
                                    d = this[o].slice();
                                return f(a,
                                    function(e, t) {
                                        var r = u["object" === i ? e: t.idx];
                                        null == r && (r = d[t.idx]),
                                            d[t.idx] = n(r, t)
                                    }),
                                    c ? (r = l(c(d)), r[o] = d, r) : l(d)
                            },
                                f(a,
                                    function(n, r) {
                                        l.fn[n] || (l.fn[n] = function(a) {
                                            var o, s = e.type(a),
                                                l = "alpha" === n ? this._hsla ? "hsla": "rgba": t,
                                                c = this[l](),
                                                u = c[r.idx];
                                            return "undefined" === s ? u: ("function" === s && (a = a.call(this, u), s = e.type(a)), null == a && r.empty ? this: ("string" === s && (o = i.exec(a)) && (a = u + parseFloat(o[2]) * ("+" === o[1] ? 1 : -1)), c[r.idx] = a, this[l](c)))
                                        })
                                    })
                        }),
                    l.hook = function(t) {
                        var n = t.split(" ");
                        f(n,
                            function(t, n) {
                                e.cssHooks[n] = {
                                    set: function(t, a) {
                                        var o, i, s = "";
                                        if ("transparent" !== a && ("string" !== e.type(a) || (o = r(a)))) {
                                            if (a = l(o || a), !d.rgba && 1 !== a._rgba[3]) {
                                                for (i = "backgroundColor" === n ? t.parentNode: t; ("" === s || "transparent" === s) && i && i.style;) try {
                                                    s = e.css(i, "backgroundColor"),
                                                        i = i.parentNode
                                                } catch(c) {}
                                                a = a.blend(s && "transparent" !== s ? s: "_default")
                                            }
                                            a = a.toRgbaString()
                                        }
                                        try {
                                            t.style[n] = a
                                        } catch(c) {}
                                    }
                                },
                                    e.fx.step[n] = function(t) {
                                        t.colorInit || (t.start = l(t.elem, n), t.end = l(t.end), t.colorInit = !0),
                                            e.cssHooks[n].set(t.elem, t.start.transition(t.end, t.pos))
                                    }
                            })
                    },
                    l.hook("backgroundColor borderBottomColor borderLeftColor borderRightColor borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor"),
                    e.cssHooks.borderColor = {
                        expand: function(e) {
                            var t = {};
                            return f(["Top", "Right", "Bottom", "Left"],
                                function(n, r) {
                                    t["border" + r + "Color"] = e
                                }),
                                t
                        }
                    },
                    o = e.Color.names = {
                        aqua: "#00ffff",
                        black: "#000000",
                        blue: "#0000ff",
                        fuchsia: "#ff00ff",
                        gray: "#808080",
                        green: "#008000",
                        lime: "#00ff00",
                        maroon: "#800000",
                        navy: "#000080",
                        olive: "#808000",
                        purple: "#800080",
                        red: "#ff0000",
                        silver: "#c0c0c0",
                        teal: "#008080",
                        white: "#ffffff",
                        yellow: "#ffff00",
                        transparent: [null, null, null, 0],
                        _default: "#ffffff"
                    }
            } (e)
        }).call(t, n("jquery"))},
        "./ClientApp/modules/jquery-plugins/jquery.postfixed.js": function(e, t, n) {
            var r = n("jquery");
            n("./node_modules/lodash.throttle/index.js");
            r.extend(r.fn, {
                posfixed: function(e) {
                    var t = {
                        direction: "top",
                        type: "while",
                        hide: !1,
                        distance: 0
                    };
                    r.extend(t, e),
                    r.support.msie && 6 == r.support.version && (r("html").css("overflow", "hidden"), r("body").css({
                        height: "100%",
                        overflow: "auto"
                    }));
                    var n = this,
                        a = r(n).offset().top,
                        o = r(n).offset().left,
                        i = "object" == typeof t.distance ? t.distance: parseInt(t.distance) || 0;
                    if ("while" == t.type) if (r.support.msie && 6 == r.support.version) r("body").scroll(function(e) {
                        r(n).offset().top - r("body").scrollTop() <= i && (r(n).css("position", "absolute"), r(n).css("top", i + "px"), r(n).css("left", o + "px")),
                        r(n).offset().top <= a && r(n).css("position", "static")
                    });
                    else {
                        var s = r(n).outerHeight(!0);
                        r(n).parent();
                        r(window).scroll(function(e) {
                            var o = r(document).height();
                            if ("top" === t.direction) {
                                r(n).offset().top - r(window).scrollTop() <= i && (r(n).css("position", "fixed"), r(n).css(t.direction, i + "px")),
                                r(n).offset().top <= a && r(n).css("position", "static")
                            } else if ("top-bottom" === t.direction) {
                                var l = r(n).offset(),
                                    c = r(window).scrollTop();
                                if (a + s + i.bottom >= o) return r(n).css("position", "static"),
                                    !0;
                                if (l.top - c <= i.top) {
                                    r(n).css("position", "fixed"),
                                        r(n).css("top", i.top + "px");
                                    var u = o - i.bottom - c - (i.top + s);
                                    u < 0 && r(n).css("top", i.top + u + "px")
                                }
                                r(n).offset().top <= a && r(n).css("position", "static")
                            } else {
                                r(window).height() - r(n).offset().top + r(window).scrollTop() - r(n).outerHeight() <= i && (r(n).css("position", "fixed"), r(n).css(t.direction, i + "px")),
                                r(n).offset().top >= a && r(n).css("position", "static")
                            }
                        })
                    }
                    if ("always" == t.type) if (r.support.msie && 6 == r.support.version) t.hide && r(n).hide(),
                        r("body").scroll(function(e) {
                            r("body").scrollTop() > 300 ? r(n).fadeIn(200) : r(n).fadeOut(200)
                        }),
                        r(n).css("position", "absolute"),
                        r(n).css(t.direction, i + "px"),
                    null != t.tag && (null != t.tag.obj ? "right" == t.tag.direction ? (r(n).css("left", t.tag.obj.offset().left + t.tag.obj.width() + t.tag.distance + "px"), r(window).resize(function() {
                        r(n).css("left", t.obj.tag.offset().left + t.tag.obj.width() + t.tag.distance + "px")
                    })) : (t.tag.obj.offset().left, t.tag.obj.width(), t.tag.distance, r(n).css("left", t.tag.obj.offset().left - r(n).outerWidth() - t.tag.distance + "px"), r(window).resize(function() {
                        r(n).css("left", t.tag.obj.offset().left - r(n).outerWidth() - t.tag.distance + "px")
                    })) : r(n).css(t.tag.direction, t.tag.distance + "px"));
                    else {
                        t.hide && r(n).hide(),
                            r(window).scroll(function(e) {
                                r(window).scrollTop() > 300 ? r(n).fadeIn(200) : r(n).fadeOut(200)
                            });
                        r(n).offset().left;
                        r(n).css("position", "fixed"),
                            r(n).css(t.direction, i + "px"),
                        null != t.tag && (null != t.tag.obj ? "right" == t.tag.direction ? (r(n).css("left", t.tag.obj.offset().left + t.tag.obj.width() + t.tag.distance + "px"), r(window).resize(function() {
                            r(n).css("left", t.obj.tag.offset().left + t.tag.obj.width() + t.tag.distance + "px")
                        })) : (t.tag.obj.offset().left, t.tag.obj.width(), t.tag.distance, r(n).css("left", t.tag.obj.offset().left - r(n).outerWidth() - t.tag.distance + "px"), r(window).resize(function() {
                            r(n).css("left", t.tag.obj.offset().left - r(n).outerWidth() - t.tag.distance + "px")
                        })) : r(n).css(t.tag.direction, t.tag.distance + "px"))
                    }
                }
            }),
                e.exports = r.posfixed
        },
        "./ClientApp/modules/jquery-plugins/jquery.vticker.js": function(e, t, n) {
            e.exports = function(e) {
                e.fn.vTicker = function(t) {
                    t = e.extend({
                            speed: 700,
                            pause: 4e3,
                            showItems: 3,
                            animation: "",
                            mousePause: !0,
                            isPaused: !1,
                            direction: "up",
                            height: 0
                        },
                        t);
                    return moveUp = function(t, n, r) {
                        if (!r.isPaused) {
                            var a = t.children("ul"),
                                o = a.children("li:first").clone(!0);
                            r.height > 0 && (n = a.children("li:first").height()),
                                a.animate({
                                        top: "-=" + n + "px"
                                    },
                                    r.speed,
                                    function() {
                                        e(this).children("li:first").remove(),
                                            e(this).css("top", "0px")
                                    }),
                            "fade" == r.animation && (a.children("li:first").fadeOut(r.speed), 0 == r.height && a.children("li:eq(" + r.showItems + ")").hide().fadeIn(r.speed).show()),
                                o.appendTo(a)
                        }
                    },
                        moveDown = function(t, n, r) {
                            if (!r.isPaused) {
                                var a = t.children("ul"),
                                    o = a.children("li:last").clone(!0);
                                r.height > 0 && (n = a.children("li:first").height()),
                                    a.css("top", "-" + n + "px").prepend(o),
                                    a.animate({
                                            top: 0
                                        },
                                        r.speed,
                                        function() {
                                            e(this).children("li:last").remove()
                                        }),
                                "fade" == r.animation && (0 == r.height && a.children("li:eq(" + r.showItems + ")").fadeOut(r.speed), a.children("li:first").hide().fadeIn(r.speed).show())
                            }
                        },
                        this.each(function() {
                            var n = e(this),
                                r = 0;
                            n.css({
                                overflow: "hidden",
                                position: "relative"
                            }).children("ul").css({
                                position: "absolute",
                                margin: 0,
                                padding: 0
                            }).children("li").css({
                                margin: 0,
                                padding: 0
                            }),
                                0 == t.height ? (n.children("ul").children("li").each(function() {
                                    e(this).height() > r && (r = e(this).height())
                                }), n.children("ul").children("li").each(function() {
                                    e(this).height(r)
                                }), n.height(r * t.showItems)) : n.height(t.height),
                                clearInterval(n.data("vticker-id"));
                            var a = setInterval(function() {
                                    "up" == t.direction ? moveUp(n, r, t) : moveDown(n, r, t)
                                },
                                t.pause);
                            n.data("vticker-id", a),
                            t.mousePause && n.bind("mouseenter",
                                function() {
                                    t.isPaused = !0
                                }).bind("mouseleave",
                                function() {
                                    t.isPaused = !1
                                })
                        })
                }
            } (n("jquery"))
        },
        "./ClientApp/modules/jsonp.js": function(e, t) {
            e.exports = function(e, t, n, r, a) {
                e = e || "",
                    t = t || {},
                    n = n || "",
                    r = r ||
                        function() {};
                if ("object" == typeof t) {
                    for (var o = "",
                             i = function(e) {
                                 var t = [];
                                 for (var n in e) e.hasOwnProperty(n) && e[n] !== undefined && t.push(n);
                                 return t
                             } (t), s = 0; s < i.length; s++) o += encodeURIComponent(i[s]) + "=" + encodeURIComponent(t[i[s]]),
                    s != i.length - 1 && (o += "&");
                    e += o ? "?" + o: ""
                } else "function" == typeof t && (r = n = t);
                "function" == typeof n && (r = n, n = "callback"),
                Date.now || (Date.now = function() {
                    return (new Date).getTime()
                });
                var l = Date.now(),
                    c = "jsonp" + Math.round(l + 1000001 * Math.random());
                window[c] = function(e) {
                    r(e);
                    try {
                        delete window[c]
                    } catch(t) {
                        window[c] = undefined
                    }
                    try {
                        document.getElementById(c).parentNode.removeChild(document.getElementById(c))
                    } catch(a) {
                        throw a
                    }
                },
                    -1 === e.indexOf("?") ? e += "?": e += "&";
                var u = document.createElement("script");
                u.id = c,
                    u.className = "jsonp",
                    u.setAttribute("src", e + n + "=" + c),
                "function" == typeof a && (u.onerror = a),
                    document.getElementsByTagName("head")[0].appendChild(u)
            }
        },
        "./ClientApp/modules/listview/css/listview.css": function(e, t) {},
        "./ClientApp/modules/listview/css/net-fixedheader-dt.css": function(e, t) {},
        "./ClientApp/modules/listview/dataTable.fixedHeader.js": function(e, t, n) {
            var r = n("jquery"),
                a = n("./node_modules/lodash.throttle/index.js");
            r.fn.dataTable || n("./node_modules/datatables.net/js/jquery.dataTables.js")(window, r);
            var o = r.fn.dataTable,
                i = 0,
                s = function(e, t) {
                    if (! (this instanceof s)) throw "FixedHeader must be initialised with the 'new' keyword."; ! 0 === t && (t = {}),
                        e = new o.Api(e),
                        this.c = r.extend(!0, {},
                            s.defaults, t),
                        this.s = {
                            dt: e,
                            position: {
                                theadTop: 0,
                                tbodyTop: 0,
                                tfootTop: 0,
                                tfootBottom: 0,
                                width: 0,
                                left: 0,
                                tfootHeight: 0,
                                theadHeight: 0,
                                windowHeight: r(window).height(),
                                visible: !0
                            },
                            headerMode: null,
                            footerMode: null,
                            autoWidth: e.settings()[0].oFeatures.bAutoWidth,
                            namespace: ".dtfc" + i++,
                            scrollLeft: {
                                header: -1,
                                footer: -1
                            },
                            enable: !0
                        },
                        this.dom = {
                            floatingHeader: null,
                            thead: r(e.table().header()),
                            tbody: r(e.table().body()),
                            tfoot: r(e.table().footer()),
                            header: {
                                host: null,
                                floating: null,
                                placeholder: null
                            },
                            footer: {
                                host: null,
                                floating: null,
                                placeholder: null
                            }
                        },
                        this.dom.header.host = this.dom.thead.parent(),
                        this.dom.footer.host = this.dom.tfoot.parent();
                    var n = e.settings()[0];
                    if (n._fixedHeader) throw "FixedHeader already initialised on table " + n.nTable.id;
                    n._fixedHeader = this,
                        this._constructor()
                };
            r.extend(s.prototype, {
                enable: function(e) {
                    this.s.enable = e,
                    this.c.header && this._modeChange("in-place", "header", !0),
                    this.c.footer && this.dom.tfoot.length && this._modeChange("in-place", "footer", !0),
                        this.update()
                },
                headerOffset: function(e) {
                    return e !== undefined && (this.c.headerOffset = e, this.update()),
                        this.c.headerOffset
                },
                footerOffset: function(e) {
                    return e !== undefined && (this.c.footerOffset = e, this.update()),
                        this.c.footerOffset
                },
                update: function() {
                    this._positions(),
                        this._scroll(!0)
                },
                _constructor: function() {
                    function e(e) {
                        t._scroll()
                    }
                    var t = this,
                        n = this.s.dt;
                    r(window).on("scroll" + this.s.namespace, a ? a(e, 50) : e).on("resize" + this.s.namespace,
                        function() {
                            t.s.position.windowHeight = r(window).height(),
                                t.update()
                        });
                    var o = r(".fh-fixedHeader"); ! this.c.headerOffset && o.length && (this.c.headerOffset = o.outerHeight());
                    var i = r(".fh-fixedFooter"); ! this.c.footerOffset && i.length && (this.c.footerOffset = i.outerHeight()),
                        n.on("column-reorder.dt.dtfc column-visibility.dt.dtfc draw.dt.dtfc column-sizing.dt.dtfc",
                            function() {
                                t.update()
                            }),
                        n.on("destroy.dtfc",
                            function() {
                                n.off(".dtfc"),
                                    r(window).off(t.s.namespace)
                            }),
                        this._positions(),
                        this._scroll()
                },
                _clone: function(e, t) {
                    var n = this.s.dt,
                        a = this.dom[e],
                        o = "header" === e ? this.dom.thead: this.dom.tfoot; ! t && a.floating ? a.floating.removeClass("fixedHeader-floating fixedHeader-locked") : (a.floating && (a.placeholder.remove(), this._unsize(e), a.floating.children().detach(), a.floating.remove()), a.floating = r(n.table().node().cloneNode(!1)).css("table-layout", "fixed").removeAttr("id").append(o).appendTo("body"), a.placeholder = o.clone(!1), a.placeholder.find("*[id]").removeAttr("id"), a.host.prepend(a.placeholder), this._matchWidths(a.placeholder, a.floating))
                },
                _matchWidths: function(e, t) {
                    var n = function(t) {
                            return r(t, e).map(function() {
                                return r(this).width()
                            }).toArray()
                        },
                        a = function(e, n) {
                            r(e, t).each(function(e) {
                                r(this).css({
                                    width: n[e],
                                    minWidth: n[e]
                                })
                            })
                        },
                        o = n("th"),
                        i = n("td");
                    a("th", o),
                        a("td", i)
                },
                _unsize: function(e) {
                    var t = this.dom[e].floating;
                    t && ("footer" === e || "header" === e && !this.s.autoWidth) ? r("th, td", t).css({
                        width: "",
                        minWidth: ""
                    }) : t && "header" === e && r("th, td", t).css("min-width", "")
                },
                _horizontal: function(e, t) {
                    var n = this.dom[e],
                        r = this.s.position,
                        a = this.s.scrollLeft;
                    n.floating && a[e] !== t && (n.floating.css("left", r.left - t), a[e] = t)
                },
                _modeChange: function(e, t, n) {
                    this.s.dt;
                    var a = this.dom[t],
                        o = this.s.position,
                        i = this.dom["footer" === t ? "tfoot": "thead"],
                        s = r.contains(i[0], document.activeElement) ? document.activeElement: null;
                    "in-place" === e ? (a.placeholder && (a.placeholder.remove(), a.placeholder = null), this._unsize(t), "header" === t ? a.host.prepend(this.dom.thead) : a.host.append(this.dom.tfoot), a.floating && (a.floating.remove(), a.floating = null)) : "in" === e ? (this._clone(t, n), a.floating.addClass("fixedHeader-floating").css("header" === t ? "top": "bottom", this.c[t + "Offset"]).css("left", o.left + "px").css("width", o.width + "px"), "footer" === t && a.floating.css("top", "")) : "below" === e ? (this._clone(t, n), a.floating.addClass("fixedHeader-locked").css("top", o.tfootTop - o.theadHeight).css("left", o.left + "px").css("width", o.width + "px")) : "above" === e && (this._clone(t, n), a.floating.addClass("fixedHeader-locked").css("top", o.tbodyTop).css("left", o.left + "px").css("width", o.width + "px")),
                    s && s !== document.activeElement && s.focus(),
                        this.s.scrollLeft.header = -1,
                        this.s.scrollLeft.footer = -1,
                        this.s[t + "Mode"] = e
                },
                _positions: function() {
                    var e = this.s.dt.table(),
                        t = this.s.position,
                        n = this.dom,
                        a = r(e.node()),
                        o = a.children("thead"),
                        i = a.children("tfoot"),
                        s = n.tbody;
                    t.visible = a.is(":visible"),
                        t.width = a.outerWidth(),
                        t.left = a.offset().left,
                        t.theadTop = o.offset().top,
                        t.tbodyTop = s.offset().top,
                        t.theadHeight = t.tbodyTop - t.theadTop,
                        i.length ? (t.tfootTop = i.offset().top, t.tfootBottom = t.tfootTop + i.outerHeight(), t.tfootHeight = t.tfootBottom - t.tfootTop) : (t.tfootTop = t.tbodyTop + s.outerHeight(), t.tfootBottom = t.tfootTop, t.tfootHeight = t.tfootTop)
                },
                _scroll: function(e) {
                    var t, n, a = r(document).scrollTop(),
                        o = r(document).scrollLeft(),
                        i = this.s.position;
                    this.s.enable && (this.c.header && (t = !i.visible || a <= i.theadTop - this.c.headerOffset ? "in-place": a <= i.tfootTop - i.theadHeight - this.c.headerOffset ? "in": "below", (e || t !== this.s.headerMode) && this._modeChange(t, "header", e), this._horizontal("header", o)), this.c.footer && this.dom.tfoot.length && (n = !i.visible || a + i.windowHeight >= i.tfootBottom + this.c.footerOffset ? "in-place": i.windowHeight + a > i.tbodyTop + i.tfootHeight + this.c.footerOffset ? "in": "above", (e || n !== this.s.footerMode) && this._modeChange(n, "footer", e), this._horizontal("footer", o)))
                }
            }),
                s.version = "3.1.3",
                s.defaults = {
                    header: !0,
                    footer: !1,
                    headerOffset: 0,
                    footerOffset: 0
                },
                r.fn.dataTable.FixedHeader = s,
                r.fn.DataTable.FixedHeader = s,
                r(document).on("init.dt.dtfh",
                    function(e, t, n) {
                        if ("dt" === e.namespace) {
                            var a = t.oInit.fixedHeader,
                                i = o.defaults.fixedHeader;
                            if ((a || i) && !t._fixedHeader) {
                                var l = r.extend({},
                                    i, a); ! 1 !== a && new s(t, l)
                            }
                        }
                    }),
                o.Api.register("fixedHeader()",
                    function() {}),
                o.Api.register("fixedHeader.adjust()",
                    function() {
                        return this.iterator("table",
                            function(e) {
                                var t = e._fixedHeader;
                                t && t.update()
                            })
                    }),
                o.Api.register("fixedHeader.enable()",
                    function(e) {
                        return this.iterator("table",
                            function(t) {
                                var n = t._fixedHeader;
                                e = e === undefined || e,
                                n && e !== n.s.enable && n.enable(e)
                            })
                    }),
                o.Api.register("fixedHeader.disable()",
                    function() {
                        return this.iterator("table",
                            function(e) {
                                var t = e._fixedHeader;
                                t && t.s.enable && t.enable(!1)
                            })
                    }),
                r.each(["header", "footer"],
                    function(e, t) {
                        o.Api.register("fixedHeader." + t + "Offset()",
                            function(e) {
                                var n = this.context;
                                return e === undefined ? n.length && n[0]._fixedHeader ? n[0]._fixedHeader[t + "Offset"]() : undefined: this.iterator("table",
                                    function(n) {
                                        var r = n._fixedHeader;
                                        r && r[t + "Offset"](e)
                                    })
                            })
                    }),
                e.exports = s
        },
        "./ClientApp/modules/listview/dataTable.inputPagination.js": function(e, t, n) {
            var r = n("jquery");
            e.exports = function() {
                function e(e) {
                    return Math.ceil(e._iDisplayStart / e._iDisplayLength) + 1
                }
                function t(e) {
                    return Math.ceil(e.fnRecordsDisplay() / e._iDisplayLength)
                }
                var n = "paginate_page";
                r.fn.dataTableExt.oPagination.input = {
                    oDefaults: {
                        iShowPages: 5
                    },
                    fnInit: function(a, o, i) {
                        function s(e) {
                            var t = a._iDisplayLength * (e - 1);
                            t < 0 && (t = 0),
                            t >= a.fnRecordsDisplay() && (t = (Math.ceil(a.fnRecordsDisplay() / a._iDisplayLength) - 1) * a._iDisplayLength),
                                a._iDisplayStart = t,
                                i(a)
                        }
                        var l = document.createElement("span"),
                            c = document.createElement("span"),
                            u = document.createElement("span"),
                            d = document.createElement("span"),
                            h = document.createElement("input"),
                            f = document.createElement("span"),
                            p = document.createElement("span"),
                            m = document.createElement("a"),
                            g = a.oLanguage.oPaginate,
                            v = a.oClasses;
                        l.innerHTML = g.sFirst,
                            c.innerHTML = g.sPrevious,
                            u.innerHTML = g.sNext,
                            d.innerHTML = g.sLast,
                            l.className = "first " + v.sPageButton,
                            c.className = "previous " + v.sPageButton,
                            u.className = "next " + v.sPageButton,
                            d.className = "last " + v.sPageButton,
                            p.className = "paginate_of",
                            f.className = n,
                            m.className = "paginte_go",
                            h.className = "paginate_input",
                        "" !== a.sTableId && (o.setAttribute("id", a.sTableId + "_paginate"), l.setAttribute("id", a.sTableId + "_first"), c.setAttribute("id", a.sTableId + "_previous"), f.setAttribute("id", a.sTableId + "_" + n), u.setAttribute("id", a.sTableId + "_next"), d.setAttribute("id", a.sTableId + "_last")),
                            p.innerText = "  转到",
                            m.innerHTML = "GO",
                            h.type = "text",
                            o.appendChild(c),
                            o.appendChild(f),
                            o.appendChild(u),
                            o.appendChild(p),
                            o.appendChild(h),
                            o.appendChild(m),
                            r(l).click(function() {
                                1 !== e(a) && (a.oApi._fnPageChange(a, "first"), i(a))
                            }),
                            r(c).click(function() {
                                1 !== e(a) && (a.oApi._fnPageChange(a, "previous"), i(a))
                            }),
                            r(u).click(function() {
                                e(a) !== t(a) && (a.oApi._fnPageChange(a, "next"), i(a))
                            }),
                            r(d).click(function() {
                                e(a) !== t(a) && (a.oApi._fnPageChange(a, "last"), i(a))
                            });
                        var b;
                        r(h).keydown(function(e) {
                            var t = this;
                            clearTimeout(b),
                                38 === e.which || 39 === e.which ? (this.value++, b = setTimeout(function() {
                                        s(t.value)
                                    },
                                    200)) : (37 === e.which || 40 === e.which) && this.value > 1 ? (this.value--, b = setTimeout(function() {
                                        s(t.value)
                                    },
                                    200)) : 13 === e.which && s(this.value),
                            ("" === this.value || this.value.match(/[^0-9]/)) && (this.value = this.value.replace(/[^\d]/g, ""))
                        }),
                            r(m).click(function(e) {
                                return s(r(h).val()),
                                    !1
                            }),
                            r("span", o).bind("mousedown",
                                function() {
                                    return ! 1
                                }),
                            r("span", o).bind("selectstart",
                                function() {
                                    return ! 1
                                });
                        t(a) <= 1 && r(o).hide()
                    },
                    fnUpdate: function(a, o) {
                        if (a.aanFeatures.p) {
                            var s = t(a),
                                l = e(a),
                                c = a.aanFeatures.p;
                            if (s <= 1) r(c).hide();
                            else {
                                var u = function(e) {
                                    var t = e._iDisplayStart,
                                        n = e._iDisplayLength,
                                        r = e.fnRecordsDisplay(),
                                        a = -1 === n,
                                        o = a ? 0 : Math.ceil(t / n),
                                        i = a ? 1 : Math.ceil(r / n),
                                        s = o > 0 ? "": e.oClasses.sPageButtonDisabled,
                                        l = o < i - 1 ? "": e.oClasses.sPageButtonDisabled;
                                    return {
                                        first: s,
                                        previous: s,
                                        next: l,
                                        last: l
                                    }
                                } (a);
                                r(c).show();
                                var d = r(c).children("." + n).empty(); (function() {
                                    function e(e, t) {
                                        var n;
                                        return "number" == typeof e ? (n = r('<a class="paginate_button"></a>').html(e).click(function(t) {
                                            return a.oApi._fnPageChange(a, e - 1),
                                                o(a),
                                                !1
                                        }), e === s && n.addClass("current")) : "ellipsis" === e && "number" == typeof t && (n = r('<a class="ellipsis">…</a>').click(function(e) {
                                            return a.oApi._fnPageChange(a, t - 1),
                                                o(a),
                                                !1
                                        })),
                                            n
                                    }
                                    var t = a.oInit.iShowPages || this.oDefaults.iShowPages,
                                        n = Math.floor(t / 2),
                                        s = Math.ceil((a._iDisplayStart + 1) / a._iDisplayLength),
                                        l = Math.ceil(a.fnRecordsDisplay() / a._iDisplayLength),
                                        c = s - n,
                                        u = s + n;
                                    l < t ? (c = 1, u = l) : c < 1 ? (c = 1, u = t) : u > l && (c = l - t + 1, u = l);
                                    var h = [];
                                    if (1 < c && (h.push(e(1)), c > 2)) {
                                        var f = c - t;
                                        h.push(e("ellipsis", f < 1 ? 1 : f))
                                    }
                                    for (i = c; i <= u; i++) h.push(e(i));
                                    if (u < l) {
                                        var p = u + t;
                                        u + 1 < l && h.push(e("ellipsis", p > l ? l: p)),
                                            h.push(e(l))
                                    }
                                    r(d).append(h)
                                }).apply(this),
                                    r(c).children(".first").removeClass(a.oClasses.sPageButtonDisabled).addClass(u.first),
                                    r(c).children(".previous").removeClass(a.oClasses.sPageButtonDisabled).addClass(u.previous),
                                    r(c).children(".next").removeClass(a.oClasses.sPageButtonDisabled).addClass(u.next),
                                    r(c).children(".last").removeClass(a.oClasses.sPageButtonDisabled).addClass(u.last),
                                    r(c).children(".paginate_input").val(l)
                            }
                        }
                    }
                }
            } ()
        },
        "./ClientApp/modules/listview/favorstock-plugin.js": function(e, t, n) { (function(t) {
            function r(e) {
                console.error(e)
            }
            var a = n("./ClientApp/modules/polyfills/json-polyfill.js"),
                o = n("./ClientApp/modules/favouriteclient.js"),
                i = new(n("./ClientApp/modules/utils.cache.js"));
            e.exports = {
                setColumn: function(e, n) {
                    if (0 !== e.indexOf("$favourite")) return ! 1;
                    var r = "Code",
                        a = "MarketType";
                    if (e.indexOf("|") > 0) {
                        var i = e.split("|")[1].trim().split(","),
                            s = i[0].trim(),
                            l = i[1] ? i[1].trim() : null;
                        r = parseInt(s) ? parseInt(s) : s,
                            a = parseInt(l) ? parseInt(l) : l
                    }
                    var c = {
                        name: "favourite-btn",
                        data: function(e, t, n, i) {
                            return new o.stockinfo(e[r], e[a])
                        },
                        title: "加自选",
                        orderable: !1,
                        className: "favourite-stock",
                        render: function(e, t, n, r) {
                            return '<a class="addzx" target="_self" onclick="return false;" href="javascript:void(0);"></a>'
                        }
                    };
                    return t.extend(n, c)
                },
                init: function(e, n) {
                    var s = a.stringify(n),
                        l = i.getOrAdd(s, new o(n));
                    e.on("draw",
                        function() {
                            var n = [];
                            if (! (i = e.column("favourite-btn:name").data())) return ! 1;
                            for (var a = 0; a < i.length; a++) n.push(i[a].id);
                            l.getlist(function(t) {
                                    for (var r = 0; r < t.length; r++) {
                                        var a = t[r],
                                            o = n.indexOf(a);
                                        if (o >= 0) {
                                            e.cells(o, "favourite-btn:name").nodes().toJQuery().find("a").removeClass("addzx delzx").addClass("delzx")
                                        }
                                    }
                                },
                                r);
                            var o = e.column("favourite-btn:name"),
                                i = o.data(),
                                s = o.nodes().toJQuery();
                            if (!s) return ! 1;
                            var c = {};
                            s.click(function(e) {
                                var n = t(this),
                                    r = o.cells(this).data()[0];
                                if (r) {
                                    var a = n.find("a");
                                    return a.hasClass("addzx") ? a.removeClass("addzx").addClass("delzx") : a.removeClass("delzx").addClass("addzx"),
                                        clearTimeout(c[r.id]),
                                        c[r.id] = setTimeout(function() {
                                                l.checkfavor(r) ? l.remove(r) : l.add(r)
                                            },
                                            500),
                                        !1
                                }
                            })
                        })
                }
            }
        }).call(t, n("jquery"))},
        "./ClientApp/modules/listview/main.js": function(e, t, n) {
            function r(e, t, n) {
                var r, h, f = this,
                    p = function(e) {
                        var t;
                        e instanceof a ? t = e: (o.isDOM(e) || "string" == typeof e) && (t = a(e));
                        t && t.length || (t = null);
                        return t
                    } (e);
                if (!p) throw "Cannot find element " + e;
                var m, g = this.namedColumns = {};
                this.setoptions = function(e, t) {
                    m = "string" == typeof e ? d(e, t) : d("default", e)
                },
                    this.load = function() {
                        return this.stop(),
                            this.draw(),
                        m.server.update && m.server.update.enable && m.server.update.interval > 0 && (h = r && r.ajax ? setInterval(function() {
                                r.ajax.reload(null, !1)
                            },
                            m.server.update.interval) : setInterval(f.draw, m.server.update.interval)),
                            r
                    },
                    this.stop = function() {
                        h && clearInterval(h)
                    },
                    this.destroy = function() {
                        this.stop(),
                        r && r.destroy(!1),
                            p.empty()
                    },
                    this.draw = function() {
                        try {
                            0 === t.indexOf("tsq") && p.addClass("hover").on("preInit.dt",
                                function(e, t) {
                                    var n = new a.fn.dataTable.Api(t);
                                    l.init({
                                        settings: m,
                                        table: n
                                    })
                                }),
                            m.datatables.ajax || (m.datatables.ajax = function(e, t) {
                                return "tsq" === t ? l.getajax(e) : function(t, n, r) {
                                    var o, i, l;
                                    if (r.bSorted && t.order.length > 0) {
                                        o = t.order[0];
                                        var c = r.oInit.columns;
                                        i = c[o.column].name || c[o.column].data,
                                            l = "asc" === o.dir ? 1 : -1
                                    }
                                    var u = {
                                        st: i ? "(" + i + ")": undefined,
                                        sr: l,
                                        p: r.oFeatures.bPaginate ? t.start / t.length + 1 : undefined,
                                        ps: r.oFeatures.bPaginate ? t.length: undefined
                                    };
                                    a.extend(!0, e.server.ajax, {
                                        data: u
                                    }),
                                        e.server.ajax.success = function(o) {
                                            var i;
                                            o instanceof Array ? (i = {
                                                data: o,
                                                draw: t.draw
                                            },
                                            "object" == typeof e.server.resolver && (e.server.resolver.path = "data")) : "object" == typeof o && (i = a.extend(!0, {
                                                    draw: t.draw
                                                },
                                                o)),
                                                "string" == typeof e.server.resolver ? i = s(i, {
                                                        type: e.server.resolver
                                                    },
                                                    r) : "object" == typeof e.server.resolver && (i = s(i, e.server.resolver, r)),
                                                i.recordsFiltered = i.recordsFiltered || i.recordsTotal,
                                                n(i || {
                                                    data: []
                                                })
                                        },
                                    r.jqXHR && r.jqXHR.abort();
                                    r.jqXHR = a.ajax(e.server.ajax)
                                }
                            } (m, t.split(".")[0]));
                            for (var e = !1,
                                     n = 0; n < m.datatables.columns.length; n++) {
                                var d = m.datatables.columns[n];
                                if (!d.data && d.name ? d.data = d.name: d.name || (d.name = d.data), d.decimal && (d.data = function(e, t) {
                                    return function(n, r, a, o) {
                                        var i = n[t];
                                        return isNaN(i) ? i: ("number" == typeof e && e > 0 ? i = (i / Math.pow(10, e)).toFixed(e) : "string" == typeof e && (e = n[e], i = (i / Math.pow(10, e)).toFixed(e)), i)
                                    }
                                } (d.decimal, d.data)), d.template) {
                                    var f = d.render;
                                    d.render = function(e, t) {
                                        return function(n, r, a, s) {
                                            var l = o.extend({
                                                    $current: n
                                                },
                                                a),
                                                c = i.render(e, l);
                                            return "function" == typeof t ? t(c, r, a, s) : c
                                        }
                                    } (d.template, f)
                                }
                                if (d.color) {
                                    f = d.render;
                                    d.render = function(e, t) {
                                        return function(n, r, a, s) {
                                            var l = "",
                                                c = {
                                                    $current: n
                                                };
                                            if ("object" == typeof e) if (isNaN(e.comparer)) if ("function" == typeof e.comparer) {
                                                var u = e.comparer(a);
                                                l = o.getColor(u)
                                            } else "string" == typeof e.comparer && (l = o.getColor(i.render(e.comparer, o.extend(c, a))));
                                            else l = o.getColor(e.comparer);
                                            else l = "string" == typeof e ? o.getColor(i.render(e, o.extend(c, a))) : o.getColor(n);
                                            return "function" == typeof t && (n = t(n, r, a, s)),
                                                l ? "<span class='" + l + "'>" + n + "</span>": n
                                        }
                                    } (d.color, f)
                                }
                                if (d.title && "simple" !== t && (d.title = "<span>" + d.title + "</span>", d.title.indexOf("</b>") < 0 && (d.title += '<b class="icon"></b>')), d.className || (d.className = "listview-col-" + (d.name || "1")), d.data && "function" != typeof d.data) if (0 === d.data.indexOf("$num")) d.title = d.title ? d.title: "序号",
                                    d.orderable = "boolean" == typeof d.orderable && d.orderable;
                                else if (0 === d.data.indexOf("$favourite")) {
                                    e = {};
                                    var v = d.data.indexOf("=");
                                    v > 0 && (e = u.parse(d.data.substring(v + 1))),
                                        c.setColumn(d.data, d)
                                }
                                g[d.name] = d
                            }
                            r = p.DataTable(m.datatables),
                            e && c.init(r, e)
                        } catch(b) {
                            console.error(b),
                                clearInterval(h)
                        }
                    },
                    this.setoptions(t, n),
                    this.datatable = this.load()
            }
            var a = n("jquery"),
                o = r.utils = n("./ClientApp/modules/utils.js"),
                i = n("./ClientApp/modules/template-web.js"),
                s = n("./ClientApp/modules/listview/resolveData.js");
            r.DataTables = n("./node_modules/datatables.net/js/jquery.dataTables.js")(window, a);
            var l = n("./ClientApp/modules/listview/topspeedlist.js"),
                c = n("./ClientApp/modules/listview/favorstock-plugin.js");
            n("./node_modules/datatables.net-select/js/dataTables.select.js")(window, a),
                n("./ClientApp/modules/listview/dataTable.inputPagination.js"),
                n("./ClientApp/modules/listview/dataTable.fixedHeader.js");
            var u = n("./ClientApp/modules/polyfills/json-polyfill.js"),
                d = r.defaults = n("./ClientApp/modules/listview/optionConverter.js");
            n("./ClientApp/modules/listview/css/listview.css"),
                n("./ClientApp/modules/listview/css/net-fixedheader-dt.css"),
                a.fn.dataTable.ext.errMode = "throw",
                a.extend(i.defaults.imports, a.extend(d.defaults.imports, o)),
                a.fn.extend({
                    ListView: function(e, t) {
                        var n = a(this);
                        return 0 !== n.length && new r(n, e, t)
                    }
                }),
                e.exports = r
        },
        "./ClientApp/modules/listview/optionConverter.js": function(e, t, n) {
            function r(e, t) {
                var n;
                switch (e) {
                    case "simple":
                        n = function(e) {
                            var t = o.extend(!0, {},
                                d, e),
                                n = o.extend(!0, {},
                                    p, {
                                        auto: !0,
                                        server: {},
                                        datatables: {
                                            dom: '<"listview simple"t>',
                                            stateSave: !1,
                                            paping: !1,
                                            retrieve: !0,
                                            processing: !0,
                                            fixedHeader: !1,
                                            pageLength: e.count
                                        }
                                    });
                            return a(n, t, "simple"),
                                n
                        } (t);
                        break;
                    case "tsq.full":
                    case "full":
                        n = function(e) {
                            var t = o.extend(!0, {},
                                h, e),
                                n = o.extend(!0, {},
                                    p, {
                                        auto: !0,
                                        server: {},
                                        datatables: {
                                            dom: '<"listview full"tp>',
                                            paping: !0,
                                            retrieve: !0,
                                            processing: !0,
                                            fixedHeader: !u && {
                                                header: !0,
                                                headerOffset: o(".top-nav-wrap").height() || 0
                                            },
                                            pageLength: e.count
                                        }
                                    });
                            return a(n, t, "full"),
                                n
                        } (t);
                        break;
                    case "pic":
                        n = function(e) {
                            var t = o.extend(!0, f, e),
                                n = o.extend(!0, {},
                                    p, {
                                        auto: !0,
                                        server: {},
                                        datatables: {
                                            paping: !0,
                                            retrieve: !0,
                                            processing: !0,
                                            pageLength: e.count || 12
                                        }
                                    });
                            return a(n, t, "full"),
                                n.datatables.createdRow = function(e, n, r) {
                                    for (var a = [], i = 0; i < n.length; i++) {
                                        var l = n[i];
                                        l.stamp = +new Date,
                                            o.extend(l, {
                                                img: s.render(t.linkTemplates.img, l),
                                                favor: s.render(t.linkTemplates.favor, l),
                                                quote: s.render(t.linkTemplates.quote, l),
                                                guba: s.render(t.linkTemplates.guba, l)
                                            }),
                                            a.push(l)
                                    }
                                    var u = s.render(c, {
                                        links: a
                                    });
                                    o("td", e).html(u)
                                },
                                n.datatables.headerCallback = function(e, t, n, r, a) {
                                    o(e).hide()
                                },
                                n.datatables.ajax = function(e, r, a) {
                                    var s = {
                                        st: "",
                                        sr: "",
                                        p: a.oFeatures.bPaginate ? e.start / e.length + 1 : undefined,
                                        ps: a.oFeatures.bPaginate ? e.length: undefined
                                    };
                                    t.sort instanceof Array && (s.st = "(" + t.sort[0] + ")", s.sr = t.sort[1] || -1),
                                        o.extend(!0, n.server.ajax, {
                                            data: s
                                        }),
                                        n.server.ajax.success = function(s) {
                                            var l;
                                            if (s instanceof Array ? (l = {
                                                data: s,
                                                draw: e.draw
                                            },
                                            "object" == typeof n.server.resolver && (n.server.resolver.path = "data")) : "object" == typeof s && (l = o.extend(!0, {
                                                    draw: e.draw
                                                },
                                                s)), "string" == typeof n.server.resolver ? l = i(l, {
                                                    type: n.server.resolver
                                                },
                                                a) : "object" == typeof n.server.resolver && (l = i(l, n.server.resolver, a)), l) {
                                                for (var c = [], u = [], d = 0; d < l.data.length; d++) {
                                                    var h = l.data[d];
                                                    d % t.columnsNum == 0 && (u = [], c.push(u)),
                                                        u.push(h)
                                                }
                                                l.data = c,
                                                    r(l)
                                            } else r({
                                                data: []
                                            })
                                        },
                                    a.jqXHR && a.jqXHR.abort(),
                                        a.jqXHR = o.ajax(n.server.ajax)
                                },
                                n
                        } (t);
                        break;
                    case "default":
                    default:
                        n = o.extend({},
                            p, t)
                }
                return n
            }
            function a(e, t, n) {
                if ("boolean" != typeof t.server || t.server) {
                    t.update > 0 ? e.server.update = {
                        enable: !0,
                        interval: t.update
                    }: e.server.update = !1,
                    t.rowId && (e.server.rowId = t.rowId),
                        "undefined" == typeof t.jsonp ? e.server.ajax = {
                            dataType: "jsonp",
                            jsonp: "cb"
                        }: "string" == typeof t.jsonp && (e.server.ajax = {
                            dataType: "jsonp",
                            jsonp: t.jsonp
                        });
                    var r;
                    if ("string" == typeof t.params) {
                        var a = 0 === t.params.indexOf("?") ? t.params: "?" + t.params;
                        r = new l(a).search(!0)
                    } else "object" == typeof t.params && (r = t.params);
                    var i = new l(t.baseurl),
                        s = i.query(!0);
                    if (e.server.ajax.url = i.search("").toString(), e.server.ajax.data = o.extend(!0, s, r), t.mappings && "string" == typeof t.mappings) {
                        var c = "csv",
                            u = "data",
                            d = ",",
                            h = t.mappings.indexOf(":"),
                            f = t.mappings.length,
                            p = {};
                        if (t.mappings.indexOf(":") > 0) {
                            var m = t.mappings.slice(0, h).split("|");
                            c = m[0],
                                u = m[1],
                                d = m[2]
                        }
                        p = new l("?" + t.mappings.slice(h + 1, f)).query(!0),
                            e.server.resolver = {
                                type: c,
                                separator: d || ",",
                                path: u || ("full" === n ? "data": ""),
                                fields: p
                            }
                    } else if ("object" == typeof t.mappings) {
                        var g = t.mappings.type ? t.mappings: {
                            fields: t.mappings
                        };
                        e.server.resolver = o.extend({
                                type: "csv",
                                separator: ",",
                                path: "full" === n ? "data": "",
                                fields: {}
                            },
                            g)
                    }
                } else e.server = !1;
                if (t.fields instanceof Array) {
                    for (var v = e.datatables.columns || [], b = e.datatables.order || [], y = 0; y < t.fields.length; y++) {
                        var _ = t.fields[y],
                            C = o.extend({
                                    orderable: "simple" !== n && "$num" !== _.data
                                },
                                _);
                        if (C.template || "ChangePercent" !== C.name || (C.template = "{{ChangePercent | percentRender}}", C.color = !0), "string" == typeof C.ordering) {
                            var w = C.ordering.split("-"),
                                x = w[0];
                            h = w[1] || b.length;
                            x && (b[h] = [y, x])
                        }
                        for (var S = !1,
                                 T = 0; T < v.length; T++) {
                            var A = v[T];
                            A && A.name && A.name === C.name && (S = !0, o.extend(A, C))
                        }
                        S || v.push(C)
                    }
                    e.datatables.columns = v,
                        e.datatables.order = b
                }
                return t.order instanceof Array && t.order.length > 0 && (e.datatables.order = t.order),
                    e
            }
            var o = n("jquery"),
                i = n("./ClientApp/modules/listview/resolveData.js"),
                s = n("./ClientApp/modules/template-web.js"),
                l = n("./ClientApp/modules/uri/main.js"),
                c = n("./ClientApp/modules/listview/templates/picmode.art"),
                u = "Microsoft Internet Explorer" == navigator.appName && "7." == navigator.appVersion.match(/7./i),
                d = {
                    baseurl: "//nufm.dfcfw.com/EM_Finance2014NumericApplication/JS.aspx?type=CT&token=4f1862fc3b5e77c150a2b985b12db0fd",
                    update: 25e3,
                    count: 5
                },
                h = {
                    baseurl: "//nufm.dfcfw.com/EM_Finance2014NumericApplication/JS.aspx?type=CT&token=4f1862fc3b5e77c150a2b985b12db0fd&js=({data:[(x)],recordsTotal:(tot),recordsFiltered:(tot)})",
                    update: 15e3,
                    count: 20
                },
                f = {
                    baseurl: "//nufm.dfcfw.com/EM_Finance2014NumericApplication/JS.aspx?type=CT&token=4f1862fc3b5e77c150a2b985b12db0fd&sty=ESBFDTC&js=({data:[(x)],recordsFiltered:(tot)})",
                    linkTemplates: {
                        img: "//pifm.eastmoney.com/EM_Finance2014PictureInterface/Index.aspx?imageType=rt&token=44c9d251add88e27b65ed86506f6e5da&id={{Code}}{{MarketType}}&_={{stamp}}",
                        quote: "//quote.eastmoney.com/web/r/{{Code}}{{MarketType }}",
                        guba: "//guba.eastmoney.com/list,{{Code}}.html",
                        favor: "//quote.eastmoney.com/favor/infavor.aspx?code={{Code}}"
                    },
                    fields: [{
                        dataIndex: -1,
                        title: "",
                        orderable: !1
                    }],
                    sort: ["ChangePercent", -1],
                    update: 15e3,
                    columnsNum: 4,
                    count: 12
                },
                p = r.defaults = {
                    server: {
                        update: {
                            enable: !0,
                            interval: 15e3
                        },
                        ajax: {
                            url: "//nufm.dfcfw.com/EM_Finance2014NumericApplication/JS.aspx",
                            data: {
                                type: "CT",
                                token: "4f1862fc3b5e77c150a2b985b12db0fd",
                                js: "({data:[(x)],recordsTotal:(tot)})"
                            },
                            dataType: "jsonp",
                            jsonp: "cb"
                        },
                        resolver: {
                            type: "csv",
                            separator: ",",
                            path: "data"
                        },
                        rowId: "row-{{Code}}{{Market}}"
                    },
                    datatables: {
                        dom: '<"listview"tp>',
                        autoWidth: !1,
                        paping: !0,
                        stateSave: !1,
                        retrieve: !0,
                        responsive: !0,
                        processing: !0,
                        serverSide: !0,
                        pageLength: 30,
                        pagingType: "input",
                        deferRender: !0,
                        fixedHeader: {
                            header: !0,
                            headerOffset: o(".top-nav-wrap").height() || 0
                        },
                        columns: [],
                        language: {
                            sProcessing: "处理中...",
                            sLengthMenu: "显示 _MENU_ 项结果",
                            sZeroRecords: "没有匹配结果",
                            sInfo: "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
                            sInfoEmpty: "显示第 0 至 0 项结果，共 0 项",
                            sInfoFiltered: "(由 _MAX_ 项结果过滤)",
                            sInfoPostFix: "",
                            sSearch: "搜索:",
                            sUrl: "",
                            sEmptyTable: "暂无数据",
                            sLoadingRecords: "载入中...",
                            sInfoThousands: ",",
                            oPaginate: {
                                sFirst: "首页",
                                sPrevious: "上一页",
                                sNext: "下一页",
                                sLast: "末页"
                            },
                            oAria: {
                                sSortAscending: ": 以升序排列此列",
                                sSortDescending: ": 以降序排列此列"
                            }
                        }
                    },
                    imports: {
                        innerMktNumMap: {
                            1 : "sh",
                            0 : "sz"
                        },
                        marketMapping: {
                            1 : "sh",
                            2 : "sz",
                            _TB: "sz"
                        },
                        percentRender: function(e) {
                            return "number" == typeof e ? e + "%": isNaN(e) || e.indexOf("%") === e.length - 1 ? e: e + "%"
                        },
                        decimalHandler: function(e) {
                            return function(t) {
                                return "number" == typeof t && "number" == typeof e && e > 0 ? (t / Math.pow(10, e)).toFixed(e) : t
                            }
                        }
                    }
                };
            e.exports = r
        },
        "./ClientApp/modules/listview/resolveData.js": function(e, t, n) { (function(t) {
            var r = n("./ClientApp/modules/utils.js");
            e.exports = function(e, n, a) {
                if (!e || !n) return ! 1;
                n = t.extend({
                        type: "csv",
                        reserve: !0
                    },
                    n);
                var o = r.extend({},
                    e, !0);
                switch (n.type.toLowerCase()) {
                    case "csv":
                        if (n.path && (o = e[n.path]), o instanceof Array) {
                            for (var i = [], s = 0; s < o.length; s++) if ("string" == typeof o[s]) {
                                var l = o[s].split(n.separator);
                                if ("object" == typeof n.fields) {
                                    var c = {};
                                    for (var u in n.fields) c[n.fields[u]] = l[u];
                                    l = c
                                }
                                l.$num = s + a._iDisplayStart + 1,
                                    i.push(l)
                            }
                            n.path ? e[n.path] = i: e = i
                        }
                        break;
                    case "json":
                        if (n.path && (o = e[n.path]), o instanceof Array) for (s = 0; s < o.length; s++) if ("object" == typeof n.fields) {
                            for (var u in n.fields) o[s][n.fields[u]] = o[s][u],
                            n.reserve || delete o[s][u];
                            o[s].$num = s + a._iDisplayStart + 1
                        }
                }
                return e
            }
        }).call(t, n("jquery"))},
        "./ClientApp/modules/listview/templates/picmode.art": function(e, t) {e.exports = '<ul class="mod-pic-ul">\r\n    {{each links}}\r\n    <li id="pic-{{$value.ID}}">\r\n        <a target="_blank" href="{{$value.quote}}" class="stock">\r\n            <img width="200" height="139" src="{{$value.img}}" title="{{$value.Name}}({{$value.Code}})" />\r\n        </a>\r\n        <div class="buttons">\r\n            <a class="btn-plus" target="_blank" href="{{$value.favor}}" title="加自选"></a>\r\n            <a class="btn-bigimg" target="_blank" href="{{$value.quote}}" title="看大图"></a>\r\n            <a class="btn-guba" target="_blank" href="{{$value.guba}}" title="进股吧"></a>\r\n        </div>\r\n    </li>\r\n    {{/each}}\r\n</ul>'},
        "./ClientApp/modules/listview/topspeedlist.js": function(e, t, n) {
            var r = n("jquery"),
                a = n("./ClientApp/modules/template-web.js"),
                o = n("./ClientApp/modules/uri/main.js"),
                i = n("./ClientApp/modules/topspeedclient/ClientManager.js"),
                s = n("./ClientApp/modules/utils.extend.js");
            n("./ClientApp/modules/jquery-plugins/jquery.color.js");
            var l, c = i.TSQListClient.response;
            e.exports = {
                init: function(e) {
                    var t = a.compile(e.settings.server.rowId),
                        n = e.settings,
                        u = function(e) {
                            var t = {};
                            if (! (e instanceof Array)) return t;
                            for (var n = 0; n < e.length; n++) {
                                var r = e[n];
                                r.data && (t[r.data] = r)
                            }
                            return t
                        } (n.datatables.columns),
                        d = e.table,
                        h = d.settings()[0];
                    l = new i.TSQListClient({
                        host: new o(n.server.ajax.url).host(),
                        enableMutiDomain: !1,
                        delay: 1e4,
                        errorRetry: 10,
                        onerror: function(e) {},
                        onmessage: function(e) {
                            var n = e instanceof c ? e: new c(e),
                                a = n.getMovedKeys();
                            if (targets = [], a.length > 0) {
                                for (var o = l.datacache.diff,
                                         i = 0; i < a.length; i++) {
                                    var f = parseInt(a[i]);
                                    targets.push(f),
                                        (y = s({},
                                            o[f], !0)).$num = h._iDisplayStart + f + 1,
                                        y.DT_RowId = t(y),
                                        (m = d.row(f)).invalidate().data(y)
                                }
                                n.mv = {}
                            }
                            for (var p in n.diff) {
                                var m;
                                if (i = parseInt(p), (m = l.datacache.diff[p]) && "number" == typeof i && !(targets.indexOf(i) >= 0)) {
                                    for (var g in m) if (u.hasOwnProperty(g)) {
                                        var v = u[g].name,
                                            b = d.cell(i, v + ":name"),
                                            y = m[g];
                                        b.data() != y && (b.invalidate().data(y),
                                            function(e) {
                                                r(e).stop().animate({
                                                        backgroundColor: "#e7eeff"
                                                    },
                                                    {
                                                        duration: "fast",
                                                        always: function() {
                                                            r(e).animate({
                                                                    "background-color": "transparent"
                                                                },
                                                                "slow")
                                                        }
                                                    })
                                            } (b.node()))
                                    }
                                    var _ = d.row(i),
                                        C = _.id(),
                                        w = t(m);
                                    C !== w && r(_.node()).attr("id", w)
                                }
                            }
                        }
                    })
                },
                getajax: function(e) {
                    var t;
                    return function(n, r, o) {
                        var c, u = s({},
                            e.server.ajax.data, !0);
                        if (n.order instanceof Array && n.order.length > 0) {
                            c = n.order[0];
                            var d = o.oInit.columns[c.column].data;
                            u.orderField = d,
                                u.orderType = "asc" === c.dir ? 0 : 1
                        }
                        u.pageIndex = o.oFeatures.bPaginate ? n.start: 0,
                            u.pageSize = o.oFeatures.bPaginate ? n.length: 20;
                        var h = new i.TSQListClient.request(u),
                            f = h.toString();
                        if (f === t) {
                            var p = l.datacache;
                            r({
                                draw: n.draw,
                                data: p.diff,
                                recordsTotal: p.total,
                                recordsFiltered: p.total
                            })
                        } else t = f,
                            l.open(h,
                                function(t) {
                                    var s = new i.TSQListClient.response(t),
                                        l = [];
                                    for (var c in s.diff) {
                                        var u = parseInt(c),
                                            d = s.diff[c];
                                        d && "number" == typeof u && (d.$num = u + o._iDisplayStart + 1, d.DT_RowId = a.render(e.server.rowId, d), l[u] = d)
                                    }
                                    r({
                                        draw: n.draw,
                                        data: l,
                                        recordsTotal: s.total,
                                        recordsFiltered: s.total
                                    })
                                })
                    }
                }
            }
        },
        "./ClientApp/modules/polyfills/json-polyfill.js": function(module, exports) {
            var define = !1;
            window.JSON || (window.JSON = {
                parse: function(sJSON) {
                    return eval("(" + sJSON + ")")
                },
                stringify: function() {
                    var e = Object.prototype.toString,
                        t = Array.isArray ||
                            function(t) {
                                return "[object Array]" === e.call(t)
                            },
                        n = {
                            '"': '\\"',
                            "\\": "\\\\",
                            "\b": "\\b",
                            "\f": "\\f",
                            "\n": "\\n",
                            "\r": "\\r",
                            "\t": "\\t"
                        },
                        r = function(e) {
                            return n[e] || "\\u" + (e.charCodeAt(0) + 65536).toString(16).substr(1)
                        },
                        a = /[\\"\u0000-\u001F\u2028\u2029]/g;
                    return function o(n) {
                        if (null == n) return "null";
                        if ("number" == typeof n) return isFinite(n) ? n.toString() : "null";
                        if ("boolean" == typeof n) return n.toString();
                        if ("object" == typeof n) {
                            if ("function" == typeof n.toJSON) return o(n.toJSON());
                            if (t(n)) {
                                for (var i = "[",
                                         s = 0; s < n.length; s++) i += (s ? ", ": "") + o(n[s]);
                                return i + "]"
                            }
                            if ("[object Object]" === e.call(n)) {
                                var l = [];
                                for (var c in n) n.hasOwnProperty(c) && l.push(o(c) + ": " + o(n[c]));
                                return "{" + l.join(", ") + "}"
                            }
                        }
                        return '"' + n.toString().replace(a, r) + '"'
                    }
                } ()
            }),
                module.exports = window.JSON
        },
        "./ClientApp/modules/polyfills/mini-polyfill.js": function(e, t) {
            var n = function() {
                function e(c, u) {
                    u = u || document;
                    if (!/^[\w\-_#]+$/.test(c) && u.querySelectorAll) return t(u.querySelectorAll(c));
                    if (c.indexOf(",") > -1) {
                        for (var d = c.split(/,/g), h = [], f = 0, p = d.length; f < p; ++f) h = h.concat(e(d[f], u));
                        return l(h)
                    }
                    var m, g = c.match(r),
                        v = g.pop(),
                        b = (v.match(o) || s)[1],
                        y = !b && (v.match(a) || s)[1],
                        _ = !b && (v.match(i) || s)[1];
                    if (y && !_ && u.getElementsByClassName) m = t(u.getElementsByClassName(y));
                    else if (m = !b && t(u.getElementsByTagName(_ || "*")), y && (m = function(e, t, n) {
                        var r, a = -1,
                            o = -1,
                            i = [];
                        for (; r = e[++a];) n.test(r[t]) && (i[++o] = r);
                        return i
                    } (m, "className", RegExp("(^|\\s)" + y + "(\\s|$)"))), b) {
                        var C = u.getElementById(b);
                        return C ? [C] : []
                    }
                    return g[0] && m[0] ? n(g, m) : m
                }
                function t(e) {
                    try {
                        return Array.prototype.slice.call(e)
                    } catch(a) {
                        for (var t = [], n = 0, r = e.length; n < r; ++n) t[n] = e[n];
                        return t
                    }
                }
                function n(e, t, r) {
                    var l = e.pop();
                    if (">" === l) return n(e, t, !0);
                    var c, u, d, h = [],
                        f = -1,
                        p = (l.match(o) || s)[1],
                        m = !p && (l.match(a) || s)[1],
                        g = !p && (l.match(i) || s)[1],
                        v = -1;
                    for (g = g && g.toLowerCase(); c = t[++v];) {
                        u = c.parentNode;
                        do {
                            if (d = !g || "*" === g || g === u.nodeName.toLowerCase(), d = d && (!p || u.id === p), d = d && (!m || RegExp("(^|\\s)" + m + "(\\s|$)").test(u.className)), r || d) break
                        } while ( u = u . parentNode );
                        d && (h[++f] = c)
                    }
                    return e[0] && h[0] ? n(e, h) : h
                }
                var r = /(?:[\w\-\\.#]+)+(?:\[\w+?=([\'"])?(?:\\\1|.)+?\1\])?|\*|>/gi,
                    a = /^(?:[\w\-_]+)?\.([\w\-_]+)/,
                    o = /^(?:[\w\-_]+)?#([\w\-_]+)/,
                    i = /^([\w\*\-_]+)/,
                    s = [null, null],
                    l = function() {
                        var e = +new Date,
                            t = function() {
                                var t = 1;
                                return function(n) {
                                    var r = n[e],
                                        a = t++;
                                    return ! r && (n[e] = a, !0)
                                }
                            } ();
                        return function(n) {
                            for (var r, a = n.length,
                                     o = [], i = -1, s = 0; s < a; ++s) r = n[s],
                            t(r) && (o[++i] = r);
                            return e += 1,
                                o
                        }
                    } ();
                return e
            } ();
            e.exports = n
        },
        "./ClientApp/modules/polyfills/requestAnimationFrame-polyfill.js": function(e, t) {
            e.exports = function() {
                for (var e = 0,
                         t = ["ms", "moz", "webkit", "o"], n = 0; n < t.length && !window.requestAnimationFrame; ++n) window.requestAnimationFrame = window[t[n] + "RequestAnimationFrame"],
                    window.cancelAnimationFrame = window[t[n] + "CancelAnimationFrame"] || window[t[n] + "CancelRequestAnimationFrame"];
                window.requestAnimationFrame || (window.requestAnimationFrame = function(t, n) {
                    var r = (new Date).getTime(),
                        a = Math.max(0, 16 - (r - e)),
                        o = window.setTimeout(function() {
                                t(r + a)
                            },
                            a);
                    return e = r + a,
                        o
                }),
                window.cancelAnimationFrame || (window.cancelAnimationFrame = function(e) {
                    clearTimeout(e)
                })
            } ()
        },
        "./ClientApp/modules/qcsroll.js": function(e, t, n) {
            n("./ClientApp/modules/polyfills/requestAnimationFrame-polyfill.js"),
                e.exports = function(e) {
                    var t = 0,
                        n = document.createElement("div"),
                        r = document.createElement("div"),
                        a = this;
                    a.element = e,
                        a.run = !0,
                        r.innerHTML = e.innerHTML,
                        r.style.cssText = "float: left; overflow: hidden; zoom: 1;",
                        n.appendChild(r),
                        e.innerHTML = "",
                        e.appendChild(n),
                        n.style.cssText = "position: absolute !important; white-space:nowrap !important;visibility: hidden !important; left: 0",
                        t = n.offsetWidth,
                        n.style.cssText = "",
                        n.style.width = t + "px",
                        n.onmouseover = function() {
                            a.run = !1
                        },
                        n.onmouseout = function() {
                            a.run = !0,
                                clearTimeout(a.timer),
                                a.timer = setTimeout(function() {
                                        cancelAnimationFrame(a.animateId),
                                            a.animate()
                                    },
                                    200)
                        },
                        this.animate = function() {
                            var t = this;
                            if (this.run) return e.scrollLeft++,
                            e.scrollLeft + e.clientWidth >= e.scrollWidth && (e.scrollLeft = 0),
                                clearTimeout(t.timer),
                                t.animateId = requestAnimationFrame(function() {
                                    t.animate()
                                }),
                                t.animateId
                        },
                        this.cancel = function(e) {
                            cancelAnimationFrame(e || a.animateId)
                        }
                }
        },
        "./ClientApp/modules/quotecharts.js": function(e, t, n) {
            function r(e, t) {
                function n() {
                    this.inited || "function" != typeof f.start || f.start(),
                        "function" == typeof this.dataloader ? this.dataloader.apply(this, [f]) : "function" == typeof f.draw && f.draw()
                }
                function r(e, n) {
                    switch (e) {
                        case "compatible":
                            return function(e) {
                                var t = e.container;
                                "string" == typeof e.container && (t = l(e.container)[0]);
                                var n = this.args = o({
                                        url: "//pifm.eastmoney.com/EM_Finance2014PictureInterface/Index.aspx",
                                        cache: !1,
                                        success: function(e) {
                                            t && (t.innerHTML = "", t.appendChild(e))
                                        },
                                        update: 1e4
                                    },
                                    e);
                                return n.update = undefined,
                                    n.container = undefined,
                                    s.imgLoader(n)
                            }.apply(this, [t]);
                        case "time":
                            return function(e) {
                                var t = this,
                                    n = this.args = o({
                                            entry: {},
                                            container: "#chart-container",
                                            width: 720,
                                            height: 655,
                                            type: "r",
                                            iscr: !1,
                                            color: {
                                                line: "#326fb2",
                                                fill: ["rgba(101,202,254, 0.2)", "rgba(101,202,254, 0.1)"]
                                            },
                                            data: {
                                                time: [],
                                                positionChanges: []
                                            },
                                            show: {
                                                indicatorArea: !1
                                            },
                                            onClickChanges: function() {
                                                window.open("//quote.eastmoney.com/changes/stocks/" + e.entry.shortmarket + e.entry.code + ".html")
                                            },
                                            onComplete: function() {},
                                            onError: function(e) {
                                                console.error(e)
                                            },
                                            update: 4e4
                                        },
                                        e),
                                    r = new a.time(n);
                                return this.dataloader = function() {
                                    this.datacache || (this.datacache = {
                                        time: {},
                                        positionChanges: []
                                    }),
                                        i(c, {
                                                id: e.entry.id,
                                                type: e.type,
                                                iscr: e.iscr
                                            },
                                            "cb",
                                            function(n) {
                                                if (r.stop(), !n || !1 === n.stats) return ! 1;
                                                t.datacache.time = n,
                                                    r.setData(t.datacache),
                                                    i(u, {
                                                            id: e.entry.id
                                                        },
                                                        "cb",
                                                        function(e) {
                                                            return !! e && "string" == typeof e[0] && (t.datacache.positionChanges = e, r.setData(t.datacache), void(t.inited ? r.redraw() : r.draw()))
                                                        },
                                                        function() {}),
                                                    i(d, {
                                                            id: e.entry.id
                                                        },
                                                        "cb",
                                                        function(e) {
                                                            if (!e) return ! 1;
                                                            0 != e.stats && (t.datacache.datak = e, r.setData(t.datacache), t.inited ? r.redraw() : r.draw())
                                                        },
                                                        function() {}),
                                                    t.inited ? r.redraw() : (r.draw(), t.inited = !0)
                                            },
                                            function(e) {
                                                console.error(e)
                                            })
                                },
                                    r
                            }.apply(this, [t]);
                        case "k":
                            return function(e) {
                                var t = this.args = o({
                                        entry: {},
                                        container: "#chart-container",
                                        width: 720,
                                        height: 655,
                                        padding: {
                                            top: 0,
                                            bottom: 0
                                        },
                                        scale: {
                                            pillar: 60,
                                            min: 10
                                        },
                                        popWin: {
                                            type: "move"
                                        },
                                        yAxisType: 1,
                                        maxin: {
                                            lineWidth: 30,
                                            skewx: 0,
                                            skewy: 0
                                        },
                                        data: {
                                            k: []
                                        },
                                        onComplete: function() {},
                                        onClick: function() {},
                                        onError: function(e) {
                                            console.error(e)
                                        },
                                        update: 6e4
                                    },
                                    e),
                                    n = new a.k(t);
                                return this.dataloader = function() {
                                    i(c, {
                                            id: e.entry.id,
                                            type: e.type,
                                            authorityType: e.authorityType
                                        },
                                        "cb",
                                        function(e) {
                                            if (n.stop(), !e || !1 === e.stats) return ! 1;
                                            n.setData({
                                                    k: e
                                                },
                                                t),
                                                self.inited ? n.redraw() : n.draw()
                                        },
                                        function(e) {})
                                },
                                    n
                            }.apply(this, [t]);
                        default:
                            return "function" == typeof n[e] ? new n[e](t) : null
                    }
                }
                var h, f, p = this;
                this.args = t,
                    this.inited = !1,
                    this.datacache = !1,
                    this.dataloader = null,
                    this.load = function() {
                        return this.inited = !1,
                            f = this.create(),
                            n.apply(this),
                            this.reload(),
                            this.inited = !0,
                            f
                    },
                    this.create = function() {
                        return r.apply(this, [e, a])
                    },
                    this.reload = function() {
                        return this.stop(),
                        this.args.update > 0 && (h = setInterval(function() {
                                n.apply(p)
                            },
                            this.args.update)),
                            this
                    },
                    this.stop = function() {
                        return clearInterval(h),
                            this
                    }
            }
            var a, o = n("./ClientApp/modules/utils.extend.js"),
                i = n("./ClientApp/modules/jsonp.js"),
                s = n("./ClientApp/modules/asyncloaders.js"),
                l = n("./ClientApp/modules/polyfills/mini-polyfill.js"),
                c = "//pdfm.eastmoney.com/EM_UBG_PDTI_Fast/api/js?rtntype=5&token=4f1862fc3b5e77c150a2b985b12db0fd",
                u = "//nuyd.eastmoney.com/EM_UBG_PositionChangesInterface/api/js?style=top&js=([(x)])&ac=normal&check=itntcd",
                d = "//pdfm.eastmoney.com/EM_UBG_PDTI_Fast/api/js?type=rk&rtntype=5&isCR=false&token=4f1862fc3b5e77c150a2b985b12db0fd",
                h = "production" === environment ? "//emcharts.dfcfw.com/ec/3.8.7/emcharts.min.js": "//172.16.58.95/emchart_test/EMCharts3/bundle/emcharts.js";
            r.preload = function(e, t) {
                if ("function" == typeof a) return "function" == typeof e && e(a),
                    a;
                try {
                    s.scriptLoader({
                        id: "emcharts3-script",
                        url: h,
                        success: function() {
                            a = n("emcharts3"),
                            "function" == typeof e && e.call(a)
                        }
                    })
                } catch(r) {
                    console.error(r)
                }
            },
                e.exports = r
        },
        "./ClientApp/modules/template-web.js": function(e, t, n) { (function(t) { !
            function(t, n) {
                e.exports = n()
            } (0,
                function() {
                    return function(e) {
                        function t(r) {
                            if (n[r]) return n[r].exports;
                            var a = n[r] = {
                                i: r,
                                l: !1,
                                exports: {}
                            };
                            return e[r].call(a.exports, a, a.exports, t),
                                a.l = !0,
                                a.exports
                        }
                        var n = {};
                        return t.m = e,
                            t.c = n,
                            t.d = function(e, n, r) {
                                t.o(e, n) || Object.defineProperty(e, n, {
                                    configurable: !1,
                                    enumerable: !0,
                                    get: r
                                })
                            },
                            t.n = function(e) {
                                var n = e && e.__esModule ?
                                    function() {
                                        return e["default"]
                                    }: function() {
                                        return e
                                    };
                                return t.d(n, "a", n),
                                    n
                            },
                            t.o = function(e, t) {
                                return Object.prototype.hasOwnProperty.call(e, t)
                            },
                            t.p = "",
                            t(t.s = 6)
                    } ([function(e, t, n) { (function(t) {
                        e.exports = !1;
                        try {
                            e.exports = "[object process]" === Object.prototype.toString.call(t.process)
                        } catch(n) {}
                    }).call(t, n(4))
                    },
                        function(e, t, n) {
                            "use strict";
                            var r = n(8),
                                a = n(3),
                                o = n(23),
                                i = function(e, t) {
                                    t.onerror(e, t);
                                    var n = function() {
                                        return "{Template Error}"
                                    };
                                    return n.mappings = [],
                                        n.sourcesContent = [],
                                        n
                                },
                                s = function l(e) {
                                    var t = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
                                    "string" != typeof e ? t = e: t.source = e,
                                        e = (t = a.$extend(t)).source,
                                    !0 === t.debug && (t.cache = !1, t.minimize = !1, t.compileDebug = !0),
                                    t.compileDebug && (t.minimize = !1),
                                    t.filename && (t.filename = t.resolveFilename(t.filename, t));
                                    var n = t.filename,
                                        s = t.cache,
                                        c = t.caches;
                                    if (s && n) {
                                        var u = c.get(n);
                                        if (u) return u
                                    }
                                    if (!e) try {
                                        e = t.loader(n, t),
                                            t.source = e
                                    } catch(m) {
                                        var d = new o({
                                            name: "CompileError",
                                            path: n,
                                            message: "template not found: " + m.message,
                                            stack: m.stack
                                        });
                                        if (t.bail) throw d;
                                        return i(d, t)
                                    }
                                    var h = void 0,
                                        f = new r(t);
                                    try {
                                        h = f.build()
                                    } catch(d) {
                                        if (d = new o(d), t.bail) throw d;
                                        return i(d, t)
                                    }
                                    var p = function(e, n) {
                                        try {
                                            return h(e, n)
                                        } catch(d) {
                                            if (!t.compileDebug) return t.cache = !1,
                                                t.compileDebug = !0,
                                                l(t)(e, n);
                                            if (d = new o(d), t.bail) throw d;
                                            return i(d, t)()
                                        }
                                    };
                                    return p.mappings = h.mappings,
                                        p.sourcesContent = h.sourcesContent,
                                        p.toString = function() {
                                            return h.toString()
                                        },
                                    s && n && c.set(n, p),
                                        p
                                };
                            s.Compiler = r,
                                e.exports = s
                        },
                        function(e, t) {
                            Object.defineProperty(t, "__esModule", {
                                value: !0
                            }),
                                t["default"] = /((['"])(?:(?!\2|\\).|\\(?:\r\n|[\s\S]))*(\2)?|`(?:[^`\\$]|\\[\s\S]|\$(?!\{)|\$\{(?:[^{}]|\{[^}]*\}?)*\}?)*(`)?)|(\/\/.*)|(\/\*(?:[^*]|\*(?!\/))*(\*\/)?)|(\/(?!\*)(?:\[(?:(?![\]\\]).|\\.)*\]|(?![\/\]\\]).|\\.)+\/(?:(?!\s*(?:\b|[\u0080-\uFFFF$\\'"~({]|[+\-!](?!=)|\.?\d))|[gmiyu]{1,5}\b(?![\u0080-\uFFFF$\\]|\s*(?:[+\-*%&|^<>!=?({]|\/(?![\/*])))))|(0[xX][\da-fA-F]+|0[oO][0-7]+|0[bB][01]+|(?:\d*\.\d+|\d+\.?)(?:[eE][+-]?\d+)?)|((?!\d)(?:(?!\s)[$\w\u0080-\uFFFF]|\\u[\da-fA-F]{4}|\\u\{[\da-fA-F]+\})+)|(--|\+\+|&&|\|\||=>|\.{3}|(?:[+\-\/%&|^]|\*{1,2}|<{1,2}|>{1,3}|!=?|={1,2})=?|[?~.,:;[\](){}])|(\s+)|(^$|[\s\S])/g,
                                t.matchToToken = function(e) {
                                    var t = {
                                        type: "invalid",
                                        value: e[0]
                                    };
                                    return e[1] ? (t.type = "string", t.closed = !(!e[3] && !e[4])) : e[5] ? t.type = "comment": e[6] ? (t.type = "comment", t.closed = !!e[7]) : e[8] ? t.type = "regex": e[9] ? t.type = "number": e[10] ? t.type = "name": e[11] ? t.type = "punctuator": e[12] && (t.type = "whitespace"),
                                        t
                                }
                        },
                        function(e, n, r) {
                            "use strict";
                            function a() {
                                this.$extend = function(e) {
                                    return e = e || {},
                                        s(e, e instanceof a ? e: this)
                                }
                            }
                            var o = r(0),
                                i = r(12),
                                s = r(13),
                                l = r(14),
                                c = r(15),
                                u = r(16),
                                d = r(17),
                                h = r(18),
                                f = r(19),
                                p = r(20),
                                m = r(22),
                                g = {
                                    source: null,
                                    filename: null,
                                    rules: [f, h],
                                    escape: !0,
                                    debug: !!o && "production" !== t.env.NODE_ENV,
                                    bail: !0,
                                    cache: !0,
                                    minimize: !0,
                                    compileDebug: !1,
                                    resolveFilename: m,
                                    include: l,
                                    htmlMinifier: p,
                                    htmlMinifierOptions: {
                                        collapseWhitespace: !0,
                                        minifyCSS: !0,
                                        minifyJS: !0,
                                        ignoreCustomFragments: []
                                    },
                                    onerror: c,
                                    loader: d,
                                    caches: u,
                                    root: "/",
                                    extname: ".art",
                                    ignore: [],
                                    imports: i
                                };
                            a.prototype = g,
                                e.exports = new a
                        },
                        function(e, t) {
                            var n;
                            n = function() {
                                return this
                            } ();
                            try {
                                n = n || Function("return this")() || (0, eval)("this")
                            } catch(r) {
                                "object" == typeof window && (n = window)
                            }
                            e.exports = n
                        },
                        function(e, t) {},
                        function(e, t, n) {
                            "use strict";
                            var r = n(7),
                                a = n(1),
                                o = n(24),
                                i = function(e, t) {
                                    return t instanceof Object ? r({
                                            filename: e
                                        },
                                        t) : a({
                                        filename: e,
                                        source: t
                                    })
                                };
                            i.render = r,
                                i.compile = a,
                                i.defaults = o,
                                e.exports = i
                        },
                        function(e, t, n) {
                            "use strict";
                            var r = n(1);
                            e.exports = function(e, t, n) {
                                return r(e, n)(t)
                            }
                        },
                        function(e, t, n) {
                            "use strict";
                            var r = n(9),
                                a = n(11),
                                o = "$data",
                                i = "$imports",
                                s = "print",
                                l = "include",
                                c = "extend",
                                u = "block",
                                d = "$$out",
                                h = "$$line",
                                f = "$$blocks",
                                p = "$$slice",
                                m = "$$from",
                                g = "$$options",
                                v = function(e, t) {
                                    return Object.hasOwnProperty.call(e, t)
                                },
                                b = JSON.stringify,
                                y = function() {
                                    function e(t) {
                                        var n, r, v = this; !
                                            function(e, t) {
                                                if (! (e instanceof t)) throw new TypeError("Cannot call a class as a function")
                                            } (this, e);
                                        var b = t.source,
                                            y = t.minimize,
                                            _ = t.htmlMinifier;
                                        if (this.options = t, this.stacks = [], this.context = [], this.scripts = [], this.CONTEXT_MAP = {},
                                            this.ignore = [o, i, g].concat(t.ignore), this.internal = (n = {},
                                            n[d] = "''", n[h] = "[0,0]", n[f] = "arguments[1]||{}", n[m] = "null", n[s] = "function(){var s=''.concat.apply('',arguments);" + d + "+=s;return s}", n[l] = "function(src,data){var s=" + g + ".include(src,data||" + o + ",arguments[2]||" + f + "," + g + ");" + d + "+=s;return s}", n[c] = "function(from){" + m + "=from}", n[p] = "function(c,p,s){p=" + d + ";" + d + "='';c();s=" + d + ";" + d + "=p+s;return s}", n[u] = "function(){var a=arguments,s;if(typeof a[0]==='function'){return " + p + "(a[0])}else if(" + m + "){if(!" + f + "[a[0]]){" + f + "[a[0]]=" + p + "(a[1])}else{" + d + "+=" + f + "[a[0]]}}else{s=" + f + "[a[0]];if(typeof s==='string'){" + d + "+=s}else{s=" + p + "(a[1])}return s}}", n), this.dependencies = (r = {},
                                            r[s] = [d], r[l] = [d, g, o, f], r[c] = [m, l], r[u] = [p, m, d, f], r), this.importContext(d), t.compileDebug && this.importContext(h), y) try {
                                            b = _(b, t)
                                        } catch(C) {}
                                        this.source = b,
                                            this.getTplTokens(b, t.rules, this).forEach(function(e) {
                                                e.type === a.TYPE_STRING ? v.parseString(e) : v.parseExpression(e)
                                            })
                                    }
                                    return e.prototype.getTplTokens = function() {
                                        return a.apply(undefined, arguments)
                                    },
                                        e.prototype.getEsTokens = function(e) {
                                            return r(e)
                                        },
                                        e.prototype.getVariables = function(e) {
                                            var t = !1;
                                            return e.filter(function(e) {
                                                return "whitespace" !== e.type && "comment" !== e.type
                                            }).filter(function(e) {
                                                return "name" === e.type && !t || (t = "punctuator" === e.type && "." === e.value, !1)
                                            }).map(function(e) {
                                                return e.value
                                            })
                                        },
                                        e.prototype.importContext = function(e) {
                                            var t = this,
                                                n = "",
                                                r = this.internal,
                                                a = this.dependencies,
                                                s = this.ignore,
                                                l = this.context,
                                                c = this.options.imports,
                                                u = this.CONTEXT_MAP;
                                            v(u, e) || -1 !== s.indexOf(e) || (v(r, e) ? (n = r[e], v(a, e) && a[e].forEach(function(e) {
                                                return t.importContext(e)
                                            })) : n = "$escape" === e || "$each" === e || v(c, e) ? i + "." + e: o + "." + e, u[e] = n, l.push({
                                                name: e,
                                                value: n
                                            }))
                                        },
                                        e.prototype.parseString = function(e) {
                                            var t = e.value;
                                            if (t) {
                                                var n = d + "+=" + b(t);
                                                this.scripts.push({
                                                    source: t,
                                                    tplToken: e,
                                                    code: n
                                                })
                                            }
                                        },
                                        e.prototype.parseExpression = function(e) {
                                            var t = this,
                                                n = e.value,
                                                r = e.script,
                                                o = r.output,
                                                i = this.options.escape,
                                                s = r.code;
                                            o && (s = !1 === i || o === a.TYPE_RAW ? d + "+=" + r.code: d + "+=$escape(" + r.code + ")");
                                            var l = this.getEsTokens(s);
                                            this.getVariables(l).forEach(function(e) {
                                                return t.importContext(e)
                                            }),
                                                this.scripts.push({
                                                    source: n,
                                                    tplToken: e,
                                                    code: s
                                                })
                                        },
                                        e.prototype.checkExpression = function(e) {
                                            for (var t = [[/^\s*}[\w\W]*?{?[\s;]*$/, ""], [/(^[\w\W]*?\([\w\W]*?(?:=>|\([\w\W]*?\))\s*{[\s;]*$)/, "$1})"], [/(^[\w\W]*?\([\w\W]*?\)\s*{[\s;]*$)/, "$1}"]], n = 0; n < t.length;) {
                                                if (t[n][0].test(e)) {
                                                    var a;
                                                    e = (a = e).replace.apply(a, t[n]);
                                                    break
                                                }
                                                n++
                                            }
                                            try {
                                                return new Function(e),
                                                    !0
                                            } catch(r) {
                                                return ! 1
                                            }
                                        },
                                        e.prototype.build = function() {
                                            var e = this.options,
                                                t = this.context,
                                                n = this.scripts,
                                                r = this.stacks,
                                                s = this.source,
                                                u = e.filename,
                                                p = e.imports,
                                                y = [],
                                                _ = v(this.CONTEXT_MAP, c),
                                                C = 0,
                                                w = function(e, t) {
                                                    var n = t.line,
                                                        a = t.start,
                                                        o = {
                                                            generated: {
                                                                line: r.length + C + 1,
                                                                column: 1
                                                            },
                                                            original: {
                                                                line: n + 1,
                                                                column: a + 1
                                                            }
                                                        };
                                                    return C += e.split(/\n/).length - 1,
                                                        o
                                                },
                                                x = function(e) {
                                                    return e.replace(/^[\t ]+|[\t ]$/g, "")
                                                };
                                            r.push("function(" + o + "){"),
                                                r.push("'use strict'"),
                                                r.push(o + "=" + o + "||{}"),
                                                r.push("var " + t.map(function(e) {
                                                    return e.name + "=" + e.value
                                                }).join(",")),
                                                e.compileDebug ? (r.push("try{"), n.forEach(function(e) {
                                                    e.tplToken.type === a.TYPE_EXPRESSION && r.push(h + "=[" + [e.tplToken.line, e.tplToken.start].join(",") + "]"),
                                                        y.push(w(e.code, e.tplToken)),
                                                        r.push(x(e.code))
                                                }), r.push("}catch(error){"), r.push("throw {" + ["name:'RuntimeError'", "path:" + b(u), "message:error.message", "line:" + h + "[0]+1", "column:" + h + "[1]+1", "source:" + b(s), "stack:error.stack"].join(",") + "}"), r.push("}")) : n.forEach(function(e) {
                                                    y.push(w(e.code, e.tplToken)),
                                                        r.push(x(e.code))
                                                }),
                                            _ && (r.push(d + "=''"), r.push(l + "(" + m + "," + o + "," + f + ")")),
                                                r.push("return " + d),
                                                r.push("}");
                                            var S = r.join("\n");
                                            try {
                                                var T = new Function(i, g, "return " + S)(p, e);
                                                return T.mappings = y,
                                                    T.sourcesContent = [s],
                                                    T
                                            } catch(F) {
                                                for (var A = 0,
                                                         k = 0,
                                                         D = 0,
                                                         P = void 0; A < n.length;) {
                                                    var j = n[A];
                                                    if (!this.checkExpression(j.code)) {
                                                        k = j.tplToken.line,
                                                            D = j.tplToken.start,
                                                            P = j.code;
                                                        break
                                                    }
                                                    A++
                                                }
                                                throw {
                                                    name: "CompileError",
                                                    path: u,
                                                    message: F.message,
                                                    line: k + 1,
                                                    column: D + 1,
                                                    source: s,
                                                    generated: P,
                                                    stack: F.stack
                                                }
                                            }
                                        },
                                        e
                                } ();
                            y.CONSTS = {
                                DATA: o,
                                IMPORTS: i,
                                PRINT: s,
                                INCLUDE: l,
                                EXTEND: c,
                                BLOCK: u,
                                OPTIONS: g,
                                OUT: d,
                                LINE: h,
                                BLOCKS: f,
                                SLICE: p,
                                FROM: m,
                                ESCAPE: "$escape",
                                EACH: "$each"
                            },
                                e.exports = y
                        },
                        function(e, t, n) {
                            "use strict";
                            var r = n(10),
                                a = n(2)["default"],
                                o = n(2).matchToToken;
                            e.exports = function(e) {
                                return e.match(a).map(function(e) {
                                    return a.lastIndex = 0,
                                        o(a.exec(e))
                                }).map(function(e) {
                                    return "name" === e.type && r(e.value) && (e.type = "keyword"),
                                        e
                                })
                            }
                        },
                        function(e, t, n) {
                            "use strict";
                            var r = {
                                abstract: !0,
                                await: !0,
                                boolean: !0,
                                "break": !0,
                                byte: !0,
                                "case": !0,
                                "catch": !0,
                                char: !0,
                                "class": !0,
                                "const": !0,
                                "continue": !0,
                                "debugger": !0,
                                "default": !0,
                                "delete": !0,
                                "do": !0,
                                double: !0,
                                "else": !0,
                                "enum": !0,
                                "export": !0,
                                "extends": !0,
                                "false": !0,
                                final: !0,
                                "finally": !0,
                                float: !0,
                                "for": !0,
                                "function": !0,
                                goto: !0,
                                "if": !0,
                                "implements": !0,
                                "import": !0,
                                "in": !0,
                                "instanceof": !0,
                                int: !0,
                                "interface": !0,
                                "let": !0,
                                long: !0,
                                native: !0,
                                "new": !0,
                                "null": !0,
                                "package": !0,
                                "private": !0,
                                "protected": !0,
                                "public": !0,
                                "return": !0,
                                short: !0,
                                "static": !0,
                                "super": !0,
                                "switch": !0,
                                synchronized: !0,
                                "this": !0,
                                "throw": !0,
                                transient: !0,
                                "true": !0,
                                "try": !0,
                                "typeof": !0,
                                "var": !0,
                                "void": !0,
                                volatile: !0,
                                "while": !0,
                                "with": !0,
                                yield: !0
                            };
                            e.exports = function(e) {
                                return r.hasOwnProperty(e)
                            }
                        },
                        function(e, t, n) {
                            "use strict";
                            function r(e, t, n, r) {
                                var a = new String(e);
                                return a.line = t,
                                    a.start = n,
                                    a.end = r,
                                    a
                            }
                            var a = function(e, t) {
                                for (var n = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
                                         a = [{
                                             type: "string",
                                             value: e,
                                             line: 0,
                                             start: 0,
                                             end: e.length
                                         }], o = 0; o < t.length; o++) !
                                    function(e) {
                                        for (var t = e.test.ignoreCase ? "ig": "g", o = e.test.source + "|^$|[\\w\\W]", i = new RegExp(o, t), s = 0; s < a.length; s++) if ("string" === a[s].type) {
                                            for (var l = a[s].line, c = a[s].start, u = a[s].end, d = a[s].value.match(i), h = [], f = 0; f < d.length; f++) {
                                                var p = d[f];
                                                e.test.lastIndex = 0;
                                                var m = e.test.exec(p),
                                                    g = m ? "expression": "string",
                                                    v = h[h.length - 1],
                                                    b = v || a[s],
                                                    y = b.value,
                                                    _ = {
                                                        type: g,
                                                        value: p,
                                                        line: l,
                                                        start: c = b.line === l ? v ? v.end: c: y.length - y.lastIndexOf("\n") - 1,
                                                        end: u = c + p.length
                                                    };
                                                if ("string" === g) v && "string" === v.type ? (v.value += p, v.end += p.length) : h.push(_);
                                                else {
                                                    m[0] = new r(m[0], l, c, u);
                                                    var C = e.use.apply(n, m);
                                                    _.script = C,
                                                        h.push(_)
                                                }
                                                l += p.split(/\n/).length - 1
                                            }
                                            a.splice.apply(a, [s, 1].concat(h)),
                                                s += h.length - 1
                                        }
                                    } (t[o]);
                                return a
                            };
                            a.TYPE_STRING = "string",
                                a.TYPE_EXPRESSION = "expression",
                                a.TYPE_RAW = "raw",
                                a.TYPE_ESCAPE = "escape",
                                e.exports = a
                        },
                        function(e, t, n) {
                            "use strict"; (function(t) {
                                function r(e) {
                                    return "string" != typeof e && (e = e === undefined || null === e ? "": "function" == typeof e ? r(e.call(e)) : JSON.stringify(e)),
                                        e
                                }
                                var a = n(0),
                                    o = Object.create(a ? t: window),
                                    i = /["&'<>]/;
                                o.$escape = function(e) {
                                    return function(e) {
                                        var t = "" + e,
                                            n = i.exec(t);
                                        if (!n) return e;
                                        var r = "",
                                            a = void 0,
                                            o = void 0,
                                            s = void 0;
                                        for (a = n.index, o = 0; a < t.length; a++) {
                                            switch (t.charCodeAt(a)) {
                                                case 34:
                                                    s = "&#34;";
                                                    break;
                                                case 38:
                                                    s = "&#38;";
                                                    break;
                                                case 39:
                                                    s = "&#39;";
                                                    break;
                                                case 60:
                                                    s = "&#60;";
                                                    break;
                                                case 62:
                                                    s = "&#62;";
                                                    break;
                                                default:
                                                    continue
                                            }
                                            o !== a && (r += t.substring(o, a)),
                                                o = a + 1,
                                                r += s
                                        }
                                        return o !== a ? r + t.substring(o, a) : r
                                    } (r(e))
                                },
                                    o.$each = function(e, t) {
                                        if (Array.isArray(e)) for (var n = 0,
                                                                       r = e.length; n < r; n++) t(e[n], n);
                                        else for (var a in e) t(e[a], a)
                                    },
                                    e.exports = o
                            }).call(t, n(4))
                        },
                        function(e, t, n) {
                            "use strict";
                            var r = Object.prototype.toString;
                            e.exports = function a(e, t) {
                                var n = void 0,
                                    o = function(e) {
                                        return null === e ? "Null": r.call(e).slice(8, -1)
                                    } (e);
                                if ("Object" === o ? n = Object.create(t || {}) : "Array" === o && (n = [].concat(t || [])), n) {
                                    for (var i in e) Object.hasOwnProperty.call(e, i) && (n[i] = a(e[i], n[i]));
                                    return n
                                }
                                return e
                            }
                        },
                        function(e, t, n) {
                            "use strict";
                            e.exports = function(e, t, r, a) {
                                var o = n(1);
                                return a = a.$extend({
                                    filename: a.resolveFilename(e, a),
                                    bail: !0,
                                    source: null
                                }),
                                    o(a)(t, r)
                            }
                        },
                        function(e, t, n) {
                            "use strict";
                            e.exports = function(e) {
                                console.error(e.name, e.message)
                            }
                        },
                        function(e, t, n) {
                            "use strict";
                            var r = {
                                __data: Object.create(null),
                                set: function(e, t) {
                                    this.__data[e] = t
                                },
                                get: function(e) {
                                    return this.__data[e]
                                },
                                reset: function() {
                                    this.__data = {}
                                }
                            };
                            e.exports = r
                        },
                        function(e, t, n) {
                            "use strict";
                            var r = n(0);
                            e.exports = function(e) {
                                if (r) return n(5).readFileSync(e, "utf8");
                                var t = document.getElementById(e);
                                return t.value || t.innerHTML
                            }
                        },
                        function(e, t, n) {
                            "use strict";
                            var r = {
                                test: /{{([@#]?)[ \t]*(\/?)([\w\W]*?)[ \t]*}}/,
                                use: function(e, t, n, a) {
                                    var o = this.options,
                                        i = this.getEsTokens(a),
                                        s = i.map(function(e) {
                                            return e.value
                                        }),
                                        l = {},
                                        c = void 0,
                                        u = !!t && "raw",
                                        d = n + s.shift(),
                                        h = function(t, n) {
                                            console.warn((o.filename || "anonymous") + ":" + (e.line + 1) + ":" + (e.start + 1) + "\nTemplate upgrade: {{" + t + "}} -> {{" + n + "}}")
                                        };
                                    switch ("#" === t && h("#value", "@value"), d) {
                                        case "set":
                                            a = "var " + s.join("").trim();
                                            break;
                                        case "if":
                                            a = "if(" + s.join("").trim() + "){";
                                            break;
                                        case "else":
                                            var f = s.indexOf("if");~f ? (s.splice(0, f + 1), a = "}else if(" + s.join("").trim() + "){") : a = "}else{";
                                            break;
                                        case "/if":
                                            a = "}";
                                            break;
                                        case "each":
                                            (c = r._split(i)).shift(),
                                            "as" === c[1] && (h("each object as value index", "each object value index"), c.splice(1, 1)),
                                                a = "$each(" + (c[0] || "$data") + ",function(" + (c[1] || "$value") + "," + (c[2] || "$index") + "){";
                                            break;
                                        case "/each":
                                            a = "})";
                                            break;
                                        case "block":
                                            (c = r._split(i)).shift(),
                                                a = "block(" + c.join(",").trim() + ",function(){";
                                            break;
                                        case "/block":
                                            a = "})";
                                            break;
                                        case "echo":
                                            d = "print",
                                                h("echo value", "value");
                                        case "print":
                                        case "include":
                                        case "extend":
                                            if (0 !== s.join("").trim().indexOf("(")) { (c = r._split(i)).shift(),
                                                a = d + "(" + c.join(",") + ")";
                                                break
                                            }
                                        default:
                                            if (~s.indexOf("|")) {
                                                var p = i.reduce(function(e, t) {
                                                        var n = t.value,
                                                            r = t.type;
                                                        return "|" === n ? e.push([]) : "whitespace" !== r && "comment" !== r && (e.length || e.push([]), ":" === n && 1 === e[e.length - 1].length ? h("value | filter: argv", "value | filter argv") : e[e.length - 1].push(t)),
                                                            e
                                                    },
                                                    []).map(function(e) {
                                                    return r._split(e)
                                                });
                                                a = p.reduce(function(e, t) {
                                                        var n = t.shift();
                                                        return t.unshift(e),
                                                        "$imports." + n + "(" + t.join(",") + ")"
                                                    },
                                                    p.shift().join(" ").trim())
                                            }
                                            u = u || "escape"
                                    }
                                    return l.code = a,
                                        l.output = u,
                                        l
                                },
                                _split: function(e) {
                                    for (var t = 0,
                                             n = (e = e.filter(function(e) {
                                                 var t = e.type;
                                                 return "whitespace" !== t && "comment" !== t
                                             })).shift(), r = /\]|\)/, a = [[n]]; t < e.length;) {
                                        var o = e[t];
                                        "punctuator" === o.type || "punctuator" === n.type && !r.test(n.value) ? a[a.length - 1].push(o) : a.push([o]),
                                            n = o,
                                            t++
                                    }
                                    return a.map(function(e) {
                                        return e.map(function(e) {
                                            return e.value
                                        }).join("")
                                    })
                                }
                            };
                            e.exports = r
                        },
                        function(e, t, n) {
                            "use strict";
                            e.exports = {
                                test: /<%(#?)((?:==|=#|[=-])?)[ \t]*([\w\W]*?)[ \t]*(-?)%>/,
                                use: function(e, t, n, r) {
                                    return n = {
                                        "-": "raw",
                                        "=": "escape",
                                        "": !1,
                                        "==": "raw",
                                        "=#": "raw"
                                    } [n],
                                    t && (r = "/*" + r + "*/", n = !1),
                                        {
                                            code: r,
                                            output: n
                                        }
                                }
                            }
                        },
                        function(e, t, n) {
                            "use strict";
                            var r = n(0);
                            e.exports = function(e, t) {
                                if (r) {
                                    var a, o = n(21).minify,
                                        i = t.htmlMinifierOptions,
                                        s = t.rules.map(function(e) {
                                            return e.test
                                        }); (a = i.ignoreCustomFragments).push.apply(a, s),
                                        e = o(e, i)
                                }
                                return e
                            }
                        },
                        function(e, t) { ("object" == typeof e && "object" == typeof e.exports ? e.exports: window).noop = function() {}
                        },
                        function(e, t, n) {
                            "use strict";
                            var r = n(0),
                                a = /^\.+\//;
                            e.exports = function(e, t) {
                                if (r) {
                                    var o = n(5),
                                        i = t.root,
                                        s = t.extname;
                                    if (a.test(e)) {
                                        var l = t.filename,
                                            c = l && e !== l ? o.dirname(l) : i;
                                        e = o.resolve(c, e)
                                    } else e = o.resolve(i, e);
                                    o.extname(e) || (e += s)
                                }
                                return e
                            }
                        },
                        function(e, t, n) {
                            "use strict";
                            var r = function(e) {
                                function t(n) { !
                                    function(e, t) {
                                        if (! (e instanceof t)) throw new TypeError("Cannot call a class as a function")
                                    } (this, t);
                                    var r = function(e, t) {
                                        if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                                        return ! t || "object" != typeof t && "function" != typeof t ? e: t
                                    } (this, e.call(this, n.message));
                                    return r.name = "TemplateError",
                                        r.message = function(e) {
                                            var t = e.name,
                                                n = e.source,
                                                r = e.path,
                                                a = e.line,
                                                o = e.column,
                                                i = e.generated,
                                                s = e.message;
                                            if (!n) return s;
                                            var l = n.split(/\n/),
                                                c = Math.max(a - 3, 0),
                                                u = Math.min(l.length, a + 3),
                                                d = l.slice(c, u).map(function(e, t) {
                                                    var n = t + c + 1;
                                                    return (n === a ? " >> ": "    ") + n + "| " + e
                                                }).join("\n");
                                            return (r || "anonymous") + ":" + a + ":" + o + "\n" + d + "\n\n" + t + ": " + s + (i ? "\n   generated: " + i: "")
                                        } (n),
                                    Error.captureStackTrace && Error.captureStackTrace(r, r.constructor),
                                        r
                                }
                                return function(e, t) {
                                    if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
                                    e.prototype = Object.create(t && t.prototype, {
                                        constructor: {
                                            value: e,
                                            enumerable: !1,
                                            writable: !0,
                                            configurable: !0
                                        }
                                    }),
                                    t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
                                } (t, e),
                                    t
                            } (Error);
                            e.exports = r
                        },
                        function(e, t, n) {
                            "use strict";
                            e.exports = n(3)
                        }])
                })
        }).call(t, n("./node_modules/node-libs-browser/node_modules/process/browser.js"))},
        "./ClientApp/modules/topspeedclient/ClientManager.js": function(e, t, n) {
            var r = n("./ClientApp/modules/topspeedclient/baseclient.js"),
                a = n("./ClientApp/modules/topspeedclient/quotelistclient.js");
            e.exports = {
                TSQListClient: a,
                TSQClient: r
            }
        },
        "./ClientApp/modules/topspeedclient/TSQExceptions.js": function(e, t, n) {
            function r(e, t, n, r) {
                var a = i;
                if ("object" == typeof arguments[0]) {
                    var o = arguments[0];
                    a = arguments[1],
                        e = o.errorCode,
                        t = o.errorMessage,
                        n = o.errorDesc,
                        r = o.parentCode
                }
                this.errorCode = e || 0,
                    this.errorMessage = t || "success",
                    this.errorDesc = n || "",
                r && a.hasOwnProperty(r) && (this.parent = a[r])
            }
            function a(e, t, n, r) {
                if ("object" == typeof arguments[0]) {
                    var a = arguments[0];
                    e = a.errorCode,
                        t = a.errorMessage,
                        n = a.errorDesc,
                        r = a.innerError
                }
                this.errorCode = e || 0,
                    this.errorMessage = t || "success",
                    this.errorDesc = n || "",
                r && (this.innerError = r)
            }
            var o = n("./ClientApp/modules/topspeedclient/errors.json"),
                i = function(e) {
                    var t = {};
                    if (e instanceof Array) for (var n = 0; n < e.length; n++) {
                        var a = e[n];
                        "number" == typeof a.errorCode && (t[a.errorCode] = new r(a, t))
                    }
                    return t
                } (o.ServerSide);
            r.errors = i;
            var s = function(e) {
                var t = {};
                if (e instanceof Array) for (var n = 0; n < e.length; n++) {
                    var r = e[n];
                    "number" == typeof r.errorCode && (t[r.errorCode] = new a(r))
                }
                return t
            } (o.ClientSide);
            a.errors = s,
                e.exports = {
                    server: r.errors,
                    client: a.errors,
                    TSQServerError: r,
                    TSQClientError: a
                }
        },
        "./ClientApp/modules/topspeedclient/baseclient.js": function(e, t, n) {
            function r(e, t) {
                function n(t) {
                    function n() {
                        "function" != typeof window[d.cb] && (window[d.cb] = function(e) {
                            d.procMessage.apply(d, [e])
                        });
                        var t = s({
                                sid: d.sid ? d.sid: 0,
                                cb: d.cb
                            },
                            v.data),
                            n = a(e.baseurl + c[v.method], t);
                        d.log("long-polling", n),
                            o(n, {},
                                "_",
                                function() {},
                                function() {
                                    d.errorHander(l.client[2])
                                })
                    }
                    if (!d.stopped) if (v.method === r.method.SERVEREVENT) {
                        if (h instanceof EventSource) return;
                        var u = a(e.baseurl + c[v.method], v.data);
                        d.log("sse start", u),
                            (h = new EventSource(u)).onmessage = function(e) {
                                d.procMessage(i.parse(e.data))
                            },
                            h.onerror = function(e) {
                                d.log("EventSource Error"),
                                    h.close();
                                var t = l.client[2];
                                t.innerError = e,
                                    d.errorHander(t)
                            }
                    } else if (v.method === r.method.POLLING) v.data.sid = d.sid ? d.sid: 0,
                        clearTimeout(p),
                        p = setTimeout(n, t || 0);
                    else if (v.method === r.method.GET) d.cancel(),
                        o(e.baseurl + c[v.method], v.data, "cb",
                            function(e) {
                                d.procMessage.apply(d, [e])
                            },
                            function() {
                                d.errorHander(l.client[2])
                            });
                    else {
                        var f = l.client[4];
                        d.errorHander.apply(d, [f])
                    }
                }
                r.cid || (r.cid = 0);
                var d = this,
                    h = null,
                    f = 0,
                    p = 0,
                    m = 0,
                    g = 0,
                    v = s({
                            method: r.method.AUTO,
                            timeout: 6e4,
                            delay: 2e3,
                            onmessage: null,
                            onerror: function(e) {
                                console.error(e)
                            },
                            errorRetry: 3
                        },
                        e);
                if (v.method === r.method.SERVEREVENT && "undefined" == typeof EventSource) throw l.client[4];
                this.sid = 0,
                    this.clientId = r.cid++,
                    this.cb = "tsq_cb_" + this.clientId + (new Date).getTime(),
                    this.onmessage = v.onmessage,
                    this.subscribe = function() {
                        m || (m = setInterval(function() { (new Date).getTime() - d.lastMsgTime > v.timeout && d.errorHander(l.client[1])
                            },
                            1e3)),
                            this.stopped = !1,
                            n.apply(this)
                    },
                    this.cancel = function() {
                        this.stopped = !0,
                            this.lastMsgTime = 0,
                            window[d.cb] = undefined,
                        h && (h.close(), h = null),
                        m && (clearInterval(m), m = 0),
                        p && (clearTimeout(p), p = 0),
                        g && (clearTimeout(g), g = 0),
                            this.log("stopped")
                    },
                    this.procMessage = function(e) {
                        try {
                            if (!e) return void this.errorHander(l.client[2]);
                            e.sid && (this.sid = e.sid),
                                e.rt == u.HEARTBEAT ? (this.log("heartbeat"), this.lastMsgTime = (new Date).getTime(), n(1e3 * Math.random())) : e.rt == u.ERROR ? this.errorHander(l.server[e.rc]) : (f = 0, this.lastMsgTime = (new Date).getTime(), this.log(e.data), "function" == typeof this.onmessage && this.onmessage.apply(d, [e.data]), n())
                        } catch(t) {
                            console.error(t)
                        }
                    },
                    this.errorHander = function() {
                        if (this.cancel(), "function" == typeof v.onerror && v.onerror.apply(d, arguments), arguments[0] instanceof l.TSQClientError || arguments[0] instanceof l.TSQServerError ? this.log(arguments[0].errorMessage, arguments) : this.log(arguments), v.errorRetry > 0) if (++f < v.errorRetry) g = setTimeout(function() {
                                d.subscribe.apply(d)
                            },
                            v.delay);
                        else {
                            var e = l.client[3];
                            "function" == typeof v.onerror && v.onerror.apply(d, [e]),
                                console.error(e.errorMessage)
                        }
                    },
                    this.log = function() {
                        try {
                            var e = arguments;
                            d.clientId,
                                (new Date).toTimeString().substr(0, 8);
                            1 == e.length ? e[0] : 2 == e.length ? (e[0], e[1]) : 3 == e.length ? (e[0], e[1], e[2]) : 4 == e.length ? (e[0], e[1], e[2], e[3]) : 5 == e.length && (e[0], e[1], e[2], e[3], e[4])
                        } catch(t) {}
                    },
                t && this.subscribe()
            }
            function a(e, t) {
                1 === arguments.length && (t = e, e = "");
                for (var n = "",
                         r = function(e) {
                             var t = [];
                             if (!e) return t;
                             for (var n in e) e.hasOwnProperty(n) && e[n] !== undefined && t.push(n);
                             return t
                         } (t), a = 0; a < r.length; a++) n += encodeURIComponent(r[a]) + "=" + encodeURIComponent(t[r[a]]),
                a != r.length - 1 && (n += "&");
                return e += n ? (e.indexOf("?") > 0 ? "&": "?") + n: ""
            }
            var o = n("./ClientApp/modules/jsonp.js"),
                i = n("./ClientApp/modules/polyfills/json-polyfill.js"),
                s = n("./ClientApp/modules/utils.extend.js"),
                l = n("./ClientApp/modules/topspeedclient/TSQExceptions.js");
            e.exports = r;
            var c = {
                    get: "/get",
                    polling: "/poll",
                    streaming: "/stream",
                    iframe: "/iframe",
                    serverevent: "/sse"
                },
                u = {
                    ERROR: 1,
                    HEARTBEAT: 2,
                    DATA: 3
                };
            r.method = {
                AUTO: "undefined" != typeof EventSource ? "serverevent": "polling",
                GET: "get",
                POLLING: "polling",
                IFRAME: "iframe",
                STREAM: "streaming",
                SERVEREVENT: "serverevent"
            },
                r.PushTypePathMap = c,
                r.ReturnType = u,
                r.version = "2.0.1"
        },
        "./ClientApp/modules/topspeedclient/errors.json": function(e, t) {
            e.exports = {
                ServerSide: [{
                    errorCode: 0,
                    errorMessage: "success",
                    errorDesc: "响应成功"
                },
                    {
                        errorCode: 1,
                        errorMessage: "server is initializing",
                        errorDesc: "服务器正在初始化中，请稍后重试"
                    },
                    {
                        errorCode: 100,
                        errorMessage: "bad request",
                        errorDesc: "请求错误"
                    },
                    {
                        errorCode: 101,
                        errorMessage: "method not allowed",
                        errorDesc: "错误的HTTP请求方法",
                        parentCode: 100
                    },
                    {
                        errorCode: 102,
                        errorMessage: "invaild request parameters",
                        errorDesc: "请求参数非法",
                        parentCode: 100
                    },
                    {
                        errorCode: 103,
                        errorMessage: "invaild or invalidated session id",
                        errorDesc: "错误或失效的长轮询请求sid",
                        parentCode: 100
                    },
                    {
                        errorCode: 1e3,
                        errorMessage: "server internal error",
                        errorDesc: "服务器内部异常"
                    }],
                ClientSide: [{
                    errorCode: 0,
                    errorMessage: "success",
                    errorDesc: "成功"
                },
                    {
                        errorCode: 1,
                        errorMessage: "request timeout",
                        errorDesc: "请求超时"
                    },
                    {
                        errorCode: 2,
                        errorMessage: "error response type",
                        errorDesc: "错误的响应类型"
                    },
                    {
                        errorCode: 3,
                        errorMessage: "retried too many times",
                        errorDesc: "超过最大重试次数"
                    },
                    {
                        errorCode: 4,
                        errorMessage: "Unsupport request method",
                        errorDesc: "不支持的请求方法"
                    }]
            }
        },
        "./ClientApp/modules/topspeedclient/quotelistclient.js": function(e, t, n) {
            function r(e) {
                var t, n = l({
                        enableMutiDomain: !0,
                        delay: 2e3,
                        errorRetry: 10
                    },
                    e),
                    r = this.datacache = new s({
                        rows: [],
                        total: 0
                    }),
                    u = n.enableMutiDomain ? (100 * Math.random() + 1).toFixed(0) + ".": "",
                    d = n.baseurl || "/api/qt/list",
                    h = "//" + u + n.host + d;
                this.get = function(e, t) {
                    e instanceof a || (e = new a(e));
                    new i({
                        baseurl: h,
                        data: e.toParam(),
                        method: i.method.GET,
                        delay: n.delay,
                        errorRetry: n.errorRetry,
                        onmessage: t,
                        onerror: n.onerror
                    }).subscribe()
                },
                    this.open = function(e, s) {
                        this.close(),
                        e instanceof a || (e = new a(e));
                        var u = !1,
                            d = e.toParam();
                        return t = new i({
                                baseurl: h,
                                data: d,
                                delay: n.delay,
                                errorRetry: n.errorRetry,
                                onmessage: function(e) {
                                    var t = new o(e); !
                                        function(e) {
                                            if (e && !(e.total <= 0)) {
                                                var t = r.getOrAdd("diff", []),
                                                    n = l([], t, !0);
                                                for (var a in e.mv) {
                                                    var o = parseInt(a),
                                                        i = parseInt(e.mv[a]);
                                                    isNaN(o) || isNaN(i) || (t[i] = n[o])
                                                }
                                                n = null;
                                                for (var s in e.diff) {
                                                    var c = parseInt(s);
                                                    if (c >= 0) {
                                                        var u = e.diff[s];
                                                        t[c] ? l(t[c], u) : t[c] = u
                                                    }
                                                }
                                                r.total = e.total
                                            }
                                        } (t),
                                        u ? "function" == typeof n.onmessage && n.onmessage.call(self, t) : ("function" == typeof s && s.call(self, t), u = !0)
                                },
                                onerror: function() {
                                    u = !1,
                                    "function" == typeof n.onerror && n.onerror.apply(self, arguments)
                                }
                            },
                            !0),
                            c.set(t.clientId, t),
                            t.clientId
                    },
                    this.close = function() {
                        r.clear(),
                        t instanceof i && (t.cancel(), c.remove(t.clientId))
                    }
            }
            function a(e) {
                this.id = 1,
                    this.pageIndex = 0,
                    this.pageSize = 20,
                    this.orderType = 1,
                    this.orderField = "f3",
                    this.fields = [],
                    this.toParam = function() {
                        var e = {
                            lid: this.id,
                            pi: this.pageIndex,
                            pz: this.pageSize,
                            po: this.orderType,
                            fid: this.orderField
                        };
                        return this.fields.length > 0 && (e.fields = this.fields.join(",")),
                            e
                    },
                    this.toString = function() {
                        var e = this.fields.length > 0 ? this.fields.join(",") : "";
                        return "lid=" + encodeURIComponent(this.id) + "&pi=" + encodeURIComponent(this.pageIndex) + "&pz=" + encodeURIComponent(this.pageSize) + "&po=" + encodeURIComponent(this.orderType) + "&fid=" + encodeURIComponent(this.orderField) + (e ? "&fields=" + encodeURIComponent(e) : "")
                    },
                "object" == typeof e && l(this, e, !0)
            }
            function o(e) {
                this.diff = {},
                    this.mv = {},
                    this.total = 0,
                "object" == typeof e && l(this, e, !0),
                    this.getMovedKeys = function() {
                        var e = [];
                        for (var t in this.mv) this.mv.hasOwnProperty(t) && e.push(this.mv[t]);
                        return e
                    }
            }
            var i = n("./ClientApp/modules/topspeedclient/baseclient.js"),
                s = n("./ClientApp/modules/utils.cache.js"),
                l = n("./ClientApp/modules/utils.extend.js"),
                c = new s;
            r.close = function(e) {
                if (e instanceof r) e.close();
                else {
                    var t = c.remove(e);
                    t instanceof i && t.cancel()
                }
            },
                r.cache = c,
                r.request = a,
                r.response = o,
                r.version = "1.0.1",
                e.exports = r
        },
        "./ClientApp/modules/uri/main.js": function(e, t, n) {
            var r = n("./ClientApp/modules/uri/src/SecondLevelDomains.js"),
                a = n("./ClientApp/modules/uri/src/URI.js")(window, null, null, r);
            n("./ClientApp/modules/uri/src/URITemplate.js")(window, a),
                n("./ClientApp/modules/uri/src/URI.fragmentQuery.js")(a);
            e.exports = a
        },
        "./ClientApp/modules/uri/src/SecondLevelDomains.js": function(e, t) { !
            function(t, n) {
                "use strict";
                "object" == typeof e && e.exports ? e.exports = n() : t.SecondLevelDomains = n(t)
            } (this,
                function(e) {
                    "use strict";
                    var t = e && e.SecondLevelDomains,
                        n = {
                            list: {
                                ac: " com gov mil net org ",
                                ae: " ac co gov mil name net org pro sch ",
                                af: " com edu gov net org ",
                                al: " com edu gov mil net org ",
                                ao: " co ed gv it og pb ",
                                ar: " com edu gob gov int mil net org tur ",
                                at: " ac co gv or ",
                                au: " asn com csiro edu gov id net org ",
                                ba: " co com edu gov mil net org rs unbi unmo unsa untz unze ",
                                bb: " biz co com edu gov info net org store tv ",
                                bh: " biz cc com edu gov info net org ",
                                bn: " com edu gov net org ",
                                bo: " com edu gob gov int mil net org tv ",
                                br: " adm adv agr am arq art ato b bio blog bmd cim cng cnt com coop ecn edu eng esp etc eti far flog fm fnd fot fst g12 ggf gov imb ind inf jor jus lel mat med mil mus net nom not ntr odo org ppg pro psc psi qsl rec slg srv tmp trd tur tv vet vlog wiki zlg ",
                                bs: " com edu gov net org ",
                                bz: " du et om ov rg ",
                                ca: " ab bc mb nb nf nl ns nt nu on pe qc sk yk ",
                                ck: " biz co edu gen gov info net org ",
                                cn: " ac ah bj com cq edu fj gd gov gs gx gz ha hb he hi hl hn jl js jx ln mil net nm nx org qh sc sd sh sn sx tj tw xj xz yn zj ",
                                co: " com edu gov mil net nom org ",
                                cr: " ac c co ed fi go or sa ",
                                cy: " ac biz com ekloges gov ltd name net org parliament press pro tm ",
                                "do": " art com edu gob gov mil net org sld web ",
                                dz: " art asso com edu gov net org pol ",
                                ec: " com edu fin gov info med mil net org pro ",
                                eg: " com edu eun gov mil name net org sci ",
                                er: " com edu gov ind mil net org rochest w ",
                                es: " com edu gob nom org ",
                                et: " biz com edu gov info name net org ",
                                fj: " ac biz com info mil name net org pro ",
                                fk: " ac co gov net nom org ",
                                fr: " asso com f gouv nom prd presse tm ",
                                gg: " co net org ",
                                gh: " com edu gov mil org ",
                                gn: " ac com gov net org ",
                                gr: " com edu gov mil net org ",
                                gt: " com edu gob ind mil net org ",
                                gu: " com edu gov net org ",
                                hk: " com edu gov idv net org ",
                                hu: " 2000 agrar bolt casino city co erotica erotika film forum games hotel info ingatlan jogasz konyvelo lakas media news org priv reklam sex shop sport suli szex tm tozsde utazas video ",
                                id: " ac co go mil net or sch web ",
                                il: " ac co gov idf k12 muni net org ",
                                "in": " ac co edu ernet firm gen gov i ind mil net nic org res ",
                                iq: " com edu gov i mil net org ",
                                ir: " ac co dnssec gov i id net org sch ",
                                it: " edu gov ",
                                je: " co net org ",
                                jo: " com edu gov mil name net org sch ",
                                jp: " ac ad co ed go gr lg ne or ",
                                ke: " ac co go info me mobi ne or sc ",
                                kh: " com edu gov mil net org per ",
                                ki: " biz com de edu gov info mob net org tel ",
                                km: " asso com coop edu gouv k medecin mil nom notaires pharmaciens presse tm veterinaire ",
                                kn: " edu gov net org ",
                                kr: " ac busan chungbuk chungnam co daegu daejeon es gangwon go gwangju gyeongbuk gyeonggi gyeongnam hs incheon jeju jeonbuk jeonnam k kg mil ms ne or pe re sc seoul ulsan ",
                                kw: " com edu gov net org ",
                                ky: " com edu gov net org ",
                                kz: " com edu gov mil net org ",
                                lb: " com edu gov net org ",
                                lk: " assn com edu gov grp hotel int ltd net ngo org sch soc web ",
                                lr: " com edu gov net org ",
                                lv: " asn com conf edu gov id mil net org ",
                                ly: " com edu gov id med net org plc sch ",
                                ma: " ac co gov m net org press ",
                                mc: " asso tm ",
                                me: " ac co edu gov its net org priv ",
                                mg: " com edu gov mil nom org prd tm ",
                                mk: " com edu gov inf name net org pro ",
                                ml: " com edu gov net org presse ",
                                mn: " edu gov org ",
                                mo: " com edu gov net org ",
                                mt: " com edu gov net org ",
                                mv: " aero biz com coop edu gov info int mil museum name net org pro ",
                                mw: " ac co com coop edu gov int museum net org ",
                                mx: " com edu gob net org ",
                                my: " com edu gov mil name net org sch ",
                                nf: " arts com firm info net other per rec store web ",
                                ng: " biz com edu gov mil mobi name net org sch ",
                                ni: " ac co com edu gob mil net nom org ",
                                np: " com edu gov mil net org ",
                                nr: " biz com edu gov info net org ",
                                om: " ac biz co com edu gov med mil museum net org pro sch ",
                                pe: " com edu gob mil net nom org sld ",
                                ph: " com edu gov i mil net ngo org ",
                                pk: " biz com edu fam gob gok gon gop gos gov net org web ",
                                pl: " art bialystok biz com edu gda gdansk gorzow gov info katowice krakow lodz lublin mil net ngo olsztyn org poznan pwr radom slupsk szczecin torun warszawa waw wroc wroclaw zgora ",
                                pr: " ac biz com edu est gov info isla name net org pro prof ",
                                ps: " com edu gov net org plo sec ",
                                pw: " belau co ed go ne or ",
                                ro: " arts com firm info nom nt org rec store tm www ",
                                rs: " ac co edu gov in org ",
                                sb: " com edu gov net org ",
                                sc: " com edu gov net org ",
                                sh: " co com edu gov net nom org ",
                                sl: " com edu gov net org ",
                                st: " co com consulado edu embaixada gov mil net org principe saotome store ",
                                sv: " com edu gob org red ",
                                sz: " ac co org ",
                                tr: " av bbs bel biz com dr edu gen gov info k12 name net org pol tel tsk tv web ",
                                tt: " aero biz cat co com coop edu gov info int jobs mil mobi museum name net org pro tel travel ",
                                tw: " club com ebiz edu game gov idv mil net org ",
                                mu: " ac co com gov net or org ",
                                mz: " ac co edu gov org ",
                                na: " co com ",
                                nz: " ac co cri geek gen govt health iwi maori mil net org parliament school ",
                                pa: " abo ac com edu gob ing med net nom org sld ",
                                pt: " com edu gov int net nome org publ ",
                                py: " com edu gov mil net org ",
                                qa: " com edu gov mil net org ",
                                re: " asso com nom ",
                                ru: " ac adygeya altai amur arkhangelsk astrakhan bashkiria belgorod bir bryansk buryatia cbg chel chelyabinsk chita chukotka chuvashia com dagestan e-burg edu gov grozny int irkutsk ivanovo izhevsk jar joshkar-ola kalmykia kaluga kamchatka karelia kazan kchr kemerovo khabarovsk khakassia khv kirov koenig komi kostroma kranoyarsk kuban kurgan kursk lipetsk magadan mari mari-el marine mil mordovia mosreg msk murmansk nalchik net nnov nov novosibirsk nsk omsk orenburg org oryol penza perm pp pskov ptz rnd ryazan sakhalin samara saratov simbirsk smolensk spb stavropol stv surgut tambov tatarstan tom tomsk tsaritsyn tsk tula tuva tver tyumen udm udmurtia ulan-ude vladikavkaz vladimir vladivostok volgograd vologda voronezh vrn vyatka yakutia yamal yekaterinburg yuzhno-sakhalinsk ",
                                rw: " ac co com edu gouv gov int mil net ",
                                sa: " com edu gov med net org pub sch ",
                                sd: " com edu gov info med net org tv ",
                                se: " a ac b bd c d e f g h i k l m n o org p parti pp press r s t tm u w x y z ",
                                sg: " com edu gov idn net org per ",
                                sn: " art com edu gouv org perso univ ",
                                sy: " com edu gov mil net news org ",
                                th: " ac co go in mi net or ",
                                tj: " ac biz co com edu go gov info int mil name net nic org test web ",
                                tn: " agrinet com defense edunet ens fin gov ind info intl mincom nat net org perso rnrt rns rnu tourism ",
                                tz: " ac co go ne or ",
                                ua: " biz cherkassy chernigov chernovtsy ck cn co com crimea cv dn dnepropetrovsk donetsk dp edu gov if in ivano-frankivsk kh kharkov kherson khmelnitskiy kiev kirovograd km kr ks kv lg lugansk lutsk lviv me mk net nikolaev od odessa org pl poltava pp rovno rv sebastopol sumy te ternopil uzhgorod vinnica vn zaporizhzhe zhitomir zp zt ",
                                ug: " ac co go ne or org sc ",
                                uk: " ac bl british-library co cym gov govt icnet jet lea ltd me mil mod national-library-scotland nel net nhs nic nls org orgn parliament plc police sch scot soc ",
                                us: " dni fed isa kids nsn ",
                                uy: " com edu gub mil net org ",
                                ve: " co com edu gob info mil net org web ",
                                vi: " co com k12 net org ",
                                vn: " ac biz com edu gov health info int name net org pro ",
                                ye: " co com gov ltd me net org plc ",
                                yu: " ac co edu gov org ",
                                za: " ac agric alt bourse city co cybernet db edu gov grondar iaccess imt inca landesign law mil net ngo nis nom olivetti org pix school tm web ",
                                zm: " ac co com edu gov net org sch ",
                                com: "ar br cn de eu gb gr hu jpn kr no qc ru sa se uk us uy za ",
                                net: "gb jp se uk ",
                                org: "ae",
                                de: "com "
                            },
                            has: function(e) {
                                var t = e.lastIndexOf(".");
                                if (t <= 0 || t >= e.length - 1) return ! 1;
                                var r = e.lastIndexOf(".", t - 1);
                                if (r <= 0 || r >= t - 1) return ! 1;
                                var a = n.list[e.slice(t + 1)];
                                return !! a && a.indexOf(" " + e.slice(r + 1, t) + " ") >= 0
                            },
                            is: function(e) {
                                var t = e.lastIndexOf(".");
                                if (t <= 0 || t >= e.length - 1) return ! 1;
                                if (e.lastIndexOf(".", t - 1) >= 0) return ! 1;
                                var r = n.list[e.slice(t + 1)];
                                return !! r && r.indexOf(" " + e.slice(0, t) + " ") >= 0
                            },
                            get: function(e) {
                                var t = e.lastIndexOf(".");
                                if (t <= 0 || t >= e.length - 1) return null;
                                var r = e.lastIndexOf(".", t - 1);
                                if (r <= 0 || r >= t - 1) return null;
                                var a = n.list[e.slice(t + 1)];
                                return a ? a.indexOf(" " + e.slice(r + 1, t) + " ") < 0 ? null: e.slice(r + 1) : null
                            },
                            noConflict: function() {
                                return e.SecondLevelDomains === this && (e.SecondLevelDomains = t),
                                    this
                            }
                        };
                    return n
                })
        },
        "./ClientApp/modules/uri/src/URI.fragmentQuery.js": function(e, t) {
            e.exports = function(e) {
                "use strict";
                var t = e.prototype,
                    n = t.fragment;
                e.fragmentPrefix = "?";
                var r = e._parts;
                return e._parts = function() {
                    var t = r();
                    return t.fragmentPrefix = e.fragmentPrefix,
                        t
                },
                    t.fragmentPrefix = function(e) {
                        return this._parts.fragmentPrefix = e,
                            this
                    },
                    t.fragment = function(t, r) {
                        var a = this._parts.fragmentPrefix,
                            o = this._parts.fragment || "";
                        return ! 0 === t ? o.substring(0, a.length) !== a ? {}: e.parseQuery(o.substring(a.length)) : t !== undefined && "string" != typeof t ? (this._parts.fragment = a + e.buildQuery(t), this.build(!r), this) : n.call(this, t, r)
                    },
                    t.addFragment = function(t, n, r) {
                        var a = this._parts.fragmentPrefix,
                            o = e.parseQuery((this._parts.fragment || "").substring(a.length));
                        return e.addQuery(o, t, n),
                            this._parts.fragment = a + e.buildQuery(o),
                        "string" != typeof t && (r = n),
                            this.build(!r),
                            this
                    },
                    t.removeFragment = function(t, n, r) {
                        var a = this._parts.fragmentPrefix,
                            o = e.parseQuery((this._parts.fragment || "").substring(a.length));
                        return e.removeQuery(o, t, n),
                            this._parts.fragment = a + e.buildQuery(o),
                        "string" != typeof t && (r = n),
                            this.build(!r),
                            this
                    },
                    t.setFragment = function(t, n, r) {
                        var a = this._parts.fragmentPrefix,
                            o = e.parseQuery((this._parts.fragment || "").substring(a.length));
                        return e.setQuery(o, t, n),
                            this._parts.fragment = a + e.buildQuery(o),
                        "string" != typeof t && (r = n),
                            this.build(!r),
                            this
                    },
                    t.addHash = t.addFragment,
                    t.removeHash = t.removeFragment,
                    t.setHash = t.setFragment,
                    e
            }
        },
        "./ClientApp/modules/uri/src/URI.js": function(e, t) {
            e.exports = function(e, t, n, r) {
                "use strict";
                function a(e, t) {
                    var n = arguments.length >= 1,
                        r = arguments.length >= 2;
                    if (! (this instanceof a)) return n ? r ? new a(e, t) : new a(e) : new a;
                    if (e === undefined) {
                        if (n) throw new TypeError("undefined is not a valid argument for URI");
                        e = "undefined" != typeof location ? location.href + "": ""
                    }
                    if (null === e && n) throw new TypeError("null is not a valid argument for URI");
                    return this.href(e),
                        t !== undefined ? this.absoluteTo(t) : this
                }
                function o(e) {
                    return e.replace(/([.*+?^=!:${}()|[\]\/\\])/g, "\\$1")
                }
                function i(e) {
                    return e === undefined ? "Undefined": String(Object.prototype.toString.call(e)).slice(8, -1)
                }
                function s(e) {
                    return "Array" === i(e)
                }
                function l(e, t) {
                    var n, r, a = {};
                    if ("RegExp" === i(t)) a = null;
                    else if (s(t)) for (n = 0, r = t.length; n < r; n++) a[t[n]] = !0;
                    else a[t] = !0;
                    for (n = 0, r = e.length; n < r; n++) { (a && a[e[n]] !== undefined || !a && t.test(e[n])) && (e.splice(n, 1), r--, n--)
                    }
                    return e
                }
                function c(e, t) {
                    var n, r;
                    if (s(t)) {
                        for (n = 0, r = t.length; n < r; n++) if (!c(e, t[n])) return ! 1;
                        return ! 0
                    }
                    var a = i(t);
                    for (n = 0, r = e.length; n < r; n++) if ("RegExp" === a) {
                        if ("string" == typeof e[n] && e[n].match(t)) return ! 0
                    } else if (e[n] === t) return ! 0;
                    return ! 1
                }
                function u(e, t) {
                    if (!s(e) || !s(t)) return ! 1;
                    if (e.length !== t.length) return ! 1;
                    e.sort(),
                        t.sort();
                    for (var n = 0,
                             r = e.length; n < r; n++) if (e[n] !== t[n]) return ! 1;
                    return ! 0
                }
                function d(e) {
                    return e.replace(/^\/+|\/+$/g, "")
                }
                function h(e) {
                    return escape(e)
                }
                function f(e) {
                    return encodeURIComponent(e).replace(/[!'()*]/g, h).replace(/\*/g, "%2A")
                }
                function p(e) {
                    return function(t, n) {
                        return t === undefined ? this._parts[e] || "": (this._parts[e] = t || null, this.build(!n), this)
                    }
                }
                function m(e, t) {
                    return function(n, r) {
                        return n === undefined ? this._parts[e] || "": (null !== n && (n += "").charAt(0) === t && (n = n.substring(1)), this._parts[e] = n, this.build(!r), this)
                    }
                }
                var g = e && e.URI;
                a.version = "1.19.1";
                var v = a.prototype,
                    b = Object.prototype.hasOwnProperty;
                a._parts = function() {
                    return {
                        protocol: null,
                        username: null,
                        password: null,
                        hostname: null,
                        urn: null,
                        port: null,
                        path: null,
                        query: null,
                        fragment: null,
                        preventInvalidHostname: a.preventInvalidHostname,
                        duplicateQueryParameters: a.duplicateQueryParameters,
                        escapeQuerySpace: a.escapeQuerySpace
                    }
                },
                    a.preventInvalidHostname = !1,
                    a.duplicateQueryParameters = !1,
                    a.escapeQuerySpace = !0,
                    a.protocol_expression = /^[a-z][a-z0-9.+-]*$/i,
                    a.idn_expression = /[^a-z0-9\._-]/i,
                    a.punycode_expression = /(xn--)/i,
                    a.ip4_expression = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/,
                    a.ip6_expression = /^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$/,
                    a.find_uri_expression = /\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))/gi,
                    a.findUri = {
                        start: /\b(?:([a-z][a-z0-9.+-]*:\/\/)|www\.)/gi,
                        end: /[\s\r\n]|$/,
                        trim: /[`!()\[\]{};:'".,<>?«»“”„‘’]+$/,
                        parens: /(\([^\)]*\)|\[[^\]]*\]|\{[^}]*\}|<[^>]*>)/g
                    },
                    a.defaultPorts = {
                        http: "80",
                        https: "443",
                        ftp: "21",
                        gopher: "70",
                        ws: "80",
                        wss: "443"
                    },
                    a.hostProtocols = ["http", "https"],
                    a.invalid_hostname_characters = /[^a-zA-Z0-9\.\-:_]/,
                    a.domAttributes = {
                        a: "href",
                        blockquote: "cite",
                        link: "href",
                        base: "href",
                        script: "src",
                        form: "action",
                        img: "src",
                        area: "href",
                        iframe: "src",
                        embed: "src",
                        source: "src",
                        track: "src",
                        input: "src",
                        audio: "src",
                        video: "src"
                    },
                    a.getDomAttribute = function(e) {
                        if (!e || !e.nodeName) return undefined;
                        var t = e.nodeName.toLowerCase();
                        return "input" === t && "image" !== e.type ? undefined: a.domAttributes[t]
                    },
                    a.encode = f,
                    a.decode = decodeURIComponent,
                    a.iso8859 = function() {
                        a.encode = escape,
                            a.decode = unescape
                    },
                    a.unicode = function() {
                        a.encode = f,
                            a.decode = decodeURIComponent
                    },
                    a.characters = {
                        pathname: {
                            encode: {
                                expression: /%(24|26|2B|2C|3B|3D|3A|40)/gi,
                                map: {
                                    "%24": "$",
                                    "%26": "&",
                                    "%2B": "+",
                                    "%2C": ",",
                                    "%3B": ";",
                                    "%3D": "=",
                                    "%3A": ":",
                                    "%40": "@"
                                }
                            },
                            decode: {
                                expression: /[\/\?#]/g,
                                map: {
                                    "/": "%2F",
                                    "?": "%3F",
                                    "#": "%23"
                                }
                            }
                        },
                        reserved: {
                            encode: {
                                expression: /%(21|23|24|26|27|28|29|2A|2B|2C|2F|3A|3B|3D|3F|40|5B|5D)/gi,
                                map: {
                                    "%3A": ":",
                                    "%2F": "/",
                                    "%3F": "?",
                                    "%23": "#",
                                    "%5B": "[",
                                    "%5D": "]",
                                    "%40": "@",
                                    "%21": "!",
                                    "%24": "$",
                                    "%26": "&",
                                    "%27": "'",
                                    "%28": "(",
                                    "%29": ")",
                                    "%2A": "*",
                                    "%2B": "+",
                                    "%2C": ",",
                                    "%3B": ";",
                                    "%3D": "="
                                }
                            }
                        },
                        urnpath: {
                            encode: {
                                expression: /%(21|24|27|28|29|2A|2B|2C|3B|3D|40)/gi,
                                map: {
                                    "%21": "!",
                                    "%24": "$",
                                    "%27": "'",
                                    "%28": "(",
                                    "%29": ")",
                                    "%2A": "*",
                                    "%2B": "+",
                                    "%2C": ",",
                                    "%3B": ";",
                                    "%3D": "=",
                                    "%40": "@"
                                }
                            },
                            decode: {
                                expression: /[\/\?#:]/g,
                                map: {
                                    "/": "%2F",
                                    "?": "%3F",
                                    "#": "%23",
                                    ":": "%3A"
                                }
                            }
                        }
                    },
                    a.encodeQuery = function(e, t) {
                        var n = a.encode(e + "");
                        return t === undefined && (t = a.escapeQuerySpace),
                            t ? n.replace(/%20/g, "+") : n
                    },
                    a.decodeQuery = function(e, t) {
                        e += "",
                        t === undefined && (t = a.escapeQuerySpace);
                        try {
                            return a.decode(t ? e.replace(/\+/g, "%20") : e)
                        } catch(n) {
                            return e
                        }
                    };
                var y, _ = {
                        encode: "encode",
                        decode: "decode"
                    },
                    C = function(e, t) {
                        return function(n) {
                            try {
                                return a[t](n + "").replace(a.characters[e][t].expression,
                                    function(n) {
                                        return a.characters[e][t].map[n]
                                    })
                            } catch(r) {
                                return n
                            }
                        }
                    };
                for (y in _) a[y + "PathSegment"] = C("pathname", _[y]),
                    a[y + "UrnPathSegment"] = C("urnpath", _[y]);
                var w = function(e, t, n) {
                    return function(r) {
                        var o;
                        o = n ?
                            function(e) {
                                return a[t](a[n](e))
                            }: a[t];
                        for (var i = (r + "").split(e), s = 0, l = i.length; s < l; s++) i[s] = o(i[s]);
                        return i.join(e)
                    }
                };
                a.decodePath = w("/", "decodePathSegment"),
                    a.decodeUrnPath = w(":", "decodeUrnPathSegment"),
                    a.recodePath = w("/", "encodePathSegment", "decode"),
                    a.recodeUrnPath = w(":", "encodeUrnPathSegment", "decode"),
                    a.encodeReserved = C("reserved", "encode"),
                    a.parse = function(e, t) {
                        var n;
                        return t || (t = {
                            preventInvalidHostname: a.preventInvalidHostname
                        }),
                        (n = e.indexOf("#")) > -1 && (t.fragment = e.substring(n + 1) || null, e = e.substring(0, n)),
                        (n = e.indexOf("?")) > -1 && (t.query = e.substring(n + 1) || null, e = e.substring(0, n)),
                            "//" === e.substring(0, 2) ? (t.protocol = null, e = e.substring(2), e = a.parseAuthority(e, t)) : (n = e.indexOf(":")) > -1 && (t.protocol = e.substring(0, n) || null, t.protocol && !t.protocol.match(a.protocol_expression) ? t.protocol = undefined: "//" === e.substring(n + 1, n + 3) ? (e = e.substring(n + 3), e = a.parseAuthority(e, t)) : (e = e.substring(n + 1), t.urn = !0)),
                            t.path = e,
                            t
                    },
                    a.parseHost = function(e, t) {
                        e || (e = "");
                        var n, r, o = (e = e.replace(/\\/g, "/")).indexOf("/");
                        if ( - 1 === o && (o = e.length), "[" === e.charAt(0)) n = e.indexOf("]"),
                            t.hostname = e.substring(1, n) || null,
                            t.port = e.substring(n + 2, o) || null,
                        "/" === t.port && (t.port = null);
                        else {
                            var i = e.indexOf(":"),
                                s = e.indexOf("/"),
                                l = e.indexOf(":", i + 1); - 1 !== l && ( - 1 === s || l < s) ? (t.hostname = e.substring(0, o) || null, t.port = null) : (r = e.substring(0, o).split(":"), t.hostname = r[0] || null, t.port = r[1] || null)
                        }
                        return t.hostname && "/" !== e.substring(o).charAt(0) && (o++, e = "/" + e),
                        t.preventInvalidHostname && a.ensureValidHostname(t.hostname, t.protocol),
                        t.port && a.ensureValidPort(t.port),
                        e.substring(o) || "/"
                    },
                    a.parseAuthority = function(e, t) {
                        return e = a.parseUserinfo(e, t),
                            a.parseHost(e, t)
                    },
                    a.parseUserinfo = function(e, t) {
                        var n, r = e.indexOf("/"),
                            o = e.lastIndexOf("@", r > -1 ? r: e.length - 1);
                        return o > -1 && ( - 1 === r || o < r) ? (n = e.substring(0, o).split(":"), t.username = n[0] ? a.decode(n[0]) : null, n.shift(), t.password = n[0] ? a.decode(n.join(":")) : null, e = e.substring(o + 1)) : (t.username = null, t.password = null),
                            e
                    },
                    a.parseQuery = function(e, t) {
                        if (!e) return {};
                        if (! (e = e.replace(/&+/g, "&").replace(/^\?*&*|&+$/g, ""))) return {};
                        for (var n, r, o, i = {},
                                 s = e.split("&"), l = s.length, c = 0; c < l; c++) n = s[c].split("="),
                            r = a.decodeQuery(n.shift(), t),
                            o = n.length ? a.decodeQuery(n.join("="), t) : null,
                            b.call(i, r) ? ("string" != typeof i[r] && null !== i[r] || (i[r] = [i[r]]), i[r].push(o)) : i[r] = o;
                        return i
                    },
                    a.build = function(e) {
                        var t = "";
                        return e.protocol && (t += e.protocol + ":"),
                        e.urn || !t && !e.hostname || (t += "//"),
                            t += a.buildAuthority(e) || "",
                        "string" == typeof e.path && ("/" !== e.path.charAt(0) && "string" == typeof e.hostname && (t += "/"), t += e.path),
                        "string" == typeof e.query && e.query && (t += "?" + e.query),
                        "string" == typeof e.fragment && e.fragment && (t += "#" + e.fragment),
                            t
                    },
                    a.buildHost = function(e) {
                        var t = "";
                        return e.hostname ? (a.ip6_expression.test(e.hostname) ? t += "[" + e.hostname + "]": t += e.hostname, e.port && (t += ":" + e.port), t) : ""
                    },
                    a.buildAuthority = function(e) {
                        return a.buildUserinfo(e) + a.buildHost(e)
                    },
                    a.buildUserinfo = function(e) {
                        var t = "";
                        return e.username && (t += a.encode(e.username)),
                        e.password && (t += ":" + a.encode(e.password)),
                        t && (t += "@"),
                            t
                    },
                    a.buildQuery = function(e, t, n) {
                        var r, o, i, l, c = "";
                        for (o in e) if (b.call(e, o) && o) if (s(e[o])) for (r = {},
                                                                                  i = 0, l = e[o].length; i < l; i++) e[o][i] !== undefined && r[e[o][i] + ""] === undefined && (c += "&" + a.buildQueryParameter(o, e[o][i], n), !0 !== t && (r[e[o][i] + ""] = !0));
                        else e[o] !== undefined && (c += "&" + a.buildQueryParameter(o, e[o], n));
                        return c.substring(1)
                    },
                    a.buildQueryParameter = function(e, t, n) {
                        return a.encodeQuery(e, n) + (null !== t ? "=" + a.encodeQuery(t, n) : "")
                    },
                    a.addQuery = function(e, t, n) {
                        if ("object" == typeof t) for (var r in t) b.call(t, r) && a.addQuery(e, r, t[r]);
                        else {
                            if ("string" != typeof t) throw new TypeError("URI.addQuery() accepts an object, string as the name parameter");
                            if (e[t] === undefined) return void(e[t] = n);
                            "string" == typeof e[t] && (e[t] = [e[t]]),
                            s(n) || (n = [n]),
                                e[t] = (e[t] || []).concat(n)
                        }
                    },
                    a.setQuery = function(e, t, n) {
                        if ("object" == typeof t) for (var r in t) b.call(t, r) && a.setQuery(e, r, t[r]);
                        else {
                            if ("string" != typeof t) throw new TypeError("URI.setQuery() accepts an object, string as the name parameter");
                            e[t] = n === undefined ? null: n
                        }
                    },
                    a.removeQuery = function(e, t, n) {
                        var r, o, c;
                        if (s(t)) for (r = 0, o = t.length; r < o; r++) e[t[r]] = undefined;
                        else if ("RegExp" === i(t)) for (c in e) t.test(c) && (e[c] = undefined);
                        else if ("object" == typeof t) for (c in t) b.call(t, c) && a.removeQuery(e, c, t[c]);
                        else {
                            if ("string" != typeof t) throw new TypeError("URI.removeQuery() accepts an object, string, RegExp as the first parameter");
                            n !== undefined ? "RegExp" === i(n) ? !s(e[t]) && n.test(e[t]) ? e[t] = undefined: e[t] = l(e[t], n) : e[t] !== String(n) || s(n) && 1 !== n.length ? s(e[t]) && (e[t] = l(e[t], n)) : e[t] = undefined: e[t] = undefined
                        }
                    },
                    a.hasQuery = function(e, t, n, r) {
                        switch (i(t)) {
                            case "String":
                                break;
                            case "RegExp":
                                for (var o in e) if (b.call(e, o) && t.test(o) && (n === undefined || a.hasQuery(e, o, n))) return ! 0;
                                return ! 1;
                            case "Object":
                                for (var l in t) if (b.call(t, l) && !a.hasQuery(e, l, t[l])) return ! 1;
                                return ! 0;
                            default:
                                throw new TypeError("URI.hasQuery() accepts a string, regular expression or object as the name parameter")
                        }
                        switch (i(n)) {
                            case "Undefined":
                                return t in e;
                            case "Boolean":
                                return n === Boolean(s(e[t]) ? e[t].length: e[t]);
                            case "Function":
                                return !! n(e[t], t, e);
                            case "Array":
                                if (!s(e[t])) return ! 1;
                                return (r ? c: u)(e[t], n);
                            case "RegExp":
                                return s(e[t]) ? !!r && c(e[t], n) : Boolean(e[t] && e[t].match(n));
                            case "Number":
                                n = String(n);
                            case "String":
                                return s(e[t]) ? !!r && c(e[t], n) : e[t] === n;
                            default:
                                throw new TypeError("URI.hasQuery() accepts undefined, boolean, string, number, RegExp, Function as the value parameter")
                        }
                    },
                    a.joinPaths = function() {
                        for (var e = [], t = [], n = 0, r = 0; r < arguments.length; r++) {
                            var o = new a(arguments[r]);
                            e.push(o);
                            for (var i = o.segment(), s = 0; s < i.length; s++)"string" == typeof i[s] && t.push(i[s]),
                            i[s] && n++
                        }
                        if (!t.length || !n) return new a("");
                        var l = new a("").segment(t);
                        return "" !== e[0].path() && "/" !== e[0].path().slice(0, 1) || l.path("/" + l.path()),
                            l.normalize()
                    },
                    a.commonPath = function(e, t) {
                        var n, r = Math.min(e.length, t.length);
                        for (n = 0; n < r; n++) if (e.charAt(n) !== t.charAt(n)) {
                            n--;
                            break
                        }
                        return n < 1 ? e.charAt(0) === t.charAt(0) && "/" === e.charAt(0) ? "/": "": ("/" === e.charAt(n) && "/" === t.charAt(n) || (n = e.substring(0, n).lastIndexOf("/")), e.substring(0, n + 1))
                    },
                    a.withinString = function(e, t, n) {
                        n || (n = {});
                        var r = n.start || a.findUri.start,
                            o = n.end || a.findUri.end,
                            i = n.trim || a.findUri.trim,
                            s = n.parens || a.findUri.parens,
                            l = /[a-z0-9-]=["']?$/i;
                        for (r.lastIndex = 0;;) {
                            var c = r.exec(e);
                            if (!c) break;
                            var u = c.index;
                            if (n.ignoreHtml) {
                                var d = e.slice(Math.max(u - 3, 0), u);
                                if (d && l.test(d)) continue
                            }
                            for (var h = u + e.slice(u).search(o), f = e.slice(u, h), p = -1;;) {
                                var m = s.exec(f);
                                if (!m) break;
                                var g = m.index + m[0].length;
                                p = Math.max(p, g)
                            }
                            if (! ((f = p > -1 ? f.slice(0, p) + f.slice(p).replace(i, "") : f.replace(i, "")).length <= c[0].length || n.ignore && n.ignore.test(f))) {
                                var v = t(f, u, h = u + f.length, e);
                                v !== undefined ? (v = String(v), e = e.slice(0, u) + v + e.slice(h), r.lastIndex = u + v.length) : r.lastIndex = h
                            }
                        }
                        return r.lastIndex = 0,
                            e
                    },
                    a.ensureValidHostname = function(e, n) {
                        var r = !!e,
                            o = !1;
                        if ( !! n && (o = c(a.hostProtocols, n)), o && !r) throw new TypeError("Hostname cannot be empty, if protocol is " + n);
                        if (e && e.match(a.invalid_hostname_characters)) {
                            if (!t) throw new TypeError('Hostname "' + e + '" contains characters other than [A-Z0-9.-:_] and Punycode.js is not available');
                            if (t.toASCII(e).match(a.invalid_hostname_characters)) throw new TypeError('Hostname "' + e + '" contains characters other than [A-Z0-9.-:_]')
                        }
                    },
                    a.ensureValidPort = function(e) {
                        if (e) {
                            var t = Number(e);
                            if (! (function(e) {
                                return /^[0-9]+$/.test(e)
                            } (t) && t > 0 && t < 65536)) throw new TypeError('Port "' + e + '" is not a valid port')
                        }
                    },
                    a.noConflict = function(t) {
                        if (t) {
                            var n = {
                                URI: this.noConflict()
                            };
                            return e.URITemplate && "function" == typeof e.URITemplate.noConflict && (n.URITemplate = e.URITemplate.noConflict()),
                            e.IPv6 && "function" == typeof e.IPv6.noConflict && (n.IPv6 = e.IPv6.noConflict()),
                            e.SecondLevelDomains && "function" == typeof e.SecondLevelDomains.noConflict && (n.SecondLevelDomains = e.SecondLevelDomains.noConflict()),
                                n
                        }
                        return e.URI === this && (e.URI = g),
                            this
                    },
                    v.build = function(e) {
                        return ! 0 === e ? this._deferred_build = !0 : (e === undefined || this._deferred_build) && (this._string = a.build(this._parts), this._deferred_build = !1),
                            this
                    },
                    v.clone = function() {
                        return new a(this)
                    },
                    v.valueOf = v.toString = function() {
                        return this.build(!1)._string
                    },
                    v.protocol = p("protocol"),
                    v.username = p("username"),
                    v.password = p("password"),
                    v.hostname = p("hostname"),
                    v.port = p("port"),
                    v.query = m("query", "?"),
                    v.fragment = m("fragment", "#"),
                    v.search = function(e, t) {
                        var n = this.query(e, t);
                        return "string" == typeof n && n.length ? "?" + n: n
                    },
                    v.hash = function(e, t) {
                        var n = this.fragment(e, t);
                        return "string" == typeof n && n.length ? "#" + n: n
                    },
                    v.pathname = function(e, t) {
                        if (e === undefined || !0 === e) {
                            var n = this._parts.path || (this._parts.hostname ? "/": "");
                            return e ? (this._parts.urn ? a.decodeUrnPath: a.decodePath)(n) : n
                        }
                        return this._parts.urn ? this._parts.path = e ? a.recodeUrnPath(e) : "": this._parts.path = e ? a.recodePath(e) : "/",
                            this.build(!t),
                            this
                    },
                    v.path = v.pathname,
                    v.href = function(e, t) {
                        var n;
                        if (e === undefined) return this.toString();
                        this._string = "",
                            this._parts = a._parts();
                        var r = e instanceof a,
                            o = "object" == typeof e && (e.hostname || e.path || e.pathname);
                        if (e.nodeName) {
                            e = e[a.getDomAttribute(e)] || "",
                                o = !1
                        }
                        if (!r && o && e.pathname !== undefined && (e = e.toString()), "string" == typeof e || e instanceof String) this._parts = a.parse(String(e), this._parts);
                        else {
                            if (!r && !o) throw new TypeError("invalid input");
                            var i = r ? e._parts: e;
                            for (n in i)"query" !== n && b.call(this._parts, n) && (this._parts[n] = i[n]);
                            i.query && this.query(i.query, !1)
                        }
                        return this.build(!t),
                            this
                    },
                    v.is = function(e) {
                        var t = !1,
                            n = !1,
                            o = !1,
                            i = !1,
                            s = !1,
                            l = !1,
                            c = !1,
                            u = !this._parts.urn;
                        switch (this._parts.hostname && (u = !1, n = a.ip4_expression.test(this._parts.hostname), o = a.ip6_expression.test(this._parts.hostname), s = (i = !(t = n || o)) && r && r.has(this._parts.hostname), l = i && a.idn_expression.test(this._parts.hostname), c = i && a.punycode_expression.test(this._parts.hostname)), e.toLowerCase()) {
                            case "relative":
                                return u;
                            case "absolute":
                                return ! u;
                            case "domain":
                            case "name":
                                return i;
                            case "sld":
                                return s;
                            case "ip":
                                return t;
                            case "ip4":
                            case "ipv4":
                            case "inet4":
                                return n;
                            case "ip6":
                            case "ipv6":
                            case "inet6":
                                return o;
                            case "idn":
                                return l;
                            case "url":
                                return ! this._parts.urn;
                            case "urn":
                                return !! this._parts.urn;
                            case "punycode":
                                return c
                        }
                        return null
                    };
                var x = v.protocol,
                    S = v.port,
                    T = v.hostname;
                v.protocol = function(e, t) {
                    if (e && !(e = e.replace(/:(\/\/)?$/, "")).match(a.protocol_expression)) throw new TypeError('Protocol "' + e + "\" contains characters other than [A-Z0-9.+-] or doesn't start with [A-Z]");
                    return x.call(this, e, t)
                },
                    v.scheme = v.protocol,
                    v.port = function(e, t) {
                        return this._parts.urn ? e === undefined ? "": this: (e !== undefined && (0 === e && (e = null), e && (":" === (e += "").charAt(0) && (e = e.substring(1)), a.ensureValidPort(e))), S.call(this, e, t))
                    },
                    v.hostname = function(e, t) {
                        if (this._parts.urn) return e === undefined ? "": this;
                        if (e !== undefined) {
                            var n = {
                                preventInvalidHostname: this._parts.preventInvalidHostname
                            };
                            if ("/" !== a.parseHost(e, n)) throw new TypeError('Hostname "' + e + '" contains characters other than [A-Z0-9.-]');
                            e = n.hostname,
                            this._parts.preventInvalidHostname && a.ensureValidHostname(e, this._parts.protocol)
                        }
                        return T.call(this, e, t)
                    },
                    v.origin = function(e, t) {
                        if (this._parts.urn) return e === undefined ? "": this;
                        if (e === undefined) {
                            var n = this.protocol();
                            return this.authority() ? (n ? n + "://": "") + this.authority() : ""
                        }
                        var r = a(e);
                        return this.protocol(r.protocol()).authority(r.authority()).build(!t),
                            this
                    },
                    v.host = function(e, t) {
                        if (this._parts.urn) return e === undefined ? "": this;
                        if (e === undefined) return this._parts.hostname ? a.buildHost(this._parts) : "";
                        if ("/" !== a.parseHost(e, this._parts)) throw new TypeError('Hostname "' + e + '" contains characters other than [A-Z0-9.-]');
                        return this.build(!t),
                            this
                    },
                    v.authority = function(e, t) {
                        if (this._parts.urn) return e === undefined ? "": this;
                        if (e === undefined) return this._parts.hostname ? a.buildAuthority(this._parts) : "";
                        if ("/" !== a.parseAuthority(e, this._parts)) throw new TypeError('Hostname "' + e + '" contains characters other than [A-Z0-9.-]');
                        return this.build(!t),
                            this
                    },
                    v.userinfo = function(e, t) {
                        if (this._parts.urn) return e === undefined ? "": this;
                        if (e === undefined) {
                            var n = a.buildUserinfo(this._parts);
                            return n ? n.substring(0, n.length - 1) : n
                        }
                        return "@" !== e[e.length - 1] && (e += "@"),
                            a.parseUserinfo(e, this._parts),
                            this.build(!t),
                            this
                    },
                    v.resource = function(e, t) {
                        var n;
                        return e === undefined ? this.path() + this.search() + this.hash() : (n = a.parse(e), this._parts.path = n.path, this._parts.query = n.query, this._parts.fragment = n.fragment, this.build(!t), this)
                    },
                    v.subdomain = function(e, t) {
                        if (this._parts.urn) return e === undefined ? "": this;
                        if (e === undefined) {
                            if (!this._parts.hostname || this.is("IP")) return "";
                            var n = this._parts.hostname.length - this.domain().length - 1;
                            return this._parts.hostname.substring(0, n) || ""
                        }
                        var r = this._parts.hostname.length - this.domain().length,
                            i = this._parts.hostname.substring(0, r),
                            s = new RegExp("^" + o(i));
                        if (e && "." !== e.charAt(e.length - 1) && (e += "."), -1 !== e.indexOf(":")) throw new TypeError("Domains cannot contain colons");
                        return e && a.ensureValidHostname(e, this._parts.protocol),
                            this._parts.hostname = this._parts.hostname.replace(s, e),
                            this.build(!t),
                            this
                    },
                    v.domain = function(e, t) {
                        if (this._parts.urn) return e === undefined ? "": this;
                        if ("boolean" == typeof e && (t = e, e = undefined), e === undefined) {
                            if (!this._parts.hostname || this.is("IP")) return "";
                            var n = this._parts.hostname.match(/\./g);
                            if (n && n.length < 2) return this._parts.hostname;
                            var r = this._parts.hostname.length - this.tld(t).length - 1;
                            return r = this._parts.hostname.lastIndexOf(".", r - 1) + 1,
                            this._parts.hostname.substring(r) || ""
                        }
                        if (!e) throw new TypeError("cannot set domain empty");
                        if ( - 1 !== e.indexOf(":")) throw new TypeError("Domains cannot contain colons");
                        if (a.ensureValidHostname(e, this._parts.protocol), !this._parts.hostname || this.is("IP")) this._parts.hostname = e;
                        else {
                            var i = new RegExp(o(this.domain()) + "$");
                            this._parts.hostname = this._parts.hostname.replace(i, e)
                        }
                        return this.build(!t),
                            this
                    },
                    v.tld = function(e, t) {
                        if (this._parts.urn) return e === undefined ? "": this;
                        if ("boolean" == typeof e && (t = e, e = undefined), e === undefined) {
                            if (!this._parts.hostname || this.is("IP")) return "";
                            var n = this._parts.hostname.lastIndexOf("."),
                                a = this._parts.hostname.substring(n + 1);
                            return ! 0 !== t && r && r.list[a.toLowerCase()] ? r.get(this._parts.hostname) || a: a
                        }
                        var i;
                        if (!e) throw new TypeError("cannot set TLD empty");
                        if (e.match(/[^a-zA-Z0-9-]/)) {
                            if (!r || !r.is(e)) throw new TypeError('TLD "' + e + '" contains characters other than [A-Z0-9]');
                            i = new RegExp(o(this.tld()) + "$"),
                                this._parts.hostname = this._parts.hostname.replace(i, e)
                        } else {
                            if (!this._parts.hostname || this.is("IP")) throw new ReferenceError("cannot set TLD on non-domain host");
                            i = new RegExp(o(this.tld()) + "$"),
                                this._parts.hostname = this._parts.hostname.replace(i, e)
                        }
                        return this.build(!t),
                            this
                    },
                    v.directory = function(e, t) {
                        if (this._parts.urn) return e === undefined ? "": this;
                        if (e === undefined || !0 === e) {
                            if (!this._parts.path && !this._parts.hostname) return "";
                            if ("/" === this._parts.path) return "/";
                            var n = this._parts.path.length - this.filename().length - 1,
                                r = this._parts.path.substring(0, n) || (this._parts.hostname ? "/": "");
                            return e ? a.decodePath(r) : r
                        }
                        var i = this._parts.path.length - this.filename().length,
                            s = this._parts.path.substring(0, i),
                            l = new RegExp("^" + o(s));
                        return this.is("relative") || (e || (e = "/"), "/" !== e.charAt(0) && (e = "/" + e)),
                        e && "/" !== e.charAt(e.length - 1) && (e += "/"),
                            e = a.recodePath(e),
                            this._parts.path = this._parts.path.replace(l, e),
                            this.build(!t),
                            this
                    },
                    v.filename = function(e, t) {
                        if (this._parts.urn) return e === undefined ? "": this;
                        if ("string" != typeof e) {
                            if (!this._parts.path || "/" === this._parts.path) return "";
                            var n = this._parts.path.lastIndexOf("/"),
                                r = this._parts.path.substring(n + 1);
                            return e ? a.decodePathSegment(r) : r
                        }
                        var i = !1;
                        "/" === e.charAt(0) && (e = e.substring(1)),
                        e.match(/\.?\//) && (i = !0);
                        var s = new RegExp(o(this.filename()) + "$");
                        return e = a.recodePath(e),
                            this._parts.path = this._parts.path.replace(s, e),
                            i ? this.normalizePath(t) : this.build(!t),
                            this
                    },
                    v.suffix = function(e, t) {
                        if (this._parts.urn) return e === undefined ? "": this;
                        if (e === undefined || !0 === e) {
                            if (!this._parts.path || "/" === this._parts.path) return "";
                            var n, r, i = this.filename(),
                                s = i.lastIndexOf(".");
                            return - 1 === s ? "": (n = i.substring(s + 1), r = /^[a-z0-9%]+$/i.test(n) ? n: "", e ? a.decodePathSegment(r) : r)
                        }
                        "." === e.charAt(0) && (e = e.substring(1));
                        var l, c = this.suffix();
                        if (c) l = e ? new RegExp(o(c) + "$") : new RegExp(o("." + c) + "$");
                        else {
                            if (!e) return this;
                            this._parts.path += "." + a.recodePath(e)
                        }
                        return l && (e = a.recodePath(e), this._parts.path = this._parts.path.replace(l, e)),
                            this.build(!t),
                            this
                    },
                    v.segment = function(e, t, n) {
                        var r = this._parts.urn ? ":": "/",
                            a = this.path(),
                            o = "/" === a.substring(0, 1),
                            i = a.split(r);
                        if (e !== undefined && "number" != typeof e && (n = t, t = e, e = undefined), e !== undefined && "number" != typeof e) throw new Error('Bad segment "' + e + '", must be 0-based integer');
                        if (o && i.shift(), e < 0 && (e = Math.max(i.length + e, 0)), t === undefined) return e === undefined ? i: i[e];
                        if (null === e || i[e] === undefined) if (s(t)) {
                            i = [];
                            for (var l = 0,
                                     c = t.length; l < c; l++)(t[l].length || i.length && i[i.length - 1].length) && (i.length && !i[i.length - 1].length && i.pop(), i.push(d(t[l])))
                        } else(t || "string" == typeof t) && (t = d(t), "" === i[i.length - 1] ? i[i.length - 1] = t: i.push(t));
                        else t ? i[e] = d(t) : i.splice(e, 1);
                        return o && i.unshift(""),
                            this.path(i.join(r), n)
                    },
                    v.segmentCoded = function(e, t, n) {
                        var r, o, i;
                        if ("number" != typeof e && (n = t, t = e, e = undefined), t === undefined) {
                            if (r = this.segment(e, t, n), s(r)) for (o = 0, i = r.length; o < i; o++) r[o] = a.decode(r[o]);
                            else r = r !== undefined ? a.decode(r) : undefined;
                            return r
                        }
                        if (s(t)) for (o = 0, i = t.length; o < i; o++) t[o] = a.encode(t[o]);
                        else t = "string" == typeof t || t instanceof String ? a.encode(t) : t;
                        return this.segment(e, t, n)
                    };
                var A = v.query;
                return v.query = function(e, t) {
                    if (!0 === e) return a.parseQuery(this._parts.query, this._parts.escapeQuerySpace);
                    if ("function" == typeof e) {
                        var n = a.parseQuery(this._parts.query, this._parts.escapeQuerySpace),
                            r = e.call(this, n);
                        return this._parts.query = a.buildQuery(r || n, this._parts.duplicateQueryParameters, this._parts.escapeQuerySpace),
                            this.build(!t),
                            this
                    }
                    return e !== undefined && "string" != typeof e ? (this._parts.query = a.buildQuery(e, this._parts.duplicateQueryParameters, this._parts.escapeQuerySpace), this.build(!t), this) : A.call(this, e, t)
                },
                    v.setQuery = function(e, t, n) {
                        var r = a.parseQuery(this._parts.query, this._parts.escapeQuerySpace);
                        if ("string" == typeof e || e instanceof String) r[e] = t !== undefined ? t: null;
                        else {
                            if ("object" != typeof e) throw new TypeError("URI.addQuery() accepts an object, string as the name parameter");
                            for (var o in e) b.call(e, o) && (r[o] = e[o])
                        }
                        return this._parts.query = a.buildQuery(r, this._parts.duplicateQueryParameters, this._parts.escapeQuerySpace),
                        "string" != typeof e && (n = t),
                            this.build(!n),
                            this
                    },
                    v.addQuery = function(e, t, n) {
                        var r = a.parseQuery(this._parts.query, this._parts.escapeQuerySpace);
                        return a.addQuery(r, e, t === undefined ? null: t),
                            this._parts.query = a.buildQuery(r, this._parts.duplicateQueryParameters, this._parts.escapeQuerySpace),
                        "string" != typeof e && (n = t),
                            this.build(!n),
                            this
                    },
                    v.removeQuery = function(e, t, n) {
                        var r = a.parseQuery(this._parts.query, this._parts.escapeQuerySpace);
                        return a.removeQuery(r, e, t),
                            this._parts.query = a.buildQuery(r, this._parts.duplicateQueryParameters, this._parts.escapeQuerySpace),
                        "string" != typeof e && (n = t),
                            this.build(!n),
                            this
                    },
                    v.hasQuery = function(e, t, n) {
                        var r = a.parseQuery(this._parts.query, this._parts.escapeQuerySpace);
                        return a.hasQuery(r, e, t, n)
                    },
                    v.setSearch = v.setQuery,
                    v.addSearch = v.addQuery,
                    v.removeSearch = v.removeQuery,
                    v.hasSearch = v.hasQuery,
                    v.normalize = function() {
                        return this._parts.urn ? this.normalizeProtocol(!1).normalizePath(!1).normalizeQuery(!1).normalizeFragment(!1).build() : this.normalizeProtocol(!1).normalizeHostname(!1).normalizePort(!1).normalizePath(!1).normalizeQuery(!1).normalizeFragment(!1).build()
                    },
                    v.normalizeProtocol = function(e) {
                        return "string" == typeof this._parts.protocol && (this._parts.protocol = this._parts.protocol.toLowerCase(), this.build(!e)),
                            this
                    },
                    v.normalizeHostname = function(e) {
                        return this._parts.hostname && (this.is("IDN") && t ? this._parts.hostname = t.toASCII(this._parts.hostname) : this.is("IPv6") && n && (this._parts.hostname = n.best(this._parts.hostname)), this._parts.hostname = this._parts.hostname.toLowerCase(), this.build(!e)),
                            this
                    },
                    v.normalizePort = function(e) {
                        return "string" == typeof this._parts.protocol && this._parts.port === a.defaultPorts[this._parts.protocol] && (this._parts.port = null, this.build(!e)),
                            this
                    },
                    v.normalizePath = function(e) {
                        var t = this._parts.path;
                        if (!t) return this;
                        if (this._parts.urn) return this._parts.path = a.recodeUrnPath(this._parts.path),
                            this.build(!e),
                            this;
                        if ("/" === this._parts.path) return this;
                        var n, r, o, i = "";
                        for ("/" !== (t = a.recodePath(t)).charAt(0) && (n = !0, t = "/" + t), "/.." !== t.slice( - 3) && "/." !== t.slice( - 2) || (t += "/"), t = t.replace(/(\/(\.\/)+)|(\/\.$)/g, "/").replace(/\/{2,}/g, "/"), n && (i = t.substring(1).match(/^(\.\.\/)+/) || "") && (i = i[0]); - 1 !== (r = t.search(/\/\.\.(\/|$)/));) 0 !== r ? ( - 1 === (o = t.substring(0, r).lastIndexOf("/")) && (o = r), t = t.substring(0, o) + t.substring(r + 3)) : t = t.substring(3);
                        return n && this.is("relative") && (t = i + t.substring(1)),
                            this._parts.path = t,
                            this.build(!e),
                            this
                    },
                    v.normalizePathname = v.normalizePath,
                    v.normalizeQuery = function(e) {
                        return "string" == typeof this._parts.query && (this._parts.query.length ? this.query(a.parseQuery(this._parts.query, this._parts.escapeQuerySpace)) : this._parts.query = null, this.build(!e)),
                            this
                    },
                    v.normalizeFragment = function(e) {
                        return this._parts.fragment || (this._parts.fragment = null, this.build(!e)),
                            this
                    },
                    v.normalizeSearch = v.normalizeQuery,
                    v.normalizeHash = v.normalizeFragment,
                    v.iso8859 = function() {
                        var e = a.encode,
                            t = a.decode;
                        a.encode = escape,
                            a.decode = decodeURIComponent;
                        try {
                            this.normalize()
                        } finally {
                            a.encode = e,
                                a.decode = t
                        }
                        return this
                    },
                    v.unicode = function() {
                        var e = a.encode,
                            t = a.decode;
                        a.encode = f,
                            a.decode = unescape;
                        try {
                            this.normalize()
                        } finally {
                            a.encode = e,
                                a.decode = t
                        }
                        return this
                    },
                    v.readable = function() {
                        var e = this.clone();
                        e.username("").password("").normalize();
                        var n = "";
                        if (e._parts.protocol && (n += e._parts.protocol + "://"), e._parts.hostname && (e.is("punycode") && t ? (n += t.toUnicode(e._parts.hostname), e._parts.port && (n += ":" + e._parts.port)) : n += e.host()), e._parts.hostname && e._parts.path && "/" !== e._parts.path.charAt(0) && (n += "/"), n += e.path(!0), e._parts.query) {
                            for (var r = "",
                                     o = 0,
                                     i = e._parts.query.split("&"), s = i.length; o < s; o++) {
                                var l = (i[o] || "").split("=");
                                r += "&" + a.decodeQuery(l[0], this._parts.escapeQuerySpace).replace(/&/g, "%26"),
                                l[1] !== undefined && (r += "=" + a.decodeQuery(l[1], this._parts.escapeQuerySpace).replace(/&/g, "%26"))
                            }
                            n += "?" + r.substring(1)
                        }
                        return n += a.decodeQuery(e.hash(), !0)
                    },
                    v.absoluteTo = function(e) {
                        var t, n, r, o = this.clone(),
                            i = ["protocol", "username", "password", "hostname", "port"];
                        if (this._parts.urn) throw new Error("URNs do not have any generally defined hierarchical components");
                        if (e instanceof a || (e = new a(e)), o._parts.protocol) return o;
                        if (o._parts.protocol = e._parts.protocol, this._parts.hostname) return o;
                        for (n = 0; r = i[n]; n++) o._parts[r] = e._parts[r];
                        return o._parts.path ? (".." === o._parts.path.substring( - 2) && (o._parts.path += "/"), "/" !== o.path().charAt(0) && (t = (t = e.directory()) || (0 === e.path().indexOf("/") ? "/": ""), o._parts.path = (t ? t + "/": "") + o._parts.path, o.normalizePath())) : (o._parts.path = e._parts.path, o._parts.query || (o._parts.query = e._parts.query)),
                            o.build(),
                            o
                    },
                    v.relativeTo = function(e) {
                        var t, n, r, o, i, s = this.clone().normalize();
                        if (s._parts.urn) throw new Error("URNs do not have any generally defined hierarchical components");
                        if (e = new a(e).normalize(), t = s._parts, n = e._parts, o = s.path(), i = e.path(), "/" !== o.charAt(0)) throw new Error("URI is already relative");
                        if ("/" !== i.charAt(0)) throw new Error("Cannot calculate a URI relative to another relative URI");
                        if (t.protocol === n.protocol && (t.protocol = null), t.username !== n.username || t.password !== n.password) return s.build();
                        if (null !== t.protocol || null !== t.username || null !== t.password) return s.build();
                        if (t.hostname !== n.hostname || t.port !== n.port) return s.build();
                        if (t.hostname = null, t.port = null, o === i) return t.path = "",
                            s.build();
                        if (! (r = a.commonPath(o, i))) return s.build();
                        var l = n.path.substring(r.length).replace(/[^\/]*$/, "").replace(/.*?\//g, "../");
                        return t.path = l + t.path.substring(r.length) || "./",
                            s.build()
                    },
                    v.equals = function(e) {
                        var t, n, r, o = this.clone(),
                            i = new a(e),
                            l = {},
                            c = {},
                            d = {};
                        if (o.normalize(), i.normalize(), o.toString() === i.toString()) return ! 0;
                        if (t = o.query(), n = i.query(), o.query(""), i.query(""), o.toString() !== i.toString()) return ! 1;
                        if (t.length !== n.length) return ! 1;
                        l = a.parseQuery(t, this._parts.escapeQuerySpace),
                            c = a.parseQuery(n, this._parts.escapeQuerySpace);
                        for (r in l) if (b.call(l, r)) {
                            if (s(l[r])) {
                                if (!u(l[r], c[r])) return ! 1
                            } else if (l[r] !== c[r]) return ! 1;
                            d[r] = !0
                        }
                        for (r in c) if (b.call(c, r) && !d[r]) return ! 1;
                        return ! 0
                    },
                    v.preventInvalidHostname = function(e) {
                        return this._parts.preventInvalidHostname = !!e,
                            this
                    },
                    v.duplicateQueryParameters = function(e) {
                        return this._parts.duplicateQueryParameters = !!e,
                            this
                    },
                    v.escapeQuerySpace = function(e) {
                        return this._parts.escapeQuerySpace = !!e,
                            this
                    },
                    a
            }
        },
        "./ClientApp/modules/uri/src/URITemplate.js": function(e, t) {
            e.exports = function(e, t) {
                "use strict";
                function n(e) {
                    return n._cache[e] ? n._cache[e] : this instanceof n ? (this.expression = e, n._cache[e] = this, this) : new n(e)
                }
                function r(e) {
                    this.data = e,
                        this.cache = {}
                }
                var a = e && e.URITemplate,
                    o = Object.prototype.hasOwnProperty,
                    i = n.prototype,
                    s = {
                        "": {
                            prefix: "",
                            separator: ",",
                            named: !1,
                            empty_name_separator: !1,
                            encode: "encode"
                        },
                        "+": {
                            prefix: "",
                            separator: ",",
                            named: !1,
                            empty_name_separator: !1,
                            encode: "encodeReserved"
                        },
                        "#": {
                            prefix: "#",
                            separator: ",",
                            named: !1,
                            empty_name_separator: !1,
                            encode: "encodeReserved"
                        },
                        ".": {
                            prefix: ".",
                            separator: ".",
                            named: !1,
                            empty_name_separator: !1,
                            encode: "encode"
                        },
                        "/": {
                            prefix: "/",
                            separator: "/",
                            named: !1,
                            empty_name_separator: !1,
                            encode: "encode"
                        },
                        ";": {
                            prefix: ";",
                            separator: ";",
                            named: !0,
                            empty_name_separator: !1,
                            encode: "encode"
                        },
                        "?": {
                            prefix: "?",
                            separator: "&",
                            named: !0,
                            empty_name_separator: !0,
                            encode: "encode"
                        },
                        "&": {
                            prefix: "&",
                            separator: "&",
                            named: !0,
                            empty_name_separator: !0,
                            encode: "encode"
                        }
                    };
                return n._cache = {},
                    n.EXPRESSION_PATTERN = /\{([^a-zA-Z0-9%_]?)([^\}]+)(\}|$)/g,
                    n.VARIABLE_PATTERN = /^([^*:.](?:\.?[^*:.])*)((\*)|:(\d+))?$/,
                    n.VARIABLE_NAME_PATTERN = /[^a-zA-Z0-9%_.]/,
                    n.LITERAL_PATTERN = /[<>{}"`^| \\]/,
                    n.expand = function(e, t, r) {
                        var a, o, i, l = s[e.operator],
                            c = l.named ? "Named": "Unnamed",
                            u = e.variables,
                            d = [];
                        for (i = 0; o = u[i]; i++) {
                            if (0 === (a = t.get(o.name)).type && r && r.strict) throw new Error('Missing expansion value for variable "' + o.name + '"');
                            if (a.val.length) {
                                if (a.type > 1 && o.maxlength) throw new Error('Invalid expression: Prefix modifier not applicable to variable "' + o.name + '"');
                                d.push(n["expand" + c](a, l, o.explode, o.explode && l.separator || ",", o.maxlength, o.name))
                            } else a.type && d.push("")
                        }
                        return d.length ? l.prefix + d.join(l.separator) : ""
                    },
                    n.expandNamed = function(e, n, r, a, o, i) {
                        var s, l, c, u = "",
                            d = n.encode,
                            h = n.empty_name_separator,
                            f = !e[d].length,
                            p = 2 === e.type ? "": t[d](i);
                        for (l = 0, c = e.val.length; l < c; l++) o ? (s = t[d](e.val[l][1].substring(0, o)), 2 === e.type && (p = t[d](e.val[l][0].substring(0, o)))) : f ? (s = t[d](e.val[l][1]), 2 === e.type ? (p = t[d](e.val[l][0]), e[d].push([p, s])) : e[d].push([undefined, s])) : (s = e[d][l][1], 2 === e.type && (p = e[d][l][0])),
                        u && (u += a),
                            r ? u += p + (h || s ? "=": "") + s: (l || (u += t[d](i) + (h || s ? "=": "")), 2 === e.type && (u += p + ","), u += s);
                        return u
                    },
                    n.expandUnnamed = function(e, n, r, a, o) {
                        var i, s, l, c = "",
                            u = n.encode,
                            d = n.empty_name_separator,
                            h = !e[u].length;
                        for (s = 0, l = e.val.length; s < l; s++) o ? i = t[u](e.val[s][1].substring(0, o)) : h ? (i = t[u](e.val[s][1]), e[u].push([2 === e.type ? t[u](e.val[s][0]) : undefined, i])) : i = e[u][s][1],
                        c && (c += a),
                        2 === e.type && (c += o ? t[u](e.val[s][0].substring(0, o)) : e[u][s][0], c += r ? d || i ? "=": "": ","),
                            c += i;
                        return c
                    },
                    n.noConflict = function() {
                        return e.URITemplate === n && (e.URITemplate = a),
                            n
                    },
                    i.expand = function(e, t) {
                        var a = "";
                        this.parts && this.parts.length || this.parse(),
                        e instanceof r || (e = new r(e));
                        for (var o = 0,
                                 i = this.parts.length; o < i; o++) a += "string" == typeof this.parts[o] ? this.parts[o] : n.expand(this.parts[o], e, t);
                        return a
                    },
                    i.parse = function() {
                        var e, t, r, a = this.expression,
                            o = n.EXPRESSION_PATTERN,
                            i = n.VARIABLE_PATTERN,
                            l = n.VARIABLE_NAME_PATTERN,
                            c = n.LITERAL_PATTERN,
                            u = [],
                            d = 0,
                            h = function(e) {
                                if (e.match(c)) throw new Error('Invalid Literal "' + e + '"');
                                return e
                            };
                        for (o.lastIndex = 0;;) {
                            if (null === (t = o.exec(a))) {
                                u.push(h(a.substring(d)));
                                break
                            }
                            if (u.push(h(a.substring(d, t.index))), d = t.index + t[0].length, !s[t[1]]) throw new Error('Unknown Operator "' + t[1] + '" in "' + t[0] + '"');
                            if (!t[3]) throw new Error('Unclosed Expression "' + t[0] + '"');
                            for (var f = 0,
                                     p = (e = t[2].split(",")).length; f < p; f++) {
                                if (null === (r = e[f].match(i))) throw new Error('Invalid Variable "' + e[f] + '" in "' + t[0] + '"');
                                if (r[1].match(l)) throw new Error('Invalid Variable Name "' + r[1] + '" in "' + t[0] + '"');
                                e[f] = {
                                    name: r[1],
                                    explode: !!r[3],
                                    maxlength: r[4] && parseInt(r[4], 10)
                                }
                            }
                            if (!e.length) throw new Error('Expression Missing Variable(s) "' + t[0] + '"');
                            u.push({
                                expression: t[0],
                                operator: t[1],
                                variables: e
                            })
                        }
                        return u.length || u.push(h(a)),
                            this.parts = u,
                            this
                    },
                    r.prototype.get = function(e) {
                        var t, n, r, a = this.data,
                            i = {
                                type: 0,
                                val: [],
                                encode: [],
                                encodeReserved: []
                            };
                        if (this.cache[e] !== undefined) return this.cache[e];
                        if (this.cache[e] = i, (r = "[object Function]" === String(Object.prototype.toString.call(a)) ? a(e) : "[object Function]" === String(Object.prototype.toString.call(a[e])) ? a[e](e) : a[e]) === undefined || null === r) return i;
                        if ("[object Array]" === String(Object.prototype.toString.call(r))) {
                            for (t = 0, n = r.length; t < n; t++) r[t] !== undefined && null !== r[t] && i.val.push([undefined, String(r[t])]);
                            i.val.length && (i.type = 3)
                        } else if ("[object Object]" === String(Object.prototype.toString.call(r))) {
                            for (t in r) o.call(r, t) && r[t] !== undefined && null !== r[t] && i.val.push([t, String(r[t])]);
                            i.val.length && (i.type = 2)
                        } else i.type = 1,
                            i.val.push([undefined, String(r)]);
                        return i
                    },
                    t.expand = function(e, r) {
                        var a = new n(e).expand(r);
                        return new t(a)
                    },
                    n
            }
        },
        "./ClientApp/modules/utils.cache.js": function(e, t, n) {
            var r = n("./ClientApp/modules/utils.extend.js");
            e.exports = function(e) {
                e && r(this, e || {},
                    !1),
                    this.getOrAdd = function(e, t) {
                        return "undefined" == typeof this[e] && (this[e] = t),
                            this[e]
                    },
                    this.set = function(e, t) {
                        return void 0 !== t && (this[e] = t),
                            this[e]
                    },
                    this.remove = function(e) {
                        var t = this[e];
                        if ("function" == typeof t) return t;
                        try {
                            delete this[e]
                        } catch(n) {
                            this[e] = undefined
                        }
                        return t
                    },
                    this.clear = function() {
                        for (var e in this) this.hasOwnProperty(e) && this.remove(e);
                        return this
                    }
            }
        },
        "./ClientApp/modules/utils.cookie.js": function(e, t, n) {
            function r(e) {
                return s.raw ? e: decodeURIComponent(e)
            }
            function a(e, t) {
                var n = s.raw ? e: function(e) {
                    0 === e.indexOf('"') && (e = e.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, "\\"));
                    try {
                        return e = decodeURIComponent(e.replace(i, " ")),
                            s.json ? JSON.parse(e) : e
                    } catch(t) {}
                } (e);
                return "function" == typeof t ? t(n) : n
            }
            var o = n("./ClientApp/modules/utils.extend.js"),
                i = /\+/g,
                s = function(e, t, n) {
                    undefined;
                    for (var o = e ? undefined: {},
                             i = document.cookie ? document.cookie.split("; ") : [], s = 0, l = i.length; s < l; s++) {
                        var c = i[s].split("="),
                            u = r(c.shift()),
                            d = c.join("=");
                        if (e && e === u) {
                            o = a(d, t);
                            break
                        }
                        e || (d = a(d)) === undefined || (o[u] = d)
                    }
                    return o
                };
            s.defaults = {},
                s.remove = function(e, t) {
                    return s(e) !== undefined && (s(e, "", o(t || {},
                        {
                            expires: -1
                        })), !s(e))
                },
                s.hasOwnProperty = function(e) {
                    return new RegExp("(?:^|;\\s*)" + escape(e).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=").test(document.cookie)
                },
                s.key = function(e) {
                    return unescape(document.cookie.replace(/\s*\=(?:.(?!;))*$/, "").split(/\s*\=(?:[^;](?!;))*[^;]?;\s*/)[e])
                },
                e.exports = s
        },
        "./ClientApp/modules/utils.extend.js": function(e, t) {
            function n(e, t, r) {
                e = e || {};
                var a, o = typeof t,
                    i = 1;
                for ("undefined" !== o && "boolean" !== o || (r = "boolean" === o && t, t = e, e = this), "object" != typeof t && "[object Function]" !== Object.prototype.toString.call(t) && (t = {}); i <= 2;) {
                    if (null != (a = 1 === i ? e: t)) for (var s in a) {
                        var l = e[s],
                            c = a[s];
                        e !== c && (r && c && "object" == typeof c && !c.nodeType ? e[s] = n(l || (null != c.length ? [] : {}), c, r) : c !== undefined && (e[s] = c))
                    }
                    i++
                }
                return e
            }
            e.exports = n
        },
        "./ClientApp/modules/utils.js": function(e, t, n) {
            function r(e, t, n) {
                "string" != typeof n && (n = "...");
                var r = 0,
                    o = 0;
                str_cut = new String;
                for (var i = 0; i < e.length; i++) a = e.charAt(i),
                    r++,
                escape(a).length > 4 && r++,
                r <= t && o++;
                return r <= t ? e.toString() : e.substr(0, o).concat(n)
            }
            function o(e, t, n) {
                if (t = t || "yyyy-MM-dd HH:mm:ss", "string" == typeof e && (e = new Date(e.replace(/-/g, "/").replace("T", " ").split("+")[0])), isNaN(e.getTime())) return n || "";
                var r = {
                        "M+": e.getMonth() + 1,
                        "d+": e.getDate(),
                        "h+": e.getHours() % 12 == 0 ? 12 : e.getHours() % 12,
                        "H+": e.getHours(),
                        "m+": e.getMinutes(),
                        "s+": e.getSeconds(),
                        "q+": Math.floor((e.getMonth() + 3) / 3),
                        S: e.getMilliseconds()
                    },
                    a = {
                        0 : "日",
                        1 : "一",
                        2 : "二",
                        3 : "三",
                        4 : "四",
                        5 : "五",
                        6 : "六"
                    };
                /(y+)/.test(t) && (t = t.replace(RegExp.$1, (e.getFullYear() + "").substr(4 - RegExp.$1.length))),
                /(E+)/.test(t) && (t = t.replace(RegExp.$1, (RegExp.$1.length > 1 ? RegExp.$1.length > 2 ? "星期": "周": "") + a[e.getDay() + ""]));
                for (var o in r) new RegExp("(" + o + ")").test(t) && (t = t.replace(RegExp.$1, 1 == RegExp.$1.length ? r[o] : ("00" + r[o]).substr(("" + r[o]).length)));
                return t
            }
            function i(e) {
                return "object" == typeof HTMLElement ? e instanceof HTMLElement: e && "object" == typeof e && 1 === e.nodeType && "string" == typeof e.nodeName
            }
            function s(e) {
                var t = parseFloat(e);
                if (!isNaN(t)) {
                    var n = t < 0 ? -1 : t > 0 ? 1 : 0;
                    return t < 0 && (t *= -1),
                        t > 0 && t < 1e4 || t < 0 && t > -1e4 ? (t * n).toString() : t > 0 && t < 1e6 || t < 0 && t > -1e6 ? (t /= 1e4).toFixed(2) * n + "万": t > 0 && t < 1e7 || t < 0 && t > -1e7 ? (t /= 1e4).toFixed(1) * n + "万": t > 0 && t < 1e8 || t < 0 && t > -1e8 ? (t /= 1e4).toFixed(0) * n + "万": t > 0 && t < 1e10 || t < 0 && t > -1e10 ? (t /= 1e8).toFixed(2) * n + "亿": t > 0 && t < 1e11 || t < 0 && t > -1e11 ? (t /= 1e8).toFixed(1) * n + "亿": t > 0 && t < 1e12 || t < 0 && t > -1e12 ? (t /= 1e8).toFixed(0) * n + "亿": t > 0 && t < 1e14 || t < 0 && t > -1e14 ? (t /= 1e12).toFixed(2) + "万亿": t > 0 && t < 1e15 || t < 0 && t > -1e15 ? (t /= 1e12).toFixed(1) * n + "万亿": t > 0 && t < 1e16 || t < 0 && t > -1e16 ? (t /= 1e12).toFixed(0) * n + "万亿": t
                }
                return "-"
            }
            var l = n("./ClientApp/modules/utils.extend.js"),
                c = n("./ClientApp/modules/utils.cache.js");
            e.exports = {
                extend: l,
                isDOM: i,
                ObjectCache: c,
                formatDate: o,
                getQueryString: function(e) {
                    var t = new RegExp("(^|&)" + e + "=([^&]*)(&|$)", "i"),
                        n = window.location.search.substr(1).match(t);
                    return null != n ? unescape(n[2]) : null
                },
                cutstr: r,
                getMarketCode: function(e) {
                    var t = sc.substring(0, 1),
                        n = sc.substring(0, 3);
                    return "5" == t || "6" == t || "9" == t ? "1": "009" == n || "126" == n || "110" == n ? "1": "2"
                },
                getColor: function() {
                    var e = 0;
                    return arguments[1] ? e = parseFloat(arguments[0]) - parseFloat(arguments[1]) : arguments[0] && (e = parseFloat(arguments[0])),
                        e > 0 ? "red": e < 0 ? "green": ""
                },
                fixMarket: function(e) {
                    var t = e.substr(0, 1),
                        n = e.substr(0, 3);
                    return "5" == t || "6" == t || "9" == t ? "1": "009" == n || "126" == n || "110" == n || "201" == n || "202" == n || "203" == n || "204" == n ? "1": "2"
                },
                numbericFormat: s,
                blinker: function(e) {
                    var t = l({
                            doms: [],
                            color: {
                                up: ["#FFDDDD", "#FFEEEE", ""],
                                down: ["#b4f7af", "#ccffcc", ""],
                                others: ["#b2c3ea", "#cedaf5", ""]
                            },
                            interval: 300,
                            blinktime: 150,
                            circle: 2
                        },
                        e),
                        n = this;
                    n.raise = !1,
                        n.loop = 0;
                    for (var r, a = [], o = 0; o < t.doms.length; o++) {
                        var s = t.doms[o];
                        i(s) ? a.push(s) : "string" == typeof t.doms[o] && (s = mini(t.doms[o])) && a.push(s)
                    }
                    r = setInterval(function() {
                            if (n.raise) {
                                for (var e = n.comparer > 0 ? t.color.up: n.comparer < 0 ? t.color.down: t.color.others, r = 0; r < e.length * t.circle; r++) setTimeout(function() {
                                        for (var t = 0; t < a.length; t++) a[t].style["background-color"] = e[n.loop];
                                        n.loop++,
                                            n.loop = n.loop % e.length
                                    },
                                    t.blinktime * r);
                                n.raise = !1
                            }
                        },
                        t.interval),
                        this.stop = function() {
                            clearInterval(r)
                        }
                }
            },
                Number.prototype.toFixedFit = function(e) {
                    var t = this.toPrecision(e + 1);
                    return t.substr(0, t.indexOf(".") + e)
                },
                String.prototype.cutstr = function(e, t) {
                    return r(this, e, t)
                },
                String.prototype.isPositive = function() {
                    var e = this;
                    if ("string" == typeof e.toLowerCase()) {
                        e = e.replace("%", "");
                        if (new RegExp("^([\\-\\+]?\\d+(\\.\\d+)?)$").test(e)) {
                            return ! new RegExp("^-").test(e)
                        }
                        return Number.NaN
                    }
                },
                String.prototype.numbericFormat = function() {
                    return s(this.toString())
                },
                Date.prototype.pattern = function(e) {
                    return o(this, e)
                },
                Array.prototype.unique = function() {
                    for (var e = [], t = {},
                             n = 0; n < this.length; n++) t[this[n]] || (e.push(this[n]), t[this[n]] = 1);
                    return e
                }
        },
        "./ClientApp/templates/boards.digest.art": function(e, t) {
            e.exports = '<div id="quote_chart_wrapper" class="col_3 head_type fl mr10">\r\n    <div id="quote_chart_title" class="head_div">\r\n        <a href="//quote.eastmoney.com/web/{{Code}}{{MarketType}}.html" target="_blank"><strong>{{Name}}行情</strong></a>\r\n    </div>\r\n    <div class="list_div">\r\n        <a href="//quote.eastmoney.com/web/{{Code}}{{MarketType}}.html" target="_blank">\r\n            <div id="quote_chart"></div>\r\n        </a>\r\n    </div>\r\n</div>\r\n<div id="capitalflow_chart_wrapper" class="col_3 head_type fl mr10">\r\n    <div id="capitalflow_chart_title" class="head_div">\r\n        <a href="//data.eastmoney.com/bkzj/{{SimpleCode}}.html" target="_blank"><strong>{{Name}}资金流向</strong></a>\r\n    </div>\r\n    <div class="list_div">\r\n        <a href="//data.eastmoney.com/bkzj/{{SimpleCode}}.html">\r\n            <div id="capitalflow_chart"></div>\r\n        </a>\r\n    </div>\r\n</div>\r\n<div id="capitalflow_rank_wrapper" class="col_3 head_type fl">\r\n    <div id="capitalflow_rank_title" class="head_div">\r\n        <a href="//data.eastmoney.com/bkzj/{{SimpleCode}}.html"><strong>{{Name}}个股资金流向排行</strong></a>\r\n    </div>\r\n    <table id="capitalflow_rank"></table>\r\n</div>'
        },
        "./ClientApp/templates/boards.hotlinks.art": function(e, t) {
            e.exports = '{{if JYS === \'1\'}}\r\n<a href="//data.eastmoney.com/bkzj/dy.html" target="_blank">地域板块资金流今日排行</a>|\r\n<a href="//data.eastmoney.com/bkzj/dy_5.html" target="_blank">地域板块资金流5日排行</a>|\r\n<a href="//data.eastmoney.com/bkzj/dy_10.html" target="_blank">地域板块资金流10日排行</a>\r\n{{else if JYS === \'2\'}}\r\n<a href="//stock.eastmoney.com/hangye.html" target="_blank">行业频道</a>|\r\n{{if Code}}\r\n<a href="//stock.eastmoney.com/hangye/hy{{Code.replace(\'BK0\',\'\')}}.html" target="_blank">{{Name}}专区</a>|\r\n{{/if}}\r\n<a href="//data.eastmoney.com/bkzj/hy.html" target="_blank">行业板块资金流今日排行</a>|\r\n<a href="//data.eastmoney.com/bkzj/hy_5.html" target="_blank">行业板块资金流5日排行</a>|\r\n<a href="//data.eastmoney.com/bkzj/hy_10.html" target="_blank">行业板块资金流10日排行</a>\r\n{{else if JYS === \'3\'}}\r\n<a href="//data.eastmoney.com/bkzj/gn.html" target="_blank">概念板块资金流今日排行</a>|\r\n<a href="//data.eastmoney.com/bkzj/gn_5.html" target="_blank">概念板块资金流5日排行</a>|\r\n<a href="//data.eastmoney.com/bkzj/gn_10.html" target="_blank">概念板块资金流10日排行</a>\r\n{{/if}}'
        },
        "./node_modules/datatables.net-select/js/dataTables.select.js": function(e, t, n) { !
            function(t) {
                e.exports = function(e, r) {
                    return e || (e = window),
                    r && r.fn.dataTable || (r = n("./node_modules/datatables.net/js/jquery.dataTables.js")(e, r).$),
                        t(r, e, e.document)
                }
            } (function(e, t, n, r) {
                "use strict";
                function a(e, t, n) {
                    var r, a, o, i = function(t, n) {
                            if (t > n) {
                                var r = n;
                                n = t,
                                    t = r
                            }
                            var a = !1;
                            return e.columns(":visible").indexes().filter(function(e) {
                                return e === t && (a = !0),
                                    e === n ? (a = !1, !0) : a
                            })
                        },
                        s = function(t, n) {
                            var r = e.rows({
                                search: "applied"
                            }).indexes();
                            if (r.indexOf(t) > r.indexOf(n)) {
                                var a = n;
                                n = t,
                                    t = a
                            }
                            var o = !1;
                            return r.filter(function(e) {
                                return e === t && (o = !0),
                                    e === n ? (o = !1, !0) : o
                            })
                        };
                    e.cells({
                        selected: !0
                    }).any() || n ? (a = i(n.column, t.column), o = s(n.row, t.row)) : (a = i(0, t.column), o = s(0, t.row)),
                        r = e.cells(o, a).flatten(),
                        e.cells(t, {
                            selected: !0
                        }).any() ? e.cells(r).deselect() : e.cells(r).select()
                }
                function o(t) {
                    var n = t.settings()[0]._select.selector;
                    e(t.table().container()).off("mousedown.dtSelect", n).off("mouseup.dtSelect", n).off("click.dtSelect", n),
                        e("body").off("click.dtSelect" + t.table().node().id)
                }
                function i(n) {
                    var r = e(n.table().container()),
                        a = n.settings()[0],
                        o = a._select.selector;
                    r.on("mousedown.dtSelect", o,
                        function(e) { (e.shiftKey || e.metaKey || e.ctrlKey) && r.css("-moz-user-select", "none").one("selectstart.dtSelect", o,
                            function() {
                                return ! 1
                            })
                        }).on("mouseup.dtSelect", o,
                        function() {
                            r.css("-moz-user-select", "")
                        }).on("click.dtSelect", o,
                        function(r) {
                            var a, o = n.select.items();
                            if (t.getSelection) {
                                var i = t.getSelection();
                                if ((!i.anchorNode || e(i.anchorNode).closest("table")[0] === n.table().node()) && "" !== e.trim(i.toString())) return
                            }
                            var l = n.settings()[0];
                            if (e(r.target).closest("div.dataTables_wrapper")[0] == n.table().container()) {
                                var c = n.cell(e(r.target).closest("td, th"));
                                if (c.any()) {
                                    var u = e.Event("user-select.dt");
                                    if (s(n, u, [o, c, r]), !u.isDefaultPrevented()) {
                                        var h = c.index();
                                        "row" === o ? (a = h.row, d(r, n, l, "row", a)) : "column" === o ? (a = c.index().column, d(r, n, l, "column", a)) : "cell" === o && (a = c.index(), d(r, n, l, "cell", a)),
                                            l._select_lastCell = h
                                    }
                                }
                            }
                        }),
                        e("body").on("click.dtSelect" + n.table().node().id,
                            function(t) {
                                if (a._select.blurable) {
                                    if (e(t.target).parents().filter(n.table().container()).length) return;
                                    if (0 === e(t.target).parents("html").length) return;
                                    if (e(t.target).parents("div.DTE").length) return;
                                    u(a, !0)
                                }
                            })
                }
                function s(t, n, r, a) {
                    a && !t.flatten().length || ("string" == typeof n && (n += ".dt"), r.unshift(t), e(t.table().node()).trigger(n, r))
                }
                function l(t) {
                    var n = t.settings()[0];
                    if (n._select.info && n.aanFeatures.i && "api" !== t.select.style()) {
                        var r = t.rows({
                                selected: !0
                            }).flatten().length,
                            a = t.columns({
                                selected: !0
                            }).flatten().length,
                            o = t.cells({
                                selected: !0
                            }).flatten().length,
                            i = function(n, r, a) {
                                n.append(e('<span class="select-item"/>').append(t.i18n("select." + r + "s", {
                                        _: "%d " + r + "s selected",
                                        0 : "",
                                        1 : "1 " + r + " selected"
                                    },
                                    a)))
                            };
                        e.each(n.aanFeatures.i,
                            function(t, n) {
                                n = e(n);
                                var s = e('<span class="select-info"/>');
                                i(s, "row", r),
                                    i(s, "column", a),
                                    i(s, "cell", o);
                                var l = n.children("span.select-info");
                                l.length && l.remove(),
                                "" !== s.text() && n.append(s)
                            })
                    }
                }
                function c(t, n, r, a) {
                    var o = t[n + "s"]({
                            search: "applied"
                        }).indexes(),
                        i = e.inArray(a, o),
                        s = e.inArray(r, o);
                    if (t[n + "s"]({
                        selected: !0
                    }).any() || -1 !== i) {
                        if (i > s) {
                            var l = s;
                            s = i,
                                i = l
                        }
                        o.splice(s + 1, o.length),
                            o.splice(0, i)
                    } else o.splice(e.inArray(r, o) + 1, o.length);
                    t[n](r, {
                        selected: !0
                    }).any() ? (o.splice(e.inArray(r, o), 1), t[n + "s"](o).deselect()) : t[n + "s"](o).select()
                }
                function u(e, t) {
                    if (t || "single" === e._select.style) {
                        var n = new p.Api(e);
                        n.rows({
                            selected: !0
                        }).deselect(),
                            n.columns({
                                selected: !0
                            }).deselect(),
                            n.cells({
                                selected: !0
                            }).deselect()
                    }
                }
                function d(e, t, n, r, o) {
                    var i = t.select.style(),
                        s = t[r](o, {
                            selected: !0
                        }).any();
                    if ("os" === i) if (e.ctrlKey || e.metaKey) t[r](o).select(!s);
                    else if (e.shiftKey)"cell" === r ? a(t, o, n._select_lastCell || null) : c(t, r, o, n._select_lastCell ? n._select_lastCell[r] : null);
                    else {
                        var l = t[r + "s"]({
                            selected: !0
                        });
                        s && 1 === l.flatten().length ? t[r](o).deselect() : (l.deselect(), t[r](o).select())
                    } else "multi+shift" == i && e.shiftKey ? "cell" === r ? a(t, o, n._select_lastCell || null) : c(t, r, o, n._select_lastCell ? n._select_lastCell[r] : null) : t[r](o).select(!s)
                }
                function h(e, t) {
                    return function(n) {
                        return n.i18n("buttons." + e, t)
                    }
                }
                function f(e) {
                    var t = e._eventNamespace;
                    return "draw.dt.DT" + t + " select.dt.DT" + t + " deselect.dt.DT" + t
                }
                var p = e.fn.dataTable;
                p.select = {},
                    p.select.version = "1.2.5-dev",
                    p.select.init = function(t) {
                        var n = t.settings()[0],
                            a = n.oInit.select,
                            o = p.defaults.select,
                            i = a === r ? o: a,
                            s = "row",
                            l = "api",
                            c = !1,
                            u = !0,
                            d = "td, th",
                            h = "selected",
                            f = !1;
                        n._select = {},
                            !0 === i ? (l = "os", f = !0) : "string" == typeof i ? (l = i, f = !0) : e.isPlainObject(i) && (i.blurable !== r && (c = i.blurable), i.info !== r && (u = i.info), i.items !== r && (s = i.items), i.style !== r && (l = i.style, f = !0), i.selector !== r && (d = i.selector), i.className !== r && (h = i.className)),
                            t.select.selector(d),
                            t.select.items(s),
                            t.select.style(l),
                            t.select.blurable(c),
                            t.select.info(u),
                            n._select.className = h,
                            e.fn.dataTable.ext.order["select-checkbox"] = function(t, n) {
                                return this.api().column(n, {
                                    order: "index"
                                }).nodes().map(function(n) {
                                    return "row" === t._select.items ? e(n).parent().hasClass(t._select.className) : "cell" === t._select.items && e(n).hasClass(t._select.className)
                                })
                            },
                        !f && e(t.table().node()).hasClass("selectable") && t.select.style("os")
                    },
                    e.each([{
                            type: "row",
                            prop: "aoData"
                        },
                            {
                                type: "column",
                                prop: "aoColumns"
                            }],
                        function(e, t) {
                            p.ext.selector[t.type].push(function(e, n, r) {
                                var a, o = n.selected,
                                    i = [];
                                if (!0 !== o && !1 !== o) return r;
                                for (var s = 0,
                                         l = r.length; s < l; s++) a = e[t.prop][r[s]],
                                (!0 === o && !0 === a._select_selected || !1 === o && !a._select_selected) && i.push(r[s]);
                                return i
                            })
                        }),
                    p.ext.selector.cell.push(function(e, t, n) {
                        var a, o = t.selected,
                            i = [];
                        if (o === r) return n;
                        for (var s = 0,
                                 l = n.length; s < l; s++) a = e.aoData[n[s].row],
                        (!0 === o && a._selected_cells && !0 === a._selected_cells[n[s].column] || !1 === o && (!a._selected_cells || !a._selected_cells[n[s].column])) && i.push(n[s]);
                        return i
                    });
                var m = p.Api.register,
                    g = p.Api.registerPlural;
                m("select()",
                    function() {
                        return this.iterator("table",
                            function(e) {
                                p.select.init(new p.Api(e))
                            })
                    }),
                    m("select.blurable()",
                        function(e) {
                            return e === r ? this.context[0]._select.blurable: this.iterator("table",
                                function(t) {
                                    t._select.blurable = e
                                })
                        }),
                    m("select.info()",
                        function(e) {
                            return l === r ? this.context[0]._select.info: this.iterator("table",
                                function(t) {
                                    t._select.info = e
                                })
                        }),
                    m("select.items()",
                        function(e) {
                            return e === r ? this.context[0]._select.items: this.iterator("table",
                                function(t) {
                                    t._select.items = e,
                                        s(new p.Api(t), "selectItems", [e])
                                })
                        }),
                    m("select.style()",
                        function(t) {
                            return t === r ? this.context[0]._select.style: this.iterator("table",
                                function(n) {
                                    n._select.style = t,
                                    n._select_init ||
                                    function(t) {
                                        var n = new p.Api(t);
                                        t.aoRowCreatedCallback.push({
                                            fn: function(n, r, a) {
                                                var o, i, s = t.aoData[a];
                                                for (s._select_selected && e(n).addClass(t._select.className), o = 0, i = t.aoColumns.length; o < i; o++)(t.aoColumns[o]._select_selected || s._selected_cells && s._selected_cells[o]) && e(s.anCells[o]).addClass(t._select.className)
                                            },
                                            sName: "select-deferRender"
                                        }),
                                            n.on("preXhr.dt.dtSelect",
                                                function() {
                                                    var e = n.rows({
                                                            selected: !0
                                                        }).ids(!0).filter(function(e) {
                                                            return e !== r
                                                        }),
                                                        t = n.cells({
                                                            selected: !0
                                                        }).eq(0).map(function(e) {
                                                            var t = n.row(e.row).id(!0);
                                                            return t ? {
                                                                row: t,
                                                                column: e.column
                                                            }: r
                                                        }).filter(function(e) {
                                                            return e !== r
                                                        });
                                                    n.one("draw.dt.dtSelect",
                                                        function() {
                                                            n.rows(e).select(),
                                                            t.any() && t.each(function(e) {
                                                                n.cells(e.row, e.column).select()
                                                            })
                                                        })
                                                }),
                                            n.on("draw.dtSelect.dt select.dtSelect.dt deselect.dtSelect.dt info.dt",
                                                function() {
                                                    l(n)
                                                }),
                                            n.on("destroy.dtSelect",
                                                function() {
                                                    o(n),
                                                        n.off(".dtSelect")
                                                })
                                    } (n);
                                    var a = new p.Api(n);
                                    o(a),
                                    "api" !== t && i(a),
                                        s(new p.Api(n), "selectStyle", [t])
                                })
                        }),
                    m("select.selector()",
                        function(e) {
                            return e === r ? this.context[0]._select.selector: this.iterator("table",
                                function(t) {
                                    o(new p.Api(t)),
                                        t._select.selector = e,
                                    "api" !== t._select.style && i(new p.Api(t))
                                })
                        }),
                    g("rows().select()", "row().select()",
                        function(t) {
                            var n = this;
                            return ! 1 === t ? this.deselect() : (this.iterator("row",
                                function(t, n) {
                                    u(t),
                                        t.aoData[n]._select_selected = !0,
                                        e(t.aoData[n].nTr).addClass(t._select.className)
                                }), this.iterator("table",
                                function(e, t) {
                                    s(n, "select", ["row", n[t]], !0)
                                }), this)
                        }),
                    g("columns().select()", "column().select()",
                        function(t) {
                            var n = this;
                            return ! 1 === t ? this.deselect() : (this.iterator("column",
                                function(t, n) {
                                    u(t),
                                        t.aoColumns[n]._select_selected = !0;
                                    var r = new p.Api(t).column(n);
                                    e(r.header()).addClass(t._select.className),
                                        e(r.footer()).addClass(t._select.className),
                                        r.nodes().to$().addClass(t._select.className)
                                }), this.iterator("table",
                                function(e, t) {
                                    s(n, "select", ["column", n[t]], !0)
                                }), this)
                        }),
                    g("cells().select()", "cell().select()",
                        function(t) {
                            var n = this;
                            return ! 1 === t ? this.deselect() : (this.iterator("cell",
                                function(t, n, a) {
                                    u(t);
                                    var o = t.aoData[n];
                                    o._selected_cells === r && (o._selected_cells = []),
                                        o._selected_cells[a] = !0,
                                    o.anCells && e(o.anCells[a]).addClass(t._select.className)
                                }), this.iterator("table",
                                function(e, t) {
                                    s(n, "select", ["cell", n[t]], !0)
                                }), this)
                        }),
                    g("rows().deselect()", "row().deselect()",
                        function() {
                            var t = this;
                            return this.iterator("row",
                                function(t, n) {
                                    t.aoData[n]._select_selected = !1,
                                        e(t.aoData[n].nTr).removeClass(t._select.className)
                                }),
                                this.iterator("table",
                                    function(e, n) {
                                        s(t, "deselect", ["row", t[n]], !0)
                                    }),
                                this
                        }),
                    g("columns().deselect()", "column().deselect()",
                        function() {
                            var t = this;
                            return this.iterator("column",
                                function(t, n) {
                                    t.aoColumns[n]._select_selected = !1;
                                    var r = new p.Api(t),
                                        a = r.column(n);
                                    e(a.header()).removeClass(t._select.className),
                                        e(a.footer()).removeClass(t._select.className),
                                        r.cells(null, n).indexes().each(function(n) {
                                            var r = t.aoData[n.row],
                                                a = r._selected_cells; ! r.anCells || a && a[n.column] || e(r.anCells[n.column]).removeClass(t._select.className)
                                        })
                                }),
                                this.iterator("table",
                                    function(e, n) {
                                        s(t, "deselect", ["column", t[n]], !0)
                                    }),
                                this
                        }),
                    g("cells().deselect()", "cell().deselect()",
                        function() {
                            var t = this;
                            return this.iterator("cell",
                                function(t, n, r) {
                                    var a = t.aoData[n];
                                    a._selected_cells[r] = !1,
                                    a.anCells && !t.aoColumns[r]._select_selected && e(a.anCells[r]).removeClass(t._select.className)
                                }),
                                this.iterator("table",
                                    function(e, n) {
                                        s(t, "deselect", ["cell", t[n]], !0)
                                    }),
                                this
                        });
                var v = 0;
                return e.extend(p.ext.buttons, {
                    selected: {
                        text: h("selected", "Selected"),
                        className: "buttons-selected",
                        limitTo: ["rows", "columns", "cells"],
                        init: function(t, n, r) {
                            var a = this;
                            r._eventNamespace = ".select" + v++,
                                t.on(f(r),
                                    function() {
                                        a.enable(function(t, n) {
                                            return ! ( - 1 === e.inArray("rows", n.limitTo) || !t.rows({
                                                selected: !0
                                            }).any()) || !( - 1 === e.inArray("columns", n.limitTo) || !t.columns({
                                                selected: !0
                                            }).any()) || !( - 1 === e.inArray("cells", n.limitTo) || !t.cells({
                                                selected: !0
                                            }).any())
                                        } (t, r))
                                    }),
                                this.disable()
                        },
                        destroy: function(e, t, n) {
                            e.off(n._eventNamespace)
                        }
                    },
                    selectedSingle: {
                        text: h("selectedSingle", "Selected single"),
                        className: "buttons-selected-single",
                        init: function(e, t, n) {
                            var r = this;
                            n._eventNamespace = ".select" + v++,
                                e.on(f(n),
                                    function() {
                                        var t = e.rows({
                                            selected: !0
                                        }).flatten().length + e.columns({
                                            selected: !0
                                        }).flatten().length + e.cells({
                                            selected: !0
                                        }).flatten().length;
                                        r.enable(1 === t)
                                    }),
                                this.disable()
                        },
                        destroy: function(e, t, n) {
                            e.off(n._eventNamespace)
                        }
                    },
                    selectAll: {
                        text: h("selectAll", "Select all"),
                        className: "buttons-select-all",
                        action: function() {
                            this[this.select.items() + "s"]().select()
                        }
                    },
                    selectNone: {
                        text: h("selectNone", "Deselect all"),
                        className: "buttons-select-none",
                        action: function() {
                            u(this.settings()[0], !0)
                        },
                        init: function(e, t, n) {
                            var r = this;
                            n._eventNamespace = ".select" + v++,
                                e.on(f(n),
                                    function() {
                                        var t = e.rows({
                                            selected: !0
                                        }).flatten().length + e.columns({
                                            selected: !0
                                        }).flatten().length + e.cells({
                                            selected: !0
                                        }).flatten().length;
                                        r.enable(t > 0)
                                    }),
                                this.disable()
                        },
                        destroy: function(e, t, n) {
                            e.off(n._eventNamespace)
                        }
                    }
                }),
                    e.each(["Row", "Column", "Cell"],
                        function(e, t) {
                            var n = t.toLowerCase();
                            p.ext.buttons["select" + t + "s"] = {
                                text: h("select" + t + "s", "Select " + n + "s"),
                                className: "buttons-select-" + n + "s",
                                action: function() {
                                    this.select.items(n)
                                },
                                init: function(e) {
                                    var t = this;
                                    e.on("selectItems.dt.DT",
                                        function(e, r, a) {
                                            t.active(a === n)
                                        })
                                }
                            }
                        }),
                    e(n).on("preInit.dt.dtSelect",
                        function(e, t) {
                            "dt" === e.namespace && p.select.init(new p.Api(t))
                        }),
                    p.select
            })
        },
        "./node_modules/datatables.net/js/jquery.dataTables.js": function(e, t, n) { !
            function(t) {
                "use strict";
                e.exports = function(e, r) {
                    return e || (e = window),
                    r || (r = "undefined" != typeof window ? n("jquery") : n("jquery")(e)),
                        t(r, e, e.document)
                }
            } (function(e, t, n, r) {
                "use strict";
                function a(t) {
                    var n, r, o = {};
                    e.each(t,
                        function(e, i) { (n = e.match(/^([^A-Z]+?)([A-Z])/)) && -1 !== "a aa ai ao as b fn i m o s ".indexOf(n[1] + " ") && (r = e.replace(n[0], n[2].toLowerCase()), o[r] = e, "o" === n[1] && a(t[e]))
                        }),
                        t._hungarianMap = o
                }
                function o(t, n, i) {
                    t._hungarianMap || a(t);
                    var s;
                    e.each(n,
                        function(a, l) { (s = t._hungarianMap[a]) === r || !i && n[s] !== r || ("o" === s.charAt(0) ? (n[s] || (n[s] = {}), e.extend(!0, n[s], n[a]), o(t[s], n[s], i)) : n[s] = n[a])
                        })
                }
                function i(e) {
                    var t = Qe.defaults.oLanguage,
                        n = e.sZeroRecords; ! e.sEmptyTable && n && "No data available in table" === t.sEmptyTable && Fe(e, e, "sZeroRecords", "sEmptyTable"),
                    !e.sLoadingRecords && n && "Loading..." === t.sLoadingRecords && Fe(e, e, "sZeroRecords", "sLoadingRecords"),
                    e.sInfoThousands && (e.sThousands = e.sInfoThousands);
                    var r = e.sDecimal;
                    r && Be(r)
                }
                function s(e) {
                    ut(e, "ordering", "bSort"),
                        ut(e, "orderMulti", "bSortMulti"),
                        ut(e, "orderClasses", "bSortClasses"),
                        ut(e, "orderCellsTop", "bSortCellsTop"),
                        ut(e, "order", "aaSorting"),
                        ut(e, "orderFixed", "aaSortingFixed"),
                        ut(e, "paging", "bPaginate"),
                        ut(e, "pagingType", "sPaginationType"),
                        ut(e, "pageLength", "iDisplayLength"),
                        ut(e, "searching", "bFilter"),
                    "boolean" == typeof e.sScrollX && (e.sScrollX = e.sScrollX ? "100%": ""),
                    "boolean" == typeof e.scrollX && (e.scrollX = e.scrollX ? "100%": "");
                    var t = e.aoSearchCols;
                    if (t) for (var n = 0,
                                    r = t.length; n < r; n++) t[n] && o(Qe.models.oSearch, t[n])
                }
                function l(t) {
                    ut(t, "orderable", "bSortable"),
                        ut(t, "orderData", "aDataSort"),
                        ut(t, "orderSequence", "asSorting"),
                        ut(t, "orderDataType", "sortDataType");
                    var n = t.aDataSort;
                    "number" != typeof n || e.isArray(n) || (t.aDataSort = [n])
                }
                function c(n) {
                    if (!Qe.__browser) {
                        var r = {};
                        Qe.__browser = r;
                        var a = e("<div/>").css({
                                position: "fixed",
                                top: 0,
                                left: -1 * e(t).scrollLeft(),
                                height: 1,
                                width: 1,
                                overflow: "hidden"
                            }).append(e("<div/>").css({
                                position: "absolute",
                                top: 1,
                                left: 1,
                                width: 100,
                                overflow: "scroll"
                            }).append(e("<div/>").css({
                                width: "100%",
                                height: 10
                            }))).appendTo("body"),
                            o = a.children(),
                            i = o.children();
                        r.barWidth = o[0].offsetWidth - o[0].clientWidth,
                            r.bScrollOversize = 100 === i[0].offsetWidth && 100 !== o[0].clientWidth,
                            r.bScrollbarLeft = 1 !== Math.round(i.offset().left),
                            r.bBounding = !!a[0].getBoundingClientRect().width,
                            a.remove()
                    }
                    e.extend(n.oBrowser, Qe.__browser),
                        n.oScroll.iBarWidth = Qe.__browser.barWidth
                }
                function u(e, t, n, a, o, i) {
                    var s, l = a,
                        c = !1;
                    for (n !== r && (s = n, c = !0); l !== o;) e.hasOwnProperty(l) && (s = c ? t(s, e[l], l, e) : e[l], c = !0, l += i);
                    return s
                }
                function d(t, r) {
                    var a = Qe.defaults.column,
                        o = t.aoColumns.length,
                        i = e.extend({},
                            Qe.models.oColumn, a, {
                                nTh: r || n.createElement("th"),
                                sTitle: a.sTitle ? a.sTitle: r ? r.innerHTML: "",
                                aDataSort: a.aDataSort ? a.aDataSort: [o],
                                mData: a.mData ? a.mData: o,
                                idx: o
                            });
                    t.aoColumns.push(i);
                    var s = t.aoPreSearchCols;
                    s[o] = e.extend({},
                        Qe.models.oSearch, s[o]),
                        h(t, o, e(r).data())
                }
                function h(t, n, a) {
                    var i = t.aoColumns[n],
                        s = t.oClasses,
                        c = e(i.nTh);
                    if (!i.sWidthOrig) {
                        i.sWidthOrig = c.attr("width") || null;
                        var u = (c.attr("style") || "").match(/width:\s*(\d+[pxem%]+)/);
                        u && (i.sWidthOrig = u[1])
                    }
                    a !== r && null !== a && (l(a), o(Qe.defaults.column, a), a.mDataProp === r || a.mData || (a.mData = a.mDataProp), a.sType && (i._sManualType = a.sType), a.className && !a.sClass && (a.sClass = a.className), a.sClass && c.addClass(a.sClass), e.extend(i, a), Fe(i, a, "sWidth", "sWidthOrig"), a.iDataSort !== r && (i.aDataSort = [a.iDataSort]), Fe(i, a, "aDataSort"));
                    var d = i.mData,
                        h = T(d),
                        f = i.mRender ? T(i.mRender) : null,
                        p = function(e) {
                            return "string" == typeof e && -1 !== e.indexOf("@")
                        };
                    i._bAttrSrc = e.isPlainObject(d) && (p(d.sort) || p(d.type) || p(d.filter)),
                        i._setter = null,
                        i.fnGetData = function(e, t, n) {
                            var a = h(e, t, r, n);
                            return f && t ? f(a, t, e, n) : a
                        },
                        i.fnSetData = function(e, t, n) {
                            return A(d)(e, t, n)
                        },
                    "number" != typeof d && (t._rowReadObject = !0),
                    t.oFeatures.bSort || (i.bSortable = !1, c.addClass(s.sSortableNone));
                    var m = -1 !== e.inArray("asc", i.asSorting),
                        g = -1 !== e.inArray("desc", i.asSorting);
                    i.bSortable && (m || g) ? m && !g ? (i.sSortingClass = s.sSortableAsc, i.sSortingClassJUI = s.sSortJUIAscAllowed) : !m && g ? (i.sSortingClass = s.sSortableDesc, i.sSortingClassJUI = s.sSortJUIDescAllowed) : (i.sSortingClass = s.sSortable, i.sSortingClassJUI = s.sSortJUI) : (i.sSortingClass = s.sSortableNone, i.sSortingClassJUI = "")
                }
                function f(e) {
                    if (!1 !== e.oFeatures.bAutoWidth) {
                        var t = e.aoColumns;
                        me(e);
                        for (var n = 0,
                                 r = t.length; n < r; n++) t[n].nTh.style.width = t[n].sWidth
                    }
                    var a = e.oScroll;
                    "" === a.sY && "" === a.sX || fe(e),
                        Me(e, null, "column-sizing", [e])
                }
                function p(e, t) {
                    var n = v(e, "bVisible");
                    return "number" == typeof n[t] ? n[t] : null
                }
                function m(t, n) {
                    var r = v(t, "bVisible"),
                        a = e.inArray(n, r);
                    return - 1 !== a ? a: null
                }
                function g(t) {
                    var n = 0;
                    return e.each(t.aoColumns,
                        function(t, r) {
                            r.bVisible && "none" !== e(r.nTh).css("display") && n++
                        }),
                        n
                }
                function v(t, n) {
                    var r = [];
                    return e.map(t.aoColumns,
                        function(e, t) {
                            e[n] && r.push(t)
                        }),
                        r
                }
                function b(e) {
                    var t, n, a, o, i, s, l, c, u, d = e.aoColumns,
                        h = e.aoData,
                        f = Qe.ext.type.detect;
                    for (t = 0, n = d.length; t < n; t++) if (l = d[t], u = [], !l.sType && l._sManualType) l.sType = l._sManualType;
                    else if (!l.sType) {
                        for (a = 0, o = f.length; a < o; a++) {
                            for (i = 0, s = h.length; i < s && (u[i] === r && (u[i] = w(e, i, t, "type")), (c = f[a](u[i], e)) || a === f.length - 1) && "html" !== c; i++);
                            if (c) {
                                l.sType = c;
                                break
                            }
                        }
                        l.sType || (l.sType = "string")
                    }
                }
                function y(t, n, a, o) {
                    var i, s, l, c, u, h, f, p = t.aoColumns;
                    if (n) for (i = n.length - 1; i >= 0; i--) {
                        var m = (f = n[i]).targets !== r ? f.targets: f.aTargets;
                        for (e.isArray(m) || (m = [m]), l = 0, c = m.length; l < c; l++) if ("number" == typeof m[l] && m[l] >= 0) {
                            for (; p.length <= m[l];) d(t);
                            o(m[l], f)
                        } else if ("number" == typeof m[l] && m[l] < 0) o(p.length + m[l], f);
                        else if ("string" == typeof m[l]) for (u = 0, h = p.length; u < h; u++)("_all" == m[l] || e(p[u].nTh).hasClass(m[l])) && o(u, f)
                    }
                    if (a) for (i = 0, s = a.length; i < s; i++) o(i, a[i])
                }
                function _(t, n, a, o) {
                    var i = t.aoData.length,
                        s = e.extend(!0, {},
                            Qe.models.oRow, {
                                src: a ? "dom": "data",
                                idx: i
                            });
                    s._aData = n,
                        t.aoData.push(s);
                    for (var l = t.aoColumns,
                             c = 0,
                             u = l.length; c < u; c++) l[c].sType = null;
                    t.aiDisplayMaster.push(i);
                    var d = t.rowIdFn(n);
                    return d !== r && (t.aIds[d] = s),
                    !a && t.oFeatures.bDeferRender || I(t, i, a, o),
                        i
                }
                function C(t, n) {
                    var r;
                    return n instanceof e || (n = e(n)),
                        n.map(function(e, n) {
                            return r = F(t, n),
                                _(t, r.data, n, r.cells)
                        })
                }
                function w(e, t, n, a) {
                    var o = e.iDraw,
                        i = e.aoColumns[n],
                        s = e.aoData[t]._aData,
                        l = i.sDefaultContent,
                        c = i.fnGetData(s, a, {
                            settings: e,
                            row: t,
                            col: n
                        });
                    if (c === r) return e.iDrawError != o && null === l && (je(e, 0, "Requested unknown parameter " + ("function" == typeof i.mData ? "{function}": "'" + i.mData + "'") + " for row " + t + ", column " + n, 4), e.iDrawError = o),
                        l;
                    if (c !== s && null !== c || null === l || a === r) {
                        if ("function" == typeof c) return c.call(s)
                    } else c = l;
                    return null === c && "display" == a ? "": c
                }
                function x(e, t, n, r) {
                    var a = e.aoColumns[n],
                        o = e.aoData[t]._aData;
                    a.fnSetData(o, r, {
                        settings: e,
                        row: t,
                        col: n
                    })
                }
                function S(t) {
                    return e.map(t.match(/(\\.|[^\.])+/g) || [""],
                        function(e) {
                            return e.replace(/\\\./g, ".")
                        })
                }
                function T(t) {
                    if (e.isPlainObject(t)) {
                        var n = {};
                        return e.each(t,
                            function(e, t) {
                                t && (n[e] = T(t))
                            }),
                            function(e, t, a, o) {
                                var i = n[t] || n._;
                                return i !== r ? i(e, t, a, o) : e
                            }
                    }
                    if (null === t) return function(e) {
                        return e
                    };
                    if ("function" == typeof t) return function(e, n, r, a) {
                        return t(e, n, r, a)
                    };
                    if ("string" != typeof t || -1 === t.indexOf(".") && -1 === t.indexOf("[") && -1 === t.indexOf("(")) return function(e, n) {
                        return e[t]
                    };
                    var a = function(t, n, o) {
                        var i, s, l, c;
                        if ("" !== o) for (var u = S(o), d = 0, h = u.length; d < h; d++) {
                            if (i = u[d].match(dt), s = u[d].match(ht), i) {
                                if (u[d] = u[d].replace(dt, ""), "" !== u[d] && (t = t[u[d]]), l = [], u.splice(0, d + 1), c = u.join("."), e.isArray(t)) for (var f = 0,
                                                                                                                                                                   p = t.length; f < p; f++) l.push(a(t[f], n, c));
                                var m = i[0].substring(1, i[0].length - 1);
                                t = "" === m ? l: l.join(m);
                                break
                            }
                            if (s) u[d] = u[d].replace(ht, ""),
                                t = t[u[d]]();
                            else {
                                if (null === t || t[u[d]] === r) return r;
                                t = t[u[d]]
                            }
                        }
                        return t
                    };
                    return function(e, n) {
                        return a(e, n, t)
                    }
                }
                function A(t) {
                    if (e.isPlainObject(t)) return A(t._);
                    if (null === t) return function() {};
                    if ("function" == typeof t) return function(e, n, r) {
                        t(e, "set", n, r)
                    };
                    if ("string" != typeof t || -1 === t.indexOf(".") && -1 === t.indexOf("[") && -1 === t.indexOf("(")) return function(e, n) {
                        e[t] = n
                    };
                    var n = function(t, a, o) {
                        for (var i, s, l, c, u, d = S(o), h = d[d.length - 1], f = 0, p = d.length - 1; f < p; f++) {
                            if (s = d[f].match(dt), l = d[f].match(ht), s) {
                                if (d[f] = d[f].replace(dt, ""), t[d[f]] = [], (i = d.slice()).splice(0, f + 1), u = i.join("."), e.isArray(a)) for (var m = 0,
                                                                                                                                                         g = a.length; m < g; m++) n(c = {},
                                    a[m], u),
                                    t[d[f]].push(c);
                                else t[d[f]] = a;
                                return
                            }
                            l && (d[f] = d[f].replace(ht, ""), t = t[d[f]](a)),
                            null !== t[d[f]] && t[d[f]] !== r || (t[d[f]] = {}),
                                t = t[d[f]]
                        }
                        h.match(ht) ? t = t[h.replace(ht, "")](a) : t[h.replace(dt, "")] = a
                    };
                    return function(e, r) {
                        return n(e, r, t)
                    }
                }
                function k(e) {
                    return at(e.aoData, "_aData")
                }
                function D(e) {
                    e.aoData.length = 0,
                        e.aiDisplayMaster.length = 0,
                        e.aiDisplay.length = 0,
                        e.aIds = {}
                }
                function P(e, t, n) {
                    for (var a = -1,
                             o = 0,
                             i = e.length; o < i; o++) e[o] == t ? a = o: e[o] > t && e[o]--; - 1 != a && n === r && e.splice(a, 1)
                }
                function j(e, t, n, a) {
                    var o, i, s = e.aoData[t],
                        l = function(n, r) {
                            for (; n.childNodes.length;) n.removeChild(n.firstChild);
                            n.innerHTML = w(e, t, r, "display")
                        };
                    if ("dom" !== n && (n && "auto" !== n || "dom" !== s.src)) {
                        var c = s.anCells;
                        if (c) if (a !== r) l(c[a], a);
                        else for (o = 0, i = c.length; o < i; o++) l(c[o], o)
                    } else s._aData = F(e, s, a, a === r ? r: s._aData).data;
                    s._aSortData = null,
                        s._aFilterData = null;
                    var u = e.aoColumns;
                    if (a !== r) u[a].sType = null;
                    else {
                        for (o = 0, i = u.length; o < i; o++) u[o].sType = null;
                        R(e, s)
                    }
                }
                function F(t, n, a, o) {
                    var i, s, l, c = [],
                        u = n.firstChild,
                        d = 0,
                        h = t.aoColumns,
                        f = t._rowReadObject;
                    o = o !== r ? o: f ? {}: [];
                    var p = function(e, t) {
                            if ("string" == typeof e) {
                                var n = e.indexOf("@");
                                if ( - 1 !== n) {
                                    var r = e.substring(n + 1);
                                    A(e)(o, t.getAttribute(r))
                                }
                            }
                        },
                        m = function(t) {
                            if (a === r || a === d) if (s = h[d], l = e.trim(t.innerHTML), s && s._bAttrSrc) {
                                A(s.mData._)(o, l),
                                    p(s.mData.sort, t),
                                    p(s.mData.type, t),
                                    p(s.mData.filter, t)
                            } else f ? (s._setter || (s._setter = A(s.mData)), s._setter(o, l)) : o[d] = l;
                            d++
                        };
                    if (u) for (; u;)"TD" != (i = u.nodeName.toUpperCase()) && "TH" != i || (m(u), c.push(u)),
                        u = u.nextSibling;
                    else for (var g = 0,
                                  v = (c = n.anCells).length; g < v; g++) m(c[g]);
                    var b = n.firstChild ? n: n.nTr;
                    if (b) {
                        var y = b.getAttribute("id");
                        y && A(t.rowId)(o, y)
                    }
                    return {
                        data: o,
                        cells: c
                    }
                }
                function I(t, r, a, o) {
                    var i, s, l, c, u, d = t.aoData[r],
                        h = d._aData,
                        f = [];
                    if (null === d.nTr) {
                        for (i = a || n.createElement("tr"), d.nTr = i, d.anCells = f, i._DT_RowIndex = r, R(t, d), c = 0, u = t.aoColumns.length; c < u; c++) l = t.aoColumns[c],
                            (s = a ? o[c] : n.createElement(l.sCellType))._DT_CellIndex = {
                                row: r,
                                column: c
                            },
                            f.push(s),
                        a && !l.mRender && l.mData === c || e.isPlainObject(l.mData) && l.mData._ === c + ".display" || (s.innerHTML = w(t, r, c, "display")),
                        l.sClass && (s.className += " " + l.sClass),
                            l.bVisible && !a ? i.appendChild(s) : !l.bVisible && a && s.parentNode.removeChild(s),
                        l.fnCreatedCell && l.fnCreatedCell.call(t.oInstance, s, w(t, r, c), h, r, c);
                        Me(t, "aoRowCreatedCallback", null, [i, h, r])
                    }
                    d.nTr.setAttribute("role", "row")
                }
                function R(t, n) {
                    var r = n.nTr,
                        a = n._aData;
                    if (r) {
                        var o = t.rowIdFn(a);
                        if (o && (r.id = o), a.DT_RowClass) {
                            var i = a.DT_RowClass.split(" ");
                            n.__rowc = n.__rowc ? ct(n.__rowc.concat(i)) : i,
                                e(r).removeClass(n.__rowc.join(" ")).addClass(a.DT_RowClass)
                        }
                        a.DT_RowAttr && e(r).attr(a.DT_RowAttr),
                        a.DT_RowData && e(r).data(a.DT_RowData)
                    }
                }
                function E(t) {
                    var n, r, a, o, i, s = t.nTHead,
                        l = t.nTFoot,
                        c = 0 === e("th, td", s).length,
                        u = t.oClasses,
                        d = t.aoColumns;
                    for (c && (o = e("<tr/>").appendTo(s)), n = 0, r = d.length; n < r; n++) i = d[n],
                        a = e(i.nTh).addClass(i.sClass),
                    c && a.appendTo(o),
                    t.oFeatures.bSort && (a.addClass(i.sSortingClass), !1 !== i.bSortable && (a.attr("tabindex", t.iTabIndex).attr("aria-controls", t.sTableId), Se(t, i.nTh, n))),
                    i.sTitle != a[0].innerHTML && a.html(i.sTitle),
                        Le(t, "header")(t, a, i, u);
                    if (c && N(t.aoHeader, s), e(s).find(">tr").attr("role", "row"), e(s).find(">tr>th, >tr>td").addClass(u.sHeaderTH), e(l).find(">tr>th, >tr>td").addClass(u.sFooterTH), null !== l) {
                        var h = t.aoFooter[0];
                        for (n = 0, r = h.length; n < r; n++)(i = d[n]).nTf = h[n].cell,
                        i.sClass && e(i.nTf).addClass(i.sClass)
                    }
                }
                function M(t, n, a) {
                    var o, i, s, l, c, u, d, h, f, p = [],
                        m = [],
                        g = t.aoColumns.length;
                    if (n) {
                        for (a === r && (a = !1), o = 0, i = n.length; o < i; o++) {
                            for (p[o] = n[o].slice(), p[o].nTr = n[o].nTr, s = g - 1; s >= 0; s--) t.aoColumns[s].bVisible || a || p[o].splice(s, 1);
                            m.push([])
                        }
                        for (o = 0, i = p.length; o < i; o++) {
                            if (d = p[o].nTr) for (; u = d.firstChild;) d.removeChild(u);
                            for (s = 0, l = p[o].length; s < l; s++) if (h = 1, f = 1, m[o][s] === r) {
                                for (d.appendChild(p[o][s].cell), m[o][s] = 1; p[o + h] !== r && p[o][s].cell == p[o + h][s].cell;) m[o + h][s] = 1,
                                    h++;
                                for (; p[o][s + f] !== r && p[o][s].cell == p[o][s + f].cell;) {
                                    for (c = 0; c < h; c++) m[o + c][s + f] = 1;
                                    f++
                                }
                                e(p[o][s].cell).attr("rowspan", h).attr("colspan", f)
                            }
                        }
                    }
                }
                function H(t) {
                    var n = Me(t, "aoPreDrawCallback", "preDraw", [t]);
                    if ( - 1 === e.inArray(!1, n)) {
                        var a = [],
                            o = 0,
                            i = t.asStripeClasses,
                            s = i.length,
                            l = (t.aoOpenRows.length, t.oLanguage),
                            c = t.iInitDisplayStart,
                            u = "ssp" == Oe(t),
                            d = t.aiDisplay;
                        t.bDrawing = !0,
                        c !== r && -1 !== c && (t._iDisplayStart = u ? c: c >= t.fnRecordsDisplay() ? 0 : c, t.iInitDisplayStart = -1);
                        var h = t._iDisplayStart,
                            f = t.fnDisplayEnd();
                        if (t.bDeferLoading) t.bDeferLoading = !1,
                            t.iDraw++,
                            de(t, !1);
                        else if (u) {
                            if (!t.bDestroying && !z(t)) return
                        } else t.iDraw++;
                        if (0 !== d.length) for (var p = u ? 0 : h, m = u ? t.aoData.length: f, v = p; v < m; v++) {
                            var b = d[v],
                                y = t.aoData[b];
                            null === y.nTr && I(t, b);
                            var _ = y.nTr;
                            if (0 !== s) {
                                var C = i[o % s];
                                y._sRowStripe != C && (e(_).removeClass(y._sRowStripe).addClass(C), y._sRowStripe = C)
                            }
                            Me(t, "aoRowCallback", null, [_, y._aData, o, v]),
                                a.push(_),
                                o++
                        } else {
                            var w = l.sZeroRecords;
                            1 == t.iDraw && "ajax" == Oe(t) ? w = l.sLoadingRecords: l.sEmptyTable && 0 === t.fnRecordsTotal() && (w = l.sEmptyTable),
                                a[0] = e("<tr/>", {
                                    "class": s ? i[0] : ""
                                }).append(e("<td />", {
                                    valign: "top",
                                    colSpan: g(t),
                                    "class": t.oClasses.sRowEmpty
                                }).html(w))[0]
                        }
                        Me(t, "aoHeaderCallback", "header", [e(t.nTHead).children("tr")[0], k(t), h, f, d]),
                            Me(t, "aoFooterCallback", "footer", [e(t.nTFoot).children("tr")[0], k(t), h, f, d]);
                        var x = e(t.nTBody);
                        x.children().detach(),
                            x.append(e(a)),
                            Me(t, "aoDrawCallback", "draw", [t]),
                            t.bSorted = !1,
                            t.bFiltered = !1,
                            t.bDrawing = !1
                    } else de(t, !1)
                }
                function L(e, t) {
                    var n = e.oFeatures,
                        r = n.bSort,
                        a = n.bFilter;
                    r && Ce(e),
                        a ? W(e, e.oPreviousSearch) : e.aiDisplay = e.aiDisplayMaster.slice(),
                    !0 !== t && (e._iDisplayStart = 0),
                        e._drawHold = t,
                        H(e),
                        e._drawHold = !1
                }
                function O(t) {
                    var n = t.oClasses,
                        r = e(t.nTable),
                        a = e("<div/>").insertBefore(r),
                        o = t.oFeatures,
                        i = e("<div/>", {
                            id: t.sTableId + "_wrapper",
                            "class": n.sWrapper + (t.nTFoot ? "": " " + n.sNoFooter)
                        });
                    t.nHolding = a[0],
                        t.nTableWrapper = i[0],
                        t.nTableReinsertBefore = t.nTable.nextSibling;
                    for (var s, l, c, u, d, h, f = t.sDom.split(""), p = 0; p < f.length; p++) {
                        if (s = null, "<" == (l = f[p])) {
                            if (c = e("<div/>")[0], "'" == (u = f[p + 1]) || '"' == u) {
                                for (d = "", h = 2; f[p + h] != u;) d += f[p + h],
                                    h++;
                                if ("H" == d ? d = n.sJUIHeader: "F" == d && (d = n.sJUIFooter), -1 != d.indexOf(".")) {
                                    var m = d.split(".");
                                    c.id = m[0].substr(1, m[0].length - 1),
                                        c.className = m[1]
                                } else "#" == d.charAt(0) ? c.id = d.substr(1, d.length - 1) : c.className = d;
                                p += h
                            }
                            i.append(c),
                                i = e(c)
                        } else if (">" == l) i = i.parent();
                        else if ("l" == l && o.bPaginate && o.bLengthChange) s = se(t);
                        else if ("f" == l && o.bFilter) s = Q(t);
                        else if ("r" == l && o.bProcessing) s = ue(t);
                        else if ("t" == l) s = he(t);
                        else if ("i" == l && o.bInfo) s = te(t);
                        else if ("p" == l && o.bPaginate) s = le(t);
                        else if (0 !== Qe.ext.feature.length) for (var g = Qe.ext.feature,
                                                                       v = 0,
                                                                       b = g.length; v < b; v++) if (l == g[v].cFeature) {
                            s = g[v].fnInit(t);
                            break
                        }
                        if (s) {
                            var y = t.aanFeatures;
                            y[l] || (y[l] = []),
                                y[l].push(s),
                                i.append(s)
                        }
                    }
                    a.replaceWith(i),
                        t.nHolding = null
                }
                function N(t, n) {
                    var r, a, o, i, s, l, c, u, d, h, f, p = e(n).children("tr"),
                        m = function(e, t, n) {
                            for (var r = e[t]; r[n];) n++;
                            return n
                        };
                    for (t.splice(0, t.length), o = 0, l = p.length; o < l; o++) t.push([]);
                    for (o = 0, l = p.length; o < l; o++) for (u = 0, a = (r = p[o]).firstChild; a;) {
                        if ("TD" == a.nodeName.toUpperCase() || "TH" == a.nodeName.toUpperCase()) for (d = 1 * a.getAttribute("colspan"), h = 1 * a.getAttribute("rowspan"), d = d && 0 !== d && 1 !== d ? d: 1, h = h && 0 !== h && 1 !== h ? h: 1, c = m(t, o, u), f = 1 === d, s = 0; s < d; s++) for (i = 0; i < h; i++) t[o + i][c + s] = {
                            cell: a,
                            unique: f
                        },
                            t[o + i].nTr = r;
                        a = a.nextSibling
                    }
                }
                function B(e, t, n) {
                    var r = [];
                    n || (n = e.aoHeader, t && N(n = [], t));
                    for (var a = 0,
                             o = n.length; a < o; a++) for (var i = 0,
                                                                s = n[a].length; i < s; i++) ! n[a][i].unique || r[i] && e.bSortCellsTop || (r[i] = n[a][i].cell);
                    return r
                }
                function q(t, n, r) {
                    if (Me(t, "aoServerParams", "serverParams", [n]), n && e.isArray(n)) {
                        var a = {},
                            o = /(.*?)\[\]$/;
                        e.each(n,
                            function(e, t) {
                                var n = t.name.match(o);
                                if (n) {
                                    var r = n[0];
                                    a[r] || (a[r] = []),
                                        a[r].push(t.value)
                                } else a[t.name] = t.value
                            }),
                            n = a
                    }
                    var i, s = t.ajax,
                        l = t.oInstance,
                        c = function(e) {
                            Me(t, null, "xhr", [t, e, t.jqXHR]),
                                r(e)
                        };
                    if (e.isPlainObject(s) && s.data) {
                        i = s.data;
                        var u = e.isFunction(i) ? i(n, t) : i;
                        n = e.isFunction(i) && u ? u: e.extend(!0, n, u),
                            delete s.data
                    }
                    var d = {
                        data: n,
                        success: function(e) {
                            var n = e.error || e.sError;
                            n && je(t, 0, n),
                                t.json = e,
                                c(e)
                        },
                        dataType: "json",
                        cache: !1,
                        type: t.sServerMethod,
                        error: function(n, r, a) {
                            var o = Me(t, null, "xhr", [t, null, t.jqXHR]); - 1 === e.inArray(!0, o) && ("parsererror" == r ? je(t, 0, "Invalid JSON response", 1) : 4 === n.readyState && je(t, 0, "Ajax error", 7)),
                                de(t, !1)
                        }
                    };
                    t.oAjaxData = n,
                        Me(t, null, "preXhr", [t, n]),
                        t.fnServerData ? t.fnServerData.call(l, t.sAjaxSource, e.map(n,
                            function(e, t) {
                                return {
                                    name: t,
                                    value: e
                                }
                            }), c, t) : t.sAjaxSource || "string" == typeof s ? t.jqXHR = e.ajax(e.extend(d, {
                            url: s || t.sAjaxSource
                        })) : e.isFunction(s) ? t.jqXHR = s.call(l, n, c, t) : (t.jqXHR = e.ajax(e.extend(d, s)), s.data = i)
                }
                function z(e) {
                    return ! e.bAjaxDataGet || (e.iDraw++, de(e, !0), q(e, $(e),
                        function(t) {
                            U(e, t)
                        }), !1)
                }
                function $(t) {
                    var n, r, a, o, i = t.aoColumns,
                        s = i.length,
                        l = t.oFeatures,
                        c = t.oPreviousSearch,
                        u = t.aoPreSearchCols,
                        d = [],
                        h = _e(t),
                        f = t._iDisplayStart,
                        p = !1 !== l.bPaginate ? t._iDisplayLength: -1,
                        m = function(e, t) {
                            d.push({
                                name: e,
                                value: t
                            })
                        };
                    m("sEcho", t.iDraw),
                        m("iColumns", s),
                        m("sColumns", at(i, "sName").join(",")),
                        m("iDisplayStart", f),
                        m("iDisplayLength", p);
                    var g = {
                        draw: t.iDraw,
                        columns: [],
                        order: [],
                        start: f,
                        length: p,
                        search: {
                            value: c.sSearch,
                            regex: c.bRegex
                        }
                    };
                    for (n = 0; n < s; n++) a = i[n],
                        o = u[n],
                        r = "function" == typeof a.mData ? "function": a.mData,
                        g.columns.push({
                            data: r,
                            name: a.sName,
                            searchable: a.bSearchable,
                            orderable: a.bSortable,
                            search: {
                                value: o.sSearch,
                                regex: o.bRegex
                            }
                        }),
                        m("mDataProp_" + n, r),
                    l.bFilter && (m("sSearch_" + n, o.sSearch), m("bRegex_" + n, o.bRegex), m("bSearchable_" + n, a.bSearchable)),
                    l.bSort && m("bSortable_" + n, a.bSortable);
                    l.bFilter && (m("sSearch", c.sSearch), m("bRegex", c.bRegex)),
                    l.bSort && (e.each(h,
                        function(e, t) {
                            g.order.push({
                                column: t.col,
                                dir: t.dir
                            }),
                                m("iSortCol_" + e, t.col),
                                m("sSortDir_" + e, t.dir)
                        }), m("iSortingCols", h.length));
                    var v = Qe.ext.legacy.ajax;
                    return null === v ? t.sAjaxSource ? d: g: v ? d: g
                }
                function U(e, t) {
                    var n = function(e, n) {
                            return t[e] !== r ? t[e] : t[n]
                        },
                        a = V(e, t),
                        o = n("sEcho", "draw"),
                        i = n("iTotalRecords", "recordsTotal"),
                        s = n("iTotalDisplayRecords", "recordsFiltered");
                    if (o) {
                        if (1 * o < e.iDraw) return;
                        e.iDraw = 1 * o
                    }
                    D(e),
                        e._iRecordsTotal = parseInt(i, 10),
                        e._iRecordsDisplay = parseInt(s, 10);
                    for (var l = 0,
                             c = a.length; l < c; l++) _(e, a[l]);
                    e.aiDisplay = e.aiDisplayMaster.slice(),
                        e.bAjaxDataGet = !1,
                        H(e),
                    e._bInitComplete || oe(e, t),
                        e.bAjaxDataGet = !0,
                        de(e, !1)
                }
                function V(t, n) {
                    var a = e.isPlainObject(t.ajax) && t.ajax.dataSrc !== r ? t.ajax.dataSrc: t.sAjaxDataProp;
                    return "data" === a ? n.aaData || n[a] : "" !== a ? T(a)(n) : n
                }
                function Q(t) {
                    var r = t.oClasses,
                        a = t.sTableId,
                        o = t.oLanguage,
                        i = t.oPreviousSearch,
                        s = t.aanFeatures,
                        l = '<input type="search" class="' + r.sFilterInput + '"/>',
                        c = o.sSearch;
                    c = c.match(/_INPUT_/) ? c.replace("_INPUT_", l) : c + l;
                    var u = e("<div/>", {
                            id: s.f ? null: a + "_filter",
                            "class": r.sFilter
                        }).append(e("<label/>").append(c)),
                        d = function() {
                            s.f;
                            var e = this.value ? this.value: "";
                            e != i.sSearch && (W(t, {
                                sSearch: e,
                                bRegex: i.bRegex,
                                bSmart: i.bSmart,
                                bCaseInsensitive: i.bCaseInsensitive
                            }), t._iDisplayStart = 0, H(t))
                        },
                        h = null !== t.searchDelay ? t.searchDelay: "ssp" === Oe(t) ? 400 : 0,
                        f = e("input", u).val(i.sSearch).attr("placeholder", o.sSearchPlaceholder).on("keyup.DT search.DT input.DT paste.DT cut.DT", h ? vt(d, h) : d).on("keypress.DT",
                            function(e) {
                                if (13 == e.keyCode) return ! 1
                            }).attr("aria-controls", a);
                    return e(t.nTable).on("search.dt.DT",
                        function(e, r) {
                            if (t === r) try {
                                f[0] !== n.activeElement && f.val(i.sSearch)
                            } catch(a) {}
                        }),
                        u[0]
                }
                function W(e, t, n) {
                    var a = e.oPreviousSearch,
                        o = e.aoPreSearchCols,
                        i = function(e) {
                            a.sSearch = e.sSearch,
                                a.bRegex = e.bRegex,
                                a.bSmart = e.bSmart,
                                a.bCaseInsensitive = e.bCaseInsensitive
                        },
                        s = function(e) {
                            return e.bEscapeRegex !== r ? !e.bEscapeRegex: e.bRegex
                        };
                    if (b(e), "ssp" != Oe(e)) {
                        X(e, t.sSearch, n, s(t), t.bSmart, t.bCaseInsensitive),
                            i(t);
                        for (var l = 0; l < o.length; l++) J(e, o[l].sSearch, l, s(o[l]), o[l].bSmart, o[l].bCaseInsensitive);
                        K(e)
                    } else i(t);
                    e.bFiltered = !0,
                        Me(e, null, "search", [e])
                }
                function K(t) {
                    for (var n, r, a = Qe.ext.search,
                             o = t.aiDisplay,
                             i = 0,
                             s = a.length; i < s; i++) {
                        for (var l = [], c = 0, u = o.length; c < u; c++) r = o[c],
                            n = t.aoData[r],
                        a[i](t, n._aFilterData, r, n._aData, c) && l.push(r);
                        o.length = 0,
                            e.merge(o, l)
                    }
                }
                function J(e, t, n, r, a, o) {
                    if ("" !== t) {
                        for (var i, s = [], l = e.aiDisplay, c = G(t, r, a, o), u = 0; u < l.length; u++) i = e.aoData[l[u]]._aFilterData[n],
                        c.test(i) && s.push(l[u]);
                        e.aiDisplay = s
                    }
                }
                function X(e, t, n, r, a, o) {
                    var i, s, l, c = G(t, r, a, o),
                        u = e.oPreviousSearch.sSearch,
                        d = e.aiDisplayMaster,
                        h = [];
                    if (0 !== Qe.ext.search.length && (n = !0), s = Z(e), t.length <= 0) e.aiDisplay = d.slice();
                    else {
                        for ((s || n || u.length > t.length || 0 !== t.indexOf(u) || e.bSorted) && (e.aiDisplay = d.slice()), i = e.aiDisplay, l = 0; l < i.length; l++) c.test(e.aoData[i[l]]._sFilterRow) && h.push(i[l]);
                        e.aiDisplay = h
                    }
                }
                function G(t, n, r, a) {
                    if (t = n ? t: ft(t), r) {
                        t = "^(?=.*?" + e.map(t.match(/"[^"]+"|[^ ]+/g) || [""],
                            function(e) {
                                if ('"' === e.charAt(0)) {
                                    var t = e.match(/^"(.*)"$/);
                                    e = t ? t[1] : e
                                }
                                return e.replace('"', "")
                            }).join(")(?=.*?") + ").*$"
                    }
                    return new RegExp(t, a ? "i": "")
                }
                function Z(e) {
                    var t, n, r, a, o, i, s, l, c = e.aoColumns,
                        u = Qe.ext.type.search,
                        d = !1;
                    for (n = 0, a = e.aoData.length; n < a; n++) if (! (l = e.aoData[n])._aFilterData) {
                        for (i = [], r = 0, o = c.length; r < o; r++)(t = c[r]).bSearchable ? (s = w(e, n, r, "filter"), u[t.sType] && (s = u[t.sType](s)), null === s && (s = ""), "string" != typeof s && s.toString && (s = s.toString())) : s = "",
                        s.indexOf && -1 !== s.indexOf("&") && (pt.innerHTML = s, s = mt ? pt.textContent: pt.innerText),
                        s.replace && (s = s.replace(/[\r\n]/g, "")),
                            i.push(s);
                        l._aFilterData = i,
                            l._sFilterRow = i.join("  "),
                            d = !0
                    }
                    return d
                }
                function Y(e) {
                    return {
                        search: e.sSearch,
                        smart: e.bSmart,
                        regex: e.bRegex,
                        caseInsensitive: e.bCaseInsensitive
                    }
                }
                function ee(e) {
                    return {
                        sSearch: e.search,
                        bSmart: e.smart,
                        bRegex: e.regex,
                        bCaseInsensitive: e.caseInsensitive
                    }
                }
                function te(t) {
                    var n = t.sTableId,
                        r = t.aanFeatures.i,
                        a = e("<div/>", {
                            "class": t.oClasses.sInfo,
                            id: r ? null: n + "_info"
                        });
                    return r || (t.aoDrawCallback.push({
                        fn: ne,
                        sName: "information"
                    }), a.attr("role", "status").attr("aria-live", "polite"), e(t.nTable).attr("aria-describedby", n + "_info")),
                        a[0]
                }
                function ne(t) {
                    var n = t.aanFeatures.i;
                    if (0 !== n.length) {
                        var r = t.oLanguage,
                            a = t._iDisplayStart + 1,
                            o = t.fnDisplayEnd(),
                            i = t.fnRecordsTotal(),
                            s = t.fnRecordsDisplay(),
                            l = s ? r.sInfo: r.sInfoEmpty;
                        s !== i && (l += " " + r.sInfoFiltered),
                            l = re(t, l += r.sInfoPostFix);
                        var c = r.fnInfoCallback;
                        null !== c && (l = c.call(t.oInstance, t, a, o, i, s, l)),
                            e(n).html(l)
                    }
                }
                function re(e, t) {
                    var n = e.fnFormatNumber,
                        r = e._iDisplayStart + 1,
                        a = e._iDisplayLength,
                        o = e.fnRecordsDisplay(),
                        i = -1 === a;
                    return t.replace(/_START_/g, n.call(e, r)).replace(/_END_/g, n.call(e, e.fnDisplayEnd())).replace(/_MAX_/g, n.call(e, e.fnRecordsTotal())).replace(/_TOTAL_/g, n.call(e, o)).replace(/_PAGE_/g, n.call(e, i ? 1 : Math.ceil(r / a))).replace(/_PAGES_/g, n.call(e, i ? 1 : Math.ceil(o / a)))
                }
                function ae(e) {
                    var t, n, r, a = e.iInitDisplayStart,
                        o = e.aoColumns,
                        i = e.oFeatures,
                        s = e.bDeferLoading;
                    if (e.bInitialised) {
                        for (O(e), E(e), M(e, e.aoHeader), M(e, e.aoFooter), de(e, !0), i.bAutoWidth && me(e), t = 0, n = o.length; t < n; t++)(r = o[t]).sWidth && (r.nTh.style.width = ye(r.sWidth));
                        Me(e, null, "preInit", [e]),
                            L(e);
                        var l = Oe(e); ("ssp" != l || s) && ("ajax" == l ? q(e, [],
                            function(n) {
                                var r = V(e, n);
                                for (t = 0; t < r.length; t++) _(e, r[t]);
                                e.iInitDisplayStart = a,
                                    L(e),
                                    de(e, !1),
                                    oe(e, n)
                            }) : (de(e, !1), oe(e)))
                    } else setTimeout(function() {
                            ae(e)
                        },
                        200)
                }
                function oe(e, t) {
                    e._bInitComplete = !0,
                    (t || e.oInit.aaData) && f(e),
                        Me(e, null, "plugin-init", [e, t]),
                        Me(e, "aoInitComplete", "init", [e, t])
                }
                function ie(e, t) {
                    var n = parseInt(t, 10);
                    e._iDisplayLength = n,
                        He(e),
                        Me(e, null, "length", [e, n])
                }
                function se(t) {
                    for (var n = t.oClasses,
                             r = t.sTableId,
                             a = t.aLengthMenu,
                             o = e.isArray(a[0]), i = o ? a[0] : a, s = o ? a[1] : a, l = e("<select/>", {
                            name: r + "_length",
                            "aria-controls": r,
                            "class": n.sLengthSelect
                        }), c = 0, u = i.length; c < u; c++) l[0][c] = new Option("number" == typeof s[c] ? t.fnFormatNumber(s[c]) : s[c], i[c]);
                    var d = e("<div><label/></div>").addClass(n.sLength);
                    return t.aanFeatures.l || (d[0].id = r + "_length"),
                        d.children().append(t.oLanguage.sLengthMenu.replace("_MENU_", l[0].outerHTML)),
                        e("select", d).val(t._iDisplayLength).on("change.DT",
                            function(n) {
                                ie(t, e(this).val()),
                                    H(t)
                            }),
                        e(t.nTable).on("length.dt.DT",
                            function(n, r, a) {
                                t === r && e("select", d).val(a)
                            }),
                        d[0]
                }
                function le(t) {
                    var n = t.sPaginationType,
                        r = Qe.ext.pager[n],
                        a = "function" == typeof r,
                        o = function(e) {
                            H(e)
                        },
                        i = e("<div/>").addClass(t.oClasses.sPaging + n)[0],
                        s = t.aanFeatures;
                    return a || r.fnInit(t, i, o),
                    s.p || (i.id = t.sTableId + "_paginate", t.aoDrawCallback.push({
                        fn: function(e) {
                            if (a) {
                                var t, n, i = e._iDisplayStart,
                                    l = e._iDisplayLength,
                                    c = e.fnRecordsDisplay(),
                                    u = -1 === l,
                                    d = u ? 0 : Math.ceil(i / l),
                                    h = u ? 1 : Math.ceil(c / l),
                                    f = r(d, h);
                                for (t = 0, n = s.p.length; t < n; t++) Le(e, "pageButton")(e, s.p[t], t, f, d, h)
                            } else r.fnUpdate(e, o)
                        },
                        sName: "pagination"
                    })),
                        i
                }
                function ce(e, t, n) {
                    var r = e._iDisplayStart,
                        a = e._iDisplayLength,
                        o = e.fnRecordsDisplay();
                    0 === o || -1 === a ? r = 0 : "number" == typeof t ? (r = t * a) > o && (r = 0) : "first" == t ? r = 0 : "previous" == t ? (r = a >= 0 ? r - a: 0) < 0 && (r = 0) : "next" == t ? r + a < o && (r += a) : "last" == t ? r = Math.floor((o - 1) / a) * a: je(e, 0, "Unknown paging action: " + t, 5);
                    var i = e._iDisplayStart !== r;
                    return e._iDisplayStart = r,
                    i && (Me(e, null, "page", [e]), n && H(e)),
                        i
                }
                function ue(t) {
                    return e("<div/>", {
                        id: t.aanFeatures.r ? null: t.sTableId + "_processing",
                        "class": t.oClasses.sProcessing
                    }).html(t.oLanguage.sProcessing).insertBefore(t.nTable)[0]
                }
                function de(t, n) {
                    t.oFeatures.bProcessing && e(t.aanFeatures.r).css("display", n ? "block": "none"),
                        Me(t, null, "processing", [t, n])
                }
                function he(t) {
                    var n = e(t.nTable);
                    n.attr("role", "grid");
                    var r = t.oScroll;
                    if ("" === r.sX && "" === r.sY) return t.nTable;
                    var a = r.sX,
                        o = r.sY,
                        i = t.oClasses,
                        s = n.children("caption"),
                        l = s.length ? s[0]._captionSide: null,
                        c = e(n[0].cloneNode(!1)),
                        u = e(n[0].cloneNode(!1)),
                        d = n.children("tfoot"),
                        h = function(e) {
                            return e ? ye(e) : null
                        };
                    d.length || (d = null);
                    var f = e("<div/>", {
                        "class": i.sScrollWrapper
                    }).append(e("<div/>", {
                        "class": i.sScrollHead
                    }).css({
                        overflow: "hidden",
                        position: "relative",
                        border: 0,
                        width: a ? h(a) : "100%"
                    }).append(e("<div/>", {
                        "class": i.sScrollHeadInner
                    }).css({
                        "box-sizing": "content-box",
                        width: r.sXInner || "100%"
                    }).append(c.removeAttr("id").css("margin-left", 0).append("top" === l ? s: null).append(n.children("thead"))))).append(e("<div/>", {
                        "class": i.sScrollBody
                    }).css({
                        position: "relative",
                        overflow: "auto",
                        width: h(a)
                    }).append(n));
                    d && f.append(e("<div/>", {
                        "class": i.sScrollFoot
                    }).css({
                        overflow: "hidden",
                        border: 0,
                        width: a ? h(a) : "100%"
                    }).append(e("<div/>", {
                        "class": i.sScrollFootInner
                    }).append(u.removeAttr("id").css("margin-left", 0).append("bottom" === l ? s: null).append(n.children("tfoot")))));
                    var p = f.children(),
                        m = p[0],
                        g = p[1],
                        v = d ? p[2] : null;
                    return a && e(g).on("scroll.DT",
                        function(e) {
                            var t = this.scrollLeft;
                            m.scrollLeft = t,
                            d && (v.scrollLeft = t)
                        }),
                        e(g).css(o && r.bCollapse ? "max-height": "height", o),
                        t.nScrollHead = m,
                        t.nScrollBody = g,
                        t.nScrollFoot = v,
                        t.aoDrawCallback.push({
                            fn: fe,
                            sName: "scrolling"
                        }),
                        f[0]
                }
                function fe(t) {
                    var n, a, o, i, s, l, c, u, d, h = t.oScroll,
                        m = h.sX,
                        g = h.sXInner,
                        v = h.sY,
                        b = h.iBarWidth,
                        y = e(t.nScrollHead),
                        _ = y[0].style,
                        C = y.children("div"),
                        w = C[0].style,
                        x = C.children("table"),
                        S = t.nScrollBody,
                        T = e(S),
                        A = S.style,
                        k = e(t.nScrollFoot).children("div"),
                        D = k.children("table"),
                        P = e(t.nTHead),
                        j = e(t.nTable),
                        F = j[0],
                        I = F.style,
                        R = t.nTFoot ? e(t.nTFoot) : null,
                        E = t.oBrowser,
                        M = E.bScrollOversize,
                        H = at(t.aoColumns, "nTh"),
                        L = [],
                        O = [],
                        N = [],
                        q = [],
                        z = function(e) {
                            var t = e.style;
                            t.paddingTop = "0",
                                t.paddingBottom = "0",
                                t.borderTopWidth = "0",
                                t.borderBottomWidth = "0",
                                t.height = 0
                        },
                        $ = S.scrollHeight > S.clientHeight;
                    if (t.scrollBarVis !== $ && t.scrollBarVis !== r) return t.scrollBarVis = $,
                        void f(t);
                    t.scrollBarVis = $,
                        j.children("thead, tfoot").remove(),
                    R && (l = R.clone().prependTo(j), a = R.find("tr"), i = l.find("tr")),
                        s = P.clone().prependTo(j),
                        n = P.find("tr"),
                        o = s.find("tr"),
                        s.find("th, td").removeAttr("tabindex"),
                    m || (A.width = "100%", y[0].style.width = "100%"),
                        e.each(B(t, s),
                            function(e, n) {
                                c = p(t, e),
                                    n.style.width = t.aoColumns[c].sWidth
                            }),
                    R && pe(function(e) {
                            e.style.width = ""
                        },
                        i),
                        d = j.outerWidth(),
                        "" === m ? (I.width = "100%", M && (j.find("tbody").height() > S.offsetHeight || "scroll" == T.css("overflow-y")) && (I.width = ye(j.outerWidth() - b)), d = j.outerWidth()) : "" !== g && (I.width = ye(g), d = j.outerWidth()),
                        pe(z, o),
                        pe(function(t) {
                                N.push(t.innerHTML),
                                    L.push(ye(e(t).css("width")))
                            },
                            o),
                        pe(function(t, n) { - 1 !== e.inArray(t, H) && (t.style.width = L[n])
                            },
                            n),
                        e(o).height(0),
                    R && (pe(z, i), pe(function(t) {
                            q.push(t.innerHTML),
                                O.push(ye(e(t).css("width")))
                        },
                        i), pe(function(e, t) {
                            e.style.width = O[t]
                        },
                        a), e(i).height(0)),
                        pe(function(e, t) {
                                e.innerHTML = '<div class="dataTables_sizing" style="height:0;overflow:hidden;">' + N[t] + "</div>",
                                    e.style.width = L[t]
                            },
                            o),
                    R && pe(function(e, t) {
                            e.innerHTML = '<div class="dataTables_sizing" style="height:0;overflow:hidden;">' + q[t] + "</div>",
                                e.style.width = O[t]
                        },
                        i),
                        j.outerWidth() < d ? (u = S.scrollHeight > S.offsetHeight || "scroll" == T.css("overflow-y") ? d + b: d, M && (S.scrollHeight > S.offsetHeight || "scroll" == T.css("overflow-y")) && (I.width = ye(u - b)), "" !== m && "" === g || je(t, 1, "Possible column misalignment", 6)) : u = "100%",
                        A.width = ye(u),
                        _.width = ye(u),
                    R && (t.nScrollFoot.style.width = ye(u)),
                    v || M && (A.height = ye(F.offsetHeight + b));
                    var U = j.outerWidth();
                    x[0].style.width = ye(U),
                        w.width = ye(U);
                    var V = j.height() > S.clientHeight || "scroll" == T.css("overflow-y"),
                        Q = "padding" + (E.bScrollbarLeft ? "Left": "Right");
                    w[Q] = V ? b + "px": "0px",
                    R && (D[0].style.width = ye(U), k[0].style.width = ye(U), k[0].style[Q] = V ? b + "px": "0px"),
                        j.children("colgroup").insertBefore(j.children("thead")),
                        T.scroll(),
                    !t.bSorted && !t.bFiltered || t._drawHold || (S.scrollTop = 0)
                }
                function pe(e, t, n) {
                    for (var r, a, o = 0,
                             i = 0,
                             s = t.length; i < s;) {
                        for (r = t[i].firstChild, a = n ? n[i].firstChild: null; r;) 1 === r.nodeType && (n ? e(r, a, o) : e(r, o), o++),
                            r = r.nextSibling,
                            a = n ? a.nextSibling: null;
                        i++
                    }
                }
                function me(n) {
                    var r, a, o, i = n.nTable,
                        s = n.aoColumns,
                        l = n.oScroll,
                        c = l.sY,
                        u = l.sX,
                        d = l.sXInner,
                        h = s.length,
                        m = v(n, "bVisible"),
                        b = e("th", n.nTHead),
                        y = i.getAttribute("width"),
                        _ = i.parentNode,
                        C = !1,
                        w = n.oBrowser,
                        x = w.bScrollOversize,
                        S = i.style.width;
                    for (S && -1 !== S.indexOf("%") && (y = S), r = 0; r < m.length; r++) null !== (a = s[m[r]]).sWidth && (a.sWidth = ge(a.sWidthOrig, _), C = !0);
                    if (x || !C && !u && !c && h == g(n) && h == b.length) for (r = 0; r < h; r++) {
                        var T = p(n, r);
                        null !== T && (s[T].sWidth = ye(b.eq(r).width()))
                    } else {
                        var A = e(i).clone().css("visibility", "hidden").removeAttr("id");
                        A.find("tbody tr").remove();
                        var k = e("<tr/>").appendTo(A.find("tbody"));
                        for (A.find("thead, tfoot").remove(), A.append(e(n.nTHead).clone()).append(e(n.nTFoot).clone()), A.find("tfoot th, tfoot td").css("width", ""), b = B(n, A.find("thead")[0]), r = 0; r < m.length; r++) a = s[m[r]],
                            b[r].style.width = null !== a.sWidthOrig && "" !== a.sWidthOrig ? ye(a.sWidthOrig) : "",
                        a.sWidthOrig && u && e(b[r]).append(e("<div/>").css({
                            width: a.sWidthOrig,
                            margin: 0,
                            padding: 0,
                            border: 0,
                            height: 1
                        }));
                        if (n.aoData.length) for (r = 0; r < m.length; r++) a = s[o = m[r]],
                            e(ve(n, o)).clone(!1).append(a.sContentPadding).appendTo(k);
                        e("[name]", A).removeAttr("name");
                        var D = e("<div/>").css(u || c ? {
                            position: "absolute",
                            top: 0,
                            left: 0,
                            height: 1,
                            right: 0,
                            overflow: "hidden"
                        }: {}).append(A).appendTo(_);
                        u && d ? A.width(d) : u ? (A.css("width", "auto"), A.removeAttr("width"), A.width() < _.clientWidth && y && A.width(_.clientWidth)) : c ? A.width(_.clientWidth) : y && A.width(y);
                        var P = 0;
                        for (r = 0; r < m.length; r++) {
                            var j = e(b[r]),
                                F = j.outerWidth() - j.width(),
                                I = w.bBounding ? Math.ceil(b[r].getBoundingClientRect().width) : j.outerWidth();
                            P += I,
                                s[m[r]].sWidth = ye(I - F)
                        }
                        i.style.width = ye(P),
                            D.remove()
                    }
                    if (y && (i.style.width = ye(y)), (y || u) && !n._reszEvt) {
                        var R = function() {
                            e(t).on("resize.DT-" + n.sInstance, vt(function() {
                                f(n)
                            }))
                        };
                        x ? setTimeout(R, 1e3) : R(),
                            n._reszEvt = !0
                    }
                }
                function ge(t, r) {
                    if (!t) return 0;
                    var a = e("<div/>").css("width", ye(t)).appendTo(r || n.body),
                        o = a[0].offsetWidth;
                    return a.remove(),
                        o
                }
                function ve(t, n) {
                    var r = be(t, n);
                    if (r < 0) return null;
                    var a = t.aoData[r];
                    return a.nTr ? a.anCells[n] : e("<td/>").html(w(t, r, n, "display"))[0]
                }
                function be(e, t) {
                    for (var n, r = -1,
                             a = -1,
                             o = 0,
                             i = e.aoData.length; o < i; o++)(n = (n = (n = w(e, o, t, "display") + "").replace(gt, "")).replace(/&nbsp;/g, " ")).length > r && (r = n.length, a = o);
                    return a
                }
                function ye(e) {
                    return null === e ? "0px": "number" == typeof e ? e < 0 ? "0px": e + "px": e.match(/\d$/) ? e + "px": e
                }
                function _e(t) {
                    var n, a, o, i, s, l, c, u = [],
                        d = t.aoColumns,
                        h = t.aaSortingFixed,
                        f = e.isPlainObject(h),
                        p = [],
                        m = function(t) {
                            t.length && !e.isArray(t[0]) ? p.push(t) : e.merge(p, t)
                        };
                    for (e.isArray(h) && m(h), f && h.pre && m(h.pre), m(t.aaSorting), f && h.post && m(h.post), n = 0; n < p.length; n++) for (a = 0, o = (i = d[c = p[n][0]].aDataSort).length; a < o; a++) l = d[s = i[a]].sType || "string",
                    p[n]._idx === r && (p[n]._idx = e.inArray(p[n][1], d[s].asSorting)),
                        u.push({
                            src: c,
                            col: s,
                            dir: p[n][1],
                            index: p[n]._idx,
                            type: l,
                            formatter: Qe.ext.type.order[l + "-pre"]
                        });
                    return u
                }
                function Ce(e) {
                    var t, n, r, a, o, i = [],
                        s = Qe.ext.type.order,
                        l = e.aoData,
                        c = (e.aoColumns, 0),
                        u = e.aiDisplayMaster;
                    for (b(e), t = 0, n = (o = _e(e)).length; t < n; t++)(a = o[t]).formatter && c++,
                        Ae(e, a.col);
                    if ("ssp" != Oe(e) && 0 !== o.length) {
                        for (t = 0, r = u.length; t < r; t++) i[u[t]] = t;
                        c === o.length ? u.sort(function(e, t) {
                            var n, r, a, s, c, u = o.length,
                                d = l[e]._aSortData,
                                h = l[t]._aSortData;
                            for (a = 0; a < u; a++) if (c = o[a], n = d[c.col], r = h[c.col], 0 !== (s = n < r ? -1 : n > r ? 1 : 0)) return "asc" === c.dir ? s: -s;
                            return n = i[e],
                                r = i[t],
                                n < r ? -1 : n > r ? 1 : 0
                        }) : u.sort(function(e, t) {
                            var n, r, a, c, u, d, h = o.length,
                                f = l[e]._aSortData,
                                p = l[t]._aSortData;
                            for (a = 0; a < h; a++) if (u = o[a], n = f[u.col], r = p[u.col], d = s[u.type + "-" + u.dir] || s["string-" + u.dir], 0 !== (c = d(n, r))) return c;
                            return n = i[e],
                                r = i[t],
                                n < r ? -1 : n > r ? 1 : 0
                        })
                    }
                    e.bSorted = !0
                }
                function we(e) {
                    for (var t, n, r = e.aoColumns,
                             a = _e(e), o = e.oLanguage.oAria, i = 0, s = r.length; i < s; i++) {
                        var l = r[i],
                            c = l.asSorting,
                            u = l.sTitle.replace(/<.*?>/g, ""),
                            d = l.nTh;
                        d.removeAttribute("aria-sort"),
                            l.bSortable ? (a.length > 0 && a[0].col == i ? (d.setAttribute("aria-sort", "asc" == a[0].dir ? "ascending": "descending"), n = c[a[0].index + 1] || c[0]) : n = c[0], t = u + ("asc" === n ? o.sSortAscending: o.sSortDescending)) : t = u,
                            d.setAttribute("aria-label", t)
                    }
                }
                function xe(t, n, a, o) {
                    var i, s = t.aoColumns[n],
                        l = t.aaSorting,
                        c = s.asSorting,
                        u = function(t, n) {
                            var a = t._idx;
                            return a === r && (a = e.inArray(t[1], c)),
                                a + 1 < c.length ? a + 1 : n ? null: 0
                        };
                    if ("number" == typeof l[0] && (l = t.aaSorting = [l]), a && t.oFeatures.bSortMulti) {
                        var d = e.inArray(n, at(l, "0")); - 1 !== d ? (null === (i = u(l[d], !0)) && 1 === l.length && (i = 0), null === i ? l.splice(d, 1) : (l[d][1] = c[i], l[d]._idx = i)) : (l.push([n, c[0], 0]), l[l.length - 1]._idx = 0)
                    } else l.length && l[0][0] == n ? (i = u(l[0]), l.length = 1, l[0][1] = c[i], l[0]._idx = i) : (l.length = 0, l.push([n, c[0]]), l[0]._idx = 0);
                    L(t),
                    "function" == typeof o && o(t)
                }
                function Se(e, t, n, r) {
                    var a = e.aoColumns[n];
                    Re(t, {},
                        function(t) { ! 1 !== a.bSortable && (e.oFeatures.bProcessing ? (de(e, !0), setTimeout(function() {
                                xe(e, n, t.shiftKey, r),
                                "ssp" !== Oe(e) && de(e, !1)
                            },
                            0)) : xe(e, n, t.shiftKey, r))
                        })
                }
                function Te(t) {
                    var n, r, a, o = t.aLastSort,
                        i = t.oClasses.sSortColumn,
                        s = _e(t),
                        l = t.oFeatures;
                    if (l.bSort && l.bSortClasses) {
                        for (n = 0, r = o.length; n < r; n++) a = o[n].src,
                            e(at(t.aoData, "anCells", a)).removeClass(i + (n < 2 ? n + 1 : 3));
                        for (n = 0, r = s.length; n < r; n++) a = s[n].src,
                            e(at(t.aoData, "anCells", a)).addClass(i + (n < 2 ? n + 1 : 3))
                    }
                    t.aLastSort = s
                }
                function Ae(e, t) {
                    var n, r = e.aoColumns[t],
                        a = Qe.ext.order[r.sSortDataType];
                    a && (n = a.call(e.oInstance, e, t, m(e, t)));
                    for (var o, i, s = Qe.ext.type.order[r.sType + "-pre"], l = 0, c = e.aoData.length; l < c; l++)(o = e.aoData[l])._aSortData || (o._aSortData = []),
                    o._aSortData[t] && !a || (i = a ? n[l] : w(e, l, t, "sort"), o._aSortData[t] = s ? s(i) : i)
                }
                function ke(t) {
                    if (t.oFeatures.bStateSave && !t.bDestroying) {
                        var n = {
                            time: +new Date,
                            start: t._iDisplayStart,
                            length: t._iDisplayLength,
                            order: e.extend(!0, [], t.aaSorting),
                            search: Y(t.oPreviousSearch),
                            columns: e.map(t.aoColumns,
                                function(e, n) {
                                    return {
                                        visible: e.bVisible,
                                        search: Y(t.aoPreSearchCols[n])
                                    }
                                })
                        };
                        Me(t, "aoStateSaveParams", "stateSaveParams", [t, n]),
                            t.oSavedState = n,
                            t.fnStateSaveCallback.call(t.oInstance, t, n)
                    }
                }
                function De(t, n, a) {
                    var o, i, s = t.aoColumns,
                        l = function(n) {
                            if (n && n.time) {
                                var l = Me(t, "aoStateLoadParams", "stateLoadParams", [t, n]);
                                if ( - 1 === e.inArray(!1, l)) {
                                    var c = t.iStateDuration;
                                    if (c > 0 && n.time < +new Date - 1e3 * c) a();
                                    else if (n.columns && s.length !== n.columns.length) a();
                                    else {
                                        if (t.oLoadedState = e.extend(!0, {},
                                            n), n.start !== r && (t._iDisplayStart = n.start, t.iInitDisplayStart = n.start), n.length !== r && (t._iDisplayLength = n.length), n.order !== r && (t.aaSorting = [], e.each(n.order,
                                            function(e, n) {
                                                t.aaSorting.push(n[0] >= s.length ? [0, n[1]] : n)
                                            })), n.search !== r && e.extend(t.oPreviousSearch, ee(n.search)), n.columns) for (o = 0, i = n.columns.length; o < i; o++) {
                                            var u = n.columns[o];
                                            u.visible !== r && (s[o].bVisible = u.visible),
                                            u.search !== r && e.extend(t.aoPreSearchCols[o], ee(u.search))
                                        }
                                        Me(t, "aoStateLoaded", "stateLoaded", [t, n]),
                                            a()
                                    }
                                } else a()
                            } else a()
                        };
                    if (t.oFeatures.bStateSave) {
                        var c = t.fnStateLoadCallback.call(t.oInstance, t, l);
                        c !== r && l(c)
                    } else a()
                }
                function Pe(t) {
                    var n = Qe.settings,
                        r = e.inArray(t, at(n, "nTable"));
                    return - 1 !== r ? n[r] : null
                }
                function je(e, n, r, a) {
                    if (r = "DataTables warning: " + (e ? "table id=" + e.sTableId + " - ": "") + r, a && (r += ". For more information about this error, please see http://datatables.net/tn/" + a), n) t.console && console.log;
                    else {
                        var o = Qe.ext,
                            i = o.sErrMode || o.errMode;
                        if (e && Me(e, null, "error", [e, a, r]), "alert" == i) alert(r);
                        else {
                            if ("throw" == i) throw new Error(r);
                            "function" == typeof i && i(e, a, r)
                        }
                    }
                }
                function Fe(t, n, a, o) {
                    e.isArray(a) ? e.each(a,
                        function(r, a) {
                            e.isArray(a) ? Fe(t, n, a[0], a[1]) : Fe(t, n, a)
                        }) : (o === r && (o = a), n[a] !== r && (t[o] = n[a]))
                }
                function Ie(t, n, r) {
                    var a;
                    for (var o in n) n.hasOwnProperty(o) && (a = n[o], e.isPlainObject(a) ? (e.isPlainObject(t[o]) || (t[o] = {}), e.extend(!0, t[o], a)) : r && "data" !== o && "aaData" !== o && e.isArray(a) ? t[o] = a.slice() : t[o] = a);
                    return t
                }
                function Re(t, n, r) {
                    e(t).on("click.DT", n,
                        function(e) {
                            t.blur(),
                                r(e)
                        }).on("keypress.DT", n,
                        function(e) {
                            13 === e.which && (e.preventDefault(), r(e))
                        }).on("selectstart.DT",
                        function() {
                            return ! 1
                        })
                }
                function Ee(e, t, n, r) {
                    n && e[t].push({
                        fn: n,
                        sName: r
                    })
                }
                function Me(t, n, r, a) {
                    var o = [];
                    if (n && (o = e.map(t[n].slice().reverse(),
                        function(e, n) {
                            return e.fn.apply(t.oInstance, a)
                        })), null !== r) {
                        var i = e.Event(r + ".dt");
                        e(t.nTable).trigger(i, a),
                            o.push(i.result)
                    }
                    return o
                }
                function He(e) {
                    var t = e._iDisplayStart,
                        n = e.fnDisplayEnd(),
                        r = e._iDisplayLength;
                    t >= n && (t = n - r),
                        t -= t % r,
                    ( - 1 === r || t < 0) && (t = 0),
                        e._iDisplayStart = t
                }
                function Le(t, n) {
                    var r = t.renderer,
                        a = Qe.ext.renderer[n];
                    return e.isPlainObject(r) && r[n] ? a[r[n]] || a._: "string" == typeof r ? a[r] || a._: a._
                }
                function Oe(e) {
                    return e.oFeatures.bServerSide ? "ssp": e.ajax || e.sAjaxSource ? "ajax": "dom"
                }
                function Ne(e, t) {
                    var n = [],
                        r = It.numbers_length,
                        a = Math.floor(r / 2);
                    return t <= r ? n = it(0, t) : e <= a ? ((n = it(0, r - 2)).push("ellipsis"), n.push(t - 1)) : e >= t - 1 - a ? ((n = it(t - (r - 2), t)).splice(0, 0, "ellipsis"), n.splice(0, 0, 0)) : ((n = it(e - a + 2, e + a - 1)).push("ellipsis"), n.push(t - 1), n.splice(0, 0, "ellipsis"), n.splice(0, 0, 0)),
                        n.DT_el = "span",
                        n
                }
                function Be(t) {
                    e.each({
                            num: function(e) {
                                return Rt(e, t)
                            },
                            "num-fmt": function(e) {
                                return Rt(e, t, Ze)
                            },
                            "html-num": function(e) {
                                return Rt(e, t, Je)
                            },
                            "html-num-fmt": function(e) {
                                return Rt(e, t, Je, Ze)
                            }
                        },
                        function(e, n) {
                            ze.type.order[e + t + "-pre"] = n,
                            e.match(/^html\-/) && (ze.type.search[e + t] = ze.type.search.html)
                        })
                }
                function qe(e) {
                    return function() {
                        var t = [Pe(this[Qe.ext.iApiIndex])].concat(Array.prototype.slice.call(arguments));
                        return Qe.ext.internal[e].apply(this, t)
                    }
                }
                var ze, $e, Ue, Ve, Qe = function(t) {
                        this.$ = function(e, t) {
                            return this.api(!0).$(e, t)
                        },
                            this._ = function(e, t) {
                                return this.api(!0).rows(e, t).data()
                            },
                            this.api = function(e) {
                                return new $e(e ? Pe(this[ze.iApiIndex]) : this)
                            },
                            this.fnAddData = function(t, n) {
                                var a = this.api(!0),
                                    o = e.isArray(t) && (e.isArray(t[0]) || e.isPlainObject(t[0])) ? a.rows.add(t) : a.row.add(t);
                                return (n === r || n) && a.draw(),
                                    o.flatten().toArray()
                            },
                            this.fnAdjustColumnSizing = function(e) {
                                var t = this.api(!0).columns.adjust(),
                                    n = t.settings()[0],
                                    a = n.oScroll;
                                e === r || e ? t.draw(!1) : "" === a.sX && "" === a.sY || fe(n)
                            },
                            this.fnClearTable = function(e) {
                                var t = this.api(!0).clear(); (e === r || e) && t.draw()
                            },
                            this.fnClose = function(e) {
                                this.api(!0).row(e).child.hide()
                            },
                            this.fnDeleteRow = function(e, t, n) {
                                var a = this.api(!0),
                                    o = a.rows(e),
                                    i = o.settings()[0],
                                    s = i.aoData[o[0][0]];
                                return o.remove(),
                                t && t.call(this, i, s),
                                (n === r || n) && a.draw(),
                                    s
                            },
                            this.fnDestroy = function(e) {
                                this.api(!0).destroy(e)
                            },
                            this.fnDraw = function(e) {
                                this.api(!0).draw(e)
                            },
                            this.fnFilter = function(e, t, n, a, o, i) {
                                var s = this.api(!0);
                                null === t || t === r ? s.search(e, n, a, i) : s.column(t).search(e, n, a, i),
                                    s.draw()
                            },
                            this.fnGetData = function(e, t) {
                                var n = this.api(!0);
                                if (e !== r) {
                                    var a = e.nodeName ? e.nodeName.toLowerCase() : "";
                                    return t !== r || "td" == a || "th" == a ? n.cell(e, t).data() : n.row(e).data() || null
                                }
                                return n.data().toArray()
                            },
                            this.fnGetNodes = function(e) {
                                var t = this.api(!0);
                                return e !== r ? t.row(e).node() : t.rows().nodes().flatten().toArray()
                            },
                            this.fnGetPosition = function(e) {
                                var t = this.api(!0),
                                    n = e.nodeName.toUpperCase();
                                if ("TR" == n) return t.row(e).index();
                                if ("TD" == n || "TH" == n) {
                                    var r = t.cell(e).index();
                                    return [r.row, r.columnVisible, r.column]
                                }
                                return null
                            },
                            this.fnIsOpen = function(e) {
                                return this.api(!0).row(e).child.isShown()
                            },
                            this.fnOpen = function(e, t, n) {
                                return this.api(!0).row(e).child(t, n).show().child()[0]
                            },
                            this.fnPageChange = function(e, t) {
                                var n = this.api(!0).page(e); (t === r || t) && n.draw(!1)
                            },
                            this.fnSetColumnVis = function(e, t, n) {
                                var a = this.api(!0).column(e).visible(t); (n === r || n) && a.columns.adjust().draw()
                            },
                            this.fnSettings = function() {
                                return Pe(this[ze.iApiIndex])
                            },
                            this.fnSort = function(e) {
                                this.api(!0).order(e).draw()
                            },
                            this.fnSortListener = function(e, t, n) {
                                this.api(!0).order.listener(e, t, n)
                            },
                            this.fnUpdate = function(e, t, n, a, o) {
                                var i = this.api(!0);
                                return n === r || null === n ? i.row(t).data(e) : i.cell(t, n).data(e),
                                (o === r || o) && i.columns.adjust(),
                                (a === r || a) && i.draw(),
                                    0
                            },
                            this.fnVersionCheck = ze.fnVersionCheck;
                        var n = this,
                            a = t === r,
                            u = this.length;
                        a && (t = {}),
                            this.oApi = this.internal = ze.internal;
                        for (var f in Qe.ext.internal) f && (this[f] = qe(f));
                        return this.each(function() {
                            var f, p = u > 1 ? Ie({},
                                t, !0) : t,
                                m = 0,
                                g = this.getAttribute("id"),
                                v = !1,
                                b = Qe.defaults,
                                w = e(this);
                            if ("table" == this.nodeName.toLowerCase()) {
                                s(b),
                                    l(b.column),
                                    o(b, b, !0),
                                    o(b.column, b.column, !0),
                                    o(b, e.extend(p, w.data()));
                                var x = Qe.settings;
                                for (m = 0, f = x.length; m < f; m++) {
                                    var S = x[m];
                                    if (S.nTable == this || S.nTHead.parentNode == this || S.nTFoot && S.nTFoot.parentNode == this) {
                                        var A = p.bRetrieve !== r ? p.bRetrieve: b.bRetrieve,
                                            k = p.bDestroy !== r ? p.bDestroy: b.bDestroy;
                                        if (a || A) return S.oInstance;
                                        if (k) {
                                            S.oInstance.fnDestroy();
                                            break
                                        }
                                        return void je(S, 0, "Cannot reinitialise DataTable", 3)
                                    }
                                    if (S.sTableId == this.id) {
                                        x.splice(m, 1);
                                        break
                                    }
                                }
                                null !== g && "" !== g || (g = "DataTables_Table_" + Qe.ext._unique++, this.id = g);
                                var D = e.extend(!0, {},
                                    Qe.models.oSettings, {
                                        sDestroyWidth: w[0].style.width,
                                        sInstance: g,
                                        sTableId: g
                                    });
                                D.nTable = this,
                                    D.oApi = n.internal,
                                    D.oInit = p,
                                    x.push(D),
                                    D.oInstance = 1 === n.length ? n: w.dataTable(),
                                    s(p),
                                p.oLanguage && i(p.oLanguage),
                                p.aLengthMenu && !p.iDisplayLength && (p.iDisplayLength = e.isArray(p.aLengthMenu[0]) ? p.aLengthMenu[0][0] : p.aLengthMenu[0]),
                                    p = Ie(e.extend(!0, {},
                                        b), p),
                                    Fe(D.oFeatures, p, ["bPaginate", "bLengthChange", "bFilter", "bSort", "bSortMulti", "bInfo", "bProcessing", "bAutoWidth", "bSortClasses", "bServerSide", "bDeferRender"]),
                                    Fe(D, p, ["asStripeClasses", "ajax", "fnServerData", "fnFormatNumber", "sServerMethod", "aaSorting", "aaSortingFixed", "aLengthMenu", "sPaginationType", "sAjaxSource", "sAjaxDataProp", "iStateDuration", "sDom", "bSortCellsTop", "iTabIndex", "fnStateLoadCallback", "fnStateSaveCallback", "renderer", "searchDelay", "rowId", ["iCookieDuration", "iStateDuration"], ["oSearch", "oPreviousSearch"], ["aoSearchCols", "aoPreSearchCols"], ["iDisplayLength", "_iDisplayLength"]]),
                                    Fe(D.oScroll, p, [["sScrollX", "sX"], ["sScrollXInner", "sXInner"], ["sScrollY", "sY"], ["bScrollCollapse", "bCollapse"]]),
                                    Fe(D.oLanguage, p, "fnInfoCallback"),
                                    Ee(D, "aoDrawCallback", p.fnDrawCallback, "user"),
                                    Ee(D, "aoServerParams", p.fnServerParams, "user"),
                                    Ee(D, "aoStateSaveParams", p.fnStateSaveParams, "user"),
                                    Ee(D, "aoStateLoadParams", p.fnStateLoadParams, "user"),
                                    Ee(D, "aoStateLoaded", p.fnStateLoaded, "user"),
                                    Ee(D, "aoRowCallback", p.fnRowCallback, "user"),
                                    Ee(D, "aoRowCreatedCallback", p.fnCreatedRow, "user"),
                                    Ee(D, "aoHeaderCallback", p.fnHeaderCallback, "user"),
                                    Ee(D, "aoFooterCallback", p.fnFooterCallback, "user"),
                                    Ee(D, "aoInitComplete", p.fnInitComplete, "user"),
                                    Ee(D, "aoPreDrawCallback", p.fnPreDrawCallback, "user"),
                                    D.rowIdFn = T(p.rowId),
                                    c(D);
                                var P = D.oClasses;
                                if (e.extend(P, Qe.ext.classes, p.oClasses), w.addClass(P.sTable), D.iInitDisplayStart === r && (D.iInitDisplayStart = p.iDisplayStart, D._iDisplayStart = p.iDisplayStart), null !== p.iDeferLoading) {
                                    D.bDeferLoading = !0;
                                    var j = e.isArray(p.iDeferLoading);
                                    D._iRecordsDisplay = j ? p.iDeferLoading[0] : p.iDeferLoading,
                                        D._iRecordsTotal = j ? p.iDeferLoading[1] : p.iDeferLoading
                                }
                                var F = D.oLanguage;
                                e.extend(!0, F, p.oLanguage),
                                F.sUrl && (e.ajax({
                                    dataType: "json",
                                    url: F.sUrl,
                                    success: function(t) {
                                        i(t),
                                            o(b.oLanguage, t),
                                            e.extend(!0, F, t),
                                            ae(D)
                                    },
                                    error: function() {
                                        ae(D)
                                    }
                                }), v = !0),
                                null === p.asStripeClasses && (D.asStripeClasses = [P.sStripeOdd, P.sStripeEven]);
                                var I = D.asStripeClasses,
                                    R = w.children("tbody").find("tr").eq(0); - 1 !== e.inArray(!0, e.map(I,
                                    function(e, t) {
                                        return R.hasClass(e)
                                    })) && (e("tbody tr", this).removeClass(I.join(" ")), D.asDestroyStripes = I.slice());
                                var E, M = [],
                                    H = this.getElementsByTagName("thead");
                                if (0 !== H.length && (N(D.aoHeader, H[0]), M = B(D)), null === p.aoColumns) for (E = [], m = 0, f = M.length; m < f; m++) E.push(null);
                                else E = p.aoColumns;
                                for (m = 0, f = E.length; m < f; m++) d(D, M ? M[m] : null);
                                if (y(D, p.aoColumnDefs, E,
                                    function(e, t) {
                                        h(D, e, t)
                                    }), R.length) {
                                    var L = function(e, t) {
                                        return null !== e.getAttribute("data-" + t) ? t: null
                                    };
                                    e(R[0]).children("th, td").each(function(e, t) {
                                        var n = D.aoColumns[e];
                                        if (n.mData === e) {
                                            var a = L(t, "sort") || L(t, "order"),
                                                o = L(t, "filter") || L(t, "search");
                                            null === a && null === o || (n.mData = {
                                                _: e + ".display",
                                                sort: null !== a ? e + ".@data-" + a: r,
                                                type: null !== a ? e + ".@data-" + a: r,
                                                filter: null !== o ? e + ".@data-" + o: r
                                            },
                                                h(D, e))
                                        }
                                    })
                                }
                                var O = D.oFeatures,
                                    q = function() {
                                        if (p.aaSorting === r) {
                                            var t = D.aaSorting;
                                            for (m = 0, f = t.length; m < f; m++) t[m][1] = D.aoColumns[m].asSorting[0]
                                        }
                                        Te(D),
                                        O.bSort && Ee(D, "aoDrawCallback",
                                            function() {
                                                if (D.bSorted) {
                                                    var t = _e(D),
                                                        n = {};
                                                    e.each(t,
                                                        function(e, t) {
                                                            n[t.src] = t.dir
                                                        }),
                                                        Me(D, null, "order", [D, t, n]),
                                                        we(D)
                                                }
                                            }),
                                            Ee(D, "aoDrawCallback",
                                                function() { (D.bSorted || "ssp" === Oe(D) || O.bDeferRender) && Te(D)
                                                },
                                                "sc");
                                        var n = w.children("caption").each(function() {
                                                this._captionSide = e(this).css("caption-side")
                                            }),
                                            a = w.children("thead");
                                        0 === a.length && (a = e("<thead/>").appendTo(w)),
                                            D.nTHead = a[0];
                                        var o = w.children("tbody");
                                        0 === o.length && (o = e("<tbody/>").appendTo(w)),
                                            D.nTBody = o[0];
                                        var i = w.children("tfoot");
                                        if (0 === i.length && n.length > 0 && ("" !== D.oScroll.sX || "" !== D.oScroll.sY) && (i = e("<tfoot/>").appendTo(w)), 0 === i.length || 0 === i.children().length ? w.addClass(P.sNoFooter) : i.length > 0 && (D.nTFoot = i[0], N(D.aoFooter, D.nTFoot)), p.aaData) for (m = 0; m < p.aaData.length; m++) _(D, p.aaData[m]);
                                        else(D.bDeferLoading || "dom" == Oe(D)) && C(D, e(D.nTBody).children("tr"));
                                        D.aiDisplay = D.aiDisplayMaster.slice(),
                                            D.bInitialised = !0,
                                        !1 === v && ae(D)
                                    };
                                p.bStateSave ? (O.bStateSave = !0, Ee(D, "aoDrawCallback", ke, "state_save"), De(D, 0, q)) : q()
                            } else je(null, 0, "Non-table node initialisation (" + this.nodeName + ")", 2)
                        }),
                            n = null,
                            this
                    },
                    We = {},
                    Ke = /[\r\n]/g,
                    Je = /<.*?>/g,
                    Xe = /^\d{2,4}[\.\/\-]\d{1,2}[\.\/\-]\d{1,2}([T ]{1}\d{1,2}[:\.]\d{2}([\.:]\d{2})?)?$/,
                    Ge = new RegExp("(\\" + ["/", ".", "*", "+", "?", "|", "(", ")", "[", "]", "{", "}", "\\", "$", "^", "-"].join("|\\") + ")", "g"),
                    Ze = /[',$£€¥%\u2009\u202F\u20BD\u20a9\u20BArfk]/gi,
                    Ye = function(e) {
                        return ! e || !0 === e || "-" === e
                    },
                    et = function(e) {
                        var t = parseInt(e, 10);
                        return ! isNaN(t) && isFinite(e) ? t: null
                    },
                    tt = function(e, t) {
                        return We[t] || (We[t] = new RegExp(ft(t), "g")),
                            "string" == typeof e && "." !== t ? e.replace(/\./g, "").replace(We[t], ".") : e
                    },
                    nt = function(e, t, n) {
                        var r = "string" == typeof e;
                        return !! Ye(e) || (t && r && (e = tt(e, t)), n && r && (e = e.replace(Ze, "")), !isNaN(parseFloat(e)) && isFinite(e))
                    },
                    rt = function(e, t, n) {
                        if (Ye(e)) return ! 0;
                        return function(e) {
                            return Ye(e) || "string" == typeof e
                        } (e) ? !!nt(lt(e), t, n) || null: null
                    },
                    at = function(e, t, n) {
                        var a = [],
                            o = 0,
                            i = e.length;
                        if (n !== r) for (; o < i; o++) e[o] && e[o][t] && a.push(e[o][t][n]);
                        else for (; o < i; o++) e[o] && a.push(e[o][t]);
                        return a
                    },
                    ot = function(e, t, n, a) {
                        var o = [],
                            i = 0,
                            s = t.length;
                        if (a !== r) for (; i < s; i++) e[t[i]][n] && o.push(e[t[i]][n][a]);
                        else for (; i < s; i++) o.push(e[t[i]][n]);
                        return o
                    },
                    it = function(e, t) {
                        var n, a = [];
                        t === r ? (t = 0, n = e) : (n = t, t = e);
                        for (var o = t; o < n; o++) a.push(o);
                        return a
                    },
                    st = function(e) {
                        for (var t = [], n = 0, r = e.length; n < r; n++) e[n] && t.push(e[n]);
                        return t
                    },
                    lt = function(e) {
                        return e.replace(Je, "")
                    },
                    ct = function(e) {
                        if (function(e) {
                            if (e.length < 2) return ! 0;
                            for (var t = e.slice().sort(), n = t[0], r = 1, a = t.length; r < a; r++) {
                                if (t[r] === n) return ! 1;
                                n = t[r]
                            }
                            return ! 0
                        } (e)) return e.slice();
                        var t, n, r, a = [],
                            o = e.length,
                            i = 0;
                        e: for (n = 0; n < o; n++) {
                            for (t = e[n], r = 0; r < i; r++) if (a[r] === t) continue e;
                            a.push(t),
                                i++
                        }
                        return a
                    };
                Qe.util = {
                    throttle: function(e, t) {
                        var n, a, o = t !== r ? t: 200;
                        return function() {
                            var t = this,
                                i = +new Date,
                                s = arguments;
                            n && i < n + o ? (clearTimeout(a), a = setTimeout(function() {
                                    n = r,
                                        e.apply(t, s)
                                },
                                o)) : (n = i, e.apply(t, s))
                        }
                    },
                    escapeRegex: function(e) {
                        return e.replace(Ge, "\\$1")
                    }
                };
                var ut = function(e, t, n) {
                        e[t] !== r && (e[n] = e[t])
                    },
                    dt = /\[.*?\]$/,
                    ht = /\(\)$/,
                    ft = Qe.util.escapeRegex,
                    pt = e("<div>")[0],
                    mt = pt.textContent !== r,
                    gt = /<.*?>/g,
                    vt = Qe.util.throttle,
                    bt = [],
                    yt = Array.prototype;
                $e = function(t, n) {
                    if (! (this instanceof $e)) return new $e(t, n);
                    var r = [],
                        a = function(t) {
                            var n = function(t) {
                                var n, r, a = Qe.settings,
                                    o = e.map(a,
                                        function(e, t) {
                                            return e.nTable
                                        });
                                return t ? t.nTable && t.oApi ? [t] : t.nodeName && "table" === t.nodeName.toLowerCase() ? -1 !== (n = e.inArray(t, o)) ? [a[n]] : null: t && "function" == typeof t.settings ? t.settings().toArray() : ("string" == typeof t ? r = e(t) : t instanceof e && (r = t), r ? r.map(function(t) {
                                    return - 1 !== (n = e.inArray(this, o)) ? a[n] : null
                                }).toArray() : void 0) : []
                            } (t);
                            n && (r = r.concat(n))
                        };
                    if (e.isArray(t)) for (var o = 0,
                                               i = t.length; o < i; o++) a(t[o]);
                    else a(t);
                    this.context = ct(r),
                    n && e.merge(this, n),
                        this.selector = {
                            rows: null,
                            cols: null,
                            opts: null
                        },
                        $e.extend(this, this, bt)
                },
                    Qe.Api = $e,
                    e.extend($e.prototype, {
                        any: function() {
                            return 0 !== this.count()
                        },
                        concat: yt.concat,
                        context: [],
                        count: function() {
                            return this.flatten().length
                        },
                        each: function(e) {
                            for (var t = 0,
                                     n = this.length; t < n; t++) e.call(this, this[t], t, this);
                            return this
                        },
                        eq: function(e) {
                            var t = this.context;
                            return t.length > e ? new $e(t[e], this[e]) : null
                        },
                        filter: function(e) {
                            var t = [];
                            if (yt.filter) t = yt.filter.call(this, e, this);
                            else for (var n = 0,
                                          r = this.length; n < r; n++) e.call(this, this[n], n, this) && t.push(this[n]);
                            return new $e(this.context, t)
                        },
                        flatten: function() {
                            var e = [];
                            return new $e(this.context, e.concat.apply(e, this.toArray()))
                        },
                        join: yt.join,
                        indexOf: yt.indexOf ||
                        function(e, t) {
                            for (var n = t || 0,
                                     r = this.length; n < r; n++) if (this[n] === e) return n;
                            return - 1
                        },
                        iterator: function(e, t, n, a) {
                            var o, i, s, l, c, u, d, h, f = [],
                                p = this.context,
                                m = this.selector;
                            for ("string" == typeof e && (a = n, n = t, t = e, e = !1), i = 0, s = p.length; i < s; i++) {
                                var g = new $e(p[i]);
                                if ("table" === t)(o = n.call(g, p[i], i)) !== r && f.push(o);
                                else if ("columns" === t || "rows" === t)(o = n.call(g, p[i], this[i], i)) !== r && f.push(o);
                                else if ("column" === t || "column-rows" === t || "row" === t || "cell" === t) for (d = this[i], "column-rows" === t && (u = St(p[i], m.opts)), l = 0, c = d.length; l < c; l++) h = d[l],
                                (o = "cell" === t ? n.call(g, p[i], h.row, h.column, i, l) : n.call(g, p[i], h, i, l, u)) !== r && f.push(o)
                            }
                            if (f.length || a) {
                                var v = new $e(p, e ? f.concat.apply([], f) : f),
                                    b = v.selector;
                                return b.rows = m.rows,
                                    b.cols = m.cols,
                                    b.opts = m.opts,
                                    v
                            }
                            return this
                        },
                        lastIndexOf: yt.lastIndexOf ||
                        function(e, t) {
                            return this.indexOf.apply(this.toArray.reverse(), arguments)
                        },
                        length: 0,
                        map: function(e) {
                            var t = [];
                            if (yt.map) t = yt.map.call(this, e, this);
                            else for (var n = 0,
                                          r = this.length; n < r; n++) t.push(e.call(this, this[n], n));
                            return new $e(this.context, t)
                        },
                        pluck: function(e) {
                            return this.map(function(t) {
                                return t[e]
                            })
                        },
                        pop: yt.pop,
                        push: yt.push,
                        reduce: yt.reduce ||
                        function(e, t) {
                            return u(this, e, t, 0, this.length, 1)
                        },
                        reduceRight: yt.reduceRight ||
                        function(e, t) {
                            return u(this, e, t, this.length - 1, -1, -1)
                        },
                        reverse: yt.reverse,
                        selector: null,
                        shift: yt.shift,
                        slice: function() {
                            return new $e(this.context, this)
                        },
                        sort: yt.sort,
                        splice: yt.splice,
                        toArray: function() {
                            return yt.slice.call(this)
                        },
                        to$: function() {
                            return e(this)
                        },
                        toJQuery: function() {
                            return e(this)
                        },
                        unique: function() {
                            return new $e(this.context, ct(this))
                        },
                        unshift: yt.unshift
                    }),
                    $e.extend = function(t, n, r) {
                        if (r.length && n && (n instanceof $e || n.__dt_wrapper)) {
                            var a, o, i, s = function(e, t, n) {
                                return function() {
                                    var r = t.apply(e, arguments);
                                    return $e.extend(r, r, n.methodExt),
                                        r
                                }
                            };
                            for (a = 0, o = r.length; a < o; a++) n[(i = r[a]).name] = "function" == typeof i.val ? s(t, i.val, i) : e.isPlainObject(i.val) ? {}: i.val,
                                n[i.name].__dt_wrapper = !0,
                                $e.extend(t, n[i.name], i.propExt)
                        }
                    },
                    $e.register = Ue = function(t, n) {
                        if (e.isArray(t)) for (var r = 0,
                                                   a = t.length; r < a; r++) $e.register(t[r], n);
                        else {
                            var o, i, s, l, c = t.split("."),
                                u = bt,
                                d = function(e, t) {
                                    for (var n = 0,
                                             r = e.length; n < r; n++) if (e[n].name === t) return e[n];
                                    return null
                                };
                            for (o = 0, i = c.length; o < i; o++) {
                                var h = d(u, s = (l = -1 !== c[o].indexOf("()")) ? c[o].replace("()", "") : c[o]);
                                h || (h = {
                                    name: s,
                                    val: {},
                                    methodExt: [],
                                    propExt: []
                                },
                                    u.push(h)),
                                    o === i - 1 ? h.val = n: u = l ? h.methodExt: h.propExt
                            }
                        }
                    },
                    $e.registerPlural = Ve = function(t, n, a) {
                        $e.register(t, a),
                            $e.register(n,
                                function() {
                                    var t = a.apply(this, arguments);
                                    return t === this ? this: t instanceof $e ? t.length ? e.isArray(t[0]) ? new $e(t.context, t[0]) : t[0] : r: t
                                })
                    };
                Ue("tables()",
                    function(t) {
                        return t ? new $e(function(t, n) {
                            if ("number" == typeof t) return [n[t]];
                            var r = e.map(n,
                                function(e, t) {
                                    return e.nTable
                                });
                            return e(r).filter(t).map(function(t) {
                                var a = e.inArray(this, r);
                                return n[a]
                            }).toArray()
                        } (t, this.context)) : this
                    }),
                    Ue("table()",
                        function(e) {
                            var t = this.tables(e),
                                n = t.context;
                            return n.length ? new $e(n[0]) : t
                        }),
                    Ve("tables().nodes()", "table().node()",
                        function() {
                            return this.iterator("table",
                                function(e) {
                                    return e.nTable
                                },
                                1)
                        }),
                    Ve("tables().body()", "table().body()",
                        function() {
                            return this.iterator("table",
                                function(e) {
                                    return e.nTBody
                                },
                                1)
                        }),
                    Ve("tables().header()", "table().header()",
                        function() {
                            return this.iterator("table",
                                function(e) {
                                    return e.nTHead
                                },
                                1)
                        }),
                    Ve("tables().footer()", "table().footer()",
                        function() {
                            return this.iterator("table",
                                function(e) {
                                    return e.nTFoot
                                },
                                1)
                        }),
                    Ve("tables().containers()", "table().container()",
                        function() {
                            return this.iterator("table",
                                function(e) {
                                    return e.nTableWrapper
                                },
                                1)
                        }),
                    Ue("draw()",
                        function(e) {
                            return this.iterator("table",
                                function(t) {
                                    "page" === e ? H(t) : ("string" == typeof e && (e = "full-hold" !== e), L(t, !1 === e))
                                })
                        }),
                    Ue("page()",
                        function(e) {
                            return e === r ? this.page.info().page: this.iterator("table",
                                function(t) {
                                    ce(t, e)
                                })
                        }),
                    Ue("page.info()",
                        function(e) {
                            if (0 === this.context.length) return r;
                            var t = this.context[0],
                                n = t._iDisplayStart,
                                a = t.oFeatures.bPaginate ? t._iDisplayLength: -1,
                                o = t.fnRecordsDisplay(),
                                i = -1 === a;
                            return {
                                page: i ? 0 : Math.floor(n / a),
                                pages: i ? 1 : Math.ceil(o / a),
                                start: n,
                                end: t.fnDisplayEnd(),
                                length: a,
                                recordsTotal: t.fnRecordsTotal(),
                                recordsDisplay: o,
                                serverSide: "ssp" === Oe(t)
                            }
                        }),
                    Ue("page.len()",
                        function(e) {
                            return e === r ? 0 !== this.context.length ? this.context[0]._iDisplayLength: r: this.iterator("table",
                                function(t) {
                                    ie(t, e)
                                })
                        });
                var _t = function(e, t, n) {
                    if (n) {
                        var r = new $e(e);
                        r.one("draw",
                            function() {
                                n(r.ajax.json())
                            })
                    }
                    if ("ssp" == Oe(e)) L(e, t);
                    else {
                        de(e, !0);
                        var a = e.jqXHR;
                        a && 4 !== a.readyState && a.abort(),
                            q(e, [],
                                function(n) {
                                    D(e);
                                    for (var r = V(e, n), a = 0, o = r.length; a < o; a++) _(e, r[a]);
                                    L(e, t),
                                        de(e, !1)
                                })
                    }
                };
                Ue("ajax.json()",
                    function() {
                        var e = this.context;
                        if (e.length > 0) return e[0].json
                    }),
                    Ue("ajax.params()",
                        function() {
                            var e = this.context;
                            if (e.length > 0) return e[0].oAjaxData
                        }),
                    Ue("ajax.reload()",
                        function(e, t) {
                            return this.iterator("table",
                                function(n) {
                                    _t(n, !1 === t, e)
                                })
                        }),
                    Ue("ajax.url()",
                        function(t) {
                            var n = this.context;
                            return t === r ? 0 === n.length ? r: (n = n[0]).ajax ? e.isPlainObject(n.ajax) ? n.ajax.url: n.ajax: n.sAjaxSource: this.iterator("table",
                                function(n) {
                                    e.isPlainObject(n.ajax) ? n.ajax.url = t: n.ajax = t
                                })
                        }),
                    Ue("ajax.url().load()",
                        function(e, t) {
                            return this.iterator("table",
                                function(n) {
                                    _t(n, !1 === t, e)
                                })
                        });
                var Ct = function(t, n, a, o, i) {
                        var s, l, c, u, d, h, f = [],
                            p = typeof n;
                        for (n && "string" !== p && "function" !== p && n.length !== r || (n = [n]), c = 0, u = n.length; c < u; c++) for (d = 0, h = (l = n[c] && n[c].split && !n[c].match(/[\[\(:]/) ? n[c].split(",") : [n[c]]).length; d < h; d++)(s = a("string" == typeof l[d] ? e.trim(l[d]) : l[d])) && s.length && (f = f.concat(s));
                        var m = ze.selector[t];
                        if (m.length) for (c = 0, u = m.length; c < u; c++) f = m[c](o, i, f);
                        return ct(f)
                    },
                    wt = function(t) {
                        return t || (t = {}),
                        t.filter && t.search === r && (t.search = t.filter),
                            e.extend({
                                    search: "none",
                                    order: "current",
                                    page: "all"
                                },
                                t)
                    },
                    xt = function(e) {
                        for (var t = 0,
                                 n = e.length; t < n; t++) if (e[t].length > 0) return e[0] = e[t],
                            e[0].length = 1,
                            e.length = 1,
                            e.context = [e.context[t]],
                            e;
                        return e.length = 0,
                            e
                    },
                    St = function(t, n) {
                        var r, a, o, i = [],
                            s = t.aiDisplay,
                            l = t.aiDisplayMaster,
                            c = n.search,
                            u = n.order,
                            d = n.page;
                        if ("ssp" == Oe(t)) return "removed" === c ? [] : it(0, l.length);
                        if ("current" == d) for (r = t._iDisplayStart, a = t.fnDisplayEnd(); r < a; r++) i.push(s[r]);
                        else if ("current" == u || "applied" == u) i = "none" == c ? l.slice() : "applied" == c ? s.slice() : e.map(l,
                            function(t, n) {
                                return - 1 === e.inArray(t, s) ? t: null
                            });
                        else if ("index" == u || "original" == u) for (r = 0, a = t.aoData.length; r < a; r++)"none" == c ? i.push(r) : ( - 1 === (o = e.inArray(r, s)) && "removed" == c || o >= 0 && "applied" == c) && i.push(r);
                        return i
                    };
                Ue("rows()",
                    function(t, n) {
                        t === r ? t = "": e.isPlainObject(t) && (n = t, t = ""),
                            n = wt(n);
                        var a = this.iterator("table",
                            function(a) {
                                return function(t, n, a) {
                                    var o;
                                    return Ct("row", n,
                                        function(n) {
                                            var i = et(n);
                                            if (null !== i && !a) return [i];
                                            if (o || (o = St(t, a)), null !== i && -1 !== e.inArray(i, o)) return [i];
                                            if (null === n || n === r || "" === n) return o;
                                            if ("function" == typeof n) return e.map(o,
                                                function(e) {
                                                    var r = t.aoData[e];
                                                    return n(e, r._aData, r.nTr) ? e: null
                                                });
                                            var s = st(ot(t.aoData, o, "nTr"));
                                            if (n.nodeName) {
                                                if (n._DT_RowIndex !== r) return [n._DT_RowIndex];
                                                if (n._DT_CellIndex) return [n._DT_CellIndex.row];
                                                var l = e(n).closest("*[data-dt-row]");
                                                return l.length ? [l.data("dt-row")] : []
                                            }
                                            if ("string" == typeof n && "#" === n.charAt(0)) {
                                                var c = t.aIds[n.replace(/^#/, "")];
                                                if (c !== r) return [c.idx]
                                            }
                                            return e(s).filter(n).map(function() {
                                                return this._DT_RowIndex
                                            }).toArray()
                                        },
                                        t, a)
                                } (a, t, n)
                            },
                            1);
                        return a.selector.rows = t,
                            a.selector.opts = n,
                            a
                    }),
                    Ue("rows().nodes()",
                        function() {
                            return this.iterator("row",
                                function(e, t) {
                                    return e.aoData[t].nTr || r
                                },
                                1)
                        }),
                    Ue("rows().data()",
                        function() {
                            return this.iterator(!0, "rows",
                                function(e, t) {
                                    return ot(e.aoData, t, "_aData")
                                },
                                1)
                        }),
                    Ve("rows().cache()", "row().cache()",
                        function(e) {
                            return this.iterator("row",
                                function(t, n) {
                                    var r = t.aoData[n];
                                    return "search" === e ? r._aFilterData: r._aSortData
                                },
                                1)
                        }),
                    Ve("rows().invalidate()", "row().invalidate()",
                        function(e) {
                            return this.iterator("row",
                                function(t, n) {
                                    j(t, n, e)
                                })
                        }),
                    Ve("rows().indexes()", "row().index()",
                        function() {
                            return this.iterator("row",
                                function(e, t) {
                                    return t
                                },
                                1)
                        }),
                    Ve("rows().ids()", "row().id()",
                        function(e) {
                            for (var t = [], n = this.context, r = 0, a = n.length; r < a; r++) for (var o = 0,
                                                                                                         i = this[r].length; o < i; o++) {
                                var s = n[r].rowIdFn(n[r].aoData[this[r][o]]._aData);
                                t.push((!0 === e ? "#": "") + s)
                            }
                            return new $e(n, t)
                        }),
                    Ve("rows().remove()", "row().remove()",
                        function() {
                            var e = this;
                            return this.iterator("row",
                                function(t, n, a) {
                                    var o, i, s, l, c, u, d = t.aoData,
                                        h = d[n];
                                    for (d.splice(n, 1), o = 0, i = d.length; o < i; o++) if (c = d[o], u = c.anCells, null !== c.nTr && (c.nTr._DT_RowIndex = o), null !== u) for (s = 0, l = u.length; s < l; s++) u[s]._DT_CellIndex.row = o;
                                    P(t.aiDisplayMaster, n),
                                        P(t.aiDisplay, n),
                                        P(e[a], n, !1),
                                    t._iRecordsDisplay > 0 && t._iRecordsDisplay--,
                                        He(t);
                                    var f = t.rowIdFn(h._aData);
                                    f !== r && delete t.aIds[f]
                                }),
                                this.iterator("table",
                                    function(e) {
                                        for (var t = 0,
                                                 n = e.aoData.length; t < n; t++) e.aoData[t].idx = t
                                    }),
                                this
                        }),
                    Ue("rows.add()",
                        function(t) {
                            var n = this.iterator("table",
                                function(e) {
                                    var n, r, a, o = [];
                                    for (r = 0, a = t.length; r < a; r++)(n = t[r]).nodeName && "TR" === n.nodeName.toUpperCase() ? o.push(C(e, n)[0]) : o.push(_(e, n));
                                    return o
                                },
                                1),
                                r = this.rows( - 1);
                            return r.pop(),
                                e.merge(r, n),
                                r
                        }),
                    Ue("row()",
                        function(e, t) {
                            return xt(this.rows(e, t))
                        }),
                    Ue("row().data()",
                        function(e) {
                            var t = this.context;
                            return e === r ? t.length && this.length ? t[0].aoData[this[0]]._aData: r: (t[0].aoData[this[0]]._aData = e, j(t[0], this[0], "data"), this)
                        }),
                    Ue("row().node()",
                        function() {
                            var e = this.context;
                            return e.length && this.length ? e[0].aoData[this[0]].nTr || null: null
                        }),
                    Ue("row.add()",
                        function(t) {
                            t instanceof e && t.length && (t = t[0]);
                            var n = this.iterator("table",
                                function(e) {
                                    return t.nodeName && "TR" === t.nodeName.toUpperCase() ? C(e, t)[0] : _(e, t)
                                });
                            return this.row(n[0])
                        });
                var Tt = function(e, t) {
                        var n = e.context;
                        if (n.length) {
                            var a = n[0].aoData[t !== r ? t: e[0]];
                            a && a._details && (a._details.remove(), a._detailsShow = r, a._details = r)
                        }
                    },
                    At = function(e, t) {
                        var n = e.context;
                        if (n.length && e.length) {
                            var r = n[0].aoData[e[0]];
                            r._details && (r._detailsShow = t, t ? r._details.insertAfter(r.nTr) : r._details.detach(), kt(n[0]))
                        }
                    },
                    kt = function(e) {
                        var t = new $e(e),
                            n = ".dt.DT_details",
                            r = "column-visibility" + n,
                            a = "destroy" + n,
                            o = e.aoData;
                        t.off("draw.dt.DT_details " + r + " " + a),
                        at(o, "_details").length > 0 && (t.on("draw.dt.DT_details",
                            function(n, r) {
                                e === r && t.rows({
                                    page: "current"
                                }).eq(0).each(function(e) {
                                    var t = o[e];
                                    t._detailsShow && t._details.insertAfter(t.nTr)
                                })
                            }), t.on(r,
                            function(t, n, r, a) {
                                if (e === n) for (var i, s = g(n), l = 0, c = o.length; l < c; l++)(i = o[l])._details && i._details.children("td[colspan]").attr("colspan", s)
                            }), t.on(a,
                            function(n, r) {
                                if (e === r) for (var a = 0,
                                                      i = o.length; a < i; a++) o[a]._details && Tt(t, a)
                            }))
                    },
                    Dt = "row().child",
                    Pt = Dt + "()";
                Ue(Pt,
                    function(t, n) {
                        var a = this.context;
                        return t === r ? a.length && this.length ? a[0].aoData[this[0]]._details: r: (!0 === t ? this.child.show() : !1 === t ? Tt(this) : a.length && this.length &&
                            function(t, n, r, a) {
                                var o = [],
                                    i = function(n, r) {
                                        if (e.isArray(n) || n instanceof e) for (var a = 0,
                                                                                     s = n.length; a < s; a++) i(n[a], r);
                                        else if (n.nodeName && "tr" === n.nodeName.toLowerCase()) o.push(n);
                                        else {
                                            var l = e("<tr><td/></tr>").addClass(r);
                                            e("td", l).addClass(r).html(n)[0].colSpan = g(t),
                                                o.push(l[0])
                                        }
                                    };
                                i(r, a),
                                n._details && n._details.detach(),
                                    n._details = e(o),
                                n._detailsShow && n._details.insertAfter(n.nTr)
                            } (a[0], a[0].aoData[this[0]], t, n), this)
                    }),
                    Ue([Dt + ".show()", Pt + ".show()"],
                        function(e) {
                            return At(this, !0),
                                this
                        }),
                    Ue([Dt + ".hide()", Pt + ".hide()"],
                        function() {
                            return At(this, !1),
                                this
                        }),
                    Ue([Dt + ".remove()", Pt + ".remove()"],
                        function() {
                            return Tt(this),
                                this
                        }),
                    Ue(Dt + ".isShown()",
                        function() {
                            var e = this.context;
                            return ! (!e.length || !this.length) && (e[0].aoData[this[0]]._detailsShow || !1)
                        });
                var jt = /^([^:]+):(name|visIdx|visible)$/,
                    Ft = function(e, t, n, r, a) {
                        for (var o = [], i = 0, s = a.length; i < s; i++) o.push(w(e, a[i], t));
                        return o
                    };
                Ue("columns()",
                    function(t, n) {
                        t === r ? t = "": e.isPlainObject(t) && (n = t, t = ""),
                            n = wt(n);
                        var a = this.iterator("table",
                            function(r) {
                                return function(t, n, r) {
                                    var a = t.aoColumns,
                                        o = at(a, "sName"),
                                        i = at(a, "nTh");
                                    return Ct("column", n,
                                        function(n) {
                                            var s = et(n);
                                            if ("" === n) return it(a.length);
                                            if (null !== s) return [s >= 0 ? s: a.length + s];
                                            if ("function" == typeof n) {
                                                var l = St(t, r);
                                                return e.map(a,
                                                    function(e, r) {
                                                        return n(r, Ft(t, r, 0, 0, l), i[r]) ? r: null
                                                    })
                                            }
                                            var c = "string" == typeof n ? n.match(jt) : "";
                                            if (c) switch (c[2]) {
                                                case "visIdx":
                                                case "visible":
                                                    var u = parseInt(c[1], 10);
                                                    if (u < 0) {
                                                        var d = e.map(a,
                                                            function(e, t) {
                                                                return e.bVisible ? t: null
                                                            });
                                                        return [d[d.length + u]]
                                                    }
                                                    return [p(t, u)];
                                                case "name":
                                                    return e.map(o,
                                                        function(e, t) {
                                                            return e === c[1] ? t: null
                                                        });
                                                default:
                                                    return []
                                            }
                                            if (n.nodeName && n._DT_CellIndex) return [n._DT_CellIndex.column];
                                            var h = e(i).filter(n).map(function() {
                                                return e.inArray(this, i)
                                            }).toArray();
                                            if (h.length || !n.nodeName) return h;
                                            var f = e(n).closest("*[data-dt-column]");
                                            return f.length ? [f.data("dt-column")] : []
                                        },
                                        t, r)
                                } (r, t, n)
                            },
                            1);
                        return a.selector.cols = t,
                            a.selector.opts = n,
                            a
                    }),
                    Ve("columns().header()", "column().header()",
                        function(e, t) {
                            return this.iterator("column",
                                function(e, t) {
                                    return e.aoColumns[t].nTh
                                },
                                1)
                        }),
                    Ve("columns().footer()", "column().footer()",
                        function(e, t) {
                            return this.iterator("column",
                                function(e, t) {
                                    return e.aoColumns[t].nTf
                                },
                                1)
                        }),
                    Ve("columns().data()", "column().data()",
                        function() {
                            return this.iterator("column-rows", Ft, 1)
                        }),
                    Ve("columns().dataSrc()", "column().dataSrc()",
                        function() {
                            return this.iterator("column",
                                function(e, t) {
                                    return e.aoColumns[t].mData
                                },
                                1)
                        }),
                    Ve("columns().cache()", "column().cache()",
                        function(e) {
                            return this.iterator("column-rows",
                                function(t, n, r, a, o) {
                                    return ot(t.aoData, o, "search" === e ? "_aFilterData": "_aSortData", n)
                                },
                                1)
                        }),
                    Ve("columns().nodes()", "column().nodes()",
                        function() {
                            return this.iterator("column-rows",
                                function(e, t, n, r, a) {
                                    return ot(e.aoData, a, "anCells", t)
                                },
                                1)
                        }),
                    Ve("columns().visible()", "column().visible()",
                        function(t, n) {
                            var a = this.iterator("column",
                                function(n, a) {
                                    if (t === r) return n.aoColumns[a].bVisible; !
                                        function(t, n, a) {
                                            var o, i, s, l, c = t.aoColumns,
                                                u = c[n],
                                                d = t.aoData;
                                            if (a === r) return u.bVisible;
                                            if (u.bVisible !== a) {
                                                if (a) {
                                                    var h = e.inArray(!0, at(c, "bVisible"), n + 1);
                                                    for (i = 0, s = d.length; i < s; i++) l = d[i].nTr,
                                                        o = d[i].anCells,
                                                    l && l.insertBefore(o[n], o[h] || null)
                                                } else e(at(t.aoData, "anCells", n)).detach();
                                                u.bVisible = a,
                                                    M(t, t.aoHeader),
                                                    M(t, t.aoFooter),
                                                    ke(t)
                                            }
                                        } (n, a, t)
                                });
                            return t !== r && (this.iterator("column",
                                function(e, r) {
                                    Me(e, null, "column-visibility", [e, r, t, n])
                                }), (n === r || n) && this.columns.adjust()),
                                a
                        }),
                    Ve("columns().indexes()", "column().index()",
                        function(e) {
                            return this.iterator("column",
                                function(t, n) {
                                    return "visible" === e ? m(t, n) : n
                                },
                                1)
                        }),
                    Ue("columns.adjust()",
                        function() {
                            return this.iterator("table",
                                function(e) {
                                    f(e)
                                },
                                1)
                        }),
                    Ue("column.index()",
                        function(e, t) {
                            if (0 !== this.context.length) {
                                var n = this.context[0];
                                if ("fromVisible" === e || "toData" === e) return p(n, t);
                                if ("fromData" === e || "toVisible" === e) return m(n, t)
                            }
                        }),
                    Ue("column()",
                        function(e, t) {
                            return xt(this.columns(e, t))
                        });
                Ue("cells()",
                    function(t, n, a) {
                        if (e.isPlainObject(t) && (t.row === r ? (a = t, t = null) : (a = n, n = null)), e.isPlainObject(n) && (a = n, n = null), null === n || n === r) return this.iterator("table",
                            function(n) {
                                return function(t, n, a) {
                                    var o, i, s, l, c, u, d, h = t.aoData,
                                        f = St(t, a),
                                        p = st(ot(h, f, "anCells")),
                                        m = e([].concat.apply([], p)),
                                        g = t.aoColumns.length;
                                    return Ct("cell", n,
                                        function(n) {
                                            var a = "function" == typeof n;
                                            if (null === n || n === r || a) {
                                                for (i = [], s = 0, l = f.length; s < l; s++) for (o = f[s], c = 0; c < g; c++) u = {
                                                    row: o,
                                                    column: c
                                                },
                                                    a ? (d = h[o], n(u, w(t, o, c), d.anCells ? d.anCells[c] : null) && i.push(u)) : i.push(u);
                                                return i
                                            }
                                            if (e.isPlainObject(n)) return [n];
                                            var p = m.filter(n).map(function(e, t) {
                                                return {
                                                    row: t._DT_CellIndex.row,
                                                    column: t._DT_CellIndex.column
                                                }
                                            }).toArray();
                                            return p.length || !n.nodeName ? p: (d = e(n).closest("*[data-dt-row]")).length ? [{
                                                row: d.data("dt-row"),
                                                column: d.data("dt-column")
                                            }] : []
                                        },
                                        t, a)
                                } (n, t, wt(a))
                            });
                        var o, i, s, l, c, u = this.columns(n, a),
                            d = this.rows(t, a),
                            h = this.iterator("table",
                                function(e, t) {
                                    for (o = [], i = 0, s = d[t].length; i < s; i++) for (l = 0, c = u[t].length; l < c; l++) o.push({
                                        row: d[t][i],
                                        column: u[t][l]
                                    });
                                    return o
                                },
                                1);
                        return e.extend(h.selector, {
                            cols: n,
                            rows: t,
                            opts: a
                        }),
                            h
                    }),
                    Ve("cells().nodes()", "cell().node()",
                        function() {
                            return this.iterator("cell",
                                function(e, t, n) {
                                    var a = e.aoData[t];
                                    return a && a.anCells ? a.anCells[n] : r
                                },
                                1)
                        }),
                    Ue("cells().data()",
                        function() {
                            return this.iterator("cell",
                                function(e, t, n) {
                                    return w(e, t, n)
                                },
                                1)
                        }),
                    Ve("cells().cache()", "cell().cache()",
                        function(e) {
                            return e = "search" === e ? "_aFilterData": "_aSortData",
                                this.iterator("cell",
                                    function(t, n, r) {
                                        return t.aoData[n][e][r]
                                    },
                                    1)
                        }),
                    Ve("cells().render()", "cell().render()",
                        function(e) {
                            return this.iterator("cell",
                                function(t, n, r) {
                                    return w(t, n, r, e)
                                },
                                1)
                        }),
                    Ve("cells().indexes()", "cell().index()",
                        function() {
                            return this.iterator("cell",
                                function(e, t, n) {
                                    return {
                                        row: t,
                                        column: n,
                                        columnVisible: m(e, n)
                                    }
                                },
                                1)
                        }),
                    Ve("cells().invalidate()", "cell().invalidate()",
                        function(e) {
                            return this.iterator("cell",
                                function(t, n, r) {
                                    j(t, n, e, r)
                                })
                        }),
                    Ue("cell()",
                        function(e, t, n) {
                            return xt(this.cells(e, t, n))
                        }),
                    Ue("cell().data()",
                        function(e) {
                            var t = this.context,
                                n = this[0];
                            return e === r ? t.length && n.length ? w(t[0], n[0].row, n[0].column) : r: (x(t[0], n[0].row, n[0].column, e), j(t[0], n[0].row, "data", n[0].column), this)
                        }),
                    Ue("order()",
                        function(t, n) {
                            var a = this.context;
                            return t === r ? 0 !== a.length ? a[0].aaSorting: r: ("number" == typeof t ? t = [[t, n]] : t.length && !e.isArray(t[0]) && (t = Array.prototype.slice.call(arguments)), this.iterator("table",
                                function(e) {
                                    e.aaSorting = t.slice()
                                }))
                        }),
                    Ue("order.listener()",
                        function(e, t, n) {
                            return this.iterator("table",
                                function(r) {
                                    Se(r, e, t, n)
                                })
                        }),
                    Ue("order.fixed()",
                        function(t) {
                            if (!t) {
                                var n = this.context,
                                    a = n.length ? n[0].aaSortingFixed: r;
                                return e.isArray(a) ? {
                                    pre: a
                                }: a
                            }
                            return this.iterator("table",
                                function(n) {
                                    n.aaSortingFixed = e.extend(!0, {},
                                        t)
                                })
                        }),
                    Ue(["columns().order()", "column().order()"],
                        function(t) {
                            var n = this;
                            return this.iterator("table",
                                function(r, a) {
                                    var o = [];
                                    e.each(n[a],
                                        function(e, n) {
                                            o.push([n, t])
                                        }),
                                        r.aaSorting = o
                                })
                        }),
                    Ue("search()",
                        function(t, n, a, o) {
                            var i = this.context;
                            return t === r ? 0 !== i.length ? i[0].oPreviousSearch.sSearch: r: this.iterator("table",
                                function(r) {
                                    r.oFeatures.bFilter && W(r, e.extend({},
                                        r.oPreviousSearch, {
                                            sSearch: t + "",
                                            bRegex: null !== n && n,
                                            bSmart: null === a || a,
                                            bCaseInsensitive: null === o || o
                                        }), 1)
                                })
                        }),
                    Ve("columns().search()", "column().search()",
                        function(t, n, a, o) {
                            return this.iterator("column",
                                function(i, s) {
                                    var l = i.aoPreSearchCols;
                                    if (t === r) return l[s].sSearch;
                                    i.oFeatures.bFilter && (e.extend(l[s], {
                                        sSearch: t + "",
                                        bRegex: null !== n && n,
                                        bSmart: null === a || a,
                                        bCaseInsensitive: null === o || o
                                    }), W(i, i.oPreviousSearch, 1))
                                })
                        }),
                    Ue("state()",
                        function() {
                            return this.context.length ? this.context[0].oSavedState: null
                        }),
                    Ue("state.clear()",
                        function() {
                            return this.iterator("table",
                                function(e) {
                                    e.fnStateSaveCallback.call(e.oInstance, e, {})
                                })
                        }),
                    Ue("state.loaded()",
                        function() {
                            return this.context.length ? this.context[0].oLoadedState: null
                        }),
                    Ue("state.save()",
                        function() {
                            return this.iterator("table",
                                function(e) {
                                    ke(e)
                                })
                        }),
                    Qe.versionCheck = Qe.fnVersionCheck = function(e) {
                        for (var t, n, r = Qe.version.split("."), a = e.split("."), o = 0, i = a.length; o < i; o++) if (t = parseInt(r[o], 10) || 0, n = parseInt(a[o], 10) || 0, t !== n) return t > n;
                        return ! 0
                    },
                    Qe.isDataTable = Qe.fnIsDataTable = function(t) {
                        var n = e(t).get(0),
                            r = !1;
                        return t instanceof Qe.Api || (e.each(Qe.settings,
                            function(t, a) {
                                var o = a.nScrollHead ? e("table", a.nScrollHead)[0] : null,
                                    i = a.nScrollFoot ? e("table", a.nScrollFoot)[0] : null;
                                a.nTable !== n && o !== n && i !== n || (r = !0)
                            }), r)
                    },
                    Qe.tables = Qe.fnTables = function(t) {
                        var n = !1;
                        e.isPlainObject(t) && (n = t.api, t = t.visible);
                        var r = e.map(Qe.settings,
                            function(n) {
                                if (!t || t && e(n.nTable).is(":visible")) return n.nTable
                            });
                        return n ? new $e(r) : r
                    },
                    Qe.camelToHungarian = o,
                    Ue("$()",
                        function(t, n) {
                            var r = this.rows(n).nodes(),
                                a = e(r);
                            return e([].concat(a.filter(t).toArray(), a.find(t).toArray()))
                        }),
                    e.each(["on", "one", "off"],
                        function(t, n) {
                            Ue(n + "()",
                                function() {
                                    var t = Array.prototype.slice.call(arguments);
                                    t[0] = e.map(t[0].split(/\s/),
                                        function(e) {
                                            return e.match(/\.dt\b/) ? e: e + ".dt"
                                        }).join(" ");
                                    var r = e(this.tables().nodes());
                                    return r[n].apply(r, t),
                                        this
                                })
                        }),
                    Ue("clear()",
                        function() {
                            return this.iterator("table",
                                function(e) {
                                    D(e)
                                })
                        }),
                    Ue("settings()",
                        function() {
                            return new $e(this.context, this.context)
                        }),
                    Ue("init()",
                        function() {
                            var e = this.context;
                            return e.length ? e[0].oInit: null
                        }),
                    Ue("data()",
                        function() {
                            return this.iterator("table",
                                function(e) {
                                    return at(e.aoData, "_aData")
                                }).flatten()
                        }),
                    Ue("destroy()",
                        function(n) {
                            return n = n || !1,
                                this.iterator("table",
                                    function(r) {
                                        var a, o = r.nTableWrapper.parentNode,
                                            i = r.oClasses,
                                            s = r.nTable,
                                            l = r.nTBody,
                                            c = r.nTHead,
                                            u = r.nTFoot,
                                            d = e(s),
                                            h = e(l),
                                            f = e(r.nTableWrapper),
                                            p = e.map(r.aoData,
                                                function(e) {
                                                    return e.nTr
                                                });
                                        r.bDestroying = !0,
                                            Me(r, "aoDestroyCallback", "destroy", [r]),
                                        n || new $e(r).columns().visible(!0),
                                            f.off(".DT").find(":not(tbody *)").off(".DT"),
                                            e(t).off(".DT-" + r.sInstance),
                                        s != c.parentNode && (d.children("thead").detach(), d.append(c)),
                                        u && s != u.parentNode && (d.children("tfoot").detach(), d.append(u)),
                                            r.aaSorting = [],
                                            r.aaSortingFixed = [],
                                            Te(r),
                                            e(p).removeClass(r.asStripeClasses.join(" ")),
                                            e("th, td", c).removeClass(i.sSortable + " " + i.sSortableAsc + " " + i.sSortableDesc + " " + i.sSortableNone),
                                            h.children().detach(),
                                            h.append(p);
                                        var m = n ? "remove": "detach";
                                        d[m](),
                                            f[m](),
                                        !n && o && (o.insertBefore(s, r.nTableReinsertBefore), d.css("width", r.sDestroyWidth).removeClass(i.sTable), (a = r.asDestroyStripes.length) && h.children().each(function(t) {
                                            e(this).addClass(r.asDestroyStripes[t % a])
                                        }));
                                        var g = e.inArray(r, Qe.settings); - 1 !== g && Qe.settings.splice(g, 1)
                                    })
                        }),
                    e.each(["column", "row", "cell"],
                        function(e, t) {
                            Ue(t + "s().every()",
                                function(e) {
                                    var n = this.selector.opts,
                                        a = this;
                                    return this.iterator(t,
                                        function(o, i, s, l, c) {
                                            e.call(a[t](i, "cell" === t ? s: n, "cell" === t ? n: r), i, s, l, c)
                                        })
                                })
                        }),
                    Ue("i18n()",
                        function(t, n, a) {
                            var o = this.context[0],
                                i = T(t)(o.oLanguage);
                            return i === r && (i = n),
                            a !== r && e.isPlainObject(i) && (i = i[a] !== r ? i[a] : i._),
                                i.replace("%d", a)
                        }),
                    Qe.version = "1.10.16",
                    Qe.settings = [],
                    Qe.models = {},
                    Qe.models.oSearch = {
                        bCaseInsensitive: !0,
                        sSearch: "",
                        bRegex: !1,
                        bSmart: !0
                    },
                    Qe.models.oRow = {
                        nTr: null,
                        anCells: null,
                        _aData: [],
                        _aSortData: null,
                        _aFilterData: null,
                        _sFilterRow: null,
                        _sRowStripe: "",
                        src: null,
                        idx: -1
                    },
                    Qe.models.oColumn = {
                        idx: null,
                        aDataSort: null,
                        asSorting: null,
                        bSearchable: null,
                        bSortable: null,
                        bVisible: null,
                        _sManualType: null,
                        _bAttrSrc: !1,
                        fnCreatedCell: null,
                        fnGetData: null,
                        fnSetData: null,
                        mData: null,
                        mRender: null,
                        nTh: null,
                        nTf: null,
                        sClass: null,
                        sContentPadding: null,
                        sDefaultContent: null,
                        sName: null,
                        sSortDataType: "std",
                        sSortingClass: null,
                        sSortingClassJUI: null,
                        sTitle: null,
                        sType: null,
                        sWidth: null,
                        sWidthOrig: null
                    },
                    Qe.defaults = {
                        aaData: null,
                        aaSorting: [[0, "asc"]],
                        aaSortingFixed: [],
                        ajax: null,
                        aLengthMenu: [10, 25, 50, 100],
                        aoColumns: null,
                        aoColumnDefs: null,
                        aoSearchCols: [],
                        asStripeClasses: null,
                        bAutoWidth: !0,
                        bDeferRender: !1,
                        bDestroy: !1,
                        bFilter: !0,
                        bInfo: !0,
                        bLengthChange: !0,
                        bPaginate: !0,
                        bProcessing: !1,
                        bRetrieve: !1,
                        bScrollCollapse: !1,
                        bServerSide: !1,
                        bSort: !0,
                        bSortMulti: !0,
                        bSortCellsTop: !1,
                        bSortClasses: !0,
                        bStateSave: !1,
                        fnCreatedRow: null,
                        fnDrawCallback: null,
                        fnFooterCallback: null,
                        fnFormatNumber: function(e) {
                            return e.toString().replace(/\B(?=(\d{3})+(?!\d))/g, this.oLanguage.sThousands)
                        },
                        fnHeaderCallback: null,
                        fnInfoCallback: null,
                        fnInitComplete: null,
                        fnPreDrawCallback: null,
                        fnRowCallback: null,
                        fnServerData: null,
                        fnServerParams: null,
                        fnStateLoadCallback: function(e) {
                            try {
                                return JSON.parse(( - 1 === e.iStateDuration ? sessionStorage: localStorage).getItem("DataTables_" + e.sInstance + "_" + location.pathname))
                            } catch(t) {}
                        },
                        fnStateLoadParams: null,
                        fnStateLoaded: null,
                        fnStateSaveCallback: function(e, t) {
                            try { ( - 1 === e.iStateDuration ? sessionStorage: localStorage).setItem("DataTables_" + e.sInstance + "_" + location.pathname, JSON.stringify(t))
                            } catch(n) {}
                        },
                        fnStateSaveParams: null,
                        iStateDuration: 7200,
                        iDeferLoading: null,
                        iDisplayLength: 10,
                        iDisplayStart: 0,
                        iTabIndex: 0,
                        oClasses: {},
                        oLanguage: {
                            oAria: {
                                sSortAscending: ": activate to sort column ascending",
                                sSortDescending: ": activate to sort column descending"
                            },
                            oPaginate: {
                                sFirst: "First",
                                sLast: "Last",
                                sNext: "Next",
                                sPrevious: "Previous"
                            },
                            sEmptyTable: "No data available in table",
                            sInfo: "Showing _START_ to _END_ of _TOTAL_ entries",
                            sInfoEmpty: "Showing 0 to 0 of 0 entries",
                            sInfoFiltered: "(filtered from _MAX_ total entries)",
                            sInfoPostFix: "",
                            sDecimal: "",
                            sThousands: ",",
                            sLengthMenu: "Show _MENU_ entries",
                            sLoadingRecords: "Loading...",
                            sProcessing: "Processing...",
                            sSearch: "Search:",
                            sSearchPlaceholder: "",
                            sUrl: "",
                            sZeroRecords: "No matching records found"
                        },
                        oSearch: e.extend({},
                            Qe.models.oSearch),
                        sAjaxDataProp: "data",
                        sAjaxSource: null,
                        sDom: "lfrtip",
                        searchDelay: null,
                        sPaginationType: "simple_numbers",
                        sScrollX: "",
                        sScrollXInner: "",
                        sScrollY: "",
                        sServerMethod: "GET",
                        renderer: null,
                        rowId: "DT_RowId"
                    },
                    a(Qe.defaults),
                    Qe.defaults.column = {
                        aDataSort: null,
                        iDataSort: -1,
                        asSorting: ["asc", "desc"],
                        bSearchable: !0,
                        bSortable: !0,
                        bVisible: !0,
                        fnCreatedCell: null,
                        mData: null,
                        mRender: null,
                        sCellType: "td",
                        sClass: "",
                        sContentPadding: "",
                        sDefaultContent: null,
                        sName: "",
                        sSortDataType: "std",
                        sTitle: null,
                        sType: null,
                        sWidth: null
                    },
                    a(Qe.defaults.column),
                    Qe.models.oSettings = {
                        oFeatures: {
                            bAutoWidth: null,
                            bDeferRender: null,
                            bFilter: null,
                            bInfo: null,
                            bLengthChange: null,
                            bPaginate: null,
                            bProcessing: null,
                            bServerSide: null,
                            bSort: null,
                            bSortMulti: null,
                            bSortClasses: null,
                            bStateSave: null
                        },
                        oScroll: {
                            bCollapse: null,
                            iBarWidth: 0,
                            sX: null,
                            sXInner: null,
                            sY: null
                        },
                        oLanguage: {
                            fnInfoCallback: null
                        },
                        oBrowser: {
                            bScrollOversize: !1,
                            bScrollbarLeft: !1,
                            bBounding: !1,
                            barWidth: 0
                        },
                        ajax: null,
                        aanFeatures: [],
                        aoData: [],
                        aiDisplay: [],
                        aiDisplayMaster: [],
                        aIds: {},
                        aoColumns: [],
                        aoHeader: [],
                        aoFooter: [],
                        oPreviousSearch: {},
                        aoPreSearchCols: [],
                        aaSorting: null,
                        aaSortingFixed: [],
                        asStripeClasses: null,
                        asDestroyStripes: [],
                        sDestroyWidth: 0,
                        aoRowCallback: [],
                        aoHeaderCallback: [],
                        aoFooterCallback: [],
                        aoDrawCallback: [],
                        aoRowCreatedCallback: [],
                        aoPreDrawCallback: [],
                        aoInitComplete: [],
                        aoStateSaveParams: [],
                        aoStateLoadParams: [],
                        aoStateLoaded: [],
                        sTableId: "",
                        nTable: null,
                        nTHead: null,
                        nTFoot: null,
                        nTBody: null,
                        nTableWrapper: null,
                        bDeferLoading: !1,
                        bInitialised: !1,
                        aoOpenRows: [],
                        sDom: null,
                        searchDelay: null,
                        sPaginationType: "two_button",
                        iStateDuration: 0,
                        aoStateSave: [],
                        aoStateLoad: [],
                        oSavedState: null,
                        oLoadedState: null,
                        sAjaxSource: null,
                        sAjaxDataProp: null,
                        bAjaxDataGet: !0,
                        jqXHR: null,
                        json: r,
                        oAjaxData: r,
                        fnServerData: null,
                        aoServerParams: [],
                        sServerMethod: null,
                        fnFormatNumber: null,
                        aLengthMenu: null,
                        iDraw: 0,
                        bDrawing: !1,
                        iDrawError: -1,
                        _iDisplayLength: 10,
                        _iDisplayStart: 0,
                        _iRecordsTotal: 0,
                        _iRecordsDisplay: 0,
                        oClasses: {},
                        bFiltered: !1,
                        bSorted: !1,
                        bSortCellsTop: null,
                        oInit: null,
                        aoDestroyCallback: [],
                        fnRecordsTotal: function() {
                            return "ssp" == Oe(this) ? 1 * this._iRecordsTotal: this.aiDisplayMaster.length
                        },
                        fnRecordsDisplay: function() {
                            return "ssp" == Oe(this) ? 1 * this._iRecordsDisplay: this.aiDisplay.length
                        },
                        fnDisplayEnd: function() {
                            var e = this._iDisplayLength,
                                t = this._iDisplayStart,
                                n = t + e,
                                r = this.aiDisplay.length,
                                a = this.oFeatures,
                                o = a.bPaginate;
                            return a.bServerSide ? !1 === o || -1 === e ? t + r: Math.min(t + e, this._iRecordsDisplay) : !o || n > r || -1 === e ? r: n
                        },
                        oInstance: null,
                        sInstance: null,
                        iTabIndex: 0,
                        nScrollHead: null,
                        nScrollFoot: null,
                        aLastSort: [],
                        oPlugins: {},
                        rowIdFn: null,
                        rowId: null
                    },
                    Qe.ext = ze = {
                        buttons: {},
                        classes: {},
                        builder: "-source-",
                        errMode: "alert",
                        feature: [],
                        search: [],
                        selector: {
                            cell: [],
                            column: [],
                            row: []
                        },
                        internal: {},
                        legacy: {
                            ajax: null
                        },
                        pager: {},
                        renderer: {
                            pageButton: {},
                            header: {}
                        },
                        order: {},
                        type: {
                            detect: [],
                            search: {},
                            order: {}
                        },
                        _unique: 0,
                        fnVersionCheck: Qe.fnVersionCheck,
                        iApiIndex: 0,
                        oJUIClasses: {},
                        sVersion: Qe.version
                    },
                    e.extend(ze, {
                        afnFiltering: ze.search,
                        aTypes: ze.type.detect,
                        ofnSearch: ze.type.search,
                        oSort: ze.type.order,
                        afnSortData: ze.order,
                        aoFeatures: ze.feature,
                        oApi: ze.internal,
                        oStdClasses: ze.classes,
                        oPagination: ze.pager
                    }),
                    e.extend(Qe.ext.classes, {
                        sTable: "dataTable",
                        sNoFooter: "no-footer",
                        sPageButton: "paginate_button",
                        sPageButtonActive: "current",
                        sPageButtonDisabled: "disabled",
                        sStripeOdd: "odd",
                        sStripeEven: "even",
                        sRowEmpty: "dataTables_empty",
                        sWrapper: "dataTables_wrapper",
                        sFilter: "dataTables_filter",
                        sInfo: "dataTables_info",
                        sPaging: "dataTables_paginate paging_",
                        sLength: "dataTables_length",
                        sProcessing: "dataTables_processing",
                        sSortAsc: "sorting_asc",
                        sSortDesc: "sorting_desc",
                        sSortable: "sorting",
                        sSortableAsc: "sorting_asc_disabled",
                        sSortableDesc: "sorting_desc_disabled",
                        sSortableNone: "sorting_disabled",
                        sSortColumn: "sorting_",
                        sFilterInput: "",
                        sLengthSelect: "",
                        sScrollWrapper: "dataTables_scroll",
                        sScrollHead: "dataTables_scrollHead",
                        sScrollHeadInner: "dataTables_scrollHeadInner",
                        sScrollBody: "dataTables_scrollBody",
                        sScrollFoot: "dataTables_scrollFoot",
                        sScrollFootInner: "dataTables_scrollFootInner",
                        sHeaderTH: "",
                        sFooterTH: "",
                        sSortJUIAsc: "",
                        sSortJUIDesc: "",
                        sSortJUI: "",
                        sSortJUIAscAllowed: "",
                        sSortJUIDescAllowed: "",
                        sSortJUIWrapper: "",
                        sSortIcon: "",
                        sJUIHeader: "",
                        sJUIFooter: ""
                    });
                var It = Qe.ext.pager;
                e.extend(It, {
                    simple: function(e, t) {
                        return ["previous", "next"]
                    },
                    full: function(e, t) {
                        return ["first", "previous", "next", "last"]
                    },
                    numbers: function(e, t) {
                        return [Ne(e, t)]
                    },
                    simple_numbers: function(e, t) {
                        return ["previous", Ne(e, t), "next"]
                    },
                    full_numbers: function(e, t) {
                        return ["first", "previous", Ne(e, t), "next", "last"]
                    },
                    first_last_numbers: function(e, t) {
                        return ["first", Ne(e, t), "last"]
                    },
                    _numbers: Ne,
                    numbers_length: 7
                }),
                    e.extend(!0, Qe.ext.renderer, {
                        pageButton: {
                            _: function(t, a, o, i, s, l) {
                                var c, u, d, h = t.oClasses,
                                    f = t.oLanguage.oPaginate,
                                    p = t.oLanguage.oAria.paginate || {},
                                    m = 0,
                                    g = function(n, r) {
                                        var a, i, d, v = function(e) {
                                            ce(t, e.data.action, !0)
                                        };
                                        for (a = 0, i = r.length; a < i; a++) if (d = r[a], e.isArray(d)) {
                                            var b = e("<" + (d.DT_el || "div") + "/>").appendTo(n);
                                            g(b, d)
                                        } else {
                                            switch (c = null, u = "", d) {
                                                case "ellipsis":
                                                    n.append('<span class="ellipsis">&#x2026;</span>');
                                                    break;
                                                case "first":
                                                    c = f.sFirst,
                                                        u = d + (s > 0 ? "": " " + h.sPageButtonDisabled);
                                                    break;
                                                case "previous":
                                                    c = f.sPrevious,
                                                        u = d + (s > 0 ? "": " " + h.sPageButtonDisabled);
                                                    break;
                                                case "next":
                                                    c = f.sNext,
                                                        u = d + (s < l - 1 ? "": " " + h.sPageButtonDisabled);
                                                    break;
                                                case "last":
                                                    c = f.sLast,
                                                        u = d + (s < l - 1 ? "": " " + h.sPageButtonDisabled);
                                                    break;
                                                default:
                                                    c = d + 1,
                                                        u = s === d ? h.sPageButtonActive: ""
                                            }
                                            null !== c && (Re(e("<a>", {
                                                    "class": h.sPageButton + " " + u,
                                                    "aria-controls": t.sTableId,
                                                    "aria-label": p[d],
                                                    "data-dt-idx": m,
                                                    tabindex: t.iTabIndex,
                                                    id: 0 === o && "string" == typeof d ? t.sTableId + "_" + d: null
                                                }).html(c).appendTo(n), {
                                                    action: d
                                                },
                                                v), m++)
                                        }
                                    };
                                try {
                                    d = e(a).find(n.activeElement).data("dt-idx")
                                } catch(v) {}
                                g(e(a).empty(), i),
                                d !== r && e(a).find("[data-dt-idx=" + d + "]").focus()
                            }
                        }
                    }),
                    e.extend(Qe.ext.type.detect, [function(e, t) {
                        var n = t.oLanguage.sDecimal;
                        return nt(e, n) ? "num" + n: null
                    },
                        function(e, t) {
                            if (e && !(e instanceof Date) && !Xe.test(e)) return null;
                            var n = Date.parse(e);
                            return null !== n && !isNaN(n) || Ye(e) ? "date": null
                        },
                        function(e, t) {
                            var n = t.oLanguage.sDecimal;
                            return nt(e, n, !0) ? "num-fmt" + n: null
                        },
                        function(e, t) {
                            var n = t.oLanguage.sDecimal;
                            return rt(e, n) ? "html-num" + n: null
                        },
                        function(e, t) {
                            var n = t.oLanguage.sDecimal;
                            return rt(e, n, !0) ? "html-num-fmt" + n: null
                        },
                        function(e, t) {
                            return Ye(e) || "string" == typeof e && -1 !== e.indexOf("<") ? "html": null
                        }]),
                    e.extend(Qe.ext.type.search, {
                        html: function(e) {
                            return Ye(e) ? e: "string" == typeof e ? e.replace(Ke, " ").replace(Je, "") : ""
                        },
                        string: function(e) {
                            return Ye(e) ? e: "string" == typeof e ? e.replace(Ke, " ") : e
                        }
                    });
                var Rt = function(e, t, n, r) {
                    return 0 === e || e && "-" !== e ? (t && (e = tt(e, t)), e.replace && (n && (e = e.replace(n, "")), r && (e = e.replace(r, ""))), 1 * e) : -Infinity
                };
                e.extend(ze.type.order, {
                    "date-pre": function(e) {
                        return Date.parse(e) || -Infinity
                    },
                    "html-pre": function(e) {
                        return Ye(e) ? "": e.replace ? e.replace(/<.*?>/g, "").toLowerCase() : e + ""
                    },
                    "string-pre": function(e) {
                        return Ye(e) ? "": "string" == typeof e ? e.toLowerCase() : e.toString ? e.toString() : ""
                    },
                    "string-asc": function(e, t) {
                        return e < t ? -1 : e > t ? 1 : 0
                    },
                    "string-desc": function(e, t) {
                        return e < t ? 1 : e > t ? -1 : 0
                    }
                }),
                    Be(""),
                    e.extend(!0, Qe.ext.renderer, {
                        header: {
                            _: function(t, n, r, a) {
                                e(t.nTable).on("order.dt.DT",
                                    function(e, o, i, s) {
                                        if (t === o) {
                                            var l = r.idx;
                                            n.removeClass(r.sSortingClass + " " + a.sSortAsc + " " + a.sSortDesc).addClass("asc" == s[l] ? a.sSortAsc: "desc" == s[l] ? a.sSortDesc: r.sSortingClass)
                                        }
                                    })
                            },
                            jqueryui: function(t, n, r, a) {
                                e("<div/>").addClass(a.sSortJUIWrapper).append(n.contents()).append(e("<span/>").addClass(a.sSortIcon + " " + r.sSortingClassJUI)).appendTo(n),
                                    e(t.nTable).on("order.dt.DT",
                                        function(e, o, i, s) {
                                            if (t === o) {
                                                var l = r.idx;
                                                n.removeClass(a.sSortAsc + " " + a.sSortDesc).addClass("asc" == s[l] ? a.sSortAsc: "desc" == s[l] ? a.sSortDesc: r.sSortingClass),
                                                    n.find("span." + a.sSortIcon).removeClass(a.sSortJUIAsc + " " + a.sSortJUIDesc + " " + a.sSortJUI + " " + a.sSortJUIAscAllowed + " " + a.sSortJUIDescAllowed).addClass("asc" == s[l] ? a.sSortJUIAsc: "desc" == s[l] ? a.sSortJUIDesc: r.sSortingClassJUI)
                                            }
                                        })
                            }
                        }
                    });
                var Et = function(e) {
                    return "string" == typeof e ? e.replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;") : e
                };
                return Qe.render = {
                    number: function(e, t, n, r, a) {
                        return {
                            display: function(o) {
                                if ("number" != typeof o && "string" != typeof o) return o;
                                var i = o < 0 ? "-": "",
                                    s = parseFloat(o);
                                if (isNaN(s)) return Et(o);
                                s = s.toFixed(n),
                                    o = Math.abs(s);
                                var l = parseInt(o, 10),
                                    c = n ? t + (o - l).toFixed(n).substring(2) : "";
                                return i + (r || "") + l.toString().replace(/\B(?=(\d{3})+(?!\d))/g, e) + c + (a || "")
                            }
                        }
                    },
                    text: function() {
                        return {
                            display: Et
                        }
                    }
                },
                    e.extend(Qe.ext.internal, {
                        _fnExternApiFunc: qe,
                        _fnBuildAjax: q,
                        _fnAjaxUpdate: z,
                        _fnAjaxParameters: $,
                        _fnAjaxUpdateDraw: U,
                        _fnAjaxDataSrc: V,
                        _fnAddColumn: d,
                        _fnColumnOptions: h,
                        _fnAdjustColumnSizing: f,
                        _fnVisibleToColumnIndex: p,
                        _fnColumnIndexToVisible: m,
                        _fnVisbleColumns: g,
                        _fnGetColumns: v,
                        _fnColumnTypes: b,
                        _fnApplyColumnDefs: y,
                        _fnHungarianMap: a,
                        _fnCamelToHungarian: o,
                        _fnLanguageCompat: i,
                        _fnBrowserDetect: c,
                        _fnAddData: _,
                        _fnAddTr: C,
                        _fnNodeToDataIndex: function(e, t) {
                            return t._DT_RowIndex !== r ? t._DT_RowIndex: null
                        },
                        _fnNodeToColumnIndex: function(t, n, r) {
                            return e.inArray(r, t.aoData[n].anCells)
                        },
                        _fnGetCellData: w,
                        _fnSetCellData: x,
                        _fnSplitObjNotation: S,
                        _fnGetObjectDataFn: T,
                        _fnSetObjectDataFn: A,
                        _fnGetDataMaster: k,
                        _fnClearTable: D,
                        _fnDeleteIndex: P,
                        _fnInvalidate: j,
                        _fnGetRowElements: F,
                        _fnCreateTr: I,
                        _fnBuildHead: E,
                        _fnDrawHead: M,
                        _fnDraw: H,
                        _fnReDraw: L,
                        _fnAddOptionsHtml: O,
                        _fnDetectHeader: N,
                        _fnGetUniqueThs: B,
                        _fnFeatureHtmlFilter: Q,
                        _fnFilterComplete: W,
                        _fnFilterCustom: K,
                        _fnFilterColumn: J,
                        _fnFilter: X,
                        _fnFilterCreateSearch: G,
                        _fnEscapeRegex: ft,
                        _fnFilterData: Z,
                        _fnFeatureHtmlInfo: te,
                        _fnUpdateInfo: ne,
                        _fnInfoMacros: re,
                        _fnInitialise: ae,
                        _fnInitComplete: oe,
                        _fnLengthChange: ie,
                        _fnFeatureHtmlLength: se,
                        _fnFeatureHtmlPaginate: le,
                        _fnPageChange: ce,
                        _fnFeatureHtmlProcessing: ue,
                        _fnProcessingDisplay: de,
                        _fnFeatureHtmlTable: he,
                        _fnScrollDraw: fe,
                        _fnApplyToChildren: pe,
                        _fnCalculateColumnWidths: me,
                        _fnThrottle: vt,
                        _fnConvertToWidth: ge,
                        _fnGetWidestNode: ve,
                        _fnGetMaxLenString: be,
                        _fnStringToCss: ye,
                        _fnSortFlatten: _e,
                        _fnSort: Ce,
                        _fnSortAria: we,
                        _fnSortListener: xe,
                        _fnSortAttachListener: Se,
                        _fnSortingClasses: Te,
                        _fnSortData: Ae,
                        _fnSaveState: ke,
                        _fnLoadState: De,
                        _fnSettingsFromNode: Pe,
                        _fnLog: je,
                        _fnMap: Fe,
                        _fnBindAction: Re,
                        _fnCallbackReg: Ee,
                        _fnCallbackFire: Me,
                        _fnLengthOverflow: He,
                        _fnRenderer: Le,
                        _fnDataSource: Oe,
                        _fnRowAttributes: R,
                        _fnCalculateEnd: function() {}
                    }),
                    e.fn.dataTable = Qe,
                    Qe.$ = e,
                    e.fn.dataTableSettings = Qe.settings,
                    e.fn.dataTableExt = Qe.ext,
                    e.fn.DataTable = function(t) {
                        return e(this).dataTable(t).api()
                    },
                    e.each(Qe,
                        function(t, n) {
                            e.fn.DataTable[t] = n
                        }),
                    e.fn.dataTable
            })
        },
        "./node_modules/lodash.throttle/index.js": function(e, t, n) { (function(t) {
            function n(e, t, n) {
                function i(t) {
                    var n = d,
                        r = h;
                    return d = h = undefined,
                        _ = t,
                        p = e.apply(r, n)
                }
                function s(e) {
                    var n = e - g,
                        r = e - _;
                    return g === undefined || n >= t || n < 0 || w && r >= f
                }
                function l() {
                    var e = y();
                    if (s(e)) return c(e);
                    m = setTimeout(l,
                        function(e) {
                            var n = t - (e - g);
                            return w ? b(n, f - (e - _)) : n
                        } (e))
                }
                function c(e) {
                    return m = undefined,
                        x && d ? i(e) : (d = h = undefined, p)
                }
                function u() {
                    var e = y(),
                        n = s(e);
                    if (d = arguments, h = this, g = e, n) {
                        if (m === undefined) return function(e) {
                            return _ = e,
                                m = setTimeout(l, t),
                                C ? i(e) : p
                        } (g);
                        if (w) return m = setTimeout(l, t),
                            i(g)
                    }
                    return m === undefined && (m = setTimeout(l, t)),
                        p
                }
                var d, h, f, p, m, g, _ = 0,
                    C = !1,
                    w = !1,
                    x = !0;
                if ("function" != typeof e) throw new TypeError(o);
                return t = a(t) || 0,
                r(n) && (C = !!n.leading, f = (w = "maxWait" in n) ? v(a(n.maxWait) || 0, t) : f, x = "trailing" in n ? !!n.trailing: x),
                    u.cancel = function() {
                        m !== undefined && clearTimeout(m),
                            _ = 0,
                            d = g = h = m = undefined
                    },
                    u.flush = function() {
                        return m === undefined ? p: c(y())
                    },
                    u
            }
            function r(e) {
                var t = typeof e;
                return !! e && ("object" == t || "function" == t)
            }
            function a(e) {
                if ("number" == typeof e) return e;
                if (function(e) {
                    return "symbol" == typeof e ||
                        function(e) {
                            return !! e && "object" == typeof e
                        } (e) && g.call(e) == s
                } (e)) return i;
                if (r(e)) {
                    var t = "function" == typeof e.valueOf ? e.valueOf() : e;
                    e = r(t) ? t + "": t
                }
                if ("string" != typeof e) return 0 === e ? e: +e;
                e = e.replace(l, "");
                var n = u.test(e);
                return n || d.test(e) ? h(e.slice(2), n ? 2 : 8) : c.test(e) ? i: +e
            }
            var o = "Expected a function",
                i = NaN,
                s = "[object Symbol]",
                l = /^\s+|\s+$/g,
                c = /^[-+]0x[0-9a-f]+$/i,
                u = /^0b[01]+$/i,
                d = /^0o[0-7]+$/i,
                h = parseInt,
                f = "object" == typeof t && t && t.Object === Object && t,
                p = "object" == typeof self && self && self.Object === Object && self,
                m = f || p || Function("return this")(),
                g = Object.prototype.toString,
                v = Math.max,
                b = Math.min,
                y = function() {
                    return m.Date.now()
                };
            e.exports = function(e, t, a) {
                var i = !0,
                    s = !0;
                if ("function" != typeof e) throw new TypeError(o);
                return r(a) && (i = "leading" in a ? !!a.leading: i, s = "trailing" in a ? !!a.trailing: s),
                    n(e, t, {
                        leading: i,
                        maxWait: t,
                        trailing: s
                    })
            }
        }).call(t, n("./node_modules/webpack/buildin/global.js"))},
        "./node_modules/node-libs-browser/node_modules/process/browser.js": function(e, t) {
            function n() {
                throw new Error("setTimeout has not been defined")
            }
            function r() {
                throw new Error("clearTimeout has not been defined")
            }
            function a(e) {
                if (c === setTimeout) return setTimeout(e, 0);
                if ((c === n || !c) && setTimeout) return c = setTimeout,
                    setTimeout(e, 0);
                try {
                    return c(e, 0)
                } catch(t) {
                    try {
                        return c.call(null, e, 0)
                    } catch(t) {
                        return c.call(this, e, 0)
                    }
                }
            }
            function o() {
                p && h && (p = !1, h.length ? f = h.concat(f) : m = -1, f.length && i())
            }
            function i() {
                if (!p) {
                    var e = a(o);
                    p = !0;
                    for (var t = f.length; t;) {
                        for (h = f, f = []; ++m < t;) h && h[m].run();
                        m = -1,
                            t = f.length
                    }
                    h = null,
                        p = !1,
                        function(e) {
                            if (u === clearTimeout) return clearTimeout(e);
                            if ((u === r || !u) && clearTimeout) return u = clearTimeout,
                                clearTimeout(e);
                            try {
                                u(e)
                            } catch(t) {
                                try {
                                    return u.call(null, e)
                                } catch(t) {
                                    return u.call(this, e)
                                }
                            }
                        } (e)
                }
            }
            function s(e, t) {
                this.fun = e,
                    this.array = t
            }
            function l() {}
            var c, u, d = e.exports = {}; !
                function() {
                    try {
                        c = "function" == typeof setTimeout ? setTimeout: n
                    } catch(e) {
                        c = n
                    }
                    try {
                        u = "function" == typeof clearTimeout ? clearTimeout: r
                    } catch(e) {
                        u = r
                    }
                } ();
            var h, f = [],
                p = !1,
                m = -1;
            d.nextTick = function(e) {
                var t = new Array(arguments.length - 1);
                if (arguments.length > 1) for (var n = 1; n < arguments.length; n++) t[n - 1] = arguments[n];
                f.push(new s(e, t)),
                1 !== f.length || p || a(i)
            },
                s.prototype.run = function() {
                    this.fun.apply(null, this.array)
                },
                d.title = "browser",
                d.browser = !0,
                d.env = {},
                d.argv = [],
                d.version = "",
                d.versions = {},
                d.on = l,
                d.addListener = l,
                d.once = l,
                d.off = l,
                d.removeListener = l,
                d.removeAllListeners = l,
                d.emit = l,
                d.prependListener = l,
                d.prependOnceListener = l,
                d.listeners = function(e) {
                    return []
                },
                d.binding = function(e) {
                    throw new Error("process.binding is not supported")
                },
                d.cwd = function() {
                    return "/"
                },
                d.chdir = function(e) {
                    throw new Error("process.chdir is not supported")
                },
                d.umask = function() {
                    return 0
                }
        },
        "./node_modules/webpack/buildin/global.js": function(e, t) {
            var n;
            n = function() {
                return this
            } ();
            try {
                n = n || Function("return this")() || (0, eval)("this")
            } catch(r) {
                "object" == typeof window && (n = window)
            }
            e.exports = n
        },
        emcharts3: function(e, t) {e.exports = emcharts3},
        jquery: function(e, t) {
            e.exports = jQuery

    });
//# sourceMappingURL=boardlist.min.js.map
