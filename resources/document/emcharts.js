!function(t) {
    function i(e) {
        if (o[e])
            return o[e].exports;
        var n = o[e] = {
            exports: {},
            id: e,
            loaded: !1
        };
        return t[e].call(n.exports, n, n.exports, i),
            n.loaded = !0,
            n.exports
    }
    var o = {};
    return i.m = t,
        i.c = o,
        i.p = "",
        i(0)
}
([function(t, i, o) {
    o(1),
        o(2),
        t.exports = o(3)
}
    , function(t, i, o) {
        var e, n;
        !function(s, a) {
            "use strict";
            e = a,
                n = "function" == typeof e ? e.call(i, o, i, t) : e,
                !(void 0 !== n && (t.exports = n))
        }(this, function() {
            var t, i, o = Array, e = o.prototype, n = Object, s = n.prototype, a = Function, r = a.prototype, p = String, h = p.prototype, l = Number, c = l.prototype, d = e.slice, u = e.splice, m = e.push, f = e.unshift, v = e.concat, g = e.join, y = r.call, A = r.apply, x = Math.max, w = Math.min, _ = s.toString, b = "function" == typeof Symbol && "symbol" == typeof Symbol.toStringTag, T = Function.prototype.toString, M = /^\s*class /, k = function(t) {
                try {
                    var i = T.call(t)
                        , o = i.replace(/\/\/.*\n/g, "")
                        , e = o.replace(/\/\*[.\s\S]*\*\//g, "")
                        , n = e.replace(/\n/gm, " ").replace(/ {2}/g, " ");
                    return M.test(n)
                } catch (s) {
                    return !1
                }
            }, C = function(t) {
                try {
                    return k(t) ? !1 : (T.call(t),
                        !0)
                } catch (i) {
                    return !1
                }
            }, N = "[object Function]", E = "[object GeneratorFunction]", t = function(t) {
                if (!t)
                    return !1;
                if ("function" != typeof t && "object" != typeof t)
                    return !1;
                if (b)
                    return C(t);
                if (k(t))
                    return !1;
                var i = _.call(t);
                return i === N || i === E
            }, S = RegExp.prototype.exec, D = function(t) {
                try {
                    return S.call(t),
                        !0
                } catch (i) {
                    return !1
                }
            }, L = "[object RegExp]";
            i = function(t) {
                return "object" != typeof t ? !1 : b ? D(t) : _.call(t) === L
            }
            ;
            var R, I = String.prototype.valueOf, Y = function(t) {
                try {
                    return I.call(t),
                        !0
                } catch (i) {
                    return !1
                }
            }, W = "[object String]";
            R = function(t) {
                return "string" == typeof t ? !0 : "object" != typeof t ? !1 : b ? Y(t) : _.call(t) === W
            }
            ;
            var F = n.defineProperty && function() {
                try {
                    var t = {};
                    n.defineProperty(t, "x", {
                        enumerable: !1,
                        value: t
                    });
                    for (var i in t)
                        return !1;
                    return t.x === t
                } catch (o) {
                    return !1
                }
            }()
                , Z = function(t) {
                var i;
                return i = F ? function(t, i, o, e) {
                        !e && i in t || n.defineProperty(t, i, {
                            configurable: !0,
                            enumerable: !1,
                            writable: !0,
                            value: o
                        })
                    }
                    : function(t, i, o, e) {
                        !e && i in t || (t[i] = o)
                    }
                    ,
                    function(o, e, n) {
                        for (var s in e)
                            t.call(e, s) && i(o, s, e[s], n)
                    }
            }(s.hasOwnProperty)
                , P = function(t) {
                var i = typeof t;
                return null === t || "object" !== i && "function" !== i
            }
                , G = l.isNaN || function(t) {
                return t !== t
            }
                , B = {
                ToInteger: function(t) {
                    var i = +t;
                    return G(i) ? i = 0 : 0 !== i && i !== 1 / 0 && i !== -(1 / 0) && (i = (i > 0 || -1) * Math.floor(Math.abs(i))),
                        i
                },
                ToPrimitive: function(i) {
                    var o, e, n;
                    if (P(i))
                        return i;
                    if (e = i.valueOf,
                    t(e) && (o = e.call(i),
                        P(o)))
                        return o;
                    if (n = i.toString,
                    t(n) && (o = n.call(i),
                        P(o)))
                        return o;
                    throw new TypeError
                },
                ToObject: function(t) {
                    if (null == t)
                        throw new TypeError("can't convert " + t + " to object");
                    return n(t)
                },
                ToUint32: function(t) {
                    return t >>> 0
                }
            }
                , z = function() {};
            Z(r, {
                bind: function(i) {
                    var o = this;
                    if (!t(o))
                        throw new TypeError("Function.prototype.bind called on incompatible " + o);
                    for (var e, s = d.call(arguments, 1), r = function() {
                        if (this instanceof e) {
                            var t = A.call(o, this, v.call(s, d.call(arguments)));
                            return n(t) === t ? t : this
                        }
                        return A.call(o, i, v.call(s, d.call(arguments)))
                    }, p = x(0, o.length - s.length), h = [], l = 0; p > l; l++)
                        m.call(h, "$" + l);
                    return e = a("binder", "return function (" + g.call(h, ",") + "){ return binder.apply(this, arguments); }")(r),
                    o.prototype && (z.prototype = o.prototype,
                        e.prototype = new z,
                        z.prototype = null),
                        e
                }
            });
            var U = y.bind(s.hasOwnProperty)
                , O = y.bind(s.toString)
                , j = y.bind(d)
                , H = A.bind(d)
                , X = y.bind(h.slice)
                , J = y.bind(h.split)
                , Q = y.bind(h.indexOf)
                , V = y.bind(m)
                , K = y.bind(s.propertyIsEnumerable)
                , q = y.bind(e.sort)
                , $ = o.isArray || function(t) {
                return "[object Array]" === O(t)
            }
                , ti = 1 !== [].unshift(0);
            Z(e, {
                unshift: function() {
                    return f.apply(this, arguments),
                        this.length
                }
            }, ti),
                Z(o, {
                    isArray: $
                });
            var ii = n("a")
                , oi = "a" !== ii[0] || !(0 in ii)
                , ei = function(t) {
                var i = !0
                    , o = !0
                    , e = !1;
                if (t)
                    try {
                        t.call("foo", function(t, o, e) {
                            "object" != typeof e && (i = !1)
                        }),
                            t.call([1], function() {
                                "use strict";
                                o = "string" == typeof this
                            }, "x")
                    } catch (n) {
                        e = !0
                    }
                return !!t && !e && i && o
            };
            Z(e, {
                forEach: function(i) {
                    var o, e = B.ToObject(this), n = oi && R(this) ? J(this, "") : e, s = -1, a = B.ToUint32(n.length);
                    if (arguments.length > 1 && (o = arguments[1]),
                        !t(i))
                        throw new TypeError("Array.prototype.forEach callback must be a function");
                    for (; ++s < a; )
                        s in n && ("undefined" == typeof o ? i(n[s], s, e) : i.call(o, n[s], s, e))
                }
            }, !ei(e.forEach)),
                Z(e, {
                    map: function(i) {
                        var e, n = B.ToObject(this), s = oi && R(this) ? J(this, "") : n, a = B.ToUint32(s.length), r = o(a);
                        if (arguments.length > 1 && (e = arguments[1]),
                            !t(i))
                            throw new TypeError("Array.prototype.map callback must be a function");
                        for (var p = 0; a > p; p++)
                            p in s && (r[p] = "undefined" == typeof e ? i(s[p], p, n) : i.call(e, s[p], p, n));
                        return r
                    }
                }, !ei(e.map)),
                Z(e, {
                    filter: function(i) {
                        var o, e, n = B.ToObject(this), s = oi && R(this) ? J(this, "") : n, a = B.ToUint32(s.length), r = [];
                        if (arguments.length > 1 && (e = arguments[1]),
                            !t(i))
                            throw new TypeError("Array.prototype.filter callback must be a function");
                        for (var p = 0; a > p; p++)
                            p in s && (o = s[p],
                            ("undefined" == typeof e ? i(o, p, n) : i.call(e, o, p, n)) && V(r, o));
                        return r
                    }
                }, !ei(e.filter)),
                Z(e, {
                    every: function(i) {
                        var o, e = B.ToObject(this), n = oi && R(this) ? J(this, "") : e, s = B.ToUint32(n.length);
                        if (arguments.length > 1 && (o = arguments[1]),
                            !t(i))
                            throw new TypeError("Array.prototype.every callback must be a function");
                        for (var a = 0; s > a; a++)
                            if (a in n && !("undefined" == typeof o ? i(n[a], a, e) : i.call(o, n[a], a, e)))
                                return !1;
                        return !0
                    }
                }, !ei(e.every)),
                Z(e, {
                    some: function(i) {
                        var o, e = B.ToObject(this), n = oi && R(this) ? J(this, "") : e, s = B.ToUint32(n.length);
                        if (arguments.length > 1 && (o = arguments[1]),
                            !t(i))
                            throw new TypeError("Array.prototype.some callback must be a function");
                        for (var a = 0; s > a; a++)
                            if (a in n && ("undefined" == typeof o ? i(n[a], a, e) : i.call(o, n[a], a, e)))
                                return !0;
                        return !1
                    }
                }, !ei(e.some));
            var ni = !1;
            e.reduce && (ni = "object" == typeof e.reduce.call("es5", function(t, i, o, e) {
                return e
            })),
                Z(e, {
                    reduce: function(i) {
                        var o = B.ToObject(this)
                            , e = oi && R(this) ? J(this, "") : o
                            , n = B.ToUint32(e.length);
                        if (!t(i))
                            throw new TypeError("Array.prototype.reduce callback must be a function");
                        if (0 === n && 1 === arguments.length)
                            throw new TypeError("reduce of empty array with no initial value");
                        var s, a = 0;
                        if (arguments.length >= 2)
                            s = arguments[1];
                        else
                            for (; ; ) {
                                if (a in e) {
                                    s = e[a++];
                                    break
                                }
                                if (++a >= n)
                                    throw new TypeError("reduce of empty array with no initial value")
                            }
                        for (; n > a; a++)
                            a in e && (s = i(s, e[a], a, o));
                        return s
                    }
                }, !ni);
            var si = !1;
            e.reduceRight && (si = "object" == typeof e.reduceRight.call("es5", function(t, i, o, e) {
                return e
            })),
                Z(e, {
                    reduceRight: function(i) {
                        var o = B.ToObject(this)
                            , e = oi && R(this) ? J(this, "") : o
                            , n = B.ToUint32(e.length);
                        if (!t(i))
                            throw new TypeError("Array.prototype.reduceRight callback must be a function");
                        if (0 === n && 1 === arguments.length)
                            throw new TypeError("reduceRight of empty array with no initial value");
                        var s, a = n - 1;
                        if (arguments.length >= 2)
                            s = arguments[1];
                        else
                            for (; ; ) {
                                if (a in e) {
                                    s = e[a--];
                                    break
                                }
                                if (--a < 0)
                                    throw new TypeError("reduceRight of empty array with no initial value")
                            }
                        if (0 > a)
                            return s;
                        do
                            a in e && (s = i(s, e[a], a, o));
                        while (a--);return s
                    }
                }, !si);
            var ai = e.indexOf && -1 !== [0, 1].indexOf(1, 2);
            Z(e, {
                indexOf: function(t) {
                    var i = oi && R(this) ? J(this, "") : B.ToObject(this)
                        , o = B.ToUint32(i.length);
                    if (0 === o)
                        return -1;
                    var e = 0;
                    for (arguments.length > 1 && (e = B.ToInteger(arguments[1])),
                             e = e >= 0 ? e : x(0, o + e); o > e; e++)
                        if (e in i && i[e] === t)
                            return e;
                    return -1
                }
            }, ai);
            var ri = e.lastIndexOf && -1 !== [0, 1].lastIndexOf(0, -3);
            Z(e, {
                lastIndexOf: function(t) {
                    var i = oi && R(this) ? J(this, "") : B.ToObject(this)
                        , o = B.ToUint32(i.length);
                    if (0 === o)
                        return -1;
                    var e = o - 1;
                    for (arguments.length > 1 && (e = w(e, B.ToInteger(arguments[1]))),
                             e = e >= 0 ? e : o - Math.abs(e); e >= 0; e--)
                        if (e in i && t === i[e])
                            return e;
                    return -1
                }
            }, ri);
            var pi = function() {
                var t = [1, 2]
                    , i = t.splice();
                return 2 === t.length && $(i) && 0 === i.length
            }();
            Z(e, {
                splice: function() {
                    return 0 === arguments.length ? [] : u.apply(this, arguments)
                }
            }, !pi);
            var hi = function() {
                var t = {};
                return e.splice.call(t, 0, 0, 1),
                1 === t.length
            }();
            Z(e, {
                splice: function(t, i) {
                    if (0 === arguments.length)
                        return [];
                    var o = arguments;
                    return this.length = x(B.ToInteger(this.length), 0),
                    arguments.length > 0 && "number" != typeof i && (o = j(arguments),
                        o.length < 2 ? V(o, this.length - t) : o[1] = B.ToInteger(i)),
                        u.apply(this, o)
                }
            }, !hi);
            var li = function() {
                var t = new o(1e5);
                return t[8] = "x",
                    t.splice(1, 1),
                7 === t.indexOf("x")
            }()
                , ci = function() {
                var t = 256
                    , i = [];
                return i[t] = "a",
                    i.splice(t + 1, 0, "b"),
                "a" === i[t]
            }();
            Z(e, {
                splice: function(t, i) {
                    for (var o, e = B.ToObject(this), n = [], s = B.ToUint32(e.length), a = B.ToInteger(t), r = 0 > a ? x(s + a, 0) : w(a, s), h = w(x(B.ToInteger(i), 0), s - r), l = 0; h > l; )
                        o = p(r + l),
                        U(e, o) && (n[l] = e[o]),
                            l += 1;
                    var c, d = j(arguments, 2), u = d.length;
                    if (h > u) {
                        l = r;
                        for (var m = s - h; m > l; )
                            o = p(l + h),
                                c = p(l + u),
                                U(e, o) ? e[c] = e[o] : delete e[c],
                                l += 1;
                        l = s;
                        for (var f = s - h + u; l > f; )
                            delete e[l - 1],
                                l -= 1
                    } else if (u > h)
                        for (l = s - h; l > r; )
                            o = p(l + h - 1),
                                c = p(l + u - 1),
                                U(e, o) ? e[c] = e[o] : delete e[c],
                                l -= 1;
                    l = r;
                    for (var v = 0; v < d.length; ++v)
                        e[l] = d[v],
                            l += 1;
                    return e.length = s - h + u,
                        n
                }
            }, !li || !ci);
            var di, ui = e.join;
            try {
                di = "1,2,3" !== Array.prototype.join.call("123", ",")
            } catch (mi) {
                di = !0
            }
            di && Z(e, {
                join: function(t) {
                    var i = "undefined" == typeof t ? "," : t;
                    return ui.call(R(this) ? J(this, "") : this, i)
                }
            }, di);
            var fi = "1,2" !== [1, 2].join(void 0);
            fi && Z(e, {
                join: function(t) {
                    var i = "undefined" == typeof t ? "," : t;
                    return ui.call(this, i)
                }
            }, fi);
            var vi = function() {
                for (var t = B.ToObject(this), i = B.ToUint32(t.length), o = 0; o < arguments.length; )
                    t[i + o] = arguments[o],
                        o += 1;
                return t.length = i + o,
                i + o
            }
                , gi = function() {
                var t = {}
                    , i = Array.prototype.push.call(t, void 0);
                return 1 !== i || 1 !== t.length || "undefined" != typeof t[0] || !U(t, 0)
            }();
            Z(e, {
                push: function() {
                    return $(this) ? m.apply(this, arguments) : vi.apply(this, arguments)
                }
            }, gi);
            var yi = function() {
                var t = []
                    , i = t.push(void 0);
                return 1 !== i || 1 !== t.length || "undefined" != typeof t[0] || !U(t, 0)
            }();
            Z(e, {
                push: vi
            }, yi),
                Z(e, {
                    slice: function() {
                        var t = R(this) ? J(this, "") : this;
                        return H(t, arguments)
                    }
                }, oi);
            var Ai = function() {
                try {
                    return [1, 2].sort(null),
                        [1, 2].sort({}),
                        !0
                } catch (t) {}
                return !1
            }()
                , xi = function() {
                try {
                    return [1, 2].sort(/a/),
                        !1
                } catch (t) {}
                return !0
            }()
                , wi = function() {
                try {
                    return [1, 2].sort(void 0),
                        !0
                } catch (t) {}
                return !1
            }();
            Z(e, {
                sort: function(i) {
                    if ("undefined" == typeof i)
                        return q(this);
                    if (!t(i))
                        throw new TypeError("Array.prototype.sort callback must be a function");
                    return q(this, i)
                }
            }, Ai || !wi || !xi);
            var _i = !K({
                toString: null
            }, "toString")
                , bi = K(function() {}, "prototype")
                , Ti = !U("x", "0")
                , Mi = function(t) {
                var i = t.constructor;
                return i && i.prototype === t
            }
                , ki = {
                $window: !0,
                $console: !0,
                $parent: !0,
                $self: !0,
                $frame: !0,
                $frames: !0,
                $frameElement: !0,
                $webkitIndexedDB: !0,
                $webkitStorageInfo: !0,
                $external: !0,
                $width: !0,
                $height: !0
            }
                , Ci = function() {
                if ("undefined" == typeof window)
                    return !1;
                for (var t in window)
                    try {
                        !ki["$" + t] && U(window, t) && null !== window[t] && "object" == typeof window[t] && Mi(window[t])
                    } catch (i) {
                        return !0
                    }
                return !1
            }()
                , Ni = function(t) {
                if ("undefined" == typeof window || !Ci)
                    return Mi(t);
                try {
                    return Mi(t)
                } catch (i) {
                    return !1
                }
            }
                , Ei = ["toString", "toLocaleString", "valueOf", "hasOwnProperty", "isPrototypeOf", "propertyIsEnumerable", "constructor"]
                , Si = Ei.length
                , Di = function(t) {
                return "[object Arguments]" === O(t)
            }
                , Li = function(i) {
                return null !== i && "object" == typeof i && "number" == typeof i.length && i.length >= 0 && !$(i) && t(i.callee)
            }
                , Ri = Di(arguments) ? Di : Li;
            Z(n, {
                keys: function(i) {
                    var o = t(i)
                        , e = Ri(i)
                        , n = null !== i && "object" == typeof i
                        , s = n && R(i);
                    if (!n && !o && !e)
                        throw new TypeError("Object.keys called on a non-object");
                    var a = []
                        , r = bi && o;
                    if (s && Ti || e)
                        for (var h = 0; h < i.length; ++h)
                            V(a, p(h));
                    if (!e)
                        for (var l in i)
                            r && "prototype" === l || !U(i, l) || V(a, p(l));
                    if (_i)
                        for (var c = Ni(i), d = 0; Si > d; d++) {
                            var u = Ei[d];
                            c && "constructor" === u || !U(i, u) || V(a, u)
                        }
                    return a
                }
            });
            var Ii = n.keys && function() {
                return 2 === n.keys(arguments).length
            }(1, 2)
                , Yi = n.keys && function() {
                var t = n.keys(arguments);
                return 1 !== arguments.length || 1 !== t.length || 1 !== t[0]
            }(1)
                , Wi = n.keys;
            Z(n, {
                keys: function(t) {
                    return Wi(Ri(t) ? j(t) : t)
                }
            }, !Ii || Yi);
            var Fi, Zi, Pi = 0 !== new Date(-0xc782b5b342b24).getUTCMonth(), Gi = new Date(-0x55d318d56a724), Bi = new Date(14496624e5), zi = "Mon, 01 Jan -45875 11:59:59 GMT" !== Gi.toUTCString(), Ui = Gi.getTimezoneOffset();
            -720 > Ui ? (Fi = "Tue Jan 02 -45875" !== Gi.toDateString(),
                Zi = !/^Thu Dec 10 2015 \d\d:\d\d:\d\d GMT[-\+]\d\d\d\d(?: |$)/.test(Bi.toString())) : (Fi = "Mon Jan 01 -45875" !== Gi.toDateString(),
                Zi = !/^Wed Dec 09 2015 \d\d:\d\d:\d\d GMT[-\+]\d\d\d\d(?: |$)/.test(Bi.toString()));
            var Oi = y.bind(Date.prototype.getFullYear)
                , ji = y.bind(Date.prototype.getMonth)
                , Hi = y.bind(Date.prototype.getDate)
                , Xi = y.bind(Date.prototype.getUTCFullYear)
                , Ji = y.bind(Date.prototype.getUTCMonth)
                , Qi = y.bind(Date.prototype.getUTCDate)
                , Vi = y.bind(Date.prototype.getUTCDay)
                , Ki = y.bind(Date.prototype.getUTCHours)
                , qi = y.bind(Date.prototype.getUTCMinutes)
                , $i = y.bind(Date.prototype.getUTCSeconds)
                , to = y.bind(Date.prototype.getUTCMilliseconds)
                , io = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
                , oo = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
                , eo = function(t, i) {
                return Hi(new Date(i,t,0))
            };
            Z(Date.prototype, {
                getFullYear: function() {
                    if (!(this && this instanceof Date))
                        throw new TypeError("this is not a Date object.");
                    var t = Oi(this);
                    return 0 > t && ji(this) > 11 ? t + 1 : t
                },
                getMonth: function() {
                    if (!(this && this instanceof Date))
                        throw new TypeError("this is not a Date object.");
                    var t = Oi(this)
                        , i = ji(this);
                    return 0 > t && i > 11 ? 0 : i
                },
                getDate: function() {
                    if (!(this && this instanceof Date))
                        throw new TypeError("this is not a Date object.");
                    var t = Oi(this)
                        , i = ji(this)
                        , o = Hi(this);
                    if (0 > t && i > 11) {
                        if (12 === i)
                            return o;
                        var e = eo(0, t + 1);
                        return e - o + 1
                    }
                    return o
                },
                getUTCFullYear: function() {
                    if (!(this && this instanceof Date))
                        throw new TypeError("this is not a Date object.");
                    var t = Xi(this);
                    return 0 > t && Ji(this) > 11 ? t + 1 : t
                },
                getUTCMonth: function() {
                    if (!(this && this instanceof Date))
                        throw new TypeError("this is not a Date object.");
                    var t = Xi(this)
                        , i = Ji(this);
                    return 0 > t && i > 11 ? 0 : i
                },
                getUTCDate: function() {
                    if (!(this && this instanceof Date))
                        throw new TypeError("this is not a Date object.");
                    var t = Xi(this)
                        , i = Ji(this)
                        , o = Qi(this);
                    if (0 > t && i > 11) {
                        if (12 === i)
                            return o;
                        var e = eo(0, t + 1);
                        return e - o + 1
                    }
                    return o
                }
            }, Pi),
                Z(Date.prototype, {
                    toUTCString: function() {
                        if (!(this && this instanceof Date))
                            throw new TypeError("this is not a Date object.");
                        var t = Vi(this)
                            , i = Qi(this)
                            , o = Ji(this)
                            , e = Xi(this)
                            , n = Ki(this)
                            , s = qi(this)
                            , a = $i(this);
                        return io[t] + ", " + (10 > i ? "0" + i : i) + " " + oo[o] + " " + e + " " + (10 > n ? "0" + n : n) + ":" + (10 > s ? "0" + s : s) + ":" + (10 > a ? "0" + a : a) + " GMT"
                    }
                }, Pi || zi),
                Z(Date.prototype, {
                    toDateString: function() {
                        if (!(this && this instanceof Date))
                            throw new TypeError("this is not a Date object.");
                        var t = this.getDay()
                            , i = this.getDate()
                            , o = this.getMonth()
                            , e = this.getFullYear();
                        return io[t] + " " + oo[o] + " " + (10 > i ? "0" + i : i) + " " + e
                    }
                }, Pi || Fi),
            (Pi || Zi) && (Date.prototype.toString = function() {
                if (!(this && this instanceof Date))
                    throw new TypeError("this is not a Date object.");
                var t = this.getDay()
                    , i = this.getDate()
                    , o = this.getMonth()
                    , e = this.getFullYear()
                    , n = this.getHours()
                    , s = this.getMinutes()
                    , a = this.getSeconds()
                    , r = this.getTimezoneOffset()
                    , p = Math.floor(Math.abs(r) / 60)
                    , h = Math.floor(Math.abs(r) % 60);
                return io[t] + " " + oo[o] + " " + (10 > i ? "0" + i : i) + " " + e + " " + (10 > n ? "0" + n : n) + ":" + (10 > s ? "0" + s : s) + ":" + (10 > a ? "0" + a : a) + " GMT" + (r > 0 ? "-" : "+") + (10 > p ? "0" + p : p) + (10 > h ? "0" + h : h)
            }
                ,
            F && n.defineProperty(Date.prototype, "toString", {
                configurable: !0,
                enumerable: !1,
                writable: !0
            }));
            var no = -621987552e5
                , so = "-000001"
                , ao = Date.prototype.toISOString && -1 === new Date(no).toISOString().indexOf(so)
                , ro = Date.prototype.toISOString && "1969-12-31T23:59:59.999Z" !== new Date(-1).toISOString()
                , po = y.bind(Date.prototype.getTime);
            Z(Date.prototype, {
                toISOString: function() {
                    if (!isFinite(this) || !isFinite(po(this)))
                        throw new RangeError("Date.prototype.toISOString called on non-finite value.");
                    var t = Xi(this)
                        , i = Ji(this);
                    t += Math.floor(i / 12),
                        i = (i % 12 + 12) % 12;
                    var o = [i + 1, Qi(this), Ki(this), qi(this), $i(this)];
                    t = (0 > t ? "-" : t > 9999 ? "+" : "") + X("00000" + Math.abs(t), t >= 0 && 9999 >= t ? -4 : -6);
                    for (var e = 0; e < o.length; ++e)
                        o[e] = X("00" + o[e], -2);
                    return t + "-" + j(o, 0, 2).join("-") + "T" + j(o, 2).join(":") + "." + X("000" + to(this), -3) + "Z"
                }
            }, ao || ro);
            var ho = function() {
                try {
                    return Date.prototype.toJSON && null === new Date(0 / 0).toJSON() && -1 !== new Date(no).toJSON().indexOf(so) && Date.prototype.toJSON.call({
                        toISOString: function() {
                            return !0
                        }
                    })
                } catch (t) {
                    return !1
                }
            }();
            ho || (Date.prototype.toJSON = function() {
                    var i = n(this)
                        , o = B.ToPrimitive(i);
                    if ("number" == typeof o && !isFinite(o))
                        return null;
                    var e = i.toISOString;
                    if (!t(e))
                        throw new TypeError("toISOString property is not callable");
                    return e.call(i)
                }
            );
            var lo = 1e15 === Date.parse("+033658-09-27T01:46:40.000Z")
                , co = !isNaN(Date.parse("2012-04-04T24:00:00.500Z")) || !isNaN(Date.parse("2012-11-31T23:59:59.000Z")) || !isNaN(Date.parse("2012-12-31T23:59:60.000Z"))
                , uo = isNaN(Date.parse("2000-01-01T00:00:00.000Z"));
            if (uo || co || !lo) {
                var mo = Math.pow(2, 31) - 1
                    , fo = G(new Date(1970,0,1,0,0,0,mo + 1).getTime());
                Date = function(t) {
                    var i = function(o, e, n, s, a, r, h) {
                        var l, c = arguments.length;
                        if (this instanceof t) {
                            var d = r
                                , u = h;
                            if (fo && c >= 7 && h > mo) {
                                var m = Math.floor(h / mo) * mo
                                    , f = Math.floor(m / 1e3);
                                d += f,
                                    u -= 1e3 * f
                            }
                            l = 1 === c && p(o) === o ? new t(i.parse(o)) : c >= 7 ? new t(o,e,n,s,a,d,u) : c >= 6 ? new t(o,e,n,s,a,d) : c >= 5 ? new t(o,e,n,s,a) : c >= 4 ? new t(o,e,n,s) : c >= 3 ? new t(o,e,n) : c >= 2 ? new t(o,e) : c >= 1 ? new t(o instanceof t ? +o : o) : new t
                        } else
                            l = t.apply(this, arguments);
                        return P(l) || Z(l, {
                            constructor: i
                        }, !0),
                            l
                    }
                        , o = new RegExp("^(\\d{4}|[+-]\\d{6})(?:-(\\d{2})(?:-(\\d{2})(?:T(\\d{2}):(\\d{2})(?::(\\d{2})(?:(\\.\\d{1,}))?)?(Z|(?:([-+])(\\d{2}):(\\d{2})))?)?)?)?$")
                        , e = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365]
                        , n = function(t, i) {
                        var o = i > 1 ? 1 : 0;
                        return e[i] + Math.floor((t - 1969 + o) / 4) - Math.floor((t - 1901 + o) / 100) + Math.floor((t - 1601 + o) / 400) + 365 * (t - 1970)
                    }
                        , s = function(i) {
                        var o = 0
                            , e = i;
                        if (fo && e > mo) {
                            var n = Math.floor(e / mo) * mo
                                , s = Math.floor(n / 1e3);
                            o += s,
                                e -= 1e3 * s
                        }
                        return l(new t(1970,0,1,0,0,o,e))
                    };
                    for (var a in t)
                        U(t, a) && (i[a] = t[a]);
                    Z(i, {
                        now: t.now,
                        UTC: t.UTC
                    }, !0),
                        i.prototype = t.prototype,
                        Z(i.prototype, {
                            constructor: i
                        }, !0);
                    var r = function(i) {
                        var e = o.exec(i);
                        if (e) {
                            var a, r = l(e[1]), p = l(e[2] || 1) - 1, h = l(e[3] || 1) - 1, c = l(e[4] || 0), d = l(e[5] || 0), u = l(e[6] || 0), m = Math.floor(1e3 * l(e[7] || 0)), f = Boolean(e[4] && !e[8]), v = "-" === e[9] ? 1 : -1, g = l(e[10] || 0), y = l(e[11] || 0), A = d > 0 || u > 0 || m > 0;
                            return (A ? 24 : 25) > c && 60 > d && 60 > u && 1e3 > m && p > -1 && 12 > p && 24 > g && 60 > y && h > -1 && h < n(r, p + 1) - n(r, p) && (a = 60 * (24 * (n(r, p) + h) + c + g * v),
                                a = 1e3 * (60 * (a + d + y * v) + u) + m,
                            f && (a = s(a)),
                            a >= -864e13 && 864e13 >= a) ? a : 0 / 0
                        }
                        return t.parse.apply(this, arguments)
                    };
                    return Z(i, {
                        parse: r
                    }),
                        i
                }(Date)
            }
            Date.now || (Date.now = function() {
                    return (new Date).getTime()
                }
            );
            var vo = c.toFixed && ("0.000" !== 8e-5.toFixed(3) || "1" !== .9.toFixed(0) || "1.25" !== 1.255.toFixed(2) || "1000000000000000128" !== 0xde0b6b3a7640080.toFixed(0))
                , go = {
                base: 1e7,
                size: 6,
                data: [0, 0, 0, 0, 0, 0],
                multiply: function(t, i) {
                    for (var o = -1, e = i; ++o < go.size; )
                        e += t * go.data[o],
                            go.data[o] = e % go.base,
                            e = Math.floor(e / go.base)
                },
                divide: function(t) {
                    for (var i = go.size, o = 0; --i >= 0; )
                        o += go.data[i],
                            go.data[i] = Math.floor(o / t),
                            o = o % t * go.base
                },
                numToString: function() {
                    for (var t = go.size, i = ""; --t >= 0; )
                        if ("" !== i || 0 === t || 0 !== go.data[t]) {
                            var o = p(go.data[t]);
                            "" === i ? i = o : i += X("0000000", 0, 7 - o.length) + o
                        }
                    return i
                },
                pow: function Fo(t, i, o) {
                    return 0 === i ? o : i % 2 === 1 ? Fo(t, i - 1, o * t) : Fo(t * t, i / 2, o)
                },
                log: function(t) {
                    for (var i = 0, o = t; o >= 4096; )
                        i += 12,
                            o /= 4096;
                    for (; o >= 2; )
                        i += 1,
                            o /= 2;
                    return i
                }
            }
                , yo = function(t) {
                var i, o, e, n, s, a, r, h;
                if (i = l(t),
                    i = G(i) ? 0 : Math.floor(i),
                0 > i || i > 20)
                    throw new RangeError("Number.toFixed called with invalid number of decimals");
                if (o = l(this),
                    G(o))
                    return "NaN";
                if (-1e21 >= o || o >= 1e21)
                    return p(o);
                if (e = "",
                0 > o && (e = "-",
                    o = -o),
                    n = "0",
                o > 1e-21)
                    if (s = go.log(o * go.pow(2, 69, 1)) - 69,
                        a = 0 > s ? o * go.pow(2, -s, 1) : o / go.pow(2, s, 1),
                        a *= 4503599627370496,
                        s = 52 - s,
                    s > 0) {
                        for (go.multiply(0, a),
                                 r = i; r >= 7; )
                            go.multiply(1e7, 0),
                                r -= 7;
                        for (go.multiply(go.pow(10, r, 1), 0),
                                 r = s - 1; r >= 23; )
                            go.divide(1 << 23),
                                r -= 23;
                        go.divide(1 << r),
                            go.multiply(1, 1),
                            go.divide(2),
                            n = go.numToString()
                    } else
                        go.multiply(0, a),
                            go.multiply(1 << -s, 0),
                            n = go.numToString() + X("0.00000000000000000000", 2, 2 + i);
                return i > 0 ? (h = n.length,
                    n = i >= h ? e + X("0.0000000000000000000", 0, i - h + 2) + n : e + X(n, 0, h - i) + "." + X(n, h - i)) : n = e + n,
                    n
            };
            Z(c, {
                toFixed: yo
            }, vo);
            var Ao = function() {
                try {
                    return "1" === 1..toPrecision(void 0)
                } catch (t) {
                    return !0
                }
            }()
                , xo = c.toPrecision;
            Z(c, {
                toPrecision: function(t) {
                    return "undefined" == typeof t ? xo.call(this) : xo.call(this, t)
                }
            }, Ao),
                2 !== "ab".split(/(?:ab)*/).length || 4 !== ".".split(/(.?)(.?)/).length || "t" === "tesst".split(/(s)*/)[1] || 4 !== "test".split(/(?:)/, -1).length || "".split(/.?/).length || ".".split(/()()/).length > 1 ? !function() {
                    var t = "undefined" == typeof /()??/.exec("")[1]
                        , o = Math.pow(2, 32) - 1;
                    h.split = function(e, n) {
                        var s = String(this);
                        if ("undefined" == typeof e && 0 === n)
                            return [];
                        if (!i(e))
                            return J(this, e, n);
                        var a, r, p, h, l = [], c = (e.ignoreCase ? "i" : "") + (e.multiline ? "m" : "") + (e.unicode ? "u" : "") + (e.sticky ? "y" : ""), d = 0, u = new RegExp(e.source,c + "g");
                        t || (a = new RegExp("^" + u.source + "$(?!\\s)",c));
                        var f = "undefined" == typeof n ? o : B.ToUint32(n);
                        for (r = u.exec(s); r && (p = r.index + r[0].length,
                            !(p > d && (V(l, X(s, d, r.index)),
                            !t && r.length > 1 && r[0].replace(a, function() {
                                for (var t = 1; t < arguments.length - 2; t++)
                                    "undefined" == typeof arguments[t] && (r[t] = void 0)
                            }),
                            r.length > 1 && r.index < s.length && m.apply(l, j(r, 1)),
                                h = r[0].length,
                                d = p,
                            l.length >= f))); )
                            u.lastIndex === r.index && u.lastIndex++,
                                r = u.exec(s);
                        return d === s.length ? (h || !u.test("")) && V(l, "") : V(l, X(s, d)),
                            l.length > f ? j(l, 0, f) : l
                    }
                }() : "0".split(void 0, 0).length && (h.split = function(t, i) {
                        return "undefined" == typeof t && 0 === i ? [] : J(this, t, i)
                    }
                );
            var wo = h.replace
                , _o = function() {
                var t = [];
                return "x".replace(/x(.)?/g, function(i, o) {
                    V(t, o)
                }),
                1 === t.length && "undefined" == typeof t[0]
            }();
            _o || (h.replace = function(o, e) {
                    var n = t(e)
                        , s = i(o) && /\)[*?]/.test(o.source);
                    if (n && s) {
                        var a = function(t) {
                            var i = arguments.length
                                , n = o.lastIndex;
                            o.lastIndex = 0;
                            var s = o.exec(t) || [];
                            return o.lastIndex = n,
                                V(s, arguments[i - 2], arguments[i - 1]),
                                e.apply(this, s)
                        };
                        return wo.call(this, o, a)
                    }
                    return wo.call(this, o, e)
                }
            );
            var bo = h.substr
                , To = "".substr && "b" !== "0b".substr(-1);
            Z(h, {
                substr: function(t, i) {
                    var o = t;
                    return 0 > t && (o = x(this.length + t, 0)),
                        bo.call(this, o, i)
                }
            }, To);
            var Mo = "	\n\f\r   ᠎             　\u2028\u2029﻿"
                , ko = "​"
                , Co = "[" + Mo + "]"
                , No = new RegExp("^" + Co + Co + "*")
                , Eo = new RegExp(Co + Co + "*$")
                , So = h.trim && (Mo.trim() || !ko.trim());
            Z(h, {
                trim: function() {
                    if ("undefined" == typeof this || null === this)
                        throw new TypeError("can't convert " + this + " to object");
                    return p(this).replace(No, "").replace(Eo, "")
                }
            }, So);
            var Do = y.bind(String.prototype.trim)
                , Lo = h.lastIndexOf && -1 !== "abcあい".lastIndexOf("あい", 2);
            Z(h, {
                lastIndexOf: function(t) {
                    if ("undefined" == typeof this || null === this)
                        throw new TypeError("can't convert " + this + " to object");
                    for (var i = p(this), o = p(t), e = arguments.length > 1 ? l(arguments[1]) : 0 / 0, n = G(e) ? 1 / 0 : B.ToInteger(e), s = w(x(n, 0), i.length), a = o.length, r = s + a; r > 0; ) {
                        r = x(0, r - a);
                        var h = Q(X(i, r, s + a), o);
                        if (-1 !== h)
                            return r + h
                    }
                    return -1
                }
            }, Lo);
            var Ro = h.lastIndexOf;
            if (Z(h, {
                lastIndexOf: function() {
                    return Ro.apply(this, arguments)
                }
            }, 1 !== h.lastIndexOf.length),
            (8 !== parseInt(Mo + "08") || 22 !== parseInt(Mo + "0x16")) && (parseInt = function(t) {
                var i = /^[\-+]?0[xX]/;
                return function(o, e) {
                    var n = Do(String(o))
                        , s = l(e) || (i.test(n) ? 16 : 10);
                    return t(n, s)
                }
            }(parseInt)),
            1 / parseFloat("-0") !== -1 / 0 && (parseFloat = function(t) {
                return function(i) {
                    var o = Do(String(i))
                        , e = t(o);
                    return 0 === e && "-" === X(o, 0, 1) ? -0 : e
                }
            }(parseFloat)),
            "RangeError: test" !== String(new RangeError("test"))) {
                var Io = function() {
                    if ("undefined" == typeof this || null === this)
                        throw new TypeError("can't convert " + this + " to object");
                    var t = this.name;
                    "undefined" == typeof t ? t = "Error" : "string" != typeof t && (t = p(t));
                    var i = this.message;
                    return "undefined" == typeof i ? i = "" : "string" != typeof i && (i = p(i)),
                        t ? i ? t + ": " + i : t : i
                };
                Error.prototype.toString = Io
            }
            if (F) {
                var Yo = function(t, i) {
                    if (K(t, i)) {
                        var o = Object.getOwnPropertyDescriptor(t, i);
                        o.configurable && (o.enumerable = !1,
                            Object.defineProperty(t, i, o))
                    }
                };
                Yo(Error.prototype, "message"),
                "" !== Error.prototype.message && (Error.prototype.message = ""),
                    Yo(Error.prototype, "name")
            }
            if ("/a/gim" !== String(/a/gim)) {
                var Wo = function() {
                    var t = "/" + this.source + "/";
                    return this.global && (t += "g"),
                    this.ignoreCase && (t += "i"),
                    this.multiline && (t += "m"),
                        t
                };
                RegExp.prototype.toString = Wo
            }
        })
    }
    , function() {
        document.createElement("canvas").getContext || !function() {
            function t() {
                return this.context_ || (this.context_ = new x(this))
            }
            function i(t, i) {
                var o = G.call(arguments, 2);
                return function() {
                    return t.apply(i, o.concat(G.call(arguments)))
                }
            }
            function o(t) {
                return String(t).replace(/&/g, "&amp;").replace(/"/g, "&quot;")
            }
            function e(t, i, o) {
                t.namespaces[i] || t.namespaces.add(i, o, "#default#VML")
            }
            function n(t) {
                if (e(t, "g_vml_", "urn:schemas-microsoft-com:vml"),
                    e(t, "g_o_", "urn:schemas-microsoft-com:office:office"),
                    !t.styleSheets["ex_canvas_"]) {
                    var i = t.createStyleSheet();
                    i.owningElement.id = "ex_canvas_",
                        i.cssText = "canvas{display:inline-block;overflow:hidden;*zoom: 1;*display: inline;text-align:left;width:300px;height:150px}"
                }
            }
            function s(t) {
                var i = t.srcElement;
                switch (t.propertyName) {
                    case "width":
                        i.getContext().clearRect(),
                            i.style.width = i.attributes.width.nodeValue + "px",
                            i.firstChild.style.width = i.clientWidth + "px";
                        break;
                    case "height":
                        i.getContext().clearRect(),
                            i.style.height = i.attributes.height.nodeValue + "px",
                            i.firstChild.style.height = i.clientHeight + "px"
                }
            }
            function a(t) {
                var i = t.srcElement;
                i.firstChild && (i.firstChild.style.width = i.clientWidth + "px",
                    i.firstChild.style.height = i.clientHeight + "px")
            }
            function r() {
                return [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
            }
            function p(t, i) {
                for (var o = r(), e = 0; 3 > e; e++)
                    for (var n = 0; 3 > n; n++) {
                        for (var s = 0, a = 0; 3 > a; a++)
                            s += t[e][a] * i[a][n];
                        o[e][n] = s
                    }
                return o
            }
            function h(t, i) {
                i.fillStyle = t.fillStyle,
                    i.lineCap = t.lineCap,
                    i.lineJoin = t.lineJoin,
                    i.lineWidth = t.lineWidth,
                    i.miterLimit = t.miterLimit,
                    i.shadowBlur = t.shadowBlur,
                    i.shadowColor = t.shadowColor,
                    i.shadowOffsetX = t.shadowOffsetX,
                    i.shadowOffsetY = t.shadowOffsetY,
                    i.strokeStyle = t.strokeStyle,
                    i.globalAlpha = t.globalAlpha,
                    i.font = t.font,
                    i.textAlign = t.textAlign,
                    i.textBaseline = t.textBaseline,
                    i.arcScaleX_ = t.arcScaleX_,
                    i.arcScaleY_ = t.arcScaleY_,
                    i.lineScale_ = t.lineScale_
            }
            function l(t) {
                var i = t.indexOf("(", 3)
                    , o = t.indexOf(")", i + 1)
                    , e = t.substring(i + 1, o).split(",");
                return (4 != e.length || "a" != t.charAt(3)) && (e[3] = 1),
                    e
            }
            function c(t) {
                return parseFloat(t) / 100
            }
            function d(t, i, o) {
                return Math.min(o, Math.max(i, t))
            }
            function u(t) {
                var i, o, e, n, s, a;
                if (n = parseFloat(t[0]) / 360 % 360,
                0 > n && n++,
                    s = d(c(t[1]), 0, 1),
                    a = d(c(t[2]), 0, 1),
                0 == s)
                    i = o = e = a;
                else {
                    var r = .5 > a ? a * (1 + s) : a + s - a * s
                        , p = 2 * a - r;
                    i = m(p, r, n + 1 / 3),
                        o = m(p, r, n),
                        e = m(p, r, n - 1 / 3)
                }
                return "#" + z[Math.floor(255 * i)] + z[Math.floor(255 * o)] + z[Math.floor(255 * e)]
            }
            function m(t, i, o) {
                return 0 > o && o++,
                o > 1 && o--,
                    1 > 6 * o ? t + 6 * (i - t) * o : 1 > 2 * o ? i : 2 > 3 * o ? t + (i - t) * (2 / 3 - o) * 6 : t
            }
            function f(t) {
                if (t in H)
                    return H[t];
                var i, o = 1;
                if (t = String(t),
                "#" == t.charAt(0))
                    i = t;
                else if (/^rgb/.test(t)) {
                    for (var e, n = l(t), i = "#", s = 0; 3 > s; s++)
                        e = -1 != n[s].indexOf("%") ? Math.floor(255 * c(n[s])) : +n[s],
                            i += z[d(e, 0, 255)];
                    o = +n[3]
                } else if (/^hsl/.test(t)) {
                    var n = l(t);
                    i = u(n),
                        o = n[3]
                } else
                    i = j[t] || t;
                return H[t] = {
                    color: i,
                    alpha: o
                }
            }
            function v(t) {
                if (J[t])
                    return J[t];
                var i = document.createElement("div")
                    , o = i.style;
                try {
                    o.font = t
                } catch (e) {}
                return J[t] = {
                    style: o.fontStyle || X.style,
                    variant: o.fontVariant || X.variant,
                    weight: o.fontWeight || X.weight,
                    size: o.fontSize || X.size,
                    family: o.fontFamily || X.family
                }
            }
            function g(t, i) {
                var o = {};
                for (var e in t)
                    o[e] = t[e];
                var n = parseFloat(i.currentStyle.fontSize)
                    , s = parseFloat(t.size);
                return o.size = "number" == typeof t.size ? t.size : -1 != t.size.indexOf("px") ? s : -1 != t.size.indexOf("em") ? n * s : -1 != t.size.indexOf("%") ? n / 100 * s : -1 != t.size.indexOf("pt") ? s / .75 : n,
                    o.size *= .981,
                    o
            }
            function y(t) {
                return t.style + " " + t.variant + " " + t.weight + " " + t.size + "px " + t.family
            }
            function A(t) {
                return Q[t] || "square"
            }
            function x(t) {
                this.m_ = r(),
                    this.mStack_ = [],
                    this.aStack_ = [],
                    this.currentPath_ = [],
                    this.strokeStyle = "#000",
                    this.fillStyle = "#000",
                    this.lineWidth = 1,
                    this.lineJoin = "miter",
                    this.lineCap = "butt",
                    this.miterLimit = 1 * Z,
                    this.globalAlpha = 1,
                    this.font = "10px sans-serif",
                    this.textAlign = "left",
                    this.textBaseline = "alphabetic",
                    this.canvas = t;
                var i = "width:" + t.clientWidth + "px;height:" + t.clientHeight + "px;overflow:hidden;position:absolute;zoom:1;"
                    , o = t.ownerDocument.createElement("div");
                o.style.cssText = i,
                    t.appendChild(o);
                var e = o.cloneNode(!1);
                e.style.backgroundColor = "red",
                    e.style.filter = "alpha(opacity=0)",
                    t.appendChild(e),
                    this.element_ = o,
                    this.arcScaleX_ = 1,
                    this.arcScaleY_ = 1,
                    this.lineScale_ = 1
            }
            function w(t, i, o, e) {
                t.currentPath_.push({
                    type: "bezierCurveTo",
                    cp1x: i.x,
                    cp1y: i.y,
                    cp2x: o.x,
                    cp2y: o.y,
                    x: e.x,
                    y: e.y
                }),
                    t.currentX_ = e.x,
                    t.currentY_ = e.y
            }
            function _(t, i) {
                var o = f(t.strokeStyle)
                    , e = o.color
                    , n = o.alpha * t.globalAlpha
                    , s = t.lineScale_ * t.lineWidth;
                1 > s && (n *= s),
                    i.push("<g_vml_:stroke", ' opacity="', n, '"', ' joinstyle="', t.lineJoin, '"', ' miterlimit="', t.miterLimit, '"', ' endcap="', A(t.lineCap), '"', ' weight="', s, 'px"', ' color="', e, '" />')
            }
            function b(t, i, o, e) {
                var n = t.fillStyle
                    , s = t.arcScaleX_
                    , a = t.arcScaleY_
                    , r = e.x - o.x
                    , p = e.y - o.y;
                if (n instanceof C) {
                    var h = 0
                        , l = {
                        x: 0,
                        y: 0
                    }
                        , c = 0
                        , d = 1;
                    if ("gradient" == n.type_) {
                        var u = n.x0_ / s
                            , m = n.y0_ / a
                            , v = n.x1_ / s
                            , g = n.y1_ / a
                            , y = T(t, u, m)
                            , A = T(t, v, g)
                            , x = A.x - y.x
                            , w = A.y - y.y;
                        h = 180 * Math.atan2(x, w) / Math.PI,
                        0 > h && (h += 360),
                        1e-6 > h && (h = 0)
                    } else {
                        var y = T(t, n.x0_, n.y0_);
                        l = {
                            x: (y.x - o.x) / r,
                            y: (y.y - o.y) / p
                        },
                            r /= s * Z,
                            p /= a * Z;
                        var _ = L.max(r, p);
                        c = 2 * n.r0_ / _,
                            d = 2 * n.r1_ / _ - c
                    }
                    var b = n.colors_;
                    b.sort(function(t, i) {
                        return t.offset - i.offset
                    });
                    for (var M = b.length, k = b[0].color, E = b[M - 1].color, S = b[0].alpha * t.globalAlpha, D = b[M - 1].alpha * t.globalAlpha, R = [], I = 0; M > I; I++) {
                        var Y = b[I];
                        R.push(Y.offset * d + c + " " + Y.color)
                    }
                    i.push('<g_vml_:fill type="', n.type_, '"', ' method="none" focus="100%"', ' color="', k, '"', ' color2="', E, '"', ' colors="', R.join(","), '"', ' opacity="', D, '"', ' g_o_:opacity2="', S, '"', ' angle="', h, '"', ' focusposition="', l.x, ",", l.y, '" />')
                } else if (n instanceof N) {
                    if (r && p) {
                        var W = -o.x
                            , F = -o.y;
                        i.push("<g_vml_:fill", ' position="', W / r * s * s, ",", F / p * a * a, '"', ' type="tile"', ' src="', n.src_, '" />')
                    }
                } else {
                    var P = f(t.fillStyle)
                        , G = P.color
                        , B = P.alpha * t.globalAlpha;
                    i.push('<g_vml_:fill color="', G, '" opacity="', B, '" />')
                }
            }
            function T(t, i, o) {
                var e = t.m_;
                return {
                    x: Z * (i * e[0][0] + o * e[1][0] + e[2][0]) - P,
                    y: Z * (i * e[0][1] + o * e[1][1] + e[2][1]) - P
                }
            }
            function M(t) {
                return isFinite(t[0][0]) && isFinite(t[0][1]) && isFinite(t[1][0]) && isFinite(t[1][1]) && isFinite(t[2][0]) && isFinite(t[2][1])
            }
            function k(t, i, o) {
                if (M(i) && (t.m_ = i,
                    o)) {
                    var e = i[0][0] * i[1][1] - i[0][1] * i[1][0];
                    t.lineScale_ = F(W(e))
                }
            }
            function C(t) {
                this.type_ = t,
                    this.x0_ = 0,
                    this.y0_ = 0,
                    this.r0_ = 0,
                    this.x1_ = 0,
                    this.y1_ = 0,
                    this.r1_ = 0,
                    this.colors_ = []
            }
            function N(t, i) {
                switch (S(t),
                    i) {
                    case "repeat":
                    case null:
                    case "":
                        this.repetition_ = "repeat";
                        break;
                    case "repeat-x":
                    case "repeat-y":
                    case "no-repeat":
                        this.repetition_ = i;
                        break;
                    default:
                        E("SYNTAX_ERR")
                }
                this.src_ = t.src,
                    this.width_ = t.width,
                    this.height_ = t.height
            }
            function E(t) {
                throw new D(t)
            }
            function S(t) {
                t && 1 == t.nodeType && "IMG" == t.tagName || E("TYPE_MISMATCH_ERR"),
                "complete" != t.readyState && E("INVALID_STATE_ERR")
            }
            function D(t) {
                this.code = this[t],
                    this.message = t + ": DOM Exception " + this.code
            }
            var L = Math
                , R = L.round
                , I = L.sin
                , Y = L.cos
                , W = L.abs
                , F = L.sqrt
                , Z = 10
                , P = Z / 2
                , G = (+navigator.userAgent.match(/MSIE ([\d.]+)?/)[1],
                Array.prototype.slice);
            n(document);
            var B = {
                init: function(t) {
                    var o = t || document;
                    o.createElement("canvas"),
                        o.attachEvent("onreadystatechange", i(this.init_, this, o))
                },
                init_: function(t) {
                    for (var i = t.getElementsByTagName("canvas"), o = 0; o < i.length; o++)
                        this.initElement(i[o])
                },
                initElement: function(i) {
                    if (!i.getContext) {
                        i.getContext = t,
                            n(i.ownerDocument);
                        try {
                            i.innerHTML = ""
                        } catch (o) {}
                        i.attachEvent("onpropertychange", s),
                            i.attachEvent("onresize", a);
                        var e = i.attributes;
                        e.width && e.width.specified ? i.style.width = e.width.nodeValue + "px" : i.width = i.clientWidth,
                            e.height && e.height.specified ? i.style.height = e.height.nodeValue + "px" : i.height = i.clientHeight
                    }
                    return i
                }
            };
            B.init();
            for (var z = [], U = 0; 16 > U; U++)
                for (var O = 0; 16 > O; O++)
                    z[16 * U + O] = U.toString(16) + O.toString(16);
            var j = {
                aliceblue: "#F0F8FF",
                antiquewhite: "#FAEBD7",
                aquamarine: "#7FFFD4",
                azure: "#F0FFFF",
                beige: "#F5F5DC",
                bisque: "#FFE4C4",
                black: "#000000",
                blanchedalmond: "#FFEBCD",
                blueviolet: "#8A2BE2",
                brown: "#A52A2A",
                burlywood: "#DEB887",
                cadetblue: "#5F9EA0",
                chartreuse: "#7FFF00",
                chocolate: "#D2691E",
                coral: "#FF7F50",
                cornflowerblue: "#6495ED",
                cornsilk: "#FFF8DC",
                crimson: "#DC143C",
                cyan: "#00FFFF",
                darkblue: "#00008B",
                darkcyan: "#008B8B",
                darkgoldenrod: "#B8860B",
                darkgray: "#A9A9A9",
                darkgreen: "#006400",
                darkgrey: "#A9A9A9",
                darkkhaki: "#BDB76B",
                darkmagenta: "#8B008B",
                darkolivegreen: "#556B2F",
                darkorange: "#FF8C00",
                darkorchid: "#9932CC",
                darkred: "#8B0000",
                darksalmon: "#E9967A",
                darkseagreen: "#8FBC8F",
                darkslateblue: "#483D8B",
                darkslategray: "#2F4F4F",
                darkslategrey: "#2F4F4F",
                darkturquoise: "#00CED1",
                darkviolet: "#9400D3",
                deeppink: "#FF1493",
                deepskyblue: "#00BFFF",
                dimgray: "#696969",
                dimgrey: "#696969",
                dodgerblue: "#1E90FF",
                firebrick: "#B22222",
                floralwhite: "#FFFAF0",
                forestgreen: "#228B22",
                gainsboro: "#DCDCDC",
                ghostwhite: "#F8F8FF",
                gold: "#FFD700",
                goldenrod: "#DAA520",
                grey: "#808080",
                greenyellow: "#ADFF2F",
                honeydew: "#F0FFF0",
                hotpink: "#FF69B4",
                indianred: "#CD5C5C",
                indigo: "#4B0082",
                ivory: "#FFFFF0",
                khaki: "#F0E68C",
                lavender: "#E6E6FA",
                lavenderblush: "#FFF0F5",
                lawngreen: "#7CFC00",
                lemonchiffon: "#FFFACD",
                lightblue: "#ADD8E6",
                lightcoral: "#F08080",
                lightcyan: "#E0FFFF",
                lightgoldenrodyellow: "#FAFAD2",
                lightgreen: "#90EE90",
                lightgrey: "#D3D3D3",
                lightpink: "#FFB6C1",
                lightsalmon: "#FFA07A",
                lightseagreen: "#20B2AA",
                lightskyblue: "#87CEFA",
                lightslategray: "#778899",
                lightslategrey: "#778899",
                lightsteelblue: "#B0C4DE",
                lightyellow: "#FFFFE0",
                limegreen: "#32CD32",
                linen: "#FAF0E6",
                magenta: "#FF00FF",
                mediumaquamarine: "#66CDAA",
                mediumblue: "#0000CD",
                mediumorchid: "#BA55D3",
                mediumpurple: "#9370DB",
                mediumseagreen: "#3CB371",
                mediumslateblue: "#7B68EE",
                mediumspringgreen: "#00FA9A",
                mediumturquoise: "#48D1CC",
                mediumvioletred: "#C71585",
                midnightblue: "#191970",
                mintcream: "#F5FFFA",
                mistyrose: "#FFE4E1",
                moccasin: "#FFE4B5",
                navajowhite: "#FFDEAD",
                oldlace: "#FDF5E6",
                olivedrab: "#6B8E23",
                orange: "#FFA500",
                orangered: "#FF4500",
                orchid: "#DA70D6",
                palegoldenrod: "#EEE8AA",
                palegreen: "#98FB98",
                paleturquoise: "#AFEEEE",
                palevioletred: "#DB7093",
                papayawhip: "#FFEFD5",
                peachpuff: "#FFDAB9",
                peru: "#CD853F",
                pink: "#FFC0CB",
                plum: "#DDA0DD",
                powderblue: "#B0E0E6",
                rosybrown: "#BC8F8F",
                royalblue: "#4169E1",
                saddlebrown: "#8B4513",
                salmon: "#FA8072",
                sandybrown: "#F4A460",
                seagreen: "#2E8B57",
                seashell: "#FFF5EE",
                sienna: "#A0522D",
                skyblue: "#87CEEB",
                slateblue: "#6A5ACD",
                slategray: "#708090",
                slategrey: "#708090",
                snow: "#FFFAFA",
                springgreen: "#00FF7F",
                steelblue: "#4682B4",
                tan: "#D2B48C",
                thistle: "#D8BFD8",
                tomato: "#FF6347",
                turquoise: "#40E0D0",
                violet: "#EE82EE",
                wheat: "#F5DEB3",
                whitesmoke: "#F5F5F5",
                yellowgreen: "#9ACD32"
            }
                , H = {}
                , X = {
                style: "normal",
                variant: "normal",
                weight: "normal",
                size: 10,
                family: "sans-serif"
            }
                , J = {}
                , Q = {
                butt: "flat",
                round: "round"
            }
                , V = x.prototype;
            V.clearRect = function() {
                this.textMeasureEl_ && (this.textMeasureEl_.removeNode(!0),
                    this.textMeasureEl_ = null),
                    this.element_.innerHTML = ""
            }
                ,
                V.beginPath = function() {
                    this.currentPath_ = []
                }
                ,
                V.moveTo = function(t, i) {
                    var o = T(this, t, i);
                    this.currentPath_.push({
                        type: "moveTo",
                        x: o.x,
                        y: o.y
                    }),
                        this.currentX_ = o.x,
                        this.currentY_ = o.y
                }
                ,
                V.lineTo = function(t, i) {
                    var o = T(this, t, i);
                    this.currentPath_.push({
                        type: "lineTo",
                        x: o.x,
                        y: o.y
                    }),
                        this.currentX_ = o.x,
                        this.currentY_ = o.y
                }
                ,
                V.bezierCurveTo = function(t, i, o, e, n, s) {
                    var a = T(this, n, s)
                        , r = T(this, t, i)
                        , p = T(this, o, e);
                    w(this, r, p, a)
                }
                ,
                V.quadraticCurveTo = function(t, i, o, e) {
                    var n = T(this, t, i)
                        , s = T(this, o, e)
                        , a = {
                        x: this.currentX_ + 2 / 3 * (n.x - this.currentX_),
                        y: this.currentY_ + 2 / 3 * (n.y - this.currentY_)
                    }
                        , r = {
                        x: a.x + (s.x - this.currentX_) / 3,
                        y: a.y + (s.y - this.currentY_) / 3
                    };
                    w(this, a, r, s)
                }
                ,
                V.arc = function(t, i, o, e, n, s) {
                    o *= Z;
                    var a = s ? "at" : "wa"
                        , r = t + Y(e) * o - P
                        , p = i + I(e) * o - P
                        , h = t + Y(n) * o - P
                        , l = i + I(n) * o - P;
                    r != h || s || (r += .125);
                    var c = T(this, t, i)
                        , d = T(this, r, p)
                        , u = T(this, h, l);
                    this.currentPath_.push({
                        type: a,
                        x: c.x,
                        y: c.y,
                        radius: o,
                        xStart: d.x,
                        yStart: d.y,
                        xEnd: u.x,
                        yEnd: u.y
                    })
                }
                ,
                V.rect = function(t, i, o, e) {
                    this.moveTo(t, i),
                        this.lineTo(t + o, i),
                        this.lineTo(t + o, i + e),
                        this.lineTo(t, i + e),
                        this.closePath()
                }
                ,
                V.strokeRect = function(t, i, o, e) {
                    var n = this.currentPath_;
                    this.beginPath(),
                        this.moveTo(t, i),
                        this.lineTo(t + o, i),
                        this.lineTo(t + o, i + e),
                        this.lineTo(t, i + e),
                        this.closePath(),
                        this.stroke(),
                        this.currentPath_ = n
                }
                ,
                V.fillRect = function(t, i, o, e) {
                    var n = this.currentPath_;
                    this.beginPath(),
                        this.moveTo(t, i),
                        this.lineTo(t + o, i),
                        this.lineTo(t + o, i + e),
                        this.lineTo(t, i + e),
                        this.closePath(),
                        this.fill(),
                        this.currentPath_ = n
                }
                ,
                V.createLinearGradient = function(t, i, o, e) {
                    var n = new C("gradient");
                    return n.x0_ = t,
                        n.y0_ = i,
                        n.x1_ = o,
                        n.y1_ = e,
                        n
                }
                ,
                V.createRadialGradient = function(t, i, o, e, n, s) {
                    var a = new C("gradientradial");
                    return a.x0_ = t,
                        a.y0_ = i,
                        a.r0_ = o,
                        a.x1_ = e,
                        a.y1_ = n,
                        a.r1_ = s,
                        a
                }
                ,
                V.drawImage = function(t) {
                    var i, o, e, n, s, a, r, p, h = t.runtimeStyle.width, l = t.runtimeStyle.height;
                    t.runtimeStyle.width = "auto",
                        t.runtimeStyle.height = "auto";
                    var c = t.width
                        , d = t.height;
                    if (t.runtimeStyle.width = h,
                        t.runtimeStyle.height = l,
                    3 == arguments.length)
                        i = arguments[1],
                            o = arguments[2],
                            s = a = 0,
                            r = e = c,
                            p = n = d;
                    else if (5 == arguments.length)
                        i = arguments[1],
                            o = arguments[2],
                            e = arguments[3],
                            n = arguments[4],
                            s = a = 0,
                            r = c,
                            p = d;
                    else {
                        if (9 != arguments.length)
                            throw Error("Invalid number of arguments");
                        s = arguments[1],
                            a = arguments[2],
                            r = arguments[3],
                            p = arguments[4],
                            i = arguments[5],
                            o = arguments[6],
                            e = arguments[7],
                            n = arguments[8]
                    }
                    var u = T(this, i, o)
                        , m = []
                        , f = 10
                        , v = 10;
                    if (m.push(" <g_vml_:group", ' coordsize="', Z * f, ",", Z * v, '"', ' coordorigin="0,0"', ' style="width:', f, "px;height:", v, "px;position:absolute;"),
                    1 != this.m_[0][0] || this.m_[0][1] || 1 != this.m_[1][1] || this.m_[1][0]) {
                        var g = [];
                        g.push("M11=", this.m_[0][0], ",", "M12=", this.m_[1][0], ",", "M21=", this.m_[0][1], ",", "M22=", this.m_[1][1], ",", "Dx=", R(u.x / Z), ",", "Dy=", R(u.y / Z), "");
                        var y = u
                            , A = T(this, i + e, o)
                            , x = T(this, i, o + n)
                            , w = T(this, i + e, o + n);
                        y.x = L.max(y.x, A.x, x.x, w.x),
                            y.y = L.max(y.y, A.y, x.y, w.y),
                            m.push("padding:0 ", R(y.x / Z), "px ", R(y.y / Z), "px 0;filter:progid:DXImageTransform.Microsoft.Matrix(", g.join(""), ", sizingmethod='clip');")
                    } else
                        m.push("top:", R(u.y / Z), "px;left:", R(u.x / Z), "px;");
                    m.push(' ">', '<g_vml_:image src="', t.src, '"', ' style="width:', Z * e, "px;", " height:", Z * n, 'px"', ' cropleft="', s / c, '"', ' croptop="', a / d, '"', ' cropright="', (c - s - r) / c, '"', ' cropbottom="', (d - a - p) / d, '"', " />", "</g_vml_:group>"),
                        this.element_.insertAdjacentHTML("BeforeEnd", m.join(""))
                }
                ,
                V.stroke = function(t) {
                    var i = []
                        , o = 10
                        , e = 10;
                    i.push("<g_vml_:shape", ' filled="', !!t, '"', ' style="position:absolute;width:', o, "px;height:", e, 'px;"', ' coordorigin="0,0"', ' coordsize="', Z * o, ",", Z * e, '"', ' stroked="', !t, '"', ' path="');
                    for (var n = {
                        x: null,
                        y: null
                    }, s = {
                        x: null,
                        y: null
                    }, a = 0; a < this.currentPath_.length; a++) {
                        var r, p = this.currentPath_[a];
                        switch (p.type) {
                            case "moveTo":
                                r = p,
                                    i.push(" m ", R(p.x), ",", R(p.y));
                                break;
                            case "lineTo":
                                i.push(" l ", R(p.x), ",", R(p.y));
                                break;
                            case "close":
                                i.push(" x "),
                                    p = null;
                                break;
                            case "bezierCurveTo":
                                i.push(" c ", R(p.cp1x), ",", R(p.cp1y), ",", R(p.cp2x), ",", R(p.cp2y), ",", R(p.x), ",", R(p.y));
                                break;
                            case "at":
                            case "wa":
                                i.push(" ", p.type, " ", R(p.x - this.arcScaleX_ * p.radius), ",", R(p.y - this.arcScaleY_ * p.radius), " ", R(p.x + this.arcScaleX_ * p.radius), ",", R(p.y + this.arcScaleY_ * p.radius), " ", R(p.xStart), ",", R(p.yStart), " ", R(p.xEnd), ",", R(p.yEnd))
                        }
                        p && ((null == n.x || p.x < n.x) && (n.x = p.x),
                        (null == s.x || p.x > s.x) && (s.x = p.x),
                        (null == n.y || p.y < n.y) && (n.y = p.y),
                        (null == s.y || p.y > s.y) && (s.y = p.y))
                    }
                    i.push(' ">'),
                        t ? b(this, i, n, s) : _(this, i),
                        i.push("</g_vml_:shape>"),
                        this.element_.insertAdjacentHTML("beforeEnd", i.join(""))
                }
                ,
                V.fill = function() {
                    this.stroke(!0)
                }
                ,
                V.closePath = function() {
                    this.currentPath_.push({
                        type: "close"
                    })
                }
                ,
                V.save = function() {
                    var t = {};
                    h(this, t),
                        this.aStack_.push(t),
                        this.mStack_.push(this.m_),
                        this.m_ = p(r(), this.m_)
                }
                ,
                V.restore = function() {
                    this.aStack_.length && (h(this.aStack_.pop(), this),
                        this.m_ = this.mStack_.pop())
                }
                ,
                V.translate = function(t, i) {
                    var o = [[1, 0, 0], [0, 1, 0], [t, i, 1]];
                    k(this, p(o, this.m_), !1)
                }
                ,
                V.rotate = function(t) {
                    var i = Y(t)
                        , o = I(t)
                        , e = [[i, o, 0], [-o, i, 0], [0, 0, 1]];
                    k(this, p(e, this.m_), !1)
                }
                ,
                V.scale = function(t, i) {
                    this.arcScaleX_ *= t,
                        this.arcScaleY_ *= i;
                    var o = [[t, 0, 0], [0, i, 0], [0, 0, 1]];
                    k(this, p(o, this.m_), !0)
                }
                ,
                V.transform = function(t, i, o, e, n, s) {
                    var a = [[t, i, 0], [o, e, 0], [n, s, 1]];
                    k(this, p(a, this.m_), !0)
                }
                ,
                V.setTransform = function(t, i, o, e, n, s) {
                    var a = [[t, i, 0], [o, e, 0], [n, s, 1]];
                    k(this, a, !0)
                }
                ,
                V.drawText_ = function(t, i, e, n, s) {
                    var a = this.m_
                        , r = 1e3
                        , p = 0
                        , h = r
                        , l = {
                        x: 0,
                        y: 0
                    }
                        , c = []
                        , d = g(v(this.font), this.element_)
                        , u = y(d)
                        , m = this.element_.currentStyle
                        , f = this.textAlign.toLowerCase();
                    switch (f) {
                        case "left":
                        case "center":
                        case "right":
                            break;
                        case "end":
                            f = "ltr" == m.direction ? "right" : "left";
                            break;
                        case "start":
                            f = "rtl" == m.direction ? "right" : "left";
                            break;
                        default:
                            f = "left"
                    }
                    switch (this.textBaseline) {
                        case "hanging":
                        case "top":
                            l.y = d.size / 1.75;
                            break;
                        case "middle":
                            break;
                        default:
                        case null:
                        case "alphabetic":
                        case "ideographic":
                        case "bottom":
                            l.y = -d.size / 2.25
                    }
                    switch (f) {
                        case "right":
                            p = r,
                                h = .05;
                            break;
                        case "center":
                            p = h = r / 2
                    }
                    var A = T(this, i + l.x, e + l.y);
                    c.push('<g_vml_:line from="', -p, ' 0" to="', h, ' 0.05" ', ' coordsize="100 100" coordorigin="0 0"', ' filled="', !s, '" stroked="', !!s, '" style="position:absolute;width:1px;height:1px;">'),
                        s ? _(this, c) : b(this, c, {
                            x: -p,
                            y: 0
                        }, {
                            x: h,
                            y: d.size
                        });
                    var x = a[0][0].toFixed(3) + "," + a[1][0].toFixed(3) + "," + a[0][1].toFixed(3) + "," + a[1][1].toFixed(3) + ",0,0"
                        , w = R(A.x / Z) + "," + R(A.y / Z);
                    c.push('<g_vml_:skew on="t" matrix="', x, '" ', ' offset="', w, '" origin="', p, ' 0" />', '<g_vml_:path textpathok="true" />', '<g_vml_:textpath on="true" string="', o(t), '" style="v-text-align:', f, ";font:", o(u), '" /></g_vml_:line>'),
                        this.element_.insertAdjacentHTML("beforeEnd", c.join(""))
                }
                ,
                V.fillText = function(t, i, o, e) {
                    this.drawText_(t, i, o, e, !1)
                }
                ,
                V.strokeText = function(t, i, o, e) {
                    this.drawText_(t, i, o, e, !0)
                }
                ,
                V.measureText = function(t) {
                    if (!this.textMeasureEl_) {
                        var i = '<span style="position:absolute;top:-20000px;left:0;padding:0;margin:0;border:none;white-space:pre;"></span>';
                        this.element_.insertAdjacentHTML("beforeEnd", i),
                            this.textMeasureEl_ = this.element_.lastChild
                    }
                    var o = this.element_.ownerDocument;
                    this.textMeasureEl_.innerHTML = "";
                    try {
                        this.textMeasureEl_.style.font = this.font
                    } catch (e) {}
                    return this.textMeasureEl_.appendChild(o.createTextNode(t)),
                        {
                            width: this.textMeasureEl_.offsetWidth
                        }
                }
                ,
                V.clip = function() {}
                ,
                V.arcTo = function() {}
                ,
                V.createPattern = function(t, i) {
                    return new N(t,i)
                }
                ,
                C.prototype.addColorStop = function(t, i) {
                    i = f(i),
                        this.colors_.push({
                            offset: t,
                            color: i.color,
                            alpha: i.alpha
                        })
                }
            ;
            var K = D.prototype = new Error;
            K.INDEX_SIZE_ERR = 1,
                K.DOMSTRING_SIZE_ERR = 2,
                K.HIERARCHY_REQUEST_ERR = 3,
                K.WRONG_DOCUMENT_ERR = 4,
                K.INVALID_CHARACTER_ERR = 5,
                K.NO_DATA_ALLOWED_ERR = 6,
                K.NO_MODIFICATION_ALLOWED_ERR = 7,
                K.NOT_FOUND_ERR = 8,
                K.NOT_SUPPORTED_ERR = 9,
                K.INUSE_ATTRIBUTE_ERR = 10,
                K.INVALID_STATE_ERR = 11,
                K.SYNTAX_ERR = 12,
                K.INVALID_MODIFICATION_ERR = 13,
                K.NAMESPACE_ERR = 14,
                K.INVALID_ACCESS_ERR = 15,
                K.VALIDATION_ERR = 16,
                K.TYPE_MISMATCH_ERR = 17,
                G_vmlCanvasManager = B,
                CanvasRenderingContext2D = x,
                CanvasGradient = C,
                CanvasPattern = N,
                DOMException = D
        }()
    }
    , function(t, i, o) {
        var e = o(4)
            , n = o(23)
            , s = o(33)
            , a = o(36)
            , r = o(42)
            , p = o(45)
            , h = o(48)
            , l = o(51)
            , c = o(54)
            , d = o(58)
            , u = o(98)
            , m = o(102)
            , f = o(128)
            , v = o(137)
            , g = o(141)
            , y = o(144)
            , A = o(157)
            , x = o(161)
            , w = o(166)
            , _ = o(170);
        o(173),
            window.EmchartsMobileTime = e,
            window.EmchartsMobileK = n,
            window.EmchartsMobiLine = s,
            window.ChartMobiBar = a,
            window.ChartMobiGroupBar = r,
            window.EmchartsWebBarQuarter = h,
            window.EmchartsWebLineQuarter = l,
            window.EmchartsWebLineRate = p,
            window.EmchartsWebLine = c,
            window.EmchartsWebTime = d,
            window.EmchartsWebTimeSimple = u,
            window.EmchartsWebK = m,
            window.EmchartsWebKSimple = f,
            window.EmchartsWebBar = v,
            window.EmchartsWebGroupBar = g,
            window.EmchartsPie = y,
            window.EmchartsWebHorizontalBar = A,
            window.EmchartsWebYCategoryStack = x,
            window.EmchartsWebHorizontalGroupBar = w,
            window.EmchartsBarLine = _,
            window.EmchartsVersion = {
                version: "2.5.9",
                date: "2017-06-09"
            }
    }
    , function(t, i, o) {
        var e = o(5)
            , n = o(7)
            , s = o(10)
            , a = o(15)
            , r = o(17)
            , p = o(18)
            , h = o(19)
            , l = o(6)
            , c = o(20)
            , d = o(22)
            , u = function() {
            function t(t) {
                this.defaultoptions = n.chartTime,
                    this.options = {},
                    l(!0, this.options, n.defaulttheme, this.defaultoptions, t),
                    this.container = document.getElementById(t.container),
                    this.container.className = this.container.className.replace(/emcharts-container/g, "").trim(),
                    this.container.className = this.container.className + " emcharts-container",
                    this.onChartLoaded = void 0 == t.onChartLoaded ? function() {}
                        : t.onChartLoaded
            }
            function i(t) {
                this.options.data = null == t ? this.options.data : t;
                var i = this.options.interactive;
                try {
                    this.options.pricedigit = t.pricedigit || 2;
                    var o = h.get_rect.apply(this, [this.options.canvas, this.options.data.total]);
                    this.options.rect_unit = o;
                    var n = new e(this.options);
                    t && t.data && t.data.length > 0 && (new a(this.options),
                        new r(this.options)),
                        n.drawYMark(),
                    this.options.showV === !0 && new p(this.options),
                        i.hideLoading(),
                        this.onChartLoaded(this)
                } catch (s) {
                    i.hideLoading()
                }
                var l = this.options.dpr;
                return d.apply(this, [this.options.context, 85 * l, 10 * l, 82 * l, 20 * l]),
                    !0
            }
            function o(t) {
                var i, o = this, e = t.canvas, n = this.options.interactive, s = !1, a = this.options.delaytouch = !0;
                e.addEventListener("touchstart", function(t) {
                    a ? (s = !1,
                        i = setTimeout(function() {
                            s = !0,
                                n.show(),
                                u.apply(o, [n, t.changedTouches[0]]),
                                t.preventDefault()
                        }, 200)) : (n.show(),
                        u.apply(o, [n, t.changedTouches[0]])),
                    o.options.preventdefault && t.preventDefault()
                }),
                    e.addEventListener("touchmove", function(t) {
                        a ? (clearTimeout(i),
                        s && (u.apply(o, [n, t.changedTouches[0]]),
                            t.preventDefault())) : u.apply(o, [n, t.changedTouches[0]]),
                        o.options.preventdefault && t.preventDefault()
                    }),
                    e.addEventListener("touchend", function(t) {
                        a && clearTimeout(i),
                            n.hide(),
                        o.options.preventdefault && t.preventDefault()
                    }),
                    e.addEventListener("touchcancel", function(t) {
                        a && clearTimeout(i),
                            n.hide(),
                        o.options.preventdefault && t.preventDefault()
                    }),
                    e.addEventListener("mousemove", function(t) {
                        u.apply(o, [n, t]),
                            t.preventDefault()
                    }),
                    e.addEventListener("mouseleave", function(t) {
                        n.hide(),
                            t.preventDefault()
                    }),
                    e.addEventListener("mouseenter", function(t) {
                        u.apply(o, [n, t]),
                            n.show(),
                            t.preventDefault()
                    })
            }
            function u(t, i) {
                var o = this.options.canvas
                    , e = this.options.data.data
                    , n = this.options.rect_unit
                    , s = n.rect_w
                    , a = this.options.padding
                    , r = i.offsetX || i.clientX - this.container.getBoundingClientRect().left
                    , p = i.offsetY || i.clientY - this.container.getBoundingClientRect().top
                    , l = h.windowToCanvas.apply(this, [o, r, p])
                    , c = l.x.toFixed(0)
                    , d = Math.floor((c - a.left) / s);
                if (e && e[d]) {
                    t.showTip(o, r, e[d]);
                    var u = h.canvasToWindow.apply(this, [o, e[d].cross_x, e[d].cross_y])
                        , m = u.x
                        , f = u.y;
                    t.cross(o, m, f)
                }
            }
            return t.prototype.init = function() {
                this.options.type || (this.options.type = "T1");
                var t = document.createElement("canvas");
                this.container.style.position = "relative";
                var i = t.getContext("2d");
                this.options.canvas = t,
                    this.options.context = i;
                var o = this.options.dpr;
                t.width = this.options.width * o,
                    t.height = this.options.height * o,
                    this.options.sepeNum = 6.5,
                void 0 === this.options.showV && (this.options.showV = !0),
                    this.options.canvas_offset_top = 2,
                    this.options.padding_left = 0,
                    this.options.k_v_away = t.height / this.options.sepeNum,
                    this.options.scale_count = void 0 == this.options.scale_count ? 0 : this.options.scale_count,
                    this.options.c_1_height = this.options.showV ? 4 * t.height / this.options.sepeNum : t.height - 90 * o,
                    this.options.padding = {},
                    this.options.padding.left = 0,
                    this.options.padding.right = 2,
                    this.options.padding.top = 2,
                    this.options.padding.bottom = 0,
                    this.options.unit = {},
                    this.options.unit.unitHeight = t.height / this.options.sepeNum,
                    this.options.y_sep = this.options.y_sep || 5,
                    this.options.x_sep = this.options.x_sep || 4,
                    "t2" == this.options.type.toLowerCase() ? this.options.x_sep = 2 : "t3" == this.options.type.toLowerCase() ? this.options.x_sep = 3 : "t4" == this.options.type.toLowerCase() ? this.options.x_sep = 4 : "t5" == this.options.type.toLowerCase() && (this.options.x_sep = 5),
                    t.style.width = this.options.width + "px",
                    t.style.height = this.options.height + "px",
                    t.style.border = "0",
                    i.translate("0", this.options.canvas_offset_top),
                    i.font = this.options.font_size * this.options.dpr + "px Arial",
                    i.lineWidth = 1 * this.options.dpr,
                    this.options.enableHandle = void 0 == this.options.enableHandle ? !0 : this.options.enableHandle,
                    this.container.appendChild(t)
            }
                ,
                t.prototype.draw = function(t) {
                    this.clear(),
                        this.init();
                    var e = this.options.interactive = new c(this.options);
                    e.showLoading();
                    var n = this;
                    try {
                        s({
                            id: this.options.code,
                            type: this.options.type
                        }, function(e) {
                            e ? i.apply(n, [e]) : i.apply(n, [[]]),
                            n.options.enableHandle && o.call(n, n.options.context),
                            t && t()
                        }, e)
                    } catch (a) {
                        e.showNoData(),
                            e.hideLoading()
                    }
                }
                ,
                t.prototype.reDraw = function() {
                    this.clear(),
                        this.init(),
                        i.call(this)
                }
                ,
                t.prototype.clear = function(t) {
                    this.container ? this.container.innerHTML = "" : document.getElementById(this.options.container).innerHTML = "",
                    t && t()
                }
                ,
                t
        }();
        t.exports = u
    }
    , function(t, i, o) {
        var e = o(6)
            , n = o(7)
            , s = o(8)
            , a = function() {
            function t(t) {
                this.defaultoptions = n.draw_xy,
                    this.options = {},
                    e(!1, this.options, this.defaultoptions, t),
                    this.draw()
            }
            function i(t, i, o, e) {
                var n = this
                    , s = this.options.padding
                    , r = (this.options.c_1_height,
                t.canvas.width - s.left - s.right,
                    e.length);
                t.save(),
                    t.strokeStyle = "#e5e5e5";
                for (var p, h = 0; p = e[h]; h++)
                    t.beginPath(),
                        t.fillStyle = (r - 1) / 2 > h ? "#19AF43" : h > (r - 1) / 2 ? "#D53D25" : "#333333",
                        t.textBaseline = 0 == h ? "bottom" : h == r - 1 ? "top" : "middle",
                        isNaN(p.num) ? t.fillText("0.00", 5, p.y) : t.fillText(p.num.toFixed(this.options.pricedigit), 5, p.y),
                        a.call(n, t, i, o, p);
                t.restore()
            }
            function o(t, i, o, e) {
                {
                    var n = this.options.padding
                        , a = this.options.c_1_height
                        , r = t.canvas.width - n.left - n.right;
                    e.length
                }
                t.save(),
                    t.strokeStyle = "#e5e5e5";
                for (var p, h = 0; p = e[h]; h++)
                    t.beginPath(),
                        h % 2 == 1 ? s(t, 0, Math.round(p.y), t.canvas.width, Math.round(p.y), 5) : (t.moveTo(0, Math.round(p.y)),
                            t.lineTo(t.canvas.width, Math.round(p.y)),
                            t.stroke());
                t.rect(1, 0, r, a),
                    t.stroke(),
                    t.restore()
            }
            function a(t, i, o, e) {
                var n = (i + o) / 2
                    , s = t.canvas.width;
                if (n)
                    var a = ((e.num - n) / n * 100).toFixed(2) + "%";
                else
                    var a = "0.00%";
                t.fillText(a, s - t.measureText(a).width - 5, e.y)
            }
            function r(t, i, o, e) {
                var n = this.options.padding.left;
                t.save(),
                    t.beginPath(),
                    t.fillStyle = "#A0A0A0",
                    t.textBaseline = "middle";
                var a = t.canvas.width - n
                    , r = i + t.canvas.height / 8 * 1 / 3
                    , p = o.length
                    , h = 0;
                if ("t1" == this.options.type.toLowerCase())
                    for (var l = 0; p > l; l++)
                        if (0 == l)
                            t.fillText(o[l].str, n, r);
                        else if (l == p - 1)
                            t.fillText(o[l].str, a - t.measureText(o[l].str).width, r);
                        else {
                            var c = a * (o[l].tick / e) + n - t.measureText(o[l].str).width / 2
                                , d = a * (o[p - 1].tick / e) + n - t.measureText(o[p - 1].str).width
                                , u = h + t.measureText(o[l].str).width;
                            if (u + 10 > c || c + t.measureText(o[l].str).width + 10 > d)
                                continue;
                            h = c,
                                t.fillText(o[l].str, c, r)
                        }
                else
                    for (var m = (a - n) / p, f = 0; p > f; f++)
                        t.fillText(o[f], (f + 1) * m - m / 2 - t.measureText(o[f]).width / 2, r);
                var v = this.options.x_sep
                    , g = a / v;
                t.strokeStyle = "#e5e5e5";
                for (var f = 1; v > f; f++) {
                    var y = !1;
                    t.beginPath(),
                        "t1" == this.options.type.toLowerCase() && f == v / 2 ? y = !0 : "t2" == this.options.type.toLowerCase() ? (this.options.x_sep = 2,
                        1 == f && (y = !0)) : "t3" == this.options.type.toLowerCase() ? this.options.x_sep = 3 : "t4" == this.options.type.toLowerCase() ? (this.options.x_sep = 4,
                        2 == f && (y = !0)) : "t5" == this.options.type.toLowerCase() ? this.options.x_sep = 5 : y = !1,
                        y ? (t.strokeStyle = "#e5e5e5",
                            t.moveTo(n + g * f, 0),
                            t.lineTo(n + g * f, i),
                            t.stroke()) : s(t, Math.round(n + g * f), 0, Math.round(n + g * f), i, 5)
                }
                t.restore()
            }
            function p(t, i, o, e) {
                for (var n = (t - i) / (o - 1), s = [], a = 0; o > a; a++)
                    s.push({
                        num: i + a * n,
                        x: 0,
                        y: e - a / (o - 1) * e
                    });
                return s
            }
            return t.prototype.draw = function() {
                var t = this.options.data
                    , i = this.options.context
                    , e = t.max
                    , n = t.min
                    , s = this.options.y_sep
                    , a = t.timeStrs
                    , h = this.options.c_1_height
                    , l = p(e, n, s, h);
                this.options.line_list_array = l,
                    o.call(this, i, e, n, l),
                a && r.apply(this, [i, h, a, t.total])
            }
                ,
                t.prototype.drawYMark = function() {
                    var t = this.options.data
                        , o = this.options.context
                        , e = t.max
                        , n = t.min
                        , s = this.options.line_list_array;
                    i.call(this, o, e, n, s)
                }
                ,
                t
        }();
        t.exports = a
    }
    , function(t) {
        var i = Object.prototype.hasOwnProperty
            , o = Object.prototype.toString
            , e = function(t) {
            return "function" == typeof Array.isArray ? Array.isArray(t) : "[object Array]" === o.call(t)
        }
            , n = function(t) {
            if (!t || "[object Object]" !== o.call(t))
                return !1;
            var e = i.call(t, "constructor")
                , n = t.constructor && t.constructor.prototype && i.call(t.constructor.prototype, "isPrototypeOf");
            if (t.constructor && !e && !n)
                return !1;
            var s;
            for (s in t)
                ;
            return "undefined" == typeof s || i.call(t, s)
        };
        t.exports = function s() {
            var t, i, o, a, r, p, h = arguments[0], l = 1, c = arguments.length, d = !1;
            for ("boolean" == typeof h ? (d = h,
                h = arguments[1] || {},
                l = 2) : ("object" != typeof h && "function" != typeof h || null == h) && (h = {}); c > l; ++l)
                if (t = arguments[l],
                null != t)
                    for (i in t)
                        o = h[i],
                            a = t[i],
                        h !== a && (d && a && (n(a) || (r = e(a))) ? (r ? (r = !1,
                            p = o && e(o) ? o : []) : p = o && n(o) ? o : {},
                            h[i] = s(d, p, a)) : "undefined" != typeof a && (h[i] = a));
            return h
        }
    }
    , function(t) {
        var i = {
            defaulttheme: {
                spacing: .4,
                padding_left: 30,
                k_v_away: 30,
                canvas_offset_top: 40,
                point_width: 6,
                font_size: 12,
                point_color: "#8f8f8f",
                up_color: "#e30000",
                down_color: "#007130"
            },
            chartTime: {
                crossline: !0
            },
            chartK: {
                crossline: !1
            },
            chartLine: {
                showPoint: !1,
                canvasPaddingTop: 10,
                canvasPaddingLeft: 20,
                pointRadius: 10,
                lineMarkWidth: 15
            },
            draw_xy: {
                axis_color: "#fff",
                y_max: 100,
                y_min: 0,
                sepe_num: 5,
                y_padding_per: .05,
                date_offset_top: 15
            },
            draw_xy_web: {
                spacing: .4,
                padding_left: 30,
                k_v_away: 30,
                canvas_offset_top: 40,
                point_width: 5,
                font_size: 12,
                point_color: "#8f8f8f",
                up_color: "#e30000",
                down_color: "#007130",
                axis_color: "#fff",
                y_max: 100,
                y_min: 0,
                sepe_num: 9,
                y_padding_per: .05,
                date_offset_top: 15,
                crossline: !0
            },
            draw_line: {
                avg_cost_color: "#DAA520"
            },
            draw_k: {},
            draw_ma: {},
            draw_v: {},
            interactive: {}
        };
        t.exports = i
    }
    , function(t, i, o) {
        function e(t, i, o, e, s, a) {
            t.save();
            for (var r = void 0 === a ? 5 : a, p = e - i, h = s - o, l = Math.floor(Math.sqrt(p * p + h * h) / r), c = 0; l > c; c++)
                c % 2 === 0 ? t.moveTo(n(i + p / l * c), n(o + h / l * c)) : t.lineTo(n(i + p / l * c), n(o + h / l * c));
            t.stroke(),
                t.restore()
        }
        var n = o(9);
        t.exports = e
    }
    , function(t) {
        function i(t) {
            if (isNaN(t))
                return t;
            var i = Math.floor(t);
            return i + .5
        }
        t.exports = i
    }
    , function(t, i, o) {
        function e(t, i, o) {
            var e = "http://pdfm2.eastmoney.com/EM_UBG_PDTI_Fast/api/js"
                , a = "fsdata"
                , r = t.type.toLowerCase();
            "t1" === r && (r = "r");
            var p = {
                id: t.id,
                TYPE: r,
                js: a + "((x))",
                rtntype: 5,
                isCR: !1
            };
            s(e, p, a, function(e) {
                try {
                    if (e) {
                        var s = n(e, r, t.id);
                        i(s)
                    } else
                        i(null)
                } catch (a) {
                    o.showNoData(),
                        o.hideLoading()
                }
            })
        }
        var n = o(11)
            , s = o(14);
        t.exports = e
    }
    , function(t, i, o) {
        function e(t) {
            var i = Math.floor(t / 60 / 60)
                , o = t / 60 % 60;
            return s(i % 24, 2) + ":" + s(o, 2)
        }
        var n = o(12)
            , s = o(13).fixed
            , a = function(t, i, o) {
            var s = (o.charAt(o.length - 1),
                t.info)
                , a = s.ticks.split("|")
                , r = s.yc
                , p = 0
                , h = s.jys;
            s.pricedigit.split(".").length > 1 && (p = 0 == s.pricedigit.split(".")[1].length ? 2 : s.pricedigit.split(".")[1].length);
            var l = 0
                , c = r
                , d = 0
                , u = ""
                , m = []
                , f = t.data
                , v = {};
            v.name = t.name,
                v.pricedigit = p,
                v.yc = r,
                v.data = [],
                v.total = s.total,
                v.timeStrs = [];
            for (var g = 0; g < f.length; g++) {
                var y = f[g].split(",");
                if (u !== y[0].split(" ")[0]) {
                    var A = y[0].split(" ")[0].split("-");
                    u = y[0].split(" ")[0],
                        m.push(A[1] + "/" + A[2])
                }
                if (d = d > Number(y[2]) ? d : Number(y[2]),
                y[1] >= y[3])
                    var x = y[1]
                        , w = y[3];
                else
                    var w = y[1]
                        , x = y[3];
                l = Math.max(l, x),
                    c = Math.min(c, w);
                var _ = {};
                _.time = y[0].split(" ")[1],
                    _.price = y[1],
                    0 != g ? (_.percent = 0 == r ? 0 : ((y[1] - r) / r * 100).toFixed(2),
                        _.up = y[1] - r > 0 ? !0 : !1) : (_.percent = 0 == r ? 0 : ((y[1] - r) / r * 100).toFixed(2),
                        _.up = y[1] - r > 0 ? !0 : !1),
                    _.volume = Number(Number(y[2]).toFixed(0)),
                    _.avg_cost = y[3],
                    v.data.push(_)
            }
            var b = 0
                , T = []
                , M = a.slice(3);
            for (g = 0,
                     len = M.length; len > g; g += 2) {
                var k = b;
                b += (M[g + 1] - M[g]) / 60 + 1,
                    0 !== g ? (T.push({
                        str: e(M[g - 1]) + "/" + e(M[g]),
                        tick: k
                    }),
                    g === len - 2 && T.push({
                        str: e(M[g + 1]),
                        tick: b
                    })) : (T.push({
                        str: e(M[g]),
                        tick: 0
                    }),
                    g === len - 2 && T.push({
                        str: e(M[g + 1]),
                        tick: b
                    }))
            }
            return 2 != h && 6 != h && 13 != h && 80 != h && (T = [],
                T.push({
                    str: e(M[0]),
                    tick: 0
                }),
                T.push({
                    str: e(M[M.length - 1]),
                    tick: b
                })),
                "r" === i ? v.timeStrs = [].concat(T) : (v.timeStrs = m,
                i.match(/[0-9]/)[0] < m.length && (v.timeStrs = m.slice(1))),
                v.total = b,
            i.match(/[0-9]/) && (v.total = b * i.match(/[0-9]/)[0]),
                v.max = n(l, c, r).max,
                v.min = n(l, c, r).min,
                v
        };
        t.exports = a
    }
    , function(t) {
        function i(t, i, o) {
            var e = 0
                , n = 0;
            o = parseFloat(o) || 0;
            var s = Math.max(Math.abs(o - t), Math.abs(o - i));
            return e = Number(o) + 1.05 * s,
                n = Number(o) - 1.05 * s,
            t == i && i == o && (e = 1.08 * o,
                n = .92 * o),
            0 == o && (e = 0,
                n = 0),
                {
                    max: e,
                    min: n
                }
        }
        t.exports = i
    }
    , function(t) {
        var i = {
            getMktByCode: function(t) {
                if (t.Length < 3)
                    return t + "1";
                var i = t.substr(0, 1)
                    , o = t.substr(0, 3);
                return "5" == i || "6" == i || "9" == i ? t + "1" : "009" == o || "126" == o || "110" == o || "201" == o || "202" == o || "203" == o || "204" == o ? t + "1" : t + "2"
            },
            fixed: function(t, i) {
                var o = 0;
                t = t.toString();
                var e = t;
                for (o = 0; o < i - t.length; o++)
                    e = "0" + e;
                return e
            },
            transform: function(t) {
                return t.replace(/(\d{4})(\d{2})(\d{2})/g, function(t, i, o, e) {
                    return i + "-" + o + "-" + e
                })
            },
            windowToCanvas: function(t, i, o) {
                return {
                    x: i * this.options.dpr,
                    y: o * this.options.dpr
                }
            },
            canvasToWindow: function(t, i, o) {
                var e = t.getBoundingClientRect();
                return {
                    x: i / this.options.dpr,
                    y: (o + this.options.canvas_offset_top) * (e.height / t.height)
                }
            },
            get_y: function(t) {
                var i = this.options.c_1_height / 1
                    , o = this.options.data.max / 1
                    , e = this.options.data.min / 1;
                return o == e ? i / 2 : i - i * (t - e) / (o - e)
            },
            get_x: function(t) {
                var i = this.options.context.canvas
                    , o = this.options.type
                    , e = this.options.rect_unit.rect_w
                    , n = this.options.data.data.length
                    , s = this.options.data.total
                    , a = this.options.padding_left;
                return "TL" == o ? (i.width - a) / s * t + a : (i.width - a) / n * t + a - e / 2
            },
            get_rect: function(t, i) {
                var o = (t.width - this.options.padding_left) / i
                    , e = o * (1 - this.options.spacing);
                return {
                    rect_w: o,
                    bar_w: e
                }
            },
            format_unit: function(t, i) {
                i = void 0 == i ? 2 : i;
                var o = !1;
                0 > t && (t = Math.abs(t),
                    o = !0);
                var e = 0
                    , n = "";
                return o ? (1e4 > t ? e = t / 1 : t >= 1e4 && 1e8 > t ? (e = t / 1e4,
                    n = "万") : t >= 1e8 ? (e = t / 1e8,
                    n = "亿") : e = t / 1,
                    e = -1 * e) : 1e4 > t ? e = t / 1 : t >= 1e4 && 1e8 > t ? (e = t / 1e4,
                    n = "万") : t >= 1e8 ? (e = t / 1e8,
                    n = "亿") : e = t / 1,
                (e / 1).toFixed(i) / 1 == (e / 1).toFixed(0) / 1 && (i = 0),
                parseFloat((e / 1).toFixed(i)) + n
            },
            translate: function(t) {
                function i(t) {
                    return 100 > t ? t.toFixed(2) : Math.round(t)
                }
                var o = (t + "").length;
                return 4 >= o ? t : 9 > o ? i(1 * t / 1e4) + "万" : 13 > o ? i(1 * t / Math.pow(10, 8)) + "亿" : 16 >= o ? i(1 * t / Math.pow(10, 12)) + "万亿" : void 0
            },
            addEvent: function(t, i, o) {
                t.attachEvent ? (t["e" + i + o] = o,
                    t[i + o] = function() {
                        t["e" + i + o](window.event)
                    }
                    ,
                    t.attachEvent("on" + i, t[i + o])) : t.addEventListener(i, o, !1)
            },
            removeEvent: function(t, i, o) {
                t.detachEvent ? (t.detachEvent("on" + i, t[i + o]),
                    t[i + o] = null) : t.removeEventListener(i, o, !1)
            }
        };
        t.exports = i
    }
    , function(t) {
        var i = function(t, i, o, e) {
            t = t || "",
                i = i || {},
                o = o || "",
                e = e || function() {}
            ;
            var n = function(t) {
                var i = [];
                for (var o in t)
                    t.hasOwnProperty(o) && i.push(o);
                return i
            };
            if ("object" == typeof i) {
                for (var s = "", a = n(i), r = 0; r < a.length; r++)
                    s += encodeURIComponent(a[r]) + "=" + encodeURIComponent(i[a[r]]),
                    r != a.length - 1 && (s += "&");
                t += "?" + s
            } else
                "function" == typeof i && (o = i,
                    e = o);
            "function" == typeof o && (e = o,
                o = "callback"),
            Date.now || (Date.now = function() {
                    return (new Date).getTime()
                }
            );
            var p = Date.now()
                , h = "jsonp" + Math.round(p + 1000001 * Math.random());
            "string" == typeof o && (h = o),
                window[h] = function(t) {
                    e(t);
                    try {
                        document.getElementsByTagName("head")[0].removeChild(l),
                            delete window[h]
                    } catch (i) {
                        window[h] = void 0
                    }
                }
                ,
                t += -1 === t.indexOf("?") ? "?" : "&";
            var l = document.createElement("script");
            l.setAttribute("src", t + o + "=" + h),
                document.getElementsByTagName("head")[0].appendChild(l)
        };
        t.exports = i
    }
    , function(t, i, o) {
        var e = o(6)
            , n = o(7)
            , s = (o(13),
            o(16))
            , a = function() {
            function t(t) {
                this.defaultoptions = n.draw_line,
                    this.options = {},
                    e(!1, this.options, this.defaultoptions, t),
                    this.draw()
            }
            function i(t, i) {
                t.beginPath(),
                    t.save(),
                    t.strokeStyle = "#195F9A";
                for (var o, e = 0; o = i[e]; e++) {
                    var n = s.get_x.call(this, e + 1)
                        , a = s.get_y.call(this, o.price);
                    t.lineTo(n, a),
                        o.cross_x = n,
                        o.cross_y = a
                }
                t.stroke(),
                    t.restore()
            }
            return t.prototype.draw = function() {
                var t = this.options.context
                    , o = this.options.data
                    , e = o.data;
                i.apply(this, [t, e])
            }
                ,
                t
        }();
        t.exports = a
    }
    , function(t) {
        var i = {
            windowToCanvas: function(t, i, o) {
                return {
                    x: i * this.options.dpr,
                    y: o * this.options.dpr
                }
            },
            canvasToWindow: function(t, i, o) {
                var e = t.getBoundingClientRect();
                return {
                    x: i / this.options.dpr,
                    y: (o + this.options.canvas_offset_top) * (e.height / t.height)
                }
            },
            get_y: function(t) {
                return this.options.data.max - this.options.data.min == 0 ? this.options.c_1_height / 2 : this.options.c_1_height - this.options.c_1_height * (t - this.options.data.min) / (this.options.data.max - this.options.data.min)
            },
            get_x: function(t) {
                var i = this.options.context.canvas
                    , o = (this.options.type,
                    this.options.rect_unit.rect_w,
                    this.options.data.data.length,
                    this.options.data.total)
                    , e = this.options.padding_left;
                return (i.width - e - this.options.padding.right) / o * t + e
            }
        };
        t.exports = i
    }
    , function(t, i, o) {
        var e = o(6)
            , n = o(7)
            , s = (o(13),
            o(16))
            , a = function() {
            function t(t) {
                this.defaultoptions = n.draw_line,
                    this.options = {},
                    e(!0, this.options, this.defaultoptions, t),
                    this.draw()
            }
            return t.prototype.draw = function() {
                var t = this.options.context
                    , i = this.options.data;
                this.options.height = t.canvas.height * n.defaulttheme.c_h_percent,
                    this.draw_k(t, i)
            }
                ,
                t.prototype.draw_k = function(t, i) {
                    var o = this.options.avg_cost_color
                        , e = i.data;
                    t.beginPath(),
                        t.lineWidth = 1,
                        t.strokeStyle = o,
                        t.fillStyle = "";
                    for (var n = 0; n < e.length; n++) {
                        var a = s.get_x.call(this, n + 1)
                            , r = s.get_y.call(this, e[n].avg_cost);
                        0 == n ? t.moveTo(a, r) : t.lineTo(a, r)
                    }
                    t.stroke()
                }
                ,
                t
        }();
        t.exports = a
    }
    , function(t, i, o) {
        var e = o(6)
            , n = o(13)
            , s = o(8)
            , a = o(7)
            , r = o(16)
            , p = function() {
            function t(t) {
                this.defaultoptions = a.draw_v,
                    this.options = {},
                    e(!1, this.options, this.defaultoptions, t),
                    this.draw()
            }
            function i() {
                var t = this.options.context
                    , i = this.options.data
                    , e = i.data
                    , n = this.options.padding_left
                    , a = 2 * this.options.unit.unitHeight
                    , h = .9 * a
                    , l = t.canvas.height
                    , c = l - a
                    , d = t.canvas.width - this.options.padding_left
                    , u = this.options.x_sep
                    , m = d / u;
                if (!e || 0 == e.length)
                    return t.beginPath(),
                        t.fillStyle = "#999",
                        t.strokeStyle = "#e5e5e5",
                        t.rect(this.options.padding_left, c, t.canvas.width - this.options.padding_left, a - 2),
                        void t.stroke();
                this.options.data.v_max = p(this.options.data);
                var f = i.v_max.toFixed(0)
                    , v = this.options.rect_unit
                    , g = v.bar_w
                    , y = this.options.up_color
                    , A = this.options.down_color;
                this.options.showVMax === !0 && o(t, f, c),
                    t.beginPath(),
                    t.strokeStyle = "#e5e5e5",
                    t.lineWidth = this.options.dpr,
                    t.rect(this.options.padding_left + 1, c, d - 2, a - 2),
                    t.moveTo(n, c + a / 2),
                    t.lineTo(d, c + a / 2),
                    t.stroke();
                for (var x = 1; u - 1 >= x; x++) {
                    var w = !1;
                    t.beginPath(),
                        "t2" == this.options.type.toLowerCase() ? (this.options.x_sep = 2,
                        1 == x && (w = !0)) : "t3" == this.options.type.toLowerCase() ? this.options.x_sep = 3 : "t4" == this.options.type.toLowerCase() ? (this.options.x_sep = 4,
                        2 == x && (w = !0)) : "t5" == this.options.type.toLowerCase() ? this.options.x_sep = 5 : w = !1,
                        w ? (t.moveTo(n + d / 2, c),
                            t.lineTo(n + d / 2, l),
                            t.stroke()) : s(t, m * x + n, c, m * x + n, l)
                }
                t.lineWidth = 1;
                for (var _, b = 0; _ = e[b]; b++) {
                    var T = _.volume
                        , M = _.up
                        , k = T / f * h
                        , C = r.get_x.call(this, b + 1)
                        , N = l - k;
                    t.beginPath(),
                        t.moveTo(C, N),
                        0 == b ? M ? (t.fillStyle = y,
                            t.strokeStyle = y) : (t.fillStyle = A,
                            t.strokeStyle = A) : _.price >= e[b - 1].price ? (t.fillStyle = y,
                            t.strokeStyle = y) : (t.fillStyle = A,
                            t.strokeStyle = A),
                        t.rect(C - g / 2, N, g, k),
                        t.fill()
                }
            }
            function o(t, i, o) {
                t.beginPath(),
                    t.fillStyle = "#999",
                    t.textBaseline = "top",
                    t.fillText(n.format_unit(i), 0, o + 10),
                    t.stroke()
            }
            function p(t) {
                if (t.data[0])
                    var i = t.data[0].volume;
                else
                    var i = 0;
                for (var o = 0, e = t.data; o < t.data.length; o++)
                    i < e[o].volume && (i = e[o].volume);
                return i
            }
            return t.prototype.draw = function() {
                var t = this.options.context;
                t.beginPath(),
                    t.save(),
                    i.call(this),
                    t.restore()
            }
                ,
                t
        }();
        t.exports = p
    }
    , function(t) {
        var i = {
            getMktByCode: function(t) {
                if (t.Length < 3)
                    return t + "1";
                var i = t.substr(0, 1)
                    , o = t.substr(0, 3);
                return "5" == i || "6" == i || "9" == i ? t + "1" : "009" == o || "126" == o || "110" == o || "201" == o || "202" == o || "203" == o || "204" == o ? t + "1" : t + "2"
            },
            fixed: function(t, i) {
                var o = 0;
                t = t.toString();
                var e = t;
                for (o = 0; o < i - t.length; o++)
                    e = "0" + e;
                return e
            },
            transform: function(t) {
                return t.replace(/(\d{4})(\d{2})(\d{2})/g, function(t, i, o, e) {
                    return i + "-" + o + "-" + e
                })
            },
            windowToCanvas: function(t, i, o) {
                return {
                    x: i * this.options.dpr,
                    y: o * this.options.dpr
                }
            },
            canvasToWindow: function(t, i, o) {
                var e = t.getBoundingClientRect();
                return {
                    x: i / this.options.dpr,
                    y: (o + this.options.canvas_offset_top) * (Math.abs(e.bottom - e.top) / t.height)
                }
            },
            get_y: function(t) {
                var i = this.options.currentData && this.options.currentData.max || this.options.data.max
                    , o = this.options.currentData && this.options.currentData.min || this.options.data.min;
                return this.options.c_k_height - this.options.c_k_height * (t - o) / (i - o)
            },
            get_x: function(t) {
                var i = this.options.context.canvas
                    , o = this.options.chartType
                    , e = this.options.rect_unit.rect_w
                    , n = this.options.currentData && this.options.currentData.data.length || this.options.data.data.length
                    , s = this.options.currentData && this.options.currentData.total || this.options.data.total
                    , a = this.options.padding.left
                    , r = this.options.padding.right;
                return "TL" == o ? (i.width - a - r) / s * t + a : (i.width - a - r) / n * t + a - e / 2
            },
            get_rect: function(t, i) {
                var o = (t.width - this.options.padding.left - this.options.padding.right) / i
                    , e = o * (1 - this.options.spacing);
                return {
                    rect_w: o,
                    bar_w: e
                }
            },
            format_unit: function(t, i) {
                i = void 0 == i ? 2 : i;
                var o = !1;
                0 > t && (t = Math.abs(t),
                    o = !0);
                var e = 0
                    , n = "";
                return o ? (1e4 > t ? e = t / 1 : t >= 1e4 && 1e8 > t ? (e = t / 1e4,
                    n = "万") : t >= 1e8 ? (e = t / 1e8,
                    n = "亿") : e = t / 1,
                    e = -1 * e) : 1e4 > t ? e = t / 1 : t >= 1e4 && 1e8 > t ? (e = t / 1e4,
                    n = "万") : t >= 1e8 ? (e = t / 1e8,
                    n = "亿") : e = t / 1,
                (e / 1).toFixed(i) / 1 == (e / 1).toFixed(0) / 1 && (i = 0),
                (e / 1).toFixed(i) + n
            },
            addEvent: function(t, i, o) {
                t.attachEvent ? (t["e" + i + o] = o,
                    t[i + o] = function() {
                        t["e" + i + o](window.event)
                    }
                    ,
                    t.attachEvent("on" + i, t[i + o])) : t.addEventListener(i, o, !1)
            },
            removeEvent: function(t, i, o) {
                t.detachEvent ? (t.detachEvent("on" + i, t[i + o]),
                    t[i + o] = null) : t.removeEventListener(i, o, !1)
            }
        };
        t.exports = i
    }
    , function(t, i, o) {
        var e = o(21)
            , n = o(13)
            , s = o(7)
            , a = function() {
            function t(t) {
                this.defaultoptions = s.interactive,
                    this.options = e(this.defaultoptions, t)
            }
            return t.prototype.cross = function(t, i, o) {
                var e = (t.getBoundingClientRect(),
                    this.options.dpr);
                if (!this.options.cross) {
                    this.options.cross = {};
                    var n = document.createElement("div");
                    n.className = "cross-y",
                        n.style.height = this.options.showV ? this.options.canvas.height / e + "px" : (this.options.c_1_height + this.options.canvas_offset_top) / e + "px",
                        n.style.top = "0px",
                        this.options.cross.y_line = n;
                    var s = document.createElement("div");
                    s.className = "cross-x",
                        s.style.width = t.width / e + "px",
                        this.options.cross.x_line = s;
                    var a = document.createElement("div");
                    a.className = "cross-p",
                        a.style.width = a.style.height = this.options.point_width + "px",
                        a.style.borderRadius = a.style.width,
                        a.style.backgroundColor = this.options.point_color,
                        this.options.cross.point = a;
                    var r = document.createDocumentFragment();
                    this.options.crossline ? (r.appendChild(s),
                        r.appendChild(n),
                        r.appendChild(a)) : r.appendChild(n),
                        document.getElementById(this.options.container).appendChild(r)
                }
                var n = this.options.cross.y_line;
                n && (n.style.left = i + "px");
                var s = this.options.cross.x_line;
                s && (s.style.top = o + "px");
                var a = this.options.cross.point;
                if (a) {
                    var p = this.options.point_width;
                    a.style.left = i - p / 2 + "px",
                        a.style.top = o - p / 2 + "px"
                }
            }
                ,
                t.prototype.markMA = function(t, i, o, e) {
                    if (this.options.mark_ma) {
                        var n = this.options.mark_ma.mark_ma;
                        this.options.mark_ma.ma_5_data.innerText = i ? "MA5:" + ("-" == i.value ? "-" : (i.value / 1).toFixed(this.options.pricedigit)) : this.default_m5 && "-" != this.default_m5.value ? "MA5:" + (this.default_m5.value / 1).toFixed(this.options.pricedigit) : "MA5: -",
                            this.options.mark_ma.ma_10_data.innerText = o ? "MA10:" + ("-" == o.value ? "-" : (o.value / 1).toFixed(this.options.pricedigit)) : this.default_m10 && "-" != this.default_m10.value ? "MA10:" + (this.default_m10.value / 1).toFixed(this.options.pricedigit) : "MA10: -",
                            this.options.mark_ma.ma_20_data.innerText = e ? "MA20:" + ("-" == e.value ? "-" : (e.value / 1).toFixed(this.options.pricedigit)) : this.default_m20 && "-" != this.default_m20.value ? "MA20:" + (this.default_m20.value / 1).toFixed(this.options.pricedigit) : "MA20: -"
                    } else {
                        this.options.mark_ma = {};
                        var n = document.createElement("div");
                        n.className = "mark-ma",
                            n.style.top = "-1px",
                            this.options.mark_ma.mark_ma = n;
                        var s = document.createDocumentFragment()
                            , a = document.createElement("span");
                        a.className = "span-m5",
                            a.innerText = i ? "MA5:" + ("-" == i.value ? "-" : (i.value / 1).toFixed(this.options.pricedigit)) : this.default_m5 && "-" != this.default_m5.value ? "MA5:" + (this.default_m5.value / 1).toFixed(this.options.pricedigit) : "MA5: -",
                            this.options.mark_ma.ma_5_data = a;
                        var r = document.createElement("span");
                        r.id = "ma_10_data",
                            r.className = "span-m10",
                            r.innerText = o ? "MA10:" + ("-" == o.value ? "-" : (o.value / 1).toFixed(this.options.pricedigit)) : this.default_m10 && "-" != this.default_m10.value ? "MA10:" + (this.default_m10.value / 1).toFixed(this.options.pricedigit) : "MA10: -",
                            this.options.mark_ma.ma_10_data = r;
                        var p = document.createElement("span");
                        p.id = "ma_20_data",
                            p.className = "span-m20",
                            p.innerText = e ? "MA20:" + ("-" == e.value ? "-" : (e.value / 1).toFixed(this.options.pricedigit)) : this.default_m20 && "-" != this.default_m20.value ? "MA20:" + (this.default_m20.value / 1).toFixed(this.options.pricedigit) : "MA20: -",
                            this.options.mark_ma.ma_20_data = p,
                            s.appendChild(a),
                            s.appendChild(r),
                            s.appendChild(p),
                            n.appendChild(s),
                            document.getElementById(this.options.container).appendChild(n),
                        document.body.clientWidth <= 320 && (a.style.fontSize = "10px",
                            r.style.fontSize = "10px",
                            p.style.fontSize = "10px")
                    }
                }
                ,
                t.prototype.scale = function(t) {
                    n.canvasToWindow.apply(this, [t, t.width, this.options.c_1_height]);
                    if (!this.options.scale) {
                        this.options.scale = {};
                        var i = document.createElement("div");
                        i.className = "scale-div",
                            i.style.right = "20px",
                            i.style.top = (this.options.c_1_height + this.options.canvas_offset_top) / this.options.dpr - 40 + "px",
                            this.options.scale.scale = i;
                        var o = document.createDocumentFragment()
                            , e = document.createElement("span");
                        e.className = "span-minus",
                            this.options.scale.minus = e;
                        var s = document.createElement("span");
                        s.className = "span-plus",
                            this.options.scale.plus = s,
                            o.appendChild(e),
                            o.appendChild(s),
                            i.appendChild(o),
                            document.getElementById(this.options.container).appendChild(i)
                    }
                }
                ,
                t.prototype.showTip = function(t, i, o) {
                    var e = this.options.type.toUpperCase();
                    if (this.options.tip) {
                        var s = this.options.tip
                            , a = this.options.tip.tip
                            , r = Math.round(o.volume);
                        "DK" == e || "WK" == e || "MK" == e || "M5K" == e || "M15K" == e || "M30K" == e || "M60K" == e ? (s.close.innerText = o.close,
                            s.percent.innerText = o.percent + "%",
                            s.count.innerText = n.translate(r),
                            s.time.innerText = o.data_time.replace(/-/g, "/")) : ("T1" == e || "T2" == e || "T3" == e || "T4" == e || "T5" == e) && (s.close.innerText = o.price,
                            s.percent.innerText = o.percent + "%",
                            s.count.innerText = n.translate(r),
                            s.time.innerText = o.time)
                    } else {
                        this.options.tip = {};
                        var a = document.createElement("div");
                        a.className = "show-tip",
                            this.options.tip.tip = a;
                        var p = document.createDocumentFragment()
                            , h = document.createElement("span");
                        h.className = "span-price",
                            this.options.tip.close = h;
                        var l = document.createElement("span");
                        this.options.tip.percent = l;
                        var c = document.createElement("span");
                        this.options.tip.count = c;
                        var d = document.createElement("span");
                        this.options.tip.time = d;
                        var u = document.createElement("div");
                        u.className = "tip-line-1",
                            u.appendChild(h),
                            u.appendChild(l);
                        var m = document.createElement("div");
                        m.className = "tip-line-2",
                            m.appendChild(c),
                            m.appendChild(d),
                            p.appendChild(u),
                            p.appendChild(m),
                            a.appendChild(p),
                            document.getElementById(this.options.container).appendChild(a);
                        var r = Math.round(o.volume);
                        if ("DK" == e || "WK" == e || "MK" == e || "M5K" == e || "M15K" == e || "M30K" == e || "M60K" == e) {
                            h.innerText = o.close,
                                l.innerText = o.percent + "%",
                                c.innerText = n.translate(r),
                                d.innerText = o.data_time,
                                a.style.top = -a.clientHeight + "px";
                            var f = "span-k-c1"
                                , v = "span-k-c2"
                        } else if ("T1" == e || "T2" == e || "T3" == e || "T4" == e || "T5" == e) {
                            h.innerText = o.price,
                                l.innerText = o.percent + "%",
                                c.innerText = n.translate(r),
                                d.innerText = o.time,
                                a.style.top = -a.clientHeight + "px",
                                a.className = a.className + " time-tip";
                            var f = "span-time-c1"
                                , v = "span-time-c2"
                        }
                        h.className = h.className + " " + f,
                            l.className = l.className + " " + v,
                            c.className = c.className + " " + f,
                            d.className = d.className + " " + v
                    }
                    o && o.up ? a.style.backgroundColor = this.options.up_color : o && !o.up && (a.style.backgroundColor = this.options.down_color),
                        a.style.left = i <= a.clientWidth / 2 + this.options.padding_left / this.options.dpr ? this.options.padding_left / this.options.dpr + "px" : i >= t.width / this.options.dpr - a.clientWidth / 2 ? t.width / this.options.dpr - a.clientWidth + "px" : i - a.clientWidth / 2 + "px"
                }
                ,
                t.prototype.markPoint = function(t, i, o, e) {
                    if (e >= 0) {
                        var s = n.canvasToWindow.apply(this, [o, o.width, this.options.c_1_height])
                            , a = n.canvasToWindow.apply(this, [o, t, this.options.c_1_height])
                            , r = document.createElement("div");
                        r.className = "mark-point";
                        var p = this.options.markPoint.imgUrl
                            , h = void 0 == this.options.markPoint.width ? 15 : this.options.markPoint.width + "px"
                            , l = void 0 == this.options.markPoint.height ? 15 : this.options.markPoint.height + "px";
                        if (p && (r.style.background = "url(" + p + ") no-repeat center center/" + h + " " + l + " #cccccc",
                            r.style.background = "url(" + p + ") no-repeat center center/" + h + " " + l + " #cccccc"),
                            this.options.markPoint.width && this.options.markPoint.height ? (r.style.width = h,
                                r.style.height = l) : (r.style.width = h,
                                r.style.height = l),
                            r.setAttribute("data-point", i),
                            !this.options.pointsContainer) {
                            var c = document.createElement("div");
                            this.options.pointsContainer = c,
                                document.getElementById(this.options.container).appendChild(this.options.pointsContainer)
                        }
                        this.options.pointsContainer.appendChild(r),
                            r.style.left = a.x - r.clientWidth / 2 + "px",
                            r.style.top = s.y - 30 + "px"
                    }
                }
                ,
                t.prototype.show = function() {
                    if (this.options.cross) {
                        var t = this.options.cross.x_line;
                        t && (t.style.display = "block");
                        var i = this.options.cross.y_line;
                        i && (i.style.display = "block");
                        var o = this.options.cross.point;
                        o && (o.style.display = "block")
                    }
                    if (this.options.tip) {
                        var e = this.options.tip.tip;
                        e && (e.style.display = "block")
                    }
                }
                ,
                t.prototype.hide = function() {
                    if (this.options.cross) {
                        var t = this.options.cross.x_line;
                        t && (t.style.display = "none");
                        var i = this.options.cross.y_line;
                        i && (i.style.display = "none");
                        var o = this.options.cross.point;
                        o && (o.style.display = "none")
                    }
                    if (this.options.mark_ma) {
                        var e = this.options.mark_ma.ma_5_data;
                        e && (e.innerText = this.default_m5 && "-" != this.default_m5.value ? "MA5:" + (this.default_m5.value / 1).toFixed(this.options.pricedigit) : "MA5: -");
                        var n = this.options.mark_ma.ma_10_data;
                        n && (n.innerText = this.default_m10 && "-" != this.default_m10.value ? "MA10:" + (this.default_m10.value / 1).toFixed(this.options.pricedigit) : "MA10: -");
                        var s = this.options.mark_ma.ma_20_data;
                        s && (s.innerText = this.default_m20 && "-" != this.default_m20.value ? "MA20:" + (this.default_m20.value / 1).toFixed(this.options.pricedigit) : "MA20: -")
                    }
                    if (this.options.tip) {
                        var a = this.options.tip.tip;
                        a && (a.style.display = "none")
                    }
                }
                ,
                t.prototype.showLoading = function() {
                    if (this.options.loading)
                        this.options.loading.style.display = "block";
                    else {
                        var t = document.getElementById(this.options.container)
                            , i = document.createElement("div");
                        i.className = "loading-chart",
                            i.innerText = "加载中...",
                            i.style.height = this.options.height - 100 + "px",
                            i.style.width = this.options.width + "px",
                            i.style.paddingTop = "100px",
                            this.options.loading = i,
                            t.appendChild(i)
                    }
                }
                ,
                t.prototype.hideLoading = function() {
                    this.options.loading.style.display = "none"
                }
                ,
                t.prototype.showNoData = function() {
                    if (this.options.noData)
                        this.options.noData.style.display = "block";
                    else {
                        var t = document.getElementById(this.options.container)
                            , i = document.createElement("div");
                        i.className = "loading-chart",
                            i.innerText = "暂无数据",
                            i.style.height = this.options.height - 100 + "px",
                            i.style.width = this.options.width + "px",
                            i.style.paddingTop = "100px",
                            this.options.noData = i,
                            t.appendChild(i)
                    }
                }
                ,
                t
        }();
        t.exports = a
    }
    , function(t) {
        var i, o, e;
        e = function(t) {
            return "[object Object]" === Object.prototype.toString.call(t)
        }
            ,
            o = function n(t, i) {
                var o;
                for (o in t)
                    if ("undefined" != typeof t[o])
                        try {
                            if (e(t[o]) && e(i[o]) && n(t[o], i[o]),
                                i.hasOwnProperty(o))
                                continue;
                            i[o] = t[o]
                        } catch (s) {}
            }
            ,
            i = function() {
                var t, i = arguments, n = {};
                if (!i.length)
                    return {};
                for (t = i.length - 1; t >= 0; t--)
                    e(i[t]) && o(i[t], n);
                return i[0] = n,
                    n
            }
            ,
            t.exports = i
    }
    , function(t) {
        function i(t, i, o, e, n, s) {
            var a = navigator.appVersion;
            (a.indexOf("MSIE 8.0") > -1 || a.indexOf("MSIE 7.0") > -1) && (s = !0);
            var r = t.canvas
                , p = new Image;
            e = void 0 == e ? 164 : e,
                n = void 0 == n ? 41 : n,
                p.width = 0,
                p.height = 0,
                p.src = "http://g1.dfcfw.com/g1/201607/20160727150611.png",
                s ? t.drawImage(p, r.width - i, o, e, n) : p.onload = function() {
                    setTimeout(function() {
                        t.drawImage(p, r.width - i, o, e, n)
                    }, 0)
                }
        }
        t.exports = i
    }
    , function(t, i, o) {
        var e = o(24)
            , n = o(7)
            , s = o(26)
            , a = o(29)
            , r = o(30)
            , p = o(31)
            , h = o(13)
            , l = o(20)
            , c = o(6)
            , d = o(22)
            , u = o(32)
            , m = function() {
            function t(t) {
                this.defaultoptions = n.chartK,
                    this.options = {},
                    c(!0, this.options, n.defaulttheme, t),
                    this.container = document.getElementById(t.container),
                    this.container.className = this.container.className.replace(/emcharts-container/g, "").trim(),
                    this.container.className = this.container.className + " emcharts-container",
                    this.options.onChartLoaded = void 0 == t.onChartLoaded ? function() {}
                        : t.onChartLoaded
            }
            function i() {
                var t = this
                    , i = t.options.type
                    , e = t.options.interactive;
                this.options.interactive.showLoading();
                try {
                    "DK" == i ? this.options.type = "K" : "WK" == i ? this.options.type = "WK" : "MK" == i ? this.options.type = "MK" : "M5K" == i ? this.options.type = "M5K" : "M15K" == i ? this.options.type = "M15K" : "M30K" == i ? this.options.type = "M30K" : "M60K" == i && (this.options.type = "M60K"),
                        s(o.call(t), function(i) {
                            i && (m.apply(t, [i]),
                                t.options.clickable = !0)
                        }, e)
                } catch (n) {
                    t.options.clickable = !0,
                        e.showNoData(),
                        e.hideLoading()
                }
            }
            function o() {
                var t = {};
                return t.type = this.options.type,
                    t.code = this.options.code,
                    t.count = this.options.scale_count,
                    t.authorityType = this.options.authorityType,
                    t
            }
            function m(t) {
                var i = this.options.context
                    , o = i.canvas;
                this.options.data = null == t ? this.options.data : t,
                    t = this.options.data;
                var n = this.options.interactive;
                try {
                    if (!t || !t.data || 0 == t.data.length)
                        return this.options.data = {},
                            new e(this.options),
                            new p(this.options),
                            void n.hideLoading();
                    if (this.options.pricedigit = t.pricedigit || 2,
                        n.options.pricedigit = this.options.pricedigit,
                        s) {
                        var s = t.five_average
                            , l = t.ten_average
                            , c = t.twenty_average;
                        n.default_m5 = s[s.length - 1],
                            n.default_m10 = l[l.length - 1],
                            n.default_m20 = c[c.length - 1]
                    }
                    var u = h.get_rect.apply(this, [o, t.data.length]);
                    this.options.rect_unit = u;
                    var m = new e(this.options);
                    if (t && t.data && t.data.length > 0 && (new a(this.options),
                        new r(this.options)),
                    this.options.showV && new p(this.options),
                        m.drawYMark(),
                        this.options.interactive.options.pointsContainer) {
                        var f = this.options.interactive.options.pointsContainer.children;
                        this.markPointsDom = f
                    }
                    n.hideLoading(),
                        this.options.onChartLoaded(this)
                } catch (v) {
                    _this.options.clickable = !0,
                        n.showNoData(),
                        n.hideLoading()
                }
                return d.apply(this, [this.options.context, 190, 20]),
                    !0
            }
            function f(t) {
                var o = this
                    , e = t.canvas
                    , n = o.options.interactive;
                this.options.clickable = !0;
                var s = !1
                    , a = this.options.delaytouch = !0;
                e.addEventListener("touchstart", function(t) {
                    a ? (s = !1,
                        timer_s = setTimeout(function() {
                            s = !0,
                                n.show(),
                                v.apply(o, [n, t.changedTouches[0]]),
                                t.preventDefault()
                        }, 200)) : (n.show(),
                        v.apply(o, [n, t.changedTouches[0]])),
                    o.options.preventdefault && t.preventDefault()
                }),
                    e.addEventListener("touchmove", function(t) {
                        a ? (clearTimeout(timer_s),
                        s && (v.apply(o, [n, t.changedTouches[0]]),
                            t.preventDefault())) : v.apply(o, [n, t.changedTouches[0]]),
                        o.options.preventdefault && t.preventDefault()
                    }),
                    e.addEventListener("touchend", function() {
                        a && clearTimeout(timer_s),
                            n.hide()
                    }),
                    e.addEventListener("touchcancel", function(t) {
                        a && clearTimeout(timer_s),
                            n.hide(),
                        o.options.preventdefault && t.preventDefault()
                    }),
                    e.addEventListener("mousemove", function(t) {
                        v.apply(o, [n, t]),
                            t.preventDefault()
                    }),
                    e.addEventListener("mouseleave", function(t) {
                        n.hide(),
                            t.preventDefault()
                    }),
                    e.addEventListener("mouseenter", function(t) {
                        n.show(),
                            t.preventDefault()
                    });
                var r = n.options.scale.plus
                    , p = n.options.scale.minus;
                r.addEventListener("click", function() {
                    var n = o.options.scale_count;
                    2 > n && o.options.clickable && (o.options.clickable = !1,
                        p.style.opacity = "1",
                        o.options.scale_count = n + 1,
                    o.options.interactive.options.pointsContainer && (o.options.interactive.options.pointsContainer.innerHTML = ""),
                        t.clearRect(0, -o.options.canvas_offset_top, e.width, e.height),
                        i.apply(o)),
                    o.options.scale_count >= 2 && (r.style.opacity = "0.5")
                }),
                    p.addEventListener("click", function() {
                        var n = o.options.scale_count;
                        n > -2 && o.options.clickable && (o.options.clickable = !1,
                            r.style.opacity = "1",
                            o.options.scale_count = n - 1,
                        o.options.interactive.options.pointsContainer && (o.options.interactive.options.pointsContainer.innerHTML = ""),
                            t.clearRect(0, -o.options.canvas_offset_top, e.width, e.height),
                            i.apply(o)),
                        o.options.scale_count <= -2 && (p.style.opacity = "0.5")
                    })
            }
            function v(t, i) {
                var o = this.options.canvas
                    , e = this.options.data.data
                    , n = this.options.data.five_average
                    , s = this.options.data.ten_average
                    , a = this.options.data.twenty_average
                    , r = this.options.rect_unit
                    , p = r.rect_w
                    , l = i.offsetX || i.clientX - this.container.getBoundingClientRect().left
                    , c = i.offsetY || i.clientY - this.container.getBoundingClientRect().top
                    , d = h.windowToCanvas.apply(this, [o, l, c])
                    , u = d.x.toFixed(0)
                    , m = Math.floor((u - this.options.padding_left) / p);
                if (e && e[m]) {
                    t.showTip(o, l, e[m]);
                    var f = h.canvasToWindow.apply(this, [o, e[m].cross_x, e[m].cross_y])
                        , v = f.x
                        , g = f.y;
                    t.cross(o, v, g)
                }
                n && n[m] && t.markMA(o, n[m], s[m], a[m])
            }
            function g() {
                var t = null == u.getCookie("emcharts-authorityType") ? "" : u.getCookie("emcharts-authorityType");
                if (void 0 === this.options.authorityType)
                    this.options.authorityType = t;
                else if (t !== this.options.authorityType) {
                    var i = 1e6
                        , o = new Date;
                    o.setTime(o.getTime() + 24 * i * 60 * 60 * 1e3),
                        u.setCookie("emcharts-authorityType", this.options.authorityType, o, "/")
                }
            }
            return t.prototype.init = function() {
                this.options.type = void 0 == this.options.type ? "DK" : this.options.type;
                var t = document.createElement("canvas");
                this.container.style.position = "relative";
                var i = t.getContext("2d");
                this.options.canvas = t,
                    this.options.context = i;
                var o = this.options.dpr;
                t.width = this.options.width * o,
                    t.height = this.options.height * o,
                    this.options.sepeNum = 7,
                    this.options.canvas_offset_top = t.height / this.options.sepeNum / 2,
                    this.options.padding_left = 0,
                    this.options.k_v_away = t.height / this.options.sepeNum,
                    this.options.scale_count = void 0 == this.options.scale_count ? 0 : this.options.scale_count,
                    this.options.showV = void 0 == this.options.showV ? !0 : this.options.showV,
                    this.options.showVMark = void 0 == this.options.showVMark ? !1 : this.options.showVMark,
                    this.options.c_1_height = this.options.showV ? 4 * t.height / this.options.sepeNum : t.height - 90 * o,
                    this.options.unit = {},
                    this.options.unit.unitHeight = t.height / this.options.sepeNum,
                    t.style.width = this.options.width + "px",
                    t.style.height = this.options.height + "px",
                    t.style.border = "0",
                    this.options.crossline = !1,
                    i.translate("0", this.options.canvas_offset_top),
                    i.font = this.options.font_size * this.options.dpr + "px Arial",
                    i.lineWidth = 1 * this.options.dpr,
                    this.options.enableHandle = void 0 == this.options.enableHandle ? !0 : this.options.enableHandle,
                    this.container.appendChild(t),
                    g.call(this)
            }
                ,
                t.prototype.draw = function(t) {
                    var i = this;
                    i.clear(),
                        i.init();
                    var e = i.options.interactive = new l(i.options);
                    e.showLoading();
                    var n = i.options.type;
                    try {
                        "DK" == n ? this.options.type = "DK" : "WK" == n ? this.options.type = "WK" : "MK" == n ? this.options.type = "MK" : "M5K" == n ? this.options.type = "M5K" : "M15K" == n ? this.options.type = "M15K" : "M30K" == n ? this.options.type = "M30K" : "M60K" == n && (this.options.type = "M60K"),
                            s(o.call(i), function(o) {
                                var n = m.apply(i, [o]);
                                e.markMA(i.options.canvas),
                                    e.scale(i.options.canvas),
                                n && i.options.enableHandle && f.call(i, i.options.context),
                                t && t(i.options)
                            }, e)
                    } catch (a) {
                        e.showNoData(),
                            e.hideLoading()
                    }
                }
                ,
                t.prototype.reDraw = function() {
                    this.clear(),
                        this.init(),
                        m.call(this)
                }
                ,
                t.prototype.clear = function(t) {
                    this.container ? this.container.innerHTML = "" : document.getElementById(this.options.container).innerHTML = "",
                    t && t()
                }
                ,
                t.prototype.getMarkPointsDom = function() {
                    var t = this.options.interactive.options.pointsContainer.children;
                    return t
                }
                ,
                t
        }();
        t.exports = m
    }
    , function(t, i, o) {
        var e = o(6)
            , n = o(7)
            , s = o(25)
            , a = o(9)
            , r = function() {
            function t(t) {
                this.defaultoptions = n.draw_xy,
                    this.options = {},
                    e(!1, this.options, this.defaultoptions, t),
                    this.draw()
            }
            function i(t, i, o, e) {
                e.length;
                t.beginPath(),
                    t.fillStyle = "#333333",
                    t.strokeStyle = "#e5e5e5",
                    t.rect(1, 0, this.options.canvas.width - 2, this.options.c_1_height),
                    t.stroke(),
                    t.beginPath();
                for (var n = 0; 3 > n; n++) {
                    var r = (n + 1) / 4 * (t.canvas.width - this.options.padding_left) + this.options.padding_left;
                    1 == n ? (t.strokeStyle = "#e5e5e5",
                        t.moveTo(a(r), .5),
                        t.lineTo(a(r), a(this.options.c_1_height)),
                        t.stroke()) : (t.strokeStyle = "#efefef",
                        s(t, r, 0, r, this.options.c_1_height, 5))
                }
                for (var p, n = 0; p = e[n]; n++)
                    t.beginPath(),
                        2 == n ? (t.strokeStyle = "#e5e5e5",
                            t.moveTo(.5, a(p.y)),
                            t.lineTo(a(t.canvas.width), a(p.y)),
                            t.stroke()) : 0 != n && n != e.length - 1 && (t.strokeStyle = "#efefef",
                            s(t, 0, Math.round(p.y), t.canvas.width, Math.round(p.y), 5))
            }
            function o(t, i, o) {
                var e = this.options.padding_left;
                t.beginPath(),
                    t.fillStyle = "#999";
                var n = t.canvas.width
                    , s = i + 1 * this.options.unit.unitHeight / 3
                    , a = this.options.data.data.filter(function(t) {
                    return void 0 !== t
                }).length;
                if (a > 10)
                    t.fillText(o[0], e, s),
                        t.fillText(o[1], (n - e) / 2 + e - t.measureText(o[1]).width / 2, s),
                        t.fillText(o[2], n - t.measureText(o[2]).width, s);
                else {
                    t.fillText(o[0], e, s);
                    var r = e + (n - e) / 10 * a - this.options.rect_unit.rect_w / 2 - t.measureText(o[2]).width / 2;
                    r > e + t.measureText(o[0]).width + 10 && t.fillText(o[2], r, s)
                }
            }
            function r(t, i, o, e) {
                for (var n = (t - i) / (o - 1), s = [], a = 0; o > a; a++)
                    s.push({
                        num: i + a * n,
                        x: 0,
                        y: e - a / (o - 1) * e
                    });
                return s
            }
            return t.prototype.draw = function() {
                var t = this.options.data
                    , e = this.options.context
                    , n = (this.options.type,
                    t.max)
                    , s = t.min
                    , a = 5
                    , p = t.timeStrs
                    , h = this.options.c_1_height
                    , l = r(n, s, a, h);
                this.options.y_mark_list = l,
                    i.apply(this, [e, n, s, l]),
                p && o.apply(this, [e, h, p])
            }
                ,
                t.prototype.drawYMark = function() {
                    var t = this.options.context;
                    t.beginPath(),
                        t.fillStyle = "#333333",
                        t.strokeStyle = "#e5e5e5";
                    for (var i, o = this.options.y_mark_list, e = 0; i = o[e]; e++)
                        t.beginPath(),
                            isNaN(i.num) ? t.fillText("0.00", 0, i.y - 10) : 0 == e ? t.fillText(i.num.toFixed(this.options.pricedigit), 5, i.y - 10) : e == o.length - 1 ? t.fillText(i.num.toFixed(this.options.pricedigit), 5, i.y + 25) : t.fillText(i.num.toFixed(this.options.pricedigit), 5, i.y + 10)
                }
                ,
                t
        }();
        t.exports = r
    }
    , function(t, i, o) {
        function e(t, i, o, e, s, a) {
            t.save();
            for (var r = void 0 === a ? 5 : a, p = e - i, h = s - o, l = Math.floor(Math.sqrt(p * p + h * h) / r), c = 0; l > c; c++)
                c % 2 === 0 ? t.moveTo(n(i + p / l * c), n(o + h / l * c)) : t.lineTo(n(i + p / l * c), n(o + h / l * c));
            t.stroke(),
                t.restore()
        }
        var n = o(9);
        t.exports = e
    }
    , function(t, i, o) {
        function e(t, i, o) {
            var e = "http://pdfm2.eastmoney.com/EM_UBG_PDTI_Fast/api/js"
                , r = "fsdata" + (new Date).getTime().toString().substring(0, 10)
                , p = t.code || t
                , h = "k";
            "dk" !== t.type.toLowerCase() && (h = t.type);
            var l = 0 | t.count
                , c = 60;
            switch (l) {
                case 0:
                    c = 60;
                    break;
                case 1:
                    c = 45;
                    break;
                case 2:
                    c = 36;
                    break;
                case -1:
                    c = 105;
                    break;
                case -2:
                    c = 205
            }
            var d = new Date
                , u = d.getFullYear().toString() + a((d.getMonth() + 1).toString(), 2) + a(d.getDate(), 2)
                , m = u + "," + (c + 20);
            null !== h.match("M") && "MK" !== h && (m = u + a(d.getHours(), 2) + a(d.getMinutes(), 2) + "," + (c + 20));
            var f = {
                id: p,
                TYPE: h,
                js: r + "((x))",
                rtntype: 5,
                QueryStyle: "2.2",
                QuerySpan: m,
                extend: "cma,5,10,20,30",
                isCR: !1
            };
            "" !== t.authorityType && t.authorityType && (f.authorityType = t.authorityType),
                n(e, f, r, function(t) {
                    try {
                        if (t) {
                            var e = t.info;
                            if (window.pricedigit = e.pricedigit.split(".").length > 1 ? 0 == e.pricedigit.split(".")[1].length ? 2 : e.pricedigit.split(".")[1].length : 0,
                            e.total < c)
                                var n = s(t, e.total);
                            else
                                var n = s(t, c);
                            n.name = t.name,
                                n.total = e.total,
                                n.count = c - 20,
                                n.pricedigit = window.pricedigit,
                            n.data.length < 10 && (n.data.length = 10),
                                i(n)
                        } else
                            i(null)
                    } catch (a) {
                        o.showNoData(),
                            o.hideLoading()
                    }
                })
        }
        var n = o(14)
            , s = o(27)
            , a = o(13).fixed;
        t.exports = e
    }
    , function(t, i, o) {
        function e(t, i) {
            var o = t.info || {}
                , e = t.data
                , h = {}
                , l = -99999999
                , c = 99999999
                , d = 0
                , u = 0;
            h.data = [],
                h.max = 0,
                h.min = t.info.yc,
                h.v_max = 0,
                h.total = t.info.total,
                h.name = t.name,
                h.code = t.code,
                h.pricedigit = (t.info.pricedigit.split(".")[1] || "").length;
            var m = t.info.yc
                , f = e.length
                , v = f - i > 0 ? f - i : 0;
            m = 0 === v ? e[0].split(/\[|\]/)[0].split(",")[2] : e[v - 1].split(/\[|\]/)[0].split(",")[2];
            var g = t.data;
            for (u = v; f > u; u++) {
                var y = g[u].split(/\[|\]/)
                    , A = g[u].split(/\[|\]/)[0].split(",")
                    , x = {};
                x.data_time = A[0],
                    x.open = A[1],
                    x.close = A[2],
                    x.highest = A[3],
                    x.lowest = A[4];
                var w;
                if (u > 0) {
                    var _ = g[u - 1].split(/\[|\]/)[0].split(",");
                    w = _[2]
                } else
                    w = o.yc || A[2];
                if (u > 0 ? x.percent = 0 == m ? 0 : (100 * (1 * x.close - m) / m * 1).toFixed(2) : (x.percent = 0,
                    l = c = x.open),
                    x.priceChange = (x.close - w).toFixed(h.pricedigit),
                    x.percent = 0 === w ? 0 : (x.priceChange / w * 100).toFixed(2),
                u == v && (x.priceChange = 0..toFixed(h.pricedigit),
                    x.percent = 0..toFixed(h.pricedigit)),
                    x.volume = A[5],
                    m = x.close,
                    x.up = 1 * x.close - 1 * x.open >= 0 ? !0 : !1,
                    y[1]) {
                    var b = y[1].split(",");
                    n.call(h, "five_average", b[0], x.data_time),
                        n.call(h, "ten_average", b[1], x.data_time),
                        n.call(h, "twenty_average", b[2], x.data_time),
                        n.call(h, "thirty_average", b[3], x.data_time)
                }
                l = Math.max(l, x.highest),
                    c = Math.min(c, x.lowest),
                    h.max = s([1 * x.highest, A[1], A[2], A[3], A[4], w]),
                    h.min = a([1 * x.highest, A[1], A[2], A[3], A[4], w]),
                    d = d > 1 * x.volume ? d : 1 * x.volume,
                    h.data.push(x)
            }
            return h.timeStrs = [],
                h.timeStrs[0] = r(e[v].split(",")[0]),
                h.timeStrs[1] = r(e[Math.floor((f + v) / 2)].split(",")[0]),
                h.timeStrs[2] = r(e[f - 1].split(",")[0]),
                h.max = parseFloat(p(l, c).max),
                h.min = parseFloat(p(l, c).min),
                h.v_max = Number(d.toFixed(2)),
                h
        }
        function n(t, i, o) {
            "-" === i && (i = null),
                void 0 === this[t] ? this[t] = [{
                    value: i,
                    date: o
                }] : this[t].push({
                    value: i,
                    date: o
                })
        }
        function s(t) {
            for (var i = [], o = 0; o < t.length; o++)
                isNaN(t[o]) || i.push(parseFloat(t[o]));
            return i.sort(function(t, i) {
                return i - t
            }),
                i[0]
        }
        function a(t) {
            for (var i = [], o = 0; o < t.length; o++)
                isNaN(t[o]) || i.push(parseFloat(t[o]));
            return i.sort(function(t, i) {
                return t - i
            }),
                i[0]
        }
        {
            var r = o(13).transform
                , p = o(28);
            o(13).fixed
        }
        t.exports = e
    }
    , function(t) {
        function i(t, i) {
            var o = 0
                , e = (t - i) / 2 * .05;
            return o = t + e,
                i -= e,
                {
                    max: o,
                    min: i
                }
        }
        t.exports = i
    }
    , function(t, i, o) {
        var e = o(6)
            , n = o(13)
            , s = o(7)
            , a = function() {
            function t(t) {
                this.defaultoptions = s.draw_k,
                    this.options = {},
                    e(!1, this.options, this.defaultoptions, t),
                    this.draw()
            }
            return t.prototype.draw = function() {
                var t = this.options.context
                    , i = this.options.data
                    , o = i.data;
                this.drawK(t, o)
            }
                ,
                t.prototype.drawK = function(t, i) {
                    var o = this.options.rect_unit
                        , e = o.bar_w
                        , s = this.options.up_color
                        , a = this.options.down_color
                        , r = this.options.interactive
                        , p = {};
                    if (this.options.markPoint && this.options.markPoint.show) {
                        var h = this.options.markPoint.dateList;
                        for (var l in h)
                            p[h[l]] = h[l]
                    }
                    for (var c, d = 0; c = i[d]; d++) {
                        var u = c.up;
                        t.beginPath(),
                            t.lineWidth = 1,
                            u ? (t.fillStyle = s,
                                t.strokeStyle = s) : (t.fillStyle = a,
                                t.strokeStyle = a);
                        var m = n.get_x.call(this, d + 1)
                            , f = n.get_y.call(this, c.open)
                            , v = n.get_y.call(this, c.close)
                            , g = n.get_y.call(this, c.highest)
                            , y = n.get_y.call(this, c.lowest);
                        c.cross_x = m,
                            c.cross_y = v,
                        p[c.data_time] && r.markPoint(m, c.data_time, this.options.context.canvas, this.options.scale_count),
                            t.moveTo(m, y),
                            t.lineTo(m, g),
                            t.stroke(),
                            t.beginPath(),
                            v >= f ? t.rect(m - e / 2, f, e, v - f) : t.rect(m - e / 2, v, e, f - v),
                            t.stroke(),
                            t.fill()
                    }
                }
                ,
                t
        }();
        t.exports = a
    }
    , function(t, i, o) {
        var e = o(6)
            , n = o(13)
            , s = o(7)
            , a = (o(9),
            function() {
                function t(t) {
                    this.defaultoptions = s.drawMA,
                        this.options = {},
                        e(!1, this.options, this.defaultoptions, t),
                        this.draw()
                }
                function i(t, i, o) {
                    var e = [];
                    t.beginPath(),
                        t.strokeStyle = o;
                    for (var s = !1, a = 0; a < i.length; a++) {
                        var r = i[a];
                        if (r && r.value) {
                            var p = n.get_x.call(this, a + 1)
                                , h = n.get_y.call(this, r.value);
                            e.push(r),
                                0 == a ? t.moveTo(p, h) : h > this.options.c_1_height || 0 > h ? (t.moveTo(p, h),
                                    s = !0) : s ? (t.moveTo(p, h),
                                    s = !1) : t.lineTo(p, h)
                        }
                    }
                    return t.stroke(),
                        t.beginPath(),
                        e
                }
                return t.prototype.draw = function() {
                    var t = this.options.context
                        , o = this.options.data
                        , e = o.five_average
                        , n = o.ten_average
                        , s = o.twenty_average;
                    this.options.ma_5_data = i.apply(this, [t, e, "#f4cb15"]),
                        this.options.ma_10_data = i.apply(this, [t, n, "#ff5b10"]),
                        this.options.ma_20_data = i.apply(this, [t, s, "#488ee6"])
                }
                    ,
                    t
            }());
        t.exports = a
    }
    , function(t, i, o) {
        var e = o(6)
            , n = o(13)
            , s = o(7)
            , a = o(25)
            , r = o(9)
            , p = function() {
            function t(t) {
                this.defaultoptions = s.draw_v,
                    this.options = {},
                    e(!1, this.options, this.defaultoptions, t),
                    this.draw()
            }
            function i() {
                var t = this.options.context
                    , i = this.options.data
                    , e = i.data
                    , s = 2 * this.options.unit.unitHeight
                    , p = .9 * s
                    , h = t.canvas.height - this.options.canvas_offset_top
                    , l = h - s;
                if (!e || 0 == e.length)
                    return t.beginPath(),
                        t.fillStyle = "#999",
                        t.strokeStyle = "rgba(230,230,230, 1)",
                        t.rect(r(this.options.padding_left), r(l), t.canvas.width - this.options.padding_left - 2, s),
                        void t.stroke();
                var c = i.v_max.toFixed(0)
                    , d = this.options.rect_unit
                    , u = d.bar_w
                    , m = this.options.up_color
                    , f = this.options.down_color;
                this.options.showV && this.options.showVMark && o.apply(this, [t, c, l]),
                    t.fillStyle = "#333333",
                    t.strokeStyle = "#e5e5e5",
                    t.lineWidth = this.options.dpr,
                    t.rect(r(this.options.padding_left), r(l), t.canvas.width - this.options.padding_left - 2, s),
                    t.moveTo(r(this.options.padding_left), r(l + s / 2)),
                    t.lineTo(r(t.canvas.width - this.options.padding_left), r(l + s / 2)),
                    t.stroke(),
                    t.beginPath();
                for (var v = 0; 3 > v; v++) {
                    var g = (v + 1) / 4 * (t.canvas.width - this.options.padding_left) + this.options.padding_left;
                    1 == v ? (t.strokeStyle = "#e5e5e5",
                        t.moveTo(r(g), r(l)),
                        t.lineTo(r(g), r(h)),
                        t.stroke()) : (t.strokeStyle = "#efefef",
                        a(t, g, l, g, h, 5))
                }
                for (var y, v = 0; y = e[v]; v++) {
                    var A = y.volume
                        , x = y.up
                        , w = A / c * p
                        , g = n.get_x.call(this, v + 1)
                        , _ = h - w;
                    t.beginPath(),
                        t.moveTo(r(g), r(_)),
                        x ? (t.fillStyle = m,
                            t.strokeStyle = m) : (t.fillStyle = f,
                            t.strokeStyle = f),
                        t.rect(r(g - u / 2), r(_), u, w),
                        t.stroke(),
                        t.fill()
                }
            }
            function o(t, i, o) {
                t.beginPath(),
                    t.fillStyle = "#999",
                    t.fillText(n.format_unit(i), 5, o + 25),
                    t.stroke()
            }
            return t.prototype.draw = function() {
                "TL" == this.options.type ? drawVTime.call(this) : i.call(this)
            }
                ,
                t
        }();
        t.exports = p
    }
    , function(t) {
        var i = {
            getCookieVal: function(t) {
                var i = document.cookie.indexOf(";", t);
                return -1 == i && (i = document.cookie.length),
                    unescape(document.cookie.substring(t, i))
            },
            getCookie: function(t) {
                for (var o = t + "=", e = o.length, n = document.cookie.length, s = 0; n > s; ) {
                    var a = s + e;
                    if (document.cookie.substring(s, a) == o)
                        return i.getCookieVal(a);
                    if (s = document.cookie.indexOf(" ", s) + 1,
                    0 == s)
                        break
                }
                return null
            },
            setCookie: function(t, i, o, e, n, s) {
                document.cookie = t + "=" + escape(i) + (o ? "; expires=" + o : "") + (e ? "; path=" + e : "") + (n ? "; domain=" + n : "") + (s ? "; secure" : "")
            },
            deleteCookie: function(t, i, o) {
                getCookie(t) && (document.cookie = t + "=" + (i ? "; path=" + i : "") + (o ? "; domain=" + o : "") + "; expires=Thu, 01-Jan-1970 00:00:01 GMT")
            }
        };
        t.exports = i
    }
    , function(t, i, o) {
        var e = o(34)
            , n = o(7)
            , s = o(35)
            , a = o(6)
            , r = o(20)
            , p = o(22)
            , h = (o(13),
            function() {
                function t(t) {
                    this.defaultoptions = n.chartLine,
                        this.options = {},
                        a(!0, this.options, n.defaulttheme, this.defaultoptions, t),
                        this.container = document.getElementById(t.container),
                        this.container.className = this.container.className.replace(/emcharts-container/g, "").trim(),
                        this.container.className = this.container.className + " emcharts-container",
                        this.onChartLoaded = void 0 == t.onChartLoaded ? function() {}
                            : t.onChartLoaded
                }
                function i(t) {
                    for (var i = -1e6, o = 0, e = [], n = 0; n < t.length; n++)
                        e = e.concat(t[n].data);
                    i = e[0],
                        o = e[0];
                    for (var n = 1; n < e.length; n++)
                        e[n] && (i = Math.max(i, e[n]),
                            o = Math.min(o, e[n]));
                    return i = i / 1 + .05 * (i - o),
                        o = o / 1 - .05 * (i - o),
                        {
                            max: i,
                            min: o
                        }
                }
                return t.prototype.init = function() {
                    this.options.type = "line";
                    var t = document.createElement("canvas");
                    this.container.style.position = "relative";
                    var i = t.getContext("2d");
                    this.options.canvas = t,
                        this.options.context = i;
                    var o = this.options.dpr;
                    t.width = this.options.width * o,
                        t.height = this.options.height * o,
                        this.options.canvas_offset_top = t.height / 18,
                        this.options.padding_left = t.width / 6,
                        this.options.k_v_away = t.height / 18,
                        this.options.scale_count = 0,
                        this.options.decimalCount = void 0 == this.options.decimalCount ? 2 : this.options.decimalCount,
                        this.options.c_1_height = this.options.showflag ? t.height * (5 / 9) : t.height * (7 / 9),
                        t.style.width = this.options.width + "px",
                        t.style.height = this.options.height + "px",
                        t.style.border = "0",
                        i.translate("0", this.options.canvas_offset_top),
                        i.font = this.options.font_size * this.options.dpr + "px Arial",
                        i.lineWidth = 1 * this.options.dpr + .5,
                        this.container.appendChild(t)
                }
                    ,
                    t.prototype.draw = function() {
                        this.clear(),
                            this.init(),
                            this.options.interactive = new r(this.options);
                        var t = this.options.context
                            , o = this.options.series;
                        this.options.data = {};
                        var n = i(o);
                        this.options.data.max = n.max,
                            this.options.data.min = n.min,
                            this.options.padding_left = t.measureText("+9000万").width + 20,
                            new e(this.options),
                            new s(this.options),
                            p.apply(this, [t, 190, 20])
                    }
                    ,
                    t.prototype.reDraw = function() {
                        this.clear(),
                            this.init(),
                            this.draw()
                    }
                    ,
                    t.prototype.clear = function(t) {
                        this.container ? this.container.innerHTML = "" : document.getElementById(this.options.container).innerHTML = "",
                        t && t()
                    }
                    ,
                    t
            }());
        t.exports = h
    }
    , function(t, i, o) {
        var e = o(6)
            , n = o(7)
            , s = o(13)
            , a = function() {
            function t(t) {
                this.defaultoptions = n.draw_xy,
                    this.options = {},
                    e(!1, this.options, this.defaultoptions, t),
                    this.draw()
            }
            function i(t, i, o, e) {
                t.save(),
                    t.fillStyle = "#b1b1b1",
                    t.strokeStyle = "#ccc",
                    t.textAlign = "right";
                for (var n, a = 0; n = e[a]; a++)
                    t.beginPath(),
                        t.moveTo(this.options.padding_left, Math.round(n.y)),
                        t.lineTo(t.canvas.width, Math.round(n.y)),
                        t.fillText(s.format_unit(n.num / 1, this.options.decimalCount), this.options.padding_left - 10, n.y + 10),
                        t.stroke();
                t.restore()
            }
            function o(t, i, o) {
                t.save();
                var e = this.options.padding_left;
                t.beginPath(),
                    t.textAlign = "center",
                    t.fillStyle = "#b1b1b1";
                for (var n, s = t.canvas.width, a = o.length, r = 0; a > r; r++)
                    n = o[r],
                    (void 0 == n.show ? !0 : n.show) && (a - 1 > r ? t.fillText(n.value, r * (s - e) / (a - 1) + e, this.options.c_1_height + 40) : r * (s - e) / (a - 1) + e + t.measureText(n.value).width > t.canvas.width && t.fillText(n.value, t.canvas.width - t.measureText(n.value).width / 2, this.options.c_1_height + 40)),
                    (void 0 == n.showline ? !0 : n.showline) && (t.strokeStyle = "#ccc",
                        t.moveTo(r * (s - e) / (a - 1) + e, 0),
                        t.lineTo(r * (s - e) / (a - 1) + e, this.options.c_1_height));
                t.stroke(),
                    t.restore()
            }
            function a(t, i, o, e) {
                for (var n = (t - i) / (o - 1), s = [], a = 0; o > a; a++)
                    s.push({
                        num: i + a * n,
                        x: 0,
                        y: e - a / (o - 1) * e
                    });
                return s
            }
            return t.prototype.draw = function() {
                var t = this.options.context
                    , e = this.options.data.max
                    , n = this.options.data.min
                    , s = this.options.sepenum || 6
                    , r = this.options.xaxis
                    , p = this.options.c_1_height
                    , h = a(e, n, s, p);
                i.call(this, t, e, n, h),
                    o.apply(this, [t, p, r])
            }
                ,
                t
        }();
        t.exports = a
    }
    , function(t, i, o) {
        var e = o(6)
            , n = o(7)
            , s = o(13)
            , a = function() {
            function t(t) {
                this.defaultoptions = n.drawLine,
                    this.options = {},
                    e(!1, this.options, this.defaultoptions, t),
                    this.draw()
            }
            function i(t, i) {
                t.save();
                var o = i.data
                    , e = o.length;
                t.beginPath();
                for (var n = 0; e > n; n++) {
                    var a = o[n];
                    if (a) {
                        var r = (t.canvas.width - this.options.padding_left) / (e - 1) * n + this.options.padding_left
                            , p = s.get_y.call(this, a);
                        0 == n ? t.moveTo(this.options.padding_left, p) : n == e - 1 ? t.lineTo(r, p) : t.lineTo(r, p)
                    }
                }
                t.stroke(),
                    t.restore()
            }
            function o(t, i) {
                t.save();
                for (var o, e = i.data, n = e.length, a = this.options.pointRadius, r = 0; o = e[r]; r++) {
                    t.beginPath();
                    var p = (t.canvas.width - this.options.padding_left) / (n - 1) * r + this.options.padding_left
                        , h = s.get_y.call(this, o);
                    0 == r ? (t.arc(p, h, a, 0, 2 * Math.PI, !0),
                        t.fill()) : r == n - 1 || (t.arc(p, h, a, 0, 2 * Math.PI, !0),
                        t.fill())
                }
                t.restore()
            }
            function a(t, i) {
                t.save();
                for (var o, e = this.options.dpr, n = t.canvas.width / 2, s = this.options.lineMarkWidth * e, a = 0, r = t.canvas.height * (7 / 9 - 1 / 18), p = 0; o = i[p]; p++) {
                    t.beginPath(),
                        t.strokeStyle = "#cadef8";
                    var h = Math.floor(p / 2) * (s + 7 * e)
                        , l = this.options.font_size * this.options.dpr + (s - this.options.font_size * this.options.dpr) / 2;
                    0 == p ? (t.fillStyle = o.color,
                        t.rect(a + 20, r, s, s),
                        t.fill(),
                        t.fillStyle = "#333",
                        t.fillText(o.name, a + s + 80, r + l)) : (p + 1) % 2 == 0 ? (t.fillStyle = o.color,
                        t.rect(n, r + h, s, s),
                        t.fill(),
                        t.fillStyle = "#333",
                        t.fillText(o.name, n + s + 60, r + h + l)) : (t.fillStyle = o.color,
                        t.rect(a + 20, r + h, s, s),
                        t.fill(),
                        t.fillStyle = "#333",
                        t.fillText(o.name, a + s + 80, r + h + l))
                }
                t.restore()
            }
            return t.prototype.draw = function() {
                var t = this.options.context;
                t.lineWidth = 1 * this.options.dpr + 1;
                for (var e, n = this.options.series, s = 0; e = n[s]; s++)
                    t.fillStyle = void 0 == e.color ? "#333" : e.color,
                        t.strokeStyle = void 0 == e.color ? "#333" : e.color,
                        i.apply(this, [t, e]),
                    e.showpoint && o.apply(this, [t, e]);
                this.options.showflag && a.apply(this, [t, n])
            }
                ,
                t
        }();
        t.exports = a
    }
    , function(t, i, o) {
        var e = o(21)
            , n = o(37)
            , s = o(38)
            , a = o(39)
            , r = o(40)
            , p = o(22)
            , h = function() {
            function t(t) {
                this.options = e(this.options, t),
                this.options.barWidth || (this.options.barWidth = .5),
                this.options.barWidth > 1 && (this.options.barWidth = 1),
                this.options.barWidth < .01 && (this.options.barWidth = .01),
                    this.container = document.getElementById(t.container),
                    this.container.className = this.container.className + " canvas-container"
            }
            return t.prototype.init = function() {
                this.container.style.position = "relative";
                var t = 2
                    , i = document.createElement("canvas");
                try {
                    var o = i.getContext("2d")
                } catch (e) {
                    i = window.G_vmlCanvasManager.initElement(i);
                    var o = i.getContext("2d")
                }
                i.width = t * this.options.width,
                    i.height = t * this.options.height,
                    i.style.width = this.options.width + "px",
                    i.style.height = this.options.height + "px",
                    i.style.border = "0";
                var o = i.getContext("2d");
                this.options.font_size = 12,
                    o.font = this.options.font_size * t + "px Arial",
                    o.lineWidth = 1 * t,
                    this.options.dpr = t,
                    this.options.canvas = i,
                    this.options.context = o,
                    this.container.appendChild(i),
                    this.options.defaultColor = "#FF7200",
                    this.options.defaultHoverColor = "#FF9A4A",
                this.options.sepeNum || (this.options.sepeNum = 4),
                    this.options.padding = {},
                    this.options.padding.left = o.measureText("2.00").width * t,
                    this.options.padding.right = 10,
                    this.options.padding.top = 2 * this.options.font_size * t;
                var n = this.options.xaxis;
                if (this.options.angle || 0 == this.options.angle) {
                    var a = n[0].value;
                    this.options.padding.bottom = o.measureText(a).width * Math.sin(2 * Math.PI / 360 * this.options.angle) + 25
                } else
                    this.options.padding.bottom = 50 * t;
                var r = (i.width - this.options.padding.left - this.options.padding.right) / this.options.series.data.length
                    , h = r * this.options.barWidth;
                this.options.unit_w_len = r,
                    this.options.unit_w_kind = h;
                var l = s(this.options.sepeNum, this.options.series.data);
                this.options.coordinate = l,
                    p.apply(this, [this.options.context, 90 * t, 40 * t, 82 * t, 20 * t])
            }
                ,
                t.prototype.draw = function(t) {
                    this.init();
                    var i = this;
                    new n(this.options),
                        a.call(this),
                        this.options.canvas.addEventListener("touchstart", function(t) {
                            var o = t.touches[0].clientX - i.container.getBoundingClientRect().left
                                , e = t.touches[0].clientY - i.container.getBoundingClientRect().top;
                            r.call(i, o, e)
                        }, !1),
                        this.options.canvas.addEventListener("click", function(t) {
                            var o = t.clientX - i.container.getBoundingClientRect().left
                                , e = t.clientY - i.container.getBoundingClientRect().top;
                            r.call(i, o, e)
                        }, !1),
                    t && t()
                }
                ,
                t.prototype.reDraw = function(t) {
                    this.options = {},
                        this.options = e(this.options, t),
                        this.clear(),
                        this.draw()
                }
                ,
                t.prototype.clear = function(t) {
                    this.container ? this.container.innerHTML = "" : document.getElementById(this.options.container).innerHTML = "",
                    t && t()
                }
                ,
                t
        }();
        t.exports = h
    }
    , function(t, i, o) {
        var e = o(21)
            , n = o(8)
            , s = function() {
            function t(t) {
                this.options = {},
                    this.options = e(this.options, t),
                    this.draw()
            }
            function i(t, i) {
                var o = 1;
                -1 !== i.toString().indexOf(".") && (o = i.toString().length - i.toString().indexOf(".") - 1);
                var e = Math.pow(10, o);
                return Math.round(t * e) / e
            }
            return t.prototype.init = function() {
                this.options.yLefShow = !0,
                    this.options.yRightShow = !0,
                    this.options.isDash = !0,
                    this.options.xSplitShow = !1,
                    this.options.xShowDivide = !1
            }
                ,
                t.prototype.draw = function() {
                    this.init();
                    {
                        var t = this.options.padding.top
                            , o = this.options.padding.left
                            , e = this.options.padding.right
                            , s = this.options.padding.bottom
                            , a = this.options.context
                            , r = this.options.canvas
                            , p = this.options.xaxis
                            , h = (this.options.series.data,
                            this.options.unit_w_len)
                            , l = this.options.dpr
                            , c = this.options.coordinate
                            , d = c.max
                            , u = c.min
                            , m = c.stepHeight
                            , f = this.options.sepeNum;
                        r.height - t - s
                    }
                    a.save();
                    var v = Math.round(r.height - s)
                        , g = Math.round(t)
                        , y = Math.round(o)
                        , A = Math.round(r.width - e);
                    a.strokeStyle = "#C9C9C9",
                        a.beginPath(),
                        a.moveTo(y, v),
                        a.lineTo(A, v),
                        a.moveTo(y, v),
                        a.lineTo(y, g),
                        a.moveTo(y, g),
                        a.lineTo(A, g),
                        a.moveTo(A, v),
                        a.lineTo(A, g),
                        a.stroke();
                    var x = (v - g) / f;
                    a.textBaseline = "top";
                    for (var w = 0, _ = p.length; _ > w; w++) {
                        var b = a.measureText(p[w].value).width;
                        p[w].show && a.fillText(p[w].value, y + w * h + (h - b) / 2, v + 15)
                    }
                    for (w = 1,
                             _ = f; _ > w; w++) {
                        var T = l / 2;
                        w == d / m ? (a.beginPath(),
                            a.moveTo(y, Math.round(x * w + t) + T),
                            a.lineTo(A, T + Math.round(x * w + t)),
                            a.stroke()) : n(a, y, Math.round(x * w + t) + T, A, T + Math.round(x * w + t), 3)
                    }
                    this.options.coordinateMaxY;
                    a.textAlign = "end";
                    for (w = 0; f >= w; w++)
                        a.beginPath(),
                            a.textBaseline = 0 === w ? "bottom" : w === f ? "top" : "middle",
                            a.fillText(i(u + w * m, m), o - 10, x * (f - w) + t)
                }
                ,
                t
        }();
        t.exports = s
    }
    , function(t) {
        function i(t, i) {
            for (var n = [], a = 0, r = i.length; r > a; a++)
                0 / 0 !== i[a] && null !== i[a] && void 0 !== i[a] && n.push(i[a]);
            n.length <= 1 && n.push(0);
            for (var p = n[0], h = n[0], r = n.length, l = {}, c = 1, a = r - 1; a >= 0; a--)
                p = Math.max(p, n[a]),
                    h = Math.min(h, n[a]);
            if (0 >= p && (p = Math.abs(h),
                h = Math.abs(p),
                c = -1),
            isNaN(p) || isNaN(h))
                return {
                    max: 0,
                    min: 0,
                    stepHeight: 0
                };
            if (p === h && 0 === p)
                return {
                    max: 0,
                    min: 0,
                    stepHeight: 0
                };
            var d = e(t, p, h)
                , u = s(d)
                , m = d * Math.pow(10, Math.abs(u) + 1)
                , f = Math.floor(p / d + 1);
            return f = h >= 0 ? t : t - Math.floor(Math.abs(h) / d + 1),
                1 === c ? (l.max = 1 * o(f * m, -Math.abs(u) - 1),
                    l.min = 1 * -o((t - f) * m, -Math.abs(u) - 1)) : (l.min = 1 * o(f * m, -Math.abs(u) - 1) * c,
                    l.max = 1 * -o((t - f) * m, -Math.abs(u) - 1) * c),
                l.stepHeight = d,
                l
        }
        function o(t, i) {
            var o = t;
            if (t.toString().indexOf("e+") > 0) {
                for (var e = t.toString().split("e+"), n = e[0].split("."), s = parseInt(e[1]), a = n[1] ? n[1].length : 0, r = n[0] + (n[1] ? n[1] : ""), p = 0, h = s - a; h > p; p++)
                    r += "0";
                o = r
            }
            var l = t + "";
            l = o + "";
            var h = l.length
                , c = l.split("")
                , d = l.indexOf(".");
            -1 === d && (d = h);
            var u = d + i;
            if (c.splice(d, 1),
            u >= h)
                for (var p = 0; u - h >= p; p++)
                    c.push("0");
            else if (0 >= u) {
                for (var p = 0; p >= u; p--)
                    c.unshift("0");
                u = 1
            }
            return d > u ? c.splice(u, 0, ".") : c.splice(u, 0, "."),
                c.join("")
        }
        function e(t, i, o) {
            var e = t
                , s = i;
            if (0 > o && i > 0 && (s = i + Math.abs(o),
                e = t - 1),
            0 === s && 0 === t)
                return 0;
            var a = s / e;
            return n(a)
        }
        function n(t) {
            var i = s(t)
                , o = t + "";
            0 > i && (o = t.toFixed(Math.abs(i) + 2));
            var e, n, o = o.replace(/\./g, "");
            return e = (o + "").match(/[1-9]/g),
                e = e[0] || 0,
                n = o.indexOf(e + "") + 1 > o.length ? "0" : o.charAt(o.indexOf(e + "") + 1),
                3 >= n ? n = "5" : (n = 0,
                    e = 1 * e + 1,
                10 === e && (e = "1",
                    n = "0",
                    i += 1)),
            1 * ((e + "" + n) * Math.pow(10, i - 1)).toFixed(Math.abs(i) + 2)
        }
        function s(t) {
            var i = (t + "").split(".");
            return 0 == i[0] && i[1] ? -(i[1].match(/^[0]*/g) + "").length - 1 : i[0].length - 1
        }
        t.exports = i
    }
    , function(t) {
        function i() {
            var t = this.options.series
                , i = this.options.unit_w_len
                , o = this.options.unit_w_kind
                , e = this.options.coordinate
                , n = e.max
                , s = e.min
                , a = e.stepHeight
                , r = this.options.sepeNum
                , p = this.options.canvas
                , h = this.options.context
                , l = this.options.padding.top
                , c = this.options.padding.left
                , d = (this.options.padding.right,
                this.options.padding.bottom)
                , u = p.height - d - l
                , m = l + n / a * u / r;
            h.beginPath(),
                h.save(),
                h.lineWidth = this.options.dpr,
                h.fillStyle = this.options.series.color;
            for (var f = 0, v = t.data.length; v > f; f++) {
                var g = o
                    , y = u * (t.data[f] / (n - s))
                    , A = f * i + c + (i - o) / 2
                    , x = m - y;
                h.fillRect(Math.round(A), Math.round(x), Math.round(g), Math.round(y))
            }
            h.restore()
        }
        t.exports = i
    }
    , function(t, i, o) {
        function e(t, i) {
            function o(t, i) {
                h.fillStyle = i;
                var o = M * (v.data[t] / (w - _))
                    , e = t * g + (g - y) / 2 + u
                    , n = o > 0 ? k - o : k
                    , s = y;
                o = Math.abs(o),
                    h.clearRect(Math.round(e), Math.round(n), Math.round(s), Math.round(o)),
                    h.fillRect(Math.round(e), Math.round(n), Math.round(s), Math.round(o))
            }
            var e, s, a, r, p = this.options.dpr, h = this.options.context, l = t * p, c = i * p, d = this.options.padding.top, u = this.options.padding.left, m = this.options.padding.right, f = this.options.padding.bottom, v = this.options.series, g = this.options.unit_w_len, y = this.options.unit_w_kind, A = this.options.canvas, x = this.options.coordinate, w = x.max, _ = x.min, b = x.stepHeight, T = this.options.sepeNum, M = A.height - d - f, k = d + w / b * M / T, C = Math.floor((l - u) / g), N = !1;
            if (0 > C || C >= v.data.length ? N = !1 : (e = M * (v.data[C] / (w - _)),
                s = y,
                a = C * g + (g - y) / 2 + u,
                r = e > 0 ? k - e : k,
            l >= a && a + s >= l && c >= r && c <= r + Math.abs(e) && (N = !0)),
                N)
                if (void 0 !== this.options.preColume && o(this.options.preColume, v.color),
                    this.options.preColume = C,
                    o(C, v.hoverColor),
                    this.options.tipPanel) {
                    var E, S, D = this.options.tipPanel, L = (k - e) / p, R = a / p + y / p / 2, I = e > 0 ? y / p / 2 - D.clientHeight : -y / p / 2;
                    D.children[0].innerHTML = this.options.xaxis[C].value,
                        D.children[1].innerHTML = (void 0 === v.name ? "" : v.name + ":") + v.data[C] + (void 0 === v.suffix ? "" : v.suffix),
                        S = d > L + I ? d / p + 10 : L + I > A.height - f ? f / p - D.clientHeight - 10 : L + I,
                        E = R * p > (A.width - m) / 2 ? a / p + y / p / 2 - D.clientWidth : a / p + y / p / 2,
                        "hidden" === this.options.tipPanel.style.visibility ? (D.style.top = S + "px",
                            D.style.left = E + "px",
                            this.options.tipPanel.style.visibility = "visible") : n.fast2slow(D, E, S)
                } else {
                    var D = document.createElement("div")
                        , Y = document.createElement("strong")
                        , W = document.createElement("div");
                    Y.innerHTML = this.options.xaxis[C].value,
                        W.innerHTML = (void 0 === v.name ? "" : v.name + ":") + v.data[C] + (void 0 === v.suffix ? "" : v.suffix),
                        D.appendChild(Y),
                        D.appendChild(W),
                        this.container.appendChild(D),
                        this.options.tipPanel = D,
                        D.style.position = "absolute",
                        D.style.mineHeight = "30px",
                        D.style.paddingRight = "10px",
                        D.style.opacity = "0.5",
                        D.style.backgroundColor = "#4C4C4C",
                        D.style.borderRadius = "5px",
                        D.style.padding = "5px",
                        D.style.color = "white",
                        Y.style.whiteSpace = "nowrap",
                        W.style.margin = "0px";
                    var L = (k - e) / p
                        , I = e > 0 ? y / p / 2 - D.clientHeight : -y / p / 2
                        , R = a / p + y / p / 2;
                    D.style.top = d > L + I ? d / p + 10 + "px" : L + I > A.height - f ? f / p - D.clientHeight - 10 + "px" : L + I + "px",
                        D.style.left = R * p > (A.width - m) / 2 ? a / p + y / p / 2 - D.clientWidth + "px" : a / p + y / p / 2 + "px"
                }
            else
                void 0 !== this.options.preColume && (tempCurrent = this.options.preColume,
                    o(tempCurrent, v.color)),
                this.options.tipPanel && (this.options.tipPanel.style.visibility = "hidden")
        }
        var n = o(41);
        t.exports = e
    }
    , function(t) {
        var i = {
            fast2slow: function(t, i, o) {
                var e = parseInt(t.style.left)
                    , n = parseInt(t.style.top)
                    , s = (o - n) / 5
                    , a = (i - e) / 5;
                if (0 === a && 0 === s)
                    return void clearTimeout(t.timer);
                if (0 === a ? s = Math.abs(s) <= 1 ? 1 * s / Math.abs(s) : s : 0 === s ? a = Math.abs(a) <= 1 ? 1 * a / Math.abs(a) : a : (s = Math.abs(s) <= 1 ? 1 * s / Math.abs(s) : s,
                    a = Math.abs(a) <= 1 ? 1 * a / Math.abs(a) : a),
                Math.abs(i - (e + a)) <= 1 && Math.abs(o - (n + s)) <= 1)
                    return void clearTimeout(t.timer);
                Math.abs(i - e) <= 1 && (a = 0),
                Math.abs(o - n) <= 1 && (s = 0),
                    t.style.left = e + a + "px",
                    t.style.top = n + s + "px";
                var r = this;
                t.timer = setTimeout(function() {
                    r.fast2slow(t, i, o)
                }, 10)
            }
        };
        t.exports = i
    }
    , function(t, i, o) {
        var e = o(43)
            , n = o(7)
            , s = o(44)
            , a = o(21)
            , r = o(22)
            , p = o(13)
            , h = o(38)
            , l = function() {
            function t(t) {
                this.defaultoptions = n.defaulttheme,
                    this.options = a(this.defaultoptions, t),
                    this.container = document.getElementById(t.container),
                    this.container.className = this.container.className + " emcharts-container",
                    this.onChartLoaded = void 0 == t.onChartLoaded ? function() {}
                        : t.onChartLoaded
            }
            function i(t, i) {
                var o = t / i
                    , e = o * (1 - this.options.groupSpacing);
                return {
                    rect_w: o,
                    bar_w: e
                }
            }
            function o(t, i) {
                var o = t / i
                    , e = o * (1 - this.options.groupUnitSpacing);
                return {
                    rect_w: o,
                    bar_w: e
                }
            }
            function l(t) {
                var i = this.options.data.max - this.options.data.min;
                if (t >= 0 && this.options.data.min < 0) {
                    var o = this.options.c_1_height * this.options.data.max / i;
                    return o - this.options.c_1_height * t / i
                }
                if (t >= 0 && this.options.data.min >= 0) {
                    var o = this.options.c_1_height;
                    return o - this.options.c_1_height * (t - this.options.data.min) / i
                }
                if (0 > t && this.options.data.max >= 0) {
                    var e = this.options.c_1_height * this.options.data.max / i;
                    return this.options.c_1_height * Math.abs(t) / i + e
                }
                return 0 > t && this.options.data.max < 0 ? this.options.c_1_height * Math.abs(t) / i + 0 : void 0
            }
            function c(t, i) {
                var o = this.options.group
                    , e = this.options.groupUnit
                    , n = this.options.padding_left
                    , s = this.options.group.rect_w - this.options.group.bar_w
                    , a = this.options.groupUnit.rect_w - this.options.groupUnit.bar_w;
                return o.rect_w * t + n + e.rect_w * i + s / 2 + a / 2
            }
            function d(t) {
                var i = t * this.options.dpr
                    , o = {}
                    , e = this.options.padding_left
                    , n = this.options.group
                    , s = this.options.groupUnit
                    , a = (this.options.canvas,
                    this.options.series)
                    , r = Math.floor((i - e) / n.rect_w);
                0 > r && (r = 0);
                var p = Math.floor((i - e - r * n.rect_w - (n.rect_w - n.bar_w) / 2) / s.rect_w);
                return 0 > p ? p = 0 : p > 3 && (p = 3),
                    o.midddleLine = c.call(this, r, p) + s.bar_w / 2,
                    o.tipsX = this.options.padding_left + n.rect_w * r,
                    o.tipsY = l.call(this, a[r].data[0]) + this.options.canvas_offset_top,
                    o.midddleLineHeight = o.tipsY,
                    o.content = {},
                    o.content.series = this.options.series[r].data,
                    o.content.colors = this.options.xaxis[r],
                    o.content.names = this.options.xaxis[r].names,
                    o.content.suffixs = this.options.xaxis[r].suffixs,
                    o.arr = r + ":" + p,
                    o
            }
            function u(t) {
                for (var i = t.length, o = [], e = 0; i > e; e++)
                    o = o.concat(t[e].data);
                var n = h(this.options.sepeNum, o)
                    , s = this.options.context
                    , a = s.measureText(p.format_unit(n.stepHeight)).width - s.measureText(p.format_unit(parseInt(n.stepHeight))).width
                    , r = s.measureText(p.format_unit(parseInt(n.max))).width
                    , l = s.measureText(p.format_unit(parseInt(n.min))).width
                    , c = r > l ? r : l
                    , d = c + a;
                return {
                    max: n.max,
                    min: n.min,
                    step: n.stepHeight,
                    maxPaddingLeftWidth: d
                }
            }
            return t.prototype.init = function() {
                this.options.type = "group-bar";
                var t = document.createElement("canvas");
                this.container.style.position = "relative";
                try {
                    var i = t.getContext("2d")
                } catch (o) {
                    t = window.G_vmlCanvasManager.initElement(t);
                    var i = t.getContext("2d")
                }
                this.options.canvas = t,
                    this.options.context = i,
                    this.options.sepeNum = void 0 == this.options.sepeNum ? 4 : this.options.sepeNum,
                this.options.sepeNum < 2 && (this.options.sepeNum = 2),
                    this.container.appendChild(t);
                var e = this.options.dpr = 1;
                t.width = this.options.width * e,
                    t.height = this.options.height * e,
                    this.options.canvas_offset_top = 15 * e,
                    this.options.c_1_height = t.height - 50 * e,
                    t.style.width = this.options.width + "px",
                    t.style.height = this.options.height + "px",
                    t.style.border = "0";
                var n = this.options.series
                    , t = this.options.canvas
                    , s = u.apply(this, [n]);
                s.min < 0 && (this.options.isLessZero = !0),
                    this.options.data = {},
                    this.options.data.max = s.max,
                    this.options.data.min = s.min,
                    this.options.data.step = s.step,
                    this.options.padding_left = s.maxPaddingLeftWidth * e + 30,
                    this.options.drawWidth = t.width - 10,
                    i.translate("0", this.options.canvas_offset_top),
                    i.font = this.options.font_size * this.options.dpr + "px Arial",
                    i.lineWidth = 1 * this.options.dpr,
                    this.options.groupSpacing = void 0 == this.options.groupSpacing ? "0.2" : this.options.groupSpacing,
                    this.options.groupUnitSpacing = void 0 == this.options.groupUnitSpacing ? "0.2" : this.options.groupUnitSpacing,
                    r.apply(this, [this.options.context, this.options.padding_left + 95 * e, 10 * e, 82 * e, 20 * e])
            }
                ,
                t.prototype.draw = function() {
                    this.clear(),
                        this.init(),
                        this.options.group = i.call(this, this.options.drawWidth - this.options.padding_left, this.options.series.length),
                        this.options.groupUnit = o.call(this, this.options.group.bar_w, this.options.series[0].data.length),
                        new e(this.options),
                        new s(this.options),
                        this.addInteractive()
                }
                ,
                t.prototype.addInteractive = function() {
                    var t, i = this.options.canvas, o = this.options.group, e = this, n = document.createElement("div"), s = document.createElement("div"), a = {}, r = "x:x", h = this.options.dpr, l = this.options.padding_left, c = this.options.canvas_offset_top, u = this.options.c_1_height;
                    n.className = "web-tips",
                        s.className = "group-bar-mark",
                        s.style.width = o.rect_w / h + "px",
                        s.style.backgroundColor = "#333",
                        e.container.appendChild(n),
                        e.container.appendChild(s),
                        p.addEvent.call(e, i, "mousemove", function(m) {
                            var f, v;
                            if (m.layerX ? (f = m.layerX,
                                v = m.layerY) : m.x && (f = m.x,
                                v = m.y),
                                f >= l / h && f * h < e.options.drawWidth && v >= c / h && (c + u) / h > v ? (n.style.display = "inline-block",
                                    s.style.display = "inline-block") : (n.style.display = "none",
                                    s.style.display = "none"),
                            f * h < e.options.drawWidth && (t = d.call(e, f)),
                            r !== t.arr) {
                                a.midddleLine = p.canvasToWindow.call(e, i, t.midddleLine, 0),
                                    a.tips = p.canvasToWindow.call(e, i, t.tipsX, t.tipsY);
                                var g = t.content.series
                                    , y = t.content.colors
                                    , A = t.content.names
                                    , x = t.content.suffixs;
                                n.innerHTML = "";
                                var w = document.createElement("div");
                                w.innerHTML = y.value,
                                    n.appendChild(w);
                                for (var _, b = 0; _ = g[b]; b++) {
                                    var T = document.createElement("span");
                                    T.className = "bar-color-span",
                                        T.style.backgroundColor = y.colors[b];
                                    var M = document.createElement("span");
                                    M.className = "bar-value-span",
                                        M.innerHTML = (void 0 === A ? "" : A[b]) + _ + (void 0 === x ? "" : x[b]);
                                    var k = document.createElement("div");
                                    k.className = "",
                                        k.appendChild(T),
                                        k.appendChild(M),
                                        n.appendChild(k)
                                }
                                var C = m.offsetX || m.clientX - e.container.getBoundingClientRect().left
                                    , N = m.offsetY || m.clientY - e.container.getBoundingClientRect().top;
                                n.style.left = C > i.width / 2 / h ? t.tipsX / h - n.clientWidth + "px" : (t.tipsX + e.options.group.rect_w) / h + "px",
                                    n.style.top = t.tipsY / h - n.clientHeight / 2 + "px";
                                var E = p.windowToCanvas.apply(e, [i, C, N])
                                    , S = E.x.toFixed(0);
                                if (S - e.options.padding_left > 0) {
                                    var D = Math.floor((S - e.options.padding_left) / o.rect_w);
                                    s.style.height = u / e.options.dpr + "px",
                                        s.style.left = (D * o.rect_w + e.options.padding_left) / h + "px",
                                        s.style.top = c / h + "px"
                                }
                                r = t.arr
                            }
                        })
                }
                ,
                t.prototype.reDraw = function() {
                    this.clear(),
                        this.init(),
                        this.draw()
                }
                ,
                t.prototype.clear = function(t) {
                    this.container ? this.container.innerHTML = "" : document.getElementById(this.options.container).innerHTML = "",
                    t && t()
                }
                ,
                t
        }();
        t.exports = l
    }
    , function(t, i, o) {
        var e = o(21)
            , n = o(7)
            , s = (o(13),
            o(8))
            , a = o(9)
            , r = function() {
            function t(t) {
                this.defaultoptions = n.draw_xy,
                    this.options = e(this.defaultoptions, t),
                    this.draw()
            }
            function i(t, i, o, e) {
                function n(t) {
                    return t && (t = parseFloat(t.toFixed(r.options.maxDot))),
                        t
                }
                t.save();
                var r = this
                    , p = e.length;
                t.fillStyle = "#979797",
                    t.textAlign = "right";
                for (var h, l = 0; h = e[l]; l++) {
                    t.beginPath();
                    var c = !0;
                    this.options.data.min < 0 ? this.options.data.min + this.options.data.step * l < 0 ? t.fillText(this.options.data.min + n(this.options.data.step * l), this.options.padding_left - 10, h.y + 5) : this.options.data.min + this.options.data.step * l == 0 ? (t.fillText(0, this.options.padding_left - 10, h.y + 5),
                        c = !1,
                        t.strokeStyle = "#c9c9c9",
                        t.moveTo(a(this.options.padding_left), a(h.y)),
                        t.lineTo(a(this.options.drawWidth), a(h.y)),
                        t.stroke()) : t.fillText(this.options.data.min + n(this.options.data.step * l), this.options.padding_left - 10, h.y + 5) : t.fillText(this.options.data.min + n(this.options.data.step * l), this.options.padding_left - 10, h.y + 5),
                    0 != l && l != p - 1 && c && (t.save(),
                        t.beginPath(),
                        t.strokeStyle = "#e6e6e6",
                        s(t, this.options.padding_left, Math.round(h.y), this.options.drawWidth, Math.round(h.y), 3),
                        t.restore())
                }
                t.restore()
            }
            function o(t, i, o) {
                t.save();
                var e = this.options.padding_left
                    , n = this.options.dpr;
                t.beginPath(),
                    t.strokeStyle = "#c9c9c9",
                    t.rect(a(e), .5, Math.round(this.options.drawWidth - e), Math.round(this.options.c_1_height)),
                    t.stroke(),
                    t.textAlign = "left",
                    t.fillStyle = "#979797";
                for (var s, r = this.options.drawWidth, p = o.length, h = 0; p > h; h++) {
                    t.beginPath(),
                        s = o[h].value;
                    {
                        var l = h * (r - e) / p + e;
                        void 0 == o[h].show ? !0 : !1
                    }
                    if ((void 0 == o[h].show || o[h].show) && t.fillText(s, l + ((r - e) / p - t.measureText(s).width) / 2, this.options.c_1_height + 20 * n),
                    h == p - 1) {
                        t.moveTo(a(l), a(this.options.c_1_height)),
                            t.lineTo(a(l), a(this.options.c_1_height + 5 * n));
                        var l = (h + 1) * (r - e) / p + e;
                        t.moveTo(a(l), a(this.options.c_1_height)),
                            t.lineTo(a(l), a(this.options.c_1_height + 5 * n))
                    } else
                        t.moveTo(a(l), a(this.options.c_1_height)),
                            t.lineTo(a(l), a(this.options.c_1_height + 5 * n));
                    t.stroke()
                }
                t.stroke(),
                    t.restore()
            }
            function r(t, i, o, e) {
                for (var n = this.options.data.step, s = [], a = 0; o >= a; a++)
                    s.push({
                        num: i + a * n,
                        x: 0,
                        y: e - a / o * e
                    });
                return s
            }
            return t.prototype.draw = function() {
                var t = this.options.context
                    , e = this.options.data.max
                    , n = this.options.data.min
                    , s = this.options.sepeNum
                    , a = this.options.xaxis
                    , p = this.options.c_1_height
                    , h = r.apply(this, [e, n, s, p]);
                i.call(this, t, e, n, h),
                    o.apply(this, [t, p, a])
            }
                ,
                t
        }();
        t.exports = r
    }
    , function(t, i, o) {
        var e = o(21)
            , n = o(7)
            , s = function() {
            function t(t) {
                this.defaultoptions = n.drawLine,
                    this.options = e(this.defaultoptions, t),
                    this.draw()
            }
            function i(t) {
                var i = this.options.data.max - this.options.data.min;
                if (t >= 0 && this.options.data.min < 0) {
                    var o = this.options.c_1_height * this.options.data.max / i;
                    return o - this.options.c_1_height * t / i
                }
                if (t >= 0 && this.options.data.min >= 0) {
                    var o = this.options.c_1_height;
                    return o - this.options.c_1_height * (t - this.options.data.min) / i
                }
                if (0 > t && this.options.data.max >= 0) {
                    var e = this.options.c_1_height * this.options.data.max / i;
                    return this.options.c_1_height * Math.abs(t) / i + e
                }
                return 0 > t && this.options.data.max < 0 ? this.options.c_1_height * Math.abs(t) / i + 0 : void 0
            }
            function o(t, i) {
                var o = this.options.group
                    , e = this.options.groupUnit
                    , n = this.options.padding_left
                    , s = this.options.group.rect_w - this.options.group.bar_w
                    , a = this.options.groupUnit.rect_w - this.options.groupUnit.bar_w;
                return o.rect_w * t + n + e.rect_w * i + s / 2 + a / 2
            }
            return t.prototype.draw = function() {
                var t = this.options.context;
                t.lineWidth = 1 * this.options.dpr + 1;
                for (var e, n = this.options.series, s = this.options.xaxis, a = 0; e = n[a]; a++)
                    for (var r = e.data, p = r.length, h = 0; p > h; h++) {
                        var l = r[h];
                        t.beginPath(),
                            t.fillStyle = void 0 == s[a].colors[h] ? "#333" : s[a].colors[h],
                            t.strokeStyle = void 0 == s[a].colors[h] ? "#333" : s[a].colors[h];
                        var c = o.apply(this, [a, h])
                            , d = i.call(this, l);
                        if (d >= 0 && this.options.data.min < 0) {
                            var u = this.options.c_1_height * this.options.data.max / (this.options.data.max - this.options.data.min);
                            t.rect(c, d, this.options.groupUnit.bar_w, u - d)
                        } else if (d >= 0 && this.options.data.min >= 0) {
                            var u = this.options.c_1_height;
                            t.rect(c, d, this.options.groupUnit.bar_w, u - d)
                        } else if (0 > d && this.options.data.max >= 0) {
                            var u = this.options.c_1_height * this.options.data.max / (this.options.data.max - this.options.data.min);
                            t.rect(c, u, this.options.groupUnit.bar_w, d)
                        } else if (0 > d && this.options.data.max < 0) {
                            var u = 0;
                            t.rect(c, u, this.options.groupUnit.bar_w, d)
                        }
                        t.fill()
                    }
            }
                ,
                t
        }();
        t.exports = s
    }
    , function(t, i, o) {
        var e = o(46)
            , n = o(7)
            , s = o(47)
            , a = o(21)
            , r = o(22)
            , p = o(13)
            , h = function() {
            function t(t) {
                this.defaultoptions = n.chartLine,
                    this.options = a(this.defaultoptions, n.defaulttheme, t),
                    this.container = document.getElementById(t.container),
                    this.container.className = this.container.className.replace(/emcharts-container/g, "").trim(),
                    this.container.className = this.container.className + " emcharts-container",
                    this.onChartLoaded = void 0 == t.onChartLoaded ? function() {}
                        : t.onChartLoaded
            }
            function i(t, i) {
                var o = {};
                o.showLine = !1,
                    o.showTips = !1;
                var e = this.options.canvas
                    , n = this.options.padding_left
                    , s = this.options.canvas_offset_top
                    , a = this.options.pointRadius = void 0 == this.options.pointRadius ? 5 : this.options.pointRadius
                    , r = this.options.dpr
                    , h = this.options.series
                    , l = this.options.xaxis
                    , c = (e.width - n) / (l.length - 1)
                    , d = (t - n + c / 2) / c;
                d = 0 > d ? 0 : Math.floor(d);
                for (var u = (e.width - n) / (l.length - 1) * d + n, m = 0; m < h.length; m++) {
                    var f = p.get_y.call(this, h[m].data[d]);
                    Math.abs(f - i + s) < 2 * a && Math.abs(u - t + a) < 2 * a && d != l.length - 1 && (o.showTips = !0,
                        o.pointY = f + s / r,
                        o.pointX = u,
                        o.content = h[m].name + " : " + h[m].data[d])
                }
                return Math.abs(u - t) < 2 * a && (0 !== d && d !== l.length - 1 ? (o.showLine = !0,
                    o.lineX = u) : o.showLine = !1),
                    o
            }
            function o(t) {
                for (var i = 0, o = 0, e = t.length, n = {}, s = 0; e > s; s++)
                    for (var a = 0; a < t[s].data.length; a++)
                        0 == s && 0 == a && (i = t[s].data[a],
                            o = t[s].data[a]),
                            i = Math.max(i, t[s].data[a]),
                            o = Math.min(o, t[s].data[a]);
                return n.max = i + .05 * Math.abs(i - o),
                    n.min = o,
                    n
            }
            return t.prototype.init = function() {
                this.options.type = "line";
                var t = document.createElement("canvas");
                this.container.style.position = "relative",
                    this.container.appendChild(t);
                try {
                    var i = t.getContext("2d")
                } catch (o) {
                    t = window.G_vmlCanvasManager.initElement(t);
                    var i = t.getContext("2d")
                }
                this.options.canvas = t,
                    this.options.context = i;
                var e = this.options.dpr = 1;
                t.width = this.options.width * e,
                    t.height = this.options.height * e,
                    this.options.canvas_offset_top = t.height / 18,
                    this.options.padding_left = t.width / 6,
                    this.options.k_v_away = t.height / 18,
                    this.options.scale_count = 0,
                    this.options.c_1_height = t.height * (8 / 9) - 10,
                    t.style.width = this.options.width + "px",
                    t.style.height = this.options.height + "px",
                    t.style.border = "0",
                    i.translate("0", this.options.canvas_offset_top),
                    i.font = this.options.font_size * this.options.dpr + "px Arial",
                    i.lineWidth = 1 * this.options.dpr,
                    r.apply(this, [this.options.context, 90, 20, 82, 20])
            }
                ,
                t.prototype.draw = function() {
                    this.clear(),
                        this.init();
                    var t = this.options.context
                        , i = this.options.series
                        , n = o(i);
                    this.options.data = {},
                        this.options.data.max = n.max,
                        this.options.data.min = n.min,
                        this.options.padding_left = t.measureText("+9000万").width + 10,
                        this.options.drawWidth = t.canvas.width - this.options.padding_left,
                        new e(this.options),
                        new s(this.options),
                        this.addInteractive()
                }
                ,
                t.prototype.reDraw = function() {
                    this.clear(),
                        this.init(),
                        this.draw()
                }
                ,
                t.prototype.clear = function(t) {
                    this.container ? this.container.innerHTML = "" : document.getElementById(this.options.container).innerHTML = "",
                    t && t()
                }
                ,
                t.prototype.addInteractive = function() {
                    var t = this.options.canvas
                        , o = this
                        , e = document.createElement("div")
                        , n = document.createElement("div")
                        , s = this.options.dpr ? this.options.dpr : 1
                        , a = this.options.padding_left
                        , r = this.options.canvas_offset_top / s
                        , h = this.options.c_1_height / s
                        , l = this.options.pointRadius;
                    e.className = "web-tips",
                        n.className = "web-middleLine",
                        o.container.appendChild(e),
                        o.container.appendChild(n),
                        p.addEvent.call(o, t, "mousemove", function(p) {
                            var c, d;
                            p.layerX ? (c = p.layerX,
                                d = p.layerY) : p.x && (c = p.x,
                                d = p.y),
                            c * s >= a - l && d >= r && r + h >= d || (e.style.display = "none",
                                n.style.display = "none");
                            var u = i.call(o, c * o.options.dpr, d * o.options.dpr);
                            u.showLine && d >= r && r + h >= d ? (n.style.display = "inline-block",
                                n.style.height = h + "px",
                                n.style.left = u.lineX / s + "px",
                                n.style.top = r + "px") : n.style.display = "none",
                                u.showTips ? (e.style.display = "inline-block",
                                    e.innerHTML = u.content,
                                    e.style.left = c * s - a < t.width / 2 ? u.pointX / s + l + "px" : u.pointX / s - l - e.clientWidth + "px",
                                    e.style.top = u.pointY / s + l + "px") : e.style.display = "none"
                        })
                }
                ,
                t
        }();
        t.exports = h
    }
    , function(t, i, o) {
        var e = o(6)
            , n = o(7)
            , s = o(13)
            , a = o(9)
            , r = function() {
            function t(t) {
                this.defaultoptions = n.draw_xy,
                    this.options = {},
                    e(!1, this.options, this.defaultoptions, t),
                    this.draw()
            }
            function i(t, i, o, e) {
                t.fillStyle = "#b1b1b1",
                    t.strokeStyle = "#ccc",
                    t.textAlign = "right";
                for (var n, r = 0; n = e[r]; r++)
                    t.beginPath(),
                    0 == r && (t.moveTo(a(this.options.padding_left), a(n.y)),
                        t.lineTo(a(t.canvas.width), a(n.y)),
                        t.stroke()),
                        t.fillText(s.format_unit(n.num / 1, 2), a(this.options.padding_left - 10), a(n.y)),
                    this.options.bothmark && t.fillText(s.format_unit(n.num / 1, 2), a(t.canvas.width - 10), a(n.y))
            }
            function o(t, i, o) {
                var e = this.options.padding_left;
                t.beginPath(),
                    t.textAlign = "center",
                    t.fillStyle = "#b1b1b1";
                for (var n, s = t.canvas.width, r = o.length, p = 0; r > p; p++)
                    if (n = o[p],
                    (void 0 == n.show ? !0 : n.show) && t.fillText(n.value, a(p * (s - e) / (r - 1) + e), a(this.options.c_1_height + 20)),
                        void 0 == n.showline ? !0 : n.showline && (0 == p || p == r - 1)) {
                        if (t.strokeStyle = "#ccc",
                        p == r - 1)
                            var h = p * (s - e) / (r - 1) + e - 1;
                        else
                            var h = p * (s - e) / (r - 1) + e;
                        t.moveTo(a(h), .5),
                            t.lineTo(a(h), a(this.options.c_1_height))
                    }
                t.stroke()
            }
            function r(t, i, o, e) {
                for (var n = (t - i) / (o - 1), s = [], a = 0; o > a; a++)
                    s.push({
                        num: i + a * n,
                        x: 0,
                        y: e - a / (o - 1) * e
                    });
                return s
            }
            return t.prototype.draw = function() {
                var t = this.options.context
                    , e = this.options.data.max
                    , n = this.options.data.min
                    , s = this.options.sepenum || 10
                    , a = this.options.xaxis
                    , p = this.options.c_1_height
                    , h = r(e, n, s, p);
                i.call(this, t, e, n, h),
                    o.apply(this, [t, p, a])
            }
                ,
                t
        }();
        t.exports = r
    }
    , function(t, i, o) {
        var e = o(6)
            , n = o(7)
            , s = o(13)
            , a = function() {
            function t(t) {
                this.defaultoptions = n.drawLine,
                    this.options = {},
                    e(!1, this.options, this.defaultoptions, t),
                    this.draw()
            }
            function i(t, i) {
                t.save();
                var o = i.data
                    , e = o.length;
                t.beginPath();
                for (var n, a = 0; n = o[a]; a++) {
                    var r = (t.canvas.width - this.options.padding_left) / (e - 1) * a + this.options.padding_left
                        , p = s.get_y.call(this, n);
                    0 == a ? t.moveTo(this.options.padding_left, p) : a == e - 1 ? t.lineTo(r, p) : t.lineTo(r, p)
                }
                t.stroke(),
                    t.restore()
            }
            function o(t, i) {
                t.save();
                for (var o, e = i.data, n = e.length, a = this.options.pointRadius, r = 0; o = e[r]; r++) {
                    t.beginPath();
                    var p = (t.canvas.width - this.options.padding_left) / (n - 1) * r + this.options.padding_left
                        , h = s.get_y.call(this, o);
                    0 == r ? (t.arc(p, h, a, 0, 2 * Math.PI, !0),
                        t.fill()) : r == n - 1 || (t.arc(p, h, a, 0, 2 * Math.PI, !0),
                        t.fill())
                }
                t.restore()
            }
            function a(t, i) {
                t.save();
                for (var o, e = this.options.dpr, n = t.canvas.width / 2, s = this.options.lineMarkWidth * e, a = 0, r = t.canvas.height * (7 / 9 - 1 / 18), p = 0; o = i[p]; p++) {
                    t.beginPath(),
                        t.strokeStyle = "#cadef8";
                    var h = Math.floor(p / 2) * (s + 7 * e)
                        , l = this.options.font_size * this.options.dpr + (s - this.options.font_size * this.options.dpr) / 2;
                    0 == p ? (t.fillStyle = o.color,
                        t.rect(a + 20, r, s, s),
                        t.fill(),
                        t.fillStyle = "#333",
                        t.fillText(o.name, a + s + 80, r + l)) : (p + 1) % 2 == 0 ? (t.fillStyle = o.color,
                        t.rect(n, r + h, s, s),
                        t.fill(),
                        t.fillStyle = "#333",
                        t.fillText(o.name, n + s + 60, r + h + l)) : (t.fillStyle = o.color,
                        t.rect(a + 20, r + h, s, s),
                        t.fill(),
                        t.fillStyle = "#333",
                        t.fillText(o.name, a + s + 80, r + h + l))
                }
                t.restore()
            }
            return t.prototype.draw = function() {
                var t = this.options.context;
                t.lineWidth = 1 * this.options.dpr + 1;
                for (var e, n = this.options.series, s = 0; e = n[s]; s++)
                    t.fillStyle = void 0 == e.color ? "#333" : e.color,
                        t.strokeStyle = void 0 == e.color ? "#333" : e.color,
                        i.apply(this, [t, e]),
                    e.showpoint && o.apply(this, [t, e]);
                this.options.showflag && a.apply(this, [t, n])
            }
                ,
                t
        }();
        t.exports = a
    }
    , function(t, i, o) {
        var e = o(49)
            , n = o(7)
            , s = o(50)
            , a = o(21)
            , r = o(22)
            , p = o(13)
            , h = o(38)
            , l = function() {
            function t(t) {
                this.defaultoptions = n.defaulttheme,
                    this.options = a(this.defaultoptions, t),
                    this.container = document.getElementById(t.container),
                    this.container.className = this.container.className.replace(/emcharts-container/g, "").trim(),
                    this.container.className = this.container.className + " emcharts-container",
                    this.onChartLoaded = void 0 == t.onChartLoaded ? function() {}
                        : t.onChartLoaded
            }
            function i(t, i) {
                var o = t / i
                    , e = o * (1 - this.options.yearUnitSpacing);
                return {
                    rect_w: o,
                    bar_w: e
                }
            }
            function o(t, i) {
                var o = t / i
                    , e = o * (1 - this.options.quarterUnitSpacing);
                return {
                    rect_w: o,
                    bar_w: e
                }
            }
            function l(t) {
                return this.options.isLessZero ? this.options.c_1_height - this.options.c_1_height * (t - this.options.data.min) / (this.options.data.max - this.options.data.min) : this.options.c_1_height - this.options.c_1_height * t / this.options.data.max
            }
            function c(t, i) {
                var o = this.options.yearUnit
                    , e = this.options.quarterUnit
                    , n = this.options.padding_left
                    , s = this.options.yearUnit.rect_w - this.options.yearUnit.bar_w
                    , a = this.options.quarterUnit.rect_w - this.options.quarterUnit.bar_w;
                return o.rect_w * t + n + e.rect_w * i + s / 2 + a / 2
            }
            function d(t) {
                var i = t * this.options.dpr
                    , o = {}
                    , e = this.options.padding_left
                    , n = this.options.yearUnit
                    , s = this.options.quarterUnit
                    , a = this.options.canvas
                    , r = this.options.series
                    , p = Math.floor((i - e) / n.rect_w);
                0 > p && (p = 0);
                var h = Math.floor((i - e - p * n.rect_w - (n.rect_w - n.bar_w) / 2) / s.rect_w);
                return 0 > h ? h = 0 : h > 3 && (h = 3),
                    o.midddleLine = c.call(this, p, h) + s.bar_w / 2,
                    o.tipsX = o.midddleLine + 3 * s.bar_w / 4,
                    o.tipsY = l.call(this, r[p].data[h]),
                i > a.width / 2 && (o.tipsX = o.midddleLine - 3 * s.bar_w / 4),
                this.options.series[p].data[h] < 0 && (o.tipsY -= 25),
                    o.midddleLineHeight = o.tipsY,
                    o.content = this.options.series[p].data[h],
                    o.arr = p + ":" + h,
                    o
            }
            function u(t) {
                for (var i = t.length, o = [], e = 0; i > e; e++)
                    o = o.concat(t[e].data);
                var n = h(this.options.sepeNum, o)
                    , s = this.options.context
                    , a = s.measureText(p.format_unit(n.stepHeight)).width - s.measureText(p.format_unit(parseInt(n.stepHeight))).width
                    , r = s.measureText(p.format_unit(parseInt(n.max))).width
                    , l = s.measureText(p.format_unit(parseInt(n.min))).width
                    , c = r > l ? r : l
                    , d = c + a;
                return {
                    max: n.max,
                    min: n.min,
                    step: n.stepHeight,
                    maxPaddingLeftWidth: d
                }
            }
            return t.prototype.init = function() {
                this.options.type = "bar-quarter";
                var t = document.createElement("canvas");
                this.container.appendChild(t),
                    this.container.style.position = "relative";
                try {
                    var i = t.getContext("2d")
                } catch (o) {
                    t = window.G_vmlCanvasManager.initElement(t);
                    var i = t.getContext("2d")
                }
                this.options.canvas = t,
                    this.options.context = i,
                    this.options.sepeNum = this.options.sepeNum || 4;
                var e = this.options.dpr = 1;
                t.width = this.options.width * e,
                    t.height = this.options.height * e,
                    this.options.canvas_offset_top = t.height / 5 / 4,
                    this.options.c_1_height = 4 * t.height / 5,
                    t.style.width = this.options.width + "px",
                    t.style.height = this.options.height + "px",
                    t.style.border = "0",
                    i.translate("0", this.options.canvas_offset_top),
                    i.font = this.options.font_size * this.options.dpr + "px Arial",
                    i.lineWidth = 1 * this.options.dpr,
                    this.options.yearUnitSpacing = "0.2",
                    this.options.quarterUnitSpacing = "0.4",
                    r.apply(this, [this.options.context, 90, 20, 82, 20])
            }
                ,
                t.prototype.draw = function() {
                    this.clear(),
                        this.init();
                    var t = this.options.series
                        , n = this.options.canvas
                        , a = u.call(this, t);
                    a.min < 0 && (this.options.isLessZero = !0),
                        this.options.data = {},
                        this.options.data.max = a.max,
                        this.options.data.min = a.min,
                        this.options.data.step = a.step;
                    var r = this.options.context;
                    this.options.padding_left = a.maxPaddingLeftWidth + 15,
                        this.options.drawWidth = r.canvas.width - this.options.padding_left,
                        this.options.yearUnit = i.call(this, n.width - this.options.padding_left, this.options.series.length),
                        this.options.quarterUnit = o.call(this, this.options.yearUnit.bar_w, 4);
                    var p = new e(this.options);
                    this.options.padding_left = p.options.padding_left,
                        new s(this.options),
                        this.addInteractive()
                }
                ,
                t.prototype.addInteractive = function() {
                    var t, i = this.options.canvas, o = this.options.yearUnit, e = this, n = document.createElement("div"), s = document.createElement("div"), a = {}, r = "x:x", h = this.options.dpr, l = this.options.padding_left, c = this.options.canvas_offset_top, u = this.options.c_1_height;
                    n.className = "web-tips",
                        s.className = "web-middleLine",
                        e.container.appendChild(n),
                        e.container.appendChild(s),
                        p.addEvent.call(e, i, "mousemove", function(m) {
                            var f, v;
                            m.layerX ? (f = m.layerX,
                                v = m.layerY) : m.x && (f = m.x,
                                v = m.y),
                                f >= l && f < i.width - (o.rect_w - o.bar_w) / 2 && v >= c && c * h + u > v * h ? (n.style.display = "inline-block",
                                    s.style.display = "inline-block") : (n.style.display = "none",
                                    s.style.display = "none"),
                                t = d.call(e, f),
                            r !== t.arr && (a.midddleLine = p.canvasToWindow.call(e, i, t.midddleLine, 0),
                                a.tips = p.canvasToWindow.call(e, i, t.tipsX, t.tipsY),
                                n.innerHTML = p.format_unit(t.content, 3) + (e.options.suffix || ""),
                                n.style.left = f > i.width / 2 ? t.tipsX - n.clientWidth + "px" : t.tipsX - n.style.width + "px",
                                n.style.top = t.tipsY * h + n.clientHeight + "px",
                                s.style.height = u + "px",
                                s.style.left = a.midddleLine.x + "px",
                                s.style.top = c + "px",
                                r = t.arr)
                        })
                }
                ,
                t.prototype.reDraw = function() {
                    this.clear(),
                        this.init(),
                        this.draw()
                }
                ,
                t.prototype.clear = function(t) {
                    this.container ? this.container.innerHTML = "" : document.getElementById(this.options.container).innerHTML = "",
                    t && t()
                }
                ,
                t
        }();
        t.exports = l
    }
    , function(t, i, o) {
        var e = o(21)
            , n = o(7)
            , s = o(13)
            , a = o(9)
            , r = function() {
            function t(t) {
                this.defaultoptions = n.draw_xy,
                    this.options = e(this.defaultoptions, t),
                    this.draw()
            }
            function i(t, i, o, e) {
                t.fillStyle = "#000",
                    t.strokeStyle = "#eeeeee",
                    t.textAlign = "right",
                    t.textBaseline = "middle";
                for (var n, r = 0; r < e.length; r++)
                    n = e[r],
                        t.beginPath(),
                    0 !== r && r !== e.length - 1 && (t.moveTo(this.options.padding_left, a(n.y)),
                        t.lineTo(t.canvas.width, a(n.y))),
                        t.fillText(s.format_unit(n.num, 0), this.options.padding_left - 5, n.y),
                        t.stroke()
            }
            function o(t, i) {
                var o = [];
                t.forEach(function(t) {
                    var e = i.measureText(s.format_unit(parseInt(t.num))).width;
                    o.push(e)
                }),
                    o.sort(function(t, i) {
                        return i - t
                    }),
                    this.options.padding_left = o[0] + 10
            }
            function r(t, i, o) {
                var e = this.options.padding_left;
                t.beginPath(),
                    t.strokeStyle = "#9f9f9f",
                    t.rect(a(e) - 1, a(0), Math.round(t.canvas.width - e), Math.round(this.options.c_1_height)),
                    t.stroke(),
                    t.closePath(),
                    t.beginPath(),
                    t.textAlign = "left",
                    t.textBaseline = "top",
                    t.fillStyle = "#000";
                for (var n, s = t.canvas.width, r = o.length, p = 0; r > p; p++)
                    n = o[p].value,
                        t.fillText(n, p * (s - e) / r + e + ((s - e) / r - t.measureText(n).width) / 2, this.options.c_1_height + 5);
                t.stroke()
            }
            function p() {
                for (var t = (this.options.canvas.width - this.options.padding_left) / this.options.series.length, i = this.options.context, o = 0; o < this.options.series.length; o++)
                    if (o % 2 == 0) {
                        i.beginPath();
                        var e = i.createLinearGradient(0, 0, 0, this.options.c_1_height);
                        e.addColorStop(0, "rgba(255,255,255,0)"),
                            e.addColorStop(1, "rgba(245,245,245,1)"),
                            i.fillStyle = e,
                            i.rect(a(this.options.padding_left + o * t), a(0), Math.round(t), Math.round(this.options.c_1_height)),
                            i.fill(),
                            i.closePath()
                    }
            }
            function h(t, i, o, e) {
                for (var n = this.options.data.step, s = [], a = 0; o >= a; a++)
                    s.push({
                        num: i + a * n,
                        x: 0,
                        y: e - a / o * e
                    });
                return s
            }
            return t.prototype.draw = function() {
                var t = this.options.context
                    , e = this.options.data.max
                    , n = this.options.data.min
                    , s = this.options.sepeNum
                    , a = this.options.xaxis
                    , l = this.options.c_1_height
                    , c = h.call(this, e, n, s, l);
                o.call(this, c, t),
                    p.call(this, t),
                    i.call(this, t, e, n, c),
                    r.apply(this, [t, l, a])
            }
                ,
                t
        }();
        t.exports = r
    }
    , function(t, i, o) {
        var e = o(21)
            , n = o(7)
            , s = function() {
            function t(t) {
                this.defaultoptions = n.drawLine,
                    this.options = e(this.defaultoptions, t),
                    this.draw()
            }
            function i(t) {
                var i = this.options.data.max
                    , o = this.options.data.min
                    , e = this.options.c_1_height
                    , n = e * (i / (i - o));
                return n - t / (i - o) * e
            }
            function o(t, i) {
                var o = this.options.yearUnit
                    , e = this.options.quarterUnit
                    , n = this.options.padding_left
                    , s = this.options.yearUnit.rect_w - this.options.yearUnit.bar_w
                    , a = this.options.quarterUnit.rect_w - this.options.quarterUnit.bar_w;
                return o.rect_w * t + n + e.rect_w * i + s / 2 + a / 2
            }
            return t.prototype.draw = function() {
                var t = this.options.context;
                t.lineWidth = 1 * this.options.dpr + 1;
                for (var e, n = this.options.series, s = this.options.xaxis, a = 0; e = n[a]; a++)
                    for (var r = e.data, p = 0; p < r.length; p++) {
                        var h = r[p];
                        t.beginPath(),
                            t.fillStyle = void 0 == s[a].colors[p] ? "#333" : s[a].colors[p],
                            t.strokeStyle = void 0 == s[a].colors[p] ? "#333" : s[a].colors[p];
                        var l = o.apply(this, [a, p])
                            , c = i.call(this, h);
                        t.rect(l, c, this.options.quarterUnit.bar_w, h / (this.options.data.max - this.options.data.min) * this.options.c_1_height),
                            t.fill()
                    }
            }
                ,
                t
        }();
        t.exports = s
    }
    , function(t, i, o) {
        var e = o(52)
            , n = o(7)
            , s = o(53)
            , a = o(21)
            , r = o(22)
            , p = o(13)
            , h = function() {
            function t(t) {
                this.defaultoptions = n.defaulttheme,
                    this.options = a(this.defaultoptions, t),
                    this.container = document.getElementById(t.container),
                    this.container.className = this.container.className.replace(/emcharts-container/g, "").trim(),
                    this.container.className = this.container.className + " emcharts-container",
                    this.onChartLoaded = void 0 == t.onChartLoaded ? function() {}
                        : t.onChartLoaded
            }
            function i(t, i) {
                var o = t / i
                    , e = o * (1 - this.options.yearUnitSpacing);
                return {
                    rect_w: o,
                    bar_w: e
                }
            }
            function o(t, i) {
                var o = t / i
                    , e = o * (1 - this.options.quarterUnitSpacing);
                return {
                    rect_w: o,
                    bar_w: e
                }
            }
            function h(t, i, o) {
                return {
                    x: i * this.options.dpr,
                    y: o * this.options.dpr
                }
            }
            function l(t, i, o) {
                var e = t.getBoundingClientRect();
                return {
                    x: i / this.options.dpr,
                    y: (o + this.options.canvas_offset_top) * (e.height / t.height)
                }
            }
            function c(t) {
                return this.options.isLessZero ? this.options.c_1_height / 2 - this.options.c_1_height / 2 * -t / this.options.data.max : this.options.c_1_height - this.options.c_1_height * (t - this.options.data.min) / (this.options.data.max - this.options.data.min)
            }
            function d(t, i) {
                var o = this.options.context.canvas
                    , e = this.options.quarterUnit
                    , n = this.options.series.length
                    , s = this.options.padding_left;
                return (o.width - s) / n * t + s + e.rect_w * i + e.rect_w / 2
            }
            function u(t) {
                var i = h.call(this, this.options.canvas, t, 0).x
                    , o = {}
                    , e = this.options.padding_left
                    , n = this.options.yearUnit
                    , s = this.options.quarterUnit
                    , a = this.options.canvas
                    , r = Math.floor((i - e) / n.rect_w);
                0 > r && (r = 0);
                var p = Math.floor((i - e - r * n.rect_w - (n.rect_w - n.bar_w) / 2) / s.rect_w);
                return 0 > p ? p = 0 : p > 3 && (p = 3),
                    o.midddleLine = d.call(this, r, p) + 3 * s.bar_w / 4,
                    o.tipsX = o.midddleLine + s.bar_w / 2,
                    o.tipsY = c.call(this, -this.options.series[r].data[p]),
                t > a.width / 2 && (o.tipsX = o.midddleLine - s.bar_w / 2),
                    o.midddleLineHeight = o.tipsY,
                    o.content = this.options.series[r].data[p],
                    o.arr = r + ":" + p,
                    o
            }
            function m(t) {
                for (var i = 0, o = 0, e = t.length, n = {}, s = 0; e > s; s++)
                    for (var a = 0; a < t[s].data.length; a++)
                        0 == s && 0 == a && (i = t[s].data[a],
                            o = t[s].data[a]),
                            i = Math.max(i, t[s].data[a]),
                            o = Math.min(o, t[s].data[a]);
                return i < Math.abs(o) ? i = Math.abs(o) + .05 * Math.abs(i - o) : i += .05 * Math.abs(i - o),
                    n.max = i,
                    n.min = o,
                    n
            }
            return t.prototype.init = function() {
                this.options.type = "quarter-line";
                var t = document.createElement("canvas");
                this.container.style.position = "relative",
                    this.container.appendChild(t);
                try {
                    var i = t.getContext("2d")
                } catch (o) {
                    t = window.G_vmlCanvasManager.initElement(t);
                    var i = t.getContext("2d")
                }
                this.options.canvas = t,
                    this.options.context = i;
                var e = this.options.dpr = 1;
                t.width = this.options.width * e,
                    t.height = this.options.height * e,
                    this.options.canvas_offset_top = t.height / 5 / 4,
                    this.options.c_1_height = 4 * t.height / 5,
                    t.style.width = this.options.width + "px",
                    t.style.height = this.options.height + "px",
                    t.style.border = "0",
                    i.translate("0", this.options.canvas_offset_top),
                    i.font = this.options.font_size * this.options.dpr + "px Arial",
                    i.lineWidth = 1 * this.options.dpr,
                    this.options.yearUnitSpacing = "0.2",
                    this.options.quarterUnitSpacing = "0.4",
                    r.apply(this, [this.options.context, 90, 20, 82, 20])
            }
                ,
                t.prototype.draw = function() {
                    this.clear(),
                        this.init();
                    var t = this.options.series
                        , n = this.options.canvas
                        , a = m(t);
                    a.min < 0 && (this.options.isLessZero = !0),
                        this.options.data = {},
                        this.options.data.max = a.max,
                        this.options.data.min = a.min;
                    var r = this.options.context;
                    this.options.padding_left = r.measureText("+9000万").width + 10,
                        this.options.drawWidth = r.canvas.width - this.options.padding_left,
                        this.options.yearUnit = i.call(this, n.width - this.options.padding_left, this.options.series.length),
                        this.options.quarterUnit = o.call(this, this.options.yearUnit.bar_w, 4),
                        new e(this.options),
                        new s(this.options),
                        this.addInteractive()
                }
                ,
                t.prototype.addInteractive = function() {
                    var t, i = this.options.canvas, o = this.options.yearUnit, e = this, n = document.createElement("div"), s = document.createElement("div"), a = {}, r = "x:x", h = this.options.dpr, c = this.options.padding_left, d = this.options.canvas_offset_top, m = this.options.c_1_height;
                    n.className = "web-tips",
                        s.className = "web-middleLine",
                        e.container.appendChild(n),
                        e.container.appendChild(s),
                        p.addEvent.call(e, i, "mousemove", function(p) {
                            var f, v;
                            p.layerX ? (f = p.layerX,
                                v = p.layerY) : p.x && (f = p.x,
                                v = p.y),
                                f >= c && f < i.width - (o.rect_w - o.bar_w) / 2 && v >= d && d * h + m > v * h ? (n.style.display = "inline-block",
                                    s.style.display = "inline-block") : (n.style.display = "none",
                                    s.style.display = "none"),
                                t = u.call(e, f),
                            r !== t.arr && (a.midddleLine = l.call(e, i, t.midddleLine, 0),
                                a.tips = l.call(e, i, t.tipsX, t.tipsY),
                                n.innerHTML = t.content,
                                n.style.left = f > i.width / 2 ? a.tips.x - n.clientWidth + "px" : a.tips.x - n.style.width + "px",
                                n.style.top = t.tipsY * h + n.clientHeight + "px",
                                s.style.height = m + "px",
                                s.style.left = a.midddleLine.x + "px",
                                s.style.top = d + "px",
                                r = t.arr)
                        })
                }
                ,
                t.prototype.reDraw = function() {
                    this.clear(),
                        this.init(),
                        this.draw()
                }
                ,
                t.prototype.clear = function(t) {
                    this.container ? this.container.innerHTML = "" : document.getElementById(this.options.container).innerHTML = "",
                    t && t()
                }
                ,
                t
        }();
        t.exports = h
    }
    , function(t, i, o) {
        var e = o(21)
            , n = o(7)
            , s = o(13)
            , a = o(9)
            , r = function() {
            function t(t) {
                this.defaultoptions = n.draw_xy,
                    this.options = e(this.defaultoptions, t),
                    this.draw()
            }
            function i(t, i, o, e) {
                t.fillStyle = "#000",
                    t.strokeStyle = "#eeeeee",
                    t.textAlign = "right";
                for (var n, r = 0; n = e[r]; r++) {
                    t.beginPath(),
                        t.moveTo(a(this.options.padding_left), a(n.y)),
                        t.lineTo(a(t.canvas.width), a(n.y));
                    var p = Math.max(this.options.data.max, Math.abs(this.options.data.min));
                    p = p.toFixed(0),
                        this.options.data.min < 0 ? 0 == r ? t.fillText(s.format_unit(-p + r * p / 2, 0), a(this.options.padding_left - 10), a(n.y)) : t.fillText(s.format_unit(-p + r * p / 2, 0), a(this.options.padding_left - 10), a(n.y + 10)) : 0 == r ? t.fillText(s.format_unit((r * p / 4).toFixed(0), 0), a(this.options.padding_left - 10), a(n.y)) : t.fillText(s.format_unit((r * this.options.data.max / 4).toFixed(0), 0), a(this.options.padding_left - 10), a(tem.y + 10)),
                        t.stroke()
                }
            }
            function o(t, i, o) {
                var e = this.options.padding_left;
                t.beginPath(),
                    t.strokeStyle = "#9f9f9f",
                    t.rect(a(e), .5, Math.round(t.canvas.width - e - 1), Math.round(this.options.c_1_height)),
                    t.stroke(),
                    t.closePath(),
                    t.beginPath(),
                    t.textAlign = "left",
                    t.fillStyle = "#000";
                for (var n, s = t.canvas.width, r = o.length, p = 0; r > p; p++)
                    n = o[p].value,
                        t.fillText(n, a(p * (s - e) / r + e + ((s - e) / r - t.measureText(n).width) / 2), a(this.options.c_1_height + 30));
                t.stroke(),
                    t.closePath()
            }
            function r() {
                for (var t = (this.options.canvas.width - this.options.padding_left) / this.options.series.length, i = this.options.context, o = 0; o < this.options.series.length; o++)
                    if (o % 2 == 0) {
                        i.beginPath();
                        var e = i.createLinearGradient(0, 0, 0, this.options.c_1_height);
                        e.addColorStop(0, "rgba(255,255,255,0)"),
                            e.addColorStop(1, "rgba(245,245,245,1)"),
                            i.fillStyle = e,
                            i.rect(this.options.padding_left + o * t, 0, t, this.options.c_1_height),
                            i.fill(),
                            i.closePath()
                    }
            }
            function p(t, i, o, e) {
                for (var n = (t - i) / (o - 1), s = [], a = 0; o > a; a++)
                    s.push({
                        num: i + a * n,
                        x: 0,
                        y: e - a / (o - 1) * e
                    });
                return s
            }
            return t.prototype.draw = function() {
                var t = this.options.context
                    , e = this.options.data.max
                    , n = 0
                    , s = 5
                    , a = this.options.xaxis
                    , h = this.options.c_1_height
                    , l = p(e, n, s, h);
                r.call(this),
                    i.call(this, t, e, n, l),
                    o.apply(this, [t, h, a])
            }
                ,
                t
        }();
        t.exports = r
    }
    , function(t, i, o) {
        var e = o(6)
            , n = o(7)
            , s = o(9)
            , a = function() {
            function t(t) {
                this.defaultoptions = n.drawLine,
                    this.options = {},
                    e(!1, this.options, this.defaultoptions, t),
                    this.draw()
            }
            function i(t, i, e) {
                t.save();
                var n = this.options.point.pointradius;
                t.fillStyle = void 0 == e ? "#333" : e;
                for (var r, p = 0; r = i[p]; p++)
                    for (var h, l = r.data, c = 0; h = l[c]; c++) {
                        t.beginPath();
                        var d = a.apply(this, [p, c])
                            , u = o.call(this, h);
                        t.arc(s(d), s(u), n, 0, 2 * Math.PI, !0),
                            t.fill()
                    }
                t.restore()
            }
            function o(t) {
                return this.options.isLessZero ? this.options.c_1_height / 2 - this.options.c_1_height / 2 * t / this.options.data.max : this.options.c_1_height - this.options.c_1_height * (t - this.options.data.min) / (this.options.data.max - this.options.data.min)
            }
            function a(t, i) {
                var o = this.options.yearUnit
                    , e = this.options.quarterUnit
                    , n = this.options.padding_left
                    , s = this.options.yearUnit.rect_w - this.options.yearUnit.bar_w;
                return o.rect_w * t + n + e.rect_w * i + s
            }
            return t.prototype.draw = function() {
                var t = this.options.context;
                t.lineWidth = 1 * this.options.dpr;
                var e = this.options.series;
                t.beginPath(),
                    t.strokeStyle = void 0 == this.options.line.color ? "#333" : this.options.line.color;
                for (var n, r = 0; n = e[r]; r++)
                    for (var p, h = n.data, l = 0; p = h[l]; l++) {
                        var c = a.apply(this, [r, l])
                            , d = o.call(this, p);
                        0 == r && 0 == l ? t.moveTo(s(c), s(d)) : t.lineTo(s(c), s(d))
                    }
                t.stroke(),
                this.options.point && this.options.point.show && i.apply(this, [t, e, this.options.point.color])
            }
                ,
                t
        }();
        t.exports = a
    }
    , function(t, i, o) {
        var e = o(55)
            , n = o(7)
            , s = o(57)
            , a = o(21)
            , r = o(20)
            , p = o(22)
            , h = o(13)
            , l = o(38)
            , c = function() {
            function t(t) {
                this.options = {},
                    this.options = a(n.defaulttheme, t),
                    this.container = document.getElementById(t.container),
                    this.container.className = this.container.className.replace(/emcharts-container/g, "").trim(),
                    this.container.className = this.container.className + " emcharts-container",
                    this.onChartLoaded = void 0 == t.onChartLoaded ? function() {}
                        : t.onChartLoaded
            }
            function i(t) {
                for (var i = t.length, o = [], e = 0; i > e; e++)
                    o = o.concat(t[e].data);
                o.sort(function(t, i) {
                    return 1 * t - 1 * i
                });
                var n = 1 * o[0]
                    , s = 1 * o[o.length - 1]
                    , a = (s + n) / 2;
                s - n > 1 && a - Math.floor(a) > 0 && (n = o[0] - (a - Math.floor(a)),
                    a = Math.floor(a));
                var r = {};
                if (s * n > 0 && n !== s && (s - n) / Math.abs(s) <= .5)
                    if (r = l(this.options.sepeNum, [s - a, n - a]),
                    (r.max + a) * (r.min + a) < 0) {
                        var p = Math.min(Math.abs(r.max + a), Math.abs(r.min + a));
                        r.max = s >= 0 ? r.max + a + p : 0,
                            r.min = s >= 0 ? 0 : r.min + a - p
                    } else
                        r.max += a,
                            r.min += a;
                else
                    r = l(this.options.sepeNum, o);
                var c = this.options.context;
                if (r.stepHeight >= 1e4)
                    var d = c.measureText(h.format_unit(r.stepHeight)).width - c.measureText(h.format_unit(parseInt(r.stepHeight))).width;
                else
                    var d = c.measureText(r.stepHeight).width - c.measureText(parseInt(r.stepHeight)).width;
                var u = c.measureText(h.format_unit(parseInt(r.max))).width
                    , m = c.measureText(h.format_unit(parseInt(r.min))).width
                    , f = u > m ? u : m
                    , v = f + d;
                return 0 == r.max && 0 == r.min && (r.max = Math.ceil(this.options.sepeNum / 2),
                    r.min = -Math.floor(this.options.sepeNum / 2),
                    r.stepHeight = 1),
                    {
                        max: r.max,
                        min: r.min,
                        step: r.stepHeight,
                        maxPaddingLeftWidth: v
                    }
            }
            return t.prototype.init = function() {
                this.options.type = "line";
                var t = !0
                    , i = document.createElement("div");
                i.className = "event-div",
                    this.container.appendChild(i),
                    this.eventDiv = i;
                var o = document.createElement("canvas");
                this.container.style.position = "relative",
                    this.container.appendChild(o);
                try {
                    var e = o.getContext("2d")
                } catch (n) {
                    o = window.G_vmlCanvasManager.initElement(o);
                    var e = o.getContext("2d")
                }
                this.options.canvas = o,
                    this.options.context = e;
                var s = this.options.dpr = void 0 == this.options.dpr ? 1 : this.options.dpr;
                o.width = this.options.width * s,
                    o.height = this.options.height * s,
                    this.options.canvas_offset_top = 15 * s,
                    this.options.scale_count = 0,
                    this.options.decimalCount = void 0 == this.options.decimalCount ? 2 : this.options.decimalCount;
                var a = this.options.xaxis;
                if (this.options.angle && 0 != this.options.angle) {
                    var r = e.measureText(a[0].value).width * Math.sin(2 * Math.PI * (this.options.angle / 360)) + 30;
                    this.options.angle_height = r,
                        this.options.c_1_height = this.options.canvas.height - this.options.canvas_offset_top * s - r * s
                } else
                    this.options.c_1_height = o.height - this.options.canvas_offset_top * s - 40 * s;
                void 0 === this.options.showname && (this.options.showname = !0),
                    this.options.sepeNum = void 0 == this.options.sepeNum ? 4 : this.options.sepeNum,
                this.options.sepeNum < 2 && (this.options.sepeNum = 2),
                    o.style.width = this.options.width + "px",
                    o.style.height = this.options.height + "px",
                    o.style.border = "0";
                var p = !1;
                p ? t && e.translate("0", this.options.canvas_offset_top) : e.translate("0", this.options.canvas_offset_top);
                var h = ""
                    , l = ""
                    , c = "";
                this.options.font ? (c = this.options.font.fontFamily ? this.options.font.fontFamily : "Arial",
                    l = this.options.font.fontSize ? this.options.font.fontSize * this.options.dpr : 12 * this.options.dpr,
                    h = l + "px " + c) : h = 12 * this.options.dpr + "px Arial",
                    e.font = h,
                    e.lineWidth = 1 * this.options.dpr,
                    this.options.padding = {},
                    this.options.padding.left = 0,
                    this.options.padding.right = 0,
                    this.options.padding.top = 0,
                    this.options.padding.bottom = 0,
                    this.options.pointRadius = void 0 == this.options.pointRadius ? 5 : this.options.pointRadius
            }
                ,
                t.prototype.draw = function() {
                    this.clear(),
                        this.init(),
                        this.options.interactive = new r(this.options);
                    var t = this.options.context
                        , o = (this.options.dpr,
                        this.options.series);
                    this.options.data = {};
                    var n = i.call(this, o);
                    this.options.data.max = n.max,
                        this.options.data.min = n.min,
                        this.options.data.step = n.step,
                        this.options.padding_left = Math.round(n.maxPaddingLeftWidth + 30);
                    var a = this.options.xaxis
                        , h = t.measureText(a[0].value).width / 2 + 10
                        , l = t.measureText(a[a.length - 1].value).width / 2 + 10;
                    if (this.options.padding_left < h && (this.options.padding_left = h),
                    this.options.series2 && 0 !== this.options.series2.length) {
                        var c = this.options.series2
                            , d = i.call(this, c);
                        this.options.data.max2 = d.max,
                            this.options.data.min2 = d.min,
                            this.options.data.step2 = d.step,
                            this.options.padding.right = Math.round(n.maxPaddingLeftWidth + 20)
                    }
                    this.options.angle && 0 != this.options.angle && (this.options.padding.right = t.measureText(this.options.xaxis[0].value).width * Math.cos(2 * Math.PI * (this.options.angle / 360)) + 10),
                    this.options.padding.right < l && (this.options.padding.right = l),
                        this.options.series2 && 0 !== this.options.series2.length ? (this.options.drawWidth = Math.round(t.canvas.width - this.options.padding.right),
                            p.apply(this, [t, 100 + this.options.padding.right, 10, 82, 20])) : (this.options.drawWidth = Math.round(t.canvas.width - this.options.padding.right),
                            p.apply(this, [t, 100 + this.options.padding.right, 10, 82, 20])),
                        new e(this.options),
                        new s(this.options),
                        this.addInteractive()
                }
                ,
                t.prototype.addInteractive = function() {
                    function t(t, f) {
                        var v = t * p - a
                            , g = f * p - r
                            , y = 0
                            , A = [];
                        y = i / 2 > v % i ? Math.floor(v / i) : Math.ceil(v / i),
                        0 > y && (y = 0),
                        y > e.length - 1 && (y = e.length - 1);
                        for (var x = 0, w = n.length; w > x; x++)
                            A.push({
                                color: n[x].color,
                                data: (void 0 === n[x].data[y] || null === n[x].data[y] ? "" : n[x].data[y]) + (n[x].suffix || ""),
                                name: n[x].name,
                                y: void 0 === n[x].data[y] ? r + h.get_y.call(l, 0) : r + h.get_y.call(l, n[x].data[y]),
                                suffix: n[x].suffix || ""
                            });
                        if (l.options.series2)
                            for (x = 0,
                                     w = s.length; w > x; x++)
                                A.push({
                                    color: s[x].color,
                                    data: (void 0 === s[x].data[y] || null === s[x].data[y] ? "" : s[x].data[y]) + (s[x].suffix || ""),
                                    name: s[x].name,
                                    y: void 0 === s[x].data[y] ? r + u / 2 : r + (u - u * (s[x].data[y] - d) / (c - d)),
                                    suffix: s[x].suffix || ""
                                });
                        A.sort(function(t, i) {
                            return t.y - i.y
                        });
                        var _ = 0
                            , b = !1;
                        if (_ = 1 == e.length ? y * i / p + i / p * .5 + a / p : y * i / p + a / p,
                            l.options.interOption) {
                            var T = l.options.interOption.tips
                                , M = T.children;
                            M[0].innerHTML = e[y].value;
                            for (var k = 0, w = A.length; w > k; k++)
                                A[k].data === A[k].suffix ? M[k + 1].style.display = "none" : (b = !0,
                                    M[k + 1].style.display = "block",
                                    M[k + 1].children[0].style.backgroundColor = A[k].color,
                                    M[k + 1].children[1].innerHTML = (l.options.showname ? A[k].name : "") + " " + A[k].data);
                            b ? T.style.display = "block" : (T.style.display = "none",
                                T.style.left = "-10000px"),
                                T.style.left = y * i / p + a / p >= o.width / p / 2 ? _ - a / 2 - T.clientWidth + "px" : _ + a / 2 + "px",
                                T.style.top = (A[0].y + A[A.length - 1].y) / 2 / p - 50 + "px";
                            var C = l.options.interOption.yLine;
                            C.style.left = _ + "px";
                            for (var N = l.options.interOption.circles, E = 0, S = A.length; S > E; E++)
                                A[E].data === A[E].suffix ? N[E].style.display = "none" : (N[E].style.display = "block",
                                    N[E].style.top = A[E].y / p - m + "px",
                                    N[E].style.left = _ - m + "px",
                                    N[E].style.borderColor = A[E].color);
                            b && (l.options.interOption.tips.style.display = "block",
                                C.style.display = "block")
                        } else {
                            l.options.interOption = {};
                            var T = document.createElement("div");
                            T.className = "chart_line_tips",
                                l.container.appendChild(T),
                                T.style.top = (A[0].y + A[A.length - 1].y) / 2 / p + "px";
                            var D = document.createElement("div");
                            D.className = "chart_line_tips_title",
                                D.innerHTML = e[y].value,
                                T.appendChild(D);
                            var C = document.createElement("div");
                            C.className = "chart_line_yline",
                                C.style.left = _ + "px",
                                C.style.top = r / p + "px",
                                C.style.height = u / p + "px",
                                l.container.appendChild(C);
                            var N = [];
                            for (x = 0,
                                     w = A.length; w > x; x++) {
                                var L = document.createElement("div");
                                L.className = "chart_line_tips_line";
                                var R = document.createElement("span");
                                R.className = "chart_line_tips_color",
                                    R.style.backgroundColor = A[x].color,
                                    L.appendChild(R);
                                var I = document.createElement("span");
                                I.innerHTML = (l.options.showname ? A[x].name : "") + A[x].data,
                                    L.appendChild(I),
                                    T.appendChild(L);
                                var Y = document.createElement("div");
                                Y.className = "chart_line_cir",
                                    Y.style.width = 2 * m + "px",
                                    Y.style.height = 2 * m + "px",
                                    Y.style.borderRadius = 2 * m + "px",
                                    Y.style.top = A[x].y / p - m + "px",
                                    Y.style.left = _ - m + "px",
                                    Y.style.borderColor = A[x].color,
                                    void 0 === A[x] || A[x].data === A[x].suffix ? (Y.style.display = "none",
                                        L.style.display = "none") : b = !0,
                                    l.container.appendChild(Y),
                                    N.push(Y)
                            }
                            T.style.left = y * i / p + a / p > o.width / 2 ? _ - a / 2 - T.clientWidth + "px" : _ + a / 2 + "px",
                                l.options.interOption.tips = T,
                                l.options.interOption.yLine = C,
                                l.options.interOption.circles = N
                        }
                        var W = this.options.series2 ? a : 10;
                        if (v >= 0 && v < o.width - a - W + 3 && g >= 0 && u >= g && b) {
                            l.options.interOption.tips.style.display = "block";
                            for (var E = 0, S = A.length; S > E; E++)
                                A[E].data === A[E].suffix || (N[E].style.display = "block");
                            C.style.display = "block"
                        } else {
                            l.options.interOption.tips.style.display = "none",
                                l.options.interOption.tips.style.left = "-10000px";
                            for (var E = 0, S = N.length; S > E; E++)
                                N[E].style.display = "none";
                            C.style.display = "none"
                        }
                    }
                    var i, o = this.options.canvas, e = this.options.xaxis, n = this.options.series, s = this.options.series2, a = (this.options.context,
                        this.options.padding_left), r = this.options.canvas_offset_top, p = this.options.dpr;
                    i = 1 === e.length ? this.options.drawWidth - a : (this.options.drawWidth - a) / (e.length - 1);
                    var l = this
                        , c = (this.options.data.max,
                        this.options.data.min,
                        this.options.data.max2)
                        , d = this.options.data.min2
                        , u = this.options.c_1_height
                        , m = this.options.pointRadius / p;
                    h.addEvent(l.eventDiv, "touchmove", function(i) {
                        var o = i.changedTouches[0]
                            , e = o.offsetX || o.clientX - l.container.getBoundingClientRect().left
                            , n = o.offsetY || o.clientY - l.container.getBoundingClientRect().top;
                        try {
                            i.preventDefault()
                        } catch (s) {}
                        t.call(l, e, n)
                    }),
                        h.addEvent(l.eventDiv, "mousemove", function(i) {
                            var o = i.clientX - l.container.getBoundingClientRect().left
                                , e = i.clientY - l.container.getBoundingClientRect().top;
                            try {
                                i.preventDefault()
                            } catch (n) {}
                            t.call(l, o, e)
                        }),
                        h.addEvent(l.eventDiv, "touchend", function(t) {
                            if (l.options.interOption) {
                                var i = l.options.interOption.circles;
                                l.options.interOption.tips.style.display = "none";
                                for (var o = 0, e = i.length; e > o; o++)
                                    i[o].style.display = "none";
                                l.options.interOption.yLine.style.display = "none"
                            }
                            try {
                                t.preventDefault()
                            } catch (n) {}
                        }),
                        h.addEvent(l.eventDiv, "mouseleave", function(t) {
                            if (l.options.interOption) {
                                var i = l.options.interOption.circles;
                                l.options.interOption.tips.style.display = "none";
                                for (var o = 0, e = i.length; e > o; o++)
                                    i[o].style.display = "none";
                                l.options.interOption.yLine.style.display = "none"
                            }
                            try {
                                t.preventDefault()
                            } catch (n) {}
                        })
                }
                ,
                t.prototype.reDraw = function() {
                    this.clear(),
                        this.init(),
                        this.draw()
                }
                ,
                t.prototype.clear = function(t) {
                    try {
                        this.container.innerHTML = "",
                        this.options.interOption && (this.options.interOption = null)
                    } catch (i) {
                        this.container.innerHTML = ""
                    }
                    t && t()
                }
                ,
                t
        }();
        t.exports = c
    }
    , function(t, i, o) {
        var e = o(21)
            , n = o(7)
            , s = o(13)
            , a = o(8)
            , r = o(9)
            , p = o(56)
            , h = function() {
            function t(t) {
                this.options = e(n.defaulttheme, t),
                    this.draw()
            }
            function i(t, i, o, e, n) {
                var s = n ? this.options.data.step2 : this.options.data.step;
                t.save(),
                    t.fillStyle = void 0 == this.options.font.color ? "#000" : this.options.font.color,
                    t.textAlign = "right",
                    t.lineWidth = 1;
                for (var p, h = this.options.dpr, c = 0; p = e[c]; c++)
                    t.beginPath(),
                    0 == c || c == e.length - 1 || (t.strokeStyle = "#e6e6e6",
                        a(t, this.options.padding_left, p.y, this.options.drawWidth, p.y, 3)),
                        this.options.series2 && 0 !== this.options.series2.length && n ? (t.textAlign = "left",
                            t.fillText(l(p.num / 1, s), r(this.options.drawWidth + 5), r(p.y + 5 * h))) : (t.textAlign = "right",
                            t.fillText(l(p.num / 1, s), r(this.options.padding_left - 5), r(p.y + 5 * h)));
                t.restore()
            }
            function o(t, i, o) {
                t.save(),
                    t.lineWidth = 1;
                var e = this.options.dpr
                    , n = this.options.padding_left;
                t.textAlign = "center",
                    t.fillStyle = this.options.font ? void 0 == this.options.font.color ? "#000" : this.options.font.color : "#000";
                for (var s, h = this.options.drawWidth, l = o.length, c = 0; l > c; c++) {
                    t.beginPath(),
                        s = o[c];
                    {
                        var d = t.measureText(s).width;
                        Math.cos(2 * Math.PI / 360 * this.options.angle) * d
                    }
                    if (void 0 == s.show ? !0 : s.show)
                        if (1 == l) {
                            var u = (this.options.drawWidth - this.options.padding_left) / 2 + this.options.padding_left;
                            t.fillText(s.value, r(u), r(this.options.c_1_height + 20 * e))
                        } else {
                            var u = c * (h - n) / (l - 1) + n;
                            this.options.angle && 0 != this.options.angle ? p(s.value, t, r(u), r(this.options.c_1_height + 10 * e), this.options.angle) : (this.options.drawWidth > u + t.measureText(s.value).width || c == l - 1) && t.fillText(s.value, r(u), r(this.options.c_1_height + 20 * e))
                        }
                    if (void 0 == s.showline ? !0 : s.showline) {
                        var u = c * (h - n) / (l - 1) + n;
                        0 == c || c == l - 1 || (t.strokeStyle = "#e6e6e6",
                            a(t, u, 0, c * (h - n) / (l - 1) + n, this.options.c_1_height + 2, 3))
                    }
                }
                t.restore()
            }
            function h(t, i, o, e, n) {
                for (var s = n, a = [], r = 0; o >= r; r++)
                    a.push({
                        num: i + r * s,
                        x: 0,
                        y: e - r / o * e
                    });
                return a
            }
            function l(t, i) {
                var o = 1;
                -1 !== i.toString().indexOf(".") && (o = i.toString().length - i.toString().indexOf(".") - 1);
                var e = Math.pow(10, o);
                return s.format_unit(Math.round(t * e) / e, o)
            }
            return t.prototype.draw = function() {
                var t = this.options.context
                    , e = this.options.data.max
                    , n = this.options.data.max2
                    , s = this.options.data.step
                    , a = this.options.data.min
                    , p = this.options.data.min2
                    , l = this.options.data.step2
                    , c = this.options.series2
                    , d = this.options.sepeNum || 4
                    , u = this.options.xaxis
                    , m = this.options.c_1_height;
                t.beginPath(),
                    t.lineWidth = 1,
                    t.strokeStyle = "#ccc",
                    t.rect(r(this.options.padding_left), .5, Math.round(this.options.drawWidth - this.options.padding_left), Math.round(m)),
                    t.stroke();
                var f = h(e, a, d, m, s);
                if (i.call(this, t, e, a, f, !1),
                c && 0 !== c.length && c.some(function(t) {
                    return 0 !== t.data.length
                })) {
                    var v = h(n, p, d, m, l);
                    i.call(this, t, e, a, v, !0)
                }
                o.apply(this, [t, m, u])
            }
                ,
                t
        }();
        t.exports = h
    }
    , function(t) {
        function i(t, i, o, e, n, s) {
            i.save(),
                i.textBaseline = "middle",
                i.textAlign = "left",
                s ? (i.translate(o, e),
                    i.rotate(Math.PI / 180 * n),
                    i.fillText(t, 0, 0),
                    i.translate(-o, -e),
                    i.rotate(-(Math.PI / 180) * n)) : (i.translate(o, e),
                    i.rotate(Math.PI / 180 * n),
                    i.fillText(t, 0, 0),
                    i.translate(-o, -e),
                    i.rotate(-(Math.PI / 180) * n)),
                i.restore()
        }
        t.exports = i
    }
    , function(t, i, o) {
        var e = o(21)
            , n = o(7)
            , s = o(13)
            , a = o(9)
            , r = function() {
            function t(t) {
                this.options = e(n.defaulttheme, t),
                    this.draw()
            }
            function i(t, i, o) {
                t.save();
                var e = i.data
                    , n = e.length;
                t.beginPath();
                for (var a = 0, p = 0; n > p; p++) {
                    var h = e[p];
                    if (null !== h && "" !== h && void 0 !== h) {
                        var l = (this.options.drawWidth - this.options.padding_left) / (n - 1) * p + this.options.padding_left;
                        if (o)
                            var c = r.call(this, h);
                        else
                            var c = s.get_y.call(this, h);
                        0 == p ? t.moveTo(this.options.padding_left, c) : p == n - 1 ? t.lineTo(l, c) : null === a || "" === a || void 0 === a ? t.moveTo(l, c) : (t.lineTo(l, c),
                            t.moveTo(l, c))
                    }
                    a = h
                }
                t.stroke(),
                    t.restore()
            }
            function o(t, i, o) {
                t.save();
                for (var e = i.data, n = e.length, p = this.options.pointRadius, h = 0; n > h; h++) {
                    var l = e[h];
                    if (null != l && "" !== l && void 0 != l) {
                        if (t.beginPath(),
                        1 == n)
                            var c = 1;
                        else
                            var c = n - 1;
                        if (1 == n)
                            var d = (this.options.drawWidth - this.options.padding_left) / 2 + this.options.padding_left;
                        else
                            var d = (this.options.drawWidth - this.options.padding_left) / c * h + this.options.padding_left;
                        if (o)
                            var u = r.call(this, l);
                        else
                            var u = s.get_y.call(this, l);
                        0 == h ? (t.arc(a(d), a(u), p, 0, 2 * Math.PI, !0),
                            t.fill()) : (t.arc(a(d), a(u), p, 0, 2 * Math.PI, !0),
                            t.fill())
                    }
                }
                t.restore()
            }
            function r(t) {
                return this.options.c_1_height - this.options.c_1_height * (t - this.options.data.min2) / (this.options.data.max2 - this.options.data.min2)
            }
            return t.prototype.draw = function() {
                var t = this.options.context;
                t.lineWidth = 1;
                for (var e, n = this.options.series, s = 0; e = n[s]; s++)
                    t.fillStyle = void 0 == e.color ? "#333" : e.color,
                        t.strokeStyle = void 0 == e.color ? "#333" : e.color,
                        i.apply(this, [t, e, !1]),
                        e.showpoint ? o.apply(this, [t, e, !1]) : 1 == e.data.length && o.apply(this, [t, e, !1]);
                if (this.options.series2)
                    for (var e, a = this.options.series2, s = 0; e = a[s]; s++)
                        t.fillStyle = void 0 == e.color ? "#333" : e.color,
                            t.strokeStyle = void 0 == e.color ? "#333" : e.color,
                            i.apply(this, [t, e, !0]),
                        e.showpoint && o.apply(this, [t, e, !0])
            }
                ,
                t
        }();
        t.exports = r
    }
    , function(t, i, o) {
        var e = o(59)
            , n = o(7)
            , s = o(61)
            , a = o(62)
            , r = o(63)
            , p = o(68)
            , h = o(60)
            , l = o(25)
            , c = o(21)
            , d = o(69)
            , u = o(22)
            , m = o(72)
            , l = o(25)
            , f = function() {
            function t(t) {
                this.defaultoptions = n.draw_xy_web,
                    this.options = {},
                    this.options = c(this.defaultTheme, this.options, this.defaultoptions, t),
                    this.container = document.getElementById(t.container),
                    this.container.className = this.container.className.replace(/emcharts-container/g, "").trim(),
                    this.container.className = this.container.className + " emcharts-container",
                    this.onChartLoaded = void 0 == t.onChartLoaded ? function() {}
                        : t.onChartLoaded
            }
            function i(t) {
                this.options.context;
                this.options.data = void 0 == t ? this.options.data : t;
                {
                    var i;
                    t.high,
                        t.low,
                        t.yc
                }
                i = this.options.interactive;
                try {
                    this.options.pricedigit = t.pricedigit;
                    var o = h.get_rect.apply(this, [this.options.context.canvas, this.options.data.total]);
                    this.options.rect_unit = o,
                        new e(this.options),
                        g.call(this),
                        v.call(this),
                        y.call(this),
                    "r" === this.options.type && A.call(this),
                        i.hideLoading();
                    var n;
                    try {
                        n = t.data[0].price
                    } catch (s) {
                        n = 0
                    }
                    var a = h.get_y.call(this, n);
                    i.showTipsTime(this.options.padding.left, a, t.data, t.data.length - 1, t),
                        this.onChartLoaded(this),
                        u.apply(this, [this.options.context, 95 + this.options.padding.right, 10, 82, 20])
                } catch (r) {
                    i.hideLoading()
                }
                return !0
            }
            function o(t) {
                var i = this
                    , o = t.canvas
                    , e = this.options.interactive;
                e.allData = this.options;
                var n = this.container;
                h.addEvent.call(i, o, "touchmove", function(t) {
                    f.apply(i, [e, t.changedTouches[0]]);
                    try {
                        t.preventDefault()
                    } catch (o) {
                        t.returnValue = !1
                    }
                }),
                    h.addEvent.call(i, o, "mousemove", function(t) {
                        f.apply(i, [e, t]);
                        try {
                            t.preventDefault()
                        } catch (o) {
                            t.returnValue = !1
                        }
                    }),
                    h.addEvent.call(i, n, "mouseleave", function(t) {
                        e.hide();
                        try {
                            t.preventDefault()
                        } catch (i) {
                            t.returnValue = !1
                        }
                    }),
                    h.addEvent.call(i, o, "mouseenter", function(t) {
                        f.apply(i, [e, t]);
                        try {
                            t.preventDefault()
                        } catch (o) {
                            t.returnValue = !1
                        }
                    })
            }
            function f(t, i) {
                if (!(!this.options.data.data.length > 0)) {
                    t.show();
                    var o = this.options.canvas
                        , e = this.options.data.data
                        , n = this.options.rect_unit
                        , s = n.rect_w
                        , a = i.clientX - this.container.getBoundingClientRect().left
                        , r = i.clientY - this.container.getBoundingClientRect().top
                        , p = h.windowToCanvas.apply(this, [o, a, r])
                        , l = (1 * p.x).toFixed(0)
                        , c = Math.floor((l - this.options.padding.left) / s);
                    if (e[c]) {
                        var d = h.canvasToWindow.apply(this, [o, e[c].cross_x, e[c].cross_y])
                            , u = d.x
                            , m = d.y;
                        t.crossTime(o, u, m),
                            t.showTipsTime(u, m, e, c, t.allData.data)
                    }
                }
            }
            function v() {
                function t(t, i) {
                    t.beginPath(),
                        t.strokeStyle = "#014D9F";
                    for (var o, e = 0; o = i[e]; e++) {
                        var n = h.get_x.call(this, e + 1)
                            , s = h.get_y.call(this, o.price);
                        0 === e ? t.moveTo(n, s) : t.lineTo(n, s),
                            o.cross_x = n,
                            o.cross_y = s
                    }
                    t.stroke()
                }
                function i(t, i) {
                    var o = h.get_y.call(this, this.options.data.min)
                        , e = t.createLinearGradient(0, 0, 0, t.canvas.height);
                    e.addColorStop(0, "rgba(200,234,250,0.7)"),
                        e.addColorStop(1, "rgba(255,255,255,0)");
                    var n = i.length;
                    t.beginPath(),
                        t.fillStyle = e,
                        t.moveTo(this.options.padding.left, o);
                    for (var s, a = 0; s = i[a]; a++) {
                        var r = h.get_x.call(this, a + 1)
                            , p = h.get_y.call(this, s.price);
                        a === n - 1 ? (t.lineTo(r, p),
                            t.lineTo(r, o)) : t.lineTo(r, p)
                    }
                    t.fill()
                }
                var o = this.options.context
                    , e = this.options.data
                    , n = e.data;
                t.apply(this, [o, n]),
                    i.apply(this, [o, n])
            }
            function g() {
                var t = this.options.context
                    , i = this.options.data;
                t.save(),
                    this.options.avg_cost_color = n.draw_line.avg_cost_color;
                var o = this.options.avg_cost_color
                    , e = i.data;
                t.beginPath(),
                    t.lineWidth = 1,
                    t.strokeStyle = o,
                    t.fillStyle = "";
                for (var s = 0; s < e.length; s++) {
                    var a = h.get_x.call(this, s + 1)
                        , r = h.get_y.call(this, e[s].avg_cost);
                    0 == s ? t.moveTo(a, r) : t.lineTo(a, r)
                }
                t.stroke(),
                    t.restore()
            }
            function y() {
                function t() {
                    var t = this.options.context
                        , o = this.options.data
                        , e = o.data
                        , n = o.v_max.toFixed(0)
                        , s = this.options.unit_height
                        , a = 3 * s
                        , r = a
                        , p = t.canvas.height - this.options.canvas_offset_top
                        , c = i.options.c2_y_top
                        , d = this.options.rect_unit
                        , u = d.bar_w
                        , m = this.options.up_color
                        , f = this.options.down_color;
                    t.beginPath(),
                        t.strokeStyle = this.options.theme.drawXY.strokeStyle;
                    var v = this.options.padding.left
                        , g = this.options.padding.right;
                    t.lineWidth = 1,
                        t.fillStyle = this.options.color.fillStyle,
                        t.strokeStyle = this.options.theme.drawXY.dashed;
                    for (var y = 0; 3 >= y; y++) {
                        var A = h.format_unit(Math.floor(n / 3 * (3 - y)));
                        0 == o.max && 0 == o.min && (A = "-"),
                            t.fillStyle = this.options.theme.drawXY.textColor,
                            t.fillText(A, v - t.measureText(A).width - 5, c + a / 3 * y),
                        0 != y && 3 != y && (t.strokeStyle = this.options.theme.drawXY.dashed,
                            l(t, v, Math.floor(c + a / 3 * y), t.canvas.width - g + 4, Math.floor(c + a / 3 * y), 5))
                    }
                    t.fill(),
                        t.fillStyle = "rgba(100 ,100 ,100, 0.3)",
                        t.fillRect(v, c, window.crSpace, a),
                        t.lineWidth = 1,
                        t.beginPath(),
                        t.strokeStyle = this.options.theme.drawXY.strokeStyle,
                        t.rect(this.options.padding.left - .5, Math.round(c) - .5, t.canvas.width - this.options.padding.left - this.options.padding.right, Math.round(a) + 1),
                        t.stroke();
                    for (var x, y = 0; x = e[y]; y++) {
                        var w = x.volume
                            , _ = x.up
                            , b = w / n * r * .95
                            , T = h.get_x.call(this, y + 1)
                            , M = p - b;
                        t.beginPath(),
                            t.moveTo(T, M);
                        var k = x.time
                            , C = k.indexOf("09:2") > -1;
                        0 == y || C ? _ ? (t.fillStyle = m,
                            t.strokeStyle = m) : (t.fillStyle = f,
                            t.strokeStyle = f) : x.price >= e[y - 1].price ? (t.fillStyle = m,
                            t.strokeStyle = m) : (t.fillStyle = f,
                            t.strokeStyle = f),
                        0 !== b && t.rect(T - u / 2, M, u, b),
                            t.stroke(),
                            t.fill()
                    }
                }
                var i = this;
                t.call(i)
            }
            function A() {
                var t = this;
                p(this.options.code, function(i, o) {
                    if (i)
                        ;
                    else {
                        if ("false" === o[0].state)
                            return;
                        m.call(t, o)
                    }
                })
            }
            return t.prototype.init = function() {
                this.options.chartType = "TL";
                var t = document.createElement("canvas");
                this.container.style.position = "relative",
                    this.container.appendChild(t);
                try {
                    var i = t.getContext("2d")
                } catch (o) {
                    t = window.G_vmlCanvasManager.initElement(t);
                    var i = t.getContext("2d")
                }
                this.options.canvas = t,
                    this.options.context = i;
                var e = this.options.dpr;
                t.width = this.options.width * e,
                    t.height = this.options.height * e,
                    this.options.y_sepe_num = 13,
                    this.options.canvas_offset_top = t.height / this.options.y_sepe_num,
                    this.options.k_v_away = t.height / this.options.y_sepe_num,
                    this.options.scale_count = 0,
                    this.options.c_k_height = this.options.c_1_height = 8 * t.height / this.options.y_sepe_num,
                    this.options.c_v_height = 3 * t.height / this.options.y_sepe_num,
                    this.options.unit_height = 1 * t.height / this.options.y_sepe_num,
                    this.options.c1_y_top = 1 * t.height / this.options.y_sepe_num,
                    this.options.c2_y_top = 9 * t.height / this.options.y_sepe_num,
                    t.style.width = this.options.width + "px",
                    t.style.height = this.options.height + "px",
                    t.style.border = "0",
                    i.translate("0", Math.round(this.options.canvas_offset_top)),
                    this.options.color = {},
                    this.options.color.strokeStyle = "rgba(230,230,230, 1)",
                    this.options.color.fillStyle = "#717171",
                    i.fillStyle = this.options.color.fillStyle,
                    i.font = "12px Arial",
                    i.lineWidth = 1 * this.options.dpr,
                    i.strokeStyle = "rgba(230,230,230, 1)",
                    this.options.padding = {},
                    this.options.padding.left = i.measureText("10000.00").width + 5,
                    this.options.padding.right = i.measureText("+100.00%").width,
                    this.options.padding.top = 0,
                    this.options.padding.bottom = 0,
                    this.initTheme()
            }
                ,
                t.prototype.initTheme = function() {
                    if ("string" == typeof this.options.theme) {
                        this.options.theme = s;
                        var t = s.cover;
                        for (var i in t)
                            this.options[i] = t[i];
                        this.container.style.backgroundColor = this.options.theme.color.background_color,
                        -1 == this.container.className.indexOf(s.themeClass) && (this.container.className = this.container.className + " " + s.themeClass);
                        var o = this.options.context;
                        o.fillStyle = this.options.theme.color.background_color,
                            o.fillRect(0, 0, this.options.wdith, this.options.height)
                    } else
                        this.options.theme = a
                }
                ,
                t.prototype.draw = function() {
                    this.clear(),
                        this.init();
                    var t = this.options.interactive = new d(this.options);
                    t.showLoading();
                    var e = this
                        , n = this.options.code.charAt(this.options.code.length - 1);
                    (7 == n || 5 == n) && (this.options.isCR = !1);
                    var s = {
                        code: this.options.code,
                        type: this.options.type,
                        isCR: !!this.options.isCR,
                        timeBefore: this.options.timeBefore,
                        inter: t,
                        container: this.container,
                        width: this.options.width,
                        height: this.options.height
                    };
                    try {
                        r(s, function(n, s) {
                            n ? (t.showNoData(),
                                t.hideLoading()) : (s ? i.apply(e, [s]) : i.apply(e, [[]]),
                                o.call(e, e.options.context))
                        })
                    } catch (a) {
                        t.showNoData(),
                            t.hideLoading()
                    }
                }
                ,
                t.prototype.intervalDraw = function() {
                    function t() {
                        var t = this
                            , i = t.options.data.max
                            , n = t.options.data.min
                            , s = t.options.data.v_max
                            , a = {
                            code: t.options.code,
                            type: t.options.type,
                            isCR: !!t.options.isCR
                        };
                        r(a, function(a, r) {
                            if (a)
                                t.options.interactive.showNoData();
                            else {
                                p = r.data.length - 2;
                                var h = r.max
                                    , l = r.min
                                    , c = r.v_max;
                                t.options.data = r,
                                    h > i || n > l || s > c ? o.call(t, r) : e.call(t, r),
                                    p = t.options.data.data.length - 1
                            }
                        })
                    }
                    function o(t) {
                        this.clear(),
                            i.apply(this, [t])
                    }
                    function e(t) {
                        var i = this.options.context;
                        n.call(this, i, t, p),
                            s.call(this, i, t, p),
                            a.call(this, i, t, p)
                    }
                    function n(t, i, o) {
                        var e = h.get_x.call(this, o + 1)
                            , n = h.get_y.call(this, i.data[o].avg_cost)
                            , s = h.get_x.call(this, i.data.length)
                            , a = h.get_y.call(this, i.data[i.data.length - 1].avg_cost);
                        t.save(),
                            t.strokeStyle = "#F1CA15",
                            t.beginPath(),
                            t.moveTo(e, n),
                            t.lineTo(s, a),
                            t.stroke(),
                            t.restore()
                    }
                    function s(t, i, o) {
                        var e = h.get_y.call(this, this.options.data.min)
                            , n = h.get_x.call(this, o + 1)
                            , s = h.get_y.call(this, i.data[o].price)
                            , a = h.get_x.call(this, i.data.length)
                            , r = h.get_y.call(this, i.data[i.data.length - 1].price);
                        t.save(),
                            t.strokeStyle = "#639EEA",
                            t.beginPath(),
                            t.moveTo(n, s),
                            t.lineTo(a, r),
                            t.stroke();
                        var p = t.createLinearGradient(0, 0, 0, t.canvas.height);
                        p.addColorStop(0, "rgba(200,234,250,0.7)"),
                            p.addColorStop(1, "rgba(255,255,255,0)"),
                            t.beginPath(),
                            t.fillStyle = p,
                            t.beginPath(),
                            t.moveTo(n, s),
                            t.lineTo(a, r),
                            t.lineTo(a, e),
                            t.lineTo(n, e),
                            t.lineTo(n, s),
                            t.fill(),
                            t.restore()
                    }
                    function a(t, i) {
                        var o = i.data.length
                            , e = t.canvas.height / 4
                            , n = .9 * e
                            , s = h.get_x.call(this, o)
                            , a = e * i.data[o - 1].volume / i.v_max
                            , r = n - a
                            , p = this.options.rect_unit.bar_w;
                        t.save(),
                            t.fillStyle = this.options.avg_cost_color,
                            t.rect(s - p / 2, r, p, a),
                            t.restore()
                    }
                    var p, l = (new Date).getMinutes(), c = this.options.intervalTimer, d = this;
                    c && clearInterval(c),
                        this.options.intervalTimer = setInterval(function() {
                            var i = new Date
                                , o = i.getMinutes()
                                , e = !0;
                            i.getHours() >= 15 && i.getMinutes() >= 0 && (clearInterval(d.options.intervalTimer),
                                e = !1),
                            i.getHours() <= 9 && i.getMinutes() < 30 && d.options.isCR === !1 && (clearInterval(d.options.intervalTimer),
                                e = !1),
                            i.getHours() <= 9 && i.getMinutes() < 15 && d.options.isCR === !0 && (clearInterval(d.options.intervalTimer),
                                e = !1),
                            o !== l && e && (l = o,
                                t.call(d))
                        }, 1e4)
                }
                ,
                t.prototype.reDraw = function() {
                    this.clear(),
                        this.init(),
                        i.call(this, this.options.data)
                }
                ,
                t.prototype.clear = function() {
                    this.container.innerHTML = ""
                }
                ,
                t
        }();
        t.exports = f
    }
    , function(t, i, o) {
        var e = o(25)
            , n = o(21)
            , s = o(60)
            , a = o(7)
            , r = o(9)
            , p = function() {
            function t(t) {
                this.defaultoptions = a.draw_xy_web,
                    this.options = {},
                    this.options = n(this.options, this.defaultoptions, t),
                    this.draw()
            }
            function i(t, i, n, a, p) {
                for (var h, l = this, c = a.length, d = this.options.padding.left, u = this.options.padding.right, m = 0; h = a[m]; m++) {
                    if (t.beginPath(),
                        t.lineWidth = 1,
                        (c - 1) / 2 > m ? (0 == m ? (t.strokeStyle = this.options.theme.drawXY.strokeStyle,
                            t.moveTo(r(d), r(h.y)),
                            t.lineTo(r(t.canvas.width - u), r(h.y))) : (t.strokeStyle = this.options.theme.drawXY.dashed,
                            e(t, d, h.y, t.canvas.width - u + 4, h.y, 5)),
                            t.fillStyle = l.options.down_color) : m > (c - 1) / 2 ? (m == c - 1 ? (t.strokeStyle = this.options.theme.drawXY.strokeStyle,
                            t.moveTo(r(d), r(h.y)),
                            t.lineTo(r(t.canvas.width - u), r(h.y))) : (t.strokeStyle = this.options.theme.drawXY.dashed,
                            e(t, d, h.y, t.canvas.width - u + 4, h.y, 5)),
                            t.fillStyle = l.options.up_color) : (t.fillStyle = this.options.theme.drawXY.textColor,
                            t.strokeStyle = this.options.theme.drawXY.dashed,
                            t.moveTo(r(d), r(h.y)),
                            t.lineTo(r(t.canvas.width - u), r(h.y))),
                        isNaN(h.num))
                        t.fillText("0.00", 0, h.y);
                    else {
                        var f = (1 * h.num).toFixed(this.options.pricedigit)
                            , v = s.formatYNum(f / 1, p);
                        0 == i && 0 == n && (v = "-"),
                            t.fillText(v, d - t.measureText(v).width - 5, h.y)
                    }
                    t.stroke(),
                        o.call(l, t, i, n, h)
                }
            }
            function o(t, i, o, e) {
                var n = this.options.padding.right
                    , s = (i + o) / 2
                    , a = t.canvas.width;
                if (s) {
                    var r = ((e.num - s) / s * 100).toFixed(2) + "%";
                    e.num - s > 0 && (r = "+" + r)
                } else
                    var r = "0.00%";
                0 == i && 0 == o && (r = "-"),
                    t.fillText(r, a - n + 5, e.y),
                    t.stroke()
            }
            function p(t, i, o) {
                var n = this.options.padding.left
                    , a = this.options.padding.right
                    , p = this.options.c_1_height;
                t.beginPath(),
                    t.fillStyle = this.options.color.fillStyle;
                var h, l = t.canvas.width, c = 8.1 * this.options.unit_height, d = o.length, u = 2 * d, m = !1, f = this.options.isCR || !1, v = 0;
                "r" != this.options.type.toLowerCase() ? h = (l - n - a) / d : (f ? (v = (l - n - a) / (4 * (d - 2) + 1),
                    h = 4 * v) : h = (l - n - a) / (d - 1),
                    m = !0),
                    t.save(),
                    t.fillStyle = this.options.theme.drawXY.textColor,
                    t.font = "12px Arial,Helvetica,San-serif",
                    t.textBaseline = "top";
                for (var g = 0; d > g; g++) {
                    var y = o[g];
                    if (g === d - 1 && m)
                        t.fillText(y.value, s.get_x.call(this, y.index) - t.measureText(y.value).width, c);
                    else if (0 === g && m)
                        t.fillText(y.value, s.get_x.call(this, y.index), c);
                    else if (1 === g && f && m)
                        t.fillText(y.value, s.get_x.call(this, y.index), c);
                    else {
                        var A = s.get_x.call(this, y.index) - t.measureText(y.value).width / 2 + 2;
                        t.fillText(y.value, A, c)
                    }
                }
                t.restore();
                var x = (this.options.c_v_height,
                t.canvas.height - this.options.canvas_offset_top)
                    , w = this.options.c2_y_top
                    , _ = (l - n - a) / u;
                t.save(),
                    t.lineWidth = 1;
                for (var g = 0; u >= g; g++)
                    if (t.beginPath(),
                    0 != g && g != u) {
                        var A;
                        f ? 1 == g ? (t.save(),
                            A = v + n,
                            t.fillStyle = "rgba(100 ,100 ,100, 0.3)",
                            t.fillRect(n, 0, v, p),
                            window.crSpace = v,
                            t.restore()) : A = n + (g - 1) * h / 2 + v : A = n + g * _,
                            t.strokeStyle = this.options.theme.drawXY.dashed,
                            e(t, A, p, A, 0, 5),
                            e(t, A, x, A, w - 10, 5)
                    } else
                        t.strokeStyle = this.options.theme.drawXY.strokeStyle,
                            t.moveTo(r(n + g * _), r(p)),
                            t.lineTo(r(n + g * _), r(0)),
                            t.stroke();
                t.restore()
            }
            function h(t, i, o, e) {
                for (var n = (t - i) / (o - 1), s = [], a = 0; o > a; a++)
                    s.push({
                        num: 1 * i + a * n,
                        x: 0,
                        y: e - a / (o - 1) * e
                    });
                return s
            }
            return t.prototype.draw = function() {
                var t = this.options.data
                    , o = this.options.context
                    , e = t.max
                    , n = t.min
                    , s = this.options.sepe_num
                    , a = t.timeStrs
                    , r = t.pricedigit
                    , l = this.options.c_1_height
                    , c = h(e, n, s, l);
                i.call(this, o, e, n, c, r),
                    p.apply(this, [o, l, a])
            }
                ,
                t
        }();
        t.exports = p
    }
    , function(t) {
        var i = {
            getMktByCode: function(t) {
                if (t.Length < 3)
                    return t + "1";
                var i = t.substr(0, 1)
                    , o = t.substr(0, 3);
                return "5" == i || "6" == i || "9" == i ? t + "1" : "009" == o || "126" == o || "110" == o || "201" == o || "202" == o || "203" == o || "204" == o ? t + "1" : t + "2"
            },
            fixed: function(t, i) {
                var o = 0;
                t = t.toString();
                var e = t;
                for (o = 0; o < i - t.length; o++)
                    e = "0" + e;
                return e
            },
            transform: function(t) {
                return t.replace(/(\d{4})(\d{2})(\d{2})/g, function(t, i, o, e) {
                    return i + "-" + o + "-" + e
                })
            },
            windowToCanvas: function(t, i, o) {
                return {
                    x: i * this.options.dpr,
                    y: o * this.options.dpr
                }
            },
            canvasToWindow: function(t, i, o) {
                var e = t.getBoundingClientRect();
                return {
                    x: i / this.options.dpr,
                    y: (o + this.options.canvas_offset_top) * (Math.abs(e.bottom - e.top) / t.height)
                }
            },
            get_y: function(t) {
                var i = this.options.currentData && this.options.currentData.max || this.options.data.max
                    , o = this.options.currentData && this.options.currentData.min || this.options.data.min;
                return i == o ? this.options.c_k_height / 2 : this.options.c_k_height - this.options.c_k_height * (t - o) / (i - o)
            },
            get_x: function(t) {
                var i = this.options.context.canvas
                    , o = this.options.chartType
                    , e = this.options.rect_unit.rect_w
                    , n = this.options.currentData && this.options.currentData.data.length || this.options.data.data.length
                    , s = this.options.currentData && this.options.currentData.total || this.options.data.total
                    , a = this.options.padding.left
                    , r = this.options.padding.right;
                return "TL" == o ? (i.width - a - r) / s * t + a : (i.width - a - r) / n * t + a - e / 2
            },
            get_rect: function(t, i) {
                var o = (t.width - this.options.padding.left - this.options.padding.right) / i
                    , e = o * (1 - this.options.spacing);
                return {
                    rect_w: o,
                    bar_w: e
                }
            },
            format_unit: function(t, i) {
                i = void 0 == i ? 2 : i;
                var o = !1;
                0 > t && (t = Math.abs(t),
                    o = !0);
                var e = 0
                    , n = "";
                return o ? (1e4 > t ? e = t / 1 : t >= 1e4 && 1e8 > t ? (e = t / 1e4,
                    n = "万") : t >= 1e8 ? (e = t / 1e8,
                    n = "亿") : e = t / 1,
                    e = -1 * e) : 1e4 > t ? e = t / 1 : t >= 1e4 && 1e8 > t ? (e = t / 1e4,
                    n = "万") : t >= 1e8 ? (e = t / 1e8,
                    n = "亿") : e = t / 1,
                (e / 1).toFixed(i) / 1 == (e / 1).toFixed(0) / 1 && (i = 0),
                (e / 1).toFixed(i) + n
            },
            addEvent: function(t, i, o) {
                t.attachEvent ? (t["e" + i + o] = o,
                    t[i + o] = function() {
                        t["e" + i + o](window.event)
                    }
                    ,
                    t.attachEvent("on" + i, t[i + o])) : t.addEventListener(i, o, !1)
            },
            removeEvent: function(t, i, o) {
                t.detachEvent ? (t.detachEvent("on" + i, t[i + o]),
                    t[i + o] = null) : t.removeEventListener(i, o, !1)
            },
            findArrayMax: function(t) {
                for (var i = [0], o = 0; o < t.length; o++)
                    isNaN(t[o]) || i.push(parseFloat(t[o]));
                return i.sort(function(t, i) {
                    return i - t
                }),
                    i[0]
            },
            findArrayMin: function(t) {
                for (var i = [0], o = 0; o < t.length; o++)
                    isNaN(t[o]) || i.push(parseFloat(t[o]));
                return i.sort(function(t, i) {
                    return t - i
                }),
                    i[0]
            },
            formatYNum: function(t, i) {
                var o = "";
                if (t > 1e5 && 1e8 > t) {
                    var e = (t / 1e4).toFixed(i);
                    o = e >= 1e3 ? e.substr(0, 4) + "万" : e.substr(0, 5) + "万"
                } else if (t > 1e8) {
                    var e = (t / 1e8).toFixed(i);
                    o = e.substr(0, 5) + "万"
                } else
                    o = t.toFixed(t >= 1e3 && i > 2 ? 2 : i);
                return o
            }
        };
        t.exports = i
    }
    , function(t) {
        var i = {
            themeClass: "__Emcharts_theme_black",
            color: {
                background_color: "#1a1a1d"
            },
            drawXY: {
                strokeStyle: "#444444",
                fillStyle: "#1a1a1d",
                textColor: "#aaaaaa"
            },
            cover: {
                up_color: "#e60302",
                down_color: "#1ba34e"
            }
        };
        t.exports = i
    }
    , function(t) {
        var i = {
            themeClass: "",
            color: {
                background_color: "#ffffff"
            },
            drawXY: {
                strokeStyle: "#949494",
                dashed: "#D2D2D2",
                fillStyle: "#ffffff",
                textColor: "#252525"
            }
        };
        t.exports = i
    }
    , function(t, i, o) {
        function e(t, i) {
            var o = r[r.self].host_time;
            t.host && (o = t.host);
            var e = "fsData" + (new Date).getTime().toString().substring(0, 10)
                , p = {
                id: t.code,
                TYPE: t.type || "R",
                js: e + "((x))",
                rtntype: 5,
                isCR: t.isCR
            };
            t.timeBefore && (p.QueryStyle = "2.2",
                p.QuerySpan = t.timeBefore + ",10000"),
                n(o, p, e, function(o) {
                    var e, n, r = t.code;
                    o.stats === !1 ? a(t) : (n = s(o, p.isCR, t.type, r),
                        n.name = o.name,
                        i(e, n))
                })
        }
        var n = o(14)
            , s = o(64)
            , a = (o(13).fixed,
            o(65))
            , r = o(67);
        t.exports = e
    }
    , function(t, i, o) {
        function e(t, i, o, e) {
            var r = t.info.yc
                , p = {}
                , h = [];
            p.v_max = 0,
                p.yc = t.info.yc,
            "NaN" == parseFloat(p.yc).toString() && (p.yc = t.info.c,
                t.info.yc = t.info.c,
                r = t.info.c),
                p.pricedigit = (t.info.pricedigit.split(".")[1] || "").length,
                p.currentPrice = t.info.c,
                p.high = "-" === t.info.h ? t.info.yc : t.info.h,
                p.low = "-" === t.info.l ? t.info.yc : t.info.l,
                p.code = t.code,
                p.timeStrs = [],
                p.data = [];
            var l = t.info.ticks.split("|");
            if (7 === l.length) {
                var c = l[3] / 60
                    , d = l[4] / 60
                    , u = l[5] / 60
                    , m = l[6] / 60
                    , f = d - c
                    , v = m - u
                    , g = f + v
                    , y = g + 2
                    , A = 0;
                i && 242 == y && (A = 15,
                    h.push({
                        index: 0,
                        value: "09:15"
                    })),
                    h.push({
                        index: A,
                        value: n(c)
                    }),
                d === c + g / 2 && h.push({
                    index: Math.floor(A + g / 4),
                    value: n(c + g / 4)
                }),
                    h.push({
                        index: Math.floor(A + g / 2),
                        value: n(d) + "/" + n(u)
                    }),
                d === c + g / 2 && h.push({
                    index: Math.floor(A + 3 * g / 4),
                    value: n(u + 1 * g / 4)
                }),
                    h.push({
                        index: A + g,
                        value: n(m)
                    })
            } else if (5 === l.length) {
                var x = l[3] / 60
                    , w = l[4] / 60
                    , g = w - x
                    , _ = 30 * Math.floor((x + w) / 60)
                    , y = g + 2;
                h.push({
                    index: 0,
                    value: n(x)
                }),
                    h.push({
                        index: Math.floor(_ - x),
                        value: n(_)
                    }),
                    h.push({
                        index: g,
                        value: n(w)
                    })
            } else if (11 === l.length) {
                var c = l[3] / 60
                    , d = l[4] / 60
                    , u = l[5] / 60
                    , m = l[6] / 60
                    , b = l[7] / 60
                    , T = l[8] / 60
                    , M = l[9] / 60
                    , k = l[10] / 60
                    , f = d - c
                    , v = m - u
                    , C = T - b
                    , N = k - M
                    , g = f + v + C + N
                    , y = g + 2
                    , A = 0;
                h.push({
                    index: A,
                    value: n(c)
                });
                var E = n(d) + "/" + n(u)
                    , S = Math.floor(f / g * g);
                h.push({
                    index: S,
                    value: E
                }),
                    h.push({
                        index: Math.floor((f + v + C) / g * g),
                        value: n(T) + "/" + n(M)
                    }),
                    h.push({
                        index: Math.floor(g),
                        value: n(k)
                    })
            }
            p.total = t.info.total % y == 0 ? t.info.total : Math.floor(t.info.total / y + 1) * y,
            i && 242 == y && (p.total += 15);
            var D = [];
            t.data.length > 0 && D.push({
                index: 0,
                value: s(t.data[0].split(",")[0].split(" ")[0])
            });
            for (var L, R = 0; L = t.data[R]; R++) {
                var I = {}
                    , Y = L.split(",");
                p.v_max = Math.max(1 * Y[2], p.v_max),
                    I.up = Y[1] - r > 0 ? !0 : !1,
                    I.percent = 0 == r ? 0 : (Math.abs(Y[1] - r) / r * 100).toFixed(2),
                    I.time = Y[0].split(" ")[1],
                    I.dateTime = Y[0].split(" ")[0],
                    I.price = Y[1],
                    I.avg_cost = 1 * Y[3],
                    I.volume = 1 * Y[2],
                    p.high = Math.max(p.high, I.price, I.avg_cost),
                    p.low = Math.min(p.low, I.price, I.avg_cost),
                s(I.dateTime) != D[D.length - 1].value && D.push({
                    index: R,
                    value: s(I.dateTime)
                }),
                    p.data.push(I)
            }
            if (p.timeStrs = [],
                "r" === o.toLowerCase() ? p.timeStrs = h : "t2" === o.toLowerCase() ? "7" === e.charAt(e.length - 1) ? p.timeStrs = D.slice(1) : 11 == l.length ? p.timeStrs = D.slice(1) : (p.timeStrs.push(h[Math.floor(h.length / 2)]),
                    p.timeStrs.push(D[1]),
                    p.timeStrs.push({
                        index: D[1].index + h[Math.floor(h.length / 2)].index,
                        value: h[Math.floor(h.length / 2)].value
                    })) : p.timeStrs = D.slice(1),
            "NaN" == p.high.toString() || "NaN" == p.low || "NaN" == parseFloat(p.yc).toString()) {
                for (var W = 0, F = 0, R = 0; R < p.data.length; R++)
                    W = W < p.data[R].price ? p.data[R].price : W,
                        F = F > p.data[R].price ? p.data[R].price : F;
                "NaN" == p.high.toString() && (p.high = W),
                "NaN" == p.low && (p.low = F),
                    p.yc = "NaN" == parseFloat(p.yc).toString() ? ((W + F) / 2).toString() : ((p.low + p.high) / 2).toString()
            }
            return p.max = a(p.high, p.low, p.yc).max,
                p.min = a(p.high, p.low, p.yc).min,
            0 == p.data.length && (p.high = 0,
                p.low = 0,
                p.max = 0,
                p.min = 0),
                p
        }
        function n(t) {
            return r(Math.floor(t / 60) % 24, 2) + ":" + r(Math.floor(t % 60), 2)
        }
        function s(t) {
            return new Date(t).getMonth() + 1 + "月" + new Date(t).getDate() + "日"
        }
        var a = o(12)
            , r = o(13).fixed;
        t.exports = e
    }
    , function(t, i, o) {
        var e = o(66);
        t.exports = function(t) {
            var i = document.createElement("img");
            i.src = e,
                i.style.position = "absolute",
                i.style.width = "152px",
                i.style.height = "154px",
                i.style.left = (t.width - 152) / 2 + "px",
                i.style.top = (t.height - 154) / 2 - t.height / 10 + "px",
                t.inter.hideLoading(),
                t.container.style.border = "1px solid #ddd",
                t.container.appendChild(i)
        }
    }
    , function(t) {
        t.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJgAAACaCAMAAABi1HEYAAACbVBMVEUAAAB/fn5/fn5/fn5/fn5/fn5/fn5/fn5/fn5/fn5/fn5/fn5/fn5/fn5/fn7Y2NjY2Nh/fn7Y2NitudPY2NjY2NjY2Njh4eFnhsRnhsTZ2tzd3d1sisbY2NjY2NjY2NjY2NjY2NjY2NhnhsTY2NjY2NjY2NjY2NhnhsRph8XY2NjY2NiDnM1nhsRnhsRnhsRnhsRnhsVnhsRoh8TZ2t3U3e7Y2NhnhsRnhsTY2NhnhsTY2NjY2NjY2NjY2NhnhsRnhsTY2NhnhsTY2NhnhsRnhsTY2NhnhsTY2NjY2NhnhsRnhsRoh8XY2NhqicbY2NhqicZnhsT///9nhsRnhsT////6+/1nhsT///9nhsRnhsT///9piMXU19/5+vvY2NjY2NhnhsRnhsRnhsRnhsTY2Nj////Y2NjY2Nj////+//////9nhsRoh8X///+3xuOestr////t8Pj////////O2Oz9/v6jtduQp9TU3e57lsz09vv///////////////////+5x+Te5fKjtdv///////+yweH////////////Y2NiFntBnhsR/fn6kttz7/P3Cz+fu8fnW3u+QptRqiMX+/v/8/f7i6PRohsR5lMtsisbx9Prl6vWgtNvf5vPb4/K5x+Squ952kslph8Xq7vfG0emtvuCYrdeNpNN9mM18l8vQ2u3J1OrAzeanud2bsNmEnc+9yua0xOLe39+KotKkss9/mc34+fz2+Pzv7+/AzeeestnLz9aUqtZzj8lwjcj3+fzz9vvX4PCRp9XBx9S0v9P19fXe4ejb3eLEytS9xNOaq82Up8yUpsyNosvFlkRjAAAAiXRSTlMAQL9/7zAgz2DfEJ+vcI/90lA+AqzujB0G/V8UCvbg3NepmpiHeUQzMg8MB/77+PLBu2YsI/7w7NLMzMS9u7mjnZOSfnNtZV5bUU8+NysgGBbdt7Crjo2NgoF8elc66+jn5NrXxrKwn3JYUUpIQyomHfTy7ufk4N7e2djX1sy9qqmYlYJ/bWA9JmpQmOQAAAgLSURBVHja7NlJa1NRGMbx9zu4MNHssknBNkOVihtFlIKKOM8uHBCcRZwHnNGX+5CbOdq0NtbayWpbEZWKdfpiGsSBHs9A7j3nbvpbJZt7/9zkJC/nkIkzO6934Y8nzFxE+++X3dh5hsIQu1tl/hxe2Cfm6p0EBZbIvHvP7HNoqsz1SjZGQSUxNMwh63+CJAXUHa/UOXT1ClIUzFq8ZgueBX5kh/GULXiKwxRMJ3y2wEcnBQOwFcBC2ELYL/k8W1EqUTBszUKY47BCga0olymApQ8f2FuVBzuoHZs37N1+hZlDC/O8eWHPsHXHvg2byVgilVubSeOnOath+T609GR396ZipPNo18pOtAx9H5nstxrGtf7JkfIcWlbc3LOYFBbtr/F0ZdlUs9TgFrthvxRXNKfGG8znD5DcPZ+56HOLm7C/qmOQl6XiY1VmjiSMm+iRLtPVGGGHYcIjW0cShzAcXRhPIksScdQiDKtjJUmk0YgwrIQMSRzFN54vn7cTVirxPDNIkkQSM2yB9y+WGkCOJHIYYIGzsGlsIolNmGYL/qlRhNUq8QRJJOIVYVkWCnbCymWep3GVpK6NCssSCDdMcd1bJHWbBYDqjqGG7Sep/RGG+XySpE5GGDY3fo6kFg/ORhVWRA8p9KAYUdhLZEkhi5cRhb3BblLYjTcRhY2ilxR6MRo0TPwP8v6Sh42jmxS6MRFN2PvHXTFSiHU9/hJGmPBSN48NYhUprcKgMI9ZCBPmsVmsIaU1eBV4ktGHifJYT0rrkY8k7C02ktJGeO2PWAHCxvo6SKkDfTVhHrMQJsxjl0jjckHYIrAQJl73GGkcY44kbA9p3BfCxG97+GGDwydI44RBmPrW0mxF2BA2k8bykan/h/FvFsIaSJNWGg3nT6yEDGllsMJ5WBNrTc6am9ILeJ5+fmgn7CNypNWLKedhy7CJtFJ4pwtTfHZthfmt3QGtWOdzX/5DaCWsjiNkYCWeCvNYmGHiPPYCq8nAahRYECBM7xnWkYF1eO04bAynyMBpvHUc1vy0hQxsweeqMI+FGSbOYxfIyMWi41XJ28jINnYdtouM7HEcNvtqCRk5zlVpmCh42AS6ychZb8Jl2IfHXYvIyKI0vjoJ0+8OiEc3DsNmsIYMJTHjMGwA68lQDgMOwzxxd0BxdOMurNaHDhJIj258Z2H92ErGjqBueF4ZfB6bxA4y9oN5+3tpKgzjAP78BxvbmKFMVPwBapdBF7puuhBhaVmBEmqXQVHRfZfB+/DQNjzbDmuT9ss1lxbOmcNFRNrv+pvykclZuoPnbXvf/FwMzs3hCzvveR/4vucmFf65+HPWA1rWaAEcW6CMtmArNA2O3adXbZfLfOnE6sYjcGw2t23NY2qCWfPYE5Cw2LR61ASz7vsUJNzQGOwWSLitLVjm/TOQcO9FQlOwHamD/Vzd6AkWzl28ABK4utESbJUugwyubrQEi9AVkMHVTXvBmINgm3QVZHB1oyXYNj0AGVzd6AgW+0CzIIWrGw3B8nQXJF2nlGDqzvPzb5nmQNIcvRbqbdE8SJqnNaFeke6ApGn6JNSL5R+CpGsrW4KpO88v2GMvSBgb6R70ha3Vo0LjvouD3SNj4MDo8FB/nwsPfdYQbDdeQ0RXX//Q8CjYm5kacLvwWE1DsHTyABtc7oGpGTjN6/G78S9fCwXlwd7SPjZz+z1eaBbyL+NJ+7SjOljcMF14wrI/BMc8k9iCK3dU3RgUFgq8I4Ormiy2MOkB1uPD1taPRrKSqg8/S1zV/MaWfD0AXb1o4welea+kjFBgk8eqIlWxtd4uCKKdumGWhUiYRkp0XCppJkTZNOpoIwgBtFUlM50Kp6koOu4lPY9HklRFOwEIor1qjmgvU9oV4mMHV0A4yn9ldIPoJ9oK8jNm79uv7F5h/FJMvCGLOERtXFM2Fosko/n1A2R2zxivyrNUauNG54Llsl8q9e+VCjLbVck8E3iuTHia3vxLeE4s+UOn98r/jvdK2+lCP2u6OHse06kxjzmeYN0BVC7wh916R6EQhoIwnCVcNBdB0aPiA7QNgRAtQiCQRViK7n8FKoi9qN183XR/OT9+Ptg7wiiI7TJQ+kEQDYuNgyhkj3Ri8s4arnP5pEbmmhvr/CQ69ro+EW3j3VpXpizmUStFlP13V7I8VkaklB7nojRVvTrftCLpGQAAAADA1k697IgJw1AAvX7mjcSH+59LmECnnS4qVZlVzyII5MWVHfPfQo6/VB3fqKrPk+gUYfzJmSEMG8gFCwucFsMOHB9IuYmcdX164WJxCptmHO1TMFG+FcYWXmp2gBgPo0tLdMOUSTifQBG6zIrU6GmvMLZIjdIgp0STY5GO12yLMh92HSl49VRW9MbYgpk1cT714/lpVi+iESmTzQgU+0e5VEpj0CEAE14l46ehXdUsFBB+g/Fagl2Xn1m7nOpPsPgVYJxMOJcs6mhtBfN7lKmttdyCCceBJxhNorQA9ah2ZFgFZ6SBiyWNe5RaruPAFi6XQybCozewvbMmZiKqGN3DAZSI7s8ot6GQpQgWD0I8Me3eSQ6CR0u4nM4CPtYoDXtQfN1EZqxgJwBXA0YB0CO/pWVE2rqVFK8nWFNfwe7TuGTTE6gpxhPMw4K+/GD3dqypYQWzwEQaXFGTSuRVKgVbg329Y85quJRGRE0B1FxSZraUDDl6RRY9NCM+fFOwquyYrDNzt1nAeb5rrwByBwY36tjdsUpYzO93/M5XIf7ZD/zGuOwb2hFNAAAAAElFTkSuQmCC"
    }
    , function(t) {
        t.exports = {
            self: "formal",
            dev: {
                host_k: "http://61.129.129.146/em_ubg_pdti_fast/api/js",
                host_time: "http://61.129.129.146/em_ubg_pdti_fast/api/js",
                host_pci: "http://nuyd.eastmoney.com/EM_UBG_PositionChangesInterface/api/js"
            },
            formal: {
                host_k: "http://pdfm2.eastmoney.com/EM_UBG_PDTI_Fast/api/js",
                host_time: "http://pdfm2.eastmoney.com/EM_UBG_PDTI_Fast/api/js",
                host_pci: "http://nuyd.eastmoney.com/EM_UBG_PositionChangesInterface/api/js"
            }
        }
    }
    , function(t, i, o) {
        function e(t, i) {
            var o = s[s.self].host_pci
                , e = "changeData" + (new Date).getTime().toString().substring(0, 10)
                , a = {
                id: t,
                style: "top",
                js: e + "([(x)])",
                ac: "normal",
                check: "itntcd",
                cd: e
            };
            n(o, a, e, function(t) {
                var o;
                o = t ? !1 : !0,
                    i(o, t)
            })
        }
        var n = o(14)
            , s = o(67);
        t.exports = e
    }
    , function(t, i, o) {
        var e = o(21)
            , n = (o(60),
            o(7))
            , s = o(70)
            , a = function() {
            function t(t) {
                this.defaultoptions = n.interactive,
                    this.options = {},
                    this.options = e(this.options, this.defaultoptions, t)
            }
            return t.prototype.crossTime = function(t, i, e) {
                var n = this.options.canvas_offset_top
                    , s = this.options.canvas.height
                    , a = this.options.container
                    , r = this.options.padding.left
                    , p = this.options.padding.right;
                if (!this.options.cross) {
                    this.options.cross = {};
                    var h = document.createElement("div");
                    h.className = "cross-y",
                        h.style.height = s - n + "px",
                        h.style.top = n + "px",
                        this.options.cross.y_line = h;
                    var l = document.createElement("div");
                    l.className = "cross-x",
                        l.style.width = t.width - r - p + "px",
                        l.style.left = r + "px",
                        this.options.cross.x_line = l;
                    var c = document.createElement("div");
                    c.className = "cross-p",
                        c.style.width = "11px",
                        c.style.height = "11px",
                        this.options.point_width = 11,
                        c.style.borderRadius = c.style.width,
                        c.style.background = "url(" + o(71) + ")",
                        this.options.cross.point = c;
                    var d = document.createDocumentFragment();
                    d.appendChild(h),
                        d.appendChild(l),
                        d.appendChild(c),
                        document.getElementById(a).appendChild(d)
                }
                var h = this.options.cross.y_line;
                this.options.cross.y_line && (h.style.left = i + "px");
                var l = this.options.cross.x_line;
                this.options.cross.x_line && (l.style.top = e + "px");
                var c = this.options.cross.point;
                if (c) {
                    var u = this.options.point_width;
                    c.style.left = i - u / 2 + "px",
                        c.style.top = e - u / 2 + "px"
                }
            }
                ,
                t.prototype.showTipsTime = function(t, i, o, e, n) {
                    var a, r = this.options.canvas, p = this.options.context, h = this.options.padding.left, l = this.options.padding.right, c = this.options.canvas_offset_top, d = this.options.c_1_height, u = this.options.container, m = this.options.code;
                    a = 0 > e ? o[0] || {} : e > o.length - 1 ? o[o.length - 1] : o[e],
                        this.options.lastItemData = o[o.length - 1];
                    var f, v;
                    7 != m.charAt(m.length - 1) && 5 != m.charAt(m.length - 1) ? (v = s(a.volume, "手"),
                        f = s(a.volume * a.price * 100, "元")) : (v = s(a.volume, "股"),
                        f = s(a.volume * a.price, "元")),
                        this.hoverData = a;
                    var g = ["#ff0000", "#666666", "#17b03e"]
                        , y = n.yc
                        , A = (y.split(".")[1] || "").length || 3
                        , x = a.price
                        , w = ((x - y) / y * 100).toFixed(A);
                    "NaN" == w && (w = "0");
                    var _;
                    _ = x - y > 0 ? g[0] : x - y == 0 ? g[1] : g[2];
                    var b = n.name + "[" + (n.code || "-") + "] " + (a.dateTime || "-") + " " + (a.time || "-")
                        , T = " 价格:<span style='color: " + (_ || "-") + ";'>" + (x || "-") + "</span> ";
                    T += " 涨幅:<span style='color: " + (_ || "-") + ";'>" + w + "%</span> ",
                        T += " 成交量:" + v;
                    var M = document.createElement("div");
                    M.innerHTML = T;
                    var k = this.options.width - h - l;
                    if (k - p.measureText(b).width - p.measureText(M.innerText).width > 0 && (b += T),
                        this.options.webTimeTips) {
                        var C = this.options.webTimeTips.time_y_left
                            , N = this.options.webTimeTips.time_y_right
                            , E = this.options.webTimeTips.time_x_bottom
                            , S = this.options.webTimeTips.time_x_top;
                        C.style.top = i + "px",
                            C.innerHTML = a.price,
                            C.style.left = h - C.clientWidth + "px",
                            C.style.display = "block",
                            N.style.top = i + "px",
                            N.style.display = "block",
                            N.innerHTML = "0.00" === a.percent ? a.percent + "%" : 0 == a.percent ? a.percent + "%" : (a.up ? "+" : "-") + a.percent + "%",
                            E.style.left = t + "px",
                            E.style.display = "block",
                            E.innerHTML = a.time,
                            E.style.left = t < h + E.clientWidth / 2 ? h + "px" : t > r.width - l - E.clientWidth / 2 ? r.width - l - E.clientWidth + "px" : t - E.clientWidth / 2 + "px",
                            E.style.top = d + c - E.clientHeight + "px",
                            S.style.display = "block",
                            S.innerHTML = b
                    } else {
                        this.options.time_data = o;
                        var C, N, E, S, D = document.createDocumentFragment();
                        C = document.createElement("div"),
                            C.setAttribute("id", "time_y_left"),
                            C.className = "time-tips-coordinate",
                            N = document.createElement("div"),
                            N.setAttribute("id", "time_y_right"),
                            N.className = "time-tips-coordinate",
                            N.style.left = r.width - l + "px",
                            E = document.createElement("div"),
                            E.setAttribute("id", "time_x_bottom"),
                            E.className = "time-tips-coordinate",
                            S = document.createElement("div"),
                            S.setAttribute("id", "time_x_top"),
                            S.className = "time-tips-top",
                            S.style.top = c - 18 + "px",
                            S.style.left = h + "px",
                            this.options.webTimeTips = {},
                            this.options.webTimeTips.time_y_left = C,
                            this.options.webTimeTips.time_y_right = N,
                            this.options.webTimeTips.time_x_top = S,
                            this.options.webTimeTips.time_x_bottom = E,
                            D.appendChild(C),
                            D.appendChild(N),
                            D.appendChild(E),
                            D.appendChild(S),
                            document.getElementById(u).appendChild(D),
                            C.style.display = "block",
                            C.innerHTML = a.price,
                            C.style.left = h - C.clientWidth + "px",
                            C.style.top = i + "px",
                            C.style.display = "none",
                            N.style.top = i + "px",
                            N.innerHTML = (a.up ? "+" : "-") + a.percent + "%",
                            E.style.display = "block",
                            E.innerHTML = a.time,
                            E.style.left = t + "px",
                            E.style.top = d + c - E.clientHeight + "px",
                            E.style.display = "none",
                            S.innerHTML = b,
                            S.style.display = "block"
                    }
                }
                ,
                t.prototype.show = function() {
                    if (this.options.cross) {
                        var t = this.options.cross.x_line;
                        t && (t.style.display = "block");
                        var i = this.options.cross.y_line;
                        i && (i.style.display = "block");
                        var o = this.options.cross.point;
                        o && (o.style.display = "block")
                    }
                    if (this.options.webTimeTips) {
                        var e = this.options.webTimeTips.time_y_left;
                        e && (e.style.display = "block");
                        var n = this.options.webTimeTips.time_y_right;
                        n && (n.style.display = "block");
                        var s = this.options.webTimeTips.time_x_bottom;
                        s && (s.style.display = "block")
                    }
                    this.showTipsNew()
                }
                ,
                t.prototype.showTipsNew = function() {
                    var t = (this.options.canvas,
                        this.options.context)
                        , i = this.options.padding.left
                        , o = this.options.padding.right
                        , e = this.options.code
                        , n = this.hoverData;
                    if (n) {
                        var a;
                        try {
                            a = this.options.webTimeTips.time_x_top
                        } catch (r) {
                            return
                        }
                        var p, h;
                        7 != e.charAt(e.length - 1) && 5 != e.charAt(e.length - 1) ? (h = s(n.volume, "手"),
                            p = s(n.volume * n.price * 100, "元")) : (h = s(n.volume, "股"),
                            p = s(n.volume * n.price, "元"));
                        var l = this.allData.data
                            , c = ["#ff0000", "#666666", "#17b03e"]
                            , d = l.yc
                            , u = (d.split(".")[1] || "").length || 3
                            , m = n.price
                            , f = ((m - d) / d * 100).toFixed(u);
                        "NaN" == f && (f = "0");
                        var v;
                        v = m - d > 0 ? c[0] : m - d == 0 ? c[1] : c[2];
                        var g = l.name + "[" + l.code + "] " + n.dateTime + " " + n.time
                            , y = " 价格:<span style='color: " + v + ";'>" + m + "</span> ";
                        y += " 涨幅:<span style='color: " + v + ";'>" + f + "%</span> ",
                            y += " 成交量:" + h;
                        var A = document.createElement("div");
                        A.innerHTML = y;
                        var x = this.options.width - i - o;
                        x - t.measureText(g).width - t.measureText(A.innerText).width > 0 && (g += y),
                            a.innerHTML = g
                    }
                }
                ,
                t.prototype.hide = function() {
                    if (this.options.cross) {
                        var t = this.options.cross.x_line;
                        t && (t.style.display = "none");
                        var i = this.options.cross.y_line;
                        i && (i.style.display = "none");
                        var o = this.options.cross.point;
                        o && (o.style.display = "none")
                    }
                    if (this.options.webTimeTips) {
                        var e = (this.options.canvas,
                            this.options.context)
                            , n = this.options.padding.left
                            , a = this.options.padding.right
                            , r = this.options.webTimeTips.time_y_left;
                        r && (r.style.display = "none");
                        var p = this.options.webTimeTips.time_y_right;
                        p && (p.style.display = "none");
                        var h = this.options.webTimeTips.time_x_bottom;
                        h && (h.style.display = "none");
                        var l = this.options.code
                            , c = this.options.lastItemData;
                        if (!c)
                            return;
                        var d, u, m = this.options.webTimeTips.time_x_top;
                        7 != l.charAt(l.length - 1) && 5 != l.charAt(l.length - 1) ? (u = s(c.volume, "手"),
                            d = s(c.volume * c.price * 100, "元")) : (u = s(c.volume, "股"),
                            d = s(c.volume * c.price, "元"));
                        var f = this.allData.data
                            , v = ["#ff0000", "#666666", "#17b03e"]
                            , g = f.yc
                            , y = (g.split(".")[1] || "").length || 3
                            , A = c.price
                            , x = ((A - g) / g * 100).toFixed(y);
                        "NaN" == x && (x = "0");
                        var w;
                        w = A - g > 0 ? v[0] : A - g == 0 ? v[1] : v[2];
                        var _ = f.name + "[" + f.code + "] " + c.dateTime + " " + c.time
                            , b = " 价格:<span style='color: " + w + ";'>" + A + "</span> ";
                        b += " 涨幅:<span style='color: " + w + ";'>" + x + "%</span> ",
                            b += " 成交量:" + u;
                        var T = document.createElement("div");
                        T.innerHTML = b;
                        var M = this.options.width - n - a;
                        M - e.measureText(_).width - e.measureText(T.innerText).width > 0 && (_ += b),
                            m.innerHTML = _
                    }
                }
                ,
                t.prototype.showLoading = function() {
                    if (this.options.loading)
                        this.options.loading.style.display = "block";
                    else {
                        var t = document.getElementById(this.options.container)
                            , i = document.createElement("div");
                        i.className = "loading-chart",
                            i.innerText = "加载中...",
                            i.style.height = this.options.height - 100 + "px",
                            i.style.width = this.options.width + "px",
                            i.style.paddingTop = "100px",
                            this.options.loading = i,
                            t.appendChild(i)
                    }
                }
                ,
                t.prototype.hideLoading = function() {
                    this.options.loading.style.display = "none"
                }
                ,
                t.prototype.showNoData = function() {
                    if (this.options.noData)
                        this.options.noData.style.display = "block";
                    else {
                        var t = document.getElementById(this.options.container)
                            , i = document.createElement("div");
                        i.className = "loading-chart",
                            i.innerText = "暂无数据",
                            i.style.height = this.options.height - 100 + "px",
                            i.style.width = this.options.width + "px",
                            i.style.paddingTop = "100px",
                            this.options.noData = i,
                            t.appendChild(i)
                    }
                }
                ,
                t
        }();
        t.exports = a
    }
    , function(t, i, o) {
        function e(t, i) {
            var o = n.format_unit(t, 2);
            return o = "万" == o.charAt(o.length - 1) ? parseFloat(o) + "(万" + i + ")" : "亿" == o.charAt(o.length - 1) ? parseFloat(o) + "(亿" + i + ")" : (parseFloat(o) || "-") + "(" + i + ")"
        }
        var n = o(60);
        t.exports = e
    }
    , function(t) {
        t.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAALCAYAAACprHcmAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA4RpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDE0IDc5LjE1Njc5NywgMjAxNC8wOC8yMC0wOTo1MzowMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDowMjE1Nzg4OS0yMzdkLTE0NDAtYWIxYy0yOGJmNGJhZWY5NjIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RDhGOTBDRkI3RkNGMTFFNjg3ODVDNTIxQjFBMDY0QjkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RDhGOTBDRkE3RkNGMTFFNjg3ODVDNTIxQjFBMDY0QjkiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKFdpbmRvd3MpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6YjMwZDIxYTktNjBmMy0xNzQ1LWJkZDYtODU0MWRiZTA5NDFiIiBzdFJlZjpkb2N1bWVudElEPSJhZG9iZTpkb2NpZDpwaG90b3Nob3A6MzVhYTg1NGYtNTg1NC0xMWU2LTljYTQtOTFjM2Q1YTMwYTEyIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+73iagQAAAPtJREFUeNpckbFOAkEURe9uFgOENUIJCA00NkpiR6nRhJI/tDax1/AJEGj4AFhXEwsgLgETi/VeeAMrk5ydnZnzZua98XpPKayVyA1pkCLZkDmZkLWEwMRLck9yODYFX5E2GZDI5yckd06Mv9GcfqGr3oJytlEY2NFnmh3G6C+2eLQgfCZ4va3hxQKufbuCFhsUHzLXgMYfyX5dufiWDFY/qLPz8L95y+1BLvqWNS7yiNmlJ3JaLiCy/43k3aB2jlmlgLesqXE1PMhRYHVsKQklw7uPdCWe9K4NTPyVJzmxOu7qLCEjOVHriXsUHfVMOlYd94KaH7sX/BNgADkYR6OgIb3wAAAAAElFTkSuQmCC"
    }
    , function(t, i, o) {
        function e(t) {
            for (var i = !0, o = [], e = this, s = e.options.data.data, h = s.length - 1, l = 0; l <= t.length - 1; l++)
                for (var c = t[l].split(","), d = {
                    changeTime: c[1],
                    changeName: c[2],
                    changeType: c[3],
                    changeInfo: c[4],
                    isProfit: c[5]
                }, u = c[1], m = a(c[3]), f = e.options.canvas_offset_top + e.options.c_1_height - 40, v = p.join(",").indexOf(c[3]) >= 0 ? "red" : "green"; h >= 0; h--)
                    if (u == e.options.data.data[h].time) {
                        for (var g = e.options.data.data[h].price, y = e.options.data.data[h].up, A = e.options.data.data[h].percent, x = 0, l = 0; l < o.length; l++)
                            o[l] == u && x++;
                        n(e.container, r.get_x.call(e, h + 1), f - 4 * x, m, d, g, y, A, v, i),
                            i = !1,
                            o.push(u);
                        break
                    }
        }
        function n(t, i, o, e, n, a, l, c, d, u) {
            var m = document.createElement("div");
            m.setAttribute("style", "opacity: 0.5;filter:alpha(opacity=50) "),
                m.style.position = "absolute",
                m.style.top = o + 20 + "px",
                m.style.left = i - 2 + "px",
                m.style.width = "6px",
                m.style.height = "6px",
                m.style.borderRadius = "50%",
                m.style.webkitBorderRadius = "50%",
                m.style.msBorderRadius = "50%",
                m.style.transitionDuration = "0.3s",
                m.style.webkitTransitionDuration = "0.3s",
                m.style.msTransitionDuration = "0.3s",
                m.style.backgroundColor = d,
                t.appendChild(m);
            var f = document.createElement("div");
            t.appendChild(f);
            var v = n.changeType
                , g = n.changeTime
                , y = n.changeInfo;
            u && (navigator.appVersion.indexOf("MSIE 9.0") > -1 ? s(m) : (m.setAttribute("class", "timePositionChangesAni"),
                m.setAttribute("id", "lastPositionChangesAni"))),
                r.addEvent(m, "mouseover", function() {
                    try {
                        document.querySelector("#lastPositionChangesAni").setAttribute("class", "")
                    } catch (t) {}
                    this.style.width = "10px",
                        this.style.height = "10px",
                        this.style.marginLeft = "-2px",
                        this.style.marginTop = "-2px",
                        this.style.zIndex = "10",
                        f.className = "timeChangeMainPad";
                    var n = '<div class="timeChangeTriangle" style="z-index:1;background-color:#65CAFE;"></div><table cellpadding="0" cellspacing="0" style="position:relative;z-index:2;min-width:120px"><tr><td style="background-color: #D1E7FF;text-align: center;padding: 6px;"><img src="' + e + '" style="height: 20px;" /><p style="padding: 0;margin: 0;white-space:nowrap;">' + v + '</p></td><td style="padding: 6px;background-color: #ffffff;"><div style="padding: 4px;">' + g + "</div>";
                    p.join(",").indexOf(v) > -1 && (n += '<div style="padding: 4px;white-space:nowrap;color: red;">' + y + "</div>"),
                    h.join(",").indexOf(v) > -1 && (n += '<div style="padding: 4px;white-space:nowrap;color: green;">' + y + "</div>"),
                        n += "</td></tr></table>",
                        f.innerHTML = n,
                        f.style.display = "block",
                        f.style.left = i - f.clientWidth / 2 + 5 + "px",
                        f.style.top = o + 50 + "px"
                }),
                r.addEvent(m, "mouseout", function() {
                    try {
                        document.querySelector("#lastPositionChangesAni").setAttribute("class", "timePositionChangesAni")
                    } catch (t) {}
                    this.style.width = "6px",
                        this.style.height = "6px",
                        this.style.marginLeft = "0px",
                        this.style.marginTop = "0px",
                        this.style.zIndex = "",
                        f.style.display = "none"
                })
        }
        function s(t) {
            var i = 6
                , o = 1
                , e = setInterval(function() {
                (i > 10 || 6 > i) && (o = -o),
                    i += o,
                    t.style.width = i + "px",
                    t.style.height = i + "px",
                    t.style.marginLeft = (6 - i) / 2 + "px",
                    t.style.marginTop = (6 - i) / 2 + "px"
            }, 100);
            setTimeout(function() {
                clearTimeout(e),
                    t.style.width = "6px",
                    t.style.height = "6px",
                    t.style.marginLeft = "0px",
                    t.style.marginTop = "0px"
            }, 3200)
        }
        var a = o(73)
            , r = o(60)
            , p = ["火箭发射", "封涨停板", "机构买单", "快速反弹", "大笔买入", "有大买盘", "向上缺口", "竞价上涨", "高开5日", "60日新高", "打开跌停", "大幅上涨"]
            , h = ["快速下跌", "封跌停板", "机构卖单", "高台跳水", "大笔卖出", "有大卖盘", "向下缺口", "竞价下跌", "低开5日", "60日新低", "打开涨停", "大幅下跌"];
        t.exports = e
    }
    , function(t, i, o) {
        function e(t) {
            var i;
            switch (t) {
                case "火箭发射":
                    i = o(74);
                    break;
                case "快速下跌":
                    i = o(75);
                    break;
                case "封涨停板":
                    i = o(76);
                    break;
                case "封跌停板":
                    i = o(77);
                    break;
                case "机构买单":
                    i = o(78);
                    break;
                case "机构卖单":
                    i = o(79);
                    break;
                case "快速反弹":
                    i = o(80);
                    break;
                case "高台跳水":
                    i = o(81);
                    break;
                case "大笔买入":
                    i = o(82);
                    break;
                case "大笔卖出":
                    i = o(83);
                    break;
                case "有大买盘":
                    i = o(84);
                    break;
                case "有大卖盘":
                    i = o(85);
                    break;
                case "向上缺口":
                    i = o(86);
                    break;
                case "向下缺口":
                    i = o(87);
                    break;
                case "竞价上涨":
                    i = o(88);
                    break;
                case "竞价下跌":
                    i = o(89);
                    break;
                case "高开5日":
                    i = o(90);
                    break;
                case "低开5日":
                    i = o(91);
                    break;
                case "60日新高":
                    i = o(92);
                    break;
                case "60日新低":
                    i = o(93);
                    break;
                case "打开跌停":
                    i = o(94);
                    break;
                case "打开涨停":
                    i = o(95);
                    break;
                case "大幅上涨":
                    i = o(96);
                    break;
                case "大幅下跌":
                    i = o(97)
            }
            return i
        }
        t.exports = e
    }
    , function(t) {
        t.exports = "data:image/gif;base64,R0lGODlhCQAUAOZdAPeUHhKi3PzU1gew7+kgKSi78uocJukcJukdJveaTfihXCm78ewnL+AiLegcJjO+8tMpNqxBWN8eKkDC8/rX2SG48faPHu0pMvze3/eiZi+98rc7TuccJ+gdJpWgut4jL/7y8vzcy/rT1en4/o6Zee0gKfvMzvvQt9v0/fORl+w6Rf7u7waw7+1weGe94dYoNeohKu5VXUO25+0lLekgKuscJX7W9+MgLDa/8r81R/b8/gmx8P3i4zyu4J2YcFOipNHw/PrGyKpEWescJtYoNv74+Mru/Ljo+/3n6PaLLNolMvaNM/rR03PT9sMzRcnu/GLN9Qym4PvKzP3z9P729+UfKWfP9eskLoK31f3j5ACu7+YcJ+0cJP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMwMTQgNzkuMTU2Nzk3LCAyMDE0LzA4LzIwLTA5OjUzOjAyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAyMTU3ODg5LTIzN2QtMTQ0MC1hYjFjLTI4YmY0YmFlZjk2MiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo1NjkwMjhERjRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1NjkwMjhERTRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNCAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpmMGNjNWYyNS1lNWU1LWEwNGUtYmEwZi00ZTFlZjU2NTQ4NWUiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDphNzU3MjcyOC00Y2MwLTExZTYtYmY2NS1hMDg3YTU2NzM5MWYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAABdACwAAAAACQAUAAAHjYBdghQpVIKHUzQlKiCHXS1cXBcxjlsHHA4wh0UEBhBDV0iCKzcBFS41WYIYQloDC06qXVJKUVoBHbJBXB8bVVyyJpHDPIICw5ECgkwzwwwiXUAyElvVCD1GD1oRwy9aOAUsWDkNRB47GhNNXT4/JF02UFYoXUsASV0jR0+CCgAJgnQcCmHhhKNDGRwFAgA7"
    }
    , function(t) {
        t.exports = "data:image/gif;base64,R0lGODlhCQAUAOZXAPvP0ewcJEnF9OYdKOgcJ+gcJveUHuscJUjF9O35/u0dJfBwd37C4/eTHvvO0OchLAKs6uIdKfWKJmLN9eIgK2hskPm6kveYRk+ktP77++odJo51kfzU1nCZvKzl+uxBS8svQGGjqO0kLFXJ9Of3/f3t7hmv6XW53WmVufrHqOccJ/ve4OYcJ7BAVOsdJeUdJzKnwuEiLfaLLOz5/vrGyP/9/OkuOLY8T/74+P3i46fj+esnMDuJuPGWm/7y8veeVcI0Rf77/PvJy0zG9ME0RQSr6SS58feSRV3M9eMhLPzX2M4sOzjA8+0wOuwcJcju/BW18Nby/Cy88v3+//3j5ACu7+0cJP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMwMTQgNzkuMTU2Nzk3LCAyMDE0LzA4LzIwLTA5OjUzOjAyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAyMTU3ODg5LTIzN2QtMTQ0MC1hYjFjLTI4YmY0YmFlZjk2MiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo1NkE1RTc1MzRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1NjlENTIxMDRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNCAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpmMGNjNWYyNS1lNWU1LWEwNGUtYmEwZi00ZTFlZjU2NTQ4NWUiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDphNzU3MjcyOC00Y2MwLTExZTYtYmY2NS1hMDg3YTU2NzM5MWYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAABXACwAAAAACQAUAAAHjIBXgldHNYODKQ0Wgx6CPwYXgjNIT1cyBhJXUTpMI1chMBhTE0NGVSgUVi4bVVIIVURWsjFVAgkMEQMvAwQnJFcAsrIKAIIcwrJKgkLIVlSCNFY3FUDOgg5LEFVFSc9XOS1VUAIg3j4aPCYdAd4ZLAUEBSolgw8BB047hz2yTQuHQWyI+HBIEI4VhwIBADs="
    }
    , function(t) {
        t.exports = "data:image/gif;base64,R0lGODlhCAAMALMPAP8mJv+Ojv9UVP+Pj//Q0P8lJf9TU//Pz/8MDP/S0v/w8P8KCv8PD//MzP8AAP///yH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMwMTQgNzkuMTU2Nzk3LCAyMDE0LzA4LzIwLTA5OjUzOjAyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAyMTU3ODg5LTIzN2QtMTQ0MC1hYjFjLTI4YmY0YmFlZjk2MiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo1NzIxQjE0NzRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1NzIxQjE0NjRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNCAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpmMGNjNWYyNS1lNWU1LWEwNGUtYmEwZi00ZTFlZjU2NTQ4NWUiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDphNzU3MjcyOC00Y2MwLTExZTYtYmY2NS1hMDg3YTU2NzM5MWYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAAPACwAAAAACAAMAAAEJ9DJSat7j6x18FOAVCiYQBnPYAVJg0hIk2CMxHhP7dyeztM2nA8TAQA7"
    }
    , function(t) {
        t.exports = "data:image/gif;base64,R0lGODlhCAAMALMPAD6yPprXmmfCZ5vXm9Xu1T2xPWbCZtTu1PL68iWoJdfv19Lt0ieoJympKRykHP///yH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMwMTQgNzkuMTU2Nzk3LCAyMDE0LzA4LzIwLTA5OjUzOjAyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAyMTU3ODg5LTIzN2QtMTQ0MC1hYjFjLTI4YmY0YmFlZjk2MiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo1NzM3MEUwNjRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1NzM3MEUwNTRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNCAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpmMGNjNWYyNS1lNWU1LWEwNGUtYmEwZi00ZTFlZjU2NTQ4NWUiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDphNzU3MjcyOC00Y2MwLTExZTYtYmY2NS1hMDg3YTU2NzM5MWYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAAPACwAAAAACAAMAAAEKPC95lyTklo868Xal3mSsjAVsyhD5TrBI7yGhABVgWBEkhySl3DoiAAAOw=="
    }
    , function(t) {
        t.exports = "data:image/gif;base64,R0lGODlhIQANAPfWAB+IY0GaXiKJY5fGU1+qWpXFU12pdC2PYQ+AZonAVGStWpLEU6DLUjyXX/8KCv/S0h+IZEObXmqwWJnHU/8MDBiEZV6pdEKaXhCAZpbFU0SbXheEZTWSYFKkdZrJU//w8P/MzAByao/BVLrb7DiWYPv14FuoTjeUXx2GZKPPoEufXUmdXVCiY/f79UOaXm2ydprHU1KjYCqOYkScYBuEZeHrzyiNYTeUacDdvO7w6kyfd0iddf8mJo/DUxOBZp3LzxOCZSiMXw9/ZhuGZBqFf0ugdHK1sGOsWSyOYv/Q0BSBUo7DUwp9WxR9U5XFUlambq3TnkKbXmeuWQp4Z16qWoO9t3W1Ufz9/J/KUL/dt2KtpWOsfKjRnpvKrY3Dm0qfUESbfDCRXonAgyyOUmGsWU2hXdvp5nu5fMrjwCCFZSOJY6PMUS2QhjGRYSqNYZjHUzqXXjqXX06hdiGIYzKSYTOSYBSCZhyGZGWuWf+dnSGJY/9TU1GiXDeVX2WtWj2YXl+qWanR0COKY0idYVyoZa3TqleldRWBWC2PXhuFZDSSYE+hdiyOYXy5VQJ2aEugXBiEV0yhggBZSSOJZWStW0efnw5+Zm2xWEScX//Pz1SlclGjXFKjcrTX1p/KXMHc3P+Ojgx9cVGiaSCJYyCJZReEZCuOZY3BVAB0ajCRYS6QYF+rWFWmdJXFUFGjZwd2af+PjxJ+ZkGbXo3BU3KyfWqwUzWUYA1+ZpjGWfr1/367v/8lJf+ens7lwiuOYU6jXYnAU2iuWwBqZzeVWXG0TQBya4i/TzqWX1moYlSkdajSw/b69DOSYf9UVBaDZVWlXv8PD4rAVA5/ZjuXX16qdP8AAP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMwMTQgNzkuMTU2Nzk3LCAyMDE0LzA4LzIwLTA5OjUzOjAyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAyMTU3ODg5LTIzN2QtMTQ0MC1hYjFjLTI4YmY0YmFlZjk2MiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo1NzNEMjg4MzRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1NzNEMjg4MjRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNCAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpmMGNjNWYyNS1lNWU1LWEwNGUtYmEwZi00ZTFlZjU2NTQ4NWUiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDphNzU3MjcyOC00Y2MwLTExZTYtYmY2NS1hMDg3YTU2NzM5MWYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAADWACwAAAAAIQANAAAI/wCtCRxI0NoVHIRYGaDGsKHDh5rOFJyIRpQiJCcaTNvIsePGBgfccBrIK0/BXlsQ3MKAQJrLlzCltRwiJJUFgUkcOMg0MEsMGXCiXAgQANOwMTOIKo0g648qAC5ufuBRrdquDwJd0XB2R4AaAQIOFfHSZBJYAXMgpCFxwI4GA9aaVa26R+COYnEkECAAaNWLT7lomdhLAE+ZEIk4bHgLa+5cUNYW2SoAY8CAVp5KGCECxsoayxMYPBLEbMMFAw9AUKhKAcQDax1UZEgQLRowZJ0CsRGGSESP2h7I+GpTgYMhgdCqQhvYYcWC2tFmhQlVxUwlJo2eR3tDhREdIH1YIH5XPjAZn9m1Tz1zJEnLiCDGlkRL4EGKjQOWBkEZX225QDl1TDBAAQU4gYsyuvzQBTFYEDgAA79UgMINKQyUXH8D6YDKMZf4ocARwYhRQw5f1EKJAgpIsMkrPkTARQsWkifQE7FgoAcAAEAAgBKmRAIJKTgCMEopUxxQyDICBQQAOw=="
    }
    , function(t) {
        t.exports = "data:image/gif;base64,R0lGODlhIQANAPfUAB+IY0GaXiKJY5fGU1+qWpXFU12pdC2PYQ+AZonAVGStWh+WHzWSYGqwWCGXITyXX16pdJLEU9bs1pnHU/H58ReEZRCAZqDLUhiEZUKaXkObXkSbXpbFU1KkdZrJUx+IZNDp0CGIY43BVLrb7I/BVEmdXWOsfEKbXvf79anR0MHc3EufXRyGZJ/KUDeVX4nAg0+hdtvp5kScYCiNYY3BU1moYp/KXEyfdziiOFSkdUGbXgp4Z22ydm2xWDqXXhJ+ZiGJYxuGZGKtpU6hdlWlXoO9txSCZhSBUo3DmxR9U5jGWc7lwmiuWxiEV8rjwCOKY6PMUSyOYXW1UWWuWY/DUwd2aSCFZfr1//z9/GqwUwp9WxOCZUqfUCCJZTCRXpvKrXKyfWGsWbTX1gJ2aEyhglKjcluoTiuOZQ9/ZgByajWUYABqZzGRYe7w6pnPmZjHUzOSYTKSYS2QhiCJYzqWX2OsWWO2Y16qWpXFUCOJZWStW2WtWvb69BuFZA5+Zp3Lz5XFUkidYSiMX2euWYi/T7/dtxuEZdTr1EugdCuOYTiWYI7DU6jRnk6jXcDdvPv14DSSYHy5VTeUaZrHU3u5fF+rWC6QYEefn4nAU02hXUScX1SlciOJY1GiaSyOUlGjXHK1sBeEZDCRYTOSYD2YXhaDZVCiY0OaXl+qWSqOYhOBZlyoZSyOYlWmdHG0TRWBWAByawB0ajeUXzeVWQ1+ZqPPoNPq0yqNYTqXX63TqlGjZ1ambhqFf+Hrzy2PXlKjYKjSw2K1YgBZSVGiXH67v63TnleldR2GZAx9cUSbfDmiOUugXEiddZjPmCSYJIrAVA5/ZjuXX16qdBaSFv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMwMTQgNzkuMTU2Nzk3LCAyMDE0LzA4LzIwLTA5OjUzOjAyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAyMTU3ODg5LTIzN2QtMTQ0MC1hYjFjLTI4YmY0YmFlZjk2MiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo1NzY0RDRDNDRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1NzY0RDRDMzRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNCAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpmMGNjNWYyNS1lNWU1LWEwNGUtYmEwZi00ZTFlZjU2NTQ4NWUiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDphNzU3MjcyOC00Y2MwLTExZTYtYmY2NS1hMDg3YTU2NzM5MWYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAADUACwAAAAAIQANAAAI/wCpCRxIkBoWR6taGZDGsKHDh5soFZzopBMkVrIeRNvIsePGBwdulZlIcIkJBLQsIIDGsqVLaCuDoBEFQaCzadOcDSz0K5WPExkCBNA0y5MMoUg16CBlCcCpmtRu5hyoy1ApFgI4CRDwChGSJHm2CgjxwYqiA0Y2GLCJU6dAZrBwNSBAAFUlHiqugDFDl8CUTGn6MKiglu1UgTDUFJg0YAAeG49A8UomBUrjCReWPYFTIcPaqG0HdljBIcGzZ5hqiEkhZ40vElROewiTiA0GBsYkgHCA0wEICdQ6lIhw+hkNL8iKxLikJRLxZ2/uRImzxYUpNzizT2tGLcew0qdFEH0ZI0zICEGEFj1L4GHQjAN+AhWjZkd7MIFDRk0YUKAAICXAEPPHF6600N8AFzSCwTGS1CIQBcrghAMFAt0QCx097KFAHUy80EsbXGShhwIKNPBJFapowAgKAx2ywAK2DLTLDxYAAQAAHwBwxBlkNNHFjQDMEcoOB+TCh0ABAQA7"
    }
    , function(t) {
        t.exports = "data:image/gif;base64,R0lGODlhEwASAOZFAP8BAf8EBP8GBv/9/f+Jif/S0v/Kyv/7+/8UFP91df80NP+pqf82Nv/j4//x8f/39//29v/Ly/8jI/++vv/k5P84OP+EhP8qKv8zM//w8P/p6f/U1P/r6/9RUf/i4v/y8v8gIP94eP/Gxv8uLv+Vlf8FBf+AgP+Fhf+xsf/l5f/t7f9UVP8WFv92dv9tbf/5+f9YWP/u7v/b2/8YGP9qav9cXP9ERP+Bgf9BQf+ysv/T0/+srP8CAv+amv/8/P8HB/8lJf+vr//AwP8kJP8AAP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMwMTQgNzkuMTU2Nzk3LCAyMDE0LzA4LzIwLTA5OjUzOjAyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAyMTU3ODg5LTIzN2QtMTQ0MC1hYjFjLTI4YmY0YmFlZjk2MiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo1NkIzNzIyRTRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1NkIzNzIyRDRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNCAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpmMGNjNWYyNS1lNWU1LWEwNGUtYmEwZi00ZTFlZjU2NTQ4NWUiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDphNzU3MjcyOC00Y2MwLTExZTYtYmY2NS1hMDg3YTU2NzM5MWYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAABFACwAAAAAEwASAAAHjIBFgoOEhUUyJIaKRTo3I0Q8B4uCHyFARJiYKZOCExeZmBGcggUBoDmjPhWgRD2jBKAtQyacGz+ZHUUQBoINDoUHDJkIGoQvChK8gyegC4UJmCUEA0UFApkrhUIAoDYeGJksHIUPLtyZ15k7iygIrJgwnBQ4rDMqowMWpphBo4MiIIjU8EcoA40YkwIBADs="
    }
    , function(t) {
        t.exports = "data:image/gif;base64,R0lGODlhEwASAOZCALrkuvv++9fv19Ds0CGmIZbVlh2kHf3+/fP68y6rLiClIJLTkh6lHu/570q3SsXoxfz+/Ey3TMfpx+b15jivODCsMEGzQdHs0W7Fbk64TmrDarLgsuX15czrzDyxPEW0Rff896XbpYTOhOf154/Sj0m2SY7SjiCmIDuwO4fPh1m8Wfr9+jGtMej26Njv2GTBZOv369nw2aHZoWfCZ1e8V4XOhfL68u347d/y37Xhtfj8+CKmInrKej2xPbjiuH3LffD58BykHP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMwMTQgNzkuMTU2Nzk3LCAyMDE0LzA4LzIwLTA5OjUzOjAyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAyMTU3ODg5LTIzN2QtMTQ0MC1hYjFjLTI4YmY0YmFlZjk2MiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo1NkIzNzIzNjRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1NkIzNzIzNTRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNCAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpmMGNjNWYyNS1lNWU1LWEwNGUtYmEwZi00ZTFlZjU2NTQ4NWUiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDphNzU3MjcyOC00Y2MwLTExZTYtYmY2NS1hMDg3YTU2NzM5MWYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAABCACwAAAAAEwASAAAHi4BCgoNCNjxAhImKHRRBGIqQBwsKQZU+kIQjNJWcLA2YQgAJnKQakDo/BqQEpDmKEqqcKhwlnBU3iiKVJwUHQgKslTOKKw4oA4QLpBuKEwiJARGcCTCghDE7nC9CIMjWBaQ1HibWQhAZpEEh5b+UnADlDxbpF9YIKT3pLexCLiQfQRgE4DcIhwwhgQAAOw=="
    }
    , function(t) {
        t.exports = "data:image/gif;base64,R0lGODlhDgAOANU6APS+Q/bYV/K2M/THRvXMS/fcXPO9O/+IiPO4NfbVVf8GAvXMSvK0MfO6N/4VBvfaWvt0L/TGRPbUU/O+PPhiIPlrJ/xbO/txSf4XCPfbW/TCQP8GA/4TBvO5Nv4UCPWyO/h6KvSsQPWRKveiPffaWfXPTvt2MvteJvbXV/qFOPXIR/x5XvTDQffZWPrir/O4PfjUifXLSfTHSP736PO7RfXOTfXLSvO5PffdXf8AAP///wAAAAAAAAAAAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMwMTQgNzkuMTU2Nzk3LCAyMDE0LzA4LzIwLTA5OjUzOjAyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAyMTU3ODg5LTIzN2QtMTQ0MC1hYjFjLTI4YmY0YmFlZjk2MiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo1NkM1QzFDOTRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1NkM1QzFDODRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNCAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpmMGNjNWYyNS1lNWU1LWEwNGUtYmEwZi00ZTFlZjU2NTQ4NWUiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDphNzU3MjcyOC00Y2MwLTExZTYtYmY2NS1hMDg3YTU2NzM5MWYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAA6ACwAAAAADgAOAAAGikCdcAaj3W402EzIdL1ktlrNJnu5moBYC8fFtWKA6+wVw7ESXMICF3sRZVvbBPcQBLyyoo1bQEgiA102RjVdJQYdGV01R4U4JCgIKgEFXIw0ezgEGgIGHydcgzBwOAMNDCIOHiYQFRdjZVwgCjm1tjouWVspG7a3uE9RIxgcFCsHB0xERjchFktCQQA7"
    }
    , function(t) {
        t.exports = "data:image/gif;base64,R0lGODlhDgAOANU7APS+Q/K2M/bYV/O4NfO9O5XVlTCnIPfcXJG9ViKmHvXMS8fFQ/O6N/bVVfTHRvO3NTSoIfXPTuLAPY+3M/O5NvXIRzSnH3q1MPfaWiKlHfO+PPbXV/TDQZHCP6HBPaDGQ/faWfbYWJLFa+K6QvTCQPfbW/TGRPbWVnq5S4+uLZHAPPXMSvbUU8WxLffZWPrir/O4PfjUifXLSfTHSP736PO7RfXOTfXLSvO5PRykHPfdXf///wAAAAAAAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMwMTQgNzkuMTU2Nzk3LCAyMDE0LzA4LzIwLTA5OjUzOjAyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAyMTU3ODg5LTIzN2QtMTQ0MC1hYjFjLTI4YmY0YmFlZjk2MiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo1NkQyMjdBNzRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1NkQyMjdBNjRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNCAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpmMGNjNWYyNS1lNWU1LWEwNGUtYmEwZi00ZTFlZjU2NTQ4NWUiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDphNzU3MjcyOC00Y2MwLTExZTYtYmY2NS1hMDg3YTU2NzM5MWYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAA7ACwAAAAADgAOAAAGicCdkBar4XC1GE3IfMFmN5vtNoO9mgCZS8fVuWSAKw0m03EaXMVKJ4MRZ9ubRocJCLyz4o17GLBMDl03RjZdEQQUJV02R4U6IBsDFQIHXIw1ezoXEgEEJCFcgwgTKh0GFi0PDCd4MTmvsBkeXW00sLAJH15gVwUFIikGEAtTVVdMOzQoI0dJS0JBADs="
    }
    , function(t) {
        t.exports = "data:image/gif;base64,R0lGODlhDwAOAOZzAOa2CPVlBui5Cf/20fHCF/Xehe68Cf0SAfjGBuW0B+2+C+q7Eei5COu6Dum7Cuy5Bum5C+q7C+u8Cum6C/zLDPHCCvTIJ//66Om6EPC+Buy+EO2/HOm2BvXJKP/XPffPOP4FAPXDB+a0Cvzz1Oq7CvXKKe7DI/XJKevAHuKyDOq+HN6sB/jNLOCvCfTHJu/BH/DDIOe3Ce+zH+y6Cea1CfdvK+e4Ce/CIOW0Ceq6DN2rBuu9C+7AHv6EevFyBui4Cf3TNO2/C+u9Gue3CP7UNee4Fey+Eey+FvPHJf7ZTPJiBfbKKvFhBPHOSuy+DvCWCvRyB+i3C+e1Bum6CfRkB9yqBf0WAuu/G+a3E+y+D+OyDPLGJP7kfO7GK+e6FP345OCuCeW1B/HQUu7BH+6tCvdSBf0VAee4FO+WFtypBPlZIOCvCv7kfee2C/7ZS+y9C//WN+y9DP8AAP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMwMTQgNzkuMTU2Nzk3LCAyMDE0LzA4LzIwLTA5OjUzOjAyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAyMTU3ODg5LTIzN2QtMTQ0MC1hYjFjLTI4YmY0YmFlZjk2MiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo1NkRCMUU5OTRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1NkRCMUU5ODRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNCAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpmMGNjNWYyNS1lNWU1LWEwNGUtYmEwZi00ZTFlZjU2NTQ4NWUiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDphNzU3MjcyOC00Y2MwLTExZTYtYmY2NS1hMDg3YTU2NzM5MWYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAABzACwAAAAADwAOAAAHw4BzcwNsHwQGMwYEH1wDghdJDWBnN1sdMGNFLQ1uFx5RPHBwQCVYK1UWcC9tHhQ0SKJCaTprRKIWOBQaCCIpGy4nSywdG1oiCBoQcRUhGQ8cUhwPGSEVcRALAHHbcQpBCtxxAAsTcQwJAhJvcW8SAgkMcRMYMdxvEQ4RZGXbQxg5cWyEmbJj2xMzB5goCUAFxQ97DnyAkEOxYhcjXgCQWAdlYkWKX8RwO3JFBRorBwIEqNFD0IgCTUw4iZNFhpoCIwQFAgA7"
    }
    , function(t) {
        t.exports = "data:image/gif;base64,R0lGODlhDwAOAOZ1AOy9C+a2CIuxE/fPOPjGBu2+C+68Ceq7C+2/HOy+EPXehem6EOu6Dui5COy5BvTIJ/zLDOu8Cuq7Eeu9C+i5Cem6C+W0B//66P/20fXDB/zz1CKlHOm5C/HCF+m2BvXJKPHCCv/XPfC+Bua0Cum7CvbKKvXKKf7ZS+y+DuvAHu7GK+m6CeOyDOa3E96sB/345Oe4FPDDIDSnGtzBItu7DOe3Cea1Cey6CdyqBfLQU5TRh+CvCem6CvPHJe7AHo67OeKyDIqwFO/BH+e4Cf7kff7UNZixEfLGJPHOS+y+D/7ZTOW1COy+Fui3C/7kfOq+HOy+Ee7BH/THJu2/C7+5G9ypBHevFeu9Guq6DOu/G+e6FOe1BvjNLC+mGuq7Cv3TNOe4FcC4DzCmGoetEd2rBne2MOCvCu7DIoivEvXJKeW0Cee2C+/CIOi4CeCuCee3CJqzEjOmG//WNxykHOy9DP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMwMTQgNzkuMTU2Nzk3LCAyMDE0LzA4LzIwLTA5OjUzOjAyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAyMTU3ODg5LTIzN2QtMTQ0MC1hYjFjLTI4YmY0YmFlZjk2MiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo1NkU3ODQ3NjRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1NkU3ODQ3NTRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNCAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpmMGNjNWYyNS1lNWU1LWEwNGUtYmEwZi00ZTFlZjU2NTQ4NWUiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDphNzU3MjcyOC00Y2MwLTExZTYtYmY2NS1hMDg3YTU2NzM5MWYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAB1ACwAAAAADwAOAAAHw4B1dRhEAx0GNwYdA04YghdKDG4wbEcfMVFgOwwnFyFNPnJyXyYtLjgPckJrIRA2PaJXVWRmRaIPahAJBCNACFJpJVwfCCwjBAkcdCAZIg4eWx4OIhkgdBwSAXTbdAVTBdx0ARIVdA0WFBEAdAARFBYNdBULNdtWNAckB+vbbwtBAtCM6SIjzLYJK5YMoYNljsOHG4zwmMCtTYqHEOGw8xJACxQVOn4IECAmDpUnWZhwy/FCkAYFZWagoJPkDBIFGgQFAgA7"
    }
    , function(t) {
        t.exports = "data:image/gif;base64,R0lGODlhDgAMAMQdAP8vL//p6f95ef99ff/r6/8tLf+QkP93d//8/P+YmP84OP8MDP/09P9kZP+Tk//5+f/q6v+cnP8PD/9CQv8uLv+Njf9SUv8GBv8HB/8ODv8BAf+srP8AAP///wAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMwMTQgNzkuMTU2Nzk3LCAyMDE0LzA4LzIwLTA5OjUzOjAyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAyMTU3ODg5LTIzN2QtMTQ0MC1hYjFjLTI4YmY0YmFlZjk2MiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo1Nzk1RDA4QjRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1Nzk1RDA4QTRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNCAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpmMGNjNWYyNS1lNWU1LWEwNGUtYmEwZi00ZTFlZjU2NTQ4NWUiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDphNzU3MjcyOC00Y2MwLTExZTYtYmY2NS1hMDg3YTU2NzM5MWYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAAdACwAAAAADgAMAAAFS2C3VV1pdoyydUHGGWf3TFwWDBynOSZi5RwBAKhJlBpADgWT1EQOSc6CGa1ehtVoAZdNCiCubi0g0nQ1q9JGUs2kTQRBAYMBDAimEAA7"
    }
    , function(t) {
        t.exports = "data:image/gif;base64,R0lGODlhDgAMAMQdAEa1RojPiIvRi+v360S0RO347aPao2XBZSeoJ4bOhiGmIVe8V+z47EW0RfX79fz+/KfcpympKfr9+pzXnJrWmk64Tp/Yn3XIdSKmIh2kHSipKLXhtRykHP///wAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMwMTQgNzkuMTU2Nzk3LCAyMDE0LzA4LzIwLTA5OjUzOjAyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAyMTU3ODg5LTIzN2QtMTQ0MC1hYjFjLTI4YmY0YmFlZjk2MiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo1Nzg2NjcwMTRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1NzdEQjQ1QTRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNCAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpmMGNjNWYyNS1lNWU1LWEwNGUtYmEwZi00ZTFlZjU2NTQ4NWUiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDphNzU3MjcyOC00Y2MwLTExZTYtYmY2NS1hMDg3YTU2NzM5MWYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAAdACwAAAAADgAMAAAFR2AnFgGBYYBQiOwWcXCsbW0W31xGMxqOawOBzxcADHGm402BuWUgCRzCCMsYRJdbQ5izsB6HWGDQm7BEkgUH2NlQzixHhRYCADs="
    }
    , function(t) {
        t.exports = "data:image/gif;base64,R0lGODlhEQAOANUoAP8PD/9VVf/8/P84OP/19f/i4v96ev9mZv94eP8eHv+mpv8ICP/s7P/Kyv/c3P8BAf+oqP+AgP93d//Z2f8ODv9FRf8ZGf9WVv99ff/Nzf9jY/9ra/+dnf9UVP/e3v/q6v88PP+vr/8gIP9HR/9LS/+0tP/S0v8AAP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMwMTQgNzkuMTU2Nzk3LCAyMDE0LzA4LzIwLTA5OjUzOjAyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAyMTU3ODg5LTIzN2QtMTQ0MC1hYjFjLTI4YmY0YmFlZjk2MiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo1NkU3ODQ3RTRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1NkU3ODQ3RDRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNCAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpmMGNjNWYyNS1lNWU1LWEwNGUtYmEwZi00ZTFlZjU2NTQ4NWUiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDphNzU3MjcyOC00Y2MwLTExZTYtYmY2NS1hMDg3YTU2NzM5MWYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAAoACwAAAAAEQAOAAAGZ0CUENXYDI9I4Sdx4iSTgsrptMg8j4jpVMS4okIP7XQkEBI6BOEEINZihIeTRhgJgMSWwKUA0SqGJWIkQh5sUxQOQoFagygGA1oDEoqCQyZaJoCVQpdTmZSMlpiaoZyjoFONKJ0nmUEAOw=="
    }
    , function(t) {
        t.exports = "data:image/gif;base64,R0lGODlhEQAOANUgACWoJfb79mjCaEm2SU64Ttjv2OLz4onQiYrQiuDz4Nbv1qjcqPT79IfPh9vw2ympKSipKKvdq4zRjHfId3DGcLDfsKXbpbLgskGzQUCyQG7FbqrdqnTHdNfv16LaohykHP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMwMTQgNzkuMTU2Nzk3LCAyMDE0LzA4LzIwLTA5OjUzOjAyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAyMTU3ODg5LTIzN2QtMTQ0MC1hYjFjLTI4YmY0YmFlZjk2MiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo1NkYzODhCMzRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1NkYzODhCMjRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNCAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpmMGNjNWYyNS1lNWU1LWEwNGUtYmEwZi00ZTFlZjU2NTQ4NWUiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDphNzU3MjcyOC00Y2MwLTExZTYtYmY2NS1hMDg3YTU2NzM5MWYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAAgACwAAAAAEQAOAAAGaEAQyPMpfjxCUMfYSRKNSOGy2BQ+i1El02k8Jqef6rCbBYuvXlCDYCQcrGRhAmJ8GBCCQXcgkIAqRhcgCgBdRQAFQhwfE0kWhh8bSQECAUkgGl0Ul5xCDBlFGJadnAUAAA6kpAsRqklBADs="
    }
    , function(t) {
        t.exports = "data:image/gif;base64,R0lGODlhFAAWAOZEAP9AQP+AgP8BAf/Pz/8JCf+vr/8VFf9gYP8GBv81Nf9kZP/f3/+4uP8rK/+Zmf+2tv++vv8KCv+fn//8/P/v7/8DA/8QEP9bW/9ZWf8XF/+7u/+cnP/a2v9ERP9hYf+Njf8eHv84OP8gIP8CAv82Nv+pqf+ysv8ICP87O//9/f8SEv9aWv/T0/8EBP/Ly/9QUP8hIf+goP/g4P+6uv8HB/+iov+5uf8kJP/V1f+rq/8wMP/b2/+Bgf8REf+mpv/5+f+jo//Q0P+/v/8AAP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMwMTQgNzkuMTU2Nzk3LCAyMDE0LzA4LzIwLTA5OjUzOjAyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAyMTU3ODg5LTIzN2QtMTQ0MC1hYjFjLTI4YmY0YmFlZjk2MiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo1NjgxNzkwMzRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1NjgxNzkwMjRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNCAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpmMGNjNWYyNS1lNWU1LWEwNGUtYmEwZi00ZTFlZjU2NTQ4NWUiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDphNzU3MjcyOC00Y2MwLTExZTYtYmY2NS1hMDg3YTU2NzM5MWYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAABEACwAAAAAFAAWAAAHu4BEAwCEhESHiImJQkNCjkKKkYiMkpWTQ5aWjAEBA5mKC5wHFgefkgNDBaaRAAGriiISqwWQFKMUqwM6Q0MvC6+SEBCINh0JDTA9GZIaEREziBe8vCiRDAS8BAxEOwkh0wqKDyfTQzQ1N0MqDbwfiSYI5fJDBCBDD4gF8bwCAvMXHAzIOJRj3xABDhzII/GDiItDJVpMG7GBSAoF0wzgUORhWoUYiXgIMMAi0oQVQyoAiSQhSKUJGHwACwQAOw=="
    }
    , function(t) {
        t.exports = "data:image/gif;base64,R0lGODlhEwAXANU/AFW7VY7SjtTu1Pz+/OP04/r9+jivOHHGcd/y3zyxPOf15/H58VG5UUS0RDmwOXnJeS+rL7jiuCqqKlm8WdDs0GvEa+T05GnDaXPHc0GzQXDGcNvx28TnxHbIdqrdqsHmwUe1R23Ebb7lvimpKTCsMCuqK2PBY1e8V5rXmuX15eLz4vv++zauNqLaovL68iGmIWzEbHXIdTuwOzWuNR+lH3LHcl6+Xub15tHs0WbCZmLAYofPhzGtMcboxhykHP///yH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMwMTQgNzkuMTU2Nzk3LCAyMDE0LzA4LzIwLTA5OjUzOjAyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAyMTU3ODg5LTIzN2QtMTQ0MC1hYjFjLTI4YmY0YmFlZjk2MiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo1NjgxNzkwQjRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1NjgxNzkwQTRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNCAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpmMGNjNWYyNS1lNWU1LWEwNGUtYmEwZi00ZTFlZjU2NTQ4NWUiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDphNzU3MjcyOC00Y2MwLTExZTYtYmY2NS1hMDg3YTU2NzM5MWYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAA/ACwAAAAAEwAXAAAGnsCfcEgUCgBIZHEp7Pl60B5z6Zxaq1amMxAQZIkE7kFy+C4Fvoi5CAisiQaPOSJdjBdmAcjnMxGsChtvQikJMzdWBQ8KQgQOfA0uTAU2PjIWKgZ8fCcFSzqbDiybmyFLOaSpmztCOEIrF6oVCaQtCBB/PwMwpDUDt5s0PD4iQwMafB1DFCOpKEQDGA9FHC+kMWsMpAxmHxMNGQYlJD9BADs="
    }
    , function(t) {
        t.exports = "data:image/gif;base64,R0lGODlhFAAOAMQZAP/S0rqNjefn5/9mZvoKCv84ONdSUsCTk/93d8hxceu+vv8ODv/c3P/19baVlf+mprS0tP9jY+0mJv9YWP/29rOzs/8AAP///6urq////wAAAAAAAAAAAAAAAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMwMTQgNzkuMTU2Nzk3LCAyMDE0LzA4LzIwLTA5OjUzOjAyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAyMTU3ODg5LTIzN2QtMTQ0MC1hYjFjLTI4YmY0YmFlZjk2MiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo1NkZGRUU5NTRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1NkZGRUU5NDRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNCAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpmMGNjNWYyNS1lNWU1LWEwNGUtYmEwZi00ZTFlZjU2NTQ4NWUiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDphNzU3MjcyOC00Y2MwLTExZTYtYmY2NS1hMDg3YTU2NzM5MWYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAAZACwAAAAAFAAOAAAFXKBQYWRpYpWQret4vii7wrSclVeO66RNXr+gkDJpzDBAUxJJGlgixxxvCUxYro8bM0h1EK6WBUPIpBok4AKCDKRiAmCAFsmjJ+FXOe2Ft8hdeyQHcRkigRgQCoQhADs="
    }
    , function(t) {
        t.exports = "data:image/gif;base64,R0lGODlhFAAOAMQWANfv15Kqkufn5/b79onQiZeqlySkJGjCaHmpebLgsimpKXTHdOLz4l+nX7S0tDumO064TnfId7OzsxykHP///6urq////wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMwMTQgNzkuMTU2Nzk3LCAyMDE0LzA4LzIwLTA5OjUzOjAyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAyMTU3ODg5LTIzN2QtMTQ0MC1hYjFjLTI4YmY0YmFlZjk2MiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo1NzA3RjE2MjRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1NzA3RjE2MTRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNCAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpmMGNjNWYyNS1lNWU1LWEwNGUtYmEwZi00ZTFlZjU2NTQ4NWUiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDphNzU3MjcyOC00Y2MwLTExZTYtYmY2NS1hMDg3YTU2NzM5MWYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAAWACwAAAAAFAAOAAAFXKAgVWRpVpJgret4vii7wrRslVSO66RNUr9gMDApAm4VoEmZrBCNyByPCXxOjsIm1Vm8RnHNYRcb1uIajy6EsKPqCoaugkF7IboJi6tOkiwmESsifBUOAgMHAyshADs="
    }
    , function(t) {
        t.exports = "data:image/gif;base64,R0lGODlhEQALANU0AP/b2//19f8kJP8tLf8dHf/S0v8DA/+mpv/m5v/q6v+0tP+urv8PD//i4v8GBv8qKv/w8P9eXv/n5/8ZGf8LC/+Wlv/9/f8fH/8XF//c3P9RUf/6+v9fX/8CAv85Of+/v/8lJf8SEv/s7P/Dw/+Rkf/Jyf/7+/82Nv96ev89Pf/U1P+3t/87O//p6f8HB/9AQP83N/+7u/8REf8AAP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMwMTQgNzkuMTU2Nzk3LCAyMDE0LzA4LzIwLTA5OjUzOjAyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAyMTU3ODg5LTIzN2QtMTQ0MC1hYjFjLTI4YmY0YmFlZjk2MiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo1NzcxMzEwMzRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1NzcxMzEwMjRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNCAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpmMGNjNWYyNS1lNWU1LWEwNGUtYmEwZi00ZTFlZjU2NTQ4NWUiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDphNzU3MjcyOC00Y2MwLTExZTYtYmY2NS1hMDg3YTU2NzM5MWYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAA0ACwAAAAAEQALAAAGaECa0CQsFhEkVqYIeBSMicqrM5s9aQnBbAIQWjjVcIkWgIUJjaImPBuRU2wBgqZysVdCyIk9kHjCBjMKRSIDbARsCigHRi0ggGERQhs0MZYxBxgzIWEyC5cfbKKjMwwyp6ioFA6pMhdBADs="
    }
    , function(t) {
        t.exports = "data:image/gif;base64,R0lGODlhEQALANU5ACeoJx+lH/r9+lW7VaTbpOb15vP685PUk0a1RvL68li8WCmpKbDfsPv9+/v+++z47DOtM024TUKzQqLaorzkvMrqyl2+XcDmwES0RNLt0uP04/b79uX15cboxiqqKjWuNSGmIVe8V3LHclq9WvH58dfv17XhtX7LfjivOOLz4qXbpbLgsiClIDuwO2LAYrHgsS+rLzauNtzx3O747q3erc7rzsLnwiuqKxykHP///wAAAAAAAAAAAAAAAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMwMTQgNzkuMTU2Nzk3LCAyMDE0LzA4LzIwLTA5OjUzOjAyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAyMTU3ODg5LTIzN2QtMTQ0MC1hYjFjLTI4YmY0YmFlZjk2MiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo1NzcxMzEwQjRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1NzcxMzEwQTRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNCAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpmMGNjNWYyNS1lNWU1LWEwNGUtYmEwZi00ZTFlZjU2NTQ4NWUiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDphNzU3MjcyOC00Y2MwLTExZTYtYmY2NS1hMDg3YTU2NzM5MWYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAA5ACwAAAAAEQALAAAGaEDUbUgcAkDF4QLHbDqfuI5taorhPE3AajrNeUmIJqvp8ja8OUPECXG+DgTvZuBscUbNAI6WE1icHyk5GgBODH0hTTAyaCJOFF4JGDg3JWgnTxdoBRI1aDkzKgp6OBWfDp+fDxMDGTlBADs="
    }
    , function(t) {
        t.exports = "data:image/gif;base64,R0lGODlhCwAOAMQZAP83N/9VVf/5+f8VFf+oqP9/f/9sbP+5uf8DA//29v9jY//X1/8MDP+qqv9vb/8jI/+3t//h4f/i4v8UFP+Cgv86Ov9paf+lpf8AAP///wAAAAAAAAAAAAAAAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMwMTQgNzkuMTU2Nzk3LCAyMDE0LzA4LzIwLTA5OjUzOjAyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAyMTU3ODg5LTIzN2QtMTQ0MC1hYjFjLTI4YmY0YmFlZjk2MiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo1NzEyNkVGNTRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1NzEyNkVGNDRFNDExMUU2QjMwM0JBMzZFQThFMzAxRiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNCAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpmMGNjNWYyNS1lNWU1LWEwNGUtYmEwZi00ZTFlZjU2NTQ4NWUiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDphNzU3MjcyOC00Y2MwLTExZTYtYmY2NS1hMDg3YTU2NzM5MWYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAAZACwAAAAACwAOAAAFO2AmZhA1nlk0IQeaCQCGDRJqyHIljASOO6IFw4drZAqBGO4RUCREF58FFcVNT1XZdZTFbKFSahg7zoQAADs="
    }
    , function(t) {
        t.exports = "data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAOCAYAAAAWo42rAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA4RpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDE0IDc5LjE1Njc5NywgMjAxNC8wOC8yMC0wOTo1MzowMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDowMjE1Nzg4OS0yMzdkLTE0NDAtYWIxYy0yOGJmNGJhZWY5NjIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NTcxMjZFRkQ0RTQxMTFFNkIzMDNCQTM2RUE4RTMwMUYiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NTcxMjZFRkM0RTQxMTFFNkIzMDNCQTM2RUE4RTMwMUYiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKFdpbmRvd3MpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6ZjBjYzVmMjUtZTVlNS1hMDRlLWJhMGYtNGUxZWY1NjU0ODVlIiBzdFJlZjpkb2N1bWVudElEPSJhZG9iZTpkb2NpZDpwaG90b3Nob3A6YTc1NzI3MjgtNGNjMC0xMWU2LWJmNjUtYTA4N2E1NjczOTFmIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+aIPXGAAAAIZJREFUeNpilFkiwwAF04A4kwEVRAPxMhCDiYFIMKwUsgDxXCDmBWIjLPJ5QBwAxNdBCncA8SocBpkDsTYQ14OsXg3Ek/DYmgoyEebGUiA+jkXRFCBegeyZX0AcDsSvkRSdBOJibL5+DMSxQPwXiN8AcRjUALivkcFOIG4E4lNA/AhZAiDAAK1EF3obl0raAAAAAElFTkSuQmCC"
    }
    , function(t, i, o) {
        var e = o(99)
            , n = o(7)
            , s = o(63)
            , a = o(68)
            , r = o(60)
            , p = (o(25),
            o(21))
            , h = o(100)
            , l = o(22)
            , c = o(101)
            , d = function() {
            function t(t) {
                this.defaultoptions = n.draw_xy_web,
                    this.options = {},
                    this.options = p(this.defaultTheme, this.options, this.defaultoptions, t),
                    this.container = document.getElementById(t.container),
                    this.container.className = this.container.className.replace(/emcharts-container/g, "").trim(),
                    this.container.className = this.container.className + " emcharts-container",
                    this.onChartLoaded = void 0 == t.onChartLoaded ? function() {}
                        : t.onChartLoaded
            }
            function i(t) {
                this.options.context;
                this.options.data = void 0 == t ? this.options.data : t;
                var i = (t.high,
                    t.low,
                    t.yc,
                    this.options.interactive);
                try {
                    this.options.pricedigit = t.pricedigit;
                    var o = r.get_rect.apply(this, [this.options.context.canvas, this.options.data.total]);
                    this.options.rect_unit = o,
                        new e(this.options),
                        m.call(this),
                        u.call(this),
                    "r" === this.options.type && f.call(this),
                        i.hideLoading();
                    var n;
                    try {
                        n = t.data[0].price
                    } catch (s) {
                        n = 0
                    }
                    var a = r.get_y.call(this, n);
                    i.showTipsTime(this.options.padding.left, a, t.data, t.data.length - 1, t),
                        this.onChartLoaded(this),
                        l.apply(this, [this.options.context, 95 + this.options.padding.right, 10, 82, 20])
                } catch (p) {
                    i.hideLoading()
                }
                return !0
            }
            function o(t) {
                var i = this
                    , o = t.canvas
                    , e = this.options.interactive;
                e.allData = this.options;
                var n = this.container;
                r.addEvent.call(i, o, "touchmove", function(t) {
                    d.apply(i, [e, t.changedTouches[0]]);
                    try {
                        t.preventDefault()
                    } catch (o) {
                        t.returnValue = !1
                    }
                }),
                    r.addEvent.call(i, o, "mousemove", function(t) {
                        d.apply(i, [e, t]);
                        try {
                            t.preventDefault()
                        } catch (o) {
                            t.returnValue = !1
                        }
                    }),
                    r.addEvent.call(i, n, "mouseleave", function(t) {
                        e.hide();
                        try {
                            t.preventDefault()
                        } catch (i) {
                            t.returnValue = !1
                        }
                    }),
                    r.addEvent.call(i, o, "mouseenter", function(t) {
                        d.apply(i, [e, t]);
                        try {
                            t.preventDefault()
                        } catch (o) {
                            t.returnValue = !1
                        }
                    })
            }
            function d(t, i) {
                if (!(!this.options.data.data.length > 0)) {
                    t.show();
                    var o = this.options.canvas
                        , e = this.options.data.data
                        , n = this.options.rect_unit
                        , s = n.rect_w
                        , a = i.clientX - this.container.getBoundingClientRect().left
                        , p = i.clientY - this.container.getBoundingClientRect().top
                        , h = r.windowToCanvas.apply(this, [o, a, p])
                        , l = (1 * h.x).toFixed(0)
                        , c = Math.floor((l - this.options.padding.left) / s);
                    if (e[c]) {
                        var d = r.canvasToWindow.apply(this, [o, e[c].cross_x, e[c].cross_y])
                            , u = d.x
                            , m = d.y;
                        t.crossTime(o, u, m),
                            t.showTipsTime(u, m, e, c, t.allData.data)
                    }
                }
            }
            function u() {
                function t(t, i) {
                    t.beginPath(),
                        t.strokeStyle = "#59A7FF";
                    for (var o, e = 0; o = i[e]; e++) {
                        var n = r.get_x.call(this, e + 1)
                            , s = r.get_y.call(this, o.price);
                        0 === e ? t.moveTo(n, s) : t.lineTo(n, s),
                            o.cross_x = n,
                            o.cross_y = s
                    }
                    t.stroke()
                }
                function i(t, i) {
                    var o = r.get_y.call(this, this.options.data.min)
                        , e = t.createLinearGradient(0, 0, 0, t.canvas.height);
                    e.addColorStop(0, "rgba(200,234,250,0.7)"),
                        e.addColorStop(1, "rgba(255,255,255,0)");
                    var n = i.length;
                    t.beginPath(),
                        t.fillStyle = e,
                        t.moveTo(this.options.padding.left, o);
                    for (var s, a = 0; s = i[a]; a++) {
                        var p = r.get_x.call(this, a + 1)
                            , h = r.get_y.call(this, s.price);
                        a === n - 1 ? (t.lineTo(p, h),
                            t.lineTo(p, o)) : t.lineTo(p, h)
                    }
                    t.fill()
                }
                var o = this.options.context
                    , e = this.options.data
                    , n = e.data;
                t.apply(this, [o, n]),
                    i.apply(this, [o, n])
            }
            function m() {
                var t = this.options.context
                    , i = this.options.data;
                t.save(),
                    this.options.avg_cost_color = n.draw_line.avg_cost_color;
                var o = this.options.avg_cost_color
                    , e = i.data;
                t.beginPath(),
                    t.lineWidth = 1,
                    t.strokeStyle = o,
                    t.fillStyle = "";
                for (var s = 0; s < e.length; s++) {
                    var a = r.get_x.call(this, s + 1)
                        , p = r.get_y.call(this, e[s].avg_cost);
                    0 == s ? t.moveTo(a, p) : t.lineTo(a, p)
                }
                t.stroke(),
                    t.restore()
            }
            function f() {
                var t = this;
                a(this.options.code, function(i, o) {
                    if (i)
                        ;
                    else {
                        if ("false" === o[0].state)
                            return;
                        c.call(t, o)
                    }
                })
            }
            return t.prototype.init = function() {
                this.options.chartType = "TL";
                var t = document.createElement("canvas");
                this.container.style.position = "relative",
                    this.container.appendChild(t);
                try {
                    var i = t.getContext("2d")
                } catch (o) {
                    t = window.G_vmlCanvasManager.initElement(t);
                    var i = t.getContext("2d")
                }
                this.options.canvas = t,
                    this.options.context = i;
                var e = this.options.dpr;
                t.width = this.options.width * e,
                    t.height = this.options.height * e,
                    this.options.y_sepe_num = 13,
                    this.options.canvas_offset_top = t.height / this.options.y_sepe_num,
                    this.options.k_v_away = t.height / this.options.y_sepe_num,
                    this.options.scale_count = 0,
                    this.options.c_k_height = this.options.c_1_height = 11 * t.height / this.options.y_sepe_num,
                    this.options.c_v_height = 3 * t.height / this.options.y_sepe_num,
                    this.options.unit_height = 1 * t.height / this.options.y_sepe_num,
                    this.options.c1_y_top = 1 * t.height / this.options.y_sepe_num,
                    this.options.c2_y_top = 12 * t.height / this.options.y_sepe_num,
                    t.style.width = this.options.width + "px",
                    t.style.height = this.options.height + "px",
                    t.style.border = "0",
                    i.translate("0", Math.round(this.options.canvas_offset_top)),
                    this.options.color = {},
                    this.options.color.strokeStyle = "rgba(230,230,230, 1)",
                    this.options.color.fillStyle = "#717171",
                    i.fillStyle = this.options.color.fillStyle,
                    i.font = "12px Arial",
                    i.lineWidth = 1 * this.options.dpr,
                    i.strokeStyle = "rgba(230,230,230, 1)";
                var n = this.options.YMaxNum || "10000.00";
                this.options.padding = {},
                    this.options.padding.left = i.measureText(n).width + 5,
                    this.options.padding.right = i.measureText("+10.00%").width,
                    this.options.padding.top = 0,
                    this.options.padding.bottom = 0
            }
                ,
                t.prototype.draw = function() {
                    this.clear(),
                        this.init();
                    var t = this.options.interactive = new h(this.options);
                    t.showLoading();
                    var e = this
                        , n = this.options.code.charAt(this.options.code.length - 1);
                    (7 == n || 5 == n) && (this.options.isCR = !1);
                    var a = {
                        code: this.options.code,
                        type: this.options.type,
                        isCR: !!this.options.isCR,
                        host: this.options.host,
                        inter: t,
                        container: this.container,
                        width: this.options.width,
                        height: this.options.height
                    };
                    try {
                        s(a, function(n, s) {
                            n ? (t.showNoData(),
                                t.hideLoading()) : (s ? i.apply(e, [s]) : i.apply(e, [[]]),
                                o.call(e, e.options.context))
                        })
                    } catch (r) {
                        t.showNoData(),
                            t.hideLoading()
                    }
                }
                ,
                t.prototype.intervalDraw = function() {
                    function t() {
                        var t = this
                            , i = t.options.data.max
                            , n = t.options.data.min
                            , a = t.options.data.v_max
                            , r = {
                            code: t.options.code,
                            type: t.options.type,
                            isCR: !!t.options.isCR
                        };
                        s(r, function(s, r) {
                            if (s)
                                t.options.interactive.showNoData();
                            else {
                                h = r.data.length - 2;
                                var p = r.max
                                    , l = r.min
                                    , c = r.v_max;
                                t.options.data = r,
                                    p > i || n > l || a > c ? o.call(t, r) : e.call(t, r),
                                    h = t.options.data.data.length - 1
                            }
                        })
                    }
                    function o(t) {
                        this.clear(),
                            i.apply(this, [t])
                    }
                    function e(t) {
                        var i = this.options.context;
                        n.call(this, i, t, h),
                            a.call(this, i, t, h),
                            p.call(this, i, t, h)
                    }
                    function n(t, i, o) {
                        var e = r.get_x.call(this, o + 1)
                            , n = r.get_y.call(this, i.data[o].avg_cost)
                            , s = r.get_x.call(this, i.data.length)
                            , a = r.get_y.call(this, i.data[i.data.length - 1].avg_cost);
                        t.save(),
                            t.strokeStyle = "#F1CA15",
                            t.beginPath(),
                            t.moveTo(e, n),
                            t.lineTo(s, a),
                            t.stroke(),
                            t.restore()
                    }
                    function a(t, i, o) {
                        var e = r.get_y.call(this, this.options.data.min)
                            , n = r.get_x.call(this, o + 1)
                            , s = r.get_y.call(this, i.data[o].price)
                            , a = r.get_x.call(this, i.data.length)
                            , p = r.get_y.call(this, i.data[i.data.length - 1].price);
                        t.save(),
                            t.strokeStyle = "#639EEA",
                            t.beginPath(),
                            t.moveTo(n, s),
                            t.lineTo(a, p),
                            t.stroke();
                        var h = t.createLinearGradient(0, 0, 0, t.canvas.height);
                        h.addColorStop(0, "rgba(200,234,250,0.7)"),
                            h.addColorStop(1, "rgba(255,255,255,0)"),
                            t.beginPath(),
                            t.fillStyle = h,
                            t.beginPath(),
                            t.moveTo(n, s),
                            t.lineTo(a, p),
                            t.lineTo(a, e),
                            t.lineTo(n, e),
                            t.lineTo(n, s),
                            t.fill(),
                            t.restore()
                    }
                    function p(t, i) {
                        var o = i.data.length
                            , e = t.canvas.height / 4
                            , n = .9 * e
                            , s = r.get_x.call(this, o)
                            , a = e * i.data[o - 1].volume / i.v_max
                            , p = n - a
                            , h = this.options.rect_unit.bar_w;
                        t.save(),
                            t.fillStyle = this.options.avg_cost_color,
                            t.rect(s - h / 2, p, h, a),
                            t.restore()
                    }
                    var h, l = (new Date).getMinutes(), c = this.options.intervalTimer, d = this;
                    c && clearInterval(c),
                        this.options.intervalTimer = setInterval(function() {
                            var i = new Date
                                , o = i.getMinutes()
                                , e = !0;
                            i.getHours() >= 15 && i.getMinutes() >= 0 && (clearInterval(d.options.intervalTimer),
                                e = !1),
                            i.getHours() <= 9 && i.getMinutes() < 30 && d.options.isCR === !1 && (clearInterval(d.options.intervalTimer),
                                e = !1),
                            i.getHours() <= 9 && i.getMinutes() < 15 && d.options.isCR === !0 && (clearInterval(d.options.intervalTimer),
                                e = !1),
                            o !== l && e && (l = o,
                                t.call(d))
                        }, 1e4)
                }
                ,
                t.prototype.reDraw = function() {
                    this.clear(),
                        this.init(),
                        i.call(this)
                }
                ,
                t.prototype.clear = function() {
                    this.container.innerHTML = ""
                }
                ,
                t
        }();
        t.exports = d
    }
    , function(t, i, o) {
        var e = o(25)
            , n = o(21)
            , s = o(60)
            , a = o(7)
            , r = o(9)
            , p = function() {
            function t(t) {
                this.defaultoptions = a.draw_xy_web,
                    this.options = {},
                    this.options = n(this.options, this.defaultoptions, t),
                    this.draw()
            }
            function i(t, i, n, a, p) {
                for (var h, l = this, c = a.length, d = this.options.padding.left, u = this.options.padding.right, m = 0; h = a[m]; m++) {
                    if (t.beginPath(),
                        t.lineWidth = 1,
                        (c - 1) / 2 > m ? (0 == m ? (t.strokeStyle = "#e1e1e1",
                            t.moveTo(r(d), r(h.y)),
                            t.lineTo(r(t.canvas.width - u), r(h.y))) : (t.strokeStyle = "#eeeeee",
                            e(t, d, h.y, t.canvas.width - u + 4, h.y, 5)),
                            t.fillStyle = "#007F24") : m > (c - 1) / 2 ? (m == c - 1 ? (t.strokeStyle = "#e1e1e1",
                            t.moveTo(r(d), r(h.y)),
                            t.lineTo(r(t.canvas.width - u), r(h.y))) : (t.strokeStyle = "#eeeeee",
                            e(t, d, h.y, t.canvas.width - u + 4, h.y, 5)),
                            t.fillStyle = "#FF0A16") : (t.fillStyle = "#333333",
                            t.strokeStyle = "#cadef8",
                            t.moveTo(r(d), r(h.y)),
                            t.lineTo(r(t.canvas.width - u), r(h.y))),
                        isNaN(h.num))
                        t.fillText("0.00", 0, h.y);
                    else {
                        var f = (1 * h.num).toFixed(this.options.pricedigit)
                            , v = s.formatYNum(f / 1, p);
                        0 == n && 0 == i && (v = "-"),
                            t.fillText(v, d - t.measureText(v).width - 5, h.y)
                    }
                    t.stroke(),
                        o.call(l, t, i, n, h)
                }
            }
            function o(t, i, o, e) {
                var n = this.options.padding.right
                    , s = (i + o) / 2
                    , a = t.canvas.width;
                if (s) {
                    var r = ((e.num - s) / s * 100).toFixed(2) + "%";
                    e.num - s > 0 && (r = "+" + r)
                } else
                    var r = "0.00%";
                0 == i && 0 == o && (r = "-"),
                    t.fillText(r, a - n + 5, e.y),
                    t.stroke()
            }
            function p(t, i, o) {
                var n = this.options.padding.left
                    , a = this.options.padding.right
                    , p = this.options.c_1_height;
                t.beginPath(),
                    t.fillStyle = this.options.color.fillStyle;
                var h, l = t.canvas.width, c = 11.1 * this.options.unit_height, d = o.length, u = 2 * d, m = !1, f = this.options.isCR || !1, v = 0;
                "r" != this.options.type.toLowerCase() ? h = (l - n - a) / d : (f ? (v = (l - n - a) / (4 * (d - 2) + 1),
                    h = 4 * v) : h = (l - n - a) / (d - 1),
                    m = !0),
                    t.save(),
                    t.fillStyle = this.options.color.fillStyle,
                    t.font = "12px Arial,Helvetica,San-serif",
                    t.textBaseline = "top";
                for (var g = 0; d > g; g++) {
                    var y = o[g];
                    g === d - 1 && m ? t.fillText(y.value, s.get_x.call(this, y.index) - t.measureText(y.value).width, c) : 0 === g && m ? t.fillText(y.value, s.get_x.call(this, y.index), c) : 1 === g && f && m ? t.fillText(y.value, s.get_x.call(this, y.index), c) : t.fillText(y.value, s.get_x.call(this, y.index) - t.measureText(y.value).width / 2 + 2, c)
                }
                t.restore();
                var A = (this.options.c_v_height,
                t.canvas.height - this.options.canvas_offset_top)
                    , x = this.options.c2_y_top
                    , w = (l - n - a) / u;
                t.save(),
                    t.lineWidth = 1;
                for (var g = 0; u >= g; g++)
                    if (t.beginPath(),
                    0 != g && g != u) {
                        var _;
                        f ? 1 == g ? (_ = v + n,
                            t.fillStyle = "#efefef",
                            t.fillRect(n, 0, v, p)) : _ = n + (g - 1) * h / 2 + v : _ = n + g * w,
                            t.strokeStyle = "#eeeeee",
                            e(t, _, p, _, 0, 5),
                            e(t, _, A, _, x - 10, 5)
                    } else
                        t.strokeStyle = "#e1e1e1",
                            t.moveTo(r(n + g * w), r(p)),
                            t.lineTo(r(n + g * w), r(0)),
                            t.stroke();
                t.restore()
            }
            function h(t, i, o, e) {
                for (var n = (t - i) / (o - 1), s = [], a = 0; o > a; a++)
                    s.push({
                        num: 1 * i + a * n,
                        x: 0,
                        y: e - a / (o - 1) * e
                    });
                return s
            }
            return t.prototype.draw = function() {
                var t = this.options.data
                    , o = this.options.context
                    , e = t.max
                    , n = t.min
                    , s = this.options.sepe_num
                    , a = t.timeStrs
                    , r = t.pricedigit
                    , l = this.options.c_1_height
                    , c = h(e, n, s, l);
                i.call(this, o, e, n, c, r),
                    p.apply(this, [o, l, a])
            }
                ,
                t
        }();
        t.exports = p
    }
    , function(t, i, o) {
        var e = o(21)
            , n = (o(60),
            o(7))
            , s = o(70)
            , a = function() {
            function t(t) {
                this.defaultoptions = n.interactive,
                    this.options = {},
                    this.options = e(this.options, this.defaultoptions, t)
            }
            return t.prototype.crossTime = function(t, i, e) {
                var n = this.options.canvas_offset_top
                    , s = this.options.canvas.height
                    , a = this.options.container
                    , r = this.options.padding.left
                    , p = this.options.padding.right;
                if (!this.options.cross) {
                    this.options.cross = {};
                    var h = document.createElement("div");
                    h.className = "cross-y",
                        h.style.height = s - n - n + "px",
                        h.style.top = n + "px",
                        this.options.cross.y_line = h;
                    var l = document.createElement("div");
                    l.className = "cross-x",
                        l.style.width = t.width - r - p + "px",
                        l.style.left = r + "px",
                        this.options.cross.x_line = l;
                    var c = document.createElement("div");
                    c.className = "cross-p",
                        c.style.width = "11px",
                        c.style.height = "11px",
                        this.options.point_width = 11,
                        c.style.borderRadius = c.style.width,
                        c.style.background = "url(" + o(71) + ")",
                        this.options.cross.point = c;
                    var d = document.createDocumentFragment();
                    d.appendChild(h),
                        d.appendChild(l),
                        d.appendChild(c),
                        document.getElementById(a).appendChild(d)
                }
                var h = this.options.cross.y_line;
                this.options.cross.y_line && (h.style.left = i + "px");
                var l = this.options.cross.x_line;
                this.options.cross.x_line && (l.style.top = e + "px");
                var c = this.options.cross.point;
                if (c) {
                    var u = this.options.point_width;
                    c.style.left = i - u / 2 + "px",
                        c.style.top = e - u / 2 + "px"
                }
            }
                ,
                t.prototype.showTipsTime = function(t, i, o, e, n) {
                    var a, r = this.options.canvas, p = this.options.context, h = this.options.padding.left, l = this.options.padding.right, c = this.options.canvas_offset_top, d = this.options.c_1_height, u = this.options.container, m = this.options.code;
                    a = 0 > e ? o[0] || {} : e > o.length - 1 ? o[o.length - 1] : o[e],
                        this.options.lastItemData = o[o.length - 1];
                    var f, v;
                    7 != m.charAt(m.length - 1) && 5 != m.charAt(m.length - 1) ? (v = s(a.volume, "手"),
                        f = s(a.volume * a.price * 100, "元")) : (v = s(a.volume, "股"),
                        f = s(a.volume * a.price, "元")),
                        this.hoverData = a;
                    var g = ["#ff0000", "#666666", "#17b03e"]
                        , y = n.yc
                        , A = (y.split(".")[1] || "").length || 3
                        , x = a.price
                        , w = ((x - y) / y * 100).toFixed(A);
                    "NaN" == w && (w = "0");
                    var _;
                    _ = x - y > 0 ? g[0] : x - y == 0 ? g[1] : g[2];
                    var b = n.name + "[" + n.code + "] " + (a.dateTime || "-") + " " + (a.time || "-")
                        , T = " 价格:<span style='color: " + (_ || "-") + ";'>" + (x || "-") + "</span> ";
                    T += " 涨幅:<span style='color: " + (_ || "-") + ";'>" + w + "%</span> ",
                        T += " 成交量:" + v;
                    var M = document.createElement("div");
                    M.innerHTML = T;
                    var k = this.options.width - h - l;
                    if (k - p.measureText(b).width - p.measureText(M.innerText).width > 0 && (b += T),
                        this.options.webTimeTips) {
                        var C = this.options.webTimeTips.time_y_left
                            , N = this.options.webTimeTips.time_y_right
                            , E = this.options.webTimeTips.time_x_bottom
                            , S = this.options.webTimeTips.time_x_top;
                        C.style.top = i + "px",
                            C.innerHTML = a.price,
                            C.style.left = h - C.clientWidth + "px",
                            C.style.display = "block",
                            N.style.top = i + "px",
                            N.style.display = "block",
                            N.innerHTML = "0.00" === a.percent ? a.percent + "%" : 0 == a.percent ? a.percent + "%" : (a.up ? "+" : "-") + a.percent + "%",
                            E.style.left = t + "px",
                            E.style.display = "block",
                            E.innerHTML = a.time,
                            E.style.left = t < h + E.clientWidth / 2 ? h + "px" : t > r.width - l - E.clientWidth / 2 ? r.width - l - E.clientWidth + "px" : t - E.clientWidth / 2 + "px",
                            E.style.top = d + c - E.clientHeight + "px",
                            S.style.display = "block",
                            S.innerHTML = b
                    } else {
                        this.options.time_data = o;
                        var C, N, E, S, D = document.createDocumentFragment();
                        C = document.createElement("div"),
                            C.setAttribute("id", "time_y_left"),
                            C.className = "time-tips-coordinate",
                            N = document.createElement("div"),
                            N.setAttribute("id", "time_y_right"),
                            N.className = "time-tips-coordinate",
                            N.style.left = r.width - l + "px",
                            E = document.createElement("div"),
                            E.setAttribute("id", "time_x_bottom"),
                            E.className = "time-tips-coordinate",
                            S = document.createElement("div"),
                            S.setAttribute("id", "time_x_top"),
                            S.className = "time-tips-top",
                            S.style.top = c - 18 + "px",
                            S.style.left = h + "px",
                            this.options.webTimeTips = {},
                            this.options.webTimeTips.time_y_left = C,
                            this.options.webTimeTips.time_y_right = N,
                            this.options.webTimeTips.time_x_top = S,
                            this.options.webTimeTips.time_x_bottom = E,
                            D.appendChild(C),
                            D.appendChild(N),
                            D.appendChild(E),
                            D.appendChild(S),
                            document.getElementById(u).appendChild(D),
                            C.style.display = "block",
                            C.innerHTML = a.price,
                            C.style.left = h - C.clientWidth + "px",
                            C.style.top = i + "px",
                            C.style.display = "none",
                            N.style.top = i + "px",
                            N.innerHTML = (a.up ? "+" : "-") + a.percent + "%",
                            E.style.display = "block",
                            E.innerHTML = a.time,
                            E.style.left = t + "px",
                            E.style.top = d + c - E.clientHeight + "px",
                            E.style.display = "none",
                            S.innerHTML = b,
                            S.style.display = "block"
                    }
                }
                ,
                t.prototype.show = function() {
                    if (this.options.cross) {
                        var t = this.options.cross.x_line;
                        t && (t.style.display = "block");
                        var i = this.options.cross.y_line;
                        i && (i.style.display = "block");
                        var o = this.options.cross.point;
                        o && (o.style.display = "block")
                    }
                    if (this.options.webTimeTips) {
                        var e = this.options.webTimeTips.time_y_left;
                        e && (e.style.display = "block");
                        var n = this.options.webTimeTips.time_y_right;
                        n && (n.style.display = "block");
                        var s = this.options.webTimeTips.time_x_bottom;
                        s && (s.style.display = "block")
                    }
                    this.showTipsNew()
                }
                ,
                t.prototype.showTipsNew = function() {
                    var t, i, o = (this.options.canvas,
                        this.options.context), e = this.options.padding.left, n = this.options.padding.right, a = this.options.code, r = this.hoverData, p = this.options.webTimeTips.time_x_top;
                    7 != a.charAt(a.length - 1) && 5 != a.charAt(a.length - 1) ? (i = s(r.volume, "手"),
                        t = s(r.volume * r.price * 100, "元")) : (i = s(r.volume, "股"),
                        t = s(r.volume * r.price, "元"));
                    var h = this.allData.data
                        , l = ["#ff0000", "#666666", "#17b03e"]
                        , c = h.yc
                        , d = (c.split(".")[1] || "").length || 3
                        , u = r.price
                        , m = ((u - c) / c * 100).toFixed(d);
                    "NaN" == m && (m = "0");
                    var f;
                    f = u - c > 0 ? l[0] : u - c == 0 ? l[1] : l[2];
                    var v = h.name + "[" + h.code + "] " + r.dateTime + " " + r.time
                        , g = " 价格:<span style='color: " + f + ";'>" + u + "</span> ";
                    g += " 涨幅:<span style='color: " + f + ";'>" + m + "%</span> ",
                        g += " 成交量:" + i;
                    var y = document.createElement("div");
                    y.innerHTML = g;
                    var A = this.options.width - e - n;
                    A - o.measureText(v).width - o.measureText(y.innerText).width > 0 && (v += g),
                        p.innerHTML = v
                }
                ,
                t.prototype.hide = function() {
                    if (this.options.cross) {
                        var t = this.options.cross.x_line;
                        t && (t.style.display = "none");
                        var i = this.options.cross.y_line;
                        i && (i.style.display = "none");
                        var o = this.options.cross.point;
                        o && (o.style.display = "none")
                    }
                    if (this.options.webTimeTips) {
                        var e = (this.options.canvas,
                            this.options.context)
                            , n = this.options.padding.left
                            , a = this.options.padding.right
                            , r = this.options.webTimeTips.time_y_left;
                        r && (r.style.display = "none");
                        var p = this.options.webTimeTips.time_y_right;
                        p && (p.style.display = "none");
                        var h = this.options.webTimeTips.time_x_bottom;
                        h && (h.style.display = "none");
                        var l = this.options.code
                            , c = this.options.lastItemData;
                        if (!c)
                            return;
                        var d, u, m = this.options.webTimeTips.time_x_top;
                        7 != l.charAt(l.length - 1) && 5 != l.charAt(l.length - 1) ? (u = s(c.volume, "手"),
                            d = s(c.volume * c.price * 100, "元")) : (u = s(c.volume, "股"),
                            d = s(c.volume * c.price, "元"));
                        var f = this.allData.data
                            , v = ["#ff0000", "#666666", "#17b03e"]
                            , g = f.yc
                            , y = (g.split(".")[1] || "").length || 3
                            , A = c.price
                            , x = ((A - g) / g * 100).toFixed(y);
                        "NaN" == x && (x = "0");
                        var w;
                        w = A - g > 0 ? v[0] : A - g == 0 ? v[1] : v[2];
                        var _ = f.name + "[" + f.code + "] " + c.dateTime + " " + c.time
                            , b = " 价格:<span style='color: " + w + ";'>" + A + "</span> ";
                        b += " 涨幅:<span style='color: " + w + ";'>" + x + "%</span> ",
                            b += " 成交量:" + u;
                        var T = document.createElement("div");
                        T.innerHTML = b;
                        var M = this.options.width - n - a;
                        M - e.measureText(_).width - e.measureText(T.innerText).width > 0 && (_ += b),
                            m.innerHTML = _
                    }
                }
                ,
                t.prototype.showLoading = function() {
                    if (this.options.loading)
                        this.options.loading.style.display = "block";
                    else {
                        var t = document.getElementById(this.options.container)
                            , i = document.createElement("div");
                        i.className = "loading-chart",
                            i.innerText = "加载中...",
                            i.style.height = this.options.height - 100 + "px",
                            i.style.width = this.options.width + "px",
                            i.style.paddingTop = "100px",
                            this.options.loading = i,
                            t.appendChild(i)
                    }
                }
                ,
                t.prototype.hideLoading = function() {
                    this.options.loading.style.display = "none"
                }
                ,
                t.prototype.showNoData = function() {
                    if (this.options.noData)
                        this.options.noData.style.display = "block";
                    else {
                        var t = document.getElementById(this.options.container)
                            , i = document.createElement("div");
                        i.className = "loading-chart",
                            i.innerText = "暂无数据",
                            i.style.height = this.options.height - 100 + "px",
                            i.style.width = this.options.width + "px",
                            i.style.paddingTop = "100px",
                            this.options.noData = i,
                            t.appendChild(i)
                    }
                }
                ,
                t
        }();
        t.exports = a
    }
    , function(t, i, o) {
        function e(t) {
            for (var i = !0, o = [], e = this, s = e.options.data.data, h = s.length - 1, l = 0; l <= t.length - 1; l++)
                for (var c = t[l].split(","), d = {
                    changeTime: c[1],
                    changeName: c[2],
                    changeType: c[3],
                    changeInfo: c[4],
                    isProfit: c[5]
                }, u = c[1], m = a(c[3]), f = e.options.canvas_offset_top + e.options.c_1_height - 40, v = p.join(",").indexOf(c[3]) >= 0 ? "red" : "green"; h >= 0; h--)
                    if (u == e.options.data.data[h].time) {
                        for (var g = e.options.data.data[h].price, y = e.options.data.data[h].up, A = e.options.data.data[h].percent, x = 0, l = 0; l < o.length; l++)
                            o[l] == u && x++;
                        n(e.container, r.get_x.call(e, h + 1), f - 4 * x, m, d, g, y, A, v, i),
                            i = !1,
                            o.push(u);
                        break
                    }
        }
        function n(t, i, o, e, n, a, l, c, d, u) {
            var m = document.createElement("div");
            m.setAttribute("style", "opacity: 0.5;filter:alpha(opacity=50) "),
                m.style.position = "absolute",
                m.style.top = o + 20 + "px",
                m.style.left = i - 2 + "px",
                m.style.width = "6px",
                m.style.height = "6px",
                m.style.borderRadius = "50%",
                m.style.webkitBorderRadius = "50%",
                m.style.msBorderRadius = "50%",
                m.style.transitionDuration = "0.3s",
                m.style.webkitTransitionDuration = "0.3s",
                m.style.msTransitionDuration = "0.3s",
                m.style.backgroundColor = d,
                t.appendChild(m);
            var f = document.createElement("div")
                , v = n.changeType
                , g = n.changeTime
                , y = n.changeInfo;
            t.appendChild(f),
            u && (navigator.appVersion.indexOf("MSIE 9.0") > -1 ? s(m) : (m.setAttribute("class", "timePositionChangesAni"),
                m.setAttribute("id", "lastPositionChangesAni"))),
                r.addEvent(m, "mouseover", function() {
                    try {
                        document.querySelector("#lastPositionChangesAni").setAttribute("class", "")
                    } catch (t) {}
                    this.style.width = "10px",
                        this.style.height = "10px",
                        this.style.marginLeft = "-2px",
                        this.style.marginTop = "-2px",
                        this.style.zIndex = "10",
                        f.className = "timeChangeMainPad";
                    var n = '<div class="timeChangeTriangle" style="z-index:1;background-color:#65CAFE;"></div><table cellpadding="0" cellspacing="0" style="position:relative;z-index:2;min-width:120px"><tr><td style="background-color: #D1E7FF;text-align: center;padding: 6px;"><img src="' + e + '" style="height: 20px;" /><p style="padding: 0;margin: 0;white-space:nowrap;">' + v + '</p></td><td style="padding: 6px;background-color: #ffffff;"><div style="padding: 4px;">' + g + "</div>";
                    p.join(",").indexOf(v) > -1 && (n += '<div style="padding: 4px;white-space:nowrap;color: red;">' + y + "</div>"),
                    h.join(",").indexOf(v) > -1 && (n += '<div style="padding: 4px;white-space:nowrap;color: green;">' + y + "</div>"),
                        n += "</td></tr></table>",
                        f.innerHTML = n,
                        f.style.display = "block",
                        f.style.left = i - f.clientWidth / 2 + 5 + "px",
                        f.style.top = o + 50 + "px"
                }),
                r.addEvent(m, "mouseout", function() {
                    try {
                        document.querySelector("#lastPositionChangesAni").setAttribute("class", "timePositionChangesAni")
                    } catch (t) {}
                    this.style.width = "6px",
                        this.style.height = "6px",
                        this.style.marginLeft = "0px",
                        this.style.marginTop = "0px",
                        this.style.zIndex = "",
                        f.style.display = "none"
                })
        }
        function s(t) {
            var i = 6
                , o = 1
                , e = setInterval(function() {
                (i > 10 || 6 > i) && (o = -o),
                    i += o,
                    t.style.width = i + "px",
                    t.style.height = i + "px",
                    t.style.marginLeft = (6 - i) / 2 + "px",
                    t.style.marginTop = (6 - i) / 2 + "px"
            }, 100);
            setTimeout(function() {
                clearTimeout(e),
                    t.style.width = "6px",
                    t.style.height = "6px",
                    t.style.marginLeft = "0px",
                    t.style.marginTop = "0px"
            }, 3200)
        }
        var a = o(73)
            , r = o(60)
            , p = ["火箭发射", "封涨停板", "机构买单", "快速反弹", "大笔买入", "有大买盘", "向上缺口", "竞价上涨", "高开5日", "60日新高", "打开跌停", "大幅上涨"]
            , h = ["快速下跌", "封跌停板", "机构卖单", "高台跳水", "大笔卖出", "有大卖盘", "向下缺口", "竞价下跌", "低开5日", "60日新低", "打开涨停", "大幅下跌"];
        t.exports = e
    }
    , function(t, i, o) {
        var e = o(103)
            , n = o(7)
            , s = o(61)
            , a = o(62)
            , r = o(104)
            , p = o(106)
            , h = o(108)
            , l = o(110)
            , c = o(111)
            , d = o(112)
            , u = o(113)
            , m = o(114)
            , f = o(115)
            , v = o(116)
            , g = o(117)
            , y = o(118)
            , A = o(119)
            , x = o(120)
            , w = o(121)
            , _ = o(122)
            , b = o(123)
            , T = o(60)
            , M = o(124)
            , k = o(125)
            , C = o(21)
            , N = o(22)
            , E = o(126)
            , S = o(32)
            , D = (o(127),
            o(109))
            , L = function() {
            function t(t) {
                this.defaultoptions = n.chartK,
                    this.options = C(n.defaulttheme, this.defaultoptions, t),
                    this.container = document.getElementById(t.container),
                    this.container.className = this.container.className.replace(/emcharts-container/g, "").trim(),
                    this.container.className = this.container.className + " emcharts-container",
                    this.options.onChartLoaded = void 0 == t.onChartLoaded ? function() {}
                        : t.onChartLoaded,
                window.authorityType || (window.authorityType = S.getCookie("beforeBackRight")),
                    window.authorityType = null == window.authorityType || void 0 == window.authorityType ? "fa" : window.authorityType,
                    this.options.authorityType = "" == window.authorityType ? "不复权" : "fa" == window.authorityType ? "前复权" : "ba" == window.authorityType ? "后复权" : "前复权"
            }
            function i(t, i) {
                this.clear(),
                    this.options.start = t,
                    this.options.end = i;
                var o = this.options.canvas;
                this.options.currentData = B(this.options.data, t, i);
                var e = this.options.currentData.data.length
                    , n = T.get_rect.apply(this, [o, e]);
                this.options.rect_unit = n,
                    this.options.drawXY.options.currentData = this.options.currentData,
                    this.options.drawXY.options.XMark = Z.apply(this, [this.options.currentData.data]),
                    this.options.drawXY.drawXYK(),
                    this.options.drawXY.drawXYV(),
                    this.options.drawXY.drawXYT();

                var s = this.options.up_t, a = this.options.down_t;

                "junxian" == s ? this.drawMA(t, i) : "sar" == s ? this.drawSAR(t, i) : "boll" == s ? this.drawBOLL(t, i) : "bbi" == s? this.drawBBI(t, i) : "expma" == s && this.drawEXPMA(t, i),

                                        "rsi" == a ? this.drawRSI(t, i) : "kdj" == a ? this.drawKDJ(t, i) : "macd" == a ? this.drawMACD(t, i) : "wr" == a ? this.drawWR(t, i) : "dmi" == a ? this.drawDMI(t, i) : "bias" == a ? this.drawBIAS(t, i) : "obv" == a ? this.drawOBV(t, i) : "cci" == a ? this.drawCCI(t, i) : "roc" == a && this.drawROC(t, i),
                    R.apply(this),
                    this.drawVMA(),
                    this.drawK()
}
            function o() {
                var t = this
                    , i = document.createElement("div");
                i.className = "kt-pad";
                var o = document.createDocumentFragment()
                    , e = document.createElement("div");
                e.className = "kt-title",
                    e.innerHTML = "主图指标",
                    o.appendChild(e);
                var n = function(i, o, e) {
                    var n = document.createElement("div");
                    n.className = "kt-line";
                    var s = document.createElement("div");
                    s.className = "kt-radio-wrap";
                    var a = document.createElement("div");
                    a.className = e ? "kt-radio kt-radio-choose" : "kt-radio",
                        s.appendChild(a),
                        n.appendChild(s);
                    var r = document.createElement("div");
                    r.className = "kt-name",
                        r.innerHTML = i,
                        n.appendChild(r),
                        T.addEvent(n, "click", function(i) {
                            {
                                var o;
                                i.srcElement || i.target
                            }
                            o = n.children[0].children[0];
                            for (var e = n.parentNode, s = 1; s < e.children.length; s++)
                                e.children[s].children[0].children[0].className = "kt-radio";
                            o.className = "kt-radio kt-radio-choose";
                            var a = n.children[1].innerHTML;
                            "均线" == a ? (t.options.up_t = "junxian",
                                t.drawMA(t.options.start, t.options.end)) : "EXPMA" == a ? (t.options.up_t = "expma",
                                t.drawEXPMA(t.options.start, t.options.end)) : "SAR" == a ? (t.options.up_t = "sar",
                                t.drawSAR(t.options.start, t.options.end)) : "BOLL" == a ? (t.options.up_t = "boll",
                                t.drawBOLL(t.options.start, t.options.end)) : "BBI" == a && (t.options.up_t = "bbi",
                                t.drawBBI(t.options.start, t.options.end))
                        }),
                        o.appendChild(n)
                };
                n("均线", o, !0),
                    n("EXPMA", o, !1),
                    n("SAR", o, !1),
                    n("BOLL", o, !1),
                    n("BBI", o, !1),
                    i.appendChild(o),
                    this.container.appendChild(i),
                    i.style.top = this.options.c1_y_top + "px",
                    i.style.left = this.options.canvas.width - this.options.padding.right - 10 + "px"
            }
            function L() {
                var t = this
                    , i = (this.options.context,
                    this.options.canvas)
                    , o = document.createElement("div");
                o.className = "tech-index",
                    o.style.width = this.options.drawWidth,
                    o.style.left = this.options.padding.left + "px",
                    o.style.top = 17 * i.height / this.options.y_sepe_num + "px";
                var e = document.createElement("div");
                e.setAttribute("id", "rsi"),
                    e.className = "tech-index-item current",
                    e.innerText = "RSI",
                    e.style.width = this.options.drawWidth / 9 + "px",
                    e.style.height = this.options.unit_height + "px",
                    e.style.lineHeight = this.options.unit_height + "px";
                var n = document.createElement("div");
                n.setAttribute("id", "kdj"),
                    n.className = "tech-index-item",
                    n.innerText = "KDJ",
                    n.style.width = this.options.drawWidth / 9 + "px",
                    n.style.height = this.options.unit_height + "px",
                    n.style.lineHeight = this.options.unit_height + "px";
                var s = document.createElement("div");
                s.setAttribute("id", "macd"),
                    s.className = "tech-index-item",
                    s.innerText = "MACD",
                    s.style.width = this.options.drawWidth / 9 + "px",
                    s.style.height = this.options.unit_height + "px",
                    s.style.lineHeight = this.options.unit_height + "px";
                var a = document.createElement("div");
                a.setAttribute("id", "wr"),
                    a.className = "tech-index-item",
                    a.innerText = "W%R",
                    a.style.width = this.options.drawWidth / 9 + "px",
                    a.style.height = this.options.unit_height + "px",
                    a.style.lineHeight = this.options.unit_height + "px";
                var r = document.createElement("div");
                r.setAttribute("id", "dmi"),
                    r.className = "tech-index-item",
                    r.innerText = "DMI",
                    r.style.width = this.options.drawWidth / 9 + "px",
                    r.style.height = this.options.unit_height + "px",
                    r.style.lineHeight = this.options.unit_height + "px";
                var p = document.createElement("div");
                p.setAttribute("id", "bias"),
                    p.className = "tech-index-item",
                    p.innerText = "BIAS",
                    p.style.width = this.options.drawWidth / 9 + "px",
                    p.style.height = this.options.unit_height + "px",
                    p.style.lineHeight = this.options.unit_height + "px";
                var h = document.createElement("div");
                h.setAttribute("id", "obv"),
                    h.className = "tech-index-item",
                    h.innerText = "OBV",
                    h.style.width = this.options.drawWidth / 9 + "px",
                    h.style.height = this.options.unit_height + "px",
                    h.style.lineHeight = this.options.unit_height + "px";
                var l = document.createElement("div");
                l.setAttribute("id", "cci"),
                    l.className = "tech-index-item",
                    l.innerText = "CCI",
                    l.style.width = this.options.drawWidth / 9 + "px",
                    l.style.height = this.options.unit_height + "px",
                    l.style.lineHeight = this.options.unit_height + "px";
                var c = document.createElement("div");
                c.setAttribute("id", "roc"),
                    c.className = "tech-index-item",
                    c.innerText = "ROC",
                    c.style.width = this.options.drawWidth / 9 + "px",
                    c.style.height = this.options.unit_height + "px",
                    c.style.lineHeight = this.options.unit_height + "px",
                    o.appendChild(e),
                    o.appendChild(n),
                    o.appendChild(s),
                    o.appendChild(a),
                    o.appendChild(r),
                    o.appendChild(p),
                    o.appendChild(h),
                    o.appendChild(l),
                    o.appendChild(c),
                    this.container.appendChild(o);
                var d = e;
                T.addEvent.call(t, e, "click", function() {
                    t.drawRSI(),
                        d.className = d.className.replace(" current", ""),
                        d = e,
                        d.className = d.className + " current"
                }),
                    T.addEvent.call(t, n, "click", function() {
                        t.drawKDJ(),
                            d.className = d.className.replace(" current", ""),
                            d = n,
                            d.className = d.className + " current"
                    }),
                    T.addEvent.call(t, s, "click", function() {
                        t.drawMACD(),
                            d.className = d.className.replace(" current", ""),
                            d = s,
                            d.className = d.className + " current"
                    }),
                    T.addEvent.call(t, a, "click", function() {
                        t.drawWR(),
                            d.className = d.className.replace(" current", ""),
                            d = a,
                            d.className = d.className + " current"
                    }),
                    T.addEvent.call(t, r, "click", function() {
                        t.drawDMI(),
                            d.className = d.className.replace(" current", ""),
                            d = r,
                            d.className = d.className + " current"
                    }),
                    T.addEvent.call(t, p, "click", function() {
                        t.drawBIAS(),
                            d.className = d.className.replace(" current", ""),
                            d = p,
                            d.className = d.className + " current"
                    }),
                    T.addEvent.call(t, h, "click", function() {
                        t.drawOBV(),
                            d.className = d.className.replace(" current", ""),
                            d = h,
                            d.className = d.className + " current"
                    }),
                    T.addEvent.call(t, l, "click", function() {
                        t.drawCCI(),
                            d.className = d.className.replace(" current", ""),
                            d = l,
                            d.className = d.className + " current"
                    }),
                    T.addEvent.call(t, c, "click", function() {
                        t.drawROC(),
                            d.className = d.className.replace(" current", ""),
                            d = c,
                            d.className = d.className + " current"
                    })
            }
            function R() {
                var t = this.options.context
                    , i = this.options.currentData
                    , o = i.data
                    , e = (i.v_max / 1).toFixed(0)
                    , n = 3 * t.canvas.height / this.options.y_sepe_num
                    , s = this.options.v_base_height
                    , a = (this.options.c2_y_top,
                this.options.c2_y_top + n)
                    , r = this.options.rect_unit
                    , p = r.bar_w
                    , h = this.options.up_color
                    , l = this.options.down_color;
                t.lineWidth = 1;
                for (var c, d = 0; c = o[d]; d++) {
                    var u = c.volume
                        , m = c.up
                        , f = u / e * s
                        , v = T.get_x.call(this, d + 1)
                        , g = a - f;
                    t.beginPath(),
                        t.moveTo(v, g),
                        m ? (t.fillStyle = "rgba(255,255,255,1)",
                            t.strokeStyle = h) : (t.fillStyle = l,
                            t.strokeStyle = l),
                        t.rect(D.format(v - p / 2), D.format(g), p, f),
                        t.stroke(),
                        t.fill()
                }
            }
            function I() {
                function t() {
                    var t = this.options.rsi.rsi6 || []
                        , i = this.options.rsi.rsi12 || []
                        , o = this.options.rsi.rsi24 || []
                        , e = this.options.start || []
                        , n = this.options.end
                        , t = this.options.rsi.rsi6 || [];
                    0 == t.length && 0 == i.length && 0 == o.length || c.apply(this, [this.options.context, t.slice(e, n), i.slice(e, n), o.slice(e, n)])
                }
                function i() {
                    var t = this
                        , i = this
                        , e = t.options.context
                        , n = t.options.junxian
                        , s = t.options.start
                        , p = t.options.end
                        , c = t.options.interactive
                        , d = n["ma" + a].slice(s, p)
                        , u = n["ma" + r].slice(s, p)
                        , m = n["ma" + h].slice(s, p)
                        , f = n["ma" + l].slice(s, p);
                    c.default_m5 = d[d.length - 1],
                        c.default_m10 = u[u.length - 1],
                        c.default_m20 = m[m.length - 1],
                        c.default_m30 = f[f.length - 1];
                    t.options.type;
                    a && 0 != a && o.apply(t, [e, d, this.options.color.m5Color]),
                    r && 0 != r && o.apply(t, [e, u, this.options.color.m10Color]),
                    h && 0 != h && o.apply(t, [e, m, this.options.color.m20Color]),
                    l && 0 != l && o.apply(t, [e, f, this.options.color.m30Color]),
                        e.save(),
                        e.fillStyle = i.options.theme.color.background_color,
                        e.fillRect(i.options.padding.left, -i.options.c1_y_top, i.options.drawWidth, i.options.c1_y_top),
                        e.restore()
                }
                function o(t, i, o) {
                    t.save();
                    var e = [];
                    t.beginPath(),
                        t.strokeStyle = o;
                    for (var n = !1, s = !0, a = 0; a < i.length; a++) {
                        var r = i[a];
                        if (r && r.value) {
                            var p = T.get_x.call(this, a + 1)
                                , h = T.get_y.call(this, r.value);
                            s && (t.moveTo(p, h),
                                s = !1),
                                e.push(r),
                                0 == a ? t.moveTo(p, h) : h > this.options.c_k_height || 0 > h ? (t.moveTo(p, h),
                                    n = !0) : (n ? t.moveTo(p, h) : t.lineTo(p, h),
                                    n = !1)
                        }
                    }
                    return t.stroke(),
                        t.restore(),
                        e
                }
                var e = this
                    , n = this.options.interactive
                    , s = {};
                s = W.call(this);
                var a = null == S.getCookie("ma1_default_num") ? 5 : S.getCookie("ma1_default_num")
                    , r = null == S.getCookie("ma2_default_num") ? 10 : S.getCookie("ma2_default_num")
                    , h = null == S.getCookie("ma3_default_num") ? 20 : S.getCookie("ma3_default_num")
                    , l = null == S.getCookie("ma4_default_num") ? 30 : S.getCookie("ma4_default_num");
                s.extend = "cma," + a + "," + r + "," + h + "," + l + "|rsi",
                    this.options.up_t = "junxian",
                    this.options.down_t = "rsi",
                    p(s, function(o) {
                        e.options.junxian = {},
                            e.options.junxian["ma" + a] = o.five_average,
                            e.options.junxian["ma" + r] = o.ten_average,
                            e.options.junxian["ma" + h] = o.twenty_average,
                            e.options.junxian["ma" + l] = o.thirty_average,
                            e.options.rsi = {},
                            e.options.rsi.rsi6 = o.rsi6,
                            e.options.rsi.rsi12 = o.rsi12,
                            e.options.rsi.rsi24 = o.rsi24,
                            i.apply(e, []),
                            t.apply(e, []),
                            e.options.interactive.options.markMAContainer ? (e.options.interactive.options.markMAContainer.innerHTML = "",
                                e.options.interactive.options.markMAContainer = null) : e.options.interactive.options.markMAContainer = null,
                            P.call(e, e.options.context);
                        var s = o.name + "[" + o.code + "]";
                        n.markMA(e.options.canvas, "junxian", e.options["junxian"], e.options.start, e.options.end, "", e.options.maColor, s),
                        o.rsi6 && o.rsi12 && o.rsi24 && n.markT(e.options.canvas, "rsi", e.options["rsi"], e.options.start, e.options.end, 59)
                    })
            }
            function Y(t) {
                var o = this
                    , e = o.options.start
                    , n = o.options.end
                    , s = (o.options.type,
                    o.options.data.data.length,
                t || o.options.scale_count);
                s ? e + 20 >= n ? e = n - 20 : e += 20 : 0 >= e - 20 ? e = 0 : e -= 20,
                    o.options.start = e,
                    o.options.end = n;
                var a = o.options.interactive;
                this.options.interactive.showLoading();
                try {
                    k.apply(this, [i, e, n]),
                        a.hideLoading()
                } catch (r) {
                    o.options.clickable = !0,
                        a.hideLoading()
                }
            }
            function W() {
                var t = {};
                return t.code = this.options.code,
                    t.type = this.options.type,
                    t.authorityType = window.authorityType,
                    t.extend = this.options.extend,
                    t
            }
            function F(t) {
                var o = this.options.context
                    , n = o.canvas;
                this.options.data = void 0 == t ? this.options.data : t,
                    t = this.options.data;
                var s = t.data
                    , a = s.length;
                a >= 1 ? (this.options.start = a > 60 ? a - 60 : 0,
                    this.options.end = a) : (this.options.start = 0,
                    this.options.end = 0),
                    this.options.currentData = B(this.options.data, this.options.start, this.options.end);
                var r = this.options.currentData.data
                    , p = r.length;
                this.options.XMark = Z.apply(this, [r]);
                var h = this.options.interactive;
                !t || !t.data || 0 == t.data.length,
                    this.options.pricedigit = t.pricedigit;
                var l = T.get_rect.apply(this, [n, p]);
                this.options.rect_unit = l,
                t && t.data.length > 0 && k.call(this, i);
                this.options.drawXY = new e(this.options);
                if (this.drawK(),
                    this.options.up_t = "junxian",
                    I.apply(this, []),
                    R.apply(this, [this.options]),
                    this.drawVMA(),
                    this.options.interactive.options.pointsContainer) {
                    var c = this.options.interactive.options.pointsContainer.children;
                    this.markPointsDom = c
                }
                return h.hideLoading(),
                    this.options.onChartLoaded(this),
                    N.apply(this, [this.options.context, 95 + this.options.padding.right, 10, 82, 20]),
                    !0
            }
            function Z(t) {
                var i = []
                    , o = t || this.options.currentData.data
                    , e = o.length;
                return e > 10 ? (i.push(o[0].date_time),
                    i.push(o[Math.floor(1 * e / 4)].date_time),
                    i.push(o[Math.floor(2 * e / 4)].date_time),
                    i.push(o[Math.floor(3 * e / 4)].date_time),
                    i.push(o[e - 1].date_time)) : o[0] && i.push(o[0].date_time),
                    i
            }
            function P(t) {
                var i = this
                    , o = t.canvas
                    , e = i.options.interactive;
                this.options.clickable = !0;
                this.options.delaytouch = !0;
                T.addEvent.call(i, o, "touchmove", function(t) {
                    G.apply(i, [e, t.changedTouches[0]]);
                    try {
                        t.preventDefault()
                    } catch (o) {
                        t.returnValue = !1
                    }
                }),
                    T.addEvent.call(i, o, "mousemove", function(t) {
                        G.apply(i, [e, t]);
                        try {
                            t.preventDefault()
                        } catch (o) {
                            t.returnValue = !1
                        }
                    }),
                    T.addEvent.call(i, i.container, "mouseleave", function(t) {
                        e.hide();
                        try {
                            t.preventDefault()
                        } catch (i) {
                            t.returnValue = !1
                        }
                    }),
                    T.addEvent.call(i, o, "mouseenter", function(t) {
                        e.show();
                        try {
                            t.preventDefault()
                        } catch (i) {
                            t.returnValue = !1
                        }
                    }),
                    T.addEvent.call(i, i.container, "mousewheel", function(t) {
                        if (i.options.data.data.length >= 60) {
                            t.wheelDelta > 0 ? i.scalePlus() : i.scaleMinus();
                            try {
                                t.preventDefault()
                            } catch (o) {
                                t.returnValue = !1
                            }
                        }
                    }),
                    T.addEvent.call(i, i.container, "DOMMouseScroll", function(t) {
                        if (i.options.data.data.length >= 60) {
                            t.detail > 0 ? i.scalePlus() : i.scaleMinus();
                            try {
                                t.preventDefault()
                            } catch (o) {
                                t.returnValue = !1
                            }
                        }
                    })
            }
            function G(t, i) {
                var o = this.options.canvas
                    , e = this.options.currentData.data
                    , n = null == S.getCookie("ma1_default_num") ? 5 : S.getCookie("ma1_default_num")
                    , s = null == S.getCookie("ma2_default_num") ? 10 : S.getCookie("ma2_default_num")
                    , a = null == S.getCookie("ma3_default_num") ? 20 : S.getCookie("ma3_default_num")
                    , r = null == S.getCookie("ma4_default_num") ? 30 : S.getCookie("ma4_default_num")
                    , p = this.options.junxian["ma" + n]
                    , h = (this.options.junxian["ma" + s],
                    this.options.junxian["ma" + a],
                    this.options.junxian["ma" + r],
                    this.options.v_ma_5)
                    , l = this.options.v_ma_10
                    , c = this.options.rect_unit
                    , d = c.rect_w
                    , u = i.offsetX || i.clientX - this.container.getBoundingClientRect().left
                    , m = i.offsetY || i.clientY - this.container.getBoundingClientRect().top
                    , f = T.windowToCanvas.apply(this, [o, u, m])
                    , v = f.x
                    , g = f.y - this.options.margin.top
                    , y = Math.floor((v - this.options.padding.left) / d);
                0 > y && (y = 0);
                try {
                    if (e[y]) {
                        var A = e[y].cross_x
                            , x = e[y].cross_y
                            , w = e[y].cross_y_open
                            , _ = e[y].cross_y_highest
                            , b = e[y].cross_y_lowest;
                        t.cross(o, A, x, g, w, _, b);
                        var M = 0 == y ? e[y].open : e[y - 1].close;
                        e[y].yc = M,
                            t.showTip(o, A, x, g, w, _, b, e[y])
                    }
                    if (p[y]) {
                        var k = this.options.data.name + "[" + this.options.data.code + "]";
                        "junxian" == this.options.up_t ? t.markMA(o, this.options.up_t, this.options[this.options.up_t], this.options.start, this.options.end, y, this.options.maColor, k) : t.markMA(o, this.options.up_t, this.options[this.options.up_t], this.options.start, this.options.end, y, this.options.TColor, k),
                            t.markVMA(o, e[y].volume, h[y], l[y]),
                            t.markT(o, this.options.down_t, this.options[this.options.down_t], this.options.start, this.options.end, y)
                    }
                } catch (C) {}
            }
            function B(t, i, o) {
                var e = z(t);
                e.max = -1e10,
                    e.min = 1e10,
                    e.v_max = 0,
                    e.total = o - i + 1,
                    e.name = t.name,
                    e.code = t.code,
                    e.v_ma_5 = t.v_ma_5.slice(i, o),
                    e.v_ma_10 = t.v_ma_10.slice(i, o),
                    e.data = t.data.slice(i, o);
                for (var n = i; o >= n; n++) {
                    var s = t.data[n];
                    if (s) {
                        var a = 1 * t.v_ma_5[n].value
                            , r = 1 * t.v_ma_10[n].value;
                        e.max = Math.max(s.highest, e.max),
                            e.min = Math.min(s.lowest, e.min);
                        var p = 1 * s.volume;
                        e.v_max = T.findArrayMax([a, r, p, e.v_max])
                    }
                }
                return e.max = e.max + .05 * (e.max - e.min),
                    e.min = e.min - .05 * (e.max - e.min) < 0 ? 0 : e.min - .05 * (e.max - e.min),
                0 == t.data.length && (e.max = 0,
                    e.min = 0),
                    e
            }
            function z(t) {
                var i = {};
                for (var o in t)
                    i[o] = "object" == typeof t[o] ? z(t[o]) : t[o];
                return i
            }
            return t.prototype.init = function() {
                this.options.type = void 0 == this.options.type ? "K" : this.options.type;
                var t = document.createElement("canvas");
                this.container.style.position = "relative";
                try {
                    var i = t.getContext("2d")
                } catch (o) {
                    t = window.G_vmlCanvasManager.initElement(t);
                    var i = t.getContext("2d")
                }
                this.options.canvas = t,
                    this.options.context = i;
                var e = this.options.dpr = 1;
                this.container.innerHTML = "",
                    this.container.appendChild(t),
                    t.width = this.options.width * e,
                    t.height = this.options.height * e,
                    this.options.scale_count = void 0 == this.options.scale_count ? !1 : this.options.scale_count,
                    t.style.width = this.options.width + "px",
                    t.style.height = this.options.height + "px",
                    t.style.border = "0",
                    i.font = this.options.font_size * this.options.dpr + "px Arial",
                    i.lineWidth = 1 * this.options.dpr,
                    i.strokeStyle = "rgba(230,230,230, 1)",
                    i.fillStyle = "#717171",
                    i.textBaseline = "middle",
                    this.options.color = {},
                    this.options.color.strokeStyle = "rgba(230,230,230, 1)",
                    this.options.color.fillStyle = "#717171",
                    this.options.color.m5Color = null == S.getCookie("ma1_default_color") ? "#0000ff" : S.getCookie("ma1_default_color"),
                    this.options.color.m10Color = null == S.getCookie("ma2_default_color") ? "#000000" : S.getCookie("ma2_default_color"),
                    this.options.color.m20Color = null == S.getCookie("ma3_default_color") ? "#ff0000" : S.getCookie("ma3_default_color"),
                    this.options.color.m30Color = null == S.getCookie("ma4_default_color") ? "#008000" : S.getCookie("ma4_default_color"),
                    this.options.maColor = [this.options.color.m5Color, this.options.color.m10Color, this.options.color.m20Color, this.options.color.m30Color],
                    this.options.TColor = ["#0000ff", "#000000", "#ff0000", "#008000"];
                var n = this.options.YMaxNum || "100000.00";
                this.options.padding = {},
                    this.options.padding.left = i.measureText(n).width,
                    this.options.padding.right = 60,
                    this.options.padding.top = 0,
                    this.options.padding.bottom = 0,
                    this.options.drawWidth = t.width - this.options.padding.left - this.options.padding.right,
                    this.options.y_sepe_num = 20,
                    this.options.x_sepe_num = 10,
                    this.options.unit_height = 1 * t.height / this.options.y_sepe_num,
                    this.options.unit_width = 1 * t.width / this.options.x_sepe_num,
                    this.options.c1_y_top = 1 * t.height / this.options.y_sepe_num,
                    this.options.c2_y_top = 10 * t.height / this.options.y_sepe_num,
                    this.options.c3_y_top = 14 * t.height / this.options.y_sepe_num,
                    this.options.c4_y_top = 18 * t.height / this.options.y_sepe_num,
                    this.options.c_k_height = 8 * t.height / this.options.y_sepe_num,
                    this.options.c_v_height = 3 * t.height / this.options.y_sepe_num,
                    this.options.v_base_height = .9 * this.options.c_v_height,
                    this.options.c_t_height = 2 * t.height / this.options.y_sepe_num,
                    this.options.margin = {},
                    this.options.margin.left = 0,
                    this.options.margin.top = 1 * t.height / this.options.y_sepe_num,
                    i.translate("0", this.options.margin.top),
                    this.newDotPoint = new l(this.options,this.container),
                    this.initTheme()
            }
                ,
                t.prototype.initTheme = function() {
                    if ("string" == typeof this.options.theme) {
                        var t = s.cover;
                        for (var i in t)
                            this.options[i] = t[i];
                        this.options.theme = s,
                            this.container.style.backgroundColor = this.options.theme.color.background_color,
                        -1 == this.container.className.indexOf(s.themeClass) && (this.container.className = this.container.className + " " + s.themeClass);
                        var o = this.options.context;
                        o.fillStyle = this.options.theme.color.background_color,
                            o.fillRect(0, 0, this.options.wdith, this.options.height)
                    } else
                        this.options.theme = a
                }
                ,
                t.prototype.draw = function(t) {
                    var i = this;
                    i.clear(),
                        i.init();
                    var e = i.options.interactive = new M(i.options);
                    e.showLoading();
                    i.options.type;
                    try {
                        var n = W.call(i);
                        n.width = this.options.width,
                            n.height = this.options.height,
                            n.inter = e,
                            n.container = this.container,
                            r(n, function(n) {
                                if (n.data.length > 0 && (L.apply(i),
                                    o.apply(i),
                                    E.apply(i)),
                                    n.name) {
                                    n.data.length < 60;
                                    var s = F.apply(i, [n]);
                                    s && (e.markMA(i.options.canvas),
                                        0 == n.data.length ? e.markVMA(i.options.canvas, "not") : e.markVMA(i.options.canvas)),
                                    t && t(null)
                                } else
                                    e.showNoData(),
                                        e.hideLoading()
                            })
                    } catch (s) {
                        e.showNoData(),
                            e.hideLoading(),
                        t && t(s)
                    }
                }
                ,
                t.prototype.reDraw = function() {
                    var t = this;
                    this.clear(),
                        this.init();
                    var i = t.options.interactive
                        , e = this.options.data;
                    if (e) {
                        var n = F.apply(t, [e]);
                        n && (i.markMA(t.options.canvas),
                            i.markVMA(t.options.canvas))
                    } else
                        i.showNoData();
                    L.apply(this),
                        o.apply(this),
                        E.apply(this)
                }
                ,
                t.prototype.clear = function() {
                    try {
                        var t = this.options.context;
                        t.clearRect(0, 0, this.options.padding.left + this.options.drawWidth, this.options.c4_y_top)
                    } catch (i) {
                        this.container.innerHTML = ""
                    }
                }
                ,
                t.prototype.getMarkPointsDom = function() {
                    var t = this.options.interactive.options.pointsContainer.children;
                    return t
                }
                ,
                t.prototype.drawVMA = function() {
                    function t(t, i, o) {
                        var e = [];
                        t.save(),
                            t.beginPath(),
                            t.strokeStyle = o;
                        for (var n = 0; n < i.length; n++) {
                            var s = i[n];
                            if (s) {
                                var h = T.get_x.call(this, n + 1)
                                    , l = (1 - s.value / a) * r + p;
                                e.push(s),
                                    0 == n ? t.moveTo(h, l) : t.lineTo(h, l)
                            }
                        }
                        return t.stroke(),
                            t.restore(),
                            e
                    }
                    var i = this.options.currentData
                        , o = this.options.context
                        , e = this.options.interactive
                        , n = i.v_ma_5
                        , s = i.v_ma_10
                        , a = (i.v_max / 1).toFixed(0)
                        , r = this.options.v_base_height
                        , p = this.options.c2_y_top;
                    this.options.v_ma_5 = t.apply(this, [o, n, "#FF00FF"]),
                        this.options.v_ma_10 = t.apply(this, [o, s, "#808080"]),
                        e.default_volume = i.data[i.data.length - 1],
                        e.default_vm5 = n[n.length - 1],
                        e.default_vm10 = s[s.length - 1]
                }
                ,
                t.prototype.drawMA = function(t, i) {
                    function o() {
                        var o = this
                            , n = o.options.context
                            , p = o.options.junxian
                            , l = o.options.interactive
                            , c = p["ma" + s].slice(t, i)
                            , d = p["ma" + a].slice(t, i)
                            , u = p["ma" + r].slice(t, i)
                            , m = p["ma" + h].slice(t, i);
                        l.default_m5 = c[c.length - 1],
                            l.default_m10 = d[d.length - 1],
                            l.default_m20 = u[u.length - 1],
                            l.default_m30 = m[m.length - 1],
                        s && 0 != s && e.apply(o, [n, c, this.options.color.m5Color]),
                        a && 0 != a && e.apply(o, [n, d, this.options.color.m10Color]),
                        r && 0 != r && e.apply(o, [n, u, this.options.color.m20Color]),
                        h && 0 != h && e.apply(o, [n, m, this.options.color.m30Color])
                    }
                    function e(t, i, o) {
                        var e = this;
                        t.save();
                        var n = [];
                        t.beginPath(),
                            t.strokeStyle = o;
                        for (var s = !1, a = !0, r = 0; r < i.length; r++) {
                            var p = i[r];
                            if (p && parseFloat(p.value)) {
                                var h = T.get_x.call(this, r + 1)
                                    , l = T.get_y.call(this, p.value);
                                a && (t.moveTo(h, l),
                                    a = !1),
                                    n.push(p),
                                    0 === r ? t.moveTo(h, l) : l > this.options.c_k_height || 0 > l ? (t.moveTo(h, l),
                                        s = !0) : (s ? t.moveTo(h, l) : t.lineTo(h, l),
                                        s = !1)
                            }
                        }
                        return t.stroke(),
                            t.restore(),
                            t.save(),
                            t.fillStyle = e.options.theme.color.background_color,
                            t.fillRect(e.options.padding.left, -e.options.c1_y_top, e.options.drawWidth, e.options.c1_y_top),
                            t.restore(),
                            n
                    }
                    var n = this;
                    this.clearK(),
                        this.options.drawXY.drawXYK(),
                        this.drawK();
                    var s = null == S.getCookie("ma1_default_num") ? 5 : S.getCookie("ma1_default_num")
                        , a = null == S.getCookie("ma2_default_num") ? 10 : S.getCookie("ma2_default_num")
                        , r = null == S.getCookie("ma3_default_num") ? 20 : S.getCookie("ma3_default_num")
                        , h = null == S.getCookie("ma4_default_num") ? 30 : S.getCookie("ma4_default_num")
                        , l = {};
                    l = W.call(this),
                        l.extend = "cma," + s + "," + a + "," + r + "," + h,
                        this.options.junxian ? (data = n.options.junxian,
                            o.apply(n, [])) : p(l, function(t) {
                            n.options.junxian = {},
                                n.options.junxian["ma" + s] = t.five_average,
                                n.options.junxian["ma" + a] = t.ten_average,
                                n.options.junxian["ma" + r] = t.twenty_average,
                                n.options.junxian["ma" + h] = t.thirty_average,
                                o.apply(n, [])
                        })
                }
                ,
                t.prototype.drawK = function(t) {
                    this.newDotPoint.delDom();
                    var i = void 0 == t ? this.options.currentData.data : t
                        , o = this.options.context
                        , e = this.options.rect_unit
                        , n = e.bar_w
                        , s = this.options.up_color
                        , a = this.options.down_color
                        , r = this.options.interactive
                        , p = {};
                    if (this.options.markPoint && this.options.markPoint.show) {
                        var l = this.options.markPoint.dateList;
                        for (var c in l)
                            p[l[c]] = l[c]
                    }
                    for (var d, u = {}, m = 0; d = i[m]; m++) {
                        var f = d.up;
                        o.beginPath(),
                            o.lineWidth = 1,
                            f ? (o.fillStyle = "rgba(255,255,255,1)",
                                o.strokeStyle = s) : (o.fillStyle = a,
                                o.strokeStyle = a),
                            u.ctx = o;
                        var v = u.x = T.get_x.call(this, m + 1);
                        u.y_open = T.get_y.call(this, d.open);
                        var g = u.y_close = T.get_y.call(this, d.close);
                        u.y_highest = T.get_y.call(this, d.highest),
                            u.y_lowest = T.get_y.call(this, d.lowest),
                            d.cross_x = v,
                            d.cross_y = g,
                            d.cross_y_open = u.y_open,
                            d.cross_y_lowest = u.y_lowest,
                            d.cross_y_highest = u.y_highest,
                        p[d.data_time] && r.markPoint(v, d.data_time, this.options.context.canvas, this.options.scale_count),
                            u.bar_w = n,
                            h.apply(this, [u, d]),
                        this.options.dotPoint && this.newDotPoint.dot(u, d)
                    }
                }
                ,
                t.prototype.drawRSI = function(t, i) {
                    function o() {
                        var o = this.options.rsi.rsi6
                            , e = this.options.rsi.rsi12
                            , n = this.options.rsi.rsi24;
                        (void 0 == t || void 0 == i) && (t = this.options.start,
                            i = this.options.end),
                            i += 1,
                            c.apply(this, [this.options.context, o.slice(t, i), e.slice(t, i), n.slice(t, i)])
                    }
                    var e = this
                        , n = {};
                    n = W.call(this),
                        n.extend = this.options.down_t = "rsi",
                        this.options.rsi ? o.apply(e, []) : p(n, function(t) {
                            e.options.rsi = {},
                                e.options.rsi.rsi6 = t.rsi6,
                                e.options.rsi.rsi12 = t.rsi12,
                                e.options.rsi.rsi24 = t.rsi24,
                                o.apply(e, [])
                        })
                }
                ,
                t.prototype.drawKDJ = function(t, i) {
                    function o() {
                        var o = this.options.kdj.k
                            , e = this.options.kdj.d
                            , n = this.options.kdj.j;
                        (void 0 == t || void 0 == i) && (t = this.options.start,
                            i = this.options.end),
                            i += 1,
                            d.apply(this, [this.options.context, o.slice(t, i), e.slice(t, i), n.slice(t, i)])
                    }
                    var e = this
                        , n = {};
                    n = W.call(this),
                        n.extend = this.options.down_t = "kdj",
                        this.options.kdj ? o.apply(e, []) : p(n, function(t) {
                            e.options.kdj = {},
                                e.options.kdj.k = t.k,
                                e.options.kdj.d = t.d,
                                e.options.kdj.j = t.j,
                                o.apply(e, [])
                        })
                }
                ,
                t.prototype.drawMACD = function(t, i) {
                    function o() {
                        var o = this.options.macd.dea
                            , n = this.options.macd.diff
                            , s = this.options.macd.macd;
                        (void 0 == t || void 0 == i) && (t = this.options.start, i = this.options.end),
                            i += 1,
                            b.apply(e, [e.options.context, o.slice(t, i), n.slice(t, i), s.slice(t, i)])
                    }
                    var e = this, n = {};
                    n = W.call(this),
                    n.extend = this.options.down_t = "macd",
                    this.options.macd ? o.apply(this, []) : p(n, function(t) {
                            e.options.macd = {},
                            e.options.macd.dea = t.dea,
                            e.options.macd.diff = t.diff,
                            e.options.macd.macd = t.macd,
                            o.apply(e, [])
                    })
                }
                ,
                t.prototype.drawWR = function(t, i) {
                    function o() {
                        var o = this.options.wr.wr6
                            , n = this.options.wr.wr10;
                        (void 0 == t || void 0 == i) && (t = this.options.start,
                            i = this.options.end),
                            i += 1,
                            u.apply(e, [e.options.context, o.slice(t, i), n.slice(t, i)])
                    }
                    var e = this
                        , n = {};
                    n = W.call(this),
                        n.extend = this.options.down_t = "wr",
                        this.options.wr ? o.apply(this, []) : p(n, function(t) {
                            e.options.wr = {},
                                e.options.wr.wr6 = t.wr6,
                                e.options.wr.wr10 = t.wr10,
                                o.apply(e, [])
                        })
                }
                ,
                t.prototype.drawDMI = function(t, i) {
                    function o() {
                        var o = this.options.dmi.pdi
                            , n = this.options.dmi.mdi
                            , s = this.options.dmi.adx
                            , a = this.options.dmi.adxr;
                        (void 0 == t || void 0 == i) && (t = this.options.start,
                            i = this.options.end),
                            i += 1,
                            m.apply(e, [e.options.context, o.slice(t, i), n.slice(t, i), s.slice(t, i), a.slice(t, i)])
                    }
                    var e = this
                        , n = {};
                    n = W.call(this),
                        n.extend = this.options.down_t = "dmi",
                        this.options.dmi ? o.apply(e, []) : p(n, function(t) {
                            e.options.dmi = {},
                                e.options.dmi.pdi = t.pdi,
                                e.options.dmi.mdi = t.mdi,
                                e.options.dmi.adx = t.adx,
                                e.options.dmi.adxr = t.adxr,
                                o.apply(e, [])
                        })
                }
                ,
                t.prototype.drawBIAS = function(t, i) {
                    function o() {
                        var o = this.options.bias.bias6
                            , e = this.options.bias.bias12
                            , n = this.options.bias.bias24;
                        (void 0 == t || void 0 == i) && (t = this.options.start,
                            i = this.options.end),
                            i += 1,
                            f.apply(this, [this.options.context, o.slice(t, i), e.slice(t, i), n.slice(t, i)])
                    }
                    var e = this
                        , n = {};
                    n = W.call(this),
                        n.extend = this.options.down_t = "bias",
                        this.options.bias ? o.apply(e, []) : p(n, function(t) {
                            e.options.bias = {},
                                e.options.bias.bias6 = t.bias6,
                                e.options.bias.bias12 = t.bias12,
                                e.options.bias.bias24 = t.bias24,
                                o.apply(e, [])
                        })
                }
                ,
                t.prototype.drawOBV = function(t, i) {
                    function o() {
                        var o = this.options.obv.obv
                            , e = this.options.obv.maobv;
                        (void 0 == t || void 0 == i) && (t = this.options.start,
                            i = this.options.end),
                            i += 1,
                            v.apply(this, [this.options.context, o.slice(t, i), e.slice(t, i)])
                    }
                    var e = this
                        , n = {};
                    n = W.call(this),
                        n.extend = this.options.down_t = "obv",
                        e.options.obv ? o.apply(this, []) : p(n, function(t) {
                            e.options.obv = {},
                                e.options.obv.obv = t.obv,
                                e.options.obv.maobv = t.maobv,
                                o.apply(e, [])
                        })
                }
                ,
                t.prototype.drawCCI = function(t, i) {
                    function o() {
                        var o = this.options.cci.cci;
                        (void 0 == t || void 0 == i) && (t = this.options.start,
                            i = this.options.end),
                            i += 1,
                            g.apply(this, [this.options.context, o.slice(t, i)])
                    }
                    var e = this
                        , n = {};
                    n = W.call(this),
                        n.extend = this.options.down_t = "cci",
                        this.options.cci ? o.apply(e, []) : p(n, function(t) {
                            e.options.cci = {},
                                e.options.cci.cci = t.cci,
                                o.apply(e, [])
                        })
                }
                ,
                t.prototype.drawROC = function(t, i) {
                    function o() {
                        var o = e.options.roc.roc
                            , n = e.options.roc.rocma;
                        (void 0 == t || void 0 == i) && (t = this.options.start,
                            i = this.options.end),
                            i += 1,
                            y.apply(e, [e.options.context, o.slice(t, i), n.slice(t, i)])
                    }
                    var e = this
                        , n = {};
                    n = W.call(this),
                        n.extend = this.options.down_t = "roc",
                        e.options.roc ? o.apply(e, []) : p(n, function(t) {
                            e.options.roc = {},
                                e.options.roc.roc = t.roc,
                                e.options.roc.rocma = t.rocma,
                                o.apply(e, [])
                        })
                }
                ,
                t.prototype.drawEXPMA = function() {
                    function t(t, o) {
                        for (var e, n = this.options.expma.expma12.slice(t, o), s = this.options.expma.expma50.slice(t, o), a = n.concat(s), r = i.options.currentData.max, p = i.options.currentData.min, h = 0; e = a[h]; h++)
                            r = Math.max(r, e.value),
                                p = Math.min(p, e.value);
                        this.options.currentData.max = r,
                            this.options.currentData.min = p,
                            this.options.drawXY.options.currentData = this.options.currentData,
                            A.apply(this, [this.options.context, n, s]);
                        var l = this.options.data.name + "[" + this.options.data.code + "]";
                        this.options.interactive.options.markUPTType = "expma";
                        var c = [{
                            name: "expma12",
                            value: n[n.length - 1].value
                        }, {
                            name: "expma50",
                            value: s[s.length - 1].value
                        }];
                        i.options.interactive.firstMarkMA(i.options.canvas, "expma", c, i.options.maColor, l)
                    }
                    var i = this
                        , o = {};
                    o = W.call(this),
                        o.extend = this.options.up_t = "expma",
                        this.options.expma ? t.apply(i, [this.options.start, this.options.end]) : p(o, function(o) {
                            i.options.expma = {},
                                i.options.expma.expma12 = o.expma12,
                                i.options.expma.expma50 = o.expma50,
                                t.apply(i, [i.options.start, i.options.end])
                        })
                }
                ,
                t.prototype.drawBOLL = function() {
                    function t(t, o) {
                        for (var e, n = this.options.boll.bollup.slice(t, o), s = this.options.boll.bollmb.slice(t, o), a = this.options.boll.bolldn.slice(t, o), r = n.concat(s).concat(a), p = i.options.currentData.max, h = i.options.currentData.min, l = 0; e = r[l]; l++)
                            p = Math.max(p, e.value),
                                h = Math.min(h, e.value);
                        this.options.currentData.max = p,
                            this.options.currentData.min = h,
                            this.options.drawXY.options.currentData = this.options.currentData,
                            w.apply(i, [i.options.context, n, s, a]);
                        var c = this.options.data.name + "[" + this.options.data.code + "]";
                        this.options.interactive.options.markUPTType = "boll";
                        var d = [{
                            name: "bollup",
                            value: n[n.length - 1].value
                        }, {
                            name: "bollmb",
                            value: s[s.length - 1].value
                        }, {
                            name: "bolldn",
                            value: a[a.length - 1].value
                        }];
                        i.options.interactive.firstMarkMA(i.options.canvas, "boll", d, i.options.maColor, c)
                    }
                    var i = this
                        , o = {};
                    o = W.call(this),
                        o.extend = this.options.up_t = "boll",
                        this.options.boll ? t.apply(i, [i.options.start, i.options.end]) : p(o, function(o) {
                            i.options.boll = {},
                                i.options.boll.bollup = o.bollup,
                                i.options.boll.bollmb = o.bollmb,
                                i.options.boll.bolldn = o.bolldn,
                                t.apply(i, [i.options.start, i.options.end])
                        })
                }
                ,
                t.prototype.drawSAR = function() {
                    function t(t, o) {
                        for (var n, s = this.options.sar.sar.slice(t, o), a = i.options.currentData.max, r = i.options.currentData.min, p = 0; n = s[p]; p++)
                            a = Math.max(a, n.value),
                                r = Math.min(r, n.value);
                        this.options.currentData.max = a,
                            this.options.currentData.min = r,
                            this.options.drawXY.options.currentData = this.options.currentData,
                            x.apply(i, [i.options.context, s, e]);
                        var h = this.options.data.name + "[" + this.options.data.code + "]";
                        this.options.interactive.options.markUPTType = "sar";
                        var l = [{
                            name: "sar",
                            value: s[s.length - 1].value
                        }];
                        i.options.interactive.firstMarkMA(i.options.canvas, "sar", l, i.options.maColor, h)
                    }
                    var i = this
                        , o = {};
                    o = W.call(this),
                        o.extend = this.options.up_t = "sar";
                    var e = this.options.currentData.data;
                    this.options.sar ? t.apply(i, [i.options.start, i.options.end]) : p(o, function(o) {
                        i.options.sar = {},
                            i.options.sar.sar = o.sar,
                            t.apply(i, [i.options.start, i.options.end])
                    })
                }
                ,
                t.prototype.drawBBI = function() {
                    function t(t, o) {
                        for (var e, n = this.options.bbi.bbi.slice(t, o), s = i.options.currentData.max, a = i.options.currentData.min, r = 0; e = n[r]; r++)
                            s = Math.max(s, e.value),
                                a = Math.min(a, e.value);
                        this.options.currentData.max = s,
                            this.options.currentData.min = a,
                            this.options.drawXY.options.currentData = this.options.currentData,
                            _.apply(i, [i.options.context, n]);
                        var p = this.options.data.name + "[" + this.options.data.code + "]";
                        this.options.interactive.options.markUPTType = "bbl";
                        var h = [{
                            name: "bbl",
                            value: n[n.length - 1].value
                        }];
                        i.options.interactive.firstMarkMA(i.options.canvas, "bbl", h, i.options.maColor, p)
                    }
                    var i = this
                        , o = {};
                    o = W.call(this),
                        o.extend = this.options.up_t = "bbi",
                        this.options.bbi ? t.apply(i, [i.options.start, i.options.end]) : p(o, function(o) {
                            i.options.bbi = {},
                                i.options.bbi.bbi = o.bbi,
                                t.apply(i, [i.options.start, i.options.end])
                        })
                }
                ,
                t.prototype.clearK = function() {
                    var t = this.options.context;
                    t.fillStyle = this.options.theme.color.background_color,
                        t.fillRect(0, -1 * this.options.unit_height, this.options.padding.left + this.options.drawWidth + 10, this.options.c2_y_top),
                        N.apply(this, [this.options.context, 95 + this.options.padding.right, 10, 82, 20]),
                        this.options.watermark = !0
                }
                ,
                t.prototype.clearT = function() {
                    var t = this.options.context;
                    t.fillStyle = this.options.theme.color.background_color,
                        t.fillRect(0, this.options.c3_y_top - 10, this.options.padding.left + this.options.drawWidth + 10, this.options.c4_y_top)
                }
                ,
                t.prototype.scalePlus = function() {
                    this.options.data.data.length >= 60 && Y.call(this, !0)
                }
                ,
                t.prototype.scaleMinus = function() {
                    this.options.data.data.length >= 60 && Y.call(this, !1)
                }
                ,
                t.prototype.beforeBackRight = function(t) {
                    var i = 1e6
                        , o = new Date;
                    o.setTime(o.getTime() + 24 * i * 60 * 60 * 1e3),
                        t ? 1 == t ? (window.authorityType = "fa",
                            S.setCookie("beforeBackRight", "fa", o, "/")) : 2 == t && (window.authorityType = "ba",
                            S.setCookie("beforeBackRight", "ba", o, "/")) : (window.authorityType = "",
                            S.setCookie("beforeBackRight", "", o, "/")),
                        this.clear(),
                        this.draw()
                }
                ,
                t
        }();
        t.exports = L
    }
    , function(t, i, o) {
        var e = o(21)
            , n = o(7)
            , s = (o(61),
            o(25))
            , a = o(60)
            , r = o(9)
            , p = function() {
            function t(t) {
                this.defaultoptions = n.draw_xy,
                    this.options = e(this.defaultoptions, t),
                    this.draw()
            }
            function i(t, i) {
                var o = this.options.drawWidth
                    , e = o / this.options.x_sepe_num
                    , n = this.options.XMark;
                t.save();
                for (var a = 0; a <= this.options.x_sepe_num; a++) {
                    t.beginPath();
                    var p = this.options.padding.left + a * e
                        , h = 0
                        , l = this.options.padding.left + a * e
                        , c = i;
                    t.strokeStyle = this.options.theme.drawXY.strokeStyle,
                        0 == a ? (t.moveTo(r(p), r(h)),
                            t.lineTo(r(l), r(c)),
                            t.stroke()) : a == this.options.x_sepe_num ? (t.moveTo(r(p - 1), r(h)),
                            t.lineTo(r(l - 1), r(c)),
                            t.stroke()) : (t.strokeStyle = this.options.theme.drawXY.dashed,
                            s(t, p, h, l, c, 5))
                }
                var d = n.length;
                t.font = "12px Arial,Helvetica,San-serif";
                var u = 1;
                t.measureText(n[0]).width + 4 >= this.options.drawWidth / 6 && (u = 2);
                for (var m = 0; d > m; m += u)
                    0 == m ? t.fillText(n[m], m / 4 * this.options.drawWidth + this.options.padding.left, this.options.c_k_height + this.options.unit_height / 2) : m == d - 1 ? t.fillText(n[m], m / 4 * this.options.drawWidth + this.options.padding.left - t.measureText(n[m]).width, this.options.c_k_height + this.options.unit_height / 2) : t.fillText(n[m], m / 4 * this.options.drawWidth + this.options.padding.left - t.measureText(n[m]).width / 2, this.options.c_k_height + this.options.unit_height / 2);
                t.restore()
            }
            function o(t, i, o, e) {
                for (var n = (t - i) / (o - 1), s = [], a = 0; o > a; a++)
                    s.push({
                        num: i + a * n,
                        x: 0,
                        y: e - a / (o - 1) * e
                    });
                return s
            }
            return t.prototype.draw = function() {
                this.drawXYK(),
                    this.drawXYV(),
                    this.drawXYT()
            }
                ,
                t.prototype.drawXYT = function() {
                    var t = this.options.context;
                    t.save(),
                        t.beginPath(),
                        t.fillStyle = this.options.theme.drawXY.strokeStyle,
                        t.strokeStyle = this.options.theme.drawXY.strokeStyle,
                        t.moveTo(r(this.options.padding.left), r(this.options.c3_y_top - this.options.unit_height)),
                        t.lineTo(r(this.options.padding.left), r(this.options.c3_y_top + this.options.c_t_height)),
                        this.options.context.rect(r(this.options.padding.left), r(this.options.c3_y_top - this.options.unit_height), Math.round(this.options.drawWidth), Math.round(this.options.c_t_height + this.options.unit_height)),
                        t.stroke();
                    for (var i = this.options.c3_y_top, o = 0; 3 > o; o++) {
                        t.beginPath();
                        var e = this.options.padding.left
                            , n = i + 1 * t.canvas.height / this.options.y_sepe_num * o
                            , a = this.options.padding.left + this.options.drawWidth
                            , p = i + 1 * t.canvas.height / this.options.y_sepe_num * o;
                        t.strokeStyle = this.options.theme.drawXY.strokeStyle,
                            0 == o || 2 == o ? (t.moveTo(r(e), r(n)),
                                t.lineTo(r(a), r(p)),
                                t.stroke()) : (t.strokeStyle = this.options.theme.drawXY.dashed,
                                s(t, e, n, a, p, 5))
                    }
                    t.beginPath(),
                        t.strokeStyle = this.options.theme.drawXY.dashed;
                    for (var h = this.options.drawWidth, l = this.options.c_t_height, c = h / this.options.x_sepe_num, o = 0; o <= this.options.x_sepe_num; o++) {
                        var e = this.options.padding.left + o * c
                            , n = this.options.c3_y_top
                            , a = this.options.padding.left + o * c
                            , p = this.options.c3_y_top + l;
                        0 != o && o != this.options.x_sepe_num && s(t, e, n, a, p, 5)
                    }
                    t.stroke(),
                        t.beginPath(),
                        t.restore()
                }
                ,
                t.prototype.drawXYV = function() {
                    var t = this.options.context
                        , i = this.options.currentData || this.options.data
                        , o = i.pricedigit
                        , o = i.pricedigit;
                    t.save(),
                        t.beginPath(),
                        t.strokeStyle = this.options.theme.drawXY.strokeStyle;
                    var e = r(this.options.padding.left)
                        , n = r(this.options.c2_y_top - this.options.unit_height)
                        , p = Math.round(this.options.drawWidth)
                        , h = Math.round(this.options.c_v_height + this.options.unit_height);
                    this.options.context.rect(e, n, p, h),
                        t.stroke();
                    var l = this.options.c2_y_top;
                    t.fillStyle = this.options.theme.drawXY.fillStyle,
                        t.strokeStyle = this.options.theme.drawXY.strokeStyle;
                    for (var c = 0; 4 > c; c++) {
                        t.beginPath();
                        var d = this.options.padding.left
                            , u = l + 1 * t.canvas.height / this.options.y_sepe_num * c
                            , m = t.canvas.width - this.options.padding.right
                            , f = l + 1 * t.canvas.height / this.options.y_sepe_num * c;
                        if (t.strokeStyle = this.options.theme.drawXY.strokeStyle,
                            0 == c || 3 == c ? (t.moveTo(r(d), r(u)),
                                t.lineTo(r(m), r(f)),
                                t.stroke()) : (t.strokeStyle = this.options.theme.drawXY.dashed,
                                s(t, d, Math.round(u) + .5, m, Math.round(f) + .5, 5)),
                        c >= 0) {
                            t.save(),
                                t.fillStyle = this.options.theme.drawXY.textColor,
                                t.strokeStyle = this.options.theme.drawXY.strokeStyle,
                                t.font = "12px Arial,Helvetica,San-serif",
                                t.textBaseline = "middle";
                            var v = a.formatYNum(i.v_max / 3 * (3 - c), o);
                            0 == i.v_max && (v = "-");
                            var g = d - t.measureText(v).width - 5;
                            t.fillText(v, g, u),
                                t.restore()
                        }
                    }
                    t.beginPath();
                    for (var y = this.options.drawWidth, A = this.options.c_v_height, x = y / this.options.x_sepe_num, c = 0; c <= this.options.x_sepe_num; c++) {
                        var d = this.options.padding.left + c * x
                            , u = this.options.c2_y_top
                            , m = this.options.padding.left + c * x
                            , f = this.options.c2_y_top + A;
                        0 != c && c != this.options.x_sepe_num && (t.strokeStyle = this.options.theme.drawXY.dashed,
                            s(t, d, Math.round(u), m, Math.round(f), 3))
                    }
                    t.stroke(),
                        t.beginPath(),
                        t.restore()
                }
                ,
                t.prototype.drawXYK = function() {
                    var t = this.options.context
                        , e = this.options.currentData || this.options.data
                        , n = e.max
                        , p = e.min
                        , h = e.pricedigit
                        , l = 9
                        , c = this.options.c_k_height
                        , d = o(n / 1, p / 1, l, c);
                    t.save();
                    var l = d.length;
                    t.fillStyle = this.options.theme.drawXY.fillStyle,
                        t.strokeStyle = this.options.theme.drawXY.strokeStyle,
                        t.font = "12px Arial,Helvetica,San-serif",
                        t.textBaseline = "middle";
                    for (var u, m = 0; u = d[m]; m++) {
                        t.beginPath(),
                            0 == m || m == d.length - 1 ? (t.strokeStyle = this.options.theme.drawXY.strokeStyle,
                                t.moveTo(r(this.options.padding.left), r(u.y)),
                                t.lineTo(r(t.canvas.width - this.options.padding.right), r(u.y)),
                                t.stroke()) : (t.strokeStyle = this.options.theme.drawXY.dashed,
                                s(t, this.options.padding.left, Math.round(u.y), t.canvas.width - this.options.padding.right, Math.round(u.y), 5)),
                            t.moveTo(.5, r(u.y + 5));
                        var f = a.formatYNum(u.num / 1, h);
                        t.fillStyle = this.options.theme.drawXY.textColor,
                        0 == n && 0 == p && (f = "-"),
                            t.fillText(f, this.options.padding.left - 5 - t.measureText(f).width, u.y + 1)
                    }
                    var c = this.options.c_k_height;
                    i.apply(this, [t, c]),
                        t.restore()
                }
                ,
                t
        }();
        t.exports = p
    }
    , function(t, i, o) {
        function e(t, i) {
            var o = r[r.self].host_k;
            t.host && (o = t.host);
            var e = "fsData" + (new Date).getTime().toString();
            "dk" == t.type.toLowerCase() && (t.type = "k");
            var p = {
                id: t.code,
                TYPE: t.type,
                js: e + "((x))",
                rtntype: 5,
                isCR: !1
            };
            "" !== t.authorityType && "undefined" != t.authorityType && (p.authorityType = t.authorityType),
                n(o, p, e, function(o) {
                    var e = {};
                    o.info ? (e = s(o, p.extend),
                        i(e)) : a(t)
                })
        }
        var n = o(14)
            , s = o(105)
            , a = o(65)
            , r = o(67);
        t.exports = e
    }
    , function(t) {
        function i(t) {
            var i = {}
                , a = t.info || {};
            i.data = [],
                i.max = 0,
                i.min = t.info.yc,
                i.v_max = 0,
                i.total = t.info.total,
                i.name = t.name,
                i.code = t.code,
                i.pricedigit = (t.info.pricedigit.split(".")[1] || "").length;
            for (var r = t.data, p = t.data.length, h = p - 1; h >= 0; h--) {
                var l, c = (r[i.total - h - 1].split(/\[|\]/),
                    r[i.total - h - 1].split(/\[|\]/)[0].split(",")), d = r[i.total - h - 1].split(","), u = d.length;
                l = r[i.total - h - 2] ? 1 * r[i.total - h - 2].split(/\[|\]/)[0].split(",")[2] : a.yc || c[2],
                "2017-09-19" == c[0];
                var m = {};
                m.date_time = c[0],
                    m.highest = c[3],
                    m.lowest = c[4],
                    m.open = c[1],
                    m.close = c[2],
                    m.volume = c[5],
                    m.amplitude = d[u - 1],
                    m.volumeMoney = c[6] ? c[6] : "--",
                    m.priceChange = Math.abs(m.close - l).toFixed(i.pricedigit),
                    m.percent = 0 === l ? 0 : (m.priceChange / l * 100).toFixed(2),
                h == p - 1 && (m.priceChange = 0,
                    m.percent = 0),
                    m.up = 1 * m.close - 1 * m.open >= 0 ? !0 : !1;
                var f = e(r, i.total - h - 1, 5)
                    , v = e(r, i.total - h - 1, 10);
                o.call(i, "v_ma_5", f, m.date_time),
                    o.call(i, "v_ma_10", v, m.date_time),
                    i.data.push(m),
                    i.max = n([i.max, m.lowest, 1 * m.highest, c[1], c[2], c[3], c[4]]),
                    i.min = s([i.min, m.lowest, 1 * m.highest, c[1], c[2], c[3], c[4]]),
                    i.v_max = i.v_max > 1 * m.volume ? i.v_max : m.volume
            }
            return i.v_ma_5 || (i.v_ma_5 = []),
            i.v_ma_10 || (i.v_ma_10 = []),
                i
        }
        function o(t, i, o) {
            "-" === i && (i = null),
                void 0 === this[t] ? this[t] = [{
                    value: i,
                    date: o
                }] : this[t].push({
                    value: i,
                    date: o
                })
        }
        function e(t, i, o) {
            var e = 0;
            if (o > i)
                return "-";
            for (var n = 0; o > n; n++)
                e += 1 * t[i - n].split(",")[5];
            return e /= o,
                e.toFixed(2)
        }
        function n(t) {
            for (var i = [], o = 0; o < t.length; o++)
                isNaN(t[o]) || i.push(parseFloat(t[o]));
            return i.sort(function(t, i) {
                return i - t
            }),
                i[0]
        }
        function s(t) {
            for (var i = [], o = 0; o < t.length; o++)
                isNaN(t[o]) || i.push(parseFloat(t[o]));
            return i.sort(function(t, i) {
                return t - i
            }),
                i[0]
        }
        t.exports = i
    }
    , function(t, i, o) {
        function e(t, i) {
            var o = a[a.self].host_k;
            t.host && (o = t.host);
            var e = "fsDataTeac" + t.extend.substring(0, 2) + (new Date).getTime().toString()
                , r = {
                id: t.code,
                TYPE: t.type || "k",
                js: e + "((x))",
                rtntype: 5,
                extend: t.extend || "RSI|MA",
                isCR: !1,
                check: "kte"
            };
            "" != t.authorityType && "undefined" != t.authorityType && (r.authorityType = t.authorityType),
                s(o, r, e, function(t) {
                    var o = n(t, r.extend);
                    i(o)
                })
        }
        var n = o(107)
            , s = o(14)
            , a = o(67);
        t.exports = e
    }
    , function(t) {
        function i(t, i) {
            var e = {};
            e.name = t.name,
                e.code = t.code,
                e.pricedigit = (t.info.pricedigit.split(".")[1] || "").length;
            for (var n = t.data, s = t.info.total, a = s - 1; a >= 0; a--) {
                for (var r = n[t.info.total - a - 1].split(/\[|\]/), p = [], h = 0; h < r.length; h++)
                    "," !== r[h] && p.push(r[h]);
                for (var l = n[t.info.total - a - 1].split(/\[|\]/)[0].split(",")[0], c = i.split("|"), d = e.pricedigit, u = 0; u < c.length; u++) {
                    var m = p[u + 1].split(",");
                    switch (c[u].toLowerCase().split(",")[0]) {
                        case "bbi":
                            o.call(e, "bbi", m[0], l, d);
                            break;
                        case "expma":
                            o.call(e, "expma12", m[0], l, d),
                                o.call(e, "expma50", m[1], l, d);
                        case "sar":
                            o.call(e, "sar", m[0], l, d);
                            break;
                        case "boll":
                            o.call(e, "bollmb", m[0], l, d),
                                o.call(e, "bollup", m[1], l, d),
                                o.call(e, "bolldn", m[2], l, d);
                            break;
                        case "cma":
                            o.call(e, "five_average", m[0], l, d),
                                o.call(e, "ten_average", m[1], l, d),
                                o.call(e, "twenty_average", m[2], l, d),
                                o.call(e, "thirty_average", m[3], l, d);
                            break;
                        case "rsi":
                            o.call(e, "rsi6", m[0], l, d),
                                o.call(e, "rsi12", m[1], l, d),
                                o.call(e, "rsi24", m[2], l, d);
                            break;
                        case "kdj":
                            o.call(e, "k", m[0], l, d),
                                o.call(e, "d", m[1], l, d),
                                o.call(e, "j", m[2], l, d);
                            break;
                        case "macd":
                            o.call(e, "diff", m[0], l, d),
                                o.call(e, "dea", m[1], l, d),
                                o.call(e, "macd", m[2], l, d);
                            break;
                        case "wr":
                            o.call(e, "wr10", m[0], l, d),
                                o.call(e, "wr6", m[1], l, d);
                            break;
                        case "dmi":
                            o.call(e, "pdi", m[0], l, d),
                                o.call(e, "mdi", m[1], l, d),
                                o.call(e, "adx", m[2], l, d),
                                o.call(e, "adxr", m[3], l, d);
                            break;
                        case "bias":
                            o.call(e, "bias6", m[0], l, d),
                                o.call(e, "bias12", m[1], l, d),
                                o.call(e, "bias24", m[2], l, d);
                            break;
                        case "obv":
                            o.call(e, "obv", m[0], l, d),
                                o.call(e, "maobv", m[1], l, d);
                            break;
                        case "cci":
                            o.call(e, "cci", m[0], l, d);
                            break;
                        case "roc":
                            o.call(e, "roc", m[0], l, d),
                                o.call(e, "rocma", m[1], l, d)
                    }
                }
            }
            return e.five_average = e.five_average || [],
                e.ten_average = e.ten_average || [],
                e.twenty_average = e.twenty_average || [],
                e.thirty_average = e.thirty_average || [],
                e
        }
        function o(t, i, o, e) {
            "-" === i && (i = null),
                void 0 === this[t] ? this[t] = [{
                    value: (1 * i).toFixed(e),
                    date: o
                }] : this[t].push({
                    value: (1 * i).toFixed(e),
                    date: o
                })
        }
        t.exports = i
    }
    , function(t, i, o) {
        function e(t) {
            var i = t.ctx;
            i.save(),
                i.beginPath(),
                i.lineWidth = 1;
            var o = t.x
                , e = t.y_open
                , s = t.y_close
                , a = t.y_highest
                , r = t.y_lowest
                , p = t.bar_w;
            i.moveTo(n.format(o), r),
                i.lineTo(n.format(o), a),
                i.stroke(),
                i.beginPath(),
                s >= e ? i.rect(n.format(t.x - p / 2), n.format(e), p, s - e) : i.rect(n.format(t.x - p / 2), n.format(s), p, e - s),
                i.stroke(),
                i.fill(),
                i.restore()
        }
        var n = o(109);
        t.exports = e
    }
    , function(t) {
        t.exports = {
            format: function(t) {
                var i = Math.round(t);
                return t > i ? i + .5 : i - .5
            }
        }
    }
    , function(t) {
        var i = function(t, i) {
            this.ops = t,
                this.container = i,
                this.dps = {},
            this.ops.dotPoint && this.init()
        };
        i.prototype.init = function() {
            this.createDom(),
                this.formatDotPoints()
        }
            ,
            i.prototype.createDom = function() {
                var t = this.ops
                    , i = (t.padding.left,
                    t.padding.right,
                    t.c1_y_top,
                    document.createElement("div"));
                i.className = "__Emcharts_k_dotpints",
                    this.div = i,
                    this.container.appendChild(i)
            }
            ,
            i.prototype.delDom = function() {
                this.div && (this.div.innerHTML = "")
            }
            ,
            i.prototype.formatDotPoints = function() {
                for (var t = {}, i = this.ops.dotPoint, o = 0; o < i.length; o++) {
                    var e = i[o].date;
                    t[e] ? t[e].push(i[o]) : (t[e] = [],
                        t[e].push(i[o]))
                }
                this.fpoints = t
            }
            ,
            i.prototype.dot = function(t, i) {
                var o = (this.ops.dotPoint.position,
                    this.fpoints)
                    , e = i.date_time
                    , n = this.ops.c1_y_top
                    , s = this.ops.c_k_height
                    , a = o[e];
                if (a) {
                    var r = t.x
                        , p = t.y_open
                        , h = t.y_close
                        , l = t.y_highest
                        , c = t.y_lowest
                        , d = (t.bar_w,
                        [p, h, l, c]);
                    d.sort(function(t, i) {
                        return t - i
                    });
                    for (var u = d[0], m = d[3], f = 0; f < a.length; f++) {
                        var v, g = a[f].pos;
                        v = "top" == g || "0%" == g ? n + s - 5 - 12 : "up" == g ? u + n - 15 - 12 * f : "down" == g ? m + n + 5 + 12 * f : "bottom" == g || "100%" == g ? n + 5 : "25%" == g ? n + .75 * s : "50%" == g ? n + .5 * s : "75%" == g ? n + .25 * s : m + n + 5 + 12 * f;
                        var y = document.createElement("a");
                        y.style.left = r - 5 + "px",
                            y.style.top = v + "px",
                            y.href = a[f].link,
                            y.target = "_blank";
                        var A = document.createElement("img");
                        A.src = a[f].img,
                            y.appendChild(A),
                            this.div.appendChild(y)
                    }
                }
            }
            ,
            t.exports = i
    }
    , function(t, i, o) {
        function e(t, i, o, e) {
            this.clearT(),
                this.options.drawXY.drawXYT(),
                t.save();
            var s = i.concat(o).concat(e)
                , a = s.length
                , r = 0
                , p = 0;
            s && s[0] && (r = s[0].value,
                p = s[0].value);
            for (var h = 0; a > h; h++)
                r = Math.max(r, s[h].value),
                    p = Math.min(p, s[h].value);
            var l = r - p
                , c = this.options.c_t_height
                , d = i.length
                , u = o.length
                , m = e.length
                , f = this.options.drawWidth / d;
            t.beginPath(),
                t.strokeStyle = "#323232";
            for (var h = 0; d > h; h++) {
                var v = c - (i[h].value - p) / l * c + this.options.c3_y_top
                    , g = this.options.padding.left + (h + 1) * f - f / 2;
                0 == h ? t.moveTo(g, v) : t.lineTo(g, v)
            }
            t.stroke(),
                t.beginPath(),
                t.strokeStyle = "#FF00FF";
            for (var h = 0; u > h; h++) {
                var v = c - (o[h].value - p) / l * c + this.options.c3_y_top
                    , g = this.options.padding.left + (h + 1) * f - f / 2;
                0 == h ? t.moveTo(g, v) : t.lineTo(g, v)
            }
            t.stroke(),
                t.beginPath(),
                t.strokeStyle = "#808080";
            for (var h = 0; m > h; h++) {
                var v = c - (e[h].value - p) / l * c + this.options.c3_y_top
                    , g = this.options.padding.left + (h + 1) * f - f / 2;
                0 == h ? t.moveTo(g, v) : t.lineTo(g, v)
            }
            t.stroke();
            var y = (r + p) / 2;
            t.fillStyle = this.options.theme.drawXY.textColor,
                0 == r && 0 == p ? (t.fillText("-", this.options.padding.left - 5 - t.measureText("-").width, this.options.c3_y_top + 5),
                    t.fillText("-", this.options.padding.left - 5 - t.measureText("-").width, this.options.c3_y_top + 5 + c / 2),
                    t.fillText("-", this.options.padding.left - 5 - t.measureText("-").width, this.options.c3_y_top + 5 + c)) : (t.fillText(n.format_unit(r), this.options.padding.left - 5 - t.measureText(n.format_unit(r)).width, this.options.c3_y_top + 5),
                    t.fillText(n.format_unit(y.toFixed(2)), this.options.padding.left - 5 - t.measureText(n.format_unit(y.toFixed(2))).width, this.options.c3_y_top + 5 + c / 2),
                    t.fillText(n.format_unit(p.toFixed(2)), this.options.padding.left - 5 - t.measureText(n.format_unit(p.toFixed(2))).width, this.options.c3_y_top + 5 + c)),
                t.beginPath(),
                t.restore()
        }
        var n = o(60);
        t.exports = e
    }
    , function(t, i, o) {
        function e(t, i, o, e) {
            this.clearT(),
                this.options.drawXY.drawXYT(),
                t.save();
            var s = i.concat(o).concat(e)
                , a = s.length;
            if (s && s[0])
                var r = s[0].value
                    , p = s[0].value;
            for (var h = 0; a > h; h++)
                r = Math.max(r, s[h].value),
                    p = Math.min(p, s[h].value);
            var l = r - p
                , c = this.options.c_t_height
                , d = i.length
                , u = o.length
                , m = e.length
                , f = this.options.drawWidth / d;
            t.beginPath(),
                t.strokeStyle = "#323232";
            for (var h = 0; d > h; h++) {
                var v = c - (i[h].value - p) / l * c + this.options.c3_y_top
                    , g = this.options.padding.left + (h + 1) * f - f / 2;
                0 == h ? t.moveTo(g, v) : t.lineTo(g, v)
            }
            t.stroke(),
                t.beginPath(),
                t.strokeStyle = "#FF00FF";
            for (var h = 0; u > h; h++) {
                var v = c - (o[h].value - p) / l * c + this.options.c3_y_top
                    , g = this.options.padding.left + (h + 1) * f - f / 2;
                0 == h ? t.moveTo(g, v) : t.lineTo(g, v)
            }
            t.stroke(),
                t.beginPath(),
                t.strokeStyle = "#808080";
            for (var h = 0; m > h; h++) {
                var v = c - (e[h].value - p) / l * c + this.options.c3_y_top
                    , g = this.options.padding.left + (h + 1) * f - f / 2;
                0 == h ? t.moveTo(g, v) : t.lineTo(g, v)
            }
            t.stroke();
            var y = (r + p) / 2;
            t.fillStyle = this.options.theme.drawXY.textColor,
                t.fillText(n.format_unit(r), this.options.padding.left - 5 - t.measureText(n.format_unit(r)).width, this.options.c3_y_top + 5),
                t.fillText(n.format_unit(y.toFixed(2)), this.options.padding.left - 5 - t.measureText(n.format_unit(y.toFixed(2))).width, this.options.c3_y_top + 5 + c / 2),
                t.fillText(n.format_unit(p.toFixed(2)), this.options.padding.left - 5 - t.measureText(n.format_unit(p.toFixed(2))).width, this.options.c3_y_top + 5 + c),
                t.beginPath(),
                t.restore()
        }
        var n = o(60);
        t.exports = e
    }
    , function(t, i, o) {
        function e(t, i, o) {
            this.clearT(),
                this.options.drawXY.drawXYT(),
                t.save();
            var e = i.concat(o)
                , s = e.length;
            if (e && e[0])
                var a = e[0].value
                    , r = e[0].value;
            for (var p = 0; s > p; p++)
                a = Math.max(a, e[p].value),
                    r = Math.min(r, e[p].value);
            var h = a - r
                , l = this.options.c_t_height
                , c = i.length
                , d = o.length
                , u = this.options.drawWidth / c;
            t.beginPath(),
                t.strokeStyle = "#323232";
            for (var p = 0; c > p; p++) {
                var m = l - (i[p].value - r) / h * l + this.options.c3_y_top
                    , f = this.options.padding.left + (p + 1) * u - u / 2;
                0 == p ? t.moveTo(f, m) : t.lineTo(f, m)
            }
            t.stroke(),
                t.beginPath(),
                t.strokeStyle = "#FF00FF";
            for (var p = 0; d > p; p++) {
                var m = l - (o[p].value - r) / h * l + this.options.c3_y_top
                    , f = this.options.padding.left + (p + 1) * u - u / 2;
                0 == p ? t.moveTo(f, m) : t.lineTo(f, m)
            }
            t.stroke();
            var v = (a + r) / 2;
            t.fillStyle = this.options.theme.drawXY.textColor,
                t.fillText(n.format_unit(a), this.options.padding.left - 5 - t.measureText(n.format_unit(a)).width, this.options.c3_y_top + 5),
                t.fillText(n.format_unit(v.toFixed(2)), this.options.padding.left - 5 - t.measureText(n.format_unit(v.toFixed(2))).width, this.options.c3_y_top + 5 + l / 2),
                t.fillText(n.format_unit(r.toFixed(2)), this.options.padding.left - 5 - t.measureText(n.format_unit(r.toFixed(2))).width, this.options.c3_y_top + 5 + l),
                t.beginPath(),
                t.restore()
        }
        var n = o(60);
        t.exports = e
    }
    , function(t, i, o) {
        function e(t, i, o, e, s) {
            this.clearT(),
                this.options.drawXY.drawXYT(),
                t.save();
            var a = i.concat(o).concat(e).concat(s)
                , r = a.length;
            if (a && a[0])
                var p = a[0].value
                    , h = a[0].value;
            for (var l = 0; r > l; l++)
                p = Math.max(p, a[l].value),
                    h = Math.min(h, a[l].value);
            var c = p - h
                , d = this.options.c_t_height
                , u = i.length
                , m = o.length
                , f = e.length
                , v = s.length
                , g = this.options.drawWidth / u;
            t.beginPath(),
                t.strokeStyle = "#323232";
            for (var l = 0; u > l; l++) {
                var y = d - (i[l].value - h) / c * d + this.options.c3_y_top
                    , A = this.options.padding.left + (l + 1) * g - g / 2;
                0 == l ? t.moveTo(A, y) : t.lineTo(A, y)
            }
            t.stroke(),
                t.beginPath(),
                t.strokeStyle = "#FF00FF";
            for (var l = 0; m > l; l++) {
                var y = d - (o[l].value - h) / c * d + this.options.c3_y_top
                    , A = this.options.padding.left + (l + 1) * g - g / 2;
                0 == l ? t.moveTo(A, y) : t.lineTo(A, y)
            }
            t.stroke(),
                t.beginPath(),
                t.strokeStyle = "#808080";
            for (var l = 0; f > l; l++) {
                var y = d - (e[l].value - h) / c * d + this.options.c3_y_top
                    , A = this.options.padding.left + (l + 1) * g - g / 2;
                0 == l ? t.moveTo(A, y) : t.lineTo(A, y)
            }
            t.stroke(),
                t.beginPath(),
                t.strokeStyle = "#800000";
            for (var l = 0; v > l; l++) {
                var y = d - (s[l].value - h) / c * d + this.options.c3_y_top
                    , A = this.options.padding.left + (l + 1) * g - g / 2;
                0 == l || y < this.options.c3_y_top || y > this.options.c3_y_top + 2 * this.options.unit_htight ? t.moveTo(A, y) : t.lineTo(A, y)
            }
            t.stroke();
            var x = (p + h) / 2;
            t.fillStyle = this.options.theme.drawXY.textColor,
                t.fillText(n.format_unit(p), this.options.padding.left - 5 - t.measureText(n.format_unit(p)).width, this.options.c3_y_top + 5),
                t.fillText(n.format_unit(x.toFixed(2)), this.options.padding.left - 5 - t.measureText(n.format_unit(x.toFixed(2))).width, this.options.c3_y_top + 5 + d / 2),
                t.fillText(n.format_unit(h.toFixed(2)), this.options.padding.left - 5 - t.measureText(n.format_unit(h.toFixed(2))).width, this.options.c3_y_top + 5 + d),
                t.beginPath(),
                t.restore()
        }
        var n = o(60);
        t.exports = e
    }
    , function(t, i, o) {
        function e(t, i, o, e) {
            this.clearT(),
                this.options.drawXY.drawXYT(),
                t.save();
            var s = i.concat(o).concat(e)
                , a = s.length;
            if (s && s[0])
                var r = s[0].value
                    , p = s[0].value;
            for (var h = 0; a > h; h++)
                r = Math.max(r, s[h].value),
                    p = Math.min(p, s[h].value);
            var l = r - p
                , c = this.options.c_t_height
                , d = i.length
                , u = o.length
                , m = e.length
                , f = this.options.drawWidth / d;
            t.beginPath(),
                t.strokeStyle = "#323232";
            for (var h = 0; d > h; h++) {
                var v = c - (i[h].value - p) / l * c + this.options.c3_y_top
                    , g = this.options.padding.left + (h + 1) * f - f / 2;
                0 == h ? t.moveTo(g, v) : t.lineTo(g, v)
            }
            t.stroke(),
                t.beginPath(),
                t.strokeStyle = "#808080";
            for (var h = 0; u > h; h++) {
                var v = c - (o[h].value - p) / l * c + this.options.c3_y_top
                    , g = this.options.padding.left + (h + 1) * f - f / 2;
                0 == h ? t.moveTo(g, v) : t.lineTo(g, v)
            }
            t.stroke(),
                t.beginPath(),
                t.strokeStyle = "#fe59fe";
            for (var h = 0; m > h; h++) {
                var v = c - (e[h].value - p) / l * c + this.options.c3_y_top
                    , g = this.options.padding.left + (h + 1) * f - f / 2;
                0 == h ? t.moveTo(g, v) : t.lineTo(g, v)
            }
            t.stroke();
            var y = (r + p) / 2;
            t.fillStyle = this.options.theme.drawXY.textColor,
                t.fillText(n.format_unit(r), this.options.padding.left - 5 - t.measureText(n.format_unit(r)).width, this.options.c3_y_top + 5),
                t.fillText(n.format_unit(y.toFixed(2)), this.options.padding.left - 5 - t.measureText(n.format_unit(y.toFixed(2))).width, this.options.c3_y_top + 5 + c / 2),
                t.fillText(n.format_unit(p.toFixed(2)), this.options.padding.left - 5 - t.measureText(n.format_unit(p.toFixed(2))).width, this.options.c3_y_top + 5 + c),
                t.beginPath(),
                t.restore()
        }
        var n = o(60);
        t.exports = e
    }
    , function(t, i, o) {
        function e(t, i, o) {
            this.clearT(),
                this.options.drawXY.drawXYT(),
                t.save();
            var e = i.concat(o)
                , s = e.length;
            if (e && e[0])
                var a = e[0].value
                    , r = e[0].value;
            for (var p = 0; s > p; p++)
                a = Math.max(a, e[p].value),
                    r = Math.min(r, e[p].value);
            var h = a - r
                , l = this.options.c_t_height
                , c = i.length
                , d = o.length
                , u = this.options.drawWidth / c;
            t.beginPath(),
                t.strokeStyle = "#323232";
            for (var p = 0; c > p; p++) {
                var m = l - (i[p].value - r) / h * l + this.options.c3_y_top
                    , f = this.options.padding.left + (p + 1) * u - u / 2;
                0 == p ? t.moveTo(f, m) : t.lineTo(f, m)
            }
            t.stroke(),
                t.beginPath(),
                t.strokeStyle = "#FF00FF";
            for (var p = 0; d > p; p++) {
                var m = l - (o[p].value - r) / h * l + this.options.c3_y_top
                    , f = this.options.padding.left + (p + 1) * u - u / 2;
                0 == p ? t.moveTo(f, m) : t.lineTo(f, m)
            }
            t.stroke();
            var v = (a + r) / 2;
            t.fillStyle = this.options.theme.drawXY.textColor,
                t.fillText(n.format_unit(a), this.options.padding.left - 5 - t.measureText(n.format_unit(a)).width, this.options.c3_y_top + 5),
                t.fillText(n.format_unit(v.toFixed(2)), this.options.padding.left - 5 - t.measureText(n.format_unit(v.toFixed(2))).width, this.options.c3_y_top + 5 + l / 2),
                t.fillText(n.format_unit(r.toFixed(2)), this.options.padding.left - 5 - t.measureText(n.format_unit(r.toFixed(2))).width, this.options.c3_y_top + 5 + l),
                t.beginPath(),
                t.restore()
        }
        var n = o(60);
        t.exports = e
    }
    , function(t, i, o) {
        function e(t, i) {
            this.clearT(),
                this.options.drawXY.drawXYT(),
                t.save();
            var o = i
                , e = i.length;
            if (o && o[0])
                var s = o[0].value
                    , a = o[0].value;
            for (var r = 0; e > r; r++)
                s = Math.max(s, o[r].value),
                    a = Math.min(a, o[r].value);
            var p = s - a
                , h = this.options.c_t_height
                , l = i.length
                , c = this.options.drawWidth / l;
            t.beginPath(),
                t.strokeStyle = "#323232";
            for (var r = 0; l > r; r++) {
                var d = h - (i[r].value - a) / p * h + this.options.c3_y_top
                    , u = this.options.padding.left + (r + 1) * c - c / 2;
                0 == r ? t.moveTo(u, d) : t.lineTo(u, d)
            }
            t.stroke();
            var m = (s + a) / 2;
            t.fillStyle = this.options.theme.drawXY.textColor,
                t.fillText(n.format_unit(s), this.options.padding.left - 5 - t.measureText(n.format_unit(s)).width, this.options.c3_y_top + 5),
                t.fillText(n.format_unit(m.toFixed(2)), this.options.padding.left - 5 - t.measureText(n.format_unit(m.toFixed(2))).width, this.options.c3_y_top + 5 + h / 2),
                t.fillText(n.format_unit(a.toFixed(2)), this.options.padding.left - 5 - t.measureText(n.format_unit(a.toFixed(2))).width, this.options.c3_y_top + 5 + h),
                t.beginPath(),
                t.restore()
        }
        var n = o(60);
        t.exports = e
    }
    , function(t, i, o) {
        function e(t, i, o) {
            this.clearT(),
                this.options.drawXY.drawXYT(),
                t.save();
            var e = i.concat(o)
                , s = e.length;
            if (e && e[0])
                var a = e[0].value
                    , r = e[0].value;
            for (var p = 0; s > p; p++)
                a = Math.max(a, e[p].value),
                    r = Math.min(r, e[p].value);
            var h = a - r
                , l = this.options.c_t_height
                , c = i.length
                , d = o.length
                , u = this.options.drawWidth / c;
            t.beginPath(),
                t.strokeStyle = "#323232";
            for (var p = 0; c > p; p++) {
                var m = l - (i[p].value - r) / h * l + this.options.c3_y_top
                    , f = this.options.padding.left + (p + 1) * u - u / 2;
                0 == p ? t.moveTo(f, m) : t.lineTo(f, m)
            }
            t.stroke(),
                t.beginPath(),
                t.strokeStyle = "#FF00FF";
            for (var p = 0; d > p; p++) {
                var m = l - (o[p].value - r) / h * l + this.options.c3_y_top
                    , f = this.options.padding.left + (p + 1) * u - u / 2;
                0 == p ? t.moveTo(f, m) : t.lineTo(f, m)
            }
            t.stroke();
            var v = (a + r) / 2;
            t.fillStyle = this.options.theme.drawXY.textColor,
                t.fillText(n.format_unit(a), this.options.padding.left - 5 - t.measureText(n.format_unit(a)).width, this.options.c3_y_top + 5),
                t.fillText(n.format_unit(v.toFixed(2)), this.options.padding.left - 5 - t.measureText(n.format_unit(v.toFixed(2))).width, this.options.c3_y_top + 5 + l / 2),
                t.fillText(n.format_unit(r.toFixed(2)), this.options.padding.left - 5 - t.measureText(n.format_unit(r.toFixed(2))).width, this.options.c3_y_top + 5 + l),
                t.beginPath(),
                t.restore()
        }
        var n = o(60);
        t.exports = e
    }
    , function(t, i, o) {
        function e(t, i, o) {
            t.save(),
                this.clearK(),
                this.options.drawXY.drawXYK(),
                this.drawK();
            var e = i.length
                , s = o.length
                , a = this.options.drawWidth / e;
            t.beginPath(),
                t.strokeStyle = "rgb(244, 203, 21)";
            for (var r = !1, p = 0; e > p; p++) {
                var h = this.options.padding.left + p * a + a / 2
                    , l = n.get_y.call(this, i[p].value);
                0 == p ? t.moveTo(h, l) : 0 > l || l > this.options.c_k_height ? (t.moveTo(h, l),
                    r = !0) : r ? (t.moveTo(h, l),
                    r = !1) : t.lineTo(h, l)
            }
            t.stroke(),
                t.beginPath(),
                t.strokeStyle = "rgb(255, 91, 16)",
                r = !1;
            for (var p = 0; s > p; p++) {
                var h = this.options.padding.left + p * a + a / 2
                    , l = n.get_y.call(this, o[p].value);
                0 == p ? t.moveTo(h, l) : 0 > l || l > this.options.c_k_height ? (t.moveTo(h, l),
                    r = !0) : r ? (t.moveTo(h, l),
                    r = !1) : t.lineTo(h, l)
            }
            t.stroke(),
                t.beginPath(),
                t.restore()
        }
        var n = o(60);
        t.exports = e
    }
    , function(t, i, o) {
        function e(t, i, o) {
            t.save(),
                this.clearK(),
                this.options.drawXY.drawXYK(),
                this.drawK();
            for (var e = i.length, s = this.options.drawWidth / e, a = this.options.up_color, r = this.options.down_color, p = 0; e > p; p++) {
                t.beginPath();
                {
                    o[p].up
                }
                p == e - 1 ? i[p].value > i[p - 1].value ? (t.fillStyle = a,
                    t.strokeStyle = a) : (t.fillStyle = r,
                    t.strokeStyle = r) : i[p + 1].value > i[p].value ? (t.fillStyle = a,
                    t.strokeStyle = a) : (t.fillStyle = r,
                    t.strokeStyle = r);
                var h = this.options.padding.left + p * s + s / 2
                    , l = n.get_y.call(this, i[p].value);
                l <= this.options.c_k_height && l >= 0 && (t.moveTo(h, l),
                    t.arc(h, l, 3, 0, 2 * Math.PI, !0),
                    t.fill(),
                    t.stroke())
            }
            t.beginPath(),
                t.restore()
        }
        var n = o(60);
        t.exports = e
    }
    , function(t, i, o) {
        function e(t, i, o, e) {
            t.save(),
                this.clearK(),
                this.options.drawXY.drawXYK(),
                this.drawK();
            var s = i.length
                , a = o.length
                , r = e.length
                , p = this.options.drawWidth / s;
            t.beginPath(),
                t.strokeStyle = "rgb(244, 203, 21)";
            for (var h = !1, l = 0; s > l; l++) {
                var c = this.options.padding.left + l * p + p / 2
                    , d = n.get_y.call(this, i[l].value);
                0 == l ? t.moveTo(c, d) : 0 > d || d > this.options.c_k_height ? (t.moveTo(c, d),
                    h = !0) : h ? (t.moveTo(c, d),
                    h = !1) : t.lineTo(c, d)
            }
            t.stroke(),
                t.beginPath(),
                t.strokeStyle = "rgb(255, 91, 16)",
                h = !1;
            for (var l = 0; a > l; l++) {
                var c = this.options.padding.left + l * p + p / 2
                    , d = n.get_y.call(this, o[l].value);
                0 == l ? t.moveTo(c, d) : 0 > d || d > this.options.c_k_height ? (t.moveTo(c, d),
                    h = !0) : h ? (t.moveTo(c, d),
                    h = !1) : t.lineTo(c, d)
            }
            t.stroke(),
                t.beginPath(),
                t.strokeStyle = "rgb(72, 142, 230)",
                h = !1;
            for (var l = 0; r > l; l++) {
                var c = this.options.padding.left + l * p + p / 2
                    , d = n.get_y.call(this, e[l].value);
                0 == l ? t.moveTo(c, d) : 0 > d || d > this.options.c_k_height ? (t.moveTo(c, d),
                    h = !0) : h ? (t.moveTo(c, d),
                    h = !1) : t.lineTo(c, d)
            }
            t.stroke(),
                t.beginPath(),
                t.restore()
        }
        var n = o(60);
        t.exports = e
    }
    , function(t, i, o) {
        function e(t, i) {
            t.save(),
                this.clearK(),
                this.options.drawXY.drawXYK(),
                this.drawK();
            var o = i.length
                , e = this.options.drawWidth / o;
            t.beginPath(),
                t.strokeStyle = "rgb(244, 203, 21)";
            for (var s = !1, a = 0; o > a; a++) {
                var r = this.options.padding.left + a * e + e / 2
                    , p = n.get_y.call(this, i[a].value);
                0 == a ? t.moveTo(r, p) : p > this.options.c_k_height || 0 > p ? (t.moveTo(r, p),
                    s = !0) : s ? (t.moveTo(r, p),
                    s = !1) : t.lineTo(r, p)
            }
            t.stroke(),
                t.beginPath(),
                t.restore()
        }
        var n = o(60);
        t.exports = e
    }
    , function(t, i, o) {
        function e(t, i, o, e) {
            this.clearT(),
                this.options.drawXY.drawXYT(),
                t.save();
            var s = i.concat(o).concat(e)
                , a = s.length;
            if (s && s[0])
                var r = s[0].value
                    , p = s[0].value;
            for (var h = 0; a > h; h++)
                r = Math.max(r, s[h].value),
                    p = Math.min(p, s[h].value);
            var l = (r - p) / 2
                , c = (r + p) / 2
                , d = this.options.c3_y_top + this.options.c_t_height / 2
                , u = this.options.c_t_height
                , m = i.length
                , f = o.length
                , v = e.length
                , g = this.options.drawWidth / m;
            t.beginPath(),
                t.strokeStyle = "#323232";
            for (var h = 0; m > h; h++) {
                if (i[h].value >= 0)
                    var y = u / 2 - (i[h].value - c) / l * u / 2 + this.options.c3_y_top;
                else
                    var y = (c - i[h].value) / l * u / 2 + this.options.c3_y_top + u / 2;
                var A = this.options.padding.left + (h + 1) * g - g / 2;
                0 == h || y > this.options.c3_y_top + this.options.c_t_height || y < this.options.c3_y_top ? t.moveTo(A, y) : t.lineTo(A, y)
            }
            t.stroke(),
                t.beginPath(),
                t.strokeStyle = "#FF00FF";
            for (var h = 0; f > h; h++) {
                if (i[h].value >= 0)
                    var y = u / 2 - (o[h].value - c) / l * u / 2 + this.options.c3_y_top;
                else
                    var y = (c - o[h].value) / l * u / 2 + this.options.c3_y_top + u / 2;
                var A = this.options.padding.left + (h + 1) * g - g / 2;
                0 == h || y > this.options.c3_y_top + this.options.c_t_height || y < this.options.c3_y_top ? t.moveTo(A, y) : t.lineTo(A, y)
            }
            t.stroke();
            for (var h = 0; v > h; h++) {
                t.beginPath();
                var A = this.options.padding.left + (h + 1) * g - g / 2;
                if (e[h].value >= c) {
                    t.strokeStyle = "#ff0000",
                        t.fillStyle = "#ff0000";
                    var y = u / 2 - (e[h].value - c) / l * u / 2 + this.options.c3_y_top;
                    t.rect(A - .6 * g / 2, y, .6 * g, Math.abs(y - d)),
                        t.fill(),
                        t.stroke()
                } else {
                    t.strokeStyle = "#17b03e",
                        t.fillStyle = "#17b03e";
                    var y = (c - o[h].value) / l * u / 2 + this.options.c3_y_top + u / 2;
                    t.rect(A - .6 * g / 2, d, .6 * g, Math.abs(y - d)),
                        t.fill(),
                        t.stroke()
                }
            }
            var c = (r + p) / 2;
            t.fillStyle = this.options.theme.drawXY.textColor,
                t.fillText(n.format_unit(r), this.options.padding.left - 5 - t.measureText(n.format_unit(r)).width, this.options.c3_y_top + 5),
                t.fillText(n.format_unit(c.toFixed(2)), this.options.padding.left - 5 - t.measureText(n.format_unit(c.toFixed(2))).width, this.options.c3_y_top + 5 + u / 2),
                t.fillText(n.format_unit(p.toFixed(2)), this.options.padding.left - 5 - t.measureText(n.format_unit(p.toFixed(2))).width, this.options.c3_y_top + 5 + u),
                t.beginPath(),
                t.beginPath(),
                t.restore()
        }
        var n = o(60);
        t.exports = e
    }
    , function(t, i, o) {
        var e = o(21)
            , n = o(60)
            , s = o(7)
            , a = (o(70),
            function() {
                function t(t) {
                    this.defaultoptions = s.interactive,
                        this.options = {},
                        this.options = e(this.options, this.defaultoptions, t)
                }
                return t.prototype.cross = function(t, i, e, n, s, a, r) {
                    this.options.dpr;
                    if (!this.options.cross) {
                        this.options.cross = {};
                        var p = document.createElement("div");
                        p.className = "cross-y",
                            p.style.height = this.options.c4_y_top - this.options.c1_y_top + "px",
                            p.style.top = this.options.c1_y_top + "px",
                            this.options.cross.y_line = p;
                        var h = document.createElement("div");
                        h.className = "cross-x",
                            h.style.width = this.options.drawWidth + "px",
                            this.options.cross.x_line = h;
                        var l = document.createElement("div");
                        l.className = "cross-p",
                            l.style.width = "11px",
                            l.style.height = "11px",
                            this.options.point_width = 11,
                            l.style.borderRadius = l.style.width,
                            l.style.background = "url(" + o(71) + ")",
                            this.options.cross.point = l;
                        var c = document.createDocumentFragment();
                        c.appendChild(h),
                            c.appendChild(p),
                            c.appendChild(l),
                            document.getElementById(this.options.container).appendChild(c)
                    }
                    var p = this.options.cross.y_line
                        , d = 0;
                    n >= r ? d = r : a >= n ? d = a : e > s ? n >= e && r >= n ? d = e : n >= s && e > n ? d = s : n >= a && s > n && (d = a) : n >= s && r >= n ? d = s : n >= e && s > n ? d = e : n >= a && e > n && (d = a),
                    p && (p.style.left = i + "px");
                    var h = this.options.cross.x_line;
                    h && (h.style.top = d + this.options.margin.top + "px",
                        h.style.left = this.options.padding.left + "px");
                    var l = this.options.cross.point;
                    if (l) {
                        var u = this.options.point_width;
                        l.style.left = i - u / 2 + "px",
                            l.style.top = d + this.options.margin.top - u / 2 + "px"
                    }
                }
                    ,
                    t.prototype.firstMarkMA = function(t, i, o, e, n) {
                        var s = e
                            , a = o;
                        this.options.markMAContainer && this.options.markMAContainer.remove(),
                            this.options.markMAContainer = document.createElement("div");
                        var r = this.options.markMAContainer;
                        r.innerHTML = "",
                            r.className = "markTContainer",
                            r.style.top = "3px",
                            r.style.left = this.options.padding.left + 10 + "px";
                        var p = document.createDocumentFragment();
                        for (m = 0; m < a.length; m++)
                            if (0 == m) {
                                var h = document.createElement("span");
                                h.innerHTML = n,
                                    p.appendChild(h);
                                {
                                    var l = document.createElement("span");
                                    a[m].value.length - 1
                                }
                                l.innerHTML = a[m].name.toUpperCase() + ": " + (null == a[m].value ? "-" : a[m].value),
                                    l.style.color = s[m],
                                    p.appendChild(l),
                                    document.getElementById(this.options.container).appendChild(r)
                            } else {
                                {
                                    var l = document.createElement("span");
                                    a[m].value.length - 1
                                }
                                l.innerHTML = a[m].name.toUpperCase() + ": " + (null == a[m].value ? "-" : a[m].value),
                                    l.style.color = s[m],
                                    p.appendChild(l)
                            }
                        for (var c = this.options.width - this.options.padding_left - 20, d = this.options.context, u = "", m = 0, f = p.childNodes.length; f > m; m++)
                            u += p.childNodes[m].innerText;
                        if (c > d.measureText(u).width)
                            r.appendChild(p);
                        else {
                            for (var m = 1, f = p.childNodes.length; f > m; m++)
                                p.childNodes[m].innerText = "";
                            r.appendChild(p)
                        }
                        this.options[i] = {},
                            this.options[i].defaultMaHtml = r.innerHTML,
                            document.getElementById(this.options.container).appendChild(r)
                    }
                    ,
                    t.prototype.markMA = function(t, i, o, e, n, s, a, r) {
                        var p = a
                            , h = []
                            , l = 0;
                        for (var c in o)
                            h.push({
                                value: o[c].slice(e, n),
                                name: c
                            });
                        if (0 != h.length)
                            if (this.options.markMAContainer && i == this.options.markUPTType) {
                                var d = this.options.markMAContainer
                                    , u = d.children;
                                if (s || "junxian" != i) {
                                    var m = this.options.width - this.options.padding_left - 20
                                        , f = this.options.context;
                                    if (0 == u.length) {
                                        var v = document.createDocumentFragment();
                                        for (l = 0; l < h.length; l++)
                                            if (0 == l) {
                                                var g = document.createElement("span");
                                                g.innerHTML = r,
                                                    v.appendChild(g);
                                                var y = document.createElement("span")
                                                    , A = h[l].value.length - 1;
                                                y.innerHTML = h[l].name.toUpperCase() + ": " + (null == h[l].value[A].value ? "-" : h[l].value[A].value),
                                                    y.style.color = p[l],
                                                    v.appendChild(y)
                                            } else {
                                                var y = document.createElement("span")
                                                    , A = h[l].value.length - 1;
                                                y.innerHTML = h[l].name.toUpperCase() + ": " + (null == h[l].value[A].value ? "-" : h[l].value[A].value),
                                                    y.style.color = p[l],
                                                    v.appendChild(y)
                                            }
                                        var x = "";
                                        if (v.childNodes.forEach(function(t) {
                                            x += t.innerText
                                        }),
                                        m > f.measureText(x).width)
                                            d.appendChild(v);
                                        else {
                                            for (var l = 1, w = v.childNodes.length; w > l; l++)
                                                v.childNodes[l].innerText = "";
                                            d.appendChild(v)
                                        }
                                    } else {
                                        u[0].innerHTML = r;
                                        for (var l = 0, l = 0; l < h.length; l++) {
                                            var y = u[l + 1];
                                            try {
                                                y.innerHTML = h[l].name.toUpperCase() + ": " + (null == h[l].value[s].value ? "-" : h[l].value[s].value)
                                            } catch (_) {
                                                if (s)
                                                    if (null == h[l].value[s].value || void 0 == h[l].value[s].value)
                                                        y.innerText = h[l].name.toUpperCase() + ": -";
                                                    else {
                                                        var y = document.createElement("span");
                                                        y.innerHTML = h[l].name.toUpperCase() + ": " + h[l].value[s].value,
                                                            y.style.color = p[l],
                                                            d.appendChild(y)
                                                    }
                                                else
                                                    ;
                                            }
                                        }
                                        for (var x = "", b = 0; b < u.length; b++)
                                            x += u[b].innerText;
                                        if (m > f.measureText(x).width)
                                            for (var b = 1; b < u.length; b++)
                                                u[b].style.display = "inline";
                                        else
                                            for (var b = 1; b < u.length; b++)
                                                u[b].style.display = "none";
                                        for (var b = l + 1; b < u.length; b++)
                                            u[b].style.display = "none"
                                    }
                                } else {
                                    d.innerHTML = this.options[this.options.markUPTType].defaultMaHtml;
                                    for (var l = 0; l < h.length; l++) {
                                        var y = u[l + 1];
                                        y.style.color = p[l]
                                    }
                                    this.options[i].defaultMaHtml = d.innerHTML
                                }
                            } else {
                                this.options.markUPTType = i,
                                this.options.markMAContainer || (this.options.markMAContainer = document.createElement("div"));
                                var d = this.options.markMAContainer;
                                d.innerHTML = "",
                                    d.className = "markTContainer",
                                    d.style.top = "3px",
                                    d.style.left = this.options.padding.left + 10 + "px";
                                var v = document.createDocumentFragment();
                                for (l = 0; l < h.length; l++)
                                    if (0 == l) {
                                        var g = document.createElement("span");
                                        g.innerHTML = r,
                                            v.appendChild(g);
                                        var y = document.createElement("span")
                                            , A = h[l].value.length - 1;
                                        y.innerHTML = A >= 0 ? h[l].name.toUpperCase() + ": " + (null == h[l].value[A].value ? "-" : h[l].value[A].value) : h[l].name.toUpperCase() + ": -",
                                            y.style.color = p[l],
                                            v.appendChild(y),
                                            document.getElementById(this.options.container).appendChild(d)
                                    } else {
                                        var y = document.createElement("span")
                                            , A = h[l].value.length - 1;
                                        y.innerHTML = A >= 0 ? h[l].name.toUpperCase() + ": " + (null == h[l].value[A].value ? "-" : h[l].value[A].value) : h[l].name.toUpperCase() + ": -",
                                            y.style.color = p[l],
                                            v.appendChild(y)
                                    }
                                for (var m = this.options.width - this.options.padding_left - 20, f = this.options.context, x = "", l = 0, w = v.childNodes.length; w > l; l++)
                                    x += v.childNodes[l].innerText;
                                if (m > f.measureText(x).width)
                                    d.appendChild(v);
                                else {
                                    for (var l = 1, w = v.childNodes.length; w > l; l++)
                                        v.childNodes[l].innerText = "";
                                    d.appendChild(v)
                                }
                                this.options[i] = {},
                                    this.options[i].defaultMaHtml = d.innerHTML,
                                    document.getElementById(this.options.container).appendChild(d)
                            }
                    }
                    ,
                    t.prototype.markVMA = function(t, i, o, e) {
                        if (this.options.mark_v_ma)
                            this.options.mark_v_ma.v_volume.innerText = i ? "VOLUME: " + n.format_unit(i, 2) : this.default_volume ? "VOLUME: " + n.format_unit(this.default_volume.volume, 2) : "VOLUME: -",
                                this.options.mark_v_ma.v_ma_5.innerText = o ? "MA5: " + n.format_unit(o.value, 2) : this.default_vm5 ? "MA5: " + n.format_unit(this.default_vm5.value, 2) : "MA5: -",
                                this.options.mark_v_ma.v_ma_10.innerText = e ? "MA10: " + n.format_unit(e.value, 2) : this.default_vm10 ? "MA10: " + n.format_unit(this.default_vm10.value, 2) : "MA10: -";
                        else {
                            this.options.mark_v_ma = {};
                            var s = document.createElement("div");
                            s.className = "markTContainer",
                                s.style.left = this.options.padding.left + "px",
                                s.style.top = this.options.c2_y_top + (this.options.height / this.options.y_sepe_num - 12) / 2 - 2 + "px",
                                this.options.mark_v_ma.mark_v_ma = s;
                            var a = document.createDocumentFragment()
                                , r = document.createElement("span");
                            r.style.marginLeft = "10px",
                                r.style.color = "#fe59fe",
                                this.options.mark_v_ma.v_volume = r,
                                this.options.mark_v_ma.v_volume.innerText = i ? "VOLUME: " + n.format_unit(i, 2) : this.default_volume ? "VOLUME: " + n.format_unit(this.default_volume.volume, 2) : "VOLUME: -";
                            var p = document.createElement("span");
                            p.style.color = "#488ee6",
                                this.options.mark_v_ma.v_ma_5 = p,
                                this.options.mark_v_ma.v_ma_5.innerText = o ? "MA5: " + n.format_unit(o.value, 2) : this.default_vm5 ? "MA5: " + n.format_unit(this.default_vm5.value, 2) : "MA5: -";
                            var h = document.createElement("span");
                            h.style.color = "#f4cb15",
                                this.options.mark_v_ma.v_ma_10 = h,
                                this.options.mark_v_ma.v_ma_10.innerText = e ? "MA10: " + n.format_unit(e.value, 2) : this.default_vm10 ? "MA10: " + n.format_unit(this.default_vm10.value, 2) : "MA10: -",
                                a.appendChild(r),
                                a.appendChild(p),
                                a.appendChild(h),
                                s.appendChild(a),
                            "not" == i && (s.style.display = "none"),
                                document.getElementById(this.options.container).appendChild(s)
                        }
                    }
                    ,
                    t.prototype.markT = function(t, i, o, e, n, s) {
                        var a = ["#323232", "#FF00FF", "#808080", "#ff7e58"]
                            , r = []
                            , p = 0;
                        for (var h in o)
                            r.push({
                                value: o[h].slice(e, n),
                                name: h
                            });
                        if (this.options.markTContainer && i == this.options.markTType)
                            for (var l = this.options.markTContainer, c = l.children, p = 0; p < r.length; p++) {
                                var d = c[p];
                                try {
                                    d.innerHTML = r[p].name.toUpperCase() + ": " + (null == r[p].value[s].value ? "-" : r[p].value[s].value)
                                } catch (u) {
                                    if (null == r[p].value[s].value || void 0 == r[p].value[s].value)
                                        d.innerText = r[p].name.toUpperCase() + ": -";
                                    else {
                                        var d = document.createElement("span");
                                        d.innerHTML = r[p].name.toUpperCase() + ": " + r[p].value[s].value,
                                            d.style.color = a[p],
                                            l.appendChild(d)
                                    }
                                }
                            }
                        else {
                            this.options.markTType = i,
                            this.options.markTContainer || (this.options.markTContainer = document.createElement("div"));
                            var l = this.options.markTContainer;
                            l.innerHTML = "",
                                l.className = "markTContainer",
                                l.style.top = this.options.c3_y_top + 3 + "px",
                                l.style.left = this.options.padding.left + 10 + "px";
                            var m = document.createDocumentFragment();
                            for (p = 0; p < r.length; p++) {
                                var d = document.createElement("span")
                                    , f = (r[p].value.length - 1,
                                    "-");
                                void 0 !== r[p].value[s] && null !== r[p].value[s].value && (f = r[p].value[s].value),
                                    d.innerHTML = r[p].name.toUpperCase() + ": " + f,
                                    d.style.color = a[p],
                                    m.appendChild(d)
                            }
                            l.appendChild(m),
                                this.options[i] = {},
                                this.options[i].defaultTHtml = l.innerHTML,
                                document.getElementById(this.options.container).appendChild(l)
                        }
                    }
                    ,
                    t.prototype.scale = function(t) {
                        var i = n.canvasToWindow.apply(this, [t, t.width, this.options.c_k_height]);
                        if (!this.options.scale) {
                            this.options.scale = {};
                            var o = document.createElement("div");
                            o.className = "scale-div",
                                o.style.left = t.width - this.options.padding.right - 120 + "px",
                                o.style.top = i.y - 40 + "px",
                                this.options.scale.scale = o;
                            var e = document.createDocumentFragment()
                                , s = document.createElement("span");
                            s.className = "span-minus",
                                this.options.scale.minus = s;
                            var a = document.createElement("span");
                            a.className = "span-plus",
                                this.options.scale.plus = a,
                                e.appendChild(s),
                                e.appendChild(a),
                                o.appendChild(e),
                                document.getElementById(this.options.container).appendChild(o)
                        }
                    }
                    ,
                    t.prototype.showTip = function(t, i, o, e, s, a, r, p) {
                        if (this.options.tip) {
                            {
                                var h = this.options.tip
                                    , l = this.options.tip.tip;
                                p.volume
                            }
                            e >= r ? x_line_y = r : a >= e ? x_line_y = a : o > s ? e >= o && r >= e ? x_line_y = o : e >= s && o > e ? x_line_y = s : e >= a && s > e && (x_line_y = a) : e >= s && r >= e ? x_line_y = s : e >= o && s > e ? x_line_y = o : e >= a && o > e && (x_line_y = a);
                            for (var c = ["close", "open", "highest", "lowest"], d = 0; d < c.length; d++)
                                h[c[d]].innerText = p[c[d]],
                                    p.percent / 1 == 0 || p[c[d]] == p.yc ? h[c[d]].style.color = "#666666" : p[c[d]] > p.yc ? h[c[d]].style.color = this.options.up_color : p[c[d]] < p.yc && (h[c[d]].style.color = this.options.down_color);
                            var u, m;
                            p.close - p.yc > 0 && yc ? (u = "+",
                                m = this.options.up_color) : p.close - p.yc == 0 ? (u = "",
                                m = "#aaa") : (u = "-",
                                m = this.options.down_color),
                                h.amplitude.innerText = p.amplitude,
                                h.percent.innerText = u + p.percent + "%",
                                h.percent.style.color = m,
                                h.priceChange.innerText = u + "" + p.priceChange,
                                h.priceChange.style.color = m,
                                h.volumeMoney.innerHTML = p.volumeMoney,
                                h.count.innerText = n.format_unit(p.volume),
                                h.date_data.innerHTML = p.date_time,
                                h.lowest.parentNode.style.fontWeight = "100",
                                h.highest.parentNode.style.fontWeight = "100",
                                h.close.parentNode.style.fontWeight = "100",
                                h.open.parentNode.style.fontWeight = "100",
                            x_line_y == r && (h.lowest.parentNode.style.fontWeight = "700"),
                            x_line_y == a && (h.highest.parentNode.style.fontWeight = "700"),
                            x_line_y == o && (h.close.parentNode.style.fontWeight = "700"),
                            x_line_y == s && (h.open.parentNode.style.fontWeight = "700")
                        } else {
                            this.options.tip = {};
                            var l = document.createElement("div");
                            l.className = "web-show-tip",
                                this.options.tip.tip = l,
                                l.style.top = this.options.c1_y_top + 30 + "px";
                            var f = document.createDocumentFragment()
                                , v = document.createElement("div");
                            v.className = "web-tip-first-line",
                                this.options.tip.date_data = v,
                                v.innerText = p.date_time;
                            var g = function(t, i) {
                                var o = document.createElement("div");
                                o.className = "web-tip-line-left",
                                    o.innerText = i;
                                var e = document.createElement("div");
                                e.className = "web-tip-line-right",
                                    this.options.tip[t] = e;
                                var n = document.createElement("div");
                                return n.className = "web-tip-line-container",
                                    n.appendChild(o),
                                    n.appendChild(e),
                                    n
                            }
                                , y = document.createElement("span");
                            this.options.tip.percent = y;
                            var A = document.createElement("span");
                            this.options.tip.count = A;
                            var x = document.createElement("span");
                            this.options.tip.time = x,
                                f.appendChild(v),
                                f.appendChild(g.call(this, "open", "开盘")),
                                f.appendChild(g.call(this, "highest", "最高")),
                                f.appendChild(g.call(this, "lowest", "最低")),
                                f.appendChild(g.call(this, "close", "收盘")),
                                f.appendChild(g.call(this, "percent", "涨跌幅")),
                                f.appendChild(g.call(this, "priceChange", "涨跌额")),
                                f.appendChild(g.call(this, "count", "成交量")),
                                f.appendChild(g.call(this, "volumeMoney", "成交金额")),
                                f.appendChild(g.call(this, "amplitude", "振幅")),
                                l.appendChild(f),
                                document.getElementById(this.options.container).appendChild(l),
                                this.options.tip.div_tip_width = l.clientWidth,
                                y.className = y.className,
                                A.className = A.className,
                                x.className = x.className
                        }
                        i <= t.width / this.options.dpr / 2 ? l.style.left = (t.width - this.options.padding.right) / this.options.dpr - this.options.tip.div_tip_width - 3 + "px" : i >= t.width / this.options.dpr / 2 && (l.style.left = this.options.padding.left / this.options.dpr + "px")
                    }
                    ,
                    t.prototype.markPoint = function(t, i, o, e) {
                        if (e >= 0) {
                            var s = n.canvasToWindow.apply(this, [o, o.width, this.options.c_1_height])
                                , a = n.canvasToWindow.apply(this, [o, t, this.options.c_1_height])
                                , r = document.createElement("div");
                            r.className = "mark-point";
                            var p = this.options.markPoint.imgUrl
                                , h = void 0 == this.options.markPoint.width ? 15 : this.options.markPoint.width + "px"
                                , l = void 0 == this.options.markPoint.height ? 15 : this.options.markPoint.height + "px";
                            if (p && (r.style.background = "url(" + p + ") no-repeat center center/" + h + " " + l + " #cccccc",
                                r.style.background = "url(" + p + ") no-repeat center center/" + h + " " + l + " #cccccc"),
                                this.options.markPoint.width && this.options.markPoint.height ? (r.style.width = h,
                                    r.style.height = l) : (r.style.width = h,
                                    r.style.height = l),
                                r.setAttribute("data-point", i),
                                !this.options.pointsContainer) {
                                var c = document.createElement("div");
                                this.options.pointsContainer = c,
                                    document.getElementById(this.options.container).appendChild(this.options.pointsContainer)
                            }
                            this.options.pointsContainer.appendChild(r),
                                r.style.left = a.x - r.clientWidth / 2 + "px",
                                r.style.top = s.y - 30 + "px"
                        }
                    }
                    ,
                    t.prototype.show = function() {
                        if (this.options.cross) {
                            var t = this.options.cross.x_line;
                            t && (t.style.display = "block");
                            var i = this.options.cross.y_line;
                            i && (i.style.display = "block");
                            var o = this.options.cross.point;
                            o && (o.style.display = "block")
                        }
                        if (this.options.tip) {
                            var e = this.options.tip.tip;
                            e && (e.style.display = "block")
                        }
                    }
                    ,
                    t.prototype.hide = function() {
                        if (this.options.cross) {
                            var t = this.options.cross.x_line;
                            t && (t.style.display = "none");
                            var i = this.options.cross.y_line;
                            i && (i.style.display = "none");
                            var o = this.options.cross.point;
                            o && (o.style.display = "none")
                        }
                        if (this.options.markMAContainer && void 0 !== this.options[this.options.markUPTType],
                        this.options.markTContainer && void 0 !== this.options[this.options.markTType] && (this.options.markTContainer.innerHTML = this.options[this.options.markTType].defaultTHtml),
                            this.options.mark_v_ma) {
                            var e = this.options.mark_v_ma.v_volume;
                            e && (this.options.mark_v_ma.v_volume.innerText = this.default_volume ? "VOLUME: " + n.format_unit(this.default_volume.volume, 2) : "VOLUME: -");
                            var s = this.options.mark_v_ma.v_ma_5;
                            s && (this.options.mark_v_ma.v_ma_5.innerText = this.default_vm5 ? "MA5: " + n.format_unit(this.default_vm5.value, 2) : "MA5: -");
                            var a = this.options.mark_v_ma.v_ma_10;
                            a && (this.options.mark_v_ma.v_ma_10.innerText = this.default_vm10 ? "MA10: " + n.format_unit(this.default_vm10.value, 2) : "MA10: -")
                        }
                        if (this.options.tip) {
                            var r = this.options.tip.tip;
                            r && (r.style.display = "none")
                        }
                    }
                    ,
                    t.prototype.showLoading = function() {
                        if (this.options.loading)
                            this.options.loading.style.display = "block";
                        else {
                            var t = document.getElementById(this.options.container)
                                , i = document.createElement("div");
                            i.className = "loading-chart",
                                i.innerText = "加载中...",
                                i.style.height = this.options.height - 100 + "px",
                                i.style.width = this.options.width + "px",
                                i.style.paddingTop = "100px",
                                this.options.loading = i,
                                t.appendChild(i)
                        }
                    }
                    ,
                    t.prototype.hideLoading = function() {
                        this.options.loading.style.display = "none"
                    }
                    ,
                    t.prototype.showNoData = function() {
                        if (this.options.noData)
                            this.options.noData.style.display = "block";
                        else {
                            var t = document.getElementById(this.options.container)
                                , i = document.createElement("div");
                            i.className = "loading-chart",
                                i.innerText = "暂无数据",
                                i.style.height = this.options.height - 100 + "px",
                                i.style.width = this.options.width + "px",
                                i.style.paddingTop = "100px",
                                this.options.noData = i,
                                t.appendChild(i)
                        }
                    }
                    ,
                    t
            }());
        t.exports = a
    }
    , function(t, i, o) {
        function e(t, i, o) {
            return i / (t - 1) * o
        }
        function n(t, i, o, n, s, a) {
            t.save(),
                t.fillStyle = "#aaa",
                t.strokeStyle = "#aaa",
                t.lineWidth = 1,
                t.beginPath(),
                t.moveTo(Math.ceil(e(n, i, s)) + .5, a),
                t.lineTo(Math.ceil(e(n, i, s)) + .5, a / 2),
                t.font = "12px",
                t.fillText(o, Math.ceil(e(n, i, s) + 5), 2 * a / 3),
                t.stroke(),
                t.restore()
        }
        function s(t, i, o, e) {
            return e - (o - i) / (t - i) * e
        }
        function a(t) {
            var i = {};
            i.top = t.offsetTop,
                i.left = t.offsetLeft;
            for (var o = t.offsetParent; o && "BODY" !== o.nodeName; )
                i.left += o.offsetLeft,
                    i.top += o.offsetTop,
                    o = o.offsetParent;
            return i
        }
        function r(t, i, o) {
            var e = {}
                , n = t.length - 1;
            return e.start = Math.floor(n * i),
                e.end = Math.ceil(n * o),
                e
        }
        var p = o(60)
            , h = function() {
            var t = this
                , i = arguments[0];
            if (1 == arguments.length) {
                for (var o = this.options.data, a = [], r = [], p = 0, h = 1e4, c = o.data.filter(function(t) {
                    return void 0 !== t
                }).length, d = 0; c > d; d++)
                    a.push({
                        date: o.data[d].date_time,
                        value: o.data[d].close
                    }),
                    (0 == d || o.data[d].date_time.substring(0, 4) != o.data[d - 1].date_time.substring(0, 4)) && r.push({
                        year: o.data[d].date_time.substring(0, 4),
                        order: d
                    }),
                        p = Math.max(p, o.data[d].close),
                        h = Math.min(h, o.data[d].close);
                var u = t.options.drawWidth
                    , m = 2 * t.options.unit_height
                    , f = document.createElement("div");
                f.style.position = "absolute",
                    f.style.overflow = "hidden",
                    f.style.left = t.options.padding.left + "px",
                    f.style.top = t.options.c4_y_top + "px",
                    t.container.appendChild(f);
                var v = document.createElement("canvas");
                try {
                    var g = v.getContext("2d")
                } catch (y) {
                    v = window.G_vmlCanvasManager.initElement(v);
                    var g = v.getContext("2d")
                }
                for (f.appendChild(v),
                         v.width = u,
                         v.height = m,
                         v.style.width = u + "px",
                         v.style.height = m + "px",
                         v.className = "slideBarCVS",
                         g.strokeStyle = "#59A7FF",
                         g.beginPath(),
                         d = 0; c > d; d++) {
                    var A = e(c, d, v.width)
                        , x = s(p, h, a[d].value, v.height);
                    0 == d ? g.moveTo(A, x) : g.lineTo(A, x)
                }
                g.stroke(),
                    g.lineTo(e(c, d, v.width), s(p, h, 0, v.height)),
                    g.lineTo(e(c, 0, v.width), s(p, h, 0, v.height));
                var w = "";
                try {
                    w = a[0].value
                } catch (y) {}
                g.lineTo(e(c, 0, v.width), s(p, h, w, v.height)),
                    g.fillStyle = "#E4EFFF",
                    g.fill(),
                    setTimeout(function() {
                        var t = r.length
                            , i = 1;
                        for (i = 2 >= t ? 1 : 7 >= t ? 2 : 13 >= t ? 3 : 17 >= t ? 4 : 5,
                                 d = 0; t > d; d += i) {
                            var e = Math.floor(o.total / t);
                            2 == r.length && 0 == d && r[1].order < e / 12 || n(g, r[d].order, r[d].year, a.length, u, m)
                        }
                    }, 100);
                var _ = document.createElement("div");
                t.options.slideBarWrap = _,
                    _.setAttribute("id", "slideBarWrap"),
                    _.style.position = "absolute",
                    _.style.height = m + "px",
                    _.style.width = (c >= 60 ? 60 / c : 1) * u + "px",
                    _.style.top = "0px",
                    _.style.left = t.options.start / c * u + "px",
                    _.className = "containerBar",
                t.options.data.data.length < 60 && (_.style.display = "none");
                var b = document.createElement("div");
                b.style.position = "absolute",
                    b.style.height = m / 2 + "px",
                    b.style.width = "7px",
                    b.style.border = "solid 1px #999",
                    b.style.top = m / 4 + "px",
                    b.style.left = "-4px",
                    b.style.color = "#999",
                    b.className = "leftDrag",
                    b.innerHTML = "|";
                var T = document.createElement("div");
                T.style.position = "absolute",
                    T.style.height = m / 2 + "px",
                    T.style.width = "7px",
                    T.style.border = "solid 1px #999",
                    T.style.top = m / 4 + "px",
                    T.style.right = "-4px",
                    T.style.color = "#999",
                    T.className = "rightDrag",
                    T.innerHTML = "|",
                    f.appendChild(_),
                    _.appendChild(b),
                    _.appendChild(T),
                    l.call(t, i, a, v, _, b, T)
            } else {
                var M = t.options.start
                    , k = t.options.end
                    , c = this.options.data.data.length
                    , C = t.options.slideBarWrap
                    , u = t.options.drawWidth
                    , N = u * M / c + "px"
                    , E = u * (k - M) / c + "px";
                C.style.left = N,
                    C.style.width = E,
                    i.call(t, M, k)
            }
        }
            , l = function(t, i, o, e, n, s) {
            function h(t) {
                return 1 * t.replace("px", "")
            }
            var l = this
                , c = h(e.style.left)
                , d = h(e.style.width)
                , u = h(n.style.width)
                , m = c - u
                , f = c + d
                , v = document.getElementsByTagName("html")[0]
                , g = i.length
                , y = h(o.style.width)
                , A = (g > 20 ? 20 / g : 1) * y
                , x = !1
                , w = !1
                , _ = !1
                , b = !1
                , T = 0
                , M = a(o);
            p.addEvent(v, "mousedown", function(t) {
                var i = t.clientX - M.left
                    , o = t.target || t.srcElement;
                o == n ? (x = !0,
                    b = !0,
                    T = c - i) : o == s ? (w = !0,
                    b = !0,
                    T = i - c - d) : o == e && (_ = !0,
                    b = !0,
                    T = i - c)
            }),
                p.addEvent(v, "mouseup", function() {
                    x = !1,
                        w = !1,
                        _ = !1,
                        v.style.cursor = "default",
                        c = h(e.style.left),
                        d = h(e.style.width),
                        m = c - u,
                        f = c + d;
                    var n = c / h(o.style.width)
                        , s = (c + d) / h(o.style.width);
                    b && (b = !1,
                        t.call(l, r(i, n, s).start, r(i, n, s).end))
                }),
                p.addEvent(v, "mousemove", function(n) {
                    var s = n.clientX - M.left;
                    if (x === !0 && (s - T >= 0 ? d - (s + T - c) > A && (e.style.left = s + T + "px",
                        e.style.width = d - (s + T - c) + "px") : (e.style.left = "0px",
                        e.style.width = d + c + "px")),
                    w === !0 && (s - T <= h(o.style.width) ? s - c - T > A && (e.style.width = s - c - T + "px") : e.style.width = h(o.style.width) - c + "px"),
                    _ === !0) {
                        e.style.left = 0 >= s - T ? "0px" : s - T >= h(o.style.width) - d ? h(o.style.width) - d + "px" : s - T + "px",
                            c = h(e.style.left),
                            d = h(e.style.width),
                            m = c - u,
                            f = c + d;
                        var a = c / h(o.style.width)
                            , p = (c + d) / h(o.style.width);
                        b && t.call(l, r(i, a, p).start, r(i, a, p).end)
                    }
                    try {
                        n.preventDefault()
                    } catch (n) {
                        n.returnValue = !1
                    }
                })
        };
        t.exports = h
    }
    , function(t, i, o) {
        function e() {
            function t(t) {
                var i;
                1 == t ? i = 5 : 2 == t ? i = 10 : 3 == t ? i = 20 : 4 == t && (i = 30);
                var o = null == s.getCookie("ma" + t + "_default_num") ? i : s.getCookie("ma" + t + "_default_num")
                    , e = null == s.getCookie("ma" + t + "_default_color") ? this.options.maColor[t - 1] : s.getCookie("ma" + t + "_default_color")
                    , a = document.createElement("input");
                if (a.setAttribute("type", "text"),
                    a.value = o,
                    a.className = "ma-item-input",
                    a.setAttribute("readonly", !0),
                1 == t) {
                    var r = document.createElement("span");
                    r.innerHTML = "第" + item_count + "条";
                    var p = document.createElement("span");
                    p.innerHTML = "日移动平均线&nbsp;设置颜色"
                } else {
                    var r = document.createElement("span");
                    r.innerHTML = "第" + item_count + "条";
                    var p = document.createElement("span");
                    p.innerHTML = "日移动平均线&nbsp;设置颜色"
                }
                item_count++;
                var h = document.createElement("div");
                h.className = "ma-item";
                var l = document.createElement("span");
                return l.className = "span-setting setting-span-ma" + t,
                e && (l.style.backgroundColor = e),
                    h.appendChild(r),
                    h.appendChild(a),
                    h.appendChild(p),
                    h.appendChild(l),
                    n.addEvent(a, "mouseleave", function(i) {
                        var o = i.target || i.srcElement
                            , e = o.value;
                        e ? isNaN(e) ? 1 == t ? o.value = 5 : 2 == t ? o.value = 10 : 3 == t ? o.value = 20 : 4 == t && (o.value = 30) : 0 > e ? o.value = Math.floor(Math.abs(e)) : e > 1e3 ? o.value = 1e3 : (-1 != (e + "").indexOf(".") ? 0 : 1) || (o.value = Math.floor(e)) : o.value = 0
                    }),
                    {
                        item: h,
                        pick: l,
                        input: a
                    }
            }
            function i() {
                var t = o.call(this, 1)
                    , i = o.call(this, 2)
                    , e = o.call(this, 3)
                    , n = o.call(this, 4);
                v.input.value = t.ma_default_value,
                    v.pick.style.backgroundColor = t.ma_default_color,
                    g.input.value = i.ma_default_value,
                    g.pick.style.backgroundColor = i.ma_default_color,
                    y.input.value = e.ma_default_value,
                    y.pick.style.backgroundColor = e.ma_default_color,
                    A.input.value = n.ma_default_value,
                    A.pick.style.backgroundColor = n.ma_default_color
            }
            function o(t) {
                var i;
                1 == t ? i = 5 : 2 == t ? i = 10 : 3 == t ? i = 20 : 4 == t && (i = 30);
                var o = null == s.getCookie("ma" + t + "_default_num") ? i : s.getCookie("ma" + t + "_default_num")
                    , e = null == s.getCookie("ma" + t + "_default_color") ? this.options.maColor[t - 1] : s.getCookie("ma" + t + "_default_color");
                return {
                    ma_default_value: o,
                    ma_default_color: e
                }
            }
            var e = this
                , a = 1e6
                , r = new Date;
            r.setTime(r.getTime() + 24 * a * 60 * 60 * 1e3);
            var p = document.createElement("div");
            p.className = "preference-container",
                p.style.top = this.options.c2_y_top + "px",
                p.style.left = this.options.padding.left + "px",
                p.style.width = this.options.drawWidth + "px",
                p.style.height = this.options.canvas.height - this.options.c2_y_top + "px";
            var h = document.createElement("div");
            h.className = "preference-shade";
            var l = document.createElement("div");
            l.className = "preference-handle",
                l.innerHTML = "均线<br/>设置",
                l.style.top = this.options.c2_y_top + "px",
                l.style.left = this.options.padding.left + this.options.drawWidth + "px";
            var c = document.createElement("div");
            c.className = "set-container",
                c.style.top = "0px",
                c.style.left = "100px";
            var d = document.createElement("div");
            d.className = "set-tab";
            var u = document.createElement("div");
            u.className = "ma-tab current",
                u.innerHTML = "均线设置";
            var m = document.createElement("div");
            m.className = "right-tab",
                m.innerHTML = "默认复权";
            var f = document.createElement("div");
            f.className = "ma-panel",
                item_count = 1;
            var v = t.call(e, 1)
                , g = t.call(e, 2)
                , y = t.call(e, 3)
                , A = t.call(e, 4);
            f.appendChild(v.item),
                f.appendChild(g.item),
                f.appendChild(y.item),
                f.appendChild(A.item);
            var x = document.createElement("div");
            x.className = "right-panel";
            for (var w = ["默认不复权", "默认使用前复权", "默认使用后复权"], _ = document.createDocumentFragment(), b = null == s.getCookie("beforeBackRight") ? "fa" : s.getCookie("beforeBackRight"), T = 0; T < w.length; T++) {
                var M = document.createElement("input");
                M.setAttribute("type", "radio"),
                    M.setAttribute("name", "rehabilitation"),
                    0 == T ? M.setAttribute("value", "") : 1 == T ? M.setAttribute("value", "fa") : 2 == T && M.setAttribute("value", "ba"),
                    "" == b ? 0 == T && M.setAttribute("checked", !0) : "fa" == b ? 1 == T && M.setAttribute("checked", !0) : "ba" == b ? 2 == T && M.setAttribute("checked", !0) : 0 == T && M.setAttribute("checked", !0);
                var k = document.createElement("div");
                k.style.marginLeft = "10px",
                    k.style.padding = "8px 0px",
                    k.style.height = "16px",
                    k.style.cursor = "pointer",
                    k.innerHTML = M.outerHTML + "&nbsp;" + w[T],
                    _.appendChild(k),
                    n.addEvent(k, "click", function(t) {
                        var i, o, e = t.target || t.srcElement;
                        for ("div" === e.tagName.toLowerCase() ? (i = e.children[0],
                            o = e.parentNode) : (i = e,
                            o = e.parentNode.parentNode),
                                 T = o.length - 1; T >= 0; T--)
                            o.children[T].children[0].setAttribute("checked", !1);
                        i.setAttribute("checked", !0),
                            i.click()
                    })
            }
            var C = document.createElement("form");
            C.className = "right-panel-form",
                C.appendChild(_),
                x.appendChild(C);
            var N = document.createElement("button");
            N.innerHTML = "确认修改",
                N.className = "confirm-btn",
                n.addEvent(N, "click", function() {
                    for (var t = C.children, i = 0; i < t.length; i++)
                        if (1 == t[i].children[0].checked) {
                            var o = t[i].children[0].value;
                            s.setCookie("beforeBackRight", o, r, "/"),
                                "" == o ? e.beforeBackRight() : "fa" == o ? e.beforeBackRight(1) : "ba" == o && e.beforeBackRight(2)
                        }
                    l.innerHTML = "均线<br/>设置",
                        p.style.display = "none",
                        L = !0,
                        e.options.color.m5Color = v.pick.style.backgroundColor,
                        e.options.maColor[0] = v.pick.style.backgroundColor,
                        s.setCookie("ma1_default_color", v.pick.style.backgroundColor, r, "/"),
                    v.input.value && s.setCookie("ma1_default_num", v.input.value, r, "/"),
                        e.options.color.m10Color = g.pick.style.backgroundColor,
                        e.options.maColor[1] = g.pick.style.backgroundColor,
                        s.setCookie("ma2_default_color", g.pick.style.backgroundColor, r, "/"),
                    g.input.value && s.setCookie("ma2_default_num", g.input.value, r, "/"),
                        e.options.color.m20Color = y.pick.style.backgroundColor,
                        e.options.maColor[2] = y.pick.style.backgroundColor,
                        s.setCookie("ma3_default_color", y.pick.style.backgroundColor, r, "/"),
                    y.input.value && s.setCookie("ma3_default_num", y.input.value, r, "/"),
                        e.options.color.m30Color = A.pick.style.backgroundColor,
                        e.options.maColor[3] = A.pick.style.backgroundColor,
                        s.setCookie("ma4_default_color", A.pick.style.backgroundColor, r, "/"),
                    A.input.value && s.setCookie("ma4_default_num", A.input.value, r, "/")
                });
            var E = document.createElement("button");
            E.innerHTML = "取消修改",
                E.className = "cancle-btn",
                n.addEvent(E, "click", function() {
                    l.innerHTML = "均线<br/>设置",
                        p.style.display = "none",
                        L = !0
                }),
                x.style.display = "none",
                d.appendChild(u),
                c.appendChild(d),
                c.appendChild(f),
                c.appendChild(x),
                c.appendChild(N),
                c.appendChild(E),
                p.appendChild(h),
                p.appendChild(c);
            var S = '<div class="colorPadTriangle"><div class="up"></div><div class="down"></div></div><table class="colorTable"><tr><td style="background-color: #FE0000;"></td><td style="background-color: #FDA748;"></td><td style="background-color: #A7DA19;"></td><td style="background-color: #57A9FF;"></td></tr><tr><td style="background-color: #FF5AFF;"></td><td style="background-color: #F73323;"></td><td style="background-color: #1CA41C;"></td><td style="background-color: #047DFF;"></td></tr><tr><td style="background-color: #FC93B2;"></td><td style="background-color: #B80000;"></td><td style="background-color: #007E3F;"></td><td style="background-color: #0766C4;"></td></tr><tr><td style="background-color: #9A2574;"></td><td style="background-color: #984300;"></td><td style="background-color: #984300;"></td><td style="background-color: #305895;"></td></tr></table>'
                , D = document.createElement("div");
            D.className = "colorPad",
                D.innerHTML = S,
                f.appendChild(D),
                p.style.display = "none",
                this.container.appendChild(l),
                this.container.appendChild(p),
                c.style.left = (this.options.drawWidth - 280) / 2 + "px",
                e.options.pickColor = {};
            var L = !0;
            n.addEvent(l, "click", function() {
                L ? (p.style.display = "block",
                    i.call(e),
                    l.innerHTML = "关闭<br/>设置",
                    L = !1) : (l.innerHTML = "均线<br/>设置",
                    p.style.display = "none",
                    L = !0)
            }),
                n.addEvent(u, "click", function() {
                    f.style.display = "block",
                    u.className.indexOf("current") < 0 && (u.className = u.className + " current"),
                        x.style.display = "none",
                        m.className = m.className.replace(" current", "")
                }),
                n.addEvent(m, "click", function() {
                    f.style.display = "none",
                        u.className = u.className.replace(" current", ""),
                        x.style.display = "block",
                    m.className.indexOf("current") < 0 && (m.className = m.className + " current")
                }),
                n.addEvent(v.pick, "click", function(t) {
                    e.options.pickColor.ma = v.pick,
                        e.options.pickColor.mark = "ma5";
                    var i, o, n;
                    t.target ? (i = t.target,
                        n = i.offsetTop) : (i = t.srcElement,
                        n = i.parentNode.offsetTop + 12),
                        o = i.offsetLeft,
                        D.style.left = o + 28 + "px",
                        D.style.top = n - 7 + "px",
                        D.style.display = "block"
                }),
                n.addEvent(g.pick, "click", function(t) {
                    e.options.pickColor.ma = g.pick,
                        e.options.pickColor.mark = "ma10";
                    var i, o, n;
                    t.target ? (i = t.target,
                        n = i.offsetTop) : (i = t.srcElement,
                        n = i.parentNode.offsetTop + 12),
                        o = i.offsetLeft,
                        D.style.left = o + 28 + "px",
                        D.style.top = n - 7 + "px",
                        D.style.display = "block"
                }),
                n.addEvent(y.pick, "click", function(t) {
                    e.options.pickColor.ma = y.pick,
                        e.options.pickColor.mark = "ma20";
                    var i, o, n;
                    t.target ? (i = t.target,
                        n = i.offsetTop) : (i = t.srcElement,
                        n = i.parentNode.offsetTop + 12),
                        o = i.offsetLeft,
                        D.style.left = o + 28 + "px",
                        D.style.top = n - 7 + "px",
                        D.style.display = "block"
                }),
                n.addEvent(A.pick, "click", function(t) {
                    e.options.pickColor.ma = A.pick,
                        e.options.pickColor.mark = "ma30";
                    var i, o, n;
                    t.target ? (i = t.target,
                        n = i.offsetTop) : (i = t.srcElement,
                        n = i.parentNode.offsetTop + 12),
                        o = i.offsetLeft,
                        D.style.left = o + 28 + "px",
                        D.style.top = n - 7 + "px",
                        D.style.display = "block"
                }),
                n.addEvent(D, "click", function(t) {
                    var i = t.srcElement || t.target
                        , o = i.style.backgroundColor;
                    o && (e.options.pickColor.ma.style.backgroundColor = o),
                        D.style.display = "none"
                })
        }
        var n = o(60)
            , s = o(32);
        t.exports = e
    }
    , function(t) {
        function i(t) {
            for (var i = this.options.data.data, o = [], e = 0, n = 0; n < i.length; n++) {
                var s = {};
                t - 1 > n ? (e += 1 * i[n].close,
                    s.value = null) : (e += 1 * i[n].close,
                    s.value = (e / t).toFixed(2),
                    e -= i[n - t + 1].close),
                    s.date = i[n].date_time,
                    o.push(s)
            }
            return o
        }
        t.exports = i
    }
    , function(t, i, o) {
        var e = o(129)
            , n = o(7)
            , s = o(130)
            , a = o(132)
            , r = o(108)
            , p = o(110)
            , h = o(60)
            , l = o(134)
            , c = o(21)
            , d = o(22)
            , u = (o(135),
            o(32))
            , m = (o(136),
            o(131))
            , f = function() {
            function t(t) {
                this.defaultoptions = n.chartK,
                    this.options = c(n.defaulttheme, this.defaultoptions, t),
                    this.container = document.getElementById(t.container),
                    this.container.className = this.container.className.replace(/emcharts-container/g, "").trim(),
                    this.container.className = this.container.className + " emcharts-container",
                    this.options.onChartLoaded = void 0 == t.onChartLoaded ? function() {}
                        : t.onChartLoaded,
                window.authorityType || (window.authorityType = u.getCookie("beforeBackRight")),
                    window.authorityType = null == window.authorityType || void 0 == window.authorityType ? "fa" : window.authorityType,
                    this.options.authorityType = "" == window.authorityType ? "不复权" : "fa" == window.authorityType ? "前复权" : "ba" == window.authorityType ? "后复权" : "前复权"
            }
            function i() {
                var t = this.options.context
                    , i = this.options.currentData
                    , o = i.data
                    , e = (i.v_max / 1).toFixed(0)
                    , n = 3 * t.canvas.height / this.options.y_sepe_num
                    , s = this.options.v_base_height
                    , a = (this.options.c2_y_top,
                this.options.c2_y_top + n)
                    , r = this.options.rect_unit
                    , p = r.bar_w
                    , l = this.options.up_color
                    , c = this.options.down_color;
                t.lineWidth = 1;
                for (var d, u = 0; d = o[u]; u++) {
                    var m = d.volume
                        , f = d.up
                        , v = m / e * s
                        , g = h.get_x.call(this, u + 1)
                        , y = a - v;
                    t.beginPath(),
                        t.moveTo(g, y),
                        f ? (t.fillStyle = l,
                            t.strokeStyle = l) : (t.fillStyle = c,
                            t.strokeStyle = c),
                        t.rect(g - p / 2, y, p, v),
                        t.stroke(),
                        t.fill()
                }
            }
            function o() {
                function t() {
                    var t = this
                        , o = t.options.context
                        , e = t.options.junxian
                        , n = t.options.start
                        , a = t.options.end
                        , h = t.options.interactive
                        , c = e["ma" + s].slice(n, a)
                        , d = e["ma" + r].slice(n, a)
                        , u = e["ma" + p].slice(n, a)
                        , m = e["ma" + l].slice(n, a);
                    h.default_m5 = c[c.length - 1],
                        h.default_m10 = d[d.length - 1],
                        h.default_m20 = u[u.length - 1],
                        h.default_m30 = m[m.length - 1],
                    s && 0 != s && i.apply(t, [o, c, this.options.color.m5Color]),
                    r && 0 != r && i.apply(t, [o, d, this.options.color.m10Color]),
                    p && 0 != p && i.apply(t, [o, u, this.options.color.m20Color]),
                    l && 0 != l && i.apply(t, [o, m, this.options.color.m30Color])
                }
                function i(t, i, o) {
                    t.save();
                    var e = [];
                    t.beginPath(),
                        t.strokeStyle = o;
                    for (var n = !1, s = !0, a = 0; a < i.length; a++) {
                        var r = i[a];
                        if (r && r.value) {
                            var p = h.get_x.call(this, a + 1)
                                , l = h.get_y.call(this, r.value);
                            s && (t.moveTo(p, l),
                                s = !1),
                                e.push(r),
                                0 == a ? t.moveTo(p, l) : l > this.options.c_k_height || 0 > l ? (t.moveTo(p, l),
                                    n = !0) : (n ? t.moveTo(p, l) : t.lineTo(p, l),
                                    n = !1)
                        }
                    }
                    return t.stroke(),
                        t.restore(),
                        e
                }
                var o = this;
                this.clearK(),
                    this.options.drawXY.drawXYK(),
                    this.drawK();
                var e = this.options.interactive
                    , n = {};
                n = f.call(this);
                var s = null == u.getCookie("ma1_default_num") ? 5 : u.getCookie("ma1_default_num")
                    , r = null == u.getCookie("ma2_default_num") ? 10 : u.getCookie("ma2_default_num")
                    , p = null == u.getCookie("ma3_default_num") ? 20 : u.getCookie("ma3_default_num")
                    , l = null == u.getCookie("ma4_default_num") ? 30 : u.getCookie("ma4_default_num");
                n.extend = "cma," + s + "," + r + "," + p + "," + l,
                    this.options.up_t = "junxian",
                    this.options.down_t = "rsi",
                    a(n, function(i) {
                        o.options.junxian = {},
                            o.options.junxian["ma" + s] = i.five_average,
                            o.options.junxian["ma" + r] = i.ten_average,
                            o.options.junxian["ma" + p] = i.twenty_average,
                            o.options.junxian["ma" + l] = i.thirty_average,
                            o.options.rsi = {},
                            o.options.rsi.rsi6 = i.rsi6,
                            o.options.rsi.rsi12 = i.rsi12,
                            o.options.rsi.rsi24 = i.rsi24,
                            t.apply(o, []),
                            o.options.interactive.options.markMAContainer ? (o.options.interactive.options.markMAContainer.innerHTML = "",
                                o.options.interactive.options.markMAContainer = null) : o.options.interactive.options.markMAContainer = null,
                            y.call(o, o.options.context);
                        var n = i.name + "[" + i.code + "]";
                        e.markMA(o.options.canvas, "junxian", o.options["junxian"], o.options.start, o.options.end, "", o.options.maColor, n)
                    })
            }
            function f() {
                var t = {};
                return t.code = this.options.code,
                    t.type = this.options.type,
                    t.authorityType = window.authorityType,
                    t.extend = this.options.extend,
                    t.onError = this.options.onError,
                    t.host = this.options.host,
                    t
            }
            function v(t) {
                var n = this.options.context
                    , s = n.canvas;
                this.options.data = void 0 == t ? this.options.data : t,
                    t = this.options.data;
                var a = t.data
                    , r = a.length;
                r >= 1 ? (this.options.start = r > 60 ? r - 60 : 0,
                    this.options.end = r) : (this.options.start = 0,
                    this.options.end = 0),
                    this.options.currentData = x(this.options.data, this.options.start, this.options.end);
                var p = this.options.currentData.data
                    , l = p.length;
                this.options.XMark = g.apply(this, [p]);
                var c = this.options.interactive;
                !t || !t.data || 0 == t.data.length,
                    this.options.pricedigit = t.pricedigit;
                var u = h.get_rect.apply(this, [s, l]);
                this.options.rect_unit = u;
                this.options.drawXY = new e(this.options);
                if (this.drawK(),
                    this.options.up_t = "junxian",
                    o.apply(this, []),
                    i.apply(this, [this.options]),
                this.options.showTrading && this.drawVMA(),
                    this.options.interactive.options.pointsContainer) {
                    var m = this.options.interactive.options.pointsContainer.children;
                    this.markPointsDom = m
                }
                return c.hideLoading(),
                    this.options.onChartLoaded(this),
                    d.apply(this, [this.options.context, 95 + this.options.padding.right, 10, 82, 20]),
                    !0
            }
            function g(t) {
                var i = []
                    , o = t || this.options.currentData.data
                    , e = o.length;
                return e > 10 ? (i.push(o[0].date_time),
                    i.push(o[Math.floor(1 * e / 4)].date_time),
                    i.push(o[Math.floor(2 * e / 4)].date_time),
                    i.push(o[Math.floor(3 * e / 4)].date_time),
                    i.push(o[e - 1].date_time)) : o[0] && i.push(o[0].date_time),
                    i
            }
            function y(t) {
                var i = this
                    , o = t.canvas
                    , e = i.options.interactive;
                this.options.clickable = !0;
                this.options.delaytouch = !0;
                h.addEvent.call(i, o, "touchmove", function(t) {
                    A.apply(i, [e, t.changedTouches[0]]);
                    try {
                        t.preventDefault()
                    } catch (o) {
                        t.returnValue = !1
                    }
                }),
                    h.addEvent.call(i, o, "mousemove", function(t) {
                        A.apply(i, [e, t]);
                        try {
                            t.preventDefault()
                        } catch (o) {
                            t.returnValue = !1
                        }
                    }),
                    h.addEvent.call(i, i.container, "mouseleave", function(t) {
                        e.hide();
                        try {
                            t.preventDefault()
                        } catch (i) {
                            t.returnValue = !1
                        }
                    }),
                    h.addEvent.call(i, o, "mouseenter", function(t) {
                        e.show();
                        try {
                            t.preventDefault()
                        } catch (i) {
                            t.returnValue = !1
                        }
                    })
            }
            function A(t, i) {
                var o = this.options.canvas
                    , e = this.options.currentData.data
                    , n = null == u.getCookie("ma1_default_num") ? 5 : u.getCookie("ma1_default_num")
                    , s = null == u.getCookie("ma2_default_num") ? 10 : u.getCookie("ma2_default_num")
                    , a = null == u.getCookie("ma3_default_num") ? 20 : u.getCookie("ma3_default_num")
                    , r = null == u.getCookie("ma4_default_num") ? 30 : u.getCookie("ma4_default_num")
                    , p = this.options.junxian["ma" + n]
                    , l = (this.options.junxian["ma" + s],
                    this.options.junxian["ma" + a],
                    this.options.junxian["ma" + r],
                    this.options.v_ma_5)
                    , c = this.options.v_ma_10
                    , d = this.options.rect_unit
                    , m = d.rect_w
                    , f = i.offsetX || i.clientX - this.container.getBoundingClientRect().left
                    , v = i.offsetY || i.clientY - this.container.getBoundingClientRect().top
                    , g = h.windowToCanvas.apply(this, [o, f, v])
                    , y = g.x
                    , A = g.y - this.options.margin.top
                    , x = Math.floor((y - this.options.padding.left) / m);
                0 > x && (x = 0);
                try {
                    if (e[x]) {
                        var w = e[x].cross_x
                            , _ = e[x].cross_y
                            , b = e[x].cross_y_open
                            , T = e[x].cross_y_highest
                            , M = e[x].cross_y_lowest;
                        t.cross(o, w, _, A, b, T, M);
                        var k = 0 == x ? e[x].open : e[x - 1].close;
                        e[x].yc = k,
                            t.showTip(o, w, _, A, b, T, M, e[x])
                    }
                    if (p[x]) {
                        var C = this.options.data.name + "[" + this.options.data.code + "]";
                        "junxian" == this.options.up_t ? t.markMA(o, this.options.up_t, this.options[this.options.up_t], this.options.start, this.options.end, x, this.options.maColor, C) : t.markMA(o, this.options.up_t, this.options[this.options.up_t], this.options.start, this.options.end, x, this.options.TColor, C),
                            t.markVMA(o, e[x].volume, l[x], c[x])
                    }
                } catch (N) {}
            }
            function x(t, i, o) {
                var e = w(t);
                e.max = -999999999,
                    e.min = 999999999,
                    e.v_max = 0,
                    e.total = o - i + 1,
                    e.name = t.name,
                    e.code = t.code,
                    e.v_ma_5 = t.v_ma_5.slice(i, o),
                    e.v_ma_10 = t.v_ma_10.slice(i, o),
                    e.data = t.data.slice(i, o);
                for (var n = i; o >= n; n++)
                    t.data[n] && (e.max = Math.max(t.data[n].highest, e.max),
                        e.min = Math.min(t.data[n].lowest, e.min),
                        e.v_max = Math.max(t.data[n].volume, e.v_max));
                return e.max = e.max + .05 * (e.max - e.min),
                    e.min = e.min - .05 * (e.max - e.min) < 0 ? 0 : e.min - .05 * (e.max - e.min),
                0 == t.data.length && (e.max = 0,
                    e.min = 0),
                    e
            }
            function w(t) {
                var i = {};
                for (var o in t)
                    i[o] = "object" == typeof t[o] ? w(t[o]) : t[o];
                return i
            }
            return t.prototype.init = function() {
                this.options.type = void 0 == this.options.type ? "K" : this.options.type;
                var t = document.createElement("canvas");
                this.container.style.position = "relative";
                try {
                    var i = t.getContext("2d")
                } catch (o) {
                    t = window.G_vmlCanvasManager.initElement(t);
                    var i = t.getContext("2d")
                }
                this.options.canvas = t,
                    this.options.context = i;
                var e = this.options.dpr = 1;
                this.container.innerHTML = "",
                    this.container.appendChild(t),
                    t.width = this.options.width * e,
                    t.height = this.options.height * e,
                    this.options.up_color = "#ff0000",
                    this.options.down_color = "#17b03e",
                    this.options.scale_count = void 0 == this.options.scale_count ? !1 : this.options.scale_count,
                    t.style.width = this.options.width + "px",
                    t.style.height = this.options.height + "px",
                    t.style.border = "0",
                    i.font = this.options.font_size * this.options.dpr + "px Arial",
                    i.lineWidth = 1 * this.options.dpr,
                    i.strokeStyle = "rgba(230,230,230, 1)",
                    i.fillStyle = "#717171",
                    i.textBaseline = "middle",
                    this.options.color = {},
                    this.options.color.strokeStyle = "rgba(230,230,230, 1)",
                    this.options.color.fillStyle = "#717171",
                    this.options.color.m5Color = null == u.getCookie("ma1_default_color") ? "#f4cb15" : u.getCookie("ma1_default_color"),
                    this.options.color.m10Color = null == u.getCookie("ma2_default_color") ? "#ff5b10" : u.getCookie("ma2_default_color"),
                    this.options.color.m20Color = null == u.getCookie("ma3_default_color") ? "#488ee6" : u.getCookie("ma3_default_color"),
                    this.options.color.m30Color = null == u.getCookie("ma4_default_color") ? "#fe59fe" : u.getCookie("ma4_default_color"),
                    this.options.maColor = [this.options.color.m5Color, this.options.color.m10Color, this.options.color.m20Color, this.options.color.m30Color],
                    this.options.TColor = ["#f4cb15", "#ff5b10", "#488ee6", "#fe59fe"];
                var n = this.options.YMaxNum || "100000.00";
                this.options.padding = {},
                    this.options.padding.left = i.measureText(n).width,
                    this.options.padding.right = 20,
                    this.options.padding.top = 0,
                    this.options.padding.bottom = 0,
                    this.options.drawWidth = t.width - this.options.padding.left - this.options.padding.right,
                    this.options.y_sepe_num = 16,
                    this.options.x_sepe_num = 10,
                    this.options.unit_height = 1 * t.height / this.options.y_sepe_num,
                    this.options.unit_width = 1 * t.width / this.options.x_sepe_num,
                    this.options.c1_y_top = 1 * t.height / this.options.y_sepe_num,
                    this.options.showTrading ? (this.options.c2_y_top = 11.5 * t.height / this.options.y_sepe_num,
                        this.options.c3_y_top = 14 * t.height / this.options.y_sepe_num,
                        this.options.c4_y_top = 15.5 * t.height / this.options.y_sepe_num,
                        this.options.c_k_height = 9 * t.height / this.options.y_sepe_num,
                        this.options.c_v_height = 3 * t.height / this.options.y_sepe_num) : (this.options.c2_y_top = 14 * t.height / this.options.y_sepe_num,
                        this.options.c3_y_top = 14 * t.height / this.options.y_sepe_num,
                        this.options.c4_y_top = 15 * t.height / this.options.y_sepe_num,
                        this.options.c_k_height = 14 * t.height / this.options.y_sepe_num,
                        this.options.c_v_height = 0 * t.height / this.options.y_sepe_num),
                    this.options.v_base_height = .9 * this.options.c_v_height,
                    this.options.c_t_height = 5 * t.height / this.options.y_sepe_num,
                    this.options.margin = {},
                    this.options.margin.left = 0,
                    this.options.margin.top = 1 * t.height / this.options.y_sepe_num,
                    i.translate("0", this.options.margin.top),
                    this.newDotPoint = new p(this.options,this.container)
            }
                ,
                t.prototype.draw = function(t) {
                    var i = this;
                    i.clear(),
                        i.init();
                    var o = i.options.interactive = new l(i.options);
                    o.showLoading();
                    i.options.type;
                    try {
                        var e = f.call(i);
                        e.width = this.options.width,
                            e.height = this.options.height,
                            e.inter = o,
                            e.container = this.container,
                            s(e, function(e) {
                                if (e.name) {
                                    var n = v.apply(i, [e]);
                                    n && i.options.showTrading && (0 == e.data.length ? o.markVMA(i.options.canvas, "not") : o.markVMA(i.options.canvas)),
                                    t && t(null)
                                } else
                                    o.showNoData(),
                                        o.hideLoading()
                            })
                    } catch (n) {
                        o.showNoData(),
                            o.hideLoading(),
                        t && t(n)
                    }
                }
                ,
                t.prototype.staticDraw = function(t) {
                    var i = this;
                    i.clear(),
                        i.init();
                    var o = i.options.interactive = new l(i.options);
                    o.showLoading();
                    var e = this.options.staticdata;
                    try {
                        var n = m(e)
                            , s = v.apply(i, [n]);
                        s && i.options.showTrading && o.markVMA(i.options.canvas)
                    } catch (a) {
                        t && t(a)
                    }
                }
                ,
                t.prototype.reDraw = function() {
                    this.clear(),
                        this.init(),
                        v.call(this)
                }
                ,
                t.prototype.clear = function() {
                    try {
                        var t = this.options.context;
                        t.clearRect(0, 0, this.options.padding.left + this.options.drawWidth, this.options.c4_y_top)
                    } catch (i) {
                        this.container.innerHTML = ""
                    }
                }
                ,
                t.prototype.getMarkPointsDom = function() {
                    var t = this.options.interactive.options.pointsContainer.children;
                    return t
                }
                ,
                t.prototype.drawVMA = function() {
                    function t(t, i, o) {
                        var e = [];
                        t.save(),
                            t.beginPath(),
                            t.strokeStyle = o;
                        for (var n = 0; n < i.length; n++) {
                            var s = i[n];
                            if (s) {
                                var l = h.get_x.call(this, n + 1)
                                    , c = (1 - s.value / a) * r + p;
                                e.push(s),
                                    0 == n ? t.moveTo(l, c) : t.lineTo(l, c)
                            }
                        }
                        return t.stroke(),
                            t.restore(),
                            e
                    }
                    var i = this.options.currentData
                        , o = this.options.context
                        , e = this.options.interactive
                        , n = i.v_ma_5
                        , s = i.v_ma_10
                        , a = (i.v_max / 1).toFixed(0)
                        , r = this.options.v_base_height
                        , p = this.options.c2_y_top;
                    this.options.v_ma_5 = t.apply(this, [o, n, "#488ee6"]),
                        this.options.v_ma_10 = t.apply(this, [o, s, "#f4cb15"]),
                        e.default_volume = i.data[i.data.length - 1],
                        e.default_vm5 = n[n.length - 1],
                        e.default_vm10 = s[s.length - 1]
                }
                ,
                t.prototype.drawMA = function(t, i) {
                    function o() {
                        var o = this
                            , n = o.options.context
                            , a = o.options.junxian
                            , h = o.options.interactive
                            , c = a["ma" + s].slice(t, i)
                            , d = a["ma" + r].slice(t, i)
                            , u = a["ma" + p].slice(t, i)
                            , m = a["ma" + l].slice(t, i);
                        h.default_m5 = c[c.length - 1],
                            h.default_m10 = d[d.length - 1],
                            h.default_m20 = u[u.length - 1],
                            h.default_m30 = m[m.length - 1],
                        s && 0 != s && e.apply(o, [n, c, this.options.color.m5Color]),
                        r && 0 != r && e.apply(o, [n, d, this.options.color.m10Color]),
                        p && 0 != p && e.apply(o, [n, u, this.options.color.m20Color]),
                        l && 0 != l && e.apply(o, [n, m, this.options.color.m30Color])
                    }
                    function e(t, i, o) {
                        t.save();
                        var e = [];
                        t.beginPath(),
                            t.strokeStyle = o;
                        for (var n = !1, s = !0, a = 0; a < i.length; a++) {
                            var r = i[a];
                            if (r && r.value) {
                                var p = h.get_x.call(this, a + 1)
                                    , l = h.get_y.call(this, r.value);
                                s && (t.moveTo(p, l),
                                    s = !1),
                                    e.push(r),
                                    0 === a ? t.moveTo(p, l) : l > this.options.c_k_height || 0 > l ? (t.moveTo(p, l),
                                        n = !0) : (n ? t.moveTo(p, l) : t.lineTo(p, l),
                                        n = !1)
                            }
                        }
                        return t.stroke(),
                            t.restore(),
                            e
                    }
                    var n = this;
                    this.clearK(),
                        this.options.drawXY.drawXYK(),
                        this.drawK();
                    var s = null == u.getCookie("ma1_default_num") ? 5 : u.getCookie("ma1_default_num")
                        , r = null == u.getCookie("ma2_default_num") ? 10 : u.getCookie("ma2_default_num")
                        , p = null == u.getCookie("ma3_default_num") ? 20 : u.getCookie("ma3_default_num")
                        , l = null == u.getCookie("ma4_default_num") ? 30 : u.getCookie("ma4_default_num")
                        , c = {};
                    c = f.call(this),
                        c.extend = "cma," + s + "," + r + "," + p + "," + l,
                        this.options.junxian ? (data = n.options.junxian,
                            o.apply(n, [])) : a(c, function(t) {
                            n.options.junxian = {},
                                n.options.junxian["ma" + s] = t.five_average,
                                n.options.junxian["ma" + r] = t.ten_average,
                                n.options.junxian["ma" + p] = t.twenty_average,
                                n.options.junxian["ma" + l] = t.thirty_average,
                                o.apply(n, [])
                        })
                }
                ,
                t.prototype.drawK = function(t) {
                    this.newDotPoint.delDom();
                    var i = void 0 == t ? this.options.currentData.data : t
                        , o = this.options.context
                        , e = this.options.rect_unit
                        , n = e.bar_w
                        , s = this.options.up_color
                        , a = this.options.down_color
                        , p = this.options.interactive
                        , l = {};
                    if (this.options.markPoint && this.options.markPoint.show) {
                        var c = this.options.markPoint.dateList;
                        for (var d in c)
                            l[c[d]] = c[d]
                    }
                    for (var u, m = {}, f = 0; u = i[f]; f++) {
                        var v = u.up;
                        o.beginPath(),
                            o.lineWidth = 1,
                            v ? (o.fillStyle = s,
                                o.strokeStyle = s) : (o.fillStyle = a,
                                o.strokeStyle = a),
                            m.ctx = o;
                        var g = m.x = h.get_x.call(this, f + 1);
                        m.y_open = h.get_y.call(this, u.open);
                        var y = m.y_close = h.get_y.call(this, u.close);
                        m.y_highest = h.get_y.call(this, u.highest),
                            m.y_lowest = h.get_y.call(this, u.lowest),
                            u.cross_x = g,
                            u.cross_y = y,
                            u.cross_y_open = m.y_open,
                            u.cross_y_lowest = m.y_lowest,
                            u.cross_y_highest = m.y_highest,
                        l[u.data_time] && p.markPoint(g, u.data_time, this.options.context.canvas, this.options.scale_count),
                            m.bar_w = n,
                            r.apply(this, [m]),
                        this.options.dotPoint && (m.c1_y_top = this.options.c1_y_top,
                            m.c_k_height = this.options.c_k_height,
                            this.newDotPoint.dot(m, u))
                    }
                }
                ,
                t.prototype.drawRSI = function(t, i) {
                    function o() {
                        var o = this.options.rsi.rsi6
                            , e = this.options.rsi.rsi12
                            , n = this.options.rsi.rsi24;
                        (void 0 == t || void 0 == i) && (t = this.options.start,
                            i = this.options.end),
                            i += 1,
                            DrawRSI.apply(this, [this.options.context, o.slice(t, i), e.slice(t, i), n.slice(t, i)])
                    }
                    var e = this
                        , n = {};
                    n = f.call(this),
                        n.extend = this.options.down_t = "rsi",
                        this.options.rsi ? o.apply(e, []) : a(n, function(t) {
                            e.options.rsi = {},
                                e.options.rsi.rsi6 = t.rsi6,
                                e.options.rsi.rsi12 = t.rsi12,
                                e.options.rsi.rsi24 = t.rsi24,
                                o.apply(e, [])
                        })
                }
                ,
                t.prototype.drawKDJ = function(t, i) {
                    function o() {
                        var o = this.options.kdj.k
                            , e = this.options.kdj.d
                            , n = this.options.kdj.j;
                        (void 0 == t || void 0 == i) && (t = this.options.start,
                            i = this.options.end),
                            i += 1,
                            DrawKDJ.apply(this, [this.options.context, o.slice(t, i), e.slice(t, i), n.slice(t, i)])
                    }
                    var e = this
                        , n = {};
                    n = f.call(this),
                        n.extend = this.options.down_t = "kdj",
                        this.options.kdj ? o.apply(e, []) : a(n, function(t) {
                            e.options.kdj = {},
                                e.options.kdj.k = t.k,
                                e.options.kdj.d = t.d,
                                e.options.kdj.j = t.j,
                                o.apply(e, [])
                        })
                }
                ,
                t.prototype.drawMACD = function(t, i) {
                    function o() {
                        var o = this.options.macd.dea
                            , n = this.options.macd.diff
                            , s = this.options.macd.macd;
                        (void 0 == t || void 0 == i) && (t = this.options.start,
                            i = this.options.end),
                            i += 1,
                            DrawMACD.apply(e, [e.options.context, o.slice(t, i), n.slice(t, i), s.slice(t, i)])
                    }
                    var e = this
                        , n = {};
                    n = f.call(this),
                        n.extend = this.options.down_t = "macd",
                        this.options.macd ? o.apply(this, []) : a(n, function(t) {
                            e.options.macd = {},
                                e.options.macd.dea = t.dea,
                                e.options.macd.diff = t.diff,
                                e.options.macd.macd = t.macd,
                                o.apply(e, [])
                        })
                }
                ,
                t.prototype.drawWR = function(t, i) {
                    function o() {
                        var o = this.options.wr.wr6
                            , n = this.options.wr.wr10;
                        (void 0 == t || void 0 == i) && (t = this.options.start,
                            i = this.options.end),
                            i += 1,
                            DrawWR.apply(e, [e.options.context, o.slice(t, i), n.slice(t, i)])
                    }
                    var e = this
                        , n = {};
                    n = f.call(this),
                        n.extend = this.options.down_t = "wr",
                        this.options.wr ? o.apply(this, []) : a(n, function(t) {
                            e.options.wr = {},
                                e.options.wr.wr6 = t.wr6,
                                e.options.wr.wr10 = t.wr10,
                                o.apply(e, [])
                        })
                }
                ,
                t.prototype.drawDMI = function(t, i) {
                    function o() {
                        var o = this.options.dmi.pdi
                            , n = this.options.dmi.mdi
                            , s = this.options.dmi.adx
                            , a = this.options.dmi.adxr;
                        (void 0 == t || void 0 == i) && (t = this.options.start,
                            i = this.options.end),
                            i += 1,
                            DrawDMI.apply(e, [e.options.context, o.slice(t, i), n.slice(t, i), s.slice(t, i), a.slice(t, i)])
                    }
                    var e = this
                        , n = {};
                    n = f.call(this),
                        n.extend = this.options.down_t = "dmi",
                        this.options.dmi ? o.apply(e, []) : a(n, function(t) {
                            e.options.dmi = {},
                                e.options.dmi.pdi = t.pdi,
                                e.options.dmi.mdi = t.mdi,
                                e.options.dmi.adx = t.adx,
                                e.options.dmi.adxr = t.adxr,
                                o.apply(e, [])
                        })
                }
                ,
                t.prototype.drawBIAS = function(t, i) {
                    function o() {
                        var o = this.options.bias.bias6
                            , e = this.options.bias.bias12
                            , n = this.options.bias.bias24;
                        (void 0 == t || void 0 == i) && (t = this.options.start,
                            i = this.options.end),
                            i += 1,
                            DrawBIAS.apply(this, [this.options.context, o.slice(t, i), e.slice(t, i), n.slice(t, i)])
                    }
                    var e = this
                        , n = {};
                    n = f.call(this),
                        n.extend = this.options.down_t = "bias",
                        this.options.bias ? o.apply(e, []) : a(n, function(t) {
                            e.options.bias = {},
                                e.options.bias.bias6 = t.bias6,
                                e.options.bias.bias12 = t.bias12,
                                e.options.bias.bias24 = t.bias24,
                                o.apply(e, [])
                        })
                }
                ,
                t.prototype.drawOBV = function(t, i) {
                    function o() {
                        var o = this.options.obv.obv
                            , e = this.options.obv.maobv;
                        (void 0 == t || void 0 == i) && (t = this.options.start,
                            i = this.options.end),
                            i += 1,
                            DrawOBV.apply(this, [this.options.context, o.slice(t, i), e.slice(t, i)])
                    }
                    var e = this
                        , n = {};
                    n = f.call(this),
                        n.extend = this.options.down_t = "obv",
                        e.options.obv ? o.apply(this, []) : a(n, function(t) {
                            e.options.obv = {},
                                e.options.obv.obv = t.obv,
                                e.options.obv.maobv = t.maobv,
                                o.apply(e, [])
                        })
                }
                ,
                t.prototype.drawCCI = function(t, i) {
                    function o() {
                        var o = this.options.cci.cci;
                        (void 0 == t || void 0 == i) && (t = this.options.start,
                            i = this.options.end),
                            i += 1,
                            DrawCCI.apply(this, [this.options.context, o.slice(t, i)])
                    }
                    var e = this
                        , n = {};
                    n = f.call(this),
                        n.extend = this.options.down_t = "cci",
                        this.options.cci ? o.apply(e, []) : a(n, function(t) {
                            e.options.cci = {},
                                e.options.cci.cci = t.cci,
                                o.apply(e, [])
                        })
                }
                ,
                t.prototype.drawROC = function(t, i) {
                    function o() {
                        var o = e.options.roc.roc
                            , n = e.options.roc.rocma;
                        (void 0 == t || void 0 == i) && (t = this.options.start,
                            i = this.options.end),
                            i += 1,
                            DrawROC.apply(e, [e.options.context, o.slice(t, i), n.slice(t, i)])
                    }
                    var e = this
                        , n = {};
                    n = f.call(this),
                        n.extend = this.options.down_t = "roc",
                        e.options.roc ? o.apply(e, []) : a(n, function(t) {
                            e.options.roc = {},
                                e.options.roc.roc = t.roc,
                                e.options.roc.rocma = t.rocma,
                                o.apply(e, [])
                        })
                }
                ,
                t.prototype.drawEXPMA = function() {
                    function t(t, o) {
                        for (var e, n = this.options.expma.expma12.slice(t, o), s = this.options.expma.expma50.slice(t, o), a = n.concat(s), r = i.options.currentData.max, p = i.options.currentData.min, h = 0; e = a[h]; h++)
                            r = Math.max(r, e.value),
                                p = Math.min(p, e.value);
                        this.options.currentData.max = r,
                            this.options.currentData.min = p,
                            this.options.drawXY.options.currentData = this.options.currentData,
                            DrawEXPMA.apply(this, [this.options.context, n, s])
                    }
                    var i = this
                        , o = {};
                    o = f.call(this),
                        o.extend = this.options.up_t = "expma",
                        this.options.expma ? t.apply(i, [this.options.start, this.options.end]) : a(o, function(o) {
                            i.options.expma = {},
                                i.options.expma.expma12 = o.expma12,
                                i.options.expma.expma50 = o.expma50,
                                t.apply(i, [i.options.start, i.options.end])
                        })
                }
                ,
                t.prototype.drawBOLL = function() {
                    function t(t, o) {
                        for (var e, n = this.options.boll.bollup.slice(t, o), s = this.options.boll.bollmb.slice(t, o), a = this.options.boll.bolldn.slice(t, o), r = n.concat(s).concat(a), p = i.options.currentData.max, h = i.options.currentData.min, l = 0; e = r[l]; l++)
                            p = Math.max(p, e.value),
                                h = Math.min(h, e.value);
                        this.options.currentData.max = p,
                            this.options.currentData.min = h,
                            this.options.drawXY.options.currentData = this.options.currentData,
                            DrawBOLL.apply(i, [i.options.context, n, s, a])
                    }
                    var i = this
                        , o = {};
                    o = f.call(this),
                        o.extend = this.options.up_t = "boll",
                        this.options.boll ? t.apply(i, [i.options.start, i.options.end]) : a(o, function(o) {
                            i.options.boll = {},
                                i.options.boll.bollup = o.bollup,
                                i.options.boll.bollmb = o.bollmb,
                                i.options.boll.bolldn = o.bolldn,
                                t.apply(i, [i.options.start, i.options.end])
                        })
                }
                ,
                t.prototype.drawSAR = function() {
                    function t(t, o) {
                        for (var n, s = this.options.sar.sar.slice(t, o), a = i.options.currentData.max, r = i.options.currentData.min, p = 0; n = s[p]; p++)
                            a = Math.max(a, n.value),
                                r = Math.min(r, n.value);
                        this.options.currentData.max = a,
                            this.options.currentData.min = r,
                            this.options.drawXY.options.currentData = this.options.currentData,
                            DrawSAR.apply(i, [i.options.context, s, e])
                    }
                    var i = this
                        , o = {};
                    o = f.call(this),
                        o.extend = this.options.up_t = "sar";
                    var e = this.options.currentData.data;
                    this.options.sar ? t.apply(i, [i.options.start, i.options.end]) : a(o, function(o) {
                        i.options.sar = {},
                            i.options.sar.sar = o.sar,
                            t.apply(i, [i.options.start, i.options.end])
                    })
                }
                ,
                t.prototype.drawBBI = function() {
                    function t(t, o) {
                        for (var e, n = this.options.bbi.bbi.slice(t, o), s = i.options.currentData.max, a = i.options.currentData.min, r = 0; e = n[r]; r++)
                            s = Math.max(s, e.value),
                                a = Math.min(a, e.value);
                        this.options.currentData.max = s,
                            this.options.currentData.min = a,
                            this.options.drawXY.options.currentData = this.options.currentData,
                            DrawBBI.apply(i, [i.options.context, n])
                    }
                    var i = this
                        , o = {};
                    o = f.call(this),
                        o.extend = this.options.up_t = "bbi",
                        this.options.bbi ? t.apply(i, [i.options.start, i.options.end]) : a(o, function(o) {
                            i.options.bbi = {},
                                i.options.bbi.bbi = o.bbi,
                                t.apply(i, [i.options.start, i.options.end])
                        })
                }
                ,
                t.prototype.clearK = function() {
                    var t = this.options.context;
                    t.fillStyle = "#fff",
                        t.fillRect(0, -1 * this.options.unit_height, this.options.padding.left + this.options.drawWidth + 10, this.options.c2_y_top),
                        d.apply(this, [this.options.context, 95 + this.options.padding.right, 10, 82, 20]),
                        this.options.watermark = !0
                }
                ,
                t.prototype.clearT = function() {
                    var t = this.options.context;
                    t.fillStyle = "#fff",
                        t.fillRect(0, this.options.c3_y_top - 10, this.options.padding.left + this.options.drawWidth + 10, this.options.c4_y_top)
                }
                ,
                t.prototype.beforeBackRight = function(t) {
                    var i = 1e6
                        , o = new Date;
                    o.setTime(o.getTime() + 24 * i * 60 * 60 * 1e3),
                        t ? 1 == t ? (window.authorityType = "fa",
                            u.setCookie("beforeBackRight", "fa", o, "/")) : 2 == t && (window.authorityType = "ba",
                            u.setCookie("beforeBackRight", "ba", o, "/")) : (window.authorityType = "",
                            u.setCookie("beforeBackRight", "", o, "/")),
                        this.clear(),
                        this.draw()
                }
                ,
                t
        }();
        t.exports = f
    }
    , function(t, i, o) {
        var e = o(21)
            , n = o(7)
            , s = o(25)
            , a = o(60)
            , r = o(9)
            , p = function() {
            function t(t) {
                this.defaultoptions = n.draw_xy,
                    this.options = e(this.defaultoptions, t),
                    this.draw()
            }
            function i(t, i) {
                var o = this.options.drawWidth
                    , e = o / this.options.x_sepe_num
                    , n = this.options.XMark;
                t.save();
                for (var a = 0; a <= this.options.x_sepe_num; a++) {
                    t.beginPath();
                    var p = this.options.padding.left + a * e
                        , h = 0
                        , l = this.options.padding.left + a * e
                        , c = i;
                    0 == a ? (t.strokeStyle = "#e1e1e1",
                        t.moveTo(r(p), r(h)),
                        t.lineTo(r(l), r(c)),
                        t.stroke()) : a == this.options.x_sepe_num ? (t.strokeStyle = "#e1e1e1",
                        t.moveTo(r(p - 1), r(h)),
                        t.lineTo(r(l - 1), r(c)),
                        t.stroke()) : (t.strokeStyle = "#eeeeee",
                        s(t, p, h, l, c, 5))
                }
                var d = n.length;
                t.font = "12px Arial,Helvetica,San-serif";
                var u = 1;
                t.measureText(n[0]).width + 4 >= this.options.drawWidth / 6 && (u = 2);
                for (var m = 0; d > m; m += u)
                    0 == m ? t.fillText(n[m], m / 4 * this.options.drawWidth + this.options.padding.left, this.options.c_k_height + this.options.unit_height / 2) : m == d - 1 ? t.fillText(n[m], m / 4 * this.options.drawWidth + this.options.padding.left - t.measureText(n[m]).width, this.options.c_k_height + this.options.unit_height / 2) : t.fillText(n[m], m / 4 * this.options.drawWidth + this.options.padding.left - t.measureText(n[m]).width / 2, this.options.c_k_height + this.options.unit_height / 2);
                t.restore()
            }
            function o(t, i, o, e) {
                for (var n = (t - i) / (o - 1), s = [], a = 0; o > a; a++)
                    s.push({
                        num: i + a * n,
                        x: 0,
                        y: e - a / (o - 1) * e
                    });
                return s
            }
            return t.prototype.draw = function() {
                this.drawXYK(),
                this.options.showTrading && this.drawXYV()
            }
                ,
                t.prototype.drawXYT = function() {
                    var t = this.options.context;
                    t.save(),
                        t.beginPath(),
                        t.fillStyle = this.options.color.fillStyle,
                        t.strokeStyle = this.options.color.strokeStyle,
                        t.moveTo(r(this.options.padding.left), r(this.options.c3_y_top - this.options.unit_height)),
                        t.lineTo(r(this.options.padding.left), r(this.options.c3_y_top + this.options.c_t_height)),
                        this.options.context.rect(r(this.options.padding.left), r(this.options.c3_y_top - this.options.unit_height), Math.round(this.options.drawWidth - 2), Math.round(this.options.c_t_height + this.options.unit_height)),
                        t.stroke();
                    var i = this.options.c3_y_top;
                    t.fillStyle = this.options.color.fillStyle,
                        t.strokeStyle = this.options.color.strokeStyle;
                    for (var o = 0; 3 > o; o++) {
                        t.beginPath();
                        var e = this.options.padding.left
                            , n = i + 1 * t.canvas.height / this.options.y_sepe_num * o
                            , a = this.options.padding.left + this.options.drawWidth
                            , p = i + 1 * t.canvas.height / this.options.y_sepe_num * o;
                        0 == o || 2 == o ? (t.strokeStyle = "#e1e1e1",
                            t.moveTo(r(e), r(n)),
                            t.lineTo(r(a), r(p)),
                            t.stroke()) : (t.strokeStyle = "#eeeeee",
                            s(t, e, n, a, p, 5))
                    }
                    t.beginPath(),
                        t.strokeStyle = "#eeeeee";
                    for (var h = this.options.drawWidth, l = this.options.c_t_height, c = h / this.options.x_sepe_num, o = 0; o <= this.options.x_sepe_num; o++) {
                        var e = this.options.padding.left + o * c
                            , n = this.options.c3_y_top
                            , a = this.options.padding.left + o * c
                            , p = this.options.c3_y_top + l;
                        0 != o && o != this.options.x_sepe_num && s(t, e, n, a, p, 5)
                    }
                    t.stroke(),
                        t.beginPath(),
                        t.restore()
                }
                ,
                t.prototype.drawXYV = function() {
                    var t = this.options.context
                        , i = this.options.currentData || this.options.data
                        , o = i.pricedigit;
                    t.save(),
                        t.beginPath(),
                        t.strokeStyle = "#e1e1e1",
                        this.options.context.rect(r(this.options.padding.left), r(this.options.c2_y_top - this.options.unit_height), Math.round(this.options.drawWidth - 2), Math.round(this.options.c_v_height + this.options.unit_height)),
                        t.stroke();
                    var e = this.options.c2_y_top;
                    t.fillStyle = this.options.color.fillStyle,
                        t.strokeStyle = this.options.color.strokeStyle;
                    for (var n = 0; 4 > n; n++) {
                        t.beginPath();
                        var p = this.options.padding.left
                            , h = e + 1 * t.canvas.height / this.options.y_sepe_num * n
                            , l = t.canvas.width - this.options.padding.right
                            , c = e + 1 * t.canvas.height / this.options.y_sepe_num * n;
                        if (0 == n || 3 == n ? (t.strokeStyle = "#e1e1e1",
                            t.moveTo(r(p), r(h)),
                            t.lineTo(r(l), r(c)),
                            t.stroke()) : (t.strokeStyle = "#eeeeee",
                            s(t, p, Math.round(h) + .5, l, Math.round(c) + .5, 5)),
                        n >= 0) {
                            t.save(),
                                t.fillStyle = this.options.color.fillStyle,
                                t.strokeStyle = this.options.color.strokeStyle,
                                t.font = "12px Arial,Helvetica,San-serif",
                                t.textBaseline = "middle";
                            var d = a.formatYNum(i.v_max / 3 * (3 - n), o);
                            0 == i.v_max && (d = "-");
                            var u = p - t.measureText(d).width - 5;
                            t.fillText(d, u, h),
                                t.restore()
                        }
                    }
                    t.beginPath();
                    for (var m = this.options.drawWidth, f = this.options.c_v_height, v = m / this.options.x_sepe_num, n = 0; n <= this.options.x_sepe_num; n++) {
                        var p = this.options.padding.left + n * v
                            , h = this.options.c2_y_top
                            , l = this.options.padding.left + n * v
                            , c = this.options.c2_y_top + f;
                        0 != n && n != this.options.x_sepe_num && (t.strokeStyle = "#eeeeee",
                            s(t, p, Math.round(h), l, Math.round(c), 5))
                    }
                    t.stroke(),
                        t.beginPath(),
                        t.restore()
                }
                ,
                t.prototype.drawXYK = function() {
                    var t = this.options.context
                        , e = this.options.currentData || this.options.data
                        , n = e.max
                        , p = e.min
                        , h = e.pricedigit
                        , l = 9
                        , c = this.options.c_k_height
                        , d = o(n / 1, p / 1, l, c);
                    t.save();
                    var l = d.length;
                    t.fillStyle = this.options.color.fillStyle,
                        t.strokeStyle = this.options.color.strokeStyle,
                        t.font = "12px Arial,Helvetica,San-serif",
                        t.textBaseline = "middle";
                    for (var u, m = 0; u = d[m]; m++) {
                        t.beginPath(),
                            0 == m || m == d.length - 1 ? (t.strokeStyle = "#e1e1e1",
                                t.moveTo(r(this.options.padding.left), r(u.y)),
                                t.lineTo(r(t.canvas.width - this.options.padding.right), r(u.y)),
                                t.stroke()) : (t.strokeStyle = "#eeeeee",
                                s(t, this.options.padding.left, Math.round(u.y), t.canvas.width - this.options.padding.right, Math.round(u.y), 5)),
                            t.moveTo(.5, r(u.y + 5));
                        var f = a.formatYNum(u.num / 1, h);
                        0 == n && 0 == p && (f = "-"),
                            t.fillText(f, this.options.padding.left - 5 - t.measureText(f).width, u.y + 1)
                    }
                    var c = this.options.c_k_height;
                    i.apply(this, [t, c]),
                        t.restore()
                }
                ,
                t
        }();
        t.exports = p
    }
    , function(t, i, o) {
        function e(t, i) {
            var o = r[r.self].host_k;
            t.host && (o = t.host);
            var e = "fsData" + (new Date).getTime().toString();
            "dk" == t.type.toLowerCase() && (t.type = "k");
            var p = {
                id: t.code,
                TYPE: t.type,
                js: e + "((x))",
                rtntype: 5,
                isCR: !1,
                style: "top",
                num: 120,
                authorityType: "FA"
            };
            "" !== t.authorityType && "undefined" != t.authorityType && (p.authorityType = t.authorityType),
                n(o, p, e, function(o) {
                    var e = {};
                    o.info ? (e = s(o, p.extend),
                        i(e)) : a(t)
                })
        }
        var n = o(14)
            , s = o(131)
            , a = o(65)
            , r = o(67);
        t.exports = e
    }
    , function(t) {
        function i(t) {
            var i = {}
                , a = t.info || {};
            i.data = [],
                i.max = 0,
                i.min = t.info.yc,
                i.v_max = 0,
                i.total = t.data.length,
                i.name = t.name,
                i.code = t.code,
                i.pricedigit = (t.info.pricedigit.split(".")[1] || "").length;
            for (var r = t.data, p = t.data.length, h = p - 1; h >= 0; h--) {
                var l, c = (r[i.total - h - 1].split(/\[|\]/),
                    r[i.total - h - 1].split(/\[|\]/)[0].split(",")), d = r[i.total - h - 1].split(","), u = d.length;
                l = r[i.total - h - 2] ? 1 * r[i.total - h - 2].split(/\[|\]/)[0].split(",")[2] : a.yc || c[2];
                var m = {};
                m.date_time = c[0],
                    m.highest = c[3],
                    m.lowest = c[4],
                    m.open = c[1],
                    m.close = c[2],
                    m.volume = c[5],
                    m.amplitude = d[u - 1],
                    m.volumeMoney = c[6] ? c[6] : "--",
                    m.priceChange = Math.abs(m.close - l).toFixed(i.pricedigit),
                    m.percent = 0 === l ? 0 : (m.priceChange / l * 100).toFixed(2),
                h == p - 1 && (m.priceChange = 0..toFixed(i.pricedigit),
                    m.percent = 0..toFixed(i.pricedigit)),
                    m.up = 1 * m.close - 1 * m.open >= 0 ? !0 : !1;
                var f = e(r, i.total - h - 1, 5)
                    , v = e(r, i.total - h - 1, 10);
                o.call(i, "v_ma_5", f, m.date_time),
                    o.call(i, "v_ma_10", v, m.date_time),
                    i.data.push(m),
                    i.max = n([i.max, m.lowest, 1 * m.highest, c[1], c[2], c[3], c[4]]),
                    i.min = s([i.min, m.lowest, 1 * m.highest, c[1], c[2], c[3], c[4]]),
                    i.v_max = i.v_max > 1 * m.volume ? i.v_max : m.volume
            }
            return i.v_ma_5 = i.v_ma_5 || [],
                i.v_ma_10 = i.v_ma_10 || [],
                i
        }
        function o(t, i, o) {
            "-" === i && (i = null),
                void 0 === this[t] ? this[t] = [{
                    value: i,
                    date: o
                }] : this[t].push({
                    value: i,
                    date: o
                })
        }
        function e(t, i, o) {
            var e = 0;
            if (o > i)
                return "-";
            for (var n = 0; o > n; n++)
                e += 1 * t[i - n].split(",")[5];
            return e /= o,
                e.toFixed(2)
        }
        function n(t) {
            for (var i = [], o = 0; o < t.length; o++)
                isNaN(t[o]) || i.push(parseFloat(t[o]));
            return i.sort(function(t, i) {
                return i - t
            }),
                i[0]
        }
        function s(t) {
            for (var i = [], o = 0; o < t.length; o++)
                isNaN(t[o]) || i.push(parseFloat(t[o]));
            return i.sort(function(t, i) {
                return t - i
            }),
                i[0]
        }
        t.exports = i
    }
    , function(t, i, o) {
        function e(t, i) {
            var o = a[a.self].host_k;
            t.host && (o = t.host);
            var e = "fsDataTeac" + t.extend.substring(0, 2) + (new Date).getTime().toString()
                , r = {
                id: t.code,
                TYPE: t.type || "k",
                js: e + "((x))",
                rtntype: 5,
                extend: t.extend || "RSI|MA",
                isCR: !1,
                check: "kte",
                style: "top",
                num: 120,
                authorityType: "FA"
            };
            "" != t.authorityType && "undefined" != t.authorityType && (r.authorityType = t.authorityType),
                s(o, r, e, function(t) {
                    var o = n(t, r.extend);
                    i(o)
                })
        }
        var n = o(133)
            , s = o(14)
            , a = o(67);
        t.exports = e
    }
    , function(t) {
        function i(t, i) {
            var e = {};
            e.name = t.name,
                e.code = t.code,
                e.pricedigit = (t.info.pricedigit.split(".")[1] || "").length;
            for (var n = t.data, s = t.data.length, a = s - 1; a >= 0; a--) {
                for (var r = n[s - a - 1].split(/\[|\]/), p = [], h = 0; h < r.length; h++)
                    "," !== r[h] && p.push(r[h]);
                for (var l = n[s - a - 1].split(/\[|\]/)[0].split(",")[0], c = i.split("|"), d = e.pricedigit, u = 0; u < c.length; u++) {
                    var m = p[u + 1].split(",");
                    switch (c[u].toLowerCase().split(",")[0]) {
                        case "bbi":
                            o.call(e, "bbi", m[0], l, d);
                            break;
                        case "expma":
                            o.call(e, "expma12", m[0], l, d),
                                o.call(e, "expma50", m[1], l, d);
                        case "sar":
                            o.call(e, "sar", m[0], l, d);
                            break;
                        case "boll":
                            o.call(e, "bollmb", m[0], l, d),
                                o.call(e, "bollup", m[1], l, d),
                                o.call(e, "bolldn", m[2], l, d);
                            break;
                        case "cma":
                            o.call(e, "five_average", m[0], l, d),
                                o.call(e, "ten_average", m[1], l, d),
                                o.call(e, "twenty_average", m[2], l, d),
                                o.call(e, "thirty_average", m[3], l, d);
                            break;
                        case "rsi":
                            o.call(e, "rsi6", m[0], l, d),
                                o.call(e, "rsi12", m[1], l, d),
                                o.call(e, "rsi24", m[2], l, d);
                            break;
                        case "kdj":
                            o.call(e, "k", m[0], l, d),
                                o.call(e, "d", m[1], l, d),
                                o.call(e, "j", m[2], l, d);
                            break;
                        case "macd":
                            o.call(e, "diff", m[0], l, d),
                                o.call(e, "dea", m[1], l, d),
                                o.call(e, "macd", m[2], l, d);
                            break;
                        case "wr":
                            o.call(e, "wr10", m[0], l, d),
                                o.call(e, "wr6", m[1], l, d);
                            break;
                        case "dmi":
                            o.call(e, "pdi", m[0], l, d),
                                o.call(e, "mdi", m[1], l, d),
                                o.call(e, "adx", m[2], l, d),
                                o.call(e, "adxr", m[3], l, d);
                            break;
                        case "bias":
                            o.call(e, "bias6", m[0], l, d),
                                o.call(e, "bias12", m[1], l, d),
                                o.call(e, "bias24", m[2], l, d);
                            break;
                        case "obv":
                            o.call(e, "obv", m[0], l, d),
                                o.call(e, "maobv", m[1], l, d);
                            break;
                        case "cci":
                            o.call(e, "cci", m[0], l, d);
                            break;
                        case "roc":
                            o.call(e, "roc", m[0], l, d),
                                o.call(e, "rocma", m[1], l, d)
                    }
                }
            }
            return e.five_average = e.five_average || [],
                e.ten_average = e.ten_average || [],
                e.twenty_average = e.twenty_average || [],
                e.thirty_average = e.thirty_average || [],
                e
        }
        function o(t, i, o, e) {
            "-" === i && (i = null),
                void 0 === this[t] ? this[t] = [{
                    value: (1 * i).toFixed(e),
                    date: o
                }] : this[t].push({
                    value: (1 * i).toFixed(e),
                    date: o
                })
        }
        t.exports = i
    }
    , function(t, i, o) {
        var e = o(21)
            , n = o(60)
            , s = o(7)
            , a = (o(70),
            function() {
                function t(t) {
                    this.defaultoptions = s.interactive,
                        this.options = {},
                        this.options = e(this.options, this.defaultoptions, t)
                }
                return t.prototype.cross = function(t, i, e, n, s, a, r) {
                    this.options.dpr;
                    if (!this.options.cross) {
                        this.options.cross = {};
                        var p = document.createElement("div");
                        p.className = "cross-y",
                            p.style.height = this.options.c4_y_top - this.options.c1_y_top + "px",
                            p.style.top = this.options.c1_y_top + "px",
                            this.options.cross.y_line = p;
                        var h = document.createElement("div");
                        h.className = "cross-x",
                            h.style.width = this.options.drawWidth + "px",
                            this.options.cross.x_line = h;
                        var l = document.createElement("div");
                        l.className = "cross-p",
                            l.style.width = "11px",
                            l.style.height = "11px",
                            this.options.point_width = 11,
                            l.style.borderRadius = l.style.width,
                            l.style.background = "url(" + o(71) + ")",
                            this.options.cross.point = l;
                        var c = document.createDocumentFragment();
                        c.appendChild(h),
                            c.appendChild(p),
                            c.appendChild(l),
                            document.getElementById(this.options.container).appendChild(c)
                    }
                    var p = this.options.cross.y_line
                        , d = 0;
                    n >= r ? d = r : a >= n ? d = a : e > s ? n >= e && r >= n ? d = e : n >= s && e > n ? d = s : n >= a && s > n && (d = a) : n >= s && r >= n ? d = s : n >= e && s > n ? d = e : n >= a && e > n && (d = a),
                    p && (p.style.left = i + "px");
                    var h = this.options.cross.x_line;
                    h && (h.style.top = d + this.options.margin.top + "px",
                        h.style.left = this.options.padding.left + "px");
                    var l = this.options.cross.point;
                    if (l) {
                        var u = this.options.point_width;
                        l.style.left = i - u / 2 + "px",
                            l.style.top = d + this.options.margin.top - u / 2 + "px"
                    }
                }
                    ,
                    t.prototype.markMA = function(t, i, o, e, n, s, a, r) {
                        var p = this.options.showName
                            , h = a
                            , l = []
                            , c = 0;
                        for (var d in o)
                            l.push({
                                value: o[d].slice(e, n),
                                name: d
                            });
                        if (this.options.markMAContainer && i == this.options.markUPTType) {
                            var u = this.options.markMAContainer
                                , m = u.children;
                            if (s || "junxian" != i) {
                                var f = this.options.width - this.options.padding_left - 20
                                    , v = this.options.context;
                                if (0 == m.length) {
                                    var g = document.createDocumentFragment();
                                    for (c = 0; c < l.length; c++)
                                        if (0 == c) {
                                            var y = document.createElement("span");
                                            y.innerHTML = p ? r : "",
                                                g.appendChild(y);
                                            var A = document.createElement("span")
                                                , x = l[c].value.length - 1;
                                            A.innerHTML = l[c].name.toUpperCase() + ": " + (null == l[c].value[x].value ? "-" : l[c].value[x].value),
                                                A.style.color = h[c],
                                                g.appendChild(A)
                                        } else {
                                            var A = document.createElement("span")
                                                , x = l[c].value.length - 1;
                                            A.innerHTML = l[c].name.toUpperCase() + ": " + (null == l[c].value[x].value ? "-" : l[c].value[x].value),
                                                A.style.color = h[c],
                                                g.appendChild(A)
                                        }
                                    var w = "";
                                    if (g.childNodes.forEach(function(t) {
                                        w += t.innerText
                                    }),
                                    f > v.measureText(w).width)
                                        u.appendChild(g);
                                    else {
                                        for (var c = 1, _ = g.childNodes.length; _ > c; c++)
                                            g.childNodes[c].innerText = "";
                                        u.appendChild(g)
                                    }
                                } else {
                                    m[0].innerHTML = p ? r : "";
                                    for (var c = 0; c < l.length; c++) {
                                        var A = m[c + 1];
                                        try {
                                            A.innerHTML = l[c].name.toUpperCase() + ": " + (null == l[c].value[s].value ? "-" : l[c].value[s].value)
                                        } catch (b) {
                                            if (s)
                                                if (null == l[c].value[s].value || void 0 == l[c].value[s].value)
                                                    A.innerText = l[c].name.toUpperCase() + ": -";
                                                else {
                                                    var A = document.createElement("span");
                                                    A.innerHTML = l[c].name.toUpperCase() + ": " + l[c].value[s].value,
                                                        A.style.color = h[c],
                                                        u.appendChild(A)
                                                }
                                            else
                                                ;
                                        }
                                    }
                                    for (var w = "", c = 0; c < m.length; c++)
                                        w += m[c].innerText;
                                    if (f > v.measureText(w).width)
                                        for (var c = 1; c < m.length; c++)
                                            m[c].style.display = "inline";
                                    else
                                        for (var c = 1; c < m.length; c++)
                                            m[c].style.display = "none"
                                }
                            } else {
                                u.innerHTML = this.options[this.options.markUPTType].defaultMaHtml;
                                for (var c = 0; c < l.length; c++) {
                                    var A = m[c + 1];
                                    A.style.color = h[c]
                                }
                                this.options[i].defaultMaHtml = u.innerHTML
                            }
                        } else {
                            this.options.markUPTType = i,
                            this.options.markMAContainer || (this.options.markMAContainer = document.createElement("div"));
                            var u = this.options.markMAContainer;
                            u.innerHTML = "",
                                u.className = "markTContainer",
                                u.style.top = "3px",
                                u.style.left = this.options.padding.left + 10 + "px";
                            var g = document.createDocumentFragment();
                            for (c = 0; c < l.length; c++)
                                if (0 == c) {
                                    var y = document.createElement("span");
                                    y.innerHTML = p ? r : "",
                                        g.appendChild(y);
                                    var A = document.createElement("span")
                                        , x = l[c].value.length - 1;
                                    A.innerHTML = x >= 0 ? l[c].name.toUpperCase() + ": " + (null == l[c].value[x].value ? "-" : l[c].value[x].value) : l[c].name.toUpperCase() + ": -",
                                        A.style.color = h[c],
                                        g.appendChild(A),
                                        document.getElementById(this.options.container).appendChild(u)
                                } else {
                                    var A = document.createElement("span")
                                        , x = l[c].value.length - 1;
                                    A.innerHTML = x >= 0 ? l[c].name.toUpperCase() + ": " + (null == l[c].value[x].value ? "-" : l[c].value[x].value) : l[c].name.toUpperCase() + ": -",
                                        A.style.color = h[c],
                                        g.appendChild(A)
                                }
                            var f = this.options.width - this.options.padding_left - 20
                                , v = this.options.context
                                , w = "";
                            window.mtest = g;
                            for (var c = 0, _ = g.childNodes.length; _ > c; c++)
                                w += g.childNodes[c].innerText;
                            if (f > v.measureText(w).width)
                                u.appendChild(g);
                            else {
                                for (var c = 1, _ = g.childNodes.length; _ > c; c++)
                                    g.childNodes[c].innerText = "";
                                u.appendChild(g)
                            }
                            this.options[i] = {},
                                this.options[i].defaultMaHtml = u.innerHTML,
                                document.getElementById(this.options.container).appendChild(u)
                        }
                    }
                    ,
                    t.prototype.markVMA = function(t, i, o, e) {
                        if (this.options.mark_v_ma)
                            this.options.mark_v_ma.v_volume.innerText = i ? "VOLUME: " + n.format_unit(i, 2) : this.default_volume ? "VOLUME: " + n.format_unit(this.default_volume.volume, 2) : "VOLUME: -",
                                this.options.mark_v_ma.v_ma_5.innerText = o ? "MA5: " + n.format_unit(o.value, 2) : this.default_vm5 ? "MA5: " + n.format_unit(this.default_vm5.value, 2) : "MA5: -",
                                this.options.mark_v_ma.v_ma_10.innerText = e ? "MA10: " + n.format_unit(e.value, 2) : this.default_vm10 ? "MA10: " + n.format_unit(this.default_vm10.value, 2) : "MA10: -";
                        else {
                            this.options.mark_v_ma = {};
                            var s = document.createElement("div");
                            s.className = "markTContainer",
                                s.style.left = this.options.padding.left + "px",
                                s.style.top = this.options.c2_y_top + (this.options.height / this.options.y_sepe_num - 12) / 2 - 2 + "px",
                                this.options.mark_v_ma.mark_v_ma = s;
                            var a = document.createDocumentFragment()
                                , r = document.createElement("span");
                            r.style.marginLeft = "10px",
                                r.style.color = "#fe59fe",
                                this.options.mark_v_ma.v_volume = r,
                                this.options.mark_v_ma.v_volume.innerText = i ? "VOLUME: " + n.format_unit(i, 2) : this.default_volume ? "VOLUME: " + n.format_unit(this.default_volume.volume, 2) : "VOLUME: -";
                            var p = document.createElement("span");
                            p.style.color = "#488ee6",
                                this.options.mark_v_ma.v_ma_5 = p,
                                this.options.mark_v_ma.v_ma_5.innerText = o ? "MA5: " + n.format_unit(o.value, 2) : this.default_vm5 ? "MA5: " + n.format_unit(this.default_vm5.value, 2) : "MA5: -";
                            var h = document.createElement("span");
                            h.style.color = "#f4cb15",
                                this.options.mark_v_ma.v_ma_10 = h,
                                this.options.mark_v_ma.v_ma_10.innerText = e ? "MA10: " + n.format_unit(e.value, 2) : this.default_vm10 ? "MA10: " + n.format_unit(this.default_vm10.value, 2) : "MA10: -",
                                a.appendChild(r),
                                a.appendChild(p),
                                a.appendChild(h),
                                s.appendChild(a),
                            "not" == i && (s.style.display = "none"),
                                document.getElementById(this.options.container).appendChild(s)
                        }
                    }
                    ,
                    t.prototype.markT = function(t, i, o, e, n, s) {
                        var a = ["#6e9fe9", "#ffba42", "#fe59fe", "#ff7e58"]
                            , r = []
                            , p = 0;
                        for (var h in o)
                            r.push({
                                value: o[h].slice(e, n),
                                name: h
                            });
                        if (this.options.markTContainer && i == this.options.markTType)
                            for (var l = this.options.markTContainer, c = l.children, p = 0; p < r.length; p++) {
                                var d = c[p];
                                try {
                                    d.innerHTML = r[p].name.toUpperCase() + ": " + (null == r[p].value[s].value ? "-" : r[p].value[s].value)
                                } catch (u) {
                                    if (null == r[p].value[s].value || void 0 == r[p].value[s].value)
                                        d.innerText = r[p].name.toUpperCase() + ": -";
                                    else {
                                        var d = document.createElement("span");
                                        d.innerHTML = r[p].name.toUpperCase() + ": " + r[p].value[s].value,
                                            d.style.color = a[p],
                                            l.appendChild(d)
                                    }
                                }
                            }
                        else {
                            this.options.markTType = i,
                            this.options.markTContainer || (this.options.markTContainer = document.createElement("div"));
                            var l = this.options.markTContainer;
                            l.innerHTML = "",
                                l.className = "markTContainer",
                                l.style.top = this.options.c3_y_top + 3 + "px",
                                l.style.left = this.options.padding.left + 10 + "px";
                            var m = document.createDocumentFragment();
                            for (p = 0; p < r.length; p++) {
                                var d = document.createElement("span")
                                    , f = (r[p].value.length - 1,
                                    "-");
                                void 0 !== r[p].value[s] && null !== r[p].value[s].value && (f = r[p].value[s].value),
                                    d.innerHTML = r[p].name.toUpperCase() + ": " + f,
                                    d.style.color = a[p],
                                    m.appendChild(d)
                            }
                            l.appendChild(m),
                                this.options[i] = {},
                                this.options[i].defaultTHtml = l.innerHTML,
                                document.getElementById(this.options.container).appendChild(l)
                        }
                    }
                    ,
                    t.prototype.scale = function(t) {
                        var i = n.canvasToWindow.apply(this, [t, t.width, this.options.c_k_height]);
                        if (!this.options.scale) {
                            this.options.scale = {};
                            var o = document.createElement("div");
                            o.className = "scale-div",
                                o.style.left = t.width - this.options.padding.right - 120 + "px",
                                o.style.top = i.y - 40 + "px",
                                this.options.scale.scale = o;
                            var e = document.createDocumentFragment()
                                , s = document.createElement("span");
                            s.className = "span-minus",
                                this.options.scale.minus = s;
                            var a = document.createElement("span");
                            a.className = "span-plus",
                                this.options.scale.plus = a,
                                e.appendChild(s),
                                e.appendChild(a),
                                o.appendChild(e),
                                document.getElementById(this.options.container).appendChild(o)
                        }
                    }
                    ,
                    t.prototype.showTip = function(t, i, o, e, s, a, r, p) {
                        if (this.options.tip) {
                            {
                                var h = this.options.tip
                                    , l = this.options.tip.tip;
                                p.volume
                            }
                            e >= r ? x_line_y = r : a >= e ? x_line_y = a : o > s ? e >= o && r >= e ? x_line_y = o : e >= s && o > e ? x_line_y = s : e >= a && s > e && (x_line_y = a) : e >= s && r >= e ? x_line_y = s : e >= o && s > e ? x_line_y = o : e >= a && o > e && (x_line_y = a);
                            for (var c = ["close", "open", "highest", "lowest"], d = 0; d < c.length; d++)
                                h[c[d]].innerText = p[c[d]],
                                    p.percent / 1 == 0 || p[c[d]] == p.yc ? h[c[d]].style.color = "#666666" : p[c[d]] > p.yc ? h[c[d]].style.color = this.options.up_color : p[c[d]] < p.yc && (h[c[d]].style.color = this.options.down_color);
                            var u, m;
                            p.percent / 1 > 0 ? (u = "+",
                                m = this.options.up_color) : p.percent / 1 == 0 ? (u = "",
                                m = "#666666") : (u = "-",
                                m = this.options.down_color),
                                h.amplitude.innerText = p.amplitude,
                                h.percent.innerText = u + p.percent + "%",
                                h.percent.style.color = m,
                                h.priceChange.innerText = u + "" + p.priceChange,
                                h.priceChange.style.color = m,
                                h.volumeMoney.innerHTML = p.volumeMoney,
                                h.count.innerText = n.format_unit(p.volume),
                                h.date_data.innerHTML = p.date_time,
                                h.lowest.parentNode.style.fontWeight = "100",
                                h.highest.parentNode.style.fontWeight = "100",
                                h.close.parentNode.style.fontWeight = "100",
                                h.open.parentNode.style.fontWeight = "100",
                            x_line_y == r && (h.lowest.parentNode.style.fontWeight = "700"),
                            x_line_y == a && (h.highest.parentNode.style.fontWeight = "700"),
                            x_line_y == o && (h.close.parentNode.style.fontWeight = "700"),
                            x_line_y == s && (h.open.parentNode.style.fontWeight = "700")
                        } else {
                            this.options.tip = {};
                            var l = document.createElement("div");
                            l.className = "web-show-tip",
                                this.options.tip.tip = l,
                                l.style.top = this.options.c1_y_top + 30 + "px";
                            var f = document.createDocumentFragment()
                                , v = document.createElement("div");
                            v.className = "web-tip-first-line",
                                this.options.tip.date_data = v,
                                v.innerText = p.date_time;
                            var g = function(t, i) {
                                var o = document.createElement("div");
                                o.className = "web-tip-line-left",
                                    o.innerText = i;
                                var e = document.createElement("div");
                                e.className = "web-tip-line-right",
                                    this.options.tip[t] = e;
                                var n = document.createElement("div");
                                return n.className = "web-tip-line-container",
                                    n.appendChild(o),
                                    n.appendChild(e),
                                    n
                            }
                                , y = document.createElement("span");
                            this.options.tip.percent = y;
                            var A = document.createElement("span");
                            this.options.tip.count = A;
                            var x = document.createElement("span");
                            this.options.tip.time = x,
                                f.appendChild(v),
                                f.appendChild(g.call(this, "open", "开盘")),
                                f.appendChild(g.call(this, "highest", "最高")),
                                f.appendChild(g.call(this, "lowest", "最低")),
                                f.appendChild(g.call(this, "close", "收盘")),
                                f.appendChild(g.call(this, "percent", "涨跌幅")),
                                f.appendChild(g.call(this, "priceChange", "涨跌额")),
                                f.appendChild(g.call(this, "count", "成交量")),
                                f.appendChild(g.call(this, "volumeMoney", "成交金额")),
                                f.appendChild(g.call(this, "amplitude", "振幅")),
                                l.appendChild(f),
                                document.getElementById(this.options.container).appendChild(l),
                                this.options.tip.div_tip_width = l.clientWidth,
                                y.className = y.className,
                                A.className = A.className,
                                x.className = x.className
                        }
                        i <= t.width / this.options.dpr / 2 ? l.style.left = (t.width - this.options.padding.right) / this.options.dpr - this.options.tip.div_tip_width - 3 + "px" : i >= t.width / this.options.dpr / 2 && (l.style.left = this.options.padding.left / this.options.dpr + "px")
                    }
                    ,
                    t.prototype.markPoint = function(t, i, o, e) {
                        if (e >= 0) {
                            var s = n.canvasToWindow.apply(this, [o, o.width, this.options.c_1_height])
                                , a = n.canvasToWindow.apply(this, [o, t, this.options.c_1_height])
                                , r = document.createElement("div");
                            r.className = "mark-point";
                            var p = this.options.markPoint.imgUrl
                                , h = void 0 == this.options.markPoint.width ? 15 : this.options.markPoint.width + "px"
                                , l = void 0 == this.options.markPoint.height ? 15 : this.options.markPoint.height + "px";
                            if (p && (r.style.background = "url(" + p + ") no-repeat center center/" + h + " " + l + " #cccccc",
                                r.style.background = "url(" + p + ") no-repeat center center/" + h + " " + l + " #cccccc"),
                                this.options.markPoint.width && this.options.markPoint.height ? (r.style.width = h,
                                    r.style.height = l) : (r.style.width = h,
                                    r.style.height = l),
                                r.setAttribute("data-point", i),
                                !this.options.pointsContainer) {
                                var c = document.createElement("div");
                                this.options.pointsContainer = c,
                                    document.getElementById(this.options.container).appendChild(this.options.pointsContainer)
                            }
                            this.options.pointsContainer.appendChild(r),
                                r.style.left = a.x - r.clientWidth / 2 + "px",
                                r.style.top = s.y - 30 + "px"
                        }
                    }
                    ,
                    t.prototype.show = function() {
                        if (this.options.cross) {
                            var t = this.options.cross.x_line;
                            t && (t.style.display = "block");
                            var i = this.options.cross.y_line;
                            i && (i.style.display = "block");
                            var o = this.options.cross.point;
                            o && (o.style.display = "block")
                        }
                        if (this.options.tip) {
                            var e = this.options.tip.tip;
                            e && (e.style.display = "block")
                        }
                    }
                    ,
                    t.prototype.hide = function() {
                        if (this.options.cross) {
                            var t = this.options.cross.x_line;
                            t && (t.style.display = "none");
                            var i = this.options.cross.y_line;
                            i && (i.style.display = "none");
                            var o = this.options.cross.point;
                            o && (o.style.display = "none")
                        }
                        if (this.options.markMAContainer && void 0 !== this.options[this.options.markUPTType] && (this.options.markMAContainer.innerHTML = this.options[this.options.markUPTType].defaultMaHtml),
                        this.options.markTContainer && void 0 !== this.options[this.options.markTType] && (this.options.markTContainer.innerHTML = this.options[this.options.markTType].defaultTHtml),
                            this.options.mark_v_ma) {
                            var e = this.options.mark_v_ma.v_volume;
                            e && (this.options.mark_v_ma.v_volume.innerText = this.default_volume ? "VOLUME: " + n.format_unit(this.default_volume.volume, 2) : "VOLUME: -");
                            var s = this.options.mark_v_ma.v_ma_5;
                            s && (this.options.mark_v_ma.v_ma_5.innerText = this.default_vm5 ? "MA5: " + n.format_unit(this.default_vm5.value, 2) : "MA5: -");
                            var a = this.options.mark_v_ma.v_ma_10;
                            a && (this.options.mark_v_ma.v_ma_10.innerText = this.default_vm10 ? "MA10: " + n.format_unit(this.default_vm10.value, 2) : "MA10: -")
                        }
                        if (this.options.tip) {
                            var r = this.options.tip.tip;
                            r && (r.style.display = "none")
                        }
                    }
                    ,
                    t.prototype.showLoading = function() {
                        if (this.options.loading)
                            this.options.loading.style.display = "block";
                        else {
                            var t = document.getElementById(this.options.container)
                                , i = document.createElement("div");
                            i.className = "loading-chart",
                                i.innerText = "加载中...",
                                i.style.height = this.options.height - 100 + "px",
                                i.style.width = this.options.width + "px",
                                i.style.paddingTop = "100px",
                                this.options.loading = i,
                                t.appendChild(i)
                        }
                    }
                    ,
                    t.prototype.hideLoading = function() {
                        this.options.loading.style.display = "none"
                    }
                    ,
                    t.prototype.showNoData = function() {
                        if (this.options.noData)
                            this.options.noData.style.display = "block";
                        else {
                            var t = document.getElementById(this.options.container)
                                , i = document.createElement("div");
                            i.className = "loading-chart",
                                i.innerText = "暂无数据",
                                i.style.height = this.options.height - 100 + "px",
                                i.style.width = this.options.width + "px",
                                i.style.paddingTop = "100px",
                                this.options.noData = i,
                                t.appendChild(i)
                        }
                    }
                    ,
                    t
            }());
        t.exports = a
    }
    , function(t, i, o) {
        function e() {
            function t(t) {
                var i;
                1 == t ? i = 5 : 2 == t ? i = 10 : 3 == t ? i = 20 : 4 == t && (i = 30);
                var o = null == s.getCookie("ma" + t + "_default_num") ? i : s.getCookie("ma" + t + "_default_num")
                    , e = null == s.getCookie("ma" + t + "_default_color") ? this.options.maColor[t - 1] : s.getCookie("ma" + t + "_default_color")
                    , a = document.createElement("input");
                if (a.setAttribute("type", "text"),
                    a.value = o,
                    a.className = "ma-item-input",
                1 == t) {
                    var r = document.createElement("span");
                    r.innerHTML = "第" + item_count + "条";
                    var p = document.createElement("span");
                    p.innerHTML = "日移动平均线&nbsp;设置颜色"
                } else {
                    var r = document.createElement("span");
                    r.innerHTML = "第" + item_count + "条";
                    var p = document.createElement("span");
                    p.innerHTML = "日移动平均线&nbsp;设置颜色"
                }
                item_count++;
                var h = document.createElement("div");
                h.className = "ma-item";
                var l = document.createElement("span");
                return l.className = "span-setting setting-span-ma" + t,
                e && (l.style.backgroundColor = e),
                    h.appendChild(r),
                    h.appendChild(a),
                    h.appendChild(p),
                    h.appendChild(l),
                    n.addEvent(a, "mouseleave", function(i) {
                        var o = i.target || i.srcElement
                            , e = o.value;
                        e ? isNaN(e) ? 1 == t ? o.value = 5 : 2 == t ? o.value = 10 : 3 == t ? o.value = 20 : 4 == t && (o.value = 30) : 0 > e ? o.value = Math.floor(Math.abs(e)) : e > 1e3 ? o.value = 1e3 : (-1 != (e + "").indexOf(".") ? 0 : 1) || (o.value = Math.floor(e)) : o.value = 0
                    }),
                    {
                        item: h,
                        pick: l,
                        input: a
                    }
            }
            function i() {
                var t = o.call(this, 1)
                    , i = o.call(this, 2)
                    , e = o.call(this, 3)
                    , n = o.call(this, 4);
                v.input.value = t.ma_default_value,
                    v.pick.style.backgroundColor = t.ma_default_color,
                    g.input.value = i.ma_default_value,
                    g.pick.style.backgroundColor = i.ma_default_color,
                    y.input.value = e.ma_default_value,
                    y.pick.style.backgroundColor = e.ma_default_color,
                    A.input.value = n.ma_default_value,
                    A.pick.style.backgroundColor = n.ma_default_color
            }
            function o(t) {
                var i;
                1 == t ? i = 5 : 2 == t ? i = 10 : 3 == t ? i = 20 : 4 == t && (i = 30);
                var o = null == s.getCookie("ma" + t + "_default_num") ? i : s.getCookie("ma" + t + "_default_num")
                    , e = null == s.getCookie("ma" + t + "_default_color") ? this.options.maColor[t - 1] : s.getCookie("ma" + t + "_default_color");
                return {
                    ma_default_value: o,
                    ma_default_color: e
                }
            }
            var e = this
                , a = 1e6
                , r = new Date;
            r.setTime(r.getTime() + 24 * a * 60 * 60 * 1e3);
            var p = document.createElement("div");
            p.className = "preference-container",
                p.style.top = this.options.c2_y_top + "px",
                p.style.left = this.options.padding.left + "px",
                p.style.width = this.options.drawWidth + "px",
                p.style.height = this.options.canvas.height - this.options.c2_y_top + "px";
            var h = document.createElement("div");
            h.className = "preference-shade";
            var l = document.createElement("div");
            l.className = "preference-handle",
                l.innerHTML = "均线<br/>设置",
                l.style.top = this.options.c2_y_top + "px",
                l.style.left = this.options.padding.left + this.options.drawWidth + "px";
            var c = document.createElement("div");
            c.className = "set-container",
                c.style.top = "0px",
                c.style.left = "100px";
            var d = document.createElement("div");
            d.className = "set-tab";
            var u = document.createElement("div");
            u.className = "ma-tab current",
                u.innerHTML = "均线设置";
            var m = document.createElement("div");
            m.className = "right-tab",
                m.innerHTML = "默认复权";
            var f = document.createElement("div");
            f.className = "ma-panel",
                item_count = 1;
            var v = t.call(e, 1)
                , g = t.call(e, 2)
                , y = t.call(e, 3)
                , A = t.call(e, 4);
            f.appendChild(v.item),
                f.appendChild(g.item),
                f.appendChild(y.item),
                f.appendChild(A.item);
            var x = document.createElement("div");
            x.className = "right-panel";
            for (var w = ["默认不复权", "默认使用前复权", "默认使用后复权"], _ = document.createDocumentFragment(), b = null == s.getCookie("beforeBackRight") ? "fa" : s.getCookie("beforeBackRight"), T = 0; T < w.length; T++) {
                var M = document.createElement("input");
                M.setAttribute("type", "radio"),
                    M.setAttribute("name", "rehabilitation"),
                    0 == T ? M.setAttribute("value", "") : 1 == T ? M.setAttribute("value", "fa") : 2 == T && M.setAttribute("value", "ba"),
                    "" == b ? 0 == T && M.setAttribute("checked", !0) : "fa" == b ? 1 == T && M.setAttribute("checked", !0) : "ba" == b ? 2 == T && M.setAttribute("checked", !0) : 0 == T && M.setAttribute("checked", !0);
                var k = document.createElement("div");
                k.style.marginLeft = "10px",
                    k.style.padding = "8px 0px",
                    k.style.height = "16px",
                    k.style.cursor = "pointer",
                    k.innerHTML = M.outerHTML + "&nbsp;" + w[T],
                    _.appendChild(k),
                    n.addEvent(k, "click", function(t) {
                        var i, o, e = t.target || t.srcElement;
                        for ("div" === e.tagName.toLowerCase() ? (i = e.children[0],
                            o = e.parentNode) : (i = e,
                            o = e.parentNode.parentNode),
                                 T = o.length - 1; T >= 0; T--)
                            o.children[T].children[0].setAttribute("checked", !1);
                        i.setAttribute("checked", !0),
                            i.click()
                    })
            }
            var C = document.createElement("form");
            C.className = "right-panel-form",
                C.appendChild(_),
                x.appendChild(C);
            var N = document.createElement("button");
            N.innerHTML = "确认修改",
                N.className = "confirm-btn",
                n.addEvent(N, "click", function() {
                    for (var t = C.children, i = 0; i < t.length; i++)
                        if (1 == t[i].children[0].checked) {
                            var o = t[i].children[0].value;
                            s.setCookie("beforeBackRight", o, r, "/"),
                                "" == o ? e.beforeBackRight() : "fa" == o ? e.beforeBackRight(1) : "ba" == o && e.beforeBackRight(2)
                        }
                    l.innerHTML = "均线<br/>设置",
                        p.style.display = "none",
                        L = !0,
                        e.options.color.m5Color = v.pick.style.backgroundColor,
                        e.options.maColor[0] = v.pick.style.backgroundColor,
                        s.setCookie("ma1_default_color", v.pick.style.backgroundColor, r, "/"),
                    v.input.value && s.setCookie("ma1_default_num", v.input.value, r, "/"),
                        e.options.color.m10Color = g.pick.style.backgroundColor,
                        e.options.maColor[1] = g.pick.style.backgroundColor,
                        s.setCookie("ma2_default_color", g.pick.style.backgroundColor, r, "/"),
                    g.input.value && s.setCookie("ma2_default_num", g.input.value, r, "/"),
                        e.options.color.m20Color = y.pick.style.backgroundColor,
                        e.options.maColor[2] = y.pick.style.backgroundColor,
                        s.setCookie("ma3_default_color", y.pick.style.backgroundColor, r, "/"),
                    y.input.value && s.setCookie("ma3_default_num", y.input.value, r, "/"),
                        e.options.color.m30Color = A.pick.style.backgroundColor,
                        e.options.maColor[3] = A.pick.style.backgroundColor,
                        s.setCookie("ma4_default_color", A.pick.style.backgroundColor, r, "/"),
                    A.input.value && s.setCookie("ma4_default_num", A.input.value, r, "/")
                });
            var E = document.createElement("button");
            E.innerHTML = "取消修改",
                E.className = "cancle-btn",
                n.addEvent(E, "click", function() {
                    l.innerHTML = "均线<br/>设置",
                        p.style.display = "none",
                        L = !0
                }),
                x.style.display = "none",
                d.appendChild(u),
                c.appendChild(d),
                c.appendChild(f),
                c.appendChild(x),
                c.appendChild(N),
                c.appendChild(E),
                p.appendChild(h),
                p.appendChild(c);
            var S = '<div class="colorPadTriangle"><div class="up"></div><div class="down"></div></div><table class="colorTable"><tr><td style="background-color: #FE0000;"></td><td style="background-color: #FDA748;"></td><td style="background-color: #A7DA19;"></td><td style="background-color: #57A9FF;"></td></tr><tr><td style="background-color: #FF5AFF;"></td><td style="background-color: #F73323;"></td><td style="background-color: #1CA41C;"></td><td style="background-color: #047DFF;"></td></tr><tr><td style="background-color: #FC93B2;"></td><td style="background-color: #B80000;"></td><td style="background-color: #007E3F;"></td><td style="background-color: #0766C4;"></td></tr><tr><td style="background-color: #9A2574;"></td><td style="background-color: #984300;"></td><td style="background-color: #984300;"></td><td style="background-color: #305895;"></td></tr></table>'
                , D = document.createElement("div");
            D.className = "colorPad",
                D.innerHTML = S,
                f.appendChild(D),
                p.style.display = "none",
                this.container.appendChild(l),
                this.container.appendChild(p),
                c.style.left = (this.options.drawWidth - 280) / 2 + "px",
                e.options.pickColor = {};
            var L = !0;
            n.addEvent(l, "click", function() {
                L ? (p.style.display = "block",
                    i.call(e),
                    l.innerHTML = "关闭<br/>设置",
                    L = !1) : (l.innerHTML = "均线<br/>设置",
                    p.style.display = "none",
                    L = !0)
            }),
                n.addEvent(u, "click", function() {
                    f.style.display = "block",
                    u.className.indexOf("current") < 0 && (u.className = u.className + " current"),
                        x.style.display = "none",
                        m.className = m.className.replace(" current", "")
                }),
                n.addEvent(m, "click", function() {
                    f.style.display = "none",
                        u.className = u.className.replace(" current", ""),
                        x.style.display = "block",
                    m.className.indexOf("current") < 0 && (m.className = m.className + " current")
                }),
                n.addEvent(v.pick, "click", function(t) {
                    e.options.pickColor.ma = v.pick,
                        e.options.pickColor.mark = "ma5";
                    var i, o, n;
                    t.target ? (i = t.target,
                        n = i.offsetTop) : (i = t.srcElement,
                        n = i.parentNode.offsetTop + 12),
                        o = i.offsetLeft,
                        D.style.left = o + 28 + "px",
                        D.style.top = n - 7 + "px",
                        D.style.display = "block"
                }),
                n.addEvent(g.pick, "click", function(t) {
                    e.options.pickColor.ma = g.pick,
                        e.options.pickColor.mark = "ma10";
                    var i, o, n;
                    t.target ? (i = t.target,
                        n = i.offsetTop) : (i = t.srcElement,
                        n = i.parentNode.offsetTop + 12),
                        o = i.offsetLeft,
                        D.style.left = o + 28 + "px",
                        D.style.top = n - 7 + "px",
                        D.style.display = "block"
                }),
                n.addEvent(y.pick, "click", function(t) {
                    e.options.pickColor.ma = y.pick,
                        e.options.pickColor.mark = "ma20";
                    var i, o, n;
                    t.target ? (i = t.target,
                        n = i.offsetTop) : (i = t.srcElement,
                        n = i.parentNode.offsetTop + 12),
                        o = i.offsetLeft,
                        D.style.left = o + 28 + "px",
                        D.style.top = n - 7 + "px",
                        D.style.display = "block"
                }),
                n.addEvent(A.pick, "click", function(t) {
                    e.options.pickColor.ma = A.pick,
                        e.options.pickColor.mark = "ma30";
                    var i, o, n;
                    t.target ? (i = t.target,
                        n = i.offsetTop) : (i = t.srcElement,
                        n = i.parentNode.offsetTop + 12),
                        o = i.offsetLeft,
                        D.style.left = o + 28 + "px",
                        D.style.top = n - 7 + "px",
                        D.style.display = "block"
                }),
                n.addEvent(D, "click", function(t) {
                    var i = t.srcElement || t.target
                        , o = i.style.backgroundColor;
                    o && (e.options.pickColor.ma.style.backgroundColor = o),
                        D.style.display = "none"
                })
        }
        var n = o(60)
            , s = o(32);
        t.exports = e
    }
    , function(t) {
        function i(t) {
            for (var i = this.options.data.data, o = [], e = 0, n = 0; n < i.length; n++) {
                var s = {};
                t - 1 > n ? (e += 1 * i[n].close,
                    s.value = null) : (e += 1 * i[n].close,
                    s.value = (e / t).toFixed(2),
                    e -= i[n - t + 1].close),
                    s.date = i[n].date_time,
                    o.push(s)
            }
            return o
        }
        t.exports = i
    }
    , function(t, i, o) {
        var e = o(21)
            , n = o(138)
            , s = o(38)
            , a = o(139)
            , r = o(140)
            , p = o(22)
            , h = o(13)
            , l = function() {
            function t(t) {
                this.options = e(this.options, t),
                this.options.barWidth || (this.options.barWidth = .5),
                this.options.barWidth > 1 && (this.options.barWidth = 1),
                this.options.barWidth < .01 && (this.options.barWidth = .01),
                    this.container = document.getElementById(t.container),
                    this.container.className = this.container.className + " canvas-container"
            }
            function i(t) {
                var i = s(this.options.sepeNum, t);
                return 0 == i.max && 0 == i.min && (i.max = 1,
                    i.min = -1,
                    i.stepHeight = 1,
                    this.options.sepeNum = 2),
                    {
                        max: i.max,
                        min: i.min,
                        stepHeight: i.stepHeight
                    }
            }
            return t.prototype.init = function() {
                this.container.style.position = "relative";
                var t = 1
                    , o = document.createElement("canvas");
                this.container.appendChild(o);
                try {
                    var e = o.getContext("2d")
                } catch (n) {
                    o = window.G_vmlCanvasManager.initElement(o);
                    var e = o.getContext("2d")
                }
                o.width = t * this.options.width,
                    o.height = t * this.options.height,
                    o.style.width = this.options.width + "px",
                    o.style.height = this.options.height + "px",
                    this.options.font_size = 12,
                    e.font = this.options.font_size * t + "px Arial",
                    e.lineWidth = 1 * t,
                    this.options.dpr = t,
                    this.options.canvas = o,
                    this.options.context = e,
                    this.options.defaultColor = "#FF7200",
                    this.options.defaultHoverColor = "#FF9A4A",
                this.options.sepeNum || (this.options.sepeNum = 4),
                    this.options.padding = {},
                    this.options.padding.left = e.measureText("+8000万").width * t,
                    this.options.padding.right = 10,
                    this.options.padding.top = 2 * this.options.font_size * t;
                var s = this.options.xaxis;
                if (this.options.angle || 0 == this.options.angle) {
                    var a = s[0].value;
                    this.options.padding.bottom = e.measureText(a).width * Math.sin(2 * Math.PI / 360 * this.options.angle) + 25
                } else
                    this.options.padding.bottom = 50 * t;
                var r = (o.width - this.options.padding.left - this.options.padding.right) / this.options.series.data.length
                    , h = r * this.options.barWidth;
                this.options.unit_w_len = r,
                    this.options.unit_w_kind = h;
                var l = i.call(this, this.options.series.data);
                this.options.coordinate = l,
                    p.apply(this, [this.options.context, 110, 40, 82, 20])
            }
                ,
                t.prototype.draw = function(t) {
                    this.clear(),
                        this.init();
                    var i = this;
                    new n(this.options),
                        a.call(this),
                        h.addEvent(this.options.canvas, "mousemove", function(t) {
                            var o, e;
                            t.layerX ? (o = t.layerX,
                                e = t.layerY) : t.x && (o = t.x,
                                e = t.y),
                                r.call(i, o, e)
                        }, !1),
                    t && t()
                }
                ,
                t.prototype.reDraw = function(t) {
                    this.options = {},
                        this.options = e(this.options, t),
                        this.clear(),
                        this.draw()
                }
                ,
                t.prototype.clear = function(t) {
                    this.container ? this.container.innerHTML = "" : document.getElementById(this.options.container).innerHTML = "",
                    t && t()
                }
                ,
                t
        }();
        t.exports = l
    }
    , function(t, i, o) {
        var e = o(21)
            , n = o(8)
            , s = o(9)
            , a = o(60)
            , r = o(56)
            , p = function() {
            function t(t) {
                this.options = {},
                    this.options = e(this.options, t),
                    this.draw()
            }
            function i(t, i) {
                var o = 1;
                -1 !== i.toString().indexOf(".") && (o = i.toString().length - i.toString().indexOf(".") - 1);
                var e = Math.pow(10, o);
                return a.format_unit(Math.round(t * e) / e, o)
            }
            return t.prototype.init = function() {
                this.options.yLefShow = !0,
                    this.options.yRightShow = !0,
                    this.options.isDash = !0,
                    this.options.xSplitShow = !1,
                    this.options.xShowDivide = !1
            }
                ,
                t.prototype.draw = function() {
                    this.init();
                    {
                        var t = this.options.padding.top
                            , o = this.options.padding.left
                            , e = this.options.padding.right
                            , a = this.options.padding.bottom
                            , p = this.options.context
                            , h = this.options.canvas
                            , l = this.options.xaxis
                            , c = (this.options.series.data,
                            this.options.unit_w_len)
                            , d = this.options.dpr
                            , u = this.options.coordinate
                            , m = u.max
                            , f = u.min
                            , v = u.stepHeight
                            , g = this.options.sepeNum;
                        h.height - t - a
                    }
                    p.save();
                    var y = s(Math.round(h.height - a))
                        , A = s(Math.round(t))
                        , x = s(Math.round(o))
                        , w = s(Math.round(h.width - e));
                    p.strokeStyle = "#C9C9C9",
                        p.beginPath(),
                        p.moveTo(x, y),
                        p.lineTo(w, y),
                        p.moveTo(x, y),
                        p.lineTo(x, A),
                        p.moveTo(x, A),
                        p.lineTo(w, A),
                        p.moveTo(w, y),
                        p.lineTo(w, A),
                        p.stroke();
                    var _ = (y - A) / g;
                    p.textBaseline = "top";
                    for (var b = 0, T = l.length; T > b; b++) {
                        var M = p.measureText(l[b].value).width;
                        if (l[b].show)
                            if (this.options.angle && 0 != this.options.angle) {
                                var k = Math.cos(2 * Math.PI / 360 * this.options.angle) * M;
                                r(l[b].value, p, s(x + b * c + c / 2 - k / 2), s(y + 10 * d), this.options.angle)
                            } else
                                p.fillText(l[b].value, s(x + b * c + c / 2 - M / 2), s(y + 10 * d))
                    }
                    for (b = 1,
                             T = g; T > b; b++) {
                        b == m / v ? (p.beginPath(),
                            p.moveTo(x, s(_ * b + t)),
                            p.lineTo(w, s(_ * b + t)),
                            p.stroke()) : (p.beginPath(),
                            n(p, x, Math.round(_ * b + t), w, Math.round(_ * b + t), 3, 2))
                    }
                    this.options.coordinateMaxY;
                    p.textAlign = "end";
                    for (b = 0; g >= b; b++)
                        p.beginPath(),
                            p.textBaseline = "middle",
                            p.fillText(i(f + b * v, v), o - 5, _ * (g - b) + t)
                }
                ,
                t
        }();
        t.exports = p
    }
    , function(t, i, o) {
        function e() {
            var t = this.options.series
                , i = this.options.unit_w_len
                , o = this.options.unit_w_kind
                , e = this.options.coordinate
                , n = e.max
                , s = e.min
                , a = e.stepHeight
                , r = this.options.sepeNum
                , p = this.options.canvas
                , h = this.options.context
                , l = this.options.padding.top
                , c = this.options.padding.left
                , d = (this.options.padding.right,
                this.options.padding.bottom)
                , u = p.height - d - l
                , m = l + n / a * u / r;
            h.beginPath(),
                h.save(),
                h.lineWidth = this.options.dpr;
            for (var f = 0, v = t.data.length; v > f; f++) {
                h.fillStyle = t.colors && t.colors[f] ? t.colors[f] : this.options.series.color;
                var g = o
                    , y = u * (t.data[f] / (n - s))
                    , A = f * i + c + (i - o) / 2
                    , x = m - y;
                h.fillRect(Math.round(A), Math.round(x), Math.round(g), Math.round(y))
            }
            h.restore()
        }
        o(9);
        t.exports = e
    }
    , function(t) {
        function i(t, i) {
            function o(t, i) {
                var o = T * (f.data[t] / (x - w))
                    , e = t * v + (v - g) / 2 + d
                    , n = o > 0 ? M - o : M
                    , s = g;
                o = Math.abs(o),
                    p.fillStyle = "white",
                    p.fillRect(Math.round(e), Math.round(n), Math.round(s), Math.round(o)),
                    p.fillStyle = i,
                    p.fillRect(Math.round(e), Math.round(n), Math.round(s), Math.round(o))
            }
            var e, n, s, a, r = this.options.dpr, p = this.options.context, h = t * r, l = i * r, c = this.options.padding.top, d = this.options.padding.left, u = this.options.padding.right, m = this.options.padding.bottom, f = this.options.series, v = this.options.unit_w_len, g = this.options.unit_w_kind, y = this.options.canvas, A = this.options.coordinate, x = A.max, w = A.min, _ = A.stepHeight, b = this.options.sepeNum, T = y.height - c - m, M = c + x / _ * T / b, k = Math.floor((h - d) / v), C = !1;
            if (0 > k || k >= f.data.length ? C = !1 : (e = T * (f.data[k] / (x - w)),
                n = g,
                s = k * v + (v - g) / 2 + d,
                a = e > 0 ? M - e : M,
            h >= s && s + n >= h && l >= a && l <= a + Math.abs(e) && (C = !0)),
                C)
                if (void 0 !== this.options.preColume && o(this.options.preColume, f.colors && f.colors[this.options.preColume] ? f.colors[this.options.preColume] : f.color),
                    this.options.preColume = k,
                    o(k, f.hoverColors && f.hoverColors[this.options.preColume] ? f.hoverColors[this.options.preColume] : f.hoverColor),
                    this.options.tipPanel) {
                    var N, E, S = this.options.tipPanel, D = (M - e) / r, L = s / r + g / r / 2, R = e > 0 ? g / r / 2 - S.clientHeight : -g / r / 2;
                    S.children[0].innerHTML = this.options.xaxis[k].value,
                        S.children[1].innerHTML = (void 0 === f.name ? "" : f.name + ":") + f.data[k] + (void 0 === f.suffix ? "" : f.suffix),
                        E = c > D + R ? c / r + 10 : D + R > y.height - m ? m / r - S.clientHeight - 10 : D + R,
                        N = L * r > (y.width - u) / 2 ? s / r + g / r / 2 - S.clientWidth : s / r + g / r / 2,
                        "hidden" === this.options.tipPanel.style.visibility ? (S.style.top = E + "px",
                            S.style.left = N + "px",
                            this.options.tipPanel.style.visibility = "visible") : (S.style.top = E + "px",
                            S.style.left = N + "px")
                } else {
                    var S = document.createElement("div")
                        , I = document.createElement("strong")
                        , Y = document.createElement("div");
                    I.innerHTML = this.options.xaxis[k].value,
                        Y.innerHTML = (void 0 === f.name ? "" : f.name + ":") + f.data[k] + (void 0 === f.suffix ? "" : f.suffix),
                        S.appendChild(I),
                        S.appendChild(Y),
                        this.container.appendChild(S),
                        this.options.tipPanel = S,
                        S.style.position = "absolute",
                        S.style.mineHeight = "30px",
                        S.style.paddingRight = "10px",
                        S.style.opacity = "0.5",
                        S.style.backgroundColor = "#4C4C4C",
                        S.style.borderRadius = "5px",
                        S.style.padding = "5px",
                        S.style.color = "white",
                        I.style.whiteSpace = "nowrap",
                        Y.style.margin = "0px";
                    var D = (M - e) / r
                        , R = e > 0 ? g / r / 2 - S.clientHeight : -g / r / 2
                        , L = s / r + g / r / 2;
                    S.style.top = c > D + R ? c / r + 10 + "px" : D + R > y.height - m ? m / r - S.clientHeight - 10 + "px" : D + R + "px",
                        S.style.left = L * r > (y.width - u) / 2 ? s / r + g / r / 2 - S.clientWidth + "px" : s / r + g / r / 2 + "px"
                }
            else
                void 0 !== this.options.preColume && (tempCurrent = this.options.preColume,
                    o(tempCurrent, f.colors && f.colors[tempCurrent] ? f.colors[tempCurrent] : f.color)),
                this.options.tipPanel && (this.options.tipPanel.style.visibility = "hidden")
        }
        t.exports = i
    }
    , function(t, i, o) {
        var e = o(142)
            , n = o(7)
            , s = o(143)
            , a = o(21)
            , r = o(22)
            , p = o(13)
            , h = o(38)
            , l = function() {
            function t(t) {
                this.defaultoptions = n.defaulttheme,
                    this.options = a(this.defaultoptions, t),
                    this.container = document.getElementById(t.container),
                    this.container.className = this.container.className + " emcharts-container",
                    this.onChartLoaded = void 0 == t.onChartLoaded ? function() {}
                        : t.onChartLoaded
            }
            function i(t, i) {
                var o = t / i
                    , e = o * (1 - this.options.groupSpacing);
                return {
                    rect_w: o,
                    bar_w: e
                }
            }
            function o(t, i) {
                var o = t / i
                    , e = o * (1 - this.options.groupUnitSpacing);
                return {
                    rect_w: o,
                    bar_w: e
                }
            }
            function l(t) {
                var i = this.options.data.max - this.options.data.min;
                if (t >= 0 && this.options.data.min < 0) {
                    var o = this.options.c_1_height * this.options.data.max / i;
                    return o - this.options.c_1_height * t / i
                }
                if (t >= 0 && this.options.data.min >= 0) {
                    var o = this.options.c_1_height;
                    return o - this.options.c_1_height * (t - this.options.data.min) / i
                }
                if (0 > t && this.options.data.max >= 0) {
                    var e = this.options.c_1_height * this.options.data.max / i;
                    return this.options.c_1_height * Math.abs(t) / i + e
                }
                return 0 > t && this.options.data.max < 0 ? this.options.c_1_height * Math.abs(t) / i + 0 : void 0
            }
            function c(t, i) {
                var o = this.options.group
                    , e = this.options.groupUnit
                    , n = this.options.padding_left
                    , s = this.options.group.rect_w - this.options.group.bar_w
                    , a = this.options.groupUnit.rect_w - this.options.groupUnit.bar_w;
                return o.rect_w * t + n + e.rect_w * i + s / 2 + a / 2
            }
            function d(t) {
                var i = t * this.options.dpr
                    , o = {}
                    , e = this.options.padding_left
                    , n = this.options.group
                    , s = this.options.groupUnit
                    , a = (this.options.canvas,
                    this.options.series)
                    , r = Math.floor((i - e) / n.rect_w);
                0 > r && (r = 0);
                var p = Math.floor((i - e - r * n.rect_w - (n.rect_w - n.bar_w) / 2) / s.rect_w);
                return 0 > p ? p = 0 : p > 3 && (p = 3),
                    o.midddleLine = c.call(this, r, p) + s.bar_w / 2,
                    o.tipsX = this.options.padding_left * this.options.dpr + n.rect_w * r,
                    o.tipsY = l.call(this, a[r].data[0]) + this.options.canvas_offset_top * this.options.dpr,
                    o.midddleLineHeight = o.tipsY,
                    o.content = {},
                    o.content.series = this.options.series[r].data,
                    o.content.colors = this.options.xaxis[r],
                    o.content.names = this.options.xaxis[r].names,
                    o.content.suffixs = this.options.xaxis[r].suffixs,
                    o.arr = r + ":" + p,
                    o
            }
            function u(t) {
                for (var i = t.length, o = [], e = 0; i > e; e++)
                    o = o.concat(t[e].data);
                var n = h(this.options.sepeNum, o)
                    , s = this.options.context
                    , a = s.measureText(p.format_unit(n.stepHeight)).width - s.measureText(p.format_unit(parseInt(n.stepHeight))).width
                    , r = s.measureText(p.format_unit(parseInt(n.max))).width
                    , l = s.measureText(p.format_unit(parseInt(n.min))).width
                    , c = r > l ? r : l
                    , d = c + a;
                return {
                    max: n.max,
                    min: n.min,
                    step: n.stepHeight,
                    maxPaddingLeftWidth: d
                }
            }
            return t.prototype.init = function() {
                this.options.type = "group-bar";
                var t = document.createElement("canvas");
                this.container.appendChild(t),
                    this.container.style.position = "relative";
                var i;
                try {
                    i = t.getContext("2d")
                } catch (o) {
                    t = window.G_vmlCanvasManager.initElement(t),
                        i = t.getContext("2d")
                }
                this.options.canvas = t,
                    this.options.context = i,
                    this.options.sepeNum = void 0 == this.options.sepeNum ? 4 : this.options.sepeNum,
                this.options.sepeNum < 2 && (this.options.sepeNum = 2);
                var e = this.options.dpr = 1;
                t.width = this.options.width * e,
                    t.height = this.options.height * e,
                    this.options.canvas_offset_top = 15 * e;
                var n = this.options.xaxis;
                if (this.options.angle && 0 != this.options.angle) {
                    var s = n[0].value
                        , a = i.measureText(s).width * Math.sin(2 * Math.PI / 360 * this.options.angle) + 40;
                    this.options.c_1_height = this.options.canvas.height - a * e
                } else
                    this.options.c_1_height = t.height - 50 * e;
                t.style.width = this.options.width + "px",
                    t.style.height = this.options.height + "px",
                    t.style.border = "0";
                var p = this.options.series
                    , h = u.apply(this, [p]);
                h.min < 0 && (this.options.isLessZero = !0),
                    this.options.data = {},
                    this.options.data.max = h.max,
                    this.options.data.min = h.min,
                    this.options.data.step = h.step;
                var l = Math.abs(h.max) > Math.abs(h.min) ? h.max : h.min;
                this.options.padding_left = i.measureText(l).width * e + 20,
                    this.options.drawWidth = t.width - 10,
                    i.translate("0", this.options.canvas_offset_top),
                    i.font = this.options.font_size * this.options.dpr + "px Arial",
                    i.lineWidth = 1 * this.options.dpr,
                    this.options.groupSpacing = void 0 == this.options.groupSpacing ? "0.2" : this.options.groupSpacing,
                    this.options.groupUnitSpacing = void 0 == this.options.groupUnitSpacing ? "0.2" : this.options.groupUnitSpacing,
                    r.apply(this, [this.options.context, this.options.padding_left + 95 * e, 10 * e, 82 * e, 20 * e])
            }
                ,
                t.prototype.draw = function() {
                    this.clear(),
                        this.init(),
                        this.options.group = i.call(this, this.options.drawWidth - this.options.padding_left, this.options.series.length),
                        this.options.groupUnit = o.call(this, this.options.group.bar_w, this.options.series[0].data.length),
                        new e(this.options),
                        new s(this.options),
                        this.addInteractive()
                }
                ,
                t.prototype.addInteractive = function() {
                    var t, i = this.options.canvas, o = this.options.group, e = this, n = document.createElement("div"), s = document.createElement("div"), a = {}, r = "x:x", h = this.options.dpr, l = this.options.padding_left, c = this.options.canvas_offset_top, u = this.options.c_1_height;
                    n.className = "web-tips",
                        s.className = "group-bar-mark",
                        s.style.width = o.rect_w / h + "px",
                        s.style.backgroundColor = "#333",
                        e.container.appendChild(n),
                        e.container.appendChild(s),
                        p.addEvent.call(e, i, "mousemove", function(m) {
                            var f, v;
                            if (m.layerX ? (f = m.layerX,
                                v = m.layerY) : m.x && (f = m.x,
                                v = m.y),
                                f >= l / h && f * h < e.options.drawWidth && v >= c / h && (c + u) / h > v ? (n.style.display = "inline-block",
                                    s.style.display = "inline-block") : (n.style.display = "none",
                                    s.style.display = "none"),
                            f * h < e.options.drawWidth && (t = d.call(e, f)),
                            r !== t.arr) {
                                a.midddleLine = p.canvasToWindow.call(e, i, t.midddleLine, 0),
                                    a.tips = p.canvasToWindow.call(e, i, t.tipsX, t.tipsY);
                                var g = t.content.series
                                    , y = t.content.colors
                                    , A = t.content.names
                                    , x = t.content.suffixs;
                                n.innerHTML = "";
                                var w = document.createElement("div");
                                w.innerHTML = y.value,
                                    n.appendChild(w);
                                for (var _ = g.length, b = 0; _ > b; b++) {
                                    var T = g[b]
                                        , M = document.createElement("span");
                                    M.className = "bar-color-span",
                                        M.style.backgroundColor = y.colors[b];
                                    var k = document.createElement("span");
                                    k.className = "bar-value-span",
                                        k.innerHTML = (void 0 === A ? "" : A[b]) + T + (void 0 === x ? "" : x[b]);
                                    var C = document.createElement("div");
                                    C.className = "",
                                        C.appendChild(M),
                                        C.appendChild(k),
                                        n.appendChild(C)
                                }
                                var N = m.offsetX || m.clientX - e.container.getBoundingClientRect().left
                                    , E = m.offsetY || m.clientY - e.container.getBoundingClientRect().top;
                                n.style.left = N > i.width / 2 / h ? t.tipsX / h - n.clientWidth + "px" : (t.tipsX + e.options.group.rect_w) / h + "px",
                                    n.style.top = t.tipsY / h - n.clientHeight / 2 + "px";
                                var S = p.windowToCanvas.apply(e, [i, N, E])
                                    , D = S.x.toFixed(0);
                                if (D - e.options.padding_left > 0) {
                                    var L = Math.floor((D - e.options.padding_left) / o.rect_w);
                                    s.style.height = u / e.options.dpr + "px",
                                        s.style.left = (L * o.rect_w + e.options.padding_left) / h + "px",
                                        s.style.top = c / h + "px"
                                }
                                r = t.arr
                            }
                        })
                }
                ,
                t.prototype.reDraw = function() {
                    this.clear(),
                        this.init(),
                        this.draw()
                }
                ,
                t.prototype.clear = function(t) {
                    this.container ? this.container.innerHTML = "" : document.getElementById(this.options.container).innerHTML = "",
                    t && t()
                }
                ,
                t
        }();
        t.exports = l
    }
    , function(t, i, o) {
        var e = o(21)
            , n = o(7)
            , s = (o(13),
            o(8))
            , a = o(9)
            , r = o(56)
            , p = function() {
            function t(t) {
                this.defaultoptions = n.draw_xy,
                    this.options = e(this.defaultoptions, t),
                    this.draw()
            }
            function i(t, i, o, e) {
                function n(t) {
                    return t && (t = parseFloat(t.toFixed(r.options.maxDot || 4))),
                        t
                }
                t.save();
                var r = this
                    , p = e.length;
                t.fillStyle = "#979797",
                    t.textAlign = "right";
                for (var h, l = 0; h = e[l]; l++) {
                    t.beginPath();
                    var c = !0
                        , d = Math.abs(Math.abs(this.options.data.max) > Math.abs(this.options.data.min) ? this.options.data.max : this.options.data.min)
                        , u = d > 1 ? 2 : 3;
                    if (u = d > 1e4 ? 0 : 2,
                    this.options.data.min < 0)
                        if (this.options.data.min + this.options.data.step * l < 0) {
                            var m = (this.options.data.min + n(this.options.data.step * l)).toFixed(u);
                            0 == parseFloat(m) && (m = "0"),
                                t.fillText(m, a(this.options.padding_left - 5), a(h.y + 5))
                        } else if (this.options.data.min + this.options.data.step * l == 0)
                            t.fillText(0, a(this.options.padding_left - 5), a(h.y + 5)),
                                c = !1,
                                t.strokeStyle = "#c9c9c9",
                                t.moveTo(a(this.options.padding_left), a(h.y)),
                                t.lineTo(a(this.options.drawWidth), a(h.y)),
                                t.stroke();
                        else {
                            var m = (this.options.data.min + n(this.options.data.step * l)).toFixed(u);
                            t.fillText(m, a(this.options.padding_left - 5), a(h.y + 5))
                        }
                    else {
                        var m = "" + h.num;
                        m.length > 6 && (m = parseFloat(m).toFixed(4));
                        var f = h.x + this.options.padding_left - 5
                            , v = h.y;
                        t.fillText(m, f, v)
                    }
                    0 != l && l != p - 1 && c && (t.save(),
                        t.beginPath(),
                        t.strokeStyle = "#e6e6e6",
                        s(t, this.options.padding_left, Math.round(h.y), this.options.drawWidth, Math.round(h.y), 3),
                        t.restore())
                }
                t.restore()
            }
            function o(t, i, o) {
                t.save();
                var e = this.options.padding_left
                    , n = this.options.dpr;
                t.beginPath(),
                    t.strokeStyle = "#c9c9c9",
                    t.rect(a(e), .5, Math.round(this.options.drawWidth - e), Math.round(this.options.c_1_height)),
                    t.stroke(),
                    t.textAlign = "left",
                    t.fillStyle = "#979797";
                for (var s, p = this.options.drawWidth, h = o.length, l = (p - e) / h, c = 0; h > c; c++) {
                    t.beginPath(),
                        s = o[c].value;
                    {
                        var d = t.measureText(s).width
                            , u = Math.cos(2 * Math.PI / 360 * this.options.angle) * d
                            , m = c * (p - e) / h + e;
                        void 0 == o[c].show ? !0 : !1
                    }
                    if (void 0 == o[c].show || o[c].show) {
                        var f = c * l + e + l / 2;
                        this.options.angle && 0 != this.options.angle ? r(s, t, a(f - u / 2), a(this.options.c_1_height + 20 * n) - 10, this.options.angle) : t.fillText(s, a(f - d / 2), a(this.options.c_1_height + 20 * n))
                    }
                    if (c == h - 1) {
                        t.moveTo(a(m), a(this.options.c_1_height)),
                            t.lineTo(a(m), a(this.options.c_1_height + 5 * n));
                        var m = (c + 1) * (p - e) / h + e;
                        t.moveTo(a(m), a(this.options.c_1_height)),
                            t.lineTo(a(m), a(this.options.c_1_height + 5 * n))
                    } else
                        t.moveTo(a(m), a(this.options.c_1_height)),
                            t.lineTo(a(m), a(this.options.c_1_height + 5 * n));
                    t.stroke()
                }
                t.stroke(),
                    t.restore()
            }
            function p(t, i, o, e) {
                for (var n = this.options.data.step, s = [], a = 0; o >= a; a++)
                    s.push({
                        num: parseFloat(i) + parseFloat(a * n),
                        x: 0,
                        y: e - a / o * e
                    });
                return s
            }
            return t.prototype.draw = function() {
                var t = this.options.context
                    , e = this.options.data.max
                    , n = this.options.data.min
                    , s = this.options.sepeNum
                    , a = this.options.xaxis
                    , r = this.options.c_1_height
                    , h = p.apply(this, [e, n, s, r]);
                i.call(this, t, e, n, h),
                    o.apply(this, [t, r, a])
            }
                ,
                t
        }();
        t.exports = p
    }
    , function(t, i, o) {
        var e = o(21)
            , n = o(7)
            , s = function() {
            function t(t) {
                this.defaultoptions = n.drawLine,
                    this.options = e(this.defaultoptions, t),
                    this.draw()
            }
            function i(t) {
                var i = this.options.data.max - this.options.data.min;
                if (t >= 0 && this.options.data.min < 0) {
                    var o = this.options.c_1_height * this.options.data.max / i;
                    return o - this.options.c_1_height * t / i
                }
                if (t >= 0 && this.options.data.min >= 0) {
                    var o = this.options.c_1_height;
                    return o - this.options.c_1_height * (t - this.options.data.min) / i
                }
                if (0 > t && this.options.data.max >= 0) {
                    var e = this.options.c_1_height * this.options.data.max / i;
                    return this.options.c_1_height * Math.abs(t) / i + e
                }
                return 0 > t && this.options.data.max < 0 ? this.options.c_1_height * Math.abs(t) / i + 0 : void 0
            }
            function o(t, i) {
                var o = this.options.group
                    , e = this.options.groupUnit
                    , n = this.options.padding_left
                    , s = this.options.group.rect_w - this.options.group.bar_w
                    , a = this.options.groupUnit.rect_w - this.options.groupUnit.bar_w;
                return o.rect_w * t + n + e.rect_w * i + s / 2 + a / 2
            }
            return t.prototype.draw = function() {
                var t = this.options.context;
                t.lineWidth = 1 * this.options.dpr + 1;
                for (var e, n = this.options.series, s = this.options.xaxis, a = 0; e = n[a]; a++)
                    for (var r = e.data, p = r.length, h = 0; p > h; h++) {
                        var l = r[h];
                        if (null != l && "" !== l && void 0 != l) {
                            t.beginPath(),
                                t.fillStyle = void 0 == s[a].colors[h] ? "#333" : s[a].colors[h],
                                t.strokeStyle = void 0 == s[a].colors[h] ? "#333" : s[a].colors[h];
                            var c = o.apply(this, [a, h])
                                , d = i.call(this, l);
                            if (d >= 0 && this.options.data.min < 0) {
                                var u = this.options.c_1_height * this.options.data.max / (this.options.data.max - this.options.data.min);
                                t.rect(c, d, this.options.groupUnit.bar_w, u - d)
                            } else if (d >= 0 && this.options.data.min >= 0) {
                                var u = this.options.c_1_height;
                                t.rect(c, d, this.options.groupUnit.bar_w, u - d)
                            } else if (0 > d && this.options.data.max >= 0) {
                                var u = this.options.c_1_height * this.options.data.max / (this.options.data.max - this.options.data.min);
                                t.rect(c, u, this.options.groupUnit.bar_w, d)
                            } else if (0 > d && this.options.data.max < 0) {
                                var u = 0;
                                t.rect(c, u, this.options.groupUnit.bar_w, d)
                            }
                            t.fill()
                        }
                    }
            }
                ,
                t
        }();
        t.exports = s
    }
    , function(t, i, o) {
        var e = o(21)
            , n = o(7)
            , s = o(22)
            , a = o(13)
            , r = o(145)
            , p = function() {
            function t(t) {
                this.options = {},
                    this.options = e(n.defaulttheme, t),
                    this.container = document.getElementById(t.container),
                    this.container.className = this.container.className.replace(/emcharts-container/g, "").trim(),
                    this.container.className = this.container.className + " emcharts-container",
                    this.onChartLoaded = void 0 == t.onChartLoaded ? function() {}
                        : t.onChartLoaded
            }
            return t.prototype.init = function() {
                var t = document.createElement("canvas")
                    , i = document.createElement("canvas");
                this.container.appendChild(t),
                    this.container.appendChild(i),
                    this.container.style.position = "relative",
                void 0 === this.options.dpr && (this.options.dpr = 2);
                try {
                    var o = t.getContext("2d")
                        , e = i.getContext("2d")
                } catch (n) {
                    t = window.G_vmlCanvasManager.initElement(t);
                    var o = t.getContext("2d");
                    i = window.G_vmlCanvasManager.initElement(i);
                    var e = i.getContext("2d");
                    this.options.dpr = 1
                }
                this.options.canvas = t,
                    this.options.context = o,
                    this.options.canvas2 = i,
                    this.options.context2 = e;
                var a = this.options.dpr;
                t.width = this.options.width * a,
                    t.height = this.options.height * a,
                    i.width = this.options.width * a,
                    i.height = this.options.height * a,
                    t.style.width = this.options.width + "px",
                    t.style.height = this.options.height + "px",
                    t.style.border = "0",
                    i.style.width = this.options.width + "px",
                    i.style.height = this.options.height + "px",
                    i.style.border = "0",
                    i.style.position = "absolute",
                    i.style.top = "0px",
                    i.style.left = "0px",
                this.options.font || (this.options.font = "12px Arial");
                var r = this.options.font.split("px ")[0] * a + "px " + this.options.font.split("px ")[1];
                o.font = r,
                    e.font = r,
                    this.options.startOffset = this.options.startOffset || Math.PI / 2;
                var p = this.options.font.split("px ")[0] * a;
                this.options.ySpace = p,
                    this.options.onPie = this.options.onPie || !1,
                    s.apply(this, [o, 130 * a, 20 * a, 82 * a, 20 * a])
            }
                ,
                t.prototype.draw = function(t) {
                    this.init();
                    for (var i = this.options.startOffset, o = this.options.context, e = this.options.dpr, n = this.options.context2, s = i, a = 0, p = this, h = this.options.data, l = this.options.ySpace, c = {
                        x: p.options.point.x * e,
                        y: p.options.point.y * e
                    }, d = this.options.radius * e, u = n.font.split("px ")[0], m = this.options.onPie, f = [], v = 0, g = h.length; g > v; v++)
                        (h[v].show || void 0 === h[v].show) && (a += parseFloat(h[v].value));
                    for (v = 0; v < h.length; v++)
                        if (h[v].show || void 0 === h[v].show) {
                            var y = s
                                , A = s + h[v].value / a * 2 * Math.PI;
                            s += h[v].value / a * 2 * Math.PI;
                            var x = (y + A) / 2;
                            f.push({
                                value: h[v].value,
                                info: h[v].info || h[v].name,
                                tip: h[v].tip || h[v].value,
                                name: h[v].name || "",
                                start: y,
                                end: A,
                                middle: x,
                                color: h[v].color,
                                showInfo: h[v].showInfo,
                                id: v,
                                isOut: !1
                            })
                        }
                    var w = c.y - d - d / 10
                        , _ = c.y + d + d / 10;
                    for (f = r.getSpaceArry(f, Math.floor((_ - w) / l), l, d, c, w),
                             f.sort(function(t, i) {
                                 return t.start - i.start
                             }),
                             this.options.pies = f,
                             v = 0; v < f.length; v++) {
                        var b = f[v].start
                            , T = f[v].end
                            , M = (f[v].middle,
                            f[v].color);
                        r.drawPie(o, c, d, b, T, M),
                            m ? r.drawInfoOn(n, f[v], f, d, c, u) : r.drawInfo(n, f[v], d, c, l)
                    }
                    this.addInteractive(),
                    t && t()
                }
                ,
                t.prototype.addInteractive = function() {
                    var t = (this.options.canvas,
                        this.options.canvas2)
                        , i = (this.container,
                        this)
                        , o = this.options.pies
                        , e = (this.options.dpr,
                        {
                            x: i.options.point.x,
                            y: i.options.point.y
                        })
                        , n = this.options.radius
                        , s = this.options.startOffset;
                    a.addEvent.call(i, t, "click", function(t) {
                        var a = t.offsetX || t.clientX - i.container.getBoundingClientRect().left
                            , p = t.offsetY || t.clientY - i.container.getBoundingClientRect().top
                            , h = a - e.x
                            , l = p - e.y
                            , c = 0;
                        if (c = r.getTheta(h, l, s),
                            i.options.prePieClick) {
                            var d = i.options.prePieClick;
                            if (c >= d.start && c <= d.end) {
                                if (h * h + l * l > (n + 15) * (n + 15))
                                    return
                            } else if (h * h + l * l > n * n)
                                return
                        } else if (h * h + l * l > n * n)
                            return;
                        for (var u = 0, m = o.length; m > u; u++)
                            if (c <= o[u].end) {
                                var f = o[u];
                                f.clicked = !0,
                                    r.pieHandlerClick.call(i, f);
                                break
                            }
                        try {
                            t.preventDefault()
                        } catch (v) {
                            t.returnValue = !1
                        }
                    }),
                        a.addEvent.call(i, t, "mousemove", function(t) {
                            var a, p;
                            t.layerX ? (a = t.layerX,
                                p = t.layerY) : t.x && (a = t.x,
                                p = t.y);
                            var h = a - e.x
                                , l = p - e.y
                                , c = 0
                                , d = !0;
                            h * h + l * l > n * n && (d = !1),
                                c = r.getTheta(h, l, s);
                            for (var u = 0, m = o.length; m > u; u++)
                                if (c <= o[u].end) {
                                    var f = o[u];
                                    f.clicked = !0,
                                        r.pieHandlerMove.call(i, f, a, p, d);
                                    break
                                }
                            try {
                                t.preventDefault()
                            } catch (v) {
                                t.returnValue = !1
                            }
                        })
                }
                ,
                t.prototype.reDraw = function() {
                    this.clear(),
                        this.draw()
                }
                ,
                t.prototype.clear = function(t) {
                    this.container ? this.container.innerHTML = "" : document.getElementById(this.options.container).innerHTML = "",
                        this.options.tips = null,
                    t && t()
                }
                ,
                t
        }();
        t.exports = p
    }
    , function(t, i, o) {
        var e = o(146)
            , n = o(147)
            , s = o(148)
            , a = o(149)
            , r = o(152)
            , p = o(153)
            , h = o(156);
        t.exports = {
            drawInfo: e,
            drawInfoOn: n,
            drawPie: s,
            getSpaceArry: a,
            getTheta: r,
            pieHandlerClick: p,
            pieHandlerMove: h
        }
    }
    , function(t) {
        t.exports = function(t, i, o, e, n, s) {
            var a = e.y - o - o / 10
                , r = Math.cos(i.middle) / Math.abs(Math.cos(i.middle))
                , p = e.x + o * Math.cos(i.middle)
                , h = e.y + o * Math.sin(i.middle)
                , l = a + i.yIndex * n
                , c = Math.asin((l - e.y) / (o + o / 10))
                , d = e.x + r * (o + o / 10) * Math.cos(c)
                , u = d + o / 10 * r;
            if (0 === i.yIndex) {
                var m = o / 10 > 3 ? o / 10 : 3;
                d = u + m * r,
                    u = d + m * r
            }
            l += Math.sin(i.middle) / Math.abs(Math.sin(i.middle)) * n / 2;
            var f = l
                , v = t.measureText(i.name).width
                , g = 1 * t.font.split("px ")[0]
                , y = g;
            if (t.save(),
                t.beginPath(),
                t.fillStyle = "#666",
                t.textAlign = Math.cos(i.middle) > 0 ? "start" : "end",
                t.textBaseline = "middle",
                s)
                switch (s) {
                    case 1:
                        t.fillStyle = "#666",
                            t.fillText(i.info, u + (Math.cos(i.middle) > 0 ? 5 : -5), f),
                            t.fillStyle = i.color;
                        break;
                    case 2:
                        t.fillStyle = i.color,
                            t.arc(u, f, 2, 0, 2 * Math.PI),
                            t.fill(),
                            t.lineWidth = 1,
                            t.beginPath(),
                            t.strokeStyle = i.color,
                            t.moveTo(p, h),
                            t.lineTo(d, l),
                            t.lineTo(u, f),
                            t.stroke();
                        break;
                    case 3:
                        t.fillStyle = "#fff",
                            t.fillRect(u + (Math.cos(i.middle) > 0 ? 5 : -v - 5), f - y / 2, v, y);
                        break;
                    case 4:
                        t.strokeStyle = "#fff",
                            t.lineWidth = 2,
                            t.moveTo(p, h),
                            t.lineTo(d, l),
                            t.lineTo(u, f),
                            t.stroke()
                }
            else
                t.fillStyle = "#666",
                    t.fillText(i.info, u + (Math.cos(i.middle) > 0 ? 5 : -5), f),
                    t.fillStyle = i.color,
                    t.arc(u + 2 * r, f, 2, 0, 2 * Math.PI),
                    t.fill(),
                    t.lineWidth = 1,
                    t.beginPath(),
                    t.strokeStyle = i.color,
                    t.moveTo(p, h),
                    t.lineTo(d, l),
                    t.lineTo(u, f),
                    t.stroke();
            t.restore()
        }
    }
    , function(t) {
        t.exports = function(t, i, o, e, n, s, a) {
            var r = n.x + (e - 20) * Math.cos(i.middle)
                , p = n.y + (e - 20) * Math.sin(i.middle)
                , h = Math.cos(i.middle) >= 0 ? !0 : !1
                , l = t.font.split("px ")[1].trim();
            if (i.showInfo) {
                if (t.beginPath(),
                    t.save(),
                    t.font = s + "px " + l,
                    t.textBaseline = "middle",
                    t.textAlign = h ? "start" : "end",
                    a) {
                    t.clearRect(0, 0, t.canvas.width, t.canvas.height);
                    for (var c = 0, d = o.length; d > c; c++)
                        if (o[c].id !== i.id) {
                            var u = Math.cos(o[c].middle) >= 0 ? !0 : !1;
                            t.textAlign = u ? "start" : "end";
                            var m = n.x + (e - 20) * Math.cos(o[c].middle)
                                , f = n.y + (e - 20) * Math.sin(o[c].middle);
                            t.fillText(o[c].info, m, f)
                        }
                } else
                    t.fillText(i.info, r, p);
                t.restore()
            }
        }
    }
    , function(t) {
        t.exports = function(t, i, o, e, n, s, a) {
            t.save(),
                t.beginPath(),
                t.fillStyle = s || "white",
                t.strokeStyle = "white";
            var r = e
                , p = n;
            Math.abs(e - n) == 2 * Math.PI && (r = 0,
                p = 2 * Math.PI),
                t.moveTo(i.x, i.y),
                t.lineTo(i.x + o * Math.cos(r), i.y + o * Math.sin(r)),
                t.arc(i.x, i.y, o, r, p, !1),
                t.lineTo(i.x, i.y),
            a && t.stroke(),
                t.fill(),
                t.restore()
        }
    }
    , function(t, i, o) {
        var e = o(150)
            , n = o(151);
        t.exports = function(t, i, o, s, a, r) {
            for (var p = [], h = [], l = 0, c = t.length; c > l; l++)
                Math.cos(t[l].middle) < 0 ? p.push(t[l]) : h.push(t[l]);
            if (p.length > i) {
                var d = p.length - i
                    , u = n(p, d);
                for (l = 0; d > l; l++)
                    p[u[l]].showInfo = !1
            }
            if (e(p, i, o, s, a, r),
            h.length > i) {
                var m = h.length - i
                    , f = n(h, m);
                for (l = 0; m > l; l++)
                    h[f[l]].showInfo = !1
            }
            return e(h, i, o, s, a, r),
                p.concat(h)
        }
    }
    , function(t) {
        t.exports = function(t, i, o, e, n, s) {
            for (var a = t.length, r = [], p = 0; i > p; p++)
                r[p] = 0;
            for (p = 0; a > p; p++)
                if (t[p].showInfo) {
                    var h = n.y + (e + e / 10) * Math.sin(t[p].middle) - s
                        , l = Math.round(h / o);
                    if (l = Math.sin(t[p].middle) > 0 ? Math.floor(h / o) : Math.ceil(h / o),
                        r[l])
                        for (var c = 1, d = Math.max(l, i - l); d > c; c++) {
                            if (i > l + c && !r[l + c]) {
                                r[l + c] = 1;
                                break
                            }
                            if (i > l - c && !r[l - c] && l - c > 0) {
                                r[l - c] = 1;
                                break
                            }
                        }
                    else
                        r[l] = 1
                }
            for (t.sort(function(t, i) {
                return Math.sin(t.middle) - Math.sin(i.middle)
            }),
                     c = 0,
                     p = 0; i >= p; p++)
                if (r[p])
                    if (t[c].showInfo)
                        t[c++].yIndex = p;
                    else {
                        for (; t[c] && !t[c].showInfo; )
                            c++;
                        t[c++].yIndex = p
                    }
        }
    }
    , function(t) {
        t.exports = function(t, i) {
            for (var o = [], e = t.length, n = 0; i > n; n++)
                o[n] = n;
            for (o.sort(function(i, o) {
                return t[o].value - t[i].value
            }),
                     n = i - 1; e > n; n++)
                for (var s = 0; i > s; s++)
                    if (t[n].value < t[o[s]].value) {
                        o[s] = n,
                            o.sort(function(i, o) {
                                return t[i].value - t[o].value
                            });
                        break
                    }
            return o
        }
    }
    , function(t) {
        t.exports = function(t, i, o) {
            var e = 0;
            return e = i > 0 ? Math.acos(t / Math.sqrt(t * t + i * i)) : 0 > i ? 2 * Math.PI - Math.acos(t / Math.sqrt(t * t + i * i)) : t > 0 ? 0 : 2 * Math.PI,
            0 > e - o && (e += 2 * Math.PI),
                e
        }
    }
    , function(t, i, o) {
        var e = o(154)
            , n = o(155);
        t.exports = function(t) {
            var i = this.options.dpr
                , o = this
                , s = {
                x: o.options.point.x * i,
                y: o.options.point.y * i
            }
                , a = this.options.radius * i
                , r = this.options.context
                , p = this.options.context2
                , h = this.options.ySpace
                , l = this.options.onPie
                , c = this.options.pies;
            this.options.prePieClick ? this.options.prePieClick == t ? (e(r, p, t, c, s, a, h, l),
                t.clicked = !1,
                this.options.prePieClick = null) : (e(r, p, this.options.prePieClick, c, s, a, h, l),
                this.options.prePieClick.clicked = !1,
                n(r, p, t, c, s, a, h, l),
                this.options.prePieClick = t) : (n(r, p, t, c, s, a, h, l),
                this.options.prePieClick = t)
        }
    }
    , function(t, i, o) {
        var e = o(148)
            , n = o(146)
            , s = o(147);
        t.exports = function(t, i, o, a, r, p, h, l) {
            var c = o.start
                , d = o.end
                , u = 1 * i.font.split("px ")[0];
            e(t, r, p + 3 * p / 20, c, d, "white"),
                l ? s(i, o, a, p, r, u, !0) : n(i, o, p, r, h, 3),
                e(t, r, p, c, d, o.color),
                l ? s(i, o, a, p, r, u) : n(i, o, p, r, h)
        }
    }
    , function(t, i, o) {
        var e = o(148)
            , n = o(146)
            , s = o(147);
        t.exports = function(t, i, o, a, r, p, h, l) {
            var c = o.start
                , d = o.end
                , u = (c + d) / 2
                , m = 1 * i.font.split("px ")[0]
                , f = p / 50
                , v = f;
            Math.sin((d - c) / 2) <= 1e-4 ? v = f : (v = f / Math.sin((d - c) / 2),
            v > p / 3 && (v = p / 3)),
                e(t, r, p + p / 20, c, d, "white"),
                l ? s(i, o, a, p, r, m, !0) : n(i, o, a, p, r, h, 3),
                e(t, {
                    x: r.x + v * Math.cos(u),
                    y: r.y + v * Math.sin(u)
                }, 43 * p / 40 - v, c, d, o.color),
                e(t, {
                    x: r.x + v * Math.cos(u),
                    y: r.y + v * Math.sin(u)
                }, 42 * p / 40 - v, c, d, "white", !0),
                e(t, {
                    x: r.x + v * Math.cos(u),
                    y: r.y + v * Math.sin(u)
                }, 41 * p / 40 - v, c, d, o.color),
                l ? s(i, o, a, p + p / 40, r, m + 4) : n(i, o, p, r, h, 1)
        }
    }
    , function(t) {
        t.exports = function(t, i, o, e) {
            var n = this.container;
            if (this.options.tips) {
                var s = this.options.tips;
                s.style.display = e ? "block" : "none",
                    s.children[0].style.backgroundColor = t.color,
                    s.children[1].innerHTML = t.name,
                    s.children[2].innerHTML = t.tip,
                    Math.cos(t.middle) >= 0 ? (s.style.top = o - s.clientHeight - 10 + "px",
                        s.style.left = i + 10 + "px") : (s.style.top = o - s.clientHeight - 10 + "px",
                        s.style.left = i - s.clientWidth - 10 + "px")
            } else {
                var s = document.createElement("div");
                s.className = "chart_line_tips",
                    s.style.fontSize = "12px",
                    s.style.display = "none";
                var a = document.createElement("span");
                a.className = "chart_line_tips_color",
                    a.style.backgroundColor = t.color;
                var r = document.createElement("span");
                r.innerHTML = t.name;
                var p = document.createElement("div");
                p.innerHTML = t.tip,
                    s.appendChild(a),
                    s.appendChild(r),
                    s.appendChild(p),
                    n.appendChild(s),
                    s.style.top = o + "px",
                    s.style.left = i + "px",
                    this.options.tips = s
            }
        }
    }
    , function(t, i, o) {
        var e = o(21)
            , n = o(22)
            , s = o(38)
            , a = o(158)
            , r = o(159)
            , p = o(160)
            , h = o(13)
            , l = function() {
            function t(t) {
                this.options = e(this.options, t),
                    this.container = document.getElementById(t.container),
                    this.container.className = this.container.className.replace(/emcharts-container/g, "").trim(),
                    this.container.className = this.container.className + " emcharts-container"
            }
            return t.prototype.init = function() {
                this.container.style.position = "relative",
                void 0 === this.options.dpr && (this.options.dpr = 1);
                var t = this.options.dpr
                    , i = document.createElement("canvas");
                this.container.appendChild(i),
                    i.width = t * this.options.width,
                    i.height = t * this.options.height,
                    i.style.width = this.options.width + "px",
                    i.style.height = this.options.height + "px",
                    i.style.border = "0px";
                try {
                    var o = i.getContext("2d")
                } catch (e) {
                    i = window.G_vmlCanvasManager.initElement(i);
                    var o = i.getContext("2d")
                }
                var o = i.getContext("2d");
                void 0 === this.options.font_size && (this.options.font_size = 12),
                    o.font = this.options.font_size * t + "px Arial",
                    o.lineWidth = 1 * t,
                    this.options.dpr = t,
                    this.options.canvas = i,
                    this.options.context = o,
                void 0 === this.options.color && (this.options.color = "#6890D5"),
                void 0 === this.options.hoverColor && (this.options.hoverColor = "#7EA1DA"),
                this.options.sepeNum || (this.options.sepeNum = 4);
                var a = this.options.yaxis
                    , r = s(this.options.sepeNum, this.options.series.data);
                this.options.coordinate = r,
                    this.options.padding = {};
                for (var p = 0, h = 0, l = a.length; l > h; h++)
                    p = Math.max(o.measureText(a[h].value).width, p);
                this.options.padding.left = (p + 10) * t;
                var c = o.measureText(r.max).width;
                this.options.padding.right = (c + 10) * t,
                    this.options.padding.top = 2 * this.options.font_size * t,
                    this.options.padding.bottom = 50 * t;
                var d = (i.height - this.options.padding.top - this.options.padding.bottom) / a.length;
                this.options.unitHeight = d,
                    n.apply(this, [this.options.context, 110 + c, 40, 82, 20])
            }
                ,
                t.prototype.draw = function(t) {
                    this.clear(),
                        this.init();
                    var i = this;
                    new a(this.options),
                        r.call(this),
                        h.addEvent(i.container, "mousemove", function(t) {
                            var o, e;
                            t.layerX ? (o = t.layerX,
                                e = t.layerY) : t.x && (o = t.x,
                                e = t.y),
                                p.call(i, o, e);
                            try {
                                t.preventDefault()
                            } catch (n) {
                                t.returnValue = !1
                            }
                        }),
                        h.addEvent(i.container, "mouseleave", function() {
                            void 0 !== i.options.tips && (i.options.tips.style.display = "none",
                                i.options.tips.style.left = "-10000")
                        }),
                    t && t()
                }
                ,
                t.prototype.reDraw = function() {
                    this.clear(),
                        this.init(),
                        this.draw()
                }
                ,
                t.prototype.clear = function(t) {
                    this.container ? this.container.innerHTML = "" : document.getElementById(this.options.container).innerHTML = "",
                    t && t()
                }
                ,
                t
        }();
        t.exports = l
    }
    , function(t, i, o) {
        var e = o(21)
            , n = o(25)
            , s = o(9)
            , a = o(60)
            , r = function() {
            function t(t) {
                this.options = {},
                    this.options = e(this.options, t),
                    this.draw()
            }
            function i(t, i) {
                var o = 1;
                -1 !== i.toString().indexOf(".") && (o = i.toString().length - i.toString().indexOf(".") - 1);
                var e = Math.pow(10, o);
                return a.format_unit(Math.round(t * e) / e, o)
            }
            return t.prototype.init = function() {
                this.options.xSplitShow = !1,
                    this.options.xShowDivide = !1
            }
                ,
                t.prototype.draw = function() {
                    this.init();
                    var t = this.options.padding.top
                        , o = this.options.padding.left
                        , e = this.options.padding.right
                        , a = this.options.padding.bottom
                        , r = this.options.context
                        , p = this.options.canvas
                        , h = this.options.yaxis
                        , l = (this.options.series.data,
                        this.options.unitHeight)
                        , c = this.options.dpr
                        , d = this.options.coordinate
                        , u = (d.max,
                        d.min)
                        , m = d.stepHeight
                        , f = this.options.sepeNum
                        , v = p.width - o - e
                        , g = v / f;
                    r.save();
                    var y = s(Math.round(p.height - a))
                        , A = s(Math.round(t))
                        , x = s(Math.round(o))
                        , w = s(Math.round(p.width - e));
                    r.strokeStyle = "#C9C9C9",
                        r.beginPath(),
                        r.moveTo(x, y),
                        r.lineTo(w, y),
                        r.moveTo(x, y),
                        r.lineTo(x, A),
                        r.moveTo(x, A),
                        r.lineTo(w, A),
                        r.moveTo(w, y),
                        r.lineTo(w, A),
                        r.stroke(),
                        r.save(),
                        r.beginPath(),
                        r.textAlign = "end",
                        r.textBaseline = "middle";
                    for (var _ = 0, b = h.length; b > _; _++) {
                        {
                            r.measureText(h[_].value).width
                        }
                        h[_].show && (r.fillText(h[_].value, o - 8 * c, A + _ * l + l / 2),
                            r.moveTo(o - 4 * c, s(A + _ * l + l / 2)),
                            r.lineTo(o, s(A + _ * l + l / 2)))
                    }
                    for (r.stroke(),
                             r.restore(),
                             _ = 1,
                             b = f; b > _; _++)
                        r.beginPath(),
                            _ == -u / m ? (r.moveTo(s(g * _ + o), A),
                                r.lineTo(s(g * _ + o), y),
                                r.stroke()) : n(r, g * _ + o, A, g * _ + o, y, 3);
                    r.save();
                    this.options.coordinateMaxY;
                    r.textAlign = "center",
                        r.textBaseline = "top";
                    for (r.beginPath(),
                             _ = 0; f >= _; _++)
                        r.fillText(i(u + _ * m, m), s(g * _ + o), y + 7 * c),
                            r.moveTo(s(g * _ + o), y),
                            r.lineTo(s(g * _ + o), y + 5 * c),
                            r.stroke();
                    r.restore()
                }
                ,
                t
        }();
        t.exports = r
    }
    , function(t, i, o) {
        var e = o(9);
        t.exports = function() {
            {
                var t = (this.options.dpr,
                    this.options.canvas)
                    , i = this.options.context
                    , o = this.options.series.data
                    , n = this.options.yaxis
                    , s = this.options.coordinate
                    , a = this.options.sepeNum
                    , r = this.options.unitHeight
                    , p = this.options.padding.left
                    , h = this.options.padding.right
                    , l = this.options.padding.top
                    , c = t.width - p - h
                    , d = e(p + c / a * (-s.min / s.stepHeight))
                    , u = this.options.color;
                this.options.hoverColor
            }
            i.save(),
                i.fillStyle = u,
                i.strokeStyle = u,
                i.beginPath();
            for (var m = 0, f = o.length; f > m; m++) {
                if (i.save(),
                void 0 !== n[m].color && (i.fillStyle = n[m].color,
                    i.strokeStyle = n[m].color),
                o[m] < 0) {
                    var v = Math.round(o[m] / s.min * (d - p));
                    i.fillRect(d - v, e(l + r * (m + .25)), v, Math.round(r / 2))
                } else
                    i.fillRect(d, e(l + r * (m + .25)), Math.round(o[m] / s.max * (c - (d - p))), Math.round(r / 2));
                i.restore()
            }
            i.restore()
        }
    }
    , function(t, i, o) {
        var e = o(9);
        t.exports = function(t, i) {
            function o(t, i) {
                if (a.save(),
                    a.fillStyle = g,
                void 0 !== p[t].color && (a.fillStyle = p[t].color),
                r[t] < 0) {
                    var o = Math.round(r[t] / h.min * (v - d));
                    a.fillRect(v - o, e(m + c * (t + .25)), o, Math.round(c / 2))
                } else
                    a.fillRect(v, e(m + c * (t + .25)), Math.round(r[t] / h.max * (f - (v - d))), Math.round(c / 2));
                if (void 0 !== i)
                    if (a.fillStyle = y,
                    void 0 !== p[t].hoverColor && (a.fillStyle = p[t].hoverColor),
                    r[i] < 0) {
                        var o = Math.round(r[i] / h.min * (v - d));
                        a.fillRect(v - o, e(m + c * (i + .25)), o, Math.round(c / 2))
                    } else
                        a.fillRect(v, e(m + c * (i + .25)), Math.round(r[i] / h.max * (f - (v - d))), Math.round(c / 2));
                a.restore()
            }
            var n = this.options.dpr
                , s = this.options.canvas
                , a = this.options.context
                , r = this.options.series.data
                , p = this.options.yaxis
                , h = this.options.coordinate
                , l = this.options.sepeNum
                , c = this.options.unitHeight
                , d = this.options.padding.left
                , u = this.options.padding.right
                , m = this.options.padding.top
                , f = (this.options.padding.bottom,
            s.width - d - u)
                , v = e(d + f / l * (-h.min / h.stepHeight))
                , g = this.options.color
                , y = this.options.hoverColor
                , A = Math.floor((i * n - m) / c);
            if (A >= 0 && A < r.length) {
                var x = void 0 === this.options.series.prefix ? "" : this.options.series.prefix
                    , w = void 0 === this.options.series.suffix ? "" : this.options.series.suffix
                    , _ = p[A].value
                    , b = x + r[A] + w;
                if (void 0 === this.options.preCursor)
                    this.options.preCursor = A,
                        o(A, A);
                else {
                    var T = this.options.preCursor;
                    this.options.preCursor = A,
                        o(T, A)
                }
                if (this.options.tips) {
                    var M = this.options.tips;
                    d >= t * n || t * n >= d + f ? (this.options.tips.style.display = "none",
                        this.options.tips.style.left = "-10000",
                        o(A)) : M.style.display = "block",
                        M.children[0].innerHTML = _,
                        M.children[1].children[1].innerHTML = b,
                        M.children[1].children[0].style.backgroundColor = void 0 !== p[A].hoverColor ? p[A].hoverColor : y,
                        M.style.left = t * n >= d + f / 2 ? t - M.clientWidth - 20 + "px" : t + 20 + "px",
                        M.style.top = i * n <= m + M.clientHeight * n ? m + "px" : i - M.clientHeight + "px"
                } else {
                    var k = document.createElement("div")
                        , C = document.createElement("div");
                    C.innerHTML = _;
                    var N = document.createElement("div")
                        , E = document.createElement("span")
                        , S = document.createElement("span");
                    S.innerHTML = b,
                        N.appendChild(E),
                        N.appendChild(S),
                        k.appendChild(C),
                        k.appendChild(N),
                        this.container.appendChild(k),
                        this.options.tips = k,
                        k.style.position = "absolute",
                        k.style.font = "12px/150% Simsun,arial,sans-serif",
                        k.style.padding = "5px",
                        k.style.backgroundColor = "#000",
                        k.style.color = "#fff",
                        k.style.borderRadius = "5px",
                        k.style.opacity = "0.7",
                        k.style.filter = "progid:DXImageTransform.Microsoft.Alpha(Opacity=70)",
                        E.style.display = "inline-block",
                        E.style.width = "6px",
                        E.style.height = "6px",
                        E.style.backgroundColor = void 0 !== p[A].hoverColor ? p[A].hoverColor : y,
                        E.style.borderRadius = "50%",
                        E.style.marginRight = "5px",
                        S.style.display = "inline-block",
                        k.style.left = t * n >= d + f / 2 ? t - k.clientWidth - 20 + "px" : t + 20 + "px",
                        k.style.top = i * n <= m + k.clientHeight * n ? m + "px" : i - k.clientHeight + "px"
                }
            } else
                this.options.tips && (this.options.tips.style.display = "none",
                    this.options.tips.style.left = "-10000",
                    A = 0 > A ? 0 : r.length - 1,
                    o(A))
        }
    }
    , function(t, i, o) {
        var e = o(21)
            , n = o(22)
            , s = o(38)
            , a = o(162)
            , r = o(163)
            , p = o(164)
            , h = o(165)
            , l = o(13)
            , c = function() {
            function t(t) {
                this.options = e(this.options, t),
                    this.container = document.getElementById(t.container),
                    this.container.className = this.container.className.replace(/emcharts-container/g, "").trim(),
                    this.container.className = this.container.className + " emcharts-container"
            }
            return t.prototype.init = function() {
                this.container.style.position = "relative",
                void 0 === this.options.dpr && (this.options.dpr = 1);
                var t = this.options.dpr
                    , i = document.createElement("canvas");
                this.container.appendChild(i),
                    i.width = t * this.options.width,
                    i.height = t * this.options.height,
                    i.style.width = this.options.width + "px",
                    i.style.height = this.options.height + "px",
                    i.style.border = "0px";
                try {
                    var o = i.getContext("2d")
                } catch (e) {
                    i = window.G_vmlCanvasManager.initElement(i);
                    var o = i.getContext("2d")
                }
                var o = i.getContext("2d");
                void 0 === this.options.font_size && (this.options.font_size = 12),
                    o.font = this.options.font_size * t + "px Arial",
                    o.lineWidth = 1 * t,
                    this.options.dpr = t,
                    this.options.canvas = i,
                    this.options.context = o,
                void 0 === this.options.color && (this.options.color = "#6890D5"),
                void 0 === this.options.hoverColor && (this.options.hoverColor = "#7EA1DA"),
                this.options.sepeNum || (this.options.sepeNum = 4);
                var a = this.options.yaxis;
                this.options.rankColors = h.ranks(this.options.series.colors, 2);
                for (var r = [], p = this.options.series.datas, l = 0, c = p.length; c > l; l++)
                    for (var d = 0; d < p[l].data.length; d++) {
                        var u = p[l] || []
                            , m = u.data[d] || 0;
                        r[d] = (r[d] || 0) + m
                    }
                var f = s(this.options.sepeNum, r);
                this.options.coordinate = f,
                    this.options.padding = {};
                for (var v = 0, l = 0, c = a.length; c > l; l++)
                    v = Math.max(o.measureText(a[l].value).width, v);
                this.options.padding.left = (v + 10) * t;
                var g = o.measureText(f.max).width;
                this.options.padding.right = (g + 10) * t,
                    this.options.padding.top = 2 * this.options.font_size * t,
                    this.options.padding.bottom = 50 * t;
                var y = (i.height - this.options.padding.top - this.options.padding.bottom) / a.length;
                this.options.unitHeight = y,
                    n.apply(this, [this.options.context, 110 + g, 40, 82, 20])
            }
                ,
                t.prototype.draw = function(t) {
                    this.clear(),
                        this.init();
                    var i = this;
                    new a(this.options),
                        r.call(this),
                        l.addEvent(i.container, "mousemove", function(t) {
                            var o, e;
                            t.layerX ? (o = t.layerX,
                                e = t.layerY) : t.x && (o = t.x,
                                e = t.y),
                                p.call(i, o, e);
                            try {
                                t.preventDefault()
                            } catch (n) {
                                t.returnValue = !1
                            }
                        }),
                        l.addEvent(i.container, "mouseleave", function() {
                            void 0 !== i.options.tips && (i.options.tips.style.display = "none",
                                i.options.tips.style.left = "-10000")
                        }),
                    t && t()
                }
                ,
                t.prototype.reDraw = function() {
                    this.clear(),
                        this.init(),
                        this.draw()
                }
                ,
                t.prototype.clear = function(t) {
                    this.container ? this.container.innerHTML = "" : document.getElementById(this.options.container).innerHTML = "",
                    t && t()
                }
                ,
                t
        }();
        t.exports = c
    }
    , function(t, i, o) {
        var e = o(21)
            , n = o(25)
            , s = o(9)
            , a = o(60)
            , r = function() {
            function t(t) {
                this.options = {},
                    this.options = e(this.options, t),
                    this.draw()
            }
            function i(t, i) {
                var o = 1;
                -1 !== i.toString().indexOf(".") && (o = i.toString().length - i.toString().indexOf(".") - 1);
                var e = Math.pow(10, o);
                return a.format_unit(Math.round(t * e) / e, o)
            }
            return t.prototype.init = function() {
                this.options.xSplitShow = !1,
                    this.options.xShowDivide = !1
            }
                ,
                t.prototype.draw = function() {
                    this.init();
                    var t = this.options.padding.top
                        , o = this.options.padding.left
                        , e = this.options.padding.right
                        , a = this.options.padding.bottom
                        , r = this.options.context
                        , p = this.options.canvas
                        , h = this.options.yaxis
                        , l = (this.options.series.data,
                        this.options.unitHeight)
                        , c = this.options.dpr
                        , d = this.options.coordinate
                        , u = (d.max,
                        d.min)
                        , m = d.stepHeight
                        , f = this.options.sepeNum
                        , v = p.width - o - e
                        , g = v / f;
                    r.save();
                    var y = s(Math.round(p.height - a))
                        , A = s(Math.round(t))
                        , x = s(Math.round(o))
                        , w = s(Math.round(p.width - e));
                    r.strokeStyle = "#C9C9C9",
                        r.beginPath(),
                        r.moveTo(x, y),
                        r.lineTo(w, y),
                        r.moveTo(x, y),
                        r.lineTo(x, A),
                        r.moveTo(x, A),
                        r.lineTo(w, A),
                        r.moveTo(w, y),
                        r.lineTo(w, A),
                        r.stroke(),
                        r.save(),
                        r.beginPath(),
                        r.textAlign = "end",
                        r.textBaseline = "middle";
                    for (var _ = 0, b = h.length; b > _; _++) {
                        {
                            r.measureText(h[_].value).width
                        }
                        h[_].show && (r.fillText(h[_].value, o - 8 * c, A + _ * l + l / 2),
                            r.moveTo(o - 4 * c, s(A + _ * l + l / 2)),
                            r.lineTo(o, s(A + _ * l + l / 2)))
                    }
                    for (r.stroke(),
                             r.restore(),
                             _ = 1,
                             b = f; b > _; _++)
                        r.beginPath(),
                            _ == -u / m ? (r.moveTo(s(g * _ + o), A),
                                r.lineTo(s(g * _ + o), y),
                                r.stroke()) : n(r, g * _ + o, A, g * _ + o, y, 3);
                    r.save();
                    this.options.coordinateMaxY;
                    r.textAlign = "center",
                        r.textBaseline = "top";
                    for (r.beginPath(),
                             _ = 0; f >= _; _++)
                        r.fillText(i(u + _ * m, m), s(g * _ + o), y + 7 * c),
                            r.moveTo(s(g * _ + o), y),
                            r.lineTo(s(g * _ + o), y + 5 * c),
                            r.stroke();
                    r.restore()
                }
                ,
                t
        }();
        t.exports = r
    }
    , function(t, i, o) {
        var e = o(9);
        t.exports = function() {
            var t = (this.options.dpr,
                this.options.canvas)
                , i = this.options.context
                , o = this.options.series.datas
                , n = (this.options.yaxis,
                this.options.coordinate)
                , s = this.options.sepeNum
                , a = this.options.unitHeight
                , r = this.options.padding.left
                , p = this.options.padding.right
                , h = this.options.padding.top
                , l = t.width - r - p
                , c = e(r + l / s * (-n.min / n.stepHeight))
                , d = this.options.color
                , u = (this.options.hoverColor,
            this.options.series.colors || ["#666666"]);
            i.save(),
                i.fillStyle = d,
                i.strokeStyle = d,
                i.beginPath();
            for (var m = 0, f = o.length; f > m; m++) {
                i.save();
                for (var v = 0, g = o[m].data.length; g > v; v++) {
                    var y = o[m].data;
                    i.fillStyle = u[m];
                    for (var A = 0, x = 0; m > x; x++) {
                        var w = parseFloat(o[x].data[v]) || 0;
                        A += w
                    }
                    var _ = parseFloat(y[v]) || 0
                        , b = Math.round(c + A / n.max * (l - (c - r)))
                        , T = Math.round(e(h + a * (v + .25)))
                        , M = Math.ceil(_ / n.max * (l - (c - r)));
                    M = M > 0 ? M + m : M;
                    var k = Math.round(a / 2);
                    i.fillRect(b, T, M, k)
                }
                i.restore()
            }
            i.restore()
        }
    }
    , function(t, i, o) {
        var e = o(9);
        t.exports = function(t, i) {
            function o(t) {
                for (var i = 0, o = r.length; o > i; i++) {
                    a.save();
                    for (var n = 0, s = r[i].data.length; s > n; n++) {
                        var p = r[i].data;
                        a.fillStyle = x == n ? t ? A[i] : y[i] : A[i];
                        for (var h = 0, c = 0; i > c; c++) {
                            var m = parseFloat(r[c].data[n]) || 0;
                            h += m
                        }
                        var w = parseFloat(p[n]) || 0
                            , _ = Math.round(g + h / l.max * (v - (g - u)))
                            , b = Math.round(e(f + d * (n + .25)))
                            , T = Math.ceil(w / l.max * (v - (g - u)));
                        T = T > 0 ? T + i : T;
                        var M = Math.round(d / 2);
                        a.fillRect(_, b, T, M)
                    }
                    a.restore()
                }
            }
            var n = this.options.dpr
                , s = this.options.canvas
                , a = this.options.context
                , r = this.options.series.datas
                , p = this.options.yaxis
                , h = this.options.width
                , l = (this.options.height,
                this.options.coordinate)
                , c = this.options.sepeNum
                , d = this.options.unitHeight
                , u = this.options.padding.left
                , m = this.options.padding.right
                , f = this.options.padding.top
                , v = (this.options.padding.bottom,
            s.width - u - m)
                , g = e(u + v / c * (-l.min / l.stepHeight))
                , y = this.options.rankColors
                , A = this.options.series.colors || ["#666666"]
                , x = Math.floor((i * n - f) / d)
                , w = !1;
            if (w = t > u && h - m > t ? !0 : !1,
            x >= 0 && x < p.length && w) {
                var _ = void 0 === this.options.series.suffix ? "" : this.options.series.suffix
                    , b = document.createElement("div");
                b.innerHTML = p[x].value;
                for (var T = document.createElement("div"), M = 0; M < r.length; M++) {
                    var k = r[M].data[x];
                    if ("" != k && "-" != k && null != k && void 0 != k) {
                        var C = document.createElement("div")
                            , N = document.createElement("span");
                        N.innerHTML = r[M].name + " " + r[M].data[x] + _;
                        var E = document.createElement("span");
                        E.style.display = "inline-block",
                            E.style.width = "6px",
                            E.style.height = "6px",
                            E.style.backgroundColor = A[M],
                            E.style.borderRadius = "50%",
                            E.style.marginRight = "5px",
                            C.appendChild(E),
                            C.appendChild(N),
                            T.appendChild(C)
                    }
                }
                if (this.options.tips) {
                    var S = this.options.tips;
                    S.innerHTML = "",
                        u >= t * n || t * n >= u + v ? (this.options.tips.style.display = "none",
                            this.options.tips.style.left = "-10000") : S.style.display = "block",
                        S.appendChild(b),
                        S.appendChild(T),
                        S.style.left = t * n >= u + v / 2 ? t - S.clientWidth - 20 + "px" : t + 20 + "px",
                        S.style.top = i * n <= f + S.clientHeight * n ? f + "px" : i - S.clientHeight + "px"
                } else {
                    var D = document.createElement("div");
                    D.appendChild(b),
                        D.appendChild(T),
                        this.container.appendChild(D),
                        this.options.tips = D,
                        D.style.position = "absolute",
                        D.style.font = "12px/150% Simsun,arial,sans-serif",
                        D.style.padding = "5px",
                        D.style.backgroundColor = "#000",
                        D.style.color = "#fff",
                        D.style.borderRadius = "5px",
                        D.style.opacity = "0.7",
                        D.style.filter = "progid:DXImageTransform.Microsoft.Alpha(Opacity=70)",
                        D.style.left = t * n >= u + v / 2 ? t - D.clientWidth - 20 + "px" : t + 20 + "px",
                        D.style.top = i * n <= f + D.clientHeight * n ? f + "px" : i - D.clientHeight + "px"
                }
                o()
            } else
                o(!0),
                this.options.tips && (this.options.tips.style.display = "none",
                    this.options.tips.style.left = "-10000",
                    x = 0 > x ? 0 : r.length - 1)
        }
    }
    , function(t) {
        var i = {
            rank: function(t, i) {
                for (var o = t.split(""), e = 1; e < o.length; e++) {
                    var n = parseInt(o[e], 16) + 1 * i;
                    o[e] = 16 > n ? n.toString(16) : "F"
                }
                return o.join("")
            },
            ranks: function(t, i) {
                for (var o = [], e = 0, n = t.length; n > e; e++) {
                    for (var s = t[e].split(""), a = 1; a < s.length; a++) {
                        var r = parseInt(s[a], 16) + 1 * i;
                        s[a] = 16 > r ? r.toString(16) : "F"
                    }
                    o.push(s.join(""))
                }
                return o
            }
        };
        t.exports = i
    }
    , function(t, i, o) {
        var e = o(21)
            , n = o(22)
            , s = o(38)
            , a = o(167)
            , r = o(168)
            , p = o(169)
            , h = o(13)
            , l = function() {
            function t(t) {
                this.options = e(this.options, t),
                    this.container = document.getElementById(t.container),
                    this.container.className = this.container.className.replace(/emcharts-container/g, "").trim(),
                    this.container.className = this.container.className + " emcharts-container"
            }
            return t.prototype.init = function() {
                this.container.style.position = "relative",
                void 0 === this.options.dpr && (this.options.dpr = 1);
                var t = this.options.dpr
                    , i = document.createElement("div");
                i.className = "event-div",
                    this.container.appendChild(i),
                    this.eventDiv = i;
                var o = document.createElement("canvas");
                this.container.appendChild(o),
                    o.width = t * this.options.width,
                    o.height = t * this.options.height,
                    o.style.width = this.options.width + "px",
                    o.style.height = this.options.height + "px",
                    o.style.border = "0px";
                try {
                    var e = o.getContext("2d")
                } catch (a) {
                    o = window.G_vmlCanvasManager.initElement(o);
                    var e = o.getContext("2d")
                }
                var e = o.getContext("2d");
                void 0 === this.options.font_size && (this.options.font_size = 12),
                    e.font = this.options.font_size * t + "px Arial",
                    e.lineWidth = 1 * t,
                    this.options.dpr = t,
                    this.options.canvas = o,
                    this.options.context = e,
                void 0 === this.options.color && (this.options.color = "#6890D5"),
                this.options.sepeNum || (this.options.sepeNum = 4);
                var r = this.options.yaxis
                    , p = this.options.series
                    , h = p.reduce(function(t, i) {
                    return t.concat(i.data)
                }, p[0].data)
                    , l = s(this.options.sepeNum, h);
                this.options.coordinate = l,
                    this.options.padding = {};
                for (var c = 0, d = 0, u = r.length; u > d; d++)
                    r[d].value.replace(/^\s+|\s+$/g, "").split("\n").map(function(t) {
                        c = Math.max(e.measureText(t).width, c)
                    });
                this.options.padding.left = (c + 10) * t;
                var m = e.measureText(l.max).width / 2;
                this.options.padding.right = (m + 5) * t,
                    this.options.padding.top = 15 * t,
                    this.options.padding.bottom = 25 * t;
                var f = (o.height - this.options.padding.top - this.options.padding.bottom) / r.length;
                this.options.unitHeight = f,
                    n.apply(this, [this.options.context, 110 + m, 40, 82, 20])
            }
                ,
                t.prototype.draw = function(t) {
                    this.clear(),
                        this.init();
                    var i = this;
                    new a(this.options),
                        r.call(this),
                        h.addEvent(i.eventDiv, "mousemove", function(t) {
                            var o, e;
                            t.layerX ? (o = t.layerX,
                                e = t.layerY) : t.x && (o = t.x,
                                e = t.y),
                                p.call(i, o, e);
                            try {
                                t.preventDefault()
                            } catch (n) {
                                t.returnValue = !1
                            }
                        }),
                        h.addEvent(i.eventDiv, "mouseleave", function() {
                            void 0 !== i.options.tips && (i.options.tips[0].style.display = "none",
                                i.options.tips[0].style.left = "-10000",
                                i.options.tips[1].style.display = "none",
                                i.options.tips[1].style.left = "-10000")
                        }),
                    t && t()
                }
                ,
                t.prototype.reDraw = function() {
                    this.clear(),
                        this.init(),
                        this.draw()
                }
                ,
                t.prototype.clear = function(t) {
                    this.container ? this.container.innerHTML = "" : document.getElementById(this.options.container).innerHTML = "",
                    this.options.tips && (this.options.tips = null),
                    t && t()
                }
                ,
                t
        }();
        t.exports = l
    }
    , function(t, i, o) {
        var e = o(21)
            , n = o(25)
            , s = o(9)
            , a = o(60)
            , r = function() {
            function t(t) {
                this.options = {},
                    this.options = e(this.options, t),
                    this.draw()
            }
            function i(t, i, o, e) {
                var n = i.replace(/^\s+|\s+$/g, "").split("\n")
                    , s = n.length;
                lineHeight = 1.2 * t.font.split("px")[0],
                    totalHeight = lineHeight * (s - 1);
                for (var a = 0; s > a; a++)
                    t.fillText(n[a], o, e - totalHeight / 2 + lineHeight * a)
            }
            function o(t, i) {
                var o = 1;
                -1 !== i.toString().indexOf(".") && (o = i.toString().length - i.toString().indexOf(".") - 1);
                var e = Math.pow(10, o);
                return a.format_unit(Math.round(t * e) / e, o)
            }
            return t.prototype.init = function() {
                this.options.xSplitShow = !1,
                    this.options.xShowDivide = !1
            }
                ,
                t.prototype.draw = function() {
                    this.init();
                    var t = this.options.padding.top
                        , e = this.options.padding.left
                        , a = this.options.padding.right
                        , r = this.options.padding.bottom
                        , p = this.options.context
                        , h = this.options.canvas
                        , l = this.options.yaxis
                        , c = (this.options.series,
                        this.options.unitHeight)
                        , d = this.options.dpr
                        , u = this.options.coordinate
                        , m = (u.max,
                        u.min)
                        , f = u.stepHeight
                        , v = this.options.sepeNum
                        , g = h.width - e - a
                        , y = g / v;
                    p.save();
                    var A = s(Math.round(h.height - r))
                        , x = s(Math.round(t))
                        , w = s(Math.round(e))
                        , _ = s(Math.round(h.width - a));
                    p.strokeStyle = "#C9C9C9",
                        p.beginPath(),
                        p.moveTo(w, A),
                        p.lineTo(_, A),
                        p.moveTo(w, A),
                        p.lineTo(w, x),
                        p.moveTo(w, x),
                        p.lineTo(_, x),
                        p.moveTo(_, A),
                        p.lineTo(_, x),
                        p.stroke(),
                        p.save(),
                        p.beginPath(),
                        p.textAlign = "end",
                        p.textBaseline = "middle";
                    for (var b = 0, T = l.length; T > b; b++) {
                        {
                            p.measureText(l[b].value).width
                        }
                        l[b].show && (i(p, l[b].value, e - 8 * d, x + b * c + c / 2),
                            p.moveTo(e - 4 * d, s(x + b * c + c / 2)),
                            p.lineTo(e, s(x + b * c + c / 2))),
                        l[b].showline && b !== T - 1 && n(p, e, x + b * c + c, e + g, x + b * c + c, 3)
                    }
                    for (p.stroke(),
                             p.restore(),
                             b = 1,
                             T = v; T > b; b++)
                        p.beginPath(),
                            b == -m / f ? (p.moveTo(s(y * b + e), x),
                                p.lineTo(s(y * b + e), A),
                                p.stroke()) : n(p, y * b + e, x, y * b + e, A, 3);
                    p.save();
                    this.options.coordinateMaxY;
                    p.textAlign = "center",
                        p.textBaseline = "top";
                    for (p.beginPath(),
                             b = 0; v >= b; b++)
                        p.fillText(o(m + b * f, f), s(y * b + e), A + 7 * d),
                            p.moveTo(s(y * b + e), A),
                            p.lineTo(s(y * b + e), A + 5 * d),
                            p.stroke();
                    p.restore()
                }
                ,
                t
        }();
        t.exports = r
    }
    , function(t, i, o) {
        var e = o(9);
        t.exports = function() {
            {
                var t = (this.options.dpr,
                    this.options.canvas)
                    , i = this.options.context
                    , o = this.options.series
                    , n = (this.options.yaxis,
                    this.options.coordinate)
                    , s = this.options.sepeNum
                    , a = this.options.unitHeight
                    , r = this.options.padding.left
                    , p = this.options.padding.right
                    , h = this.options.padding.top
                    , l = t.width - r - p
                    , c = e(r + l / s * (-n.min / n.stepHeight))
                    , d = this.options.color;
                this.options.hoverColor
            }
            i.save(),
                i.beginPath();
            for (var u = 0, m = o.length; m > u; u++) {
                var f = o[u]
                    , v = a / (1.618 * m + .618)
                    , g = .618 * v
                    , d = "#7CB5EC";
                void 0 !== f.color && (d = f.color),
                    i.fillStyle = d,
                    i.strokeStyle = d;
                for (var y = 0, A = f.data.length; A > y; y++) {
                    var x = Math.round(f.data[y] / n.min * (c - r));
                    i.fillRect(c - x, e(h + a * y + g * (u + 1) + v * u), x, v)
                }
            }
            i.restore()
        }
    }
    , function(t, i, o) {
        var e = o(9);
        t.exports = function(t, i) {
            var o = this.options.dpr
                , n = this.options.canvas
                , s = (this.options.context,
                this.options.series)
                , a = this.options.yaxis
                , r = this.options.coordinate
                , p = this.options.sepeNum
                , h = this.options.unitHeight
                , l = this.options.padding.left
                , c = this.options.padding.right
                , d = this.options.padding.top
                , u = this.options.padding.bottom
                , m = n.width - l - c
                , f = (e(l + m / p * (-r.min / r.stepHeight)),
                this.options.color,
                this.options.hoverColor,
                10)
                , v = Math.floor((i * o - d) / h);
            if (v >= 0 && v < a.length && t > l && t < n.width - c) {
                for (var g = a[v].value, y = [], A = 0, x = s.length; x > A; A++) {
                    var w = void 0 === s[A].prefix ? "" : s[A].prefix
                        , _ = void 0 === s[A].suffix ? "" : s[A].suffix;
                    y.push({
                        text: w + s[A].data[v] + _,
                        color: s[A].color
                    })
                }
                if (this.options.tips) {
                    var b = this.options.tips[0]
                        , T = this.options.tips[1]
                        , M = T.children
                        , k = M[0];
                    k.innerHTML = g,
                        b.style.top = d + h * v + "px",
                        b.style.left = l + "px";
                    for (var C = 0, N = M.length; N - 1 > C; C++)
                        M[C + 1].children[1].innerHTML = y[C].text;
                    b.style.display = "block",
                        T.style.display = "block",
                        T.style.top = d + h * (2 / 3 + v) + T.clientHeight < n.height - u && d + h * (2 / 3 + v) + T.clientHeight > d ? d + h * (2 / 3 + v) + "px" : d + h * (2 / 3 + v) + T.clientHeight > n.height - u ? n.height - u - T.clientHeight - h * (1 / 3) + "px" : n.height - u - T.clientHeight - h * (2 / 3) + "px",
                        T.style.left = t > m / 2 + l ? t - f - T.clientWidth + "px" : t + f + "px"
                } else {
                    var E = document.createElement("div")
                        , S = document.createElement("div");
                    this.options.tips = [],
                        this.options.tips.push(E),
                        this.container.appendChild(E),
                        E.className = "horizontal-gmask",
                        E.style.width = m + "px",
                        E.style.height = h + "px",
                        E.style.top = d + h * v + "px",
                        E.style.left = l + "px",
                        this.options.tips.push(S),
                        this.container.appendChild(S),
                        S.className = "horizontal-gpanel";
                    var D = document.createElement("div");
                    D.innerHTML = g,
                        S.appendChild(D);
                    for (var L = 0, R = y.length; R > L; L++) {
                        var I = document.createElement("div");
                        I.style.paddingTop = "8px",
                            I.innerHTML = '<i style="display: inline-block;margin-right:5px; height: 10px;width: 10px; border-radius: 5px;background-color:' + y[L].color + '"></i><span>' + y[L].text + "</span>",
                            S.appendChild(I)
                    }
                    S.style.top = d + h * (2 / 3 + v) + S.clientHeight < n.height - u && d + h * (2 / 3 + v) + S.clientHeight > d ? d + h * (2 / 3 + v) + "px" : d + h * (2 / 3 + v) + S.clientHeight > n.height - u ? n.height - u - S.clientHeight - h * (1 / 3) + "px" : n.height - u - S.clientHeight - h * (2 / 3) + "px",
                        S.style.left = t > m / 2 + l ? t - f - S.clientWidth + "px" : t + f + "px"
                }
            } else
                this.options.tips && (this.options.tips[0].style.display = "none",
                    this.options.tips[0].style.left = "-10000",
                    this.options.tips[1].style.display = "none",
                    this.options.tips[1].style.left = "-10000")
        }
    }
    , function(t, i, o) {
        var e = o(171)
            , n = o(7)
            , s = o(172)
            , a = o(21)
            , r = o(20)
            , p = o(22)
            , h = o(13)
            , l = o(38)
            , c = function() {
            function t(t) {
                this.options = {},
                    this.options = a(n.defaulttheme, t),
                    this.container = document.getElementById(t.container),
                    this.container.className = this.container.className.replace(/emcharts-container/g, "").trim(),
                    this.container.className = this.container.className + " emcharts-container",
                    this.onChartLoaded = void 0 == t.onChartLoaded ? function() {}
                        : t.onChartLoaded
            }
            function i(t) {
                for (var i = t.length, o = [], e = 0; i > e; e++)
                    o = o.concat(t[e].data);
                var n = o.filter(function(t) {
                    return "" !== t && void 0 !== t && null !== t
                }).sort(function(t, i) {
                    return 1 * t - 1 * i
                })
                    , s = 1 * n[0]
                    , a = 1 * n[n.length - 1]
                    , r = (a + s) / 2;
                a - s > 1 && r - Math.floor(r) > 0 && (s = n[0] - (r - Math.floor(r)),
                    r = Math.floor(r));
                var p = {};
                if (a * s > 0 && s !== a && (a - s) / Math.abs(a) <= .5)
                    if (p = l(this.options.sepeNum, [a - r, s - r]),
                    (p.max + r) * (p.min + r) < 0) {
                        var c = Math.min(Math.abs(p.max + r), Math.abs(p.min + r));
                        p.max = a >= 0 ? p.max + r + c : 0,
                            p.min = a >= 0 ? 0 : p.min + r - c
                    } else
                        p.max += r,
                            p.min += r;
                else
                    p = l(this.options.sepeNum, n);
                var d = this.options.context;
                if (p.stepHeight >= 1e4)
                    var u = d.measureText(h.format_unit(p.stepHeight)).width - d.measureText(h.format_unit(parseInt(p.stepHeight))).width;
                else
                    var u = d.measureText(p.stepHeight).width - d.measureText(parseInt(p.stepHeight)).width;
                var m = d.measureText(h.format_unit(parseInt(p.max))).width
                    , f = d.measureText(h.format_unit(parseInt(p.min))).width
                    , v = m > f ? m : f
                    , g = v + u;
                return 0 == p.max && 0 == p.min && (p = l(this.options.sepeNum, [1, -1])),
                    {
                        max: p.max,
                        min: p.min,
                        step: p.stepHeight,
                        maxPaddingLeftWidth: g
                    }
            }
            return t.prototype.init = function() {
                this.options.type = "line";
                var t = !0
                    , i = document.createElement("div");
                i.className = "event-div",
                    this.container.appendChild(i),
                    this.eventDiv = i;
                var o = document.createElement("canvas");
                this.container.appendChild(o),
                    this.container.style.position = "relative";
                try {
                    var e = o.getContext("2d")
                } catch (n) {
                    o = window.G_vmlCanvasManager.initElement(o);
                    var e = o.getContext("2d")
                }
                this.options.canvas = o,
                    this.options.context = e;
                var s = this.options.dpr = void 0 == this.options.dpr ? 1 : this.options.dpr;
                o.width = this.options.width * s,
                    o.height = this.options.height * s,
                    this.options.canvas_offset_top = 15 * s,
                    this.options.scale_count = 0,
                    this.options.decimalCount = void 0 == this.options.decimalCount ? 2 : this.options.decimalCount;
                var a = this.options.xaxis;
                if (this.options.angle && 0 != this.options.angle) {
                    var r = a[0].value
                        , p = (r.split("").length,
                    e.measureText(a[0].value).width * Math.sin(2 * Math.PI / 360 * this.options.angle) + 50);
                    this.options.c_1_height = this.options.canvas.height - p * s
                } else
                    this.options.c_1_height = o.height - 50 * s;
                void 0 === this.options.showname && (this.options.showname = !0),
                    this.options.sepeNum = void 0 == this.options.sepeNum ? 4 : this.options.sepeNum,
                this.options.sepeNum < 2 && (this.options.sepeNum = 2),
                    o.style.width = this.options.width + "px",
                    o.style.height = this.options.height + "px",
                    o.style.border = "0";
                var h = !1;
                h ? t && e.translate("0", this.options.canvas_offset_top) : e.translate("0", this.options.canvas_offset_top);
                var l = ""
                    , c = ""
                    , d = "";
                this.options.font ? (d = this.options.font.fontFamily ? this.options.font.fontFamily : "Arial",
                    c = this.options.font.fontSize ? this.options.font.fontSize * this.options.dpr : 12 * this.options.dpr,
                    l = c + "px " + d) : l = 12 * this.options.dpr + "px Arial",
                    e.font = l,
                    e.lineWidth = 1 * this.options.dpr,
                    this.options.spacing = .2,
                    this.options.padding = {},
                    this.options.padding.left = 0,
                    this.options.padding.right = 0,
                    this.options.padding.top = 0,
                    this.options.padding.bottom = 0,
                    this.options.pointRadius = void 0 == this.options.pointRadius ? 5 : this.options.pointRadius
            }
                ,
                t.prototype.draw = function() {
                    this.clear(),
                        this.init(),
                        this.options.interactive = new r(this.options);
                    var t = this.options.context
                        , o = this.options.series;
                    this.options.data = {};
                    var n = i.call(this, o);
                    this.options.data.max = n.max,
                        this.options.data.min = n.min,
                        this.options.data.step = n.step,
                        this.options.padding_left = Math.round(n.maxPaddingLeftWidth + 20);
                    var a = this.options.xaxis
                        , h = t.measureText(a[0].value).width / 2 + 10
                        , l = t.measureText(a[a.length - 1].value).width / 2 + 10;
                    if (this.options.angle && 0 != this.options.angle && (this.options.padding.right = t.measureText(this.options.xaxis[0].value).width * Math.cos(2 * Math.PI * (this.options.angle / 360)) + 10),
                    this.options.padding.right < l && (this.options.padding.right = l),
                    this.options.padding_left < h && (this.options.padding_left = h),
                    this.options.series2 && 0 !== this.options.series2.length) {
                        var c = this.options.series2
                            , d = i.call(this, c);
                        this.options.data.max2 = d.max,
                            this.options.data.min2 = d.min,
                            this.options.data.step2 = d.step,
                            this.options.padding.right = Math.round(d.maxPaddingLeftWidth + 20)
                    } else
                        this.options.series2 = void 0;
                    this.options.series2 && 0 !== this.options.series2.length ? (this.options.drawWidth = Math.round(t.canvas.width - this.options.padding.right),
                        p.apply(this, [t, 100 + this.options.padding_left, 10, 82, 20])) : (this.options.drawWidth = Math.round(t.canvas.width - this.options.padding.right),
                        p.apply(this, [t, 100 + this.options.padding_left, 10, 82, 20])),
                        this.options.unit = {};
                    var u = this.options.xaxis.length;
                    this.options.unit.unitWidth = (this.options.drawWidth - this.options.padding_left) / u,
                        this.options.unit.groupBarWidth = .618 * this.options.unit.unitWidth;
                    var m = 0;
                    if (this.options.series)
                        for (var f, v = 0; f = o[v]; v++)
                            "bar" == f.type && m++;
                    if (this.options.series2)
                        for (var f, v = 0; f = c[v]; v++)
                            "bar" == f.type && m++;
                    this.options.unit.groupBarCount = m,
                        new e(this.options),
                        new s(this.options),
                        this.addInteractive()
                }
                ,
                t.prototype.addInteractive = function() {
                    function t(t, d) {
                        var u = t * s - e
                            , m = d * s - n
                            , f = this.options.series
                            , v = this.options.series2
                            , g = 0
                            , y = []
                            , A = a.options.unit.unitWidth;
                        g = Math.floor(u / A),
                        0 > g && (g = 0),
                        g > o.length - 1 && (g = o.length - 1);
                        for (var x = 0, w = f.length; w > x; x++)
                            y.push({
                                color: f[x].color,
                                data: (void 0 === f[x].data[g] || null === f[x].data[g] ? "" : f[x].data[g]) + (f[x].suffix || ""),
                                name: f[x].name,
                                y: void 0 === f[x].data[g] ? n + h.get_y.call(a, 0) : n + h.get_y.call(a, f[x].data[g]),
                                suffix: f[x].suffix || "",
                                type: f[x].type
                            });
                        if (a.options.series2)
                            for (x = 0,
                                     w = v.length; w > x; x++)
                                y.push({
                                    color: v[x].color,
                                    data: (void 0 === v[x].data[g] || null === v[x].data[g] ? "" : v[x].data[g]) + (v[x].suffix || ""),
                                    name: v[x].name,
                                    y: 0 == v[x].data[g] ? n + l / 2 : n + (l - l * (v[x].data[g] - p) / (r - p)),
                                    suffix: v[x].suffix || "",
                                    type: v[x].type
                                });
                        y.sort(function(t, i) {
                            return t.y - i.y
                        });
                        var _ = 0
                            , b = !1;
                        if (_ = Math.round(1 == o.length ? g * A / s + A / s * .5 + e / s : g * A / s + e / s + A / s * .5),
                            a.options.interOption) {
                            var T = a.options.interOption.tips
                                , M = T.children;
                            M[0].innerHTML = o[g].value;
                            for (var k = 0, w = y.length; w > k; k++)
                                y[k].data === y[k].suffix ? M[k + 1].style.display = "none" : (b = !0,
                                    M[k + 1].style.display = "block",
                                    M[k + 1].children[0].style.backgroundColor = y[k].color,
                                    M[k + 1].children[0].style.verticalAlign = "middle",
                                    M[k + 1].children[1].innerHTML = (a.options.showname ? y[k].name : "") + " " + y[k].data);
                            b ? T.style.display = "block" : (T.style.display = "none",
                                T.style.left = "-10000px"),
                                T.style.left = g * A / s + e / s >= i.width / s / 2 ? _ - T.clientWidth + "px" : _ + "px";
                            var C = (y[0].y + y[y.length - 1].y) / 2 / s - 50;
                            T.style.top = C + "px";
                            var N = a.options.interOption.yLine;
                            N.style.left = _ + "px",
                            0 !== a.options.unit.groupBarCount && (N.style.left = _ - A / 2 + "px");
                            for (var E = a.options.interOption.circles, S = 0, D = y.length; D > S; S++)
                                y[S].data === y[S].suffix ? E[S].style.display = "none" : (E[S].style.display = "block",
                                    E[S].style.top = y[S].y / s - c + "px",
                                    E[S].style.left = _ - c + "px",
                                    E[S].style.borderColor = y[S].color);
                            b && (a.options.interOption.tips.style.display = "block",
                                N.style.display = "block")
                        } else {
                            a.options.interOption = {};
                            var T = document.createElement("div");
                            T.className = "chart_line_tips",
                                a.container.appendChild(T),
                                T.style.top = (y[0].y + y[y.length - 1].y) / 2 / s + "px";
                            var L = document.createElement("div");
                            L.className = "chart_line_tips_title",
                                L.innerHTML = o[g].value,
                                T.appendChild(L);
                            var N = document.createElement("div");
                            N.className = "chart_line_yline",
                                N.style.left = _ + "px",
                                N.style.top = n / s + "px",
                                N.style.height = l / s + "px",
                            0 !== a.options.unit.groupBarCount && (N.className = "chart_line_ybar",
                                N.style.width = a.options.unit.unitWidth + "px",
                                N.style.left = _ - a.options.unit.unitWidth / 2 + "px"),
                                a.container.appendChild(N);
                            var E = [];
                            for (x = 0,
                                     w = y.length; w > x; x++) {
                                var R = document.createElement("div");
                                R.className = "chart_line_tips_line";
                                var I = document.createElement("span");
                                I.className = "chart_line_tips_color",
                                    I.style.backgroundColor = y[x].color,
                                    R.appendChild(I);
                                var Y = document.createElement("span");
                                Y.innerHTML = (a.options.showname ? y[x].name : "") + y[x].data,
                                    R.appendChild(Y),
                                    T.appendChild(R);
                                var W = document.createElement("div");
                                W.className = "chart_line_cir",
                                    W.style.width = 2 * c + "px",
                                    W.style.height = 2 * c + "px",
                                    W.style.borderRadius = 2 * c + "px",
                                    W.style.top = y[x].y / s - c + "px",
                                    W.style.left = _ - c + "px",
                                    W.style.borderColor = y[x].color,
                                    y[x].data === y[x].suffix ? (W.style.display = "none",
                                        R.style.display = "none") : b = !0,
                                "line" == y[x].type && 0 == this.options.unit.groupBarCount && a.container.appendChild(W),
                                    E.push(W)
                            }
                            T.style.left = g * A / s + e / s > i.width / 2 ? _ - T.clientWidth + "px" : _ + "px",
                                a.options.interOption.tips = T,
                                a.options.interOption.yLine = N,
                                a.options.interOption.circles = E
                        }
                        var F = this.options.series2 ? e : 10;
                        if (u >= 0 && u < i.width - e - F + 3 && m >= 0 && l >= m && b) {
                            a.options.interOption.tips.style.display = "block";
                            for (var S = 0, D = y.length; D > S; S++)
                                E[S].style.display = y[S].data === y[S].suffix ? "none" : "block";
                            N.style.display = "block"
                        } else {
                            a.options.interOption.tips.style.display = "none",
                                a.options.interOption.tips.style.left = "-10000px";
                            for (var S = 0, D = E.length; D > S; S++)
                                E[S].style.display = "none";
                            N.style.display = "none"
                        }
                    }
                    var i = this.options.canvas
                        , o = this.options.xaxis
                        , e = (this.options.series,
                        this.options.series2,
                        this.options.context,
                        this.options.padding_left)
                        , n = this.options.canvas_offset_top
                        , s = this.options.dpr
                        , a = this
                        , r = (this.options.data.max,
                        this.options.data.min,
                        this.options.data.max2)
                        , p = this.options.data.min2
                        , l = this.options.c_1_height
                        , c = this.options.pointRadius / s;
                    h.addEvent(a.eventDiv, "touchmove", function(i) {
                        var o = i.changedTouches[0]
                            , e = o.offsetX || o.clientX - a.container.getBoundingClientRect().left
                            , n = o.offsetY || o.clientY - a.container.getBoundingClientRect().top;
                        try {
                            i.preventDefault()
                        } catch (s) {}
                        t.call(a, e, n)
                    }),
                        h.addEvent(a.eventDiv, "mousemove", function(i) {
                            var o = i.offsetX || i.clientX - a.container.getBoundingClientRect().left
                                , e = i.offsetY || i.clientY - a.container.getBoundingClientRect().top;
                            try {
                                i.preventDefault()
                            } catch (n) {}
                            t.call(a, o, e)
                        }),
                        h.addEvent(a.eventDiv, "touchend", function(t) {
                            if (a.options.interOption) {
                                var i = a.options.interOption.circles;
                                a.options.interOption.tips.style.display = "none";
                                for (var o = 0, e = i.length; e > o; o++)
                                    i[o].style.display = "none";
                                a.options.interOption.yLine.style.display = "none"
                            }
                            try {
                                t.preventDefault()
                            } catch (n) {}
                        }),
                        h.addEvent(a.eventDiv, "mouseleave", function(t) {
                            if (a.options.interOption) {
                                var i = a.options.interOption.circles;
                                a.options.interOption.tips.style.display = "none";
                                for (var o = 0, e = i.length; e > o; o++)
                                    i[o].style.display = "none";
                                a.options.interOption.yLine.style.display = "none"
                            }
                            try {
                                t.preventDefault()
                            } catch (n) {}
                        })
                }
                ,
                t.prototype.reDraw = function() {
                    this.clear(),
                        this.init(),
                        this.draw()
                }
                ,
                t.prototype.clear = function(t) {
                    try {
                        this.container.innerHTML = "",
                        this.options.interOption && (this.options.interOption = null)
                    } catch (i) {
                        this.container.innerHTML = ""
                    }
                    t && t()
                }
                ,
                t
        }();
        t.exports = c
    }
    , function(t, i, o) {
        var e = o(21)
            , n = o(7)
            , s = o(13)
            , a = o(8)
            , r = o(9)
            , p = o(56)
            , h = function() {
            function t(t) {
                this.options = e(n.defaulttheme, t),
                    this.draw()
            }
            function i(t, i, o, e, n) {
                n ? this.options.data.step2 : this.options.data.step;
                t.save(),
                    t.fillStyle = void 0 == this.options.font.color ? "#000" : this.options.font.color,
                    t.textAlign = "right",
                    t.lineWidth = 1;
                for (var s, p = this.options.dpr, h = 0; s = e[h]; h++)
                    t.beginPath(),
                    0 == h || h == e.length - 1 || (t.strokeStyle = "#e6e6e6",
                        a(t, this.options.padding_left, s.y, this.options.drawWidth, s.y, 3)),
                        this.options.series2 && n ? (t.textAlign = "left",
                            t.fillText(l(s.num / 1, this.options.data.step2), r(this.options.drawWidth + 5), r(s.y + 5 * p))) : (t.textAlign = "right",
                            t.fillText(l(s.num / 1, this.options.data.step), r(this.options.padding_left - 5), r(s.y + 5 * p)));
                t.restore()
            }
            function o(t, i, o) {
                t.save(),
                    t.lineWidth = 1;
                {
                    var e = this.options.dpr;
                    this.options.padding_left
                }
                t.textAlign = "center",
                    t.fillStyle = this.options.font ? void 0 == this.options.font.color ? "#000" : this.options.font.color : "#000";
                for (var n, s = (this.options.drawWidth,
                    o.length), h = 0; s > h; h++) {
                    if (t.beginPath(),
                        n = o[h],
                        void 0 == n.show ? !0 : n.show)
                        if (1 == s) {
                            var l = (this.options.drawWidth - this.options.padding_left) / 2 + this.options.padding_left;
                            t.fillText(n.value, r(l), r(this.options.c_1_height + 20 * e))
                        } else {
                            var l = this.options.unit.unitWidth * h + this.options.unit.unitWidth / 2 + this.options.padding_left;
                            this.options.angle && 0 != this.options.angle ? p(n.value, t, r(l), r(this.options.c_1_height + 10 * e), this.options.angle) : t.fillText(n.value, r(l), r(this.options.c_1_height + 20 * e))
                        }
                    if (void 0 == n.showline ? !0 : n.showline)
                        if (0 == h)
                            ;
                        else {
                            t.strokeStyle = "#e6e6e6";
                            var l = this.options.unit.unitWidth * h + this.options.padding_left;
                            a(t, l, 0, l, this.options.c_1_height + 2, 3)
                        }
                }
                t.restore()
            }
            function h(t, i, o, e, n) {
                for (var s = n, a = [], r = 0; o >= r; r++)
                    a.push({
                        num: i + r * s,
                        x: 0,
                        y: e - r / o * e
                    });
                return a
            }
            function l(t, i) {
                var o = 1;
                -1 !== i.toString().indexOf(".") && (o = i.toString().length - i.toString().indexOf(".") - 1);
                var e = Math.pow(10, o);
                return s.format_unit(Math.round(t * e) / e, o)
            }
            return t.prototype.draw = function() {
                var t = this.options.context
                    , e = this.options.data.max
                    , n = this.options.data.max2
                    , s = this.options.data.step
                    , a = this.options.data.min
                    , p = this.options.data.min2
                    , l = this.options.data.step2
                    , c = this.options.sepeNum || 4
                    , d = this.options.xaxis
                    , u = this.options.c_1_height;
                t.beginPath(),
                    t.lineWidth = 1,
                    t.strokeStyle = "#ccc",
                    t.rect(r(this.options.padding_left), .5, Math.round(this.options.drawWidth - this.options.padding_left), Math.round(u)),
                    t.stroke();
                var m = h(e, a, c, u, s);
                if (i.call(this, t, e, a, m, !1),
                    this.options.series2) {
                    var f = h(n, p, c, u, l);
                    i.call(this, t, e, a, f, !0)
                }
                o.apply(this, [t, u, d])
            }
                ,
                t
        }();
        t.exports = h
    }
    , function(t, i, o) {
        var e = o(21)
            , n = o(7)
            , s = o(13)
            , a = o(9)
            , r = function() {
            function t(t) {
                this.options = e(n.defaulttheme, t),
                    this.draw()
            }
            function i(t, i, o) {
                t.save();
                var e = i.data
                    , n = e.length
                    , r = 0;
                r = o ? this.options.start_y_2 : this.options.start_y_1;
                var h = this.options.unit.groupBarWidth
                    , l = h / this.options.unit.groupBarCount
                    , c = this.options.groupBarCount;
                t.beginPath();
                for (var d = 0; n > d; d++) {
                    var u = e[d];
                    if (null != u && "" !== u && void 0 != u) {
                        var m = this.options.unit.unitWidth * d + this.options.unit.unitWidth / 2 + this.options.padding_left;
                        if (o)
                            var f = p.call(this, u);
                        else
                            var f = s.get_y.call(this, u);
                        u >= 0 ? t.rect(a(m - h / 2 + l * (c - 1)), f, l, r - f) : t.rect(a(m - h / 2 + l * (c - 1)), r, l, f - r)
                    }
                }
                t.fill(),
                    t.stroke(),
                    t.restore()
            }
            function o(t, i, o) {
                t.save();
                var e = i.data
                    , n = e.length;
                t.beginPath();
                for (var a = 0; n > a; a++) {
                    var r = e[a];
                    if (null != r && "" !== r && void 0 != r) {
                        var h = this.options.unit.unitWidth * a + this.options.unit.unitWidth / 2 + this.options.padding_left;
                        if (o)
                            var l = p.call(this, r);
                        else
                            var l = s.get_y.call(this, r);
                        0 == a ? t.moveTo(h, l) : a == n - 1 ? t.lineTo(h, l) : t.lineTo(h, l)
                    }
                }
                t.stroke(),
                    t.beginPath(),
                    t.restore()
            }
            function r(t, i, o) {
                t.save();
                for (var e = i.data, n = e.length, r = this.options.pointRadius, h = 0; n > h; h++) {
                    var l = e[h];
                    if (null != l && "" !== l && void 0 != l) {
                        if (t.beginPath(),
                        1 == n)
                            ;
                        else
                            ;if (1 == n)
                            var c = (this.options.drawWidth - this.options.padding_left) / 2 + this.options.padding_left;
                        else
                            var c = this.options.unit.unitWidth * h + this.options.unit.unitWidth / 2 + this.options.padding_left;
                        if (o)
                            var d = p.call(this, l);
                        else
                            var d = s.get_y.call(this, l);
                        0 == h ? (t.arc(a(c), a(d), r, 0, 2 * Math.PI, !0),
                            t.fill()) : (t.arc(a(c), a(d), r, 0, 2 * Math.PI, !0),
                            t.fill())
                    }
                }
                t.restore()
            }
            function p(t) {
                return this.options.c_1_height - this.options.c_1_height * (t - this.options.data.min2) / (this.options.data.max2 - this.options.data.min2)
            }
            return t.prototype.draw = function() {
                var t = this.options.context;
                t.lineWidth = 1;
                var e = this.options.series;
                this.options.groupBarCount = 0;
                this.options.start_y_1 = this.options.data.min * this.options.data.max < 0 ? s.get_y.call(this, 0) : s.get_y.call(this, this.options.data.min),
                    this.options.start_y_2 = this.options.data.min2 * this.options.data.max2 < 0 ? p.call(this, 0) : p.call(this, this.options.data.min2);
                for (var n, a = 0; n = e[a]; a++)
                    t.fillStyle = void 0 == n.color ? "#333" : n.color,
                        t.strokeStyle = void 0 == n.color ? "#333" : n.color,
                    "bar" == n.type && (this.options.groupBarCount++,
                        i.apply(this, [t, n, !1]));
                for (var n, a = 0; n = e[a]; a++)
                    t.fillStyle = void 0 == n.color ? "#333" : n.color,
                        t.strokeStyle = void 0 == n.color ? "#333" : n.color,
                    "line" == n.type && (o.apply(this, [t, n, !1]),
                        n.showpoint ? r.apply(this, [t, n, !1]) : 1 == n.data.length && r.apply(this, [t, n, !1]));
                if (this.options.series2)
                    for (var n, h = this.options.series2, a = 0; n = h[a]; a++)
                        t.fillStyle = void 0 == n.color ? "#333" : n.color,
                            t.strokeStyle = void 0 == n.color ? "#333" : n.color,
                        "bar" == n.type && (this.options.groupBarCount++,
                            i.apply(this, [t, n, !0]));
                if (this.options.series2)
                    for (var n, h = this.options.series2, a = 0; n = h[a]; a++)
                        t.fillStyle = void 0 == n.color ? "#333" : n.color,
                            t.strokeStyle = void 0 == n.color ? "#333" : n.color,
                        "line" == n.type && (o.apply(this, [t, n, !0]),
                            n.showpoint ? r.apply(this, [t, n, !0]) : 1 == n.data.length && r.apply(this, [t, n, !0]))
            }
                ,
                t
        }();
        t.exports = r
    }
    , function(t, i, o) {
        var e = o(174);
        "string" == typeof e && (e = [[t.id, e, ""]]);
        o(179)(e, {});
        e.locals && (t.exports = e.locals)
    }
    , function(t, i, o) {
        i = t.exports = o(175)(),
            i.push([t.id, "\r\n/*手机版图表样式*/\r\n.emcharts-container .show-tip {\r\n    position: absolute;\r\n    top: 0px;\r\n    z-index: 999;\r\n    border: 0;\r\n    padding: 10px;\r\n    border-radius: 7px;\r\n    background-color: #17b03e;\r\n    color: #fff;\r\n    font-size: 20px;\r\n    font-weight: lighter;\r\n    font-family: 'Microsoft Yahei';\r\n   \r\n}\r\n.emcharts-container .show-tip .span-price{\r\n	font-size: 22px;\r\n}\r\n.emcharts-container .show-tip span{\r\n	font-size: 14px;height:25px;line-height:25px;\r\n    white-space: nowrap;\r\n}\r\n\r\n.emcharts-container .tip-line-1,.tip-line-2{\r\n	height:25px;\r\n	line-height:25px;\r\n}\r\n\r\n.emcharts-container .show-tip .span-time-c1{\r\n	text-align: left;\r\n	margin-right: 10px;\r\n}\r\n.emcharts-container .show-tip .span-time-c2{\r\n	text-align: right;\r\n}\r\n\r\n.emcharts-container .show-tip .span-k-c1{\r\n	text-align: left;\r\n	margin-right: 10px;\r\n}\r\n.emcharts-container .show-tip .span-k-c2{\r\n	text-align: right;\r\n}\r\n\r\n\r\n.emcharts-container .cross-y{\r\n	position:absolute;top:0px;z-index:98;border-left:1px dashed #8f8f8f;width:0px;background-color:#fff;pointer-events: none;\r\n}\r\n.emcharts-container .cross-x{\r\n	position:absolute;left:0px;z-index:98;border-top:1px dashed #8f8f8f;height:0px;\r\n}\r\n.emcharts-container .cross-p{\r\n	position:absolute;z-index:100;\r\n}\r\n.emcharts-container .mark-ma{\r\n	position:absolute;top:0px;left:0px;z-index:97;border:0;font-family:Arial;font-weight:lighter;\r\n}\r\n.emcharts-container .mark-ma span {\r\n	display:inline-block;padding-right:3px;font-size:12px;text-align:left;color:#ffba42;\r\n}\r\n\r\n.emcharts-container .mark-ma .span-m5{\r\n	color:#f4cb15;\r\n}\r\n.emcharts-container .mark-ma .span-m10{\r\n	color:#ff5b10;\r\n}\r\n.emcharts-container .mark-ma .span-m20{\r\n	color:#488ee6;\r\n}\r\n.emcharts-container .mark-ma .span-m30{\r\n	color:#fe59fe;\r\n}\r\n\r\n.emcharts-container .bar-color-span{\r\n	display: inline-block;\r\n	margin-left: 10px;\r\n	width: 10px;\r\n	height: 10px;\r\n	vertical-align: middle;\r\n}\r\n\r\n.emcharts-container .bar-value-span{\r\n	display: inline-block;\r\n	min-width: 50px;\r\n	padding: 0px 5px;\r\n	height: 20px;\r\n	line-height: 20px;\r\n	vertical-align: middle;\r\n	text-align: center;\r\n}\r\n\r\n/* chart_line 的样式 */\r\n.emcharts-container .chart_line_tips{\r\n	position: absolute;\r\n	z-index: 100;\r\n	background-color: #000;\r\n	filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=70);\r\n	opacity: 0.7;\r\n	color:white;\r\n	min-width: 80px;\r\n	padding: 10px;\r\n	border-radius: 5px;\r\n	font-family: \"Microsoft YaHei\";\r\n	font-weight: lighter;\r\n	text-align: left;\r\n	-webkit-user-select:none; -moz-user-select:none; -ms-user-select:none; user-select:none;\r\n}\r\n.emcharts-container .chart_line_tips_title{\r\n	font-size: 16px;\r\n	font-weight: 400px;\r\n	padding: 5px 0px;\r\n}\r\n.emcharts-container .chart_line_tips_line{\r\n	padding: 2px 0px;\r\n	font-size: 12px;\r\n}\r\n.emcharts-container .chart_line_tips_color{\r\n	display: inline-block;\r\n	width: 10px;\r\n	height: 10px;\r\n	border-radius: 10px;\r\n	margin-right: 5px;\r\n}\r\n.emcharts-container .chart_line_tips_content{}\r\n.emcharts-container .chart_line_yline{\r\n	position: absolute;\r\n	width: 0px;\r\n	border-left: solid 1px #898989;\r\n}\r\n.emcharts-container .chart_line_ybar{\r\n	position: absolute;\r\n	background-color: rgb(51,51,51);	\r\n	filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=10);\r\n	opacity: 0.1;\r\n}\r\n.emcharts-container .chart_line_cir{\r\n	background-color: white;\r\n	width: 10px;\r\n	height: 10px;\r\n	border-radius: 10px;\r\n	border: solid 1px white;\r\n	position: absolute;\r\n}\r\n\r\n.emcharts-container .event-div{\r\n	background-color: #fff;\r\n	opacity: 0;\r\n	position: absolute;\r\n	top: 0px;\r\n	bottom: 0px;\r\n	left: 0px;\r\n	right: 0px;\r\n	filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=0);\r\n	z-index: 1000;\r\n}\r\n\r\n/* 技术指标的样式 */\r\n.emcharts-container .markTContainer{\r\n	position: absolute;\r\n	font-family: 'Microsoft Yahei';\r\n	font-weight: lighter;\r\n	font-size: 12px;\r\n}\r\n.emcharts-container .markTContainer span{\r\n	margin-right: 5px;\r\n	display: inline-block;\r\n	*display: inline;\r\n	*zoom:1;\r\n}\r\n\r\n.emcharts-container .scale-div{\r\n	position:absolute;z-index:99;border:0px;width:90px;height:45px;\r\n	opacity:0.7;\r\n}\r\n.emcharts-container .scale-div .span-minus{\r\n	width: 30px;height: 30px;float: left;border-radius: 6px;\r\n	background: url(" + o(176) + ") no-repeat center center #cccccc;\r\n	font-size: 14px;\r\n}\r\n.emcharts-container .scale-div .span-plus{\r\n	width: 30px;height: 30px;float: right;border-radius: 6px;\r\n	background: url(" + o(177) + ") no-repeat center center #cccccc;\r\n	font-size: 14px;\r\n}\r\n.emcharts-container .scale-opacity{\r\n	opacity:0.3;\r\n}\r\n\r\n.emcharts-container .loading-chart {\r\n	position: absolute;\r\n	left:0;\r\n	top:0;\r\n	z-index: 100;\r\n	width: 100%;\r\n	height: 100%;\r\n	/*background-color: #fff;*/\r\n	text-align: center;\r\n    /*opacity: 0.7;*/\r\n    font-size: 20px;\r\n}\r\n\r\n.emcharts-container .delay-div{\r\n	position: absolute;\r\n	left:0;\r\n	top:0;\r\n	z-index: 10000;\r\n	width: 100%;\r\n	height: 100%;\r\n	background-color: #fff;\r\n    opacity: 0;\r\n}\r\n\r\n.emcharts-container .mark-point{\r\n	position: absolute;\r\n	z-index: 97;\r\n	min-width: 15px;\r\n	min-height: 15px;\r\n	border-radius: 10px;\r\n	background: url(" + o(178) + ") no-repeat center center/15px 15px #fff;\r\n	opacity: 1;\r\n}\r\n\r\n\r\n/*web港股的样式*/\r\n.emcharts-container .web-tips{\r\n	display: none;\r\n	left: -10000px;\r\n	top: -10000px;\r\n	position: absolute;\r\n	text-align: left;\r\n	background-color: #898989;\r\n	border-radius: 2px;\r\n	z-index: 10000;\r\n	display: inline-block;\r\n	color: white;\r\n	font-size: 12px;\r\n	text-align: center;\r\n	padding: 2px;\r\n	white-space:nowrap;\r\n}\r\n.emcharts-container .web-middleLine{\r\n	display: none;\r\n	position: absolute;\r\n	width: 0px;\r\n	border-right: dashed 1px #898989;\r\n}\r\n\r\n.emcharts-container .group-bar-mark{\r\n	position: absolute;\r\n	opacity: 0.1;\r\n	filter: progid:DXImageTransform.Microsoft.Alpha(opacity=10);\r\n}\r\n\r\n.emcharts-container .tech-index{\r\n	position:absolute;\r\n	z-index: 99;\r\n}\r\n\r\n.emcharts-container .tech-index-item{\r\n	background-color: #707070;\r\n	float:left;\r\n	text-align: center;\r\n	color:#fff;\r\n	cursor: pointer;\r\n}\r\n\r\n.emcharts-container .tech-index .current{\r\n	background-color: #c2c2c2;\r\n	color:#707070;\r\n}\r\n\r\n\r\n/*web版图表样式*/\r\n.emcharts-container{\r\n	-webkit-user-select:none;\r\n    -moz-user-select:none;\r\n    -ms-user-select:none;\r\n    user-select:none;\r\n    -webkit-tap-highlight-color: transparent;\r\n\r\n   	font-family: simsun;\r\n	font-size: 12px;\r\n	box-sizing: inherit;\r\n}\r\n\r\n.emcharts-container{\r\n	-webkit-user-select:none;\r\n    -moz-user-select:none;\r\n    -ms-user-select:none;\r\n    user-select:none;\r\n    -webkit-tap-highlight-color: transparent;\r\n\r\n   	font-family: simsun;\r\n	font-size: 12px;\r\n	box-sizing: inherit;\r\n}\r\n.emcharts-container .web-show-tip {\r\n	position: absolute;\r\n	top: 0px;\r\n	z-index: 999;\r\n	border: 1px solid #97c8ff;\r\n	/*padding: 8px;*/\r\n	box-shadow: 2px 2px 2px rgba(0,0,0,0.1);\r\n	background-color: #fff;\r\n	color: #666;\r\n	width: 150px;\r\n	font-size: 12px;\r\n	font-weight: lighter;\r\n	font-family: 'arial';\r\n\r\n}\r\n\r\n.emcharts-container .web-tip-line-left{\r\n	float: left;\r\n	width:50%;\r\n	text-align: left;\r\n	height:20px;\r\n	line-height: 20px;\r\n\r\n	*height:15px;\r\n	*line-height: 15px;\r\n}\r\n\r\n.emcharts-container .web-tip-line-right{\r\n	float: left;\r\n	width:50%;\r\n	text-align: right;\r\n	height:20px;\r\n	line-height: 20px;\r\n	white-space: nowrap;\r\n	*height:15px;\r\n	*line-height: 15px;\r\n}\r\n\r\n.emcharts-container .web-tip-first-line{\r\n	width:100%;\r\n	padding: 3px 0px;\r\n	background: #edf5ff;\r\n	text-align: center;\r\n}\r\n\r\n.emcharts-container .web-tip-line-container{\r\n	padding:3px 8px;\r\n	*margin-bottom: 8px;\r\n}\r\n\r\n.emcharts-container .time-tips-coordinate{\r\n	display: none;\r\n	position: absolute;\r\n	height: 14px;\r\n	padding: 0px 3px;\r\n	font-size: 12px;\r\n	background-color: #707070;\r\n	color: white;\r\n	z-index: 1000;\r\n}\r\n.emcharts-container .time-tips-top{\r\n	display: none;\r\n	font-size: 12px;\r\n	color: #000000;\r\n	position: absolute;\r\n	top: 0px;\r\n	font-weight: lighter;\r\n	font-family: \"Microsoft Yahei\";\r\n}\r\n\r\n/* web 版 分时图盘口异动样式 */\r\n.emcharts-container .timeChangeMainPad {\r\n    border: solid #65CAFE 1px;\r\n    position: absolute;\r\n    top: 100px;\r\n    left: 100px;\r\n    font-size: 12px;\r\n    color: #555;\r\n    background-color: white;\r\n    z-index: 1000;\r\n}\r\n.emcharts-container .timeChangeTriangle{\r\n	*display: none;\r\n	position: absolute;\r\n    left: 55px;\r\n    top: -6px;\r\n	width: 10px;\r\n	height: 10px;\r\n	background-color: #D1E7FF;\r\n	border-left: solid 1px #65CAFE;\r\n	border-top: solid 1px #65CAFE;\r\n	-webkit-transform: rotate(45deg);    /* for Chrome || Safari */\r\n	-moz-transform: rotate(45deg);       /* for Firefox */\r\n	-ms-transform: rotate(45deg);        /* for IE */\r\n	o-transform: rotate(45deg);         /* for Opera */\r\n}\r\n.emcharts-container .timeChangeTable{\r\n	*width: 170px;\r\n	min-width: 100px;\r\n	height: 50px;\r\n	border-spacing: 0px;\r\n    table-layout: fixed;\r\n}\r\n.emcharts-container .timeChangeHeader{\r\n	background-color: #D1E7FF;\r\n  	text-align: center;\r\n  	*width: 170px;\r\n    min-width: 100px;\r\n	padding: 7px 0px 5px 0px;\r\n	font-size: 14px;          \r\n}\r\n.timeChangeTable td{\r\n	padding: 0px 4px;\r\n	white-space: nowrap;\r\n}\r\n\r\n/* web版 k线图指标样式 */\r\n\r\n.emcharts-container .kt-pad {\r\n    position: absolute;\r\n    color: #5F5F5F;\r\n    margin-left: 15px;\r\n    min-width: 55px;\r\n}\r\n\r\n.emcharts-container .kt-title {\r\n    margin-bottom: 5px;\r\n    font-size: 12px;\r\n}\r\n\r\n.emcharts-container .kt-line {\r\n    /*background-color: white;*/ \r\n    position: relative; \r\n    margin: 7px 0px;\r\n}\r\n\r\n.emcharts-container .kt-line:hover{\r\n    cursor: pointer;\r\n}\r\n\r\n.emcharts-container .kt-radio-wrap{\r\n	position: relative; \r\n	display: inline-block; \r\n	*zoom:1;\r\n	*display:inline; \r\n	border: solid 1px #305896;height: 10px; \r\n	width: 10px;\r\n	/*background-color: white;*/\r\n	box-sizing:content-box;\r\n}\r\n\r\n.emcharts-container .kt-radio {\r\n    position:absolute; margin: 2px; width: 6px; height: 6px; background-color: #305896;display: none;\r\n}\r\n\r\n.emcharts-container .kt-name {\r\n    color: #555;line-height: 18px;display: inline-block; height: 16px;padding-left: 5px;position: absolute; top: -2px; left: 15px;*display: inline;\r\n}\r\n\r\n.emcharts-container .kt-radio-choose {\r\n    display: block;\r\n}\r\n\r\n/* web k 线的滑动 */\r\n.emcharts-container .slideBarCVS{\r\n	/*background-color: white;*/\r\n	outline: solid 1px #E9E9E9;\r\n\r\n}\r\n\r\n.emcharts-container .leftDrag{\r\n	background-color: white;\r\n	opacity: 2;\r\n	filter: progid:DXImageTransform.Microsoft.Alpha(opacity=200);\r\n}\r\n.emcharts-container .leftDrag:hover{\r\n	cursor: w-resize;\r\n}\r\n\r\n.emcharts-container .rightDrag{\r\n	background-color: white;\r\n}\r\n.emcharts-container .rightDrag:hover{cursor: e-resize;}\r\n.emcharts-container .containerBar{\r\n	background-color: #e0e0e0;\r\n	opacity: 0.5;\r\n	filter: progid:DXImageTransform.Microsoft.Alpha(opacity=50);\r\n	border:solid 1px #35709C;\r\n}\r\n.emcharts-container .containerBar:hover{cursor: move;}\r\n\r\n.emcharts-container .preference-container{\r\n	position: absolute;\r\n	z-index: 101;\r\n}\r\n.emcharts-container .preference-shade{\r\n	background-color: #333;\r\n	opacity: 0.1;\r\n	filter: progid:DXImageTransform.Microsoft.Alpha(opacity=10);\r\n	width:100%;\r\n	height: 100%;\r\n}\r\n.emcharts-container .preference-handle{\r\n	position:absolute;\r\n	z-index: 101;\r\n	/*background-color: #fff;*/\r\n	border: 1px solid #e6e6e6;\r\n	border-left: 0;\r\n	width:30px;\r\n	height: 40px;\r\n	line-height: 20px;\r\n	padding:5px;\r\n	text-align: center;\r\n	cursor: pointer;\r\n	font-size: 12px;\r\n    box-sizing: content-box;\r\n}\r\n.emcharts-container .set-tab{\r\n	border-bottom:1px solid #4267A0;\r\n	height:30px;\r\n	margin-bottom: 5px;\r\n}\r\n.emcharts-container .ma-tab{\r\n	/*float:left;*/\r\n	/*width:50%;*/\r\n	height:30px;\r\n	line-height: 30px;\r\n	text-align: center;\r\n	cursor: pointer;\r\n	font-size: 14px;\r\n}\r\n.emcharts-container .set-tab .current{\r\n	background-color: #4267A0;\r\n	color:#fff;\r\n}\r\n.emcharts-container .right-tab{\r\n	float:left;\r\n	width:50%;\r\n	height:30px;\r\n	line-height: 30px;\r\n	text-align: center;\r\n	cursor: pointer;\r\n	font-size: 14px;\r\n}\r\n\r\n.emcharts-container .set-container{\r\n	position: absolute;\r\n	z-index: 103;\r\n	background-color: #fff;\r\n	width:280px;\r\n	height:200px;\r\n}\r\n\r\n.emcharts-container .pre-notice{\r\n	height:20px;\r\n	line-height: 20px;\r\n	text-align: center;\r\n	margin: 5px 0px;\r\n	font-size: 14px;\r\n}\r\n.emcharts-container .ma-item{\r\n	height: 27px;\r\n	line-height: 30px;\r\n	padding-left: 15px;\r\n	text-align: center;\r\n}\r\n.emcharts-container .setting-span-ma5{\r\n	background-color:#f4cb15;\r\n}\r\n.emcharts-container .setting-span-ma10{\r\n	background-color:#ff5b10;\r\n}\r\n.emcharts-container .setting-span-ma20{\r\n	background-color:#488ee6;\r\n}\r\n.emcharts-container .setting-span-ma30{\r\n	background-color:#fe59fe;\r\n}\r\n\r\n.emcharts-container .span-setting{\r\n	display: inline-block;\r\n	*display: inline;\r\n	*zoom:1;\r\n	height: 15px;\r\n	width:15px;\r\n	margin-left: 10px;\r\n	vertical-align:middle;\r\n	cursor: pointer;\r\n}\r\n.emcharts-container .item-span{\r\n	\r\n}\r\n\r\n.emcharts-container .right-panel{\r\n	text-align: center;\r\n}\r\n.emcharts-container .right-panel-form{\r\n	text-align: left;\r\n	width: 160px;\r\n	margin: 20px auto;\r\n	line-height: 1.8;\r\n}\r\n/*.right-panel-btn{\r\n	border: none;\r\n	padding: 5px 15px;\r\n	margin: 10px 27px;\r\n	background-color: #4267A0;\r\n	color: white;\r\n}\r\n*/\r\n\r\n.emcharts-container .confirm-btn{\r\n	float:left;\r\n	border: none;\r\n	margin-left:30px;\r\n	margin-top:10px;\r\n	width:90px;\r\n	height: 30px;\r\n	line-height: 30px;\r\n	background-color: #305895;\r\n	color: white;\r\n	font-size: 14px;\r\n}\r\n.emcharts-container .cancle-btn{\r\n	float:right;\r\n	border: none;\r\n	margin-right:30px;\r\n	margin-top:10px;\r\n	width:90px;\r\n	height: 30px;\r\n	line-height: 30px;\r\n	background-color: #305895;\r\n	color: white;\r\n	font-size: 14px;\r\n}\r\n\r\n.emcharts-container .colorPad{\r\n	display: none;\r\n	position: absolute;\r\n	top: 120px;\r\n	left: 120px;\r\n	background-color: white;\r\n}\r\n.emcharts-container .colorPadTriangle{\r\n	position: absolute;\r\n	top: 9px;\r\n	left: -12px;\r\n	width: 16px;\r\n	height: 11px;\r\n	*display: none;\r\n	background-color: white;\r\n	overflow: hidden;\r\n	z-index: 100;\r\n}\r\n.emcharts-container .colorPadTriangle .up{\r\n	position: absolute;\r\n	top: 2px;\r\n	left: 3px;\r\n	width: 12px;\r\n	height: 10px;\r\n	border-top: solid 1px #aaa;\r\n	background-color: white;\r\n	-webkit-transform: rotate(-25deg);    /* for Chrome || Safari */\r\n	-moz-transform: rotate(-25deg);       /* for Firefox */\r\n	-ms-transform: rotate(-25deg);        /* for IE */\r\n	o-transform: rotate(-25deg);         /* for Opera */\r\n	*display: none;\r\n	z-index: 10;\r\n}\r\n.emcharts-container .colorPadTriangle .down{\r\n	position: absolute;\r\n	top: 7px;\r\n	left: -1px;\r\n	width: 12px;\r\n	height: 10px;\r\n	border-top: solid 1px #aaa;\r\n	background-color: white;\r\n	-webkit-transform: rotate(25deg);    /* for Chrome || Safari */\r\n	-moz-transform: rotate(25deg);       /* for Firefox */\r\n	-ms-transform: rotate(25deg);        /* for IE */\r\n	o-transform: rotate(25deg);         /* for Opera */\r\n	*display: none;\r\n	z-index: 10;\r\n}\r\n.emcharts-container .colorTable{\r\n	width: 100px;\r\n	height:90px;\r\n	border: solid 1px #aaa; \r\n}\r\n.emcharts-container .colorTable td{\r\n	position: relative;\r\n	z-index: 100;\r\n	border: solid 3px #fff;\r\n}\r\n.emcharts-container .ma-panel{\r\n	position: relative;\r\n}\r\n.emcharts-container .ma-item-input{\r\n	width:30px;\r\n	height: 15px;\r\n	line-height: 15px;\r\n	margin-left: 5px;\r\n	margin-right: 5px;\r\n	font-size: 12px;\r\n	vertical-align:middle;\r\n	padding-left: 3px;\r\n}\r\n\r\n.emcharts-container .fu-quan-select{\r\n\r\n	-webkit-user-select:none;\r\n    -moz-user-select:none;\r\n    -ms-user-select:none;\r\n    user-select:none;\r\n\r\n	position: absolute;\r\n	z-index: 100;\r\n	right:0px;\r\n	top:5px;\r\n	width:50px;\r\n	height:20px;\r\n	font-size: 12px;\r\n	font-family: simsun;\r\n\r\n	 -webkit-appearance: none;\r\n	 padding-left: 3px;\r\n}\r\n/* barHorizontal */\r\n.emcharts-container .horizontal-gmask{\r\n	position: absolute;\r\n	background-color: #000;\r\n	opacity: 0.1;\r\n	filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=10);\r\n}\r\n.emcharts-container .horizontal-gpanel{\r\n	position: absolute;\r\n	background-color: #000;\r\n	border-radius: 5px;\r\n	opacity: 0.7;\r\n	filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=70);\r\n	color:#ffffff;\r\n	padding: 5px 10px;\r\n}\r\n\r\n/*盘口异动的数据最后一条闪烁*/\r\n.timePositionChangesAni {\r\n	animation: timePositionChangesAni 0.7s infinite linear alternate;\r\n	-webkit-animation: timePositionChangesAni 0.7s infinite linear alternate;\r\n	-ms-animation: timePositionChangesAni 0.7s infinite linear alternate;\r\n}\r\n\r\n@keyframes timePositionChangesAni{\r\n	0%{width: 6px;height: 6px;margin-top: 0;margin-left: 0;}\r\n	100%{width: 10px;height: 10px;margin-top: -2px;margin-left: -2px;}\r\n}\r\n\r\n@-webkit-keyframes timePositionChangesAni{\r\n	0%{width: 6px;height: 6px;margin-top: 0;margin-left: 0;}\r\n	100%{width: 10px;height: 10px;margin-top: -2px;margin-left: -2px;}\r\n}\r\n\r\n@-ms-keyframes timePositionChangesAni{\r\n	0%{width: 6px;height: 6px;margin-top: 0;margin-left: 0;}\r\n	100%{width: 10px;height: 10px;margin-top: -2px;margin-left: -2px;}\r\n}\r\n\r\n\r\n\r\n/* 打点样式 */\r\n.__Emcharts_k_dotpints {\r\n	position: absolute;\r\n	z-index: 10;\r\n	left: 0;\r\n	top: 0;\r\n	height: 0;\r\n	width: 0;\r\n	overflow: visible;\r\n}\r\n\r\n.__Emcharts_k_dotpints a {\r\n	position: absolute;\r\n	display: block;\r\n	width: 10px;\r\n	height: 10px;\r\n	border: none;\r\n	outline: none;\r\n}\r\n.__Emcharts_k_dotpints a img {\r\n	display: block;\r\n	width: 100%;\r\n	height: 100%;\r\n	outline: none;\r\n	border: none;\r\n}\r\n\r\n", ""])
    }
    , function(t) {
        t.exports = function() {
            var t = [];
            return t.toString = function() {
                for (var t = [], i = 0; i < this.length; i++) {
                    var o = this[i];
                    t.push(o[2] ? "@media " + o[2] + "{" + o[1] + "}" : o[1])
                }
                return t.join("")
            }
                ,
                t.i = function(i, o) {
                    "string" == typeof i && (i = [[null, i, ""]]);
                    for (var e = {}, n = 0; n < this.length; n++) {
                        var s = this[n][0];
                        "number" == typeof s && (e[s] = !0)
                    }
                    for (n = 0; n < i.length; n++) {
                        var a = i[n];
                        "number" == typeof a[0] && e[a[0]] || (o && !a[2] ? a[2] = o : o && (a[2] = "(" + a[2] + ") and (" + o + ")"),
                            t.push(a))
                    }
                }
                ,
                t
        }
    }
    , function(t) {
        t.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAADCAYAAAB4bZQtAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKTWlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVN3WJP3Fj7f92UPVkLY8LGXbIEAIiOsCMgQWaIQkgBhhBASQMWFiApWFBURnEhVxILVCkidiOKgKLhnQYqIWotVXDjuH9yntX167+3t+9f7vOec5/zOec8PgBESJpHmomoAOVKFPDrYH49PSMTJvYACFUjgBCAQ5svCZwXFAADwA3l4fnSwP/wBr28AAgBw1S4kEsfh/4O6UCZXACCRAOAiEucLAZBSAMguVMgUAMgYALBTs2QKAJQAAGx5fEIiAKoNAOz0ST4FANipk9wXANiiHKkIAI0BAJkoRyQCQLsAYFWBUiwCwMIAoKxAIi4EwK4BgFm2MkcCgL0FAHaOWJAPQGAAgJlCLMwAIDgCAEMeE80DIEwDoDDSv+CpX3CFuEgBAMDLlc2XS9IzFLiV0Bp38vDg4iHiwmyxQmEXKRBmCeQinJebIxNI5wNMzgwAABr50cH+OD+Q5+bk4eZm52zv9MWi/mvwbyI+IfHf/ryMAgQAEE7P79pf5eXWA3DHAbB1v2upWwDaVgBo3/ldM9sJoFoK0Hr5i3k4/EAenqFQyDwdHAoLC+0lYqG9MOOLPv8z4W/gi372/EAe/tt68ABxmkCZrcCjg/1xYW52rlKO58sEQjFu9+cj/seFf/2OKdHiNLFcLBWK8ViJuFAiTcd5uVKRRCHJleIS6X8y8R+W/QmTdw0ArIZPwE62B7XLbMB+7gECiw5Y0nYAQH7zLYwaC5EAEGc0Mnn3AACTv/mPQCsBAM2XpOMAALzoGFyolBdMxggAAESggSqwQQcMwRSswA6cwR28wBcCYQZEQAwkwDwQQgbkgBwKoRiWQRlUwDrYBLWwAxqgEZrhELTBMTgN5+ASXIHrcBcGYBiewhi8hgkEQcgIE2EhOogRYo7YIs4IF5mOBCJhSDSSgKQg6YgUUSLFyHKkAqlCapFdSCPyLXIUOY1cQPqQ28ggMor8irxHMZSBslED1AJ1QLmoHxqKxqBz0XQ0D12AlqJr0Rq0Hj2AtqKn0UvodXQAfYqOY4DRMQ5mjNlhXIyHRWCJWBomxxZj5Vg1Vo81Yx1YN3YVG8CeYe8IJAKLgBPsCF6EEMJsgpCQR1hMWEOoJewjtBK6CFcJg4Qxwicik6hPtCV6EvnEeGI6sZBYRqwm7iEeIZ4lXicOE1+TSCQOyZLkTgohJZAySQtJa0jbSC2kU6Q+0hBpnEwm65Btyd7kCLKArCCXkbeQD5BPkvvJw+S3FDrFiOJMCaIkUqSUEko1ZT/lBKWfMkKZoKpRzame1AiqiDqfWkltoHZQL1OHqRM0dZolzZsWQ8ukLaPV0JppZ2n3aC/pdLoJ3YMeRZfQl9Jr6Afp5+mD9HcMDYYNg8dIYigZaxl7GacYtxkvmUymBdOXmchUMNcyG5lnmA+Yb1VYKvYqfBWRyhKVOpVWlX6V56pUVXNVP9V5qgtUq1UPq15WfaZGVbNQ46kJ1Bar1akdVbupNq7OUndSj1DPUV+jvl/9gvpjDbKGhUaghkijVGO3xhmNIRbGMmXxWELWclYD6yxrmE1iW7L57Ex2Bfsbdi97TFNDc6pmrGaRZp3mcc0BDsax4PA52ZxKziHODc57LQMtPy2x1mqtZq1+rTfaetq+2mLtcu0W7eva73VwnUCdLJ31Om0693UJuja6UbqFutt1z+o+02PreekJ9cr1Dund0Uf1bfSj9Rfq79bv0R83MDQINpAZbDE4Y/DMkGPoa5hpuNHwhOGoEctoupHEaKPRSaMnuCbuh2fjNXgXPmasbxxirDTeZdxrPGFiaTLbpMSkxeS+Kc2Ua5pmutG003TMzMgs3KzYrMnsjjnVnGueYb7ZvNv8jYWlRZzFSos2i8eW2pZ8ywWWTZb3rJhWPlZ5VvVW16xJ1lzrLOtt1ldsUBtXmwybOpvLtqitm63Edptt3xTiFI8p0in1U27aMez87ArsmuwG7Tn2YfYl9m32zx3MHBId1jt0O3xydHXMdmxwvOuk4TTDqcSpw+lXZxtnoXOd8zUXpkuQyxKXdpcXU22niqdun3rLleUa7rrStdP1o5u7m9yt2W3U3cw9xX2r+00umxvJXcM970H08PdY4nHM452nm6fC85DnL152Xlle+70eT7OcJp7WMG3I28Rb4L3Le2A6Pj1l+s7pAz7GPgKfep+Hvqa+It89viN+1n6Zfgf8nvs7+sv9j/i/4XnyFvFOBWABwQHlAb2BGoGzA2sDHwSZBKUHNQWNBbsGLww+FUIMCQ1ZH3KTb8AX8hv5YzPcZyya0RXKCJ0VWhv6MMwmTB7WEY6GzwjfEH5vpvlM6cy2CIjgR2yIuB9pGZkX+X0UKSoyqi7qUbRTdHF09yzWrORZ+2e9jvGPqYy5O9tqtnJ2Z6xqbFJsY+ybuIC4qriBeIf4RfGXEnQTJAntieTE2MQ9ieNzAudsmjOc5JpUlnRjruXcorkX5unOy553PFk1WZB8OIWYEpeyP+WDIEJQLxhP5aduTR0T8oSbhU9FvqKNolGxt7hKPJLmnVaV9jjdO31D+miGT0Z1xjMJT1IreZEZkrkj801WRNberM/ZcdktOZSclJyjUg1plrQr1zC3KLdPZisrkw3keeZtyhuTh8r35CP5c/PbFWyFTNGjtFKuUA4WTC+oK3hbGFt4uEi9SFrUM99m/ur5IwuCFny9kLBQuLCz2Lh4WfHgIr9FuxYji1MXdy4xXVK6ZHhp8NJ9y2jLspb9UOJYUlXyannc8o5Sg9KlpUMrglc0lamUycturvRauWMVYZVkVe9ql9VbVn8qF5VfrHCsqK74sEa45uJXTl/VfPV5bdra3kq3yu3rSOuk626s91m/r0q9akHV0IbwDa0b8Y3lG19tSt50oXpq9Y7NtM3KzQM1YTXtW8y2rNvyoTaj9nqdf13LVv2tq7e+2Sba1r/dd3vzDoMdFTve75TsvLUreFdrvUV99W7S7oLdjxpiG7q/5n7duEd3T8Wej3ulewf2Re/ranRvbNyvv7+yCW1SNo0eSDpw5ZuAb9qb7Zp3tXBaKg7CQeXBJ9+mfHvjUOihzsPcw83fmX+39QjrSHkr0jq/dawto22gPaG97+iMo50dXh1Hvrf/fu8x42N1xzWPV56gnSg98fnkgpPjp2Snnp1OPz3Umdx590z8mWtdUV29Z0PPnj8XdO5Mt1/3yfPe549d8Lxw9CL3Ytslt0utPa49R35w/eFIr1tv62X3y+1XPK509E3rO9Hv03/6asDVc9f41y5dn3m978bsG7duJt0cuCW69fh29u0XdwruTNxdeo94r/y+2v3qB/oP6n+0/rFlwG3g+GDAYM/DWQ/vDgmHnv6U/9OH4dJHzEfVI0YjjY+dHx8bDRq98mTOk+GnsqcTz8p+Vv9563Or59/94vtLz1j82PAL+YvPv655qfNy76uprzrHI8cfvM55PfGm/K3O233vuO+638e9H5ko/ED+UPPR+mPHp9BP9z7nfP78L/eE8/sl0p8zAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAAdSURBVHjaYvz///9/BhoBJgYaApoaDgAAAP//AwBSRgQCNPlECAAAAABJRU5ErkJggg=="
    }
    , function(t) {
        t.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAAXCAYAAADgKtSgAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKTWlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVN3WJP3Fj7f92UPVkLY8LGXbIEAIiOsCMgQWaIQkgBhhBASQMWFiApWFBURnEhVxILVCkidiOKgKLhnQYqIWotVXDjuH9yntX167+3t+9f7vOec5/zOec8PgBESJpHmomoAOVKFPDrYH49PSMTJvYACFUjgBCAQ5svCZwXFAADwA3l4fnSwP/wBr28AAgBw1S4kEsfh/4O6UCZXACCRAOAiEucLAZBSAMguVMgUAMgYALBTs2QKAJQAAGx5fEIiAKoNAOz0ST4FANipk9wXANiiHKkIAI0BAJkoRyQCQLsAYFWBUiwCwMIAoKxAIi4EwK4BgFm2MkcCgL0FAHaOWJAPQGAAgJlCLMwAIDgCAEMeE80DIEwDoDDSv+CpX3CFuEgBAMDLlc2XS9IzFLiV0Bp38vDg4iHiwmyxQmEXKRBmCeQinJebIxNI5wNMzgwAABr50cH+OD+Q5+bk4eZm52zv9MWi/mvwbyI+IfHf/ryMAgQAEE7P79pf5eXWA3DHAbB1v2upWwDaVgBo3/ldM9sJoFoK0Hr5i3k4/EAenqFQyDwdHAoLC+0lYqG9MOOLPv8z4W/gi372/EAe/tt68ABxmkCZrcCjg/1xYW52rlKO58sEQjFu9+cj/seFf/2OKdHiNLFcLBWK8ViJuFAiTcd5uVKRRCHJleIS6X8y8R+W/QmTdw0ArIZPwE62B7XLbMB+7gECiw5Y0nYAQH7zLYwaC5EAEGc0Mnn3AACTv/mPQCsBAM2XpOMAALzoGFyolBdMxggAAESggSqwQQcMwRSswA6cwR28wBcCYQZEQAwkwDwQQgbkgBwKoRiWQRlUwDrYBLWwAxqgEZrhELTBMTgN5+ASXIHrcBcGYBiewhi8hgkEQcgIE2EhOogRYo7YIs4IF5mOBCJhSDSSgKQg6YgUUSLFyHKkAqlCapFdSCPyLXIUOY1cQPqQ28ggMor8irxHMZSBslED1AJ1QLmoHxqKxqBz0XQ0D12AlqJr0Rq0Hj2AtqKn0UvodXQAfYqOY4DRMQ5mjNlhXIyHRWCJWBomxxZj5Vg1Vo81Yx1YN3YVG8CeYe8IJAKLgBPsCF6EEMJsgpCQR1hMWEOoJewjtBK6CFcJg4Qxwicik6hPtCV6EvnEeGI6sZBYRqwm7iEeIZ4lXicOE1+TSCQOyZLkTgohJZAySQtJa0jbSC2kU6Q+0hBpnEwm65Btyd7kCLKArCCXkbeQD5BPkvvJw+S3FDrFiOJMCaIkUqSUEko1ZT/lBKWfMkKZoKpRzame1AiqiDqfWkltoHZQL1OHqRM0dZolzZsWQ8ukLaPV0JppZ2n3aC/pdLoJ3YMeRZfQl9Jr6Afp5+mD9HcMDYYNg8dIYigZaxl7GacYtxkvmUymBdOXmchUMNcyG5lnmA+Yb1VYKvYqfBWRyhKVOpVWlX6V56pUVXNVP9V5qgtUq1UPq15WfaZGVbNQ46kJ1Bar1akdVbupNq7OUndSj1DPUV+jvl/9gvpjDbKGhUaghkijVGO3xhmNIRbGMmXxWELWclYD6yxrmE1iW7L57Ex2Bfsbdi97TFNDc6pmrGaRZp3mcc0BDsax4PA52ZxKziHODc57LQMtPy2x1mqtZq1+rTfaetq+2mLtcu0W7eva73VwnUCdLJ31Om0693UJuja6UbqFutt1z+o+02PreekJ9cr1Dund0Uf1bfSj9Rfq79bv0R83MDQINpAZbDE4Y/DMkGPoa5hpuNHwhOGoEctoupHEaKPRSaMnuCbuh2fjNXgXPmasbxxirDTeZdxrPGFiaTLbpMSkxeS+Kc2Ua5pmutG003TMzMgs3KzYrMnsjjnVnGueYb7ZvNv8jYWlRZzFSos2i8eW2pZ8ywWWTZb3rJhWPlZ5VvVW16xJ1lzrLOtt1ldsUBtXmwybOpvLtqitm63Edptt3xTiFI8p0in1U27aMez87ArsmuwG7Tn2YfYl9m32zx3MHBId1jt0O3xydHXMdmxwvOuk4TTDqcSpw+lXZxtnoXOd8zUXpkuQyxKXdpcXU22niqdun3rLleUa7rrStdP1o5u7m9yt2W3U3cw9xX2r+00umxvJXcM970H08PdY4nHM452nm6fC85DnL152Xlle+70eT7OcJp7WMG3I28Rb4L3Le2A6Pj1l+s7pAz7GPgKfep+Hvqa+It89viN+1n6Zfgf8nvs7+sv9j/i/4XnyFvFOBWABwQHlAb2BGoGzA2sDHwSZBKUHNQWNBbsGLww+FUIMCQ1ZH3KTb8AX8hv5YzPcZyya0RXKCJ0VWhv6MMwmTB7WEY6GzwjfEH5vpvlM6cy2CIjgR2yIuB9pGZkX+X0UKSoyqi7qUbRTdHF09yzWrORZ+2e9jvGPqYy5O9tqtnJ2Z6xqbFJsY+ybuIC4qriBeIf4RfGXEnQTJAntieTE2MQ9ieNzAudsmjOc5JpUlnRjruXcorkX5unOy553PFk1WZB8OIWYEpeyP+WDIEJQLxhP5aduTR0T8oSbhU9FvqKNolGxt7hKPJLmnVaV9jjdO31D+miGT0Z1xjMJT1IreZEZkrkj801WRNberM/ZcdktOZSclJyjUg1plrQr1zC3KLdPZisrkw3keeZtyhuTh8r35CP5c/PbFWyFTNGjtFKuUA4WTC+oK3hbGFt4uEi9SFrUM99m/ur5IwuCFny9kLBQuLCz2Lh4WfHgIr9FuxYji1MXdy4xXVK6ZHhp8NJ9y2jLspb9UOJYUlXyannc8o5Sg9KlpUMrglc0lamUycturvRauWMVYZVkVe9ql9VbVn8qF5VfrHCsqK74sEa45uJXTl/VfPV5bdra3kq3yu3rSOuk626s91m/r0q9akHV0IbwDa0b8Y3lG19tSt50oXpq9Y7NtM3KzQM1YTXtW8y2rNvyoTaj9nqdf13LVv2tq7e+2Sba1r/dd3vzDoMdFTve75TsvLUreFdrvUV99W7S7oLdjxpiG7q/5n7duEd3T8Wej3ulewf2Re/ranRvbNyvv7+yCW1SNo0eSDpw5ZuAb9qb7Zp3tXBaKg7CQeXBJ9+mfHvjUOihzsPcw83fmX+39QjrSHkr0jq/dawto22gPaG97+iMo50dXh1Hvrf/fu8x42N1xzWPV56gnSg98fnkgpPjp2Snnp1OPz3Umdx590z8mWtdUV29Z0PPnj8XdO5Mt1/3yfPe549d8Lxw9CL3Ytslt0utPa49R35w/eFIr1tv62X3y+1XPK509E3rO9Hv03/6asDVc9f41y5dn3m978bsG7duJt0cuCW69fh29u0XdwruTNxdeo94r/y+2v3qB/oP6n+0/rFlwG3g+GDAYM/DWQ/vDgmHnv6U/9OH4dJHzEfVI0YjjY+dHx8bDRq98mTOk+GnsqcTz8p+Vv9563Or59/94vtLz1j82PAL+YvPv655qfNy76uprzrHI8cfvM55PfGm/K3O233vuO+638e9H5ko/ED+UPPR+mPHp9BP9z7nfP78L/eE8/sl0p8zAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAABFSURBVHjaYjxz5gwDMcDY2Pg/jH327FlGYvQwMdAQjBo+avhIMJzx/////0eDBR2wEFsIjRZco4aPGj4AhgMAAAD//wMAq70Q99ei408AAAAASUVORK5CYII="
    }
    , function(t) {
        t.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyFpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDoxQjBBMjM2MjI2REMxMUU2QURERTg3REFCOEYzQUQzNiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDoxQjBBMjM2MzI2REMxMUU2QURERTg3REFCOEYzQUQzNiI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjFCMEEyMzYwMjZEQzExRTZBRERFODdEQUI4RjNBRDM2IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjFCMEEyMzYxMjZEQzExRTZBRERFODdEQUI4RjNBRDM2Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+FqRI6gAAAypJREFUeNqsVktIlFEUPhND6DRgOfSCCFs0jQYNWTaJ1qRGjQxhRNKDFi2KJCOoyFVFJgQ9XAQtkhRclVCJFoa5GJUpQQrCFmkygdAsejCZYSO1mb7Dvf/8j7nzAg98zP3Pvfd8cx733GtLHKRs4gZ41T6gDHBJfQz4CAwBfcB0JiO2DER+oA3YRblJGLgKjKomlyh0BcADYDgPEpJrh+XegmxExUAIOMPeUv5ik3tD0paSyAG8BCqTmtsTRCvcapOH24kuD6YjrJS2HJrCbpi8B/hMy11riIrWEc3KPJfU6nPTb4kO4c8HkJapN7p+Lqqt90mbp43F4JfxNYer6xvK4RjRTEh4djcs9IXLiBb+6Ov4m4V14RdE3ae0mQRQwwWiEbGF6uS/3h4Uy+pPiI3zc0RPL+le3XxO1Fyue3odHs1MGgmM8poLhXPkTZKweKoEAaN4FZYcEGMtV+xd9DNRQ4v4Zr0bJiLv0+WLbXvZo1YMrqVMbzlK1PqY6EKdOnRaqKyhfHKfaLDNau2GPe1ZKd8rfvccR0hCoijYw/4uEUoWZ5E+ZtmBPc7lKmu7mcijJCqrIPr5nWgbchlpRrgmhX7kkfCwGrqzt8y5cnaC+JfK2ia7oXeZw1boFOOBbqIjFxHceqL2c3pBMDGHqw4l/m5A/xNc3qni4hz9xWCpSc0H8UdUFAKXdyMSH/+NcPrNJW0scU1iX4lavFbtP7vswmtN3ni2EnWeF0QsdwLmbScRopJSlHVVrq0pxkRTJiIuAj47sxm6/koUhsMp2pBRuFt86FHt+GSXh7UmqdKSnUnY4y8Rog2bdd36jeJbTRRmol7TOcpGwl7E51PDpuVVLc+4M0zINpFduNq4kY72pc6xR8YzZW5BE1r3vqJsqkbhLt2I8h7q0fseF467Qhxc12qRI7Mk5K2bvI/4+n2oLFXtXOwMWDuzKBzug3y4eztU+WGbI9Y3g0PejD5aHBkH+AKLW29YVgTlgsUgCWokqjdDTJZ6h4xvvpKQe2ulrYyvoAWgSRKO5UEyJgmajJ7k8q5LHk+gAdgPlFoekNzSXwH9ssOklf8CDADTqe/lOuA5/wAAAABJRU5ErkJggg=="
    }
    , function(t) {
        function i(t, i) {
            for (var o = 0; o < t.length; o++) {
                var e = t[o]
                    , n = c[e.id];
                if (n) {
                    n.refs++;
                    for (var s = 0; s < n.parts.length; s++)
                        n.parts[s](e.parts[s]);
                    for (; s < e.parts.length; s++)
                        n.parts.push(r(e.parts[s], i))
                } else {
                    for (var a = [], s = 0; s < e.parts.length; s++)
                        a.push(r(e.parts[s], i));
                    c[e.id] = {
                        id: e.id,
                        refs: 1,
                        parts: a
                    }
                }
            }
        }
        function o(t) {
            for (var i = [], o = {}, e = 0; e < t.length; e++) {
                var n = t[e]
                    , s = n[0]
                    , a = n[1]
                    , r = n[2]
                    , p = n[3]
                    , h = {
                    css: a,
                    media: r,
                    sourceMap: p
                };
                o[s] ? o[s].parts.push(h) : i.push(o[s] = {
                    id: s,
                    parts: [h]
                })
            }
            return i
        }
        function e(t, i) {
            var o = m()
                , e = g[g.length - 1];
            if ("top" === t.insertAt)
                e ? e.nextSibling ? o.insertBefore(i, e.nextSibling) : o.appendChild(i) : o.insertBefore(i, o.firstChild),
                    g.push(i);
            else {
                if ("bottom" !== t.insertAt)
                    throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
                o.appendChild(i)
            }
        }
        function n(t) {
            t.parentNode.removeChild(t);
            var i = g.indexOf(t);
            i >= 0 && g.splice(i, 1)
        }
        function s(t) {
            var i = document.createElement("style");
            return i.type = "text/css",
                e(t, i),
                i
        }
        function a(t) {
            var i = document.createElement("link");
            return i.rel = "stylesheet",
                e(t, i),
                i
        }
        function r(t, i) {
            var o, e, r;
            if (i.singleton) {
                var c = v++;
                o = f || (f = s(i)),
                    e = p.bind(null, o, c, !1),
                    r = p.bind(null, o, c, !0)
            } else
                t.sourceMap && "function" == typeof URL && "function" == typeof URL.createObjectURL && "function" == typeof URL.revokeObjectURL && "function" == typeof Blob && "function" == typeof btoa ? (o = a(i),
                        e = l.bind(null, o),
                        r = function() {
                            n(o),
                            o.href && URL.revokeObjectURL(o.href)
                        }
                ) : (o = s(i),
                        e = h.bind(null, o),
                        r = function() {
                            n(o)
                        }
                );
            return e(t),
                function(i) {
                    if (i) {
                        if (i.css === t.css && i.media === t.media && i.sourceMap === t.sourceMap)
                            return;
                        e(t = i)
                    } else
                        r()
                }
        }
        function p(t, i, o, e) {
            var n = o ? "" : e.css;
            if (t.styleSheet)
                t.styleSheet.cssText = y(i, n);
            else {
                var s = document.createTextNode(n)
                    , a = t.childNodes;
                a[i] && t.removeChild(a[i]),
                    a.length ? t.insertBefore(s, a[i]) : t.appendChild(s)
            }
        }
        function h(t, i) {
            var o = i.css
                , e = i.media;
            if (e && t.setAttribute("media", e),
                t.styleSheet)
                t.styleSheet.cssText = o;
            else {
                for (; t.firstChild; )
                    t.removeChild(t.firstChild);
                t.appendChild(document.createTextNode(o))
            }
        }
        function l(t, i) {
            var o = i.css
                , e = i.sourceMap;
            e && (o += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(e)))) + " */");
            var n = new Blob([o],{
                type: "text/css"
            })
                , s = t.href;
            t.href = URL.createObjectURL(n),
            s && URL.revokeObjectURL(s)
        }
        var c = {}
            , d = function(t) {
            var i;
            return function() {
                return "undefined" == typeof i && (i = t.apply(this, arguments)),
                    i
            }
        }
            , u = d(function() {
            return /msie [6-9]\b/.test(self.navigator.userAgent.toLowerCase())
        })
            , m = d(function() {
            return document.head || document.getElementsByTagName("head")[0]
        })
            , f = null
            , v = 0
            , g = [];
        t.exports = function(t, e) {
            e = e || {},
            "undefined" == typeof e.singleton && (e.singleton = u()),
            "undefined" == typeof e.insertAt && (e.insertAt = "bottom");
            var n = o(t);
            return i(n, e),
                function(t) {
                    for (var s = [], a = 0; a < n.length; a++) {
                        var r = n[a]
                            , p = c[r.id];
                        p.refs--,
                            s.push(p)
                    }
                    if (t) {
                        var h = o(t);
                        i(h, e)
                    }
                    for (var a = 0; a < s.length; a++) {
                        var p = s[a];
                        if (0 === p.refs) {
                            for (var l = 0; l < p.parts.length; l++)
                                p.parts[l]();
                            delete c[p.id]
                        }
                    }
                }
        }
        ;
        var y = function() {
            var t = [];
            return function(i, o) {
                return t[i] = o,
                    t.filter(Boolean).join("\n")
            }
        }()
    }
]);
